#include "StdAfx.h"

#include "Nodes/G2FlowBaseNode.h"
#include "Player.h"
#include "ILevelSystem.h"
#include "IAIActor.h"
#include "IAgent.h"
#include "Code_System/SpawnSystem.h"
#include "Code_System/WorldStability.h"
#include "Code_System/ModifySystem.h"
#include "Code_System/DisableSystem.h"
#include "Code_System/Money.h"
#include "Code_System/Loot.h"
#include "Code_System/AbilityModule.h"
#include "Code_System/BasicRayCast.h"
#include "Code_System/SpawnedAIManager.h"
#include "Code_System/MicroSystem/StateManager.h"
#include "Code_System/EntityDataBase.h"
#include "EntityBulletMode.h"


//--------GetStability--------
class CFlowGetStability : public CFlowBaseNode<eNCT_Singleton> ,public IGameFrameworkListener
{
	enum INPUTS
	{
		EIP_ENABLE,
		EIP_DISABLE
	};
	enum OUTPUTS
	{
		EOP_STABILITY,
	};
public:
	CFlowGetStability(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[]=
		{
			InputPortConfig_Void("Enable",_HELP("Call this node")),
			InputPortConfig_Void("Disable",_HELP("Call this node")),
			{0}
		};
		static const SOutputPortConfig out_ports[]=
		{
			OutputPortConfig<int>( "Stability", _HELP("Mass of modified entity")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("Get Stability");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				m_actInfo=*pActInfo;
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				break;
			}
		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_ENABLE))
			{
				gEnv->pGame->GetIGameFramework()->RegisterListener(this, "CFlowGetStability",FRAMEWORKLISTENERPRIORITY_GAME);
			}
			if (IsPortActive(pActInfo, EIP_DISABLE))
			{
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
			}
			break;
		}
	}
	// IGameFrameworkListener
	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) {}
	virtual void OnPostUpdate(float fDelta)
	{
		ActivateOutput(&m_actInfo,EOP_STABILITY,(int)g_pGame->GetWorldStability()->Getstability());
	}
private:
	SActivationInfo m_actInfo;
};


//--------SystemControl--------
class CFlowSystemControl : public CFlowBaseNode<eNCT_Singleton>
{
	enum INPUTS
	{
		EIP_EnableSpawnSystem,
		EIP_EnableScanSystem,
		EIP_EnableDisableSystem,
		EIP_EnableDeleteSystem,
		EIP_EnableModifySystem,
		EIP_Shutdown
	};
	enum OUTPUTS
	{
		EOP_SpawnSystemEnabled,
		EOP_ScanSystemEnabled,
		EOP_DisableSystemEnabled,
		EOP_DeleteSystemEnabled,
		EOP_ModifySystemEnabled,
		EOP_HasShutdown,
	};
public:
	CFlowSystemControl(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_Void("EnableSpawnSystem",_HELP("Call this node")),
			InputPortConfig_Void("EnableScanSystem",_HELP("Call this node")),
			InputPortConfig_Void("EnableDisableSystem",_HELP("Call this node")),
			InputPortConfig_Void("EnableDeleteSystem",_HELP("Call this node")),
			InputPortConfig_Void("EnableModifySystem",_HELP("Call this node")),
			InputPortConfig_Void("Shutdown",_HELP("Call this node")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<bool>( "SpawnSystemEnable", _HELP("True if is spawn mode selected")),
			OutputPortConfig<bool>( "ScanSystemEnabled", _HELP("True if is scan mode selected")),
			OutputPortConfig<bool>( "DisableSystemEnabled", _HELP("True if is disable mode selected")),
			OutputPortConfig<bool>( "DeleteSystemEnabled", _HELP("True if is delete mode selected")),
			OutputPortConfig<bool>( "ModifySystemEnabled", _HELP("True if is modify mode selected")),
			OutputPortConfig<bool>( "HasShutdown", _HELP("True if is shut down all system")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("Control System");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case(eFE_Activate):
			{
				BasicRayCast *brc=g_pGame->GetBasicRayCast();
				if(IsPortActive(pActInfo,EIP_EnableSpawnSystem))
				{
					SpawnSystem *ts=g_pGame->GetSpawnSystem();
					ModifySystem *pm=g_pGame->GetModifySystem();
					if(ts->IsFunctionStarted()==false || ts->GetMode() !=0)  //Start or close spawn ystem
					{
						pm->Shutdown();
						ts->SetStartFunction(true);
						ts->Setedit(false);
						ts->Setlocked(false);
						brc->ChangeFlag(ent_terrain | ent_static);
						brc->ChangeDistance(10.f);
						ActivateOutput(pActInfo,EOP_SpawnSystemEnabled,true);
						return;
					}
					if(ts->IsFunctionStarted())
					{
						ActivateOutput(pActInfo,EOP_HasShutdown,true);
						ts->Shutdown();
						brc->Load();
					}
					break;
				}
				if(IsPortActive(pActInfo,EIP_EnableScanSystem))
				{
					SpawnSystem *ts=g_pGame->GetSpawnSystem();
					ModifySystem *pm=g_pGame->GetModifySystem();
					if(ts->IsFunctionStarted()==false || ts->GetMode() !=1 )
					{
						pm->Shutdown();
						ts->SetStartFunction(true);
						ts->Setedit(false);
						ts->Setlocked(false);
						brc->ChangeFlag(ent_static | ent_rigid | ent_sleeping_rigid | ent_living | ent_independent);
						brc->ChangeDistance(10.f);
						ActivateOutput(pActInfo,EOP_ScanSystemEnabled,true);
						return;
					}
					if(ts->IsFunctionStarted())
					{
						ActivateOutput(pActInfo,EOP_HasShutdown,true);
						ts->Shutdown();
						brc->Load();
					}
					break;
				}
				if(IsPortActive(pActInfo,EIP_EnableDeleteSystem))
				{
					SpawnSystem *ts=g_pGame->GetSpawnSystem();
					ModifySystem *pm=g_pGame->GetModifySystem();
					if(pm->IsFunctionStarted() == false)
					{
						ts->Shutdown();
						pm->SetStartFunction(true);
						brc->ChangeFlag(ent_static | ent_rigid | ent_sleeping_rigid | ent_living | ent_independent);
						brc->ChangeDistance(15.f);
						ActivateOutput(pActInfo,EOP_DeleteSystemEnabled,true);
						return;
					}
					if(pm->IsFunctionStarted())
					{
						ActivateOutput(pActInfo,EOP_HasShutdown,true);
						pm->Shutdown();
						brc->Load();
					}
					break;
				}
				if(IsPortActive(pActInfo,EIP_EnableModifySystem))
				{
					SpawnSystem *ts=g_pGame->GetSpawnSystem();
					ModifySystem *pm=g_pGame->GetModifySystem();
					if(pm->IsFunctionStarted() == false)
					{
						ts->Shutdown();
						pm->SetStartFunction(true);
						brc->ChangeFlag( ent_rigid | ent_sleeping_rigid | ent_living | ent_independent);
						brc->ChangeDistance(15.f);
						ActivateOutput(pActInfo,EOP_ModifySystemEnabled,true);
						return;
					}
					if(pm->IsFunctionStarted())
					{
						ActivateOutput(pActInfo,EOP_HasShutdown,true);
						pm->Shutdown();
						brc->Load();
					}
					break;
				}
				if(IsPortActive(pActInfo,EIP_Shutdown))
				{
					SpawnSystem *ts=g_pGame->GetSpawnSystem();
					ModifySystem *pm=g_pGame->GetModifySystem();
					ts->Shutdown();
					pm->Shutdown();
					brc->Load();
					ActivateOutput(pActInfo,EOP_HasShutdown,true);
				}
			}
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

//--------SpawnSystemState--------
class CFlowSpawnSystemState : public CFlowBaseNode<eNCT_Singleton>
{
	enum INPUTS
	{
		EIP_Call=0,
	};
	enum OUTPUTS
	{
		EOP_Normal=0,
		EOP_Deep,
		EOP_Edit,
		EOP_Shutdown,
	};
public:
	CFlowSpawnSystemState(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_Void("Call",_HELP("Call this node")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<bool>( "IsNormalMode", _HELP("True if is spawn mode selected")),
			OutputPortConfig<bool>( "IsDeepMode", _HELP("True if is scan mode selected")),
			OutputPortConfig<bool>( "IsEditing", _HELP("True if is editing")),
			OutputPortConfig<bool>( "Shutdown", _HELP("True if is editing")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("SpawnSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case(eFE_Activate):
			{
				if(IsPortActive(pActInfo,EIP_Call))
				{
					ConnectMode *pCM=g_pGame->GetConnectMode();
					if(g_pGame->GetConnectMode()->GetMode() == eConnectMode::eCMode::Normal)
						ActivateOutput(pActInfo,EOP_Normal,true);
					if(g_pGame->GetConnectMode()->GetMode() == eConnectMode::eCMode::Deep)
					{
						if(!g_pGame->GetSpawnSystem()->IsFunctionStarted())
						{
							ActivateOutput(pActInfo,EOP_Deep,true);
							pCM->SetLevel(eConnectMode::eCLevel::eCLevel_SpawnMenu);
						}
					}
					/*if(g_pGame->GetSpawnSystem()->IsLocked())
						ActivateOutput(pActInfo,EOP_Edit,true);*/
					if(g_pGame->GetSpawnSystem()->IsFunctionStarted())
					{
						g_pGame->GetSpawnSystem()->Shutdown();
						ActivateOutput(pActInfo,EOP_Shutdown,true);
						pCM->PopLevel();
					}
					break;
				}
			}
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};



//--------GetSpawnXML-------//
class CFlowSpawnSystemGetXML : public CFlowBaseNode<eNCT_Singleton> ,public IGameFrameworkListener
{
	enum INPUTS
	{
		EIP_Get=0,
	};
	enum OUTPUTS
	{
		EOP_Name = 0,
		EOP_Cost,
		EOP_Index,
		EOP_Class,
		EOP_Favo,
		EOP_Finished,
	};
public:
	CFlowSpawnSystemGetXML(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Get",_HELP("Get XML")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<string>( "Name", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Cost", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Index", _HELP("Fire rate of this weapon")),
			OutputPortConfig<string>( "Class", _HELP("Fire rate of this weapon")),
			OutputPortConfig<bool>( "Favorite", _HELP("Fire rate of this weapon")),
			OutputPortConfig<bool>( "Finished", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("SpawnSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case eFE_Initialize:
			{
				pm=g_pGame->GetModifySystem();
				m_actInfo=*pActInfo;
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				break;
			}
		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_Get))
			{
				SpawnSystem *ts=g_pGame->GetSpawnSystem();
				count=0;
				root=gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
				maxNode=root->getChildCount();
				//root->setAttr("max",maxNode);		//Optional.
				gEnv->pGame->GetIGameFramework()->RegisterListener(this, "CFlowSpawnSystemGetXML",FRAMEWORKLISTENERPRIORITY_GAME);
				break;
			}
		}
	}

	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) {}
	virtual void OnPostUpdate(float fDelta)
	{
		if(count<maxNode)
		{
			XmlString m_Name;
			int m_Cost;
			int m_Index;
			XmlString m_Class;
			bool m_Favorite;
			XmlNodeRef Entity_Ref=root->getChild(count);
			bool isSet(false);
			Entity_Ref->getAttr("set",isSet);
			if(!isSet)
			{
				XmlNodeRef params=Entity_Ref->getChild(0);
				params->getAttr("Name",m_Name);
				params->getAttr("StabilityCost",m_Cost);
				params->getAttr("Index",m_Index);
				params->getAttr("Class",m_Class);
				params->getAttr("Favorite",m_Favorite);
				ActivateOutput(&m_actInfo,EOP_Name,(string)m_Name);
				ActivateOutput(&m_actInfo,EOP_Cost,m_Cost);
				ActivateOutput(&m_actInfo,EOP_Index,m_Index);
				ActivateOutput(&m_actInfo,EOP_Class,(string)m_Class);
				ActivateOutput(&m_actInfo,EOP_Favo,m_Favorite);
			}
			else
			{
				XmlNodeRef SetEntity=Entity_Ref->getChild(0);
				XmlNodeRef params=SetEntity->getChild(0);
				m_Name=Entity_Ref->getTag();
				params->getAttr("StabilityCost",m_Cost);
				Entity_Ref->getAttr("Index",m_Index);
				params->getAttr("Class",m_Class);
				params->getAttr("Favorite",m_Favorite);
				ActivateOutput(&m_actInfo,EOP_Name,(string)m_Name);
				ActivateOutput(&m_actInfo,EOP_Cost,m_Cost);
				ActivateOutput(&m_actInfo,EOP_Index,m_Index);
				ActivateOutput(&m_actInfo,EOP_Class,(string)m_Class);
				ActivateOutput(&m_actInfo,EOP_Favo,m_Favorite);
			}
			count++;
		}
		else
		{
			g_pGame->GetSpawnSystem()->SetXMLUpdate(false);
			gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

private:
	XmlNodeRef root;
	int maxNode;
	int count;
	ModifySystem *pm;
	SActivationInfo m_actInfo;
};



//--------GetSpawnSystemIndex--------
class CFlowIsSpawnSystemIndex : public CFlowBaseNode<eNCT_Singleton>
{
	enum INPUTS
	{
		EIP_Index=0,
	};
	enum OUTPUTS
	{
		EOP_Index=0,
	};
public:
	CFlowIsSpawnSystemIndex(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Index",_HELP("Get system state")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<int>( "Index", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("SpawnSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case(eFE_Activate):
			{
				if(IsPortActive(pActInfo,EIP_Index))
				{
					SpawnSystem *ts=g_pGame->GetSpawnSystem();
					int tindex=GetPortInt(pActInfo,EIP_Index);
					if(tindex>0)
					{
						ts->SetXMLindex(tindex-1);
						ts->FindEntityNode(tindex-1);
						bool isSet(false);
						ts->GetEntityNode()->getAttr("set",isSet);
						g_pGame->GetConnectMode()->PopLevel();
						if(!isSet)
						{
							ts->Spawn(tindex-1);
							ts->SetStartFunction(true);
							ts->Setlocked(true);
							ActivateOutput(pActInfo,EOP_Index,tindex-1);
						}
						else
						{
							ts->Spawn(tindex-1);
							ts->SetStartFunction(true);
							ts->Setlocked(true);
							ActivateOutput(pActInfo,EOP_Index,tindex-1);
						}
					}
					else
						return;
				}
			}
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

//--------GetSpawnListClass--------
class CFlowGetSpawnClass : public CFlowBaseNode<eNCT_Singleton>
{
	enum INPUTS
	{
		EIP_Index = 0,
	};
	enum OUTPUTS
	{
		EOP_BASIC,
		EOP_DESTROY,
		EOP_ITEM,
		EOP_VEHICLE,
		EOP_AI,
		EOP_LIGHT,
		EOP_LADDER,
	};
public:
	CFlowGetSpawnClass(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Index",_HELP("Get system state")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<int>( "BasicEntity", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "DestroyableObject", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Item", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Vehicle", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "AI", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Light", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Ladder", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("SpawnSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case(eFE_Activate):
			{
				if(IsPortActive(pActInfo,EIP_Index))
				{
					SpawnSystem *ts=g_pGame->GetSpawnSystem();
					if(ts->IsFunctionStarted())
					{
						int tindex=GetPortInt(pActInfo,EIP_Index);
						if(tindex>=0)
						{
							XmlNodeRef root = gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
							XmlNodeRef EntityNode=root->getChild(tindex);
							XmlNodeRef Params=EntityNode->getChild(0);
							XmlString className;
							Params->getAttr("Class",className);
							if(className == "BasicEntity")
								ActivateOutput(pActInfo,EOP_BASIC,tindex);
							if(className == "DestroyableObject")
								ActivateOutput(pActInfo,EOP_DESTROY,tindex);
							if(className == "Item")
								ActivateOutput(pActInfo,EOP_ITEM,tindex);
							if(className == "Vehicle")
								ActivateOutput(pActInfo,EOP_VEHICLE,tindex);
							if(className == "AI")
								ActivateOutput(pActInfo,EOP_AI,tindex);
							if(className == "DestroyableLight")
								ActivateOutput(pActInfo,EOP_LIGHT,tindex);
							if(className == "Ladder")
								ActivateOutput(pActInfo,EOP_LADDER,tindex);
						}
					}
					else
						return;
				}
			}
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};


//--------GetSpawnData_BasicEntity--------
class CFlowGetSD_BasicEntity : public CFlowBaseNode<eNCT_Singleton>
{
	enum INPUTS
	{
		EIP_Index,
	};
	enum OUTPUTS
	{
		EOP_TIME = 0,
		EOP_MASS,
		EOP_DENSITY,
		EOP_RIGID,
	};
public:
	CFlowGetSD_BasicEntity(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Index",_HELP("Get system state")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<float>( "SpawnTime", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Mass", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Density", _HELP("Fire rate of this weapon")),
			OutputPortConfig<bool>( "Rigid", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("SpawnSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case(eFE_Activate):
			{
				if(IsPortActive(pActInfo,EIP_Index))
				{
					SpawnSystem *ts=g_pGame->GetSpawnSystem();
					if(ts->IsFunctionStarted())
					{
						int tindex=GetPortInt(pActInfo,EIP_Index);
						if(tindex>=0)
						{
							XmlNodeRef root=gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
							XmlNodeRef EntityNode=root->getChild(tindex);
							XmlNodeRef Params=EntityNode->getChild(0);
							Params->getAttr("SpawnTime",spawntime);
							Params->getAttr("Mass",mass);
							Params->getAttr("Density",density);
							Params->getAttr("Type",type);
							ActivateOutput(pActInfo,EOP_TIME,spawntime);
							ActivateOutput(pActInfo,EOP_MASS,mass);
							ActivateOutput(pActInfo,EOP_DENSITY,density);
							if(type==1)
								ActivateOutput(pActInfo,EOP_RIGID,false);
							if(type==2)
								ActivateOutput(pActInfo,EOP_RIGID,true);
						}
					}
					else
						return;
				}
			}
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
private:
	float spawntime;
	int mass;
	int density;
	int type;
};


//--------GetSpawnData_DestroyableObject--------
class CFlowGetSD_DestroyableObject : public CFlowBaseNode<eNCT_Singleton>
{
	enum INPUTS
	{
		EIP_Index,
	};
	enum OUTPUTS
	{
		EOP_TIME = 0,
		EOP_MASS,
		EOP_DENSITY,
		EOP_RIGID,
		EOP_EXPLODE,
		EOP_DAMAGE,
		EOP_MINRAD,
		EOP_RAD,
		EOP_MINPHYSRAD,
		EOP_PHYSRAD,
		EOP_PRESSURE,
		EOP_HEALTH,
	};
public:
	CFlowGetSD_DestroyableObject(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Index",_HELP("Get system state")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<float>( "SpawnTime", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Mass", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Density", _HELP("Fire rate of this weapon")),
			OutputPortConfig<bool>( "Rigid", _HELP("Fire rate of this weapon")),
			OutputPortConfig<bool>( "Explode", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Damage", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "MinRadius", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Radius", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "MinPhysRadius", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "PhysRadius", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Pressure", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Health", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("SpawnSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case(eFE_Activate):
			{
				if(IsPortActive(pActInfo,EIP_Index))
				{
					SpawnSystem *ts=g_pGame->GetSpawnSystem();
					if(ts->IsFunctionStarted())
					{
						int tindex=GetPortInt(pActInfo,EIP_Index);
						if(tindex>=0)
						{
							XmlNodeRef root= gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
							XmlNodeRef EntityNode=root->getChild(tindex);
							XmlNodeRef Params=EntityNode->getChild(0);
							Params->getAttr("SpawnTime",spawntime);
							Params->getAttr("Mass",mass);
							Params->getAttr("Density",density);
							Params->getAttr("Type",type);
							Params->getAttr("Explode",explode);
							Params->getAttr("Damage",damage);
							Params->getAttr("MinRadius",minradius);
							Params->getAttr("Radius",radius);
							Params->getAttr("MinPhysRadius",minphysradius);
							Params->getAttr("PhysRadius",physradius);
							Params->getAttr("Pressure",pressure);
							Params->getAttr("Health",health);
							ActivateOutput(pActInfo,EOP_TIME,spawntime);
							ActivateOutput(pActInfo,EOP_MASS,mass);
							ActivateOutput(pActInfo,EOP_DENSITY,density);
							if(type==1)
								ActivateOutput(pActInfo,EOP_RIGID,false);
							if(type==2)
								ActivateOutput(pActInfo,EOP_RIGID,true);
							if(explode==1)
								ActivateOutput(pActInfo,EOP_EXPLODE,true);
							if(explode==0)
								ActivateOutput(pActInfo,EOP_EXPLODE,false);
							ActivateOutput(pActInfo,EOP_DAMAGE,damage);
							ActivateOutput(pActInfo,EOP_MINRAD,minphysradius);
							ActivateOutput(pActInfo,EOP_RAD,radius);
							ActivateOutput(pActInfo,EOP_MINPHYSRAD,minphysradius);
							ActivateOutput(pActInfo,EOP_PHYSRAD,physradius);
							ActivateOutput(pActInfo,EOP_PRESSURE,pressure);
							ActivateOutput(pActInfo,EOP_HEALTH,health);
						}
					}
					else
						return;
				}
			}
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
private:
	float spawntime;
	int mass;
	int density;
	int type;
	bool explode;
	int damage;
	float minradius;
	float radius;
	float minphysradius;
	float physradius;
	int pressure;
	int health;
};



//--------GetSpawnData_Item--------
class CFlowGetSD_Item : public CFlowBaseNode<eNCT_Singleton>
{
	enum INPUTS
	{
		EIP_Index,
	};
	enum OUTPUTS
	{
		EOP_TIME = 0,
		EOP_RATE,
		EOP_DAMAGE,
		EOP_CLIPSIZE,
	};
public:
	CFlowGetSD_Item(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Index",_HELP("Get system state")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<float>( "SpawnTime", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "FireRate", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Damage", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "ClipSize", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("SpawnSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case(eFE_Activate):
			{
				if(IsPortActive(pActInfo,EIP_Index))
				{
					SpawnSystem *ts=g_pGame->GetSpawnSystem();
					if(ts->IsFunctionStarted())
					{
						int tindex=GetPortInt(pActInfo,EIP_Index);
						if(tindex>=0)
						{
							XmlNodeRef root= gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
							XmlNodeRef EntityNode=root->getChild(tindex);
							XmlNodeRef Params=EntityNode->getChild(0);
							XmlString weaponClass;
							Params->getAttr("Name",weaponClass);
							const char *itemfile=g_pGame->GetIGameFramework()->GetIItemSystem()->GetItemParamsDescriptionFile(weaponClass);
							XmlNodeRef item_root = gEnv->pSystem->LoadXmlFromFile(itemfile);
							XmlNodeRef firemodes;
							XmlNodeRef defaultfiremode;
							XmlNodeRef fire;
							if(weaponClass == "Rifle")
							{
								firemodes=item_root->findChild("firemodes");
								defaultfiremode=firemodes->getChild(0);
								fire=defaultfiremode->getChild(0);
								fire->getChild(2)->getAttr("value",firerate);
								fire->getChild(3)->getAttr("value",damage);
								fire->getChild(8)->getAttr("value",clipsize);
							}
							Params->getAttr("SpawnTime",spawntime);
							ActivateOutput(pActInfo,EOP_TIME,spawntime);
							ActivateOutput(pActInfo,EOP_RATE,firerate);
							ActivateOutput(pActInfo,EOP_DAMAGE,damage);
							ActivateOutput(pActInfo,EOP_CLIPSIZE,clipsize);
						}
					}
					else
						return;
				}
			}
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
private:
	float spawntime;
	int firerate;
	int damage;
	int clipsize;
};

//--------GetSpawnData_Vehicle--------
class CFlowGetSD_Vehicle : public CFlowBaseNode<eNCT_Singleton>
{
	enum INPUTS
	{
		EIP_Index,
	};
	enum OUTPUTS
	{
		EOP_TIME = 0,
		EOP_MASS,
		EOP_DENSITY,
		EOP_ACCE,
		EOP_DECCE,
		EOP_TOPSPEED,
	};
public:
	CFlowGetSD_Vehicle(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Index",_HELP("Get system state")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<float>( "SpawnTime", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Mass", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Density", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Acceleration", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Decceletation", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "TopSpeed", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("SpawnSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case(eFE_Activate):
			{
				if(IsPortActive(pActInfo,EIP_Index))
				{
					SpawnSystem *ts=g_pGame->GetSpawnSystem();
					if(ts->IsFunctionStarted())
					{
						int tindex=GetPortInt(pActInfo,EIP_Index);
						if(tindex>=0)
						{
							XmlNodeRef root= gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
							XmlNodeRef EntityNode=root->getChild(tindex);
							XmlNodeRef Params=EntityNode->getChild(0);
							XmlString VehicleClass;
							Params->getAttr("Name",VehicleClass);
							Params->getAttr("Mass",mass);
							Params->getAttr("Density",density);
							XmlNodeRef vehicle_root=gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Vehicles/Implementations/Xml/HMMWV.xml");
							XmlNodeRef MovementParams=vehicle_root->findChild("MovementParams");
							if(VehicleClass == "HMMWV")
							{
								XmlNodeRef ArcadeWheeledParams=MovementParams->findChild("ArcadeWheeled");
								XmlNodeRef HandlingParams=ArcadeWheeledParams->findChild("Handling");
								XmlNodeRef PowerParams=HandlingParams->findChild("Power");
								XmlNodeRef FrictionPrams=HandlingParams->findChild("Friction");
								PowerParams->getAttr("acceleration",acceleration);
								PowerParams->getAttr("decceleration",decceleration);
								PowerParams->getAttr("topSpeed",topSpeed);
							}
							Params->getAttr("SpawnTime",spawntime);
							ActivateOutput(pActInfo,EOP_TIME,spawntime);
							ActivateOutput(pActInfo,EOP_MASS,mass);
							ActivateOutput(pActInfo,EOP_DENSITY,density);
							ActivateOutput(pActInfo,EOP_ACCE,acceleration);
							ActivateOutput(pActInfo,EOP_DECCE,decceleration);
							ActivateOutput(pActInfo,EOP_TOPSPEED,topSpeed);
						}
					}
					else
						return;
				}
			}
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
private:
	float spawntime;
	int mass;
	int density;
	float acceleration;
	float decceleration;
	float topSpeed;
};


//--------GetSpawnData_BasicEntity--------
class CFlowGetSD_AI : public CFlowBaseNode<eNCT_Singleton>
{
	enum INPUTS
	{
		EIP_Index,
	};
	enum OUTPUTS
	{
		EOP_TIME = 0,
		EOP_MASS,
		EOP_DENSITY,
		EOP_LEVEL,
	};
public:
	CFlowGetSD_AI(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Index",_HELP("Get system state")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<float>( "SpawnTime", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Mass", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Density", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Level", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("SpawnSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case(eFE_Activate):
			{
				if(IsPortActive(pActInfo,EIP_Index))
				{
					SpawnSystem *ts=g_pGame->GetSpawnSystem();
					if(ts->IsFunctionStarted())
					{
						int tindex=GetPortInt(pActInfo,EIP_Index);
						if(tindex>=0)
						{
							XmlNodeRef root= gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
							XmlNodeRef EntityNode=root->getChild(tindex);
							XmlNodeRef Params=EntityNode->getChild(0);
							Params->getAttr("SpawnTime",spawntime);
							Params->getAttr("Mass",mass);
							Params->getAttr("Density",density);
							Params->getAttr("Level",level);
							ActivateOutput(pActInfo,EOP_TIME,spawntime);
							ActivateOutput(pActInfo,EOP_MASS,mass);
							ActivateOutput(pActInfo,EOP_DENSITY,density);
							ActivateOutput(pActInfo,EOP_LEVEL,level);
						}
					}
					else
						return;
				}
			}
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
private:
	float spawntime;
	int mass;
	int density;
	int level;
};



//--------GetSpawnData_Light--------
class CFlowGetSD_Light : public CFlowBaseNode<eNCT_Singleton>
{
	enum INPUTS
	{
		EIP_Index,
	};
	enum OUTPUTS
	{
		EOP_TIME = 0,
		EOP_MASS,
		EOP_DENSITY,
		EOP_RIGID,
		EOP_EXPLODE,
		EOP_DAMAGE,
		EOP_MINRAD,
		EOP_RAD,
		EOP_MINPHYSRAD,
		EOP_PHYSRAD,
		EOP_PRESSURE,
		EOP_HEALTH,
		EOP_LIGHTRAD,
		EOP_DIFFUSE,
		EOP_SPECULAR,
		EOP_HDR,
	};
public:
	CFlowGetSD_Light(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Index",_HELP("Get system state")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<float>( "SpawnTime", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Mass", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Density", _HELP("Fire rate of this weapon")),
			OutputPortConfig<bool>( "Rigid", _HELP("Fire rate of this weapon")),
			OutputPortConfig<bool>( "Explode", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Damage", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "MinRadius", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Radius", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "MinPhysRadius", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "PhysRadius", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Pressure", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Health", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "LightRadius", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Diffuse", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Specular", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "HDR", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("SpawnSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case(eFE_Activate):
			{
				if(IsPortActive(pActInfo,EIP_Index))
				{
					SpawnSystem *ts=g_pGame->GetSpawnSystem();
					if(ts->IsFunctionStarted())
					{
						int tindex=GetPortInt(pActInfo,EIP_Index);
						if(tindex>=0)
						{
							XmlNodeRef root= gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
							XmlNodeRef EntityNode=root->getChild(tindex);
							XmlNodeRef Params=EntityNode->getChild(0);
							Params->getAttr("SpawnTime",spawntime);
							Params->getAttr("Mass",mass);
							Params->getAttr("Density",density);
							Params->getAttr("Type",type);
							Params->getAttr("Explode",explode);
							Params->getAttr("Damage",damage);
							Params->getAttr("MinRadius",minradius);
							Params->getAttr("Radius",radius);
							Params->getAttr("MinPhysRadius",minphysradius);
							Params->getAttr("PhysRadius",physradius);
							Params->getAttr("Pressure",pressure);
							Params->getAttr("Health",health);
							Params->getAttr("Light_Radius",lightradius);
							Params->getAttr("DiffuseMultiplier",diffuse);
							Params->getAttr("SpecularMultiplier",specular);
							Params->getAttr("HDRDynamic",hdr);
							ActivateOutput(pActInfo,EOP_TIME,spawntime);
							ActivateOutput(pActInfo,EOP_MASS,mass);
							ActivateOutput(pActInfo,EOP_DENSITY,density);
							if(type==1)
								ActivateOutput(pActInfo,EOP_RIGID,false);
							if(type==2)
								ActivateOutput(pActInfo,EOP_RIGID,true);
							if(explode==1)
								ActivateOutput(pActInfo,EOP_EXPLODE,true);
							if(explode==0)
								ActivateOutput(pActInfo,EOP_EXPLODE,false);
							ActivateOutput(pActInfo,EOP_DAMAGE,damage);
							ActivateOutput(pActInfo,EOP_MINRAD,minphysradius);
							ActivateOutput(pActInfo,EOP_RAD,radius);
							ActivateOutput(pActInfo,EOP_MINPHYSRAD,minphysradius);
							ActivateOutput(pActInfo,EOP_PHYSRAD,physradius);
							ActivateOutput(pActInfo,EOP_PRESSURE,pressure);
							ActivateOutput(pActInfo,EOP_HEALTH,health);
							ActivateOutput(pActInfo,EOP_LIGHTRAD,lightradius);
							ActivateOutput(pActInfo,EOP_DIFFUSE,diffuse);
							ActivateOutput(pActInfo,EOP_SPECULAR,specular);
							ActivateOutput(pActInfo,EOP_HDR,hdr);
						}
					}
					else
						return;
				}
			}
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
private:
	float spawntime;
	int mass;
	int density;
	int type;
	bool explode;
	int damage;
	float minradius;
	float radius;
	float minphysradius;
	float physradius;
	int pressure;
	int health;
	float lightradius;
	float diffuse;
	float specular;
	float hdr;
};


//--------GetSpawnData_Ladder--------
class CFlowGetSD_Ladder : public CFlowBaseNode<eNCT_Singleton>
{
	enum INPUTS
	{
		EIP_Index,
	};
	enum OUTPUTS
	{
		EOP_TIME = 0,
		EOP_HEIGHT,
	};
public:
	CFlowGetSD_Ladder(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Index",_HELP("Get system state")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<float>( "SpawnTime", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Height", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("SpawnSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case(eFE_Activate):
			{
				if(IsPortActive(pActInfo,EIP_Index))
				{
					SpawnSystem *ts=g_pGame->GetSpawnSystem();
					if(ts->IsFunctionStarted())
					{
						int tindex=GetPortInt(pActInfo,EIP_Index);
						if(tindex>=0)
						{
							XmlNodeRef root= gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
							XmlNodeRef EntityNode=root->getChild(tindex);
							XmlNodeRef Params=EntityNode->getChild(0);
							Params->getAttr("SpawnTime",spawntime);
							Params->getAttr("Height",height);
							ActivateOutput(pActInfo,EOP_TIME,spawntime);
							ActivateOutput(pActInfo,EOP_HEIGHT,height);
						}
					}
					else
						return;
				}
			}
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
private:
	float spawntime;
	float height;
};



//--------Spawn_AddFavorite--------
class CFlowS_AddFavorite : public CFlowBaseNode<eNCT_Singleton>
{
	enum INPUTS
	{
		EIP_Index,
	};
	enum OUTPUTS
	{

	};
public:
	CFlowS_AddFavorite(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Index",_HELP("Get system state")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("SpawnSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case(eFE_Activate):
			{
				if(IsPortActive(pActInfo,EIP_Index))
				{
					SpawnSystem *ts=g_pGame->GetSpawnSystem();
					if(ts->IsFunctionStarted())
					{
						int tindex=GetPortInt(pActInfo,EIP_Index);
						if(tindex>=0)
						{
							XmlNodeRef root= gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
							XmlNodeRef EntityNode=root->getChild(tindex);
							XmlNodeRef Params=EntityNode->getChild(0);
							Params->getAttr("Favorite",favorite);
							if(favorite)
								Params->setAttr("Favorite",0);
							else
								Params->setAttr("Favorite",1);
							root->saveToFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
						}
					}
					else
						return;
				}
			}
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
private:
	float spawntime;
	bool favorite;
};





/*
--------Modify_BasicEntity--------
class CFlowModifyBasicEntity : public CFlowBaseNode<eNCT_Instanced>, public IGameFrameworkListener
{
	enum INPUTS
	{
		EIP_ENABLE,
		EIP_DISABLE
	};
	enum OUTPUTS
	{
		EOP_MASS,
		EOP_DENSITY,
		EOP_RIGID
	};
public:
	CFlowModifyBasicEntity(SActivationInfo * pActInfo){}
	IFlowNodePtr Clone( SActivationInfo *pActInfo )
	{
		return new CFlowModifyBasicEntity(pActInfo);
	}

	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[]=
		{
			InputPortConfig_Void("Enable",_HELP("Call this node")),
			InputPortConfig_Void("Disable",_HELP("Call this node")),
			{0}
		};
		static const SOutputPortConfig out_ports[]=
		{
			OutputPortConfig<float>( "Mass", _HELP("Mass of modified entity")),
			OutputPortConfig<float>( "Density", _HELP("Mass of modified entity")),
			OutputPortConfig<bool>( "Rigid", _HELP("Mass of modified entity")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("Modify--BasicEntity");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				pm=g_pGame->GetModifySystem();
				m_actInfo=*pActInfo;
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				break;
			}
		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_ENABLE))
			{
				gEnv->pGame->GetIGameFramework()->RegisterListener(this, "CFlowModifyBasicEntity",FRAMEWORKLISTENERPRIORITY_GAME);
			}
			if (IsPortActive(pActInfo, EIP_DISABLE))
			{
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
			}
			break;
		}
	}
	// IGameFrameworkListener
	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) {}
	virtual void OnPostUpdate(float fDelta)
	{
		if(pm)
		{
			ActivateOutput(&m_actInfo,EOP_MASS,pm->MP.BPP.mass);
			ActivateOutput(&m_actInfo,EOP_DENSITY,pm->MP.BPP.density);
			ActivateOutput(&m_actInfo,EOP_RIGID,pm->MP.BPP.rigid);
		}
	}
private:
	ModifySystem *pm;
	SActivationInfo m_actInfo;
};




--------Modify_DestroyableObject--------
class CFlowModifyDestroyableObject : public CFlowBaseNode<eNCT_Instanced>, public IGameFrameworkListener
{
	enum INPUTS
	{
		EIP_ENABLE,
		EIP_DISABLE
	};
	enum OUTPUTS
	{
		EOP_MASS,
		EOP_DENSITY,
		EOP_RIGID,
		EOP_EXPLODE,
		EOP_EFFECTSCALE,
		EOP_MINRADIUS,
		EOP_RADIUS,
		EOP_MINPHYSRADIUS,
		EOP_PRESSURE,
		EOP_DAMAGE,
		EOP_INVULNERABLE,
		EOP_HEALTH
	};
public:
	CFlowModifyDestroyableObject(SActivationInfo * pActInfo){}
	IFlowNodePtr Clone( SActivationInfo *pActInfo )
	{
		return new CFlowModifyBasicEntity(pActInfo);
	}

	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[]=
		{
			InputPortConfig_Void("Enable",_HELP("Call this node")),
			InputPortConfig_Void("Disable",_HELP("Call this node")),
			{0}
		};
		static const SOutputPortConfig out_ports[]=
		{
			OutputPortConfig<float>( "Mass", _HELP("Mass of modified entity")),
			OutputPortConfig<float>( "Density", _HELP("Mass of modified entity")),
			OutputPortConfig<bool>( "Rigid", _HELP("Mass of modified entity")),
			OutputPortConfig<bool>( "Explode", _HELP("If this entity will explode")),
			OutputPortConfig<float>( "EffectScale", _HELP("Effect scale of the explosion")),
			OutputPortConfig<float>( "MinRadius", _HELP("Min explsion radius")),
			OutputPortConfig<float>( "Radius", _HELP("Explosion radius")),
			OutputPortConfig<float>( "MinPhysRadius", _HELP("Min physics radius of the explosion")),
			OutputPortConfig<float>( "Pressure", _HELP("Explosion pressure")),
			OutputPortConfig<float>( "Damage", _HELP("Explosion Damage")),
			OutputPortConfig<bool>( "Invulnerable", _HELP("If this entity is invulnerable")),
			OutputPortConfig<float>( "Health", _HELP("Health of this entity")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("Modify--DestroyableObject");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				pm=g_pGame->GetModifySystem();
				m_actInfo=*pActInfo;
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				break;
			}
		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_ENABLE))
			{
				gEnv->pGame->GetIGameFramework()->RegisterListener(this, "CFlowModifyDestroyableObject",FRAMEWORKLISTENERPRIORITY_GAME);
			}
			if (IsPortActive(pActInfo, EIP_DISABLE))
			{
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
			}
			break;
		}
	}
	// IGameFrameworkListener
	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) {}
	virtual void OnPostUpdate(float fDelta)
	{
		if(pm)
		{
			ActivateOutput(&m_actInfo,EOP_MASS,pm->MP.BPP.mass);
			ActivateOutput(&m_actInfo,EOP_DENSITY,pm->MP.BPP.density);
			ActivateOutput(&m_actInfo,EOP_RIGID,pm->MP.BPP.rigid);

			ActivateOutput(&m_actInfo,EOP_EXPLODE,pm->MP.EP.explode);
			ActivateOutput(&m_actInfo,EOP_EFFECTSCALE,pm->MP.EP.effectscale);
			ActivateOutput(&m_actInfo,EOP_MINRADIUS,pm->MP.EP.minradius);
			ActivateOutput(&m_actInfo,EOP_RADIUS,pm->MP.EP.radius);
			ActivateOutput(&m_actInfo,EOP_MINPHYSRADIUS,pm->MP.EP.minphysradius);
			ActivateOutput(&m_actInfo,EOP_PRESSURE,pm->MP.EP.pressure);
			ActivateOutput(&m_actInfo,EOP_DAMAGE,pm->MP.EP.damage);
			ActivateOutput(&m_actInfo,EOP_INVULNERABLE,pm->MP.EP.invulnerable);
			ActivateOutput(&m_actInfo,EOP_HEALTH,pm->MP.EP.health);
		}
	}
private:
	ModifySystem *pm;
	SActivationInfo m_actInfo;
};

--------Modify_LandVehicle--------
class CFlowModifyLandVehicle : public CFlowBaseNode<eNCT_Instanced>, public IGameFrameworkListener
{
	enum INPUTS
	{
		EIP_ENABLE,
		EIP_DISABLE
	};
	enum OUTPUTS
	{
		EOP_MASS,
		EOP_RIGID,
		EOP_ACCELERATION,
		EOP_DECCELERATION,
		EOP_TOPSPEED,
		EOP_REVERSESPEED,
		EOP_BACKFRICTION,
		EOP_FRONTFRICTION,
		EOP_VELOCITY,
		EOP_DEGREE,
		EOP_DEFAULT
	};
public:
	CFlowModifyLandVehicle(SActivationInfo * pActInfo){}
	IFlowNodePtr Clone( SActivationInfo *pActInfo )
	{
		return new CFlowModifyBasicEntity(pActInfo);
	}

	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[]=
		{
			InputPortConfig_Void("Enable",_HELP("Call this node")),
			InputPortConfig_Void("Disable",_HELP("Call this node")),
			{0}
		};
		static const SOutputPortConfig out_ports[]=
		{
			OutputPortConfig<float>( "Mass", _HELP("Mass of modified entity")),
			OutputPortConfig<bool>( "Rigid", _HELP("Mass of modified entity")),
			OutputPortConfig<float>( "Acceleration", _HELP("If this entity will explode")),
			OutputPortConfig<float>( "Decceleration", _HELP("Explode delay")),
			OutputPortConfig<float>( "TopSpeed", _HELP("Effect scale of the explosion")),
			OutputPortConfig<float>( "ReverseSpeed", _HELP("Min explsion radius")),
			OutputPortConfig<float>( "BackFriction", _HELP("Explode delay")),
			OutputPortConfig<float>( "FrontFriction", _HELP("Effect scale of the explosion")),
			OutputPortConfig<float>( "Velocity", _HELP("Min explsion radius")),
			OutputPortConfig<float>( "Degree", _HELP("Min explsion radius")),
			OutputPortConfig<float>( "Default", _HELP("Min explsion radius")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("Modify--DestroyableObject");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				pm=g_pGame->GetModifySystem();
				m_actInfo=*pActInfo;
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				break;
			}
		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_ENABLE))
			{
				gEnv->pGame->GetIGameFramework()->RegisterListener(this, "CFlowModifyLandVehicle",FRAMEWORKLISTENERPRIORITY_GAME);
			}
			if (IsPortActive(pActInfo, EIP_DISABLE))
			{
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
			}
			break;
		}
	}
	// IGameFrameworkListener
	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) {}
	virtual void OnPostUpdate(float fDelta)
	{
		if(pm)
		{
			ActivateOutput(&m_actInfo,EOP_MASS,pm->MP.BPP.mass);
			ActivateOutput(&m_actInfo,EOP_RIGID,pm->MP.BPP.rigid);
			ActivateOutput(&m_actInfo,EOP_ACCELERATION,pm->MP.LVP.acceleration);
			ActivateOutput(&m_actInfo,EOP_DECCELERATION,pm->MP.LVP.decceleration);
			ActivateOutput(&m_actInfo,EOP_TOPSPEED,pm->MP.LVP.topSpeed);
			ActivateOutput(&m_actInfo,EOP_REVERSESPEED,pm->MP.LVP.reverseSpeed);
			ActivateOutput(&m_actInfo,EOP_BACKFRICTION,pm->MP.LVP.backfriction);
			ActivateOutput(&m_actInfo,EOP_FRONTFRICTION,pm->MP.LVP.frontfriction);
			ActivateOutput(&m_actInfo,EOP_VELOCITY,pm->MP.LVP.velocitymultiplier);
			ActivateOutput(&m_actInfo,EOP_DEGREE,pm->MP.LVP.degree);
			ActivateOutput(&m_actInfo,EOP_DEFAULT,pm->MP.defaultxml);
		}
	}
private:
	ModifySystem *pm;
	SActivationInfo m_actInfo;
};

--------Modify_Light--------
class CFlowModifyLight : public CFlowBaseNode<eNCT_Instanced>, public IGameFrameworkListener
{
	enum INPUTS
	{
		EIP_ENABLE,
		EIP_DISABLE
	};
	enum OUTPUTS
	{
		EOP_MASS,
		EOP_DENSITY,
		EOP_RIGID,
		EOP_EXPLODE,
		EOP_EFFECTSCALE,
		EOP_MINRADIUS,
		EOP_RADIUS,
		EOP_MINPHYSRADIUS,
		EOP_PRESSURE,
		EOP_DAMAGE,
		EOP_INVULNERABLE,
		EOP_HEALTH,
		EOP_TURNON,
		EOP_LIGHTRADIUS,
		EOP_LIGHTSTYLE,
		EOP_DIFFUSE,
		EOP_SPECULAR,
		EOP_HDR
	};
public:
	CFlowModifyLight(SActivationInfo * pActInfo){}
	IFlowNodePtr Clone( SActivationInfo *pActInfo )
	{
		return new CFlowModifyBasicEntity(pActInfo);
	}

	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[]=
		{
			InputPortConfig_Void("Enable",_HELP("Call this node")),
			InputPortConfig_Void("Disable",_HELP("Call this node")),
			{0}
		};
		static const SOutputPortConfig out_ports[]=
		{
			OutputPortConfig<float>( "Mass", _HELP("Mass of modified entity")),
			OutputPortConfig<float>( "Density", _HELP("Mass of modified entity")),
			OutputPortConfig<bool>( "Rigid", _HELP("Mass of modified entity")),
			OutputPortConfig<bool>( "Explode", _HELP("If this entity will explode")),
			OutputPortConfig<float>( "EffectScale", _HELP("Effect scale of the explosion")),
			OutputPortConfig<float>( "MinRadius", _HELP("Min explsion radius")),
			OutputPortConfig<float>( "Radius", _HELP("Explosion radius")),
			OutputPortConfig<float>( "MinPhysRadius", _HELP("Min physics radius of the explosion")),
			OutputPortConfig<float>( "Pressure", _HELP("Explosion pressure")),
			OutputPortConfig<float>( "Damage", _HELP("Explosion Damage")),
			OutputPortConfig<bool>( "Invulnerable", _HELP("If this entity is invulnerable")),
			OutputPortConfig<float>( "Health", _HELP("Health of this entity")),
			OutputPortConfig<bool>( "TurnOn", _HELP("If this entity is invulnerable")),
			OutputPortConfig<float>( "LightRadius", _HELP("Min explsion radius")),
			OutputPortConfig<float>( "LightStyle", _HELP("Explosion radius")),
			OutputPortConfig<float>( "Diffuse", _HELP("Min physics radius of the explosion")),
			OutputPortConfig<float>( "Specular", _HELP("Explosion pressure")),
			OutputPortConfig<float>( "HDR", _HELP("Explosion Damage")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("Modify--DestroyableObject");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				pm=g_pGame->GetModifySystem();
				m_actInfo=*pActInfo;
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				break;
			}
		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_ENABLE))
			{
				gEnv->pGame->GetIGameFramework()->RegisterListener(this, "CFlowModifyLight",FRAMEWORKLISTENERPRIORITY_GAME);
			}
			if (IsPortActive(pActInfo, EIP_DISABLE))
			{
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
			}
			break;
		}
	}
	// IGameFrameworkListener
	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) {}
	virtual void OnPostUpdate(float fDelta)
	{
		if(pm)
		{
			ActivateOutput(&m_actInfo,EOP_MASS,pm->MP.BPP.mass);
			ActivateOutput(&m_actInfo,EOP_DENSITY,pm->MP.BPP.density);
			ActivateOutput(&m_actInfo,EOP_RIGID,pm->MP.BPP.rigid);

			ActivateOutput(&m_actInfo,EOP_EXPLODE,pm->MP.EP.explode);
			ActivateOutput(&m_actInfo,EOP_EFFECTSCALE,pm->MP.EP.effectscale);
			ActivateOutput(&m_actInfo,EOP_MINRADIUS,pm->MP.EP.minradius);
			ActivateOutput(&m_actInfo,EOP_RADIUS,pm->MP.EP.radius);
			ActivateOutput(&m_actInfo,EOP_MINPHYSRADIUS,pm->MP.EP.minphysradius);
			ActivateOutput(&m_actInfo,EOP_PRESSURE,pm->MP.EP.pressure);
			ActivateOutput(&m_actInfo,EOP_DAMAGE,pm->MP.EP.damage);
			ActivateOutput(&m_actInfo,EOP_INVULNERABLE,pm->MP.EP.invulnerable);
			ActivateOutput(&m_actInfo,EOP_HEALTH,pm->MP.EP.health);

			ActivateOutput(&m_actInfo,EOP_TURNON,pm->MP.LP.turn_on);
			ActivateOutput(&m_actInfo,EOP_LIGHTRADIUS,pm->MP.LP.lightradius);
			ActivateOutput(&m_actInfo,EOP_LIGHTSTYLE,pm->MP.LP.lightstyle);
			ActivateOutput(&m_actInfo,EOP_DIFFUSE,pm->MP.LP.diffusemultiplier);
			ActivateOutput(&m_actInfo,EOP_SPECULAR,pm->MP.LP.specularmultiplier);
			ActivateOutput(&m_actInfo,EOP_HDR,pm->MP.LP.HDRDynamic);
		}
	}
private:
	ModifySystem *pm;
	SActivationInfo m_actInfo;
};


--------Modify_StandardWeapon--------
class CFlowModifyStandardWeapon : public CFlowBaseNode<eNCT_Instanced>, public IGameFrameworkListener
{
	enum INPUTS
	{
		EIP_ENABLE,
		EIP_DISABLE
	};
	enum OUTPUTS
	{
		EOP_FIRERATE,
		EOP_PLAYERDAMAGE,
		EOP_AIDAMAGE,
		EOP_CLIPSIZE
	};
public:
	CFlowModifyStandardWeapon(SActivationInfo * pActInfo){}
	IFlowNodePtr Clone( SActivationInfo *pActInfo )
	{
		return new CFlowModifyBasicEntity(pActInfo);
	}

	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[]=
		{
			InputPortConfig_Void("Enable",_HELP("Call this node")),
			InputPortConfig_Void("Disable",_HELP("Call this node")),
			{0}
		};
		static const SOutputPortConfig out_ports[]=
		{
			OutputPortConfig<float>( "FireRate", _HELP("Fire rate of this weapon")),
			OutputPortConfig<bool>( "PlayerDamage", _HELP("Damage of this weapon(player vs AI)")),
			OutputPortConfig<float>( "AIDamage", _HELP("Damage of this weapon(AI vs player)")),
			OutputPortConfig<float>( "ClipSize", _HELP("Clip size of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("Modify--DestroyableObject");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				pm=g_pGame->GetModifySystem();
				m_actInfo=*pActInfo;
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				break;
			}
		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_ENABLE))
			{
				gEnv->pGame->GetIGameFramework()->RegisterListener(this, "CFlowModifyLandVehicle",FRAMEWORKLISTENERPRIORITY_GAME);
			}
			if (IsPortActive(pActInfo, EIP_DISABLE))
			{
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
			}
			break;
		}
	}
	// IGameFrameworkListener
	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) {}
	virtual void OnPostUpdate(float fDelta)
	{
		if(pm)
		{
			ActivateOutput(&m_actInfo,EOP_FIRERATE,pm->MP.BWP.firerate);
			ActivateOutput(&m_actInfo,EOP_PLAYERDAMAGE,pm->MP.BWP.player_damage);
			ActivateOutput(&m_actInfo,EOP_AIDAMAGE,pm->MP.BWP.ai_damage);
			ActivateOutput(&m_actInfo,EOP_CLIPSIZE,pm->MP.BWP.clip_size);
		}
	}
private:
	ModifySystem *pm;
	SActivationInfo m_actInfo;
};*/



//--------IsFacingBody--------
class CFlowLootBody : public CFlowBaseNode<eNCT_Singleton> ,public IGameFrameworkListener
{
	enum INPUTS
	{
		EIP_ENABLE=0,
		EIP_DISABLE,
	};
	enum OUTPUTS
	{
		EOP_ENTITY,
		EOP_ISBODY,
		EOP_PROGRESS,
		EOP_WIDTH,
		EOP_HEIGHT,
	};
public:
	CFlowLootBody(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Enable",_HELP("Get XML")),
			InputPortConfig_AnyType("Disable",_HELP("Get XML")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<EntityId>( "Entity", _HELP("Fire rate of this weapon")),
			OutputPortConfig<bool>( "IsBody", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Progress", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Width", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Height", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("LootSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
			case eFE_Initialize:
				{
					m_actInfo=*pActInfo;
					gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
					break;
				}
			case eFE_Activate:
				if (IsPortActive(pActInfo, EIP_ENABLE))
				{
					gEnv->pGame->GetIGameFramework()->RegisterListener(this, "CFlowLootBody",FRAMEWORKLISTENERPRIORITY_GAME);
					break;
				}
				if (IsPortActive(pActInfo, EIP_DISABLE))
				{
					gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
					break;
				}
		}
	}

	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) {}
	virtual void OnPostUpdate(float fDelta)
	{
		if(g_pGame->GetLootSystem()->Isbody())
			ActivateOutput(&m_actInfo,EOP_ISBODY,true);
		else
			ActivateOutput(&m_actInfo,EOP_ISBODY,false);
		IEntity* pEntity=g_pGame->GetLootSystem()->getLootEntity();
		if(pEntity)
			ActivateOutput(&m_actInfo,EOP_ENTITY,pEntity->GetId());
		ActivateOutput(&m_actInfo,EOP_WIDTH,gEnv->pRenderer->GetWidth());
		ActivateOutput(&m_actInfo,EOP_HEIGHT,gEnv->pRenderer->GetHeight());
		pEntity=NULL;
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

private:
	SActivationInfo m_actInfo;
};


//--------LootData--------
class CFlowLootEntity : public CFlowBaseNode<eNCT_Singleton> ,public IGameFrameworkListener
{
	enum INPUTS
	{
		EIP_ENABLE=0,
		EIP_DISABLE,
	};
	enum OUTPUTS
	{
		EOP_MONEY,
		EOP_STABILITY,
		EOP_HEALTH,
		EOP_AMMO,
		EOP_AMMOCLASS,
		EOP_DATA,
		EOP_ONCALL,
	};
public:
	CFlowLootEntity(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Enable",_HELP("Get XML")),
			InputPortConfig_AnyType("Disable",_HELP("Get XML")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<int>( "Money", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Stability", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Health", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Ammo", _HELP("Fire rate of this weapon")),
			OutputPortConfig<string>( "AmmoClass", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "Data", _HELP("Fire rate of this weapon")),
			OutputPortConfig<int>( "OnCall", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("LootSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case eFE_Initialize:
			{
				m_actInfo=*pActInfo;
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				break;
			}
		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_ENABLE))
			{
				gEnv->pGame->GetIGameFramework()->RegisterListener(this, "CFlowLootBody",FRAMEWORKLISTENERPRIORITY_GAME);
				break;
			}
			if (IsPortActive(pActInfo, EIP_DISABLE))
			{
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				break;
			}
		}
	}

	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) {}
	virtual void OnPostUpdate(float fDelta)
	{
		pEntity=g_pGame->GetLootSystem()->getLootEntity();
		if(pEntity && g_pGame->GetLootSystem()->isAfterScan())
		{	
			tDrops=g_pGame->GetLootSystem()->Drops;
			if(tDrops.count(pEntity->GetId()))
			{
				money_num=tDrops[pEntity->GetId()].money_num;
				stability_num=tDrops[pEntity->GetId()].stability_num;
				health_num=tDrops[pEntity->GetId()].health_num;
				ammo_num=tDrops[pEntity->GetId()].ammo_num;
				AmmoClass=tDrops[pEntity->GetId()].AmmoClass;
				data_num=tDrops[pEntity->GetId()].data_num;
				if(money_num > 0)
					ActivateOutput(&m_actInfo,EOP_MONEY,money_num);
				if(stability_num > 0)
					ActivateOutput(&m_actInfo,EOP_STABILITY,stability_num);
				if(health_num > 0)
					ActivateOutput(&m_actInfo,EOP_HEALTH,health_num);
				if(ammo_num > 0)
				{
					ActivateOutput(&m_actInfo,EOP_AMMO,ammo_num);
					ActivateOutput(&m_actInfo,EOP_AMMOCLASS,(string)AmmoClass->GetName());
				}
				if(data_num > 0)
					ActivateOutput(&m_actInfo,EOP_DATA,data_num);
				ActivateOutput(&m_actInfo,EOP_ONCALL,data_num);
			}
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

private:
	int money_num;
	float health_num;
	int stability_num;
	IEntityClass *AmmoClass;
	int ammo_num;
	int data_num;
	IEntity *pEntity;
	std::map<EntityId,DropParams> tDrops;
	SActivationInfo m_actInfo;
};

//--------DrawLockBox--------
class CFlowDrawLock : public CFlowBaseNode<eNCT_Singleton> ,public IGameFrameworkListener
{
	enum INPUTS
	{
		EIP_ENABLE=0,
		EIP_DISABLE,
	};
	enum OUTPUTS
	{
		EOP_LUX,
		EOP_LUY,
		EOP_LDX,
		EOP_LDY,
		EOP_RUX,
		EOP_RUY,
		EOP_RDX,
		EOP_RDY,
		EOP_WIDTH,
		EOP_HEIGHT,
		EOP_NOTHING,
	};
public:
	CFlowDrawLock(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Enable",_HELP("Get XML")),
			InputPortConfig_AnyType("Disable",_HELP("Get XML")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<float>( "Left_up_x", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Left_up_y", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Left_down_x", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Left_down_y", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Right_up_x", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Right_up_y", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Right_down_x", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Right_down_y", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Width", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Height", _HELP("Fire rate of this weapon")),
			OutputPortConfig<bool>( "noTarget", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("LootSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case eFE_Initialize:
			{
				m_actInfo=*pActInfo;
				brc=g_pGame->GetBasicRayCast();
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				break;
			}
		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_ENABLE))
			{
				gEnv->pGame->GetIGameFramework()->RegisterListener(this, "CFlowLootBody",FRAMEWORKLISTENERPRIORITY_GAME);
				break;
			}
			if (IsPortActive(pActInfo, EIP_DISABLE))
			{
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				break;
			}
		}
	}

	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) {}
	virtual void OnPostUpdate(float fDelta)
	{
		if(!brc->GetEntity() || g_pGame->GetConnectMode()->GetMode() != eConnectMode::eCMode::Deep)
			ActivateOutput(&m_actInfo,EOP_NOTHING,true);
		else if(brc->GetEntity())
		{
			ActivateOutput(&m_actInfo,EOP_LUX,brc->GetScreen_Left_up().x);
			ActivateOutput(&m_actInfo,EOP_LUY,brc->GetScreen_Left_up().y);
			ActivateOutput(&m_actInfo,EOP_LDX,brc->GetScreen_Left_down().x);
			ActivateOutput(&m_actInfo,EOP_LDY,brc->GetScreen_Left_down().y);
			ActivateOutput(&m_actInfo,EOP_RUX,brc->GetScreen_Right_up().x);
			ActivateOutput(&m_actInfo,EOP_RUY,brc->GetScreen_Right_up().y);
			ActivateOutput(&m_actInfo,EOP_RDX,brc->GetScreen_Right_down().x);
			ActivateOutput(&m_actInfo,EOP_RDY,brc->GetScreen_Right_down().y);
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

private:
	BasicRayCast *brc;
	SActivationInfo m_actInfo;
};



//--------ModuleState--------
class CFlowModuleState : public CFlowBaseNode<eNCT_Singleton> ,public IGameFrameworkListener
{
	enum INPUTS
	{
		EIP_ENABLE=0,
		EIP_DISABLE,
	};
	enum OUTPUTS
	{
		EOP_TYPE1,
		EOP_TIME1,
		EOP_MAXTIME1,
		EOP_SELECTED1,
		EOP_TYPE2,
		EOP_TIME2,
		EOP_MAXTIME2,
		EOP_SELECTED2,
		EOP_TYPE3,
		EOP_TIME3,
		EOP_MAXTIME3,
		EOP_SELECTED3,
	};
public:
	CFlowModuleState(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Enable",_HELP("Get XML")),
			InputPortConfig_AnyType("Disable",_HELP("Get XML")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<string>( "Type_1", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Timer_1", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Cd_1", _HELP("Fire rate of this weapon")),
			OutputPortConfig<bool>( "Selected_1", _HELP("Fire rate of this weapon")),
			OutputPortConfig<string>( "Type_2", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Timer_2", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Cd_2", _HELP("Fire rate of this weapon")),
			OutputPortConfig<bool>( "Selected_", _HELP("Fire rate of this weapon")),
			OutputPortConfig<string>( "Type_3", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Timer_3", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Cd_3", _HELP("Fire rate of this weapon")),
			OutputPortConfig<bool>( "Selected_3", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("LootSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case eFE_Initialize:
			{
				m_actInfo=*pActInfo;
				type_1_set=false;
				type_2_set=false;
				type_3_set=false;
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				break;
			}
		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_ENABLE))
			{
				type_1_set=false;
				type_2_set=false;
				type_3_set=false;
				gEnv->pGame->GetIGameFramework()->RegisterListener(this, "CFlowModuleState",FRAMEWORKLISTENERPRIORITY_GAME);
				break;
			}
			if (IsPortActive(pActInfo, EIP_DISABLE))
			{
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				type_1_set=false;
				type_2_set=false;
				type_3_set=false;
				break;
			}
		}
	}

	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) {}
	virtual void OnPostUpdate(float fDelta)
	{
		AbilityModule *am=g_pGame->GetAbilityModule();
		slot_1=am->GetSlot(1);
		slot_2=am->GetSlot(2);
		slot_3=am->GetSlot(3);
		ActivateOutput(&m_actInfo,EOP_TYPE1,(string)slot_1->GetName());
		ActivateOutput(&m_actInfo,EOP_TYPE2,(string)slot_2->GetName());
		ActivateOutput(&m_actInfo,EOP_TYPE3,(string)slot_3->GetName());
		ActivateOutput(&m_actInfo,EOP_TIME1,slot_1->GetTimer().Cdtimer);
		ActivateOutput(&m_actInfo,EOP_TIME2,slot_2->GetTimer().Cdtimer);
		ActivateOutput(&m_actInfo,EOP_TIME3,slot_3->GetTimer().Cdtimer);
		ActivateOutput(&m_actInfo,EOP_MAXTIME1,slot_1->GetTimer().Cd);
		ActivateOutput(&m_actInfo,EOP_MAXTIME2,slot_2->GetTimer().Cd);
		ActivateOutput(&m_actInfo,EOP_MAXTIME3,slot_3->GetTimer().Cd);
		ActivateOutput(&m_actInfo,EOP_SELECTED1,am->IsModuleSelected(1));
		ActivateOutput(&m_actInfo,EOP_SELECTED2,am->IsModuleSelected(1));
		ActivateOutput(&m_actInfo,EOP_SELECTED3,am->IsModuleSelected(1));
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

private:
	BaseAbility *slot_1;
	BaseAbility *slot_2;
	BaseAbility *slot_3;
	bool type_1_set;
	bool type_2_set;
	bool type_3_set;
	SActivationInfo m_actInfo;
};


class CFlowNode_AIManager : public CFlowBaseNode<eNCT_Singleton> ,public IGameFrameworkListener
{
	enum INPUTS
	{
		EIP_ENABLE=0,
		EIP_DISABLE,
		EIP_UPDATE,
	};
	enum OUTPUTS
	{
		EOP_ENTITYID,
		EOP_PATROLFINISHED,
		EOP_ORINPOS,
		EOP_DESTPOS,
		EOP_DONE,
	};
public:
	CFlowNode_AIManager(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Enable",_HELP("Get XML")),
			InputPortConfig_AnyType("Disable",_HELP("Get XML")),
			InputPortConfig_AnyType("Update",_HELP("Get XML")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<EntityId>( "GruntId", _HELP("Fire rate of this weapon")),
			OutputPortConfig<bool>( "IsPatrolFinished", _HELP("Fire rate of this weapon")),
			OutputPortConfig<Vec3>( "OriginalPosition", _HELP("Fire rate of this weapon")),
			OutputPortConfig<Vec3>( "Destination", _HELP("Fire rate of this weapon")),
			OutputPortConfig_Void( "Done",	_HELP( "Triggers if weapon changed" ) ),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("AIManager");
		config.SetCategory(EFLN_APPROVED);
	}

	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case eFE_Initialize:
			{
				m_actInfo=*pActInfo;
				Manager = g_pGame->GetSpawnedAIManager();
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				break;
			}
		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_ENABLE))
			{
				gEnv->pGame->GetIGameFramework()->RegisterListener(this, "CFlowNode_AIManager",FRAMEWORKLISTENERPRIORITY_GAME);
				break;
			}
			if (IsPortActive(pActInfo, EIP_DISABLE))
			{
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				break;
			}
			if (IsPortActive(pActInfo, EIP_UPDATE))
			{
				Manager->Update();
				break;
			}
		}
	}

	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) {}
	virtual void OnPostUpdate(float fDelta)
	{
		if(!Manager)
			Manager = g_pGame->GetSpawnedAIManager();
		else
		{
			AIBindParams *params = Manager->GetBindParams();
			if(params)
			{

			}
			Manager->Update();
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

private:
	SpawnedAIManager *Manager;
	SActivationInfo m_actInfo;
};


class CFlowNode_EnemyState : public CFlowBaseNode<eNCT_Singleton> ,public IGameFrameworkListener
{
	enum INPUTS
	{
		EIP_ENABLE=0,
		EIP_DISABLE,
	};
	enum OUTPUTS
	{
		EOP_ENTITYID,
		EOP_CURHEALTH,
		EOP_MAXHEALTH,
		EOP_CLASSNAME,
		EOP_POS,
		EOP_HIDE,
	};
public:
	CFlowNode_EnemyState(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Enable",_HELP("Get XML")),
			InputPortConfig_AnyType("Disable",_HELP("Get XML")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<EntityId>( "EntityId", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "CurHealth", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "MaxHealth", _HELP("Fire rate of this weapon")),
			OutputPortConfig<string>( "ClassName", _HELP("Fire rate of this weapon")),
			OutputPortConfig<Vec3>( "Pos", _HELP("Fire rate of this weapon")),
			OutputPortConfig_Void( "Hide", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("EnemyState");
		config.SetCategory(EFLN_APPROVED);
	}

	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case eFE_Initialize:
			{
				m_actInfo=*pActInfo;
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				break;
			}
		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_ENABLE))
			{
				gEnv->pGame->GetIGameFramework()->RegisterListener(this, "CFlowNode_AIManager",FRAMEWORKLISTENERPRIORITY_GAME);
				pBRC = g_pGame->GetBasicRayCast();
				break;
			}
			if (IsPortActive(pActInfo, EIP_DISABLE))
			{
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				pBRC = 0;
				break;
			}
		}
	}

	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) {}
	virtual void OnPostUpdate(float fDelta)
	{
		/*if(pBRC)
		{
			if(pActor && pActor->GetEntity()->GetId() != g_pGame->GetClientActorId())
			{
				Vec3 IndicatorPos = pActor->GetEntity()->GetPos();
				AABB aabb;
				pActor->GetEntity()->GetLocalBounds(aabb);
				float z_offset = aabb.GetRadius()*2;
				IndicatorPos.z += z_offset;
				float c_health = pActor->GetHealth();
				float m_health = pActor->GetMaxHealth();
				ActivateOutput(&m_actInfo,EOP_ENTITYID, pActor->GetEntity()->GetId());
				ActivateOutput(&m_actInfo,EOP_CURHEALTH, c_health);
				ActivateOutput(&m_actInfo,EOP_MAXHEALTH, m_health);
				ActivateOutput(&m_actInfo,EOP_CLASSNAME,string(pActor->GetEntity()->GetClass()->GetName()));
				ActivateOutput(&m_actInfo,EOP_POS,IndicatorPos);
			}
			else
			{
				ActivateOutput(&m_actInfo,EOP_HIDE,true);
			}
		}*/
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

private:
	BasicRayCast *pBRC;
	SActivationInfo m_actInfo;
};

class CFlowNode_ProgressState : public CFlowBaseNode<eNCT_Singleton> ,public IGameFrameworkListener
{
	enum INPUTS
	{
		EIP_ENABLE=0,
		EIP_DISABLE,
	};
	enum OUTPUTS
	{
		EOP_ENTITYID,
		EOP_PROGRESS,
		EOP_POS,
		EOP_ERASE,
	};
public:
	CFlowNode_ProgressState(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_AnyType("Enable",_HELP("Get XML")),
			InputPortConfig_AnyType("Disable",_HELP("Get XML")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<EntityId>( "EntityId", _HELP("Fire rate of this weapon")),
			OutputPortConfig<float>( "Progress", _HELP("Fire rate of this weapon")),
			OutputPortConfig<Vec3>( "Pos", _HELP("Fire rate of this weapon")),
			OutputPortConfig_Void( "Erase", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("EnemyState");
		config.SetCategory(EFLN_APPROVED);
	}

	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case eFE_Initialize:
			{
				m_actInfo=*pActInfo;
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				break;
			}
		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_ENABLE))
			{
				gEnv->pGame->GetIGameFramework()->RegisterListener(this, "CFlowNode_AIManager",FRAMEWORKLISTENERPRIORITY_GAME);
				manager = &StateManager::theStateManager();
				break;
			}
			if (IsPortActive(pActInfo, EIP_DISABLE))
			{
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
				manager = &StateManager::theStateManager();
				break;
			}
		}
	}

	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) {}
	virtual void OnPostUpdate(float fDelta)
	{
		std::map<EntityId, progressState> &pStates = manager->GetProgressStates();
		if(!pStates.empty())
		{
			std::map<EntityId, progressState>::iterator iter = pStates.begin();
			std::map<EntityId, progressState>::iterator end = pStates.end();
			while(iter != end)
			{
				progressState &state = iter->second;
				if(state.tick_timer < state.last_time)
				{
					EntityId Id = iter->first;
					IEntity* pEntity = gEnv->pEntitySystem->GetEntity(Id);
					if(pEntity)
					{
						state.tick_timer += fDelta;
						int progress = static_cast<int>((state.tick_timer / state.last_time) * 99 + 1);
						AABB aabb;
						pEntity->GetWorldBounds(aabb);
						Vec3 EntityPos = aabb.GetCenter();
						ActivateOutput(&m_actInfo,EOP_ENTITYID, pEntity->GetId());
						ActivateOutput(&m_actInfo,EOP_PROGRESS, progress);
						ActivateOutput(&m_actInfo,EOP_POS, EntityPos);
					}
				}
				else
				{
					ActivateOutput(&m_actInfo,EOP_ENTITYID, iter->first);
					ActivateOutput(&m_actInfo,EOP_ERASE, true);
					pStates.erase(iter++);
					continue;
				}
				++iter;
			}
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

private:
	StateManager *manager;
	SActivationInfo m_actInfo;
};

//--------GetSpawnListClass--------
class CFlow_RayCastSystemLink : public CFlowBaseNode<eNCT_Singleton>
{
	enum INPUTS
	{
		EIP_Delete = 0,
		EIP_Scan,
		EIP_Modify,
	};
	enum OUTPUTS
	{
		EOP_DONE = 0,
	};
public:
	CFlow_RayCastSystemLink(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_Void("Delete",_HELP("Get system state")),
			InputPortConfig_Void("Scan",_HELP("Get system state")),
			InputPortConfig_Void("Modify",_HELP("Get system state")),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig_Void( "Done", _HELP("Fire rate of this weapon")),
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("SpawnSystem State");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case(eFE_Activate):
			{
				if(IsPortActive(pActInfo,EIP_Delete))
				{
					IEntity *pEntity = g_pGame->GetBasicRayCast()->GetEntity();
					if(pEntity)
					{
						g_pGame->GetDeleteSystem()->ProcessDeleteEntity(pEntity);
						ActivateOutput(pActInfo, EOP_DONE, true);
					}
				}
				if(IsPortActive(pActInfo,EIP_Scan))
				{
					IEntity *pEntity = g_pGame->GetBasicRayCast()->GetEntity();
					if(pEntity)
					{
						g_pGame->GetScanSystem()->ProcessScanEntity(pEntity);
						ActivateOutput(pActInfo, EOP_DONE, true);
					}
				}
			}
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};


//--------GetStability--------
class CFlowSetDataBasePos : public CFlowBaseNode<eNCT_Singleton>
{
	enum INPUTS
	{
		EIP_POS,
	};
	enum OUTPUTS
	{

	};
public:
	CFlowSetDataBasePos(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[]=
		{
			InputPortConfig<Vec3>("Enable",_HELP("Call this node")),
			{0}
		};
		static const SOutputPortConfig out_ports[]=
		{
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("Get Stability");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				m_actInfo=*pActInfo;
				break;
			}
		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_POS))
			{
				EntityDataBase::Instance().SetProject(GetPortVec3(pActInfo, EIP_POS));
			}
			break;
		}
	}
private:
	SActivationInfo m_actInfo;
};



//--------EntitFireModeUpdate--------
class CFlowEntityBulletRefreash : public CFlowBaseNode<eNCT_Singleton>
{
	enum INPUTS
	{
		EIP_UPDATE,
	};
	enum OUTPUTS
	{

	};
public:
	CFlowEntityBulletRefreash(SActivationInfo * pActInfo){}
	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[]=
		{
			InputPortConfig_Void("Update",_HELP("Call this node")),
			{0}
		};
		static const SOutputPortConfig out_ports[]=
		{
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("Get Stability");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				m_actInfo=*pActInfo;
				break;
			}
		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_UPDATE))
			{
				IActor *pPlayer = g_pGame->GetIGameFramework()->GetClientActor();
				if(pPlayer)
				{
					CWeapon *pWeapon = static_cast<CWeapon*>(pPlayer->GetCurrentItem(false));
					if(pWeapon)
					{
						IEntityClass *pClass = pWeapon->GetEntity()->GetClass();
						if(pClass == EntityClassPreLoad::Instance().pEntityGun)
						{
							int iFireMode = pWeapon ? pWeapon->GetCurrentFireMode() : -1;
							IFireMode* pFireMode = pWeapon ? pWeapon->GetFireMode(iFireMode) : NULL;
							//Code System : Sensor get entity gun's ammo type
							CEntityBulletMode *pEntityGunMode = static_cast<CEntityBulletMode*>(pFireMode);
							if(pEntityGunMode)
							{
								pEntityGunMode->UpdateUIInfo();
							}
						}
					}
				}
			}
			break;
		}
	}
private:
	SActivationInfo m_actInfo;
};
//Stability
REGISTER_FLOW_NODE( "Code:WorldStability:GetStability", CFlowGetStability );

//SpawnSystem
REGISTER_FLOW_NODE( "Code:SpawnSystem:SystemState", CFlowSpawnSystemState);
REGISTER_FLOW_NODE( "Code:SpawnSystem:SetIndex", CFlowIsSpawnSystemIndex);
REGISTER_FLOW_NODE( "Code:SpawnSystem:GetXml", CFlowSpawnSystemGetXML);
REGISTER_FLOW_NODE( "Code:SpawnSystem:GetSpawnClass", CFlowGetSpawnClass);
REGISTER_FLOW_NODE( "Code:SpawnSystem:SpawnData:BasicEntity", CFlowGetSD_BasicEntity);
REGISTER_FLOW_NODE( "Code:SpawnSystem:SpawnData:DestroyableObject", CFlowGetSD_DestroyableObject);
REGISTER_FLOW_NODE( "Code:SpawnSystem:SpawnData:Item", CFlowGetSD_Item);
REGISTER_FLOW_NODE( "Code:SpawnSystem:SpawnData:Vehicle", CFlowGetSD_Vehicle);
REGISTER_FLOW_NODE( "Code:SpawnSystem:SpawnData:AI", CFlowGetSD_AI);
REGISTER_FLOW_NODE( "Code:SpawnSystem:SpawnData:Light", CFlowGetSD_Light);
REGISTER_FLOW_NODE( "Code:SpawnSystem:SpawnData:Ladder", CFlowGetSD_Ladder);
REGISTER_FLOW_NODE( "Code:SpawnSystem:AddFavorite", CFlowS_AddFavorite);


//Control_All
REGISTER_FLOW_NODE( "Code:System:Control", CFlowSystemControl);

/*//ModifySystem
REGISTER_FLOW_NODE( "Code:ModifySystem:BasicEntityOut", CFlowModifyBasicEntity);
REGISTER_FLOW_NODE( "Code:ModifySystem:DestroyableObjectOut", CFlowModifyDestroyableObject);
REGISTER_FLOW_NODE( "Code:ModifySystem:LandVehicleOut", CFlowModifyLandVehicle);
REGISTER_FLOW_NODE( "Code:ModifySystem:LightOut", CFlowModifyLight);
REGISTER_FLOW_NODE( "Code:ModifySystem:StandardWeaponOut", CFlowModifyStandardWeapon);*/

//LootSystem
REGISTER_FLOW_NODE( "Code:LootSystem:LootState", CFlowLootBody);
REGISTER_FLOW_NODE( "Code:LootSystem:LootEntities", CFlowLootEntity);

//Another
REGISTER_FLOW_NODE( "Code:DrawLock", CFlowDrawLock);
REGISTER_FLOW_NODE( "Code:ModuleState", CFlowModuleState);
REGISTER_FLOW_NODE( "Code:SpawnedAIManager", CFlowNode_AIManager);
REGISTER_FLOW_NODE( "Code:RayCastLink", CFlow_RayCastSystemLink);

//EnemyState
REGISTER_FLOW_NODE( "Code:EnemyState", CFlowNode_EnemyState);
REGISTER_FLOW_NODE( "Code:StateManager", CFlowNode_ProgressState);
REGISTER_FLOW_NODE( "Code:SetDataBasePos", CFlowSetDataBasePos);

REGISTER_FLOW_NODE( "Code:UpdateEntityGunInfo", CFlowEntityBulletRefreash);