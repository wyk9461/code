#ifndef __BLACKHOLE_H__
#define __BLACKHOLE_H__

#include "StdAfx.h"
#include "Projectile.h"

class CBlackHole : public CProjectile 
{
private:
	typedef CProjectile BaseClass;
public:
	CBlackHole();
	~CBlackHole();
	// CProjectile
	virtual void HandleEvent(const SGameObjectEvent &event);
	virtual void Update(SEntityUpdateContext &ctx, int updateSlot);

private:
	void OnAttraction();
	bool AliveObjectCheck(IEntity *) const;
private:
	Vec3 centerPos;
	float time;
	int conter;
};

#endif //__BLACKHOLE_H__