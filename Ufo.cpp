#include "StdAfx.h"
#include "Ufo.h"
#include "Game.h"
#include "GameRules.h"
#include "Actor.h"
#include "Bullet.h"
#include "WeaponSystem.h"

CUfo::CUfo()
	: existTime(0.0)
	, bMap(false)
	, fireTime(0.0)
{

}

CUfo::~CUfo()
{

}


void CUfo::HandleEvent(const SGameObjectEvent &event)
{

	BaseClass::HandleEvent(event);
}

void CUfo::Update(SEntityUpdateContext &ctx, int updateSlot)
{
	BaseClass::Update(ctx, updateSlot);
	if (existTime < 5)
	{
		if (fireTime > 2)
		{
			if (!bMap)
			{
				bMap = true;
				Vec3 currPos = this->GetEntity()->GetPos();
				IPhysicalEntity **nearbyEntities;
				int n = gEnv->pPhysicalWorld->GetEntitiesInBox(currPos-Vec3(20),currPos+Vec3(20),nearbyEntities,ent_living);
				for(int i = 0; i < n; i++)
				{
					IEntity* pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
					if(pEntity != g_pGame->GetIGameFramework()->GetClientActor()->GetEntity())
						tarMap[pEntity] = 5;
				}
			}
			else
			{
				std::map<IEntity*, int>::iterator map_it = tarMap.begin();
				while (map_it != tarMap.end())
				{
					if (map_it->second > 0)
					{
						--map_it->second;
						
						IEntityClass* pTargetClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("TraceBullet");
						CBullet* pAmmo = static_cast<CBullet*>(g_pGame->GetWeaponSystem()->SpawnAmmo(pTargetClass, false));
						if (pAmmo)
						{
							int idamage = CProjectile::m_damage;
							CProjectile::SProjectileDesc projectileDesc(
								m_ownerId, m_hostId, m_weaponId, idamage, 0, 0, 0, m_hitTypeId,
								0, false);
							pAmmo->SetParams(projectileDesc);

							Vec3 tPos = map_it->first->GetPos();
							Vec3 dir = tPos + Vec3(0,0,1) - this->GetLastPos();
							dir.normalize();
							pAmmo->Launch(this->GetLastPos(), dir, Vec3(ZERO));
							break;
						}
					}
					else
					{
						++map_it;
					}
				}
			}
		}
	}
	else
	{
		bMap = false;
		existTime = 0;
		fireTime = 0;
		Destroy();
	}
	existTime += ctx.fFrameTime;
	fireTime += ctx.fFrameTime;
}

