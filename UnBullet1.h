#ifndef _UNBULLET1_H_
#define _UNBULLET1_H_

#include "StdAfx.h"
#include "Projectile.h"

class CUnBullet1 : public CProjectile
{
	typedef CProjectile BaseClass;
public:
	CUnBullet1();
	~CUnBullet1();
	virtual void HandleEvent(const SGameObjectEvent &);
	virtual void ProcessEvent(SEntityEvent &event);
	virtual void Update(SEntityUpdateContext &ctx, int updateSlot);

private:

	float damage;
	float liveTime;
	float time;
	float startTime;
	bool bAble;
};
#endif 