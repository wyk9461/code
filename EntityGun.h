#ifndef _ENTITYGUN_H_
#define _ENTITYGUN_H_

#include "Weapon.h"

struct BaseEntityParames
{
	BaseEntityParames(IEntity *_pEntity, float _mass, int _type)
		: pEntity(_pEntity)
		, mass(_mass)
		, damage(0)
		, ammo_count(0)
		, type(_type){}

	BaseEntityParames()
		: pEntity(NULL)
		, mass(100)
		, damage(0)
		, ammo_count(0)
		, type(0){}

	IEntity *pEntity;
	float mass;
	float damage;
	int ammo_count;
	int type;

	BaseEntityParames &operator=(const BaseEntityParames &rhs)
	{
		if(this == &rhs)
			return *this;
		pEntity = rhs.pEntity;
		mass = rhs.mass;
		damage = rhs.damage;
		ammo_count = rhs.ammo_count;
		type = rhs.type;
		return *this;
	}
};

struct EntityUpdateParams
{
	EntityUpdateParams()
	:	pEntity(NULL)
	,	Center(Vec3(ZERO))
	,	scale(1)
	,	erase_time(1)
	,	tick_timer(0){}

	IEntity *pEntity;
	Vec3 Center;
	float erase_time;
	float tick_timer;
	float scale;

	EntityUpdateParams &operator=(const EntityUpdateParams &rhs)
	{
		if(this == &rhs)
			return *this;
		pEntity = rhs.pEntity;
		Center = rhs.Center;
		erase_time = rhs.erase_time;
		tick_timer = rhs.tick_timer;
		scale = rhs.scale;
		return *this;
	}
};

class CEntityGun : public CWeapon
{
private:

	typedef CWeapon BaseClass;

public:

	CEntityGun();
	virtual ~CEntityGun();

	virtual void OnShoot(EntityId shooterId, EntityId ammoId, IEntityClass* pAmmoType, const Vec3 &pos, const Vec3 &dir, const Vec3 &vel);
	virtual bool OnActionTransformEntity(EntityId actorId, const ActionId& actionId, int activationMode, float value);


	virtual void Update(SEntityUpdateContext& ctx, int);
	virtual bool CanReload() const;

	virtual IEntity* GetBasicEntity();
	virtual float GetBasicEntityMass();
	virtual int GetProjectileType();
	virtual float GetDamage(int type);
	virtual int GetEntityAmmoCount(int type);
	virtual void DeleteBasicEntityClip(int type);
	virtual void SetCurType(int type);

private:
	int GetStabilityCost(IEntity*);
	float GetEffect(IEntity*);
	void SetAmmoSlot(IEntity* pEntity, int type, int ammo_count);
	void AbsorbEntity();
	int GetEntityType(IEntity *pEntity);

	//UI Interface
	float CalculateDamage(IEntity *pEntity);
	void SpawnParticle(IEntity *pEntity);
	void SpawnIndicateEntity(IEntity *pEntity);
	void UpdateIndicatrEntity();
	void GenerateUIMessage(int type, int damage);

private:
	int curType;
	std::map<int,BaseEntityParames> m_AmmoMap;
	
	//UI Params
	std::list<EntityUpdateParams> UpdateList;
	static IParticleEffect *pObsorbParticle;
	static IParticleEffect *pHideParticle;
	IParticleEmitter *pEmitter;
	IEntity *pIndicateEntity;
};

#endif