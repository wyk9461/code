#ifndef _LASERGUN_H_
#define _LASERGUN_H_

#include "Weapon.h"

class CLaserGun : public CWeapon
{
private:

	typedef CWeapon BaseClass;

public:

	CLaserGun();
	virtual ~CLaserGun();

	virtual void Update(SEntityUpdateContext& ctx, int slot);
	virtual void OnShoot(EntityId shooterId, EntityId ammoId, IEntityClass* pAmmoType, const Vec3 &pos, const Vec3 &dir, const Vec3 &vel);

private:

	IEntityClass* pAmmoType;
	float startTime;
	float incTime;
	bool bInc;
};

#endif