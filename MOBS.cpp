#include "StdAfx.h"
#include "MOBS.h"

CMOBS::CMOBS()
	: bStart(false)
	, bExStart(false)
	, m_intervalTime(0.0)
	, m_liveTime(0.0)
{
	m_startPos = Vec3(ZERO);
	m_dir = Vec3(ZERO);
	m_hitPos = Vec3(ZERO);
}

CMOBS::~CMOBS()
{

}

void CMOBS::HandleEvent(const SGameObjectEvent &event)
{
	CProjectile::HandleEvent(event);    //官方API
	if(event.event == eGFE_OnCollision)    //判断实体事件是否为碰撞事件,若不是则不进行操作
	{
		EventPhysCollision *pCollision = (EventPhysCollision*)(event.ptr);
		if(!pCollision)
			return;

					
		Vec3 hitPos = pCollision->pt;
		hitPos.z = gEnv->p3DEngine->GetTerrainZ(static_cast<int>(hitPos.x),static_cast<int>(hitPos.y));
		Vec3 startPos = hitPos + Vec3(0,0,20);
		Vec3 dir = Vec3(0,0,-1);
		
		m_hitPos = hitPos;
		m_startPos = startPos;
		m_dir = dir;

		bStart = true;
		Destroy();
	}
}

void CMOBS::Update(SEntityUpdateContext &ctx, int updateSlot)
{
	BaseClass::Update(ctx,updateSlot);

	//SetVelocity(m_hitPos,Vec3(ZERO),Vec3(ZERO));

	if (bStart)
	{
		m_intervalTime += ctx.fFrameTime;
		if (m_intervalTime < 1.0)
		{
			//将此处替换为粒子效果，之后只产生爆炸
		}
		else
		{
			m_intervalTime = 0;
			bExStart = true;
			bStart = false;
		}
	}

	if (bExStart)
	{
		float damage = 300;
		ExplosionInfo einfo(0, 0, 0, damage , m_hitPos, Vec3(0,0,0),0.1f, 20.0f,  0.1,  10.0f , 0 , 1000.0f , 0.0f , 0);
		einfo.SetEffect("explosions.Grenade_SCAR.character",1.0f,0.2f);  //Need to change
		einfo.type = g_pGame->GetGameRules()->GetHitTypeId( "Explosion" );
		g_pGame->GetGameRules()->QueueExplosion(einfo);

		bExStart = false;

		m_intervalTime = 0;
	}

	m_liveTime += ctx.fFrameTime;
	if (m_liveTime > 10)
	{
		m_liveTime = 0;
	}
}
