#include "StdAfx.h"
#include "AirGun.h"

CAirGun::CAirGun()
	: m_time(0)
{
}

CAirGun::~CAirGun()
{

}

void CAirGun::OnShoot(EntityId shooterId, EntityId ammoId, IEntityClass* pAmmoType, const Vec3 &pos, const Vec3 &dir, const Vec3 &vel)
{
	BaseClass::OnShoot(shooterId, ammoId, pAmmoType, pos, dir, vel);

	SetAmmoCount(pAmmoType, 2);
	GetTarget();
 	OnPush();
}

void CAirGun::Update(SEntityUpdateContext& ctx, int num)
{
	BaseClass::Update(ctx,num);
	
// 	m_time += ctx.fFrameTime;
// 	if(m_time < 1.2)
// 	{
// 		std::vector<IEntity*>::iterator pushIt;
// 		if (!liveVec.empty())
// 		{
// 			for (pushIt = liveVec.begin();pushIt != liveVec.end(); pushIt++)
// 			{
// 				IEntity *pEntity = *pushIt;
// 
// 				CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());
// 				if (pActor->IsDead())
// 				{
// 					AirPush(pEntity);
// 				}
// 			}
// 		}
// 	}
// 	else
// 	{
// 		m_time = 0;
// 		liveVec.clear();
// 	}
}

void CAirGun::GetTarget()
{
	IEntity *pActor = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity();
	Vec3 currPos = pActor->GetPos();
	IPhysicalEntity **nearbyEntities = NULL;
	int n = gEnv->pPhysicalWorld->GetEntitiesInBox(currPos-Vec3(10),currPos+Vec3(10),nearbyEntities,
		ent_rigid|ent_sleeping_rigid|ent_living);
	for(int i = 0; i < n; i++)
	{
		IEntity* pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
		if(pEntity)
		{
			Vec3 check_dir = pEntity->GetPos() - currPos;
			const Vec3 &fwddir = pActor->GetForwardDir();
			check_dir.normalize();

			f32 dot = fwddir.Dot(check_dir);
			float angle = RAD2DEG(cry_acosf(dot));

			const float check_angle = 30.0;//中轴线为边的30度角
// 			Vec3 mark(ZERO);
// 			if (gEnv->pRenderer->GetCamera().Project(pEntity->GetPos(), mark))
			if (angle < check_angle)
			{
 				CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());
 				if (pActor)
 				{
					if(pEntity != g_pGame->GetIGameFramework()->GetClientActor()->GetEntity())
						liveVec.push_back(pEntity);
 				}
				else
				{
					objVec.push_back(pEntity);
				}
			}
		}
	}
}

void CAirGun::OnPush()
{
	std::vector<IEntity*>::iterator pushIt;
	if (!liveVec.empty())
	{
		for (pushIt = liveVec.begin();pushIt != liveVec.end(); pushIt++)
		{
			IEntity *pEntity = *pushIt;
			Vec3 oPos = this->GetEntity()->GetPos();

			CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());
			if (pActor && pActor->GetHealth() < 600)
			{
				float damage = 600;
				EntityId shooterId = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetId();
				HitInfo hitInfo(shooterId, pEntity->GetId(), this->GetEntityId(),
					damage, 0.0f, -1, -1,1, oPos, pEntity->GetPos() -oPos,
					oPos - pEntity->GetPos());
				g_pGame->GetGameRules()->ClientHit(hitInfo);
			}
			
// 			CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());
// 			if (pActor && pActor->GetHealth() < 600)
// 			{
// 				pActor->Kill();
// 				pActor->RagDollize(true);
// 			}

			//pActor->GetActorStats()->isInBlendRagdoll = true;

			AirPush(pEntity);
			//pActor->RagDollize(false);
		}
		liveVec.clear();
	}
	
	if (!objVec.empty())
	{
		for (pushIt = objVec.begin();pushIt != objVec.end(); pushIt++)
		{
			IEntity *pEntity = *pushIt;
			AirPush(pEntity);
		}
		objVec.clear();
	}
}

void CAirGun::AirPush(IEntity *pEntity)
{
	Vec3 tPos = pEntity->GetPos();
	Vec3 oPos = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
	Vec3 dir = oPos - tPos;

	pe_status_dynamics params;
	pEntity->GetPhysics()->GetStatus(&params);
	float &mass = params.mass;

	//need to chenge
	//float times = mass * 10;//需要一个类似Log的函数来保证力不会过大
	float times = 0.0;
	if (mass > 10)
	{
		times = mass * (1 / log(mass));
	}
	else
	{
		times = mass;
	}
	dir.normalized();
	dir += Vec3(0,0,2);
	dir.normalized();
	IEntityPhysicalProxy* pp=(IEntityPhysicalProxy*)pEntity->GetProxy(ENTITY_PROXY_PHYSICS);
	pp->AddImpulse(-1,oPos,-dir*times,true,1,10);
}