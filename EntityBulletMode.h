//For EntityGun
//launch Entity Bullet 
//The bullet launched will cause different effect 

#ifndef __ENTITYBULLETMODE_H__
#define __ENTITYBULLETMODE_H__

#if _MSC_VER > 1000
# pragma once
#endif


#include "Rapid.h"

class CEntityBulletMode : public CRapid
{

private:
	typedef CRapid BaseClass;

public:
	CRY_DECLARE_GTI(CEntityBulletMode);

	CEntityBulletMode();
	virtual ~CEntityBulletMode(){};
	virtual bool Shoot(bool resetAnimation, bool autoreload, bool isRemote=false);
	virtual void GetMemoryUsage(ICrySizer * s) const;
	virtual bool FillAmmo(bool fromInventory);
//	virtual void StartReload(int zoomed);
	virtual void Reload(int zoomed);
	virtual void PushEntityBulletClipDeque(int type);
	virtual void PopEntityBulletClipDeque();
	virtual void SelectNextClip(bool next);
	virtual void SetCurrType(int type);

	//////////////////////////Weapon note//////////////////////////////////////
	//	Set curr_type in the front of the deque(clips_Deq)
	void ClipSwitch();
	ILINE const int  GetCurType() const{return curr_type;}
	void EmitTracer(const Vec3& pos,const Vec3& destination, CTracerManager::STracerParams &params, CProjectile* pProjectile);
	void UpdateUIInfo();
private:
	std::deque<int> clips_Deq;
	int curr_type;
};

#endif 