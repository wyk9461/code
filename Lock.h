//For LockGun 
//Use LockAmmo
//Use for AOE Weapon

#ifndef __LOCK_H__
#define __LOCK_H__

#if _MSC_VER > 1000
# pragma once
#endif


#include "Single.h"

class CLock : public CSingle
{

private:
	typedef CSingle BaseClass;

public:
	CRY_DECLARE_GTI(CLock);

	CLock();
	virtual ~CLock(){};
	virtual bool Shoot(bool resetAnimation, bool autoreload, bool isRemote=false);
	virtual void GetMemoryUsage(ICrySizer * s) const;
	virtual void GetLockEntity(IEntity *lockEntity);
	
private:
	IEntity *myLockEntity;
};

#endif 