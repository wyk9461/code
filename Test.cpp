#include "StdAfx.h"
#include "Test.h"
#include "Game.h"
#include "GameRules.h"
#include "Actor.h"

CTest::CTest()
{
}

CTest::~CTest()
{

}

void CTest::ProcessEvent(SEntityEvent &event)
{
}

void CTest::HandleEvent(const SGameObjectEvent &event)
{
	CProjectile::HandleEvent(event);    //官方API
	if(event.event == eGFE_OnCollision)    //判断实体事件是否为碰撞事件,若不是则不进行操作
	{
		float damage=100;

		EventPhysCollision *pCollision = (EventPhysCollision*)(event.ptr);  //强制转换类型,在CE里很常见
		if(!pCollision)    //如果指针为空则不进行操作
			return;
		IEntity *pTarget=pCollision->iForeignData[1] ==PHYS_FOREIGN_ID_ENTITY ? (IEntity*)pCollision->pForeignData[1] : 0;    //获取与弹头发生碰撞的实体.(PHYS_FOREIGN_ID_ENTITY表明是实体)

		if(pTarget)
		{
			CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId());    //获取角色
			EntityId targetId=pTarget->GetId();
			CryLogAlways(pTarget->GetClass()->GetName());    //CryLogAlwasy是用的非常多的Debug函数,用法跟printf一样.默认类型为string,%d,%f啥的你都懂的
			HitInfo hitInfo(m_ownerId ? m_ownerId : m_hostId, targetId, m_weaponId,
				damage, 0.0f, 0, pCollision->partid[1],
				m_hitTypeId, pCollision->pt, Vec3(0,0,1), pCollision->n);    //Hitinfo是个结构体,定义了子弹(或别的什么)造成的伤害的各种参数.这里是用构造函数了,笔记里应该有讲
			hitInfo.knocksDown=false;
			hitInfo.knocksDownLeg=false;
			hitInfo.remote = IsRemote();
			hitInfo.projectileId = GetEntityId();
			hitInfo.bulletType = m_pAmmoParams->bulletType;
			hitInfo.knocksDown = CheckAnyProjectileFlags(ePFlag_knocksTarget) && ( damage > m_minDamageForKnockDown );
			hitInfo.knocksDownLeg = m_chanceToKnockDownLeg>0 && damage>m_minDamageForKnockDownLeg && m_chanceToKnockDownLeg>(int)Random(100);
			hitInfo.penetrationCount = 0;
			hitInfo.hitViaProxy = CheckAnyProjectileFlags(ePFlag_firedViaProxy);
			hitInfo.aimed = CheckAnyProjectileFlags(ePFlag_aimedShot);    //上面一长串都是初始化hitinfo里的玩意,这些记住就好,记不住就直接复制粘贴...
			g_pGame->GetGameRules()->ClientHit(hitInfo);    //处理这个伤害事件
		}
		DestroyImmediate();
		//Destroy();    //销毁弹头.
	}
}
