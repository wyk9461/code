#include "StdAfx.h"
#include "MOBSLuncher.h"
#include "Weapon.h"

CMOBSLuncher::CMOBSLuncher()
	: CDTime(0)
	, bCD(false)
	, m_pAmmoType(NULL)
{
}

CMOBSLuncher::~CMOBSLuncher()
{
}

void CMOBSLuncher::StartFire()
{
	if (bCD)
	{
		BaseClass::StartFire();
		bCD = false;
		BaseClass::StopFire();
	}
}

void CMOBSLuncher::Update(SEntityUpdateContext& ctx, int slot)
{
	BaseClass::Update(ctx,slot);
	if (bCD == false)
	{
		CDTime += ctx.fFrameTime;

		if (CDTime > 10)
		{
			bCD = true;
			CDTime = 0;
		}
	}
}