﻿#include "StdAfx.h"
#include "BlackHole.h"
#include "Game.h"
#include "ItemSystem.h"
#include "IEntity.h"

CBlackHole::CBlackHole()
	: time(0)
	, conter(0)
{
	centerPos = Vec3(0,0,0);
}

CBlackHole::~CBlackHole()
{
}

void CBlackHole::HandleEvent(const SGameObjectEvent &event)
{
	CProjectile::HandleEvent(event);
	if (event.event == eGFE_OnCollision)
	{		
		EventPhysCollision *pCollision = (EventPhysCollision*)(event.ptr);
		if(!pCollision)
			return;
		IEntity *pTarget=pCollision->iForeignData[1] ==PHYS_FOREIGN_ID_ENTITY ? (IEntity*)pCollision->pForeignData[1] : 0;  
		
		if (!pTarget)
		{
			GetEntity()->IncKeepAliveCounter();//Hide not Destory
			conter++;
		}
		
		Vec3 TargetPos = pCollision->pt;

		centerPos = TargetPos;

		time = 0;
		//OnAttraction();
		Destroy();
	}
}

void CBlackHole::Update(SEntityUpdateContext &ctx, int updateSlot)
{
	BaseClass::Update(ctx,updateSlot);
	
	if (conter > 0)
	{
		GetEntity()->DecKeepAliveCounter();
		conter--;
	}

	time += ctx.fFrameTime;
	if (centerPos != Vec3(0,0,0))
	{
		if (time < 10)
		{
			OnAttraction();
		}
		else
		{
			centerPos = Vec3(0,0,0);
			time = 0;
		}
	}
}

void CBlackHole::OnAttraction()
{
	IPhysicalEntity **nearbyEntities;
	int n = gEnv->pPhysicalWorld->GetEntitiesInBox(centerPos-Vec3(20),centerPos+Vec3(20),nearbyEntities,ent_rigid|ent_sleeping_rigid|ent_living);
	for(int i = 0; i < n; i++)
	{
		IEntity* pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
		if (pEntity)
		{
			if (AliveObjectCheck(pEntity))
			{
				Vec3 hitPos = pEntity->GetPos();
				Vec3 tdir = centerPos - hitPos;
				
				pe_status_dynamics params;
				pEntity->GetPhysics()->GetStatus(&params);
				float &mass = params.mass;
				
				//Just like gravity
				float times = (1 / tdir.GetLengthFloat()) * mass * 1/*coefficient*/;
				IEntityPhysicalProxy* pp=(IEntityPhysicalProxy*)pEntity->GetProxy(ENTITY_PROXY_PHYSICS);
				pp->AddImpulse(-1,hitPos,tdir*times,true,1,10);
			}
		}
	}
}

bool CBlackHole::AliveObjectCheck(IEntity *pEntity) const
{
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());
	if (pActor)
	{
		float health = pActor->GetHealth();
		//秒杀血量
		if (health > 600)
		{
			return false;
		}
		else
		{
			float damage = 600;
			EntityId shooterId = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetId();
			HitInfo hit;
			hit.shooterId=g_pGame->GetClientActorId();
			hit.targetId=pEntity->GetId();
			hit.dir=-(gEnv->pEntitySystem->GetEntity(hit.targetId)->GetPos()-pActor->GetEntity()->GetPos()).normalize();
			hit.bulletType=0;
			hit.hitViaProxy=false;
			hit.partId=0;
			hit.type=1;
			hit.projectileId=1;
			hit.weaponId=1;
			hit.damage = damage;
			g_pGame->GetGameRules()->ClientHit(hit);
		}
	}
	return true;
}