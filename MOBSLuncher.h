#ifndef _MOBSLUNCHER_H_
#define _MOBSLUNCHER_H_

#include "Weapon.h"

class CMOBSLuncher : public CWeapon
{
private:

	typedef CWeapon BaseClass;

public:

	CMOBSLuncher();
	virtual ~CMOBSLuncher();

	virtual void StartFire();
	virtual void Update(SEntityUpdateContext& ctx, int slot);

private:
	float CDTime;
	bool bCD;
	IEntityClass * m_pAmmoType;
};

#endif