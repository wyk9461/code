#ifndef _UNNAMED1_H_
#define _UNNAMED1_H_

#include "Weapon.h"

class CUnnamed1 : public CWeapon
{
private:

	typedef CWeapon BaseClass;

public:

	CUnnamed1();
	virtual ~CUnnamed1();

	virtual void OnShoot(EntityId shooterId, EntityId ammoId, IEntityClass* pAmmoType, const Vec3 &pos, const Vec3 &dir, const Vec3 &vel);
	virtual void StartFire();
	virtual void Update(SEntityUpdateContext& ctx, int slot);

private:
	float CDTime;
	bool bCD;
	IEntityClass * m_pAmmoType;
};

#endif