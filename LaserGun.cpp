#include "StdAfx.h"
#include "LaserGun.h"
#include "Weapon.h"

CLaserGun::CLaserGun()
	: startTime(0)
	, incTime(0)
	, bInc(true)
{
}

CLaserGun::~CLaserGun()
{
}

void CLaserGun::Update(SEntityUpdateContext& ctx, int slot)
{
	BaseClass::Update(ctx,slot);

	if (bInc == false)
	{
		startTime += ctx.fFrameTime;
		if(startTime > 10)
		{
			bInc = true;
			startTime = 0;
		}
	}
	if (bInc == true)
	{
		int currentAmmoCount = GetFireMode(GetCurrentFireMode())->GetAmmoCount();
		int clipSize = GetFireMode(GetCurrentFireMode())->GetClipSize();

		if (currentAmmoCount < clipSize)
		{
			pAmmoType = GetFireMode(GetCurrentFireMode())->GetAmmoType();
			incTime += ctx.fFrameTime;
			
			if (0.5 < incTime)
			{
				SetAmmoCount(pAmmoType, ++currentAmmoCount);
				incTime = 0;
			}
		}
	}
}

void CLaserGun::OnShoot(EntityId shooterId, EntityId ammoId, IEntityClass* pAmmoType, const Vec3 &pos, const Vec3 &dir, const Vec3 &vel)
{
	BaseClass::OnShoot(shooterId, ammoId, pAmmoType, pos, dir, vel);

	bInc = false;
}
