#include "StdAfx.h"
#include "UnBullet1.h"
#include "Game.h"
#include "GameRules.h"
#include "Actor.h"

CUnBullet1::CUnBullet1()
	: damage(1000)
	, liveTime(0)
	, time(0)
	, startTime(0)
	, bAble(true)
{
}

CUnBullet1::~CUnBullet1()
{

}

void CUnBullet1::HandleEvent(const SGameObjectEvent &event)
{
	if (CheckAnyProjectileFlags(ePFlag_destroying | ePFlag_needDestruction))
	{
		return;
	}

	FUNCTION_PROFILER(GetISystem(), PROFILE_GAME);

	if (event.event == eGFE_OnPostStep && (event.flags&eGOEF_LoggedPhysicsEvent)==0)
	{
		//==============================================================================
		// Warning this is called directly from the physics thread! must be thread safe!
		//==============================================================================
		EventPhysPostStep* pEvent = (EventPhysPostStep*)event.ptr;
		g_pGame->GetWeaponSystem()->OnProjectilePhysicsPostStep(this, pEvent, 1);
	}
	if (event.event == eGFE_OnCollision)
	{
		GetEntity()->EnablePhysics(false);
	}
}

void CUnBullet1::ProcessEvent(SEntityEvent &event)
{
}

void CUnBullet1::Update(SEntityUpdateContext &ctx, int updateSlot)
{
	BaseClass::Update(ctx,updateSlot);

	startTime += ctx.fFrameTime;
	if (bAble)
	{
		GetEntity()->EnablePhysics(false);
		bAble = false;
	}


	if (startTime > 1)
	{
		time += ctx.fFrameTime;
		if (time > 0.5)
		{
			ExplosionInfo einfo(0, 0, 0, damage , this->GetEntity()->GetPos(), Vec3(0,0,0),0.1f, 10.0f,  0.1,  10.0f , 0 , 1000.0f , 0.0f , 0);
			einfo.SetEffect("explosions.Grenade_SCAR.character",1.0f,0.2f);  //Need to change
			einfo.type = g_pGame->GetGameRules()->GetHitTypeId( "Explosion" );
			g_pGame->GetGameRules()->QueueExplosion(einfo);
			time = 0;
		}
		liveTime += ctx.fFrameTime;
		if (liveTime > 5)
		{
			liveTime = 0;
			startTime = 0;
			Destroy();
		}
	}

	Vec3 forDir = GetEntity()->GetForwardDir();
	Vec3 newPos = GetEntity()->GetPos();
	newPos = newPos + forDir * 0.3f;
	GetEntity()->SetPos(newPos);

}