#include "StdAfx.h"
#include "Trace.h"
#include "Game.h"
#include "GameRules.h"
#include "Actor.h"

CTrace::CTrace()
	: pTarget(NULL)
	, damage(30)
	, currentPos(Vec3(ZERO))
	, time(0)
	, dtime(0)
	, bActive(true)
{
}
bool CTrace::Init(IGameObject *pGameObject)
{
	CProjectile::Init(pGameObject);
	damage = 30;
	currentPos = Vec3(ZERO);
	pTarget = NULL;
	time = 0;
	dtime = 0;
	bActive = true;
	return true;
}

void CTrace::ReInitFromPool()
{
	CProjectile::ReInitFromPool();
	damage = 30;
	currentPos = Vec3(ZERO);
	pTarget = NULL;
	time = 0;
	dtime = 0;
	bActive = true;
}

CTrace::~CTrace()
{

}

void CTrace::HandleEvent(const SGameObjectEvent &event)
{
	CProjectile::HandleEvent(event);    //官方API
	if(event.event == eGFE_OnCollision)    //判断实体事件是否为碰撞事件,若不是则不进行操作
	{
		EventPhysCollision *pCollision = (EventPhysCollision*)(event.ptr);  //强制转换类型,在CE里很常见
		if(!pCollision)    //如果指针为空则不进行操作
			return;
		IEntity *pTarget=pCollision->iForeignData[1] ==PHYS_FOREIGN_ID_ENTITY ? (IEntity*)pCollision->pForeignData[1] : 0;    //获取与弹头发生碰撞的实体.(PHYS_FOREIGN_ID_ENTITY表明是实体)

		if(pTarget)
		{
			EntityId targetId=pTarget->GetId();
			HitInfo hitInfo(m_ownerId ? m_ownerId : m_hostId, targetId, m_weaponId,
				damage, 0.0f, 0, pCollision->partid[1],
				m_hitTypeId, pCollision->pt, Vec3(0,0,1), pCollision->n);    //Hitinfo是个结构体,定义了子弹(或别的什么)造成的伤害的各种参数.这里是用构造函数了,笔记里应该有讲
			hitInfo.remote = IsRemote();
			hitInfo.projectileId = GetEntityId();
			hitInfo.bulletType = m_pAmmoParams->bulletType;
			/*hitInfo.knocksDown = CheckAnyProjectileFlags(ePFlag_knocksTarget) && ( damage > m_minDamageForKnockDown );
			hitInfo.knocksDownLeg = m_chanceToKnockDownLeg>0 && damage>m_minDamageForKnockDownLeg && m_chanceToKnockDownLeg>(int)Random(100);*/
			hitInfo.knocksDown = true;
			hitInfo.knocksDownLeg = true;
			hitInfo.penetrationCount = 0;
			hitInfo.hitViaProxy = CheckAnyProjectileFlags(ePFlag_firedViaProxy);
			hitInfo.aimed = CheckAnyProjectileFlags(ePFlag_aimedShot);    //上面一长串都是初始化hitinfo里的玩意,这些记住就好,记不住就直接复制粘贴...
			g_pGame->GetGameRules()->ClientHit(hitInfo);    //处理这个伤害事件
		}
		Destroy();    //销毁弹头.
	}
}

void CTrace::ProcessEvent(SEntityEvent &event)
{

}


void CTrace::Update(SEntityUpdateContext &ctx, int updateSlot)
{
	BaseClass::Update(ctx,updateSlot);
	dtime += ctx.fFrameTime;
	if (dtime > 10)
	{
		dtime = 0;
		Destroy();
	}
	if(!pTarget)
	{
		IPhysicalEntity **nearbyEntities;
		int n = gEnv->pPhysicalWorld->GetEntitiesInBox(GetEntity()->GetPos()-Vec3(15),GetEntity()->GetPos()+Vec3(15),nearbyEntities,ent_living);

		if(n >= 1 && dtime > 1)
		{
			std::map<float,EntityId> DistanceMap;
			for(int i = 0; i < n; i++)
			{
				IEntity* pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
				Vec3 hitPos = pEntity->GetPos() + Vec3(0,0,1.4);
				Vec3 playerPos =  gEnv->pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
				CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());
				bool isdead = pActor->IsDead();
				if (isdead == false && pEntity->GetId() != g_pGame->GetClientActorId())
				{
					float length = (GetEntity()->GetPos() - hitPos).GetLengthSquared();
					DistanceMap[length] = pEntity->GetId();
				}
			}
			if(!DistanceMap.empty())
			{
				pTarget = gEnv->pEntitySystem->GetEntity(DistanceMap.begin()->second);
				DistanceMap.clear();
			}
		}
	}
	else
	{
		IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Projectile_Effect.tracer_test");
		pEffect->Spawn(true,IParticleEffect::ParticleLoc(GetEntity()->GetPos(),Vec3(0,0,1)));
		Vec3 dir1 = GetEntity()->GetForwardDir();
		Vec3 finaldir = pTarget->GetPos() + Vec3(0,0,1.4) - GetEntity()->GetPos();
		dir1.normalize();
		finaldir.normalize();
		float x_offset_speed = (finaldir.x - dir1.x) * ctx.fFrameTime *10;
		float y_offset_speed = (finaldir.y - dir1.y) * ctx.fFrameTime *10;
		float z_offset_speed = (finaldir.z - dir1.z) * ctx.fFrameTime *10;
		Vec3 next_dir = dir1 + Vec3(x_offset_speed,y_offset_speed,z_offset_speed);
		next_dir.normalize();
		SetVelocity(GetEntity()->GetPos(),next_dir,next_dir);
		float length = (GetEntity()->GetPos() - pTarget->GetPos()).GetLengthSquared();
		if(length < 4)
			SetVelocity(GetEntity()->GetPos(),finaldir,finaldir*5);
	}
}


void CTrace::Destroy()
{
	CProjectile::Destroy();
	damage = 30;
	currentPos = Vec3(ZERO);
	pTarget = NULL;
	time = 0;
	dtime = 0;
	bActive = true;
}