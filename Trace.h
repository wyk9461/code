#ifndef _TRACE_H_
#define _TRACE_H_

#include "StdAfx.h"
#include "Projectile.h"

class CTrace : public CProjectile
{
	typedef CProjectile BaseClass;
public:
	CTrace();
	~CTrace();
	virtual bool Init(IGameObject *pGameObject);
	virtual void ReInitFromPool();
	virtual void HandleEvent(const SGameObjectEvent &);
	virtual void ProcessEvent(SEntityEvent &event);
	virtual void Update(SEntityUpdateContext &ctx, int updateSlot);
	void Destroy();
private:
	float damage;
	Vec3 currentPos;
	IEntity* pTarget;
	float time;
	float dtime;
	bool bActive;
};
#endif 