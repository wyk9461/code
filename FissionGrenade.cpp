#include "StdAfx.h"
#include "FissionGrenade.h"

CFissionGrenade::CFissionGrenade()
	: m_fissionTimes(0)
{
}

CFissionGrenade::~CFissionGrenade()
{
	m_fissionTimes = 0;
}

void CFissionGrenade::Explode(const CProjectile::SExplodeDesc& explodeDesc)
{
	BaseClass::Explode(explodeDesc);
	/*IParticleEffect *HitEffect = gEnv->pParticleManager->FindEffect("explosions.Grenade_SCAR.character");
	HitEffect->Spawn(true,IParticleEffect::ParticleLoc(this->GetLastPos()));*/
}


void CFissionGrenade::HandleEvent(const SGameObjectEvent &event)
{
	BaseClass::HandleEvent(event);
	if (event.event == eGFE_OnCollision)
	{
		if (m_fissionTimes < 2)
		{
			IEntityClass *pFissionGrenade=this->GetEntity()->GetClass();

			if(pFissionGrenade)
			{
				CFissionGrenade* pGrenade = static_cast<CFissionGrenade*>(g_pGame->GetWeaponSystem()->SpawnAmmo(pFissionGrenade, false));

				int idamage= CProjectile::m_damage;
				CProjectile::SProjectileDesc projectileDesc(
					m_ownerId, m_hostId, m_weaponId, idamage, 0, 0, 0, m_hitTypeId,
					0, false);

				EntityId actorId = g_pGame->GetClientActorId();

				m_fissionTimes++;

				pGrenade->SetParams(projectileDesc,m_fissionTimes);
				float x = Random(-1.0,1.0);
				float y = Random(-1.0,1.0);
				Vec3 dir = Vec3(x, y, 1.0);
				dir.normalize();
				pGrenade->Launch(this->GetLastPos(),dir,Vec3(ZERO),0.3);
			}
		}
	}
}

void CFissionGrenade::SetParams(const SProjectileDesc& projectileDesc, int fissionTimes)
{
	BaseClass::SetParams(projectileDesc);
	m_fissionTimes = fissionTimes;
}