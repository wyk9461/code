#ifndef _AIRGUN_H_
#define _AIRGUN_H_

#include "Weapon.h"

class CAirGun : public CWeapon
{
private:

	typedef CWeapon BaseClass;

public:

	CAirGun();
	virtual ~CAirGun();

	virtual void OnShoot(EntityId shooterId, EntityId ammoId, IEntityClass* pAmmoType, const Vec3 &pos, const Vec3 &dir, const Vec3 &vel);
	virtual void Update(SEntityUpdateContext& ctx, int);

private:
	void GetTarget();
	void OnPush();
	void AirPush(IEntity *pEntity);

private:
	float m_time;
	std::vector<IEntity*> liveVec;
	std::vector<IEntity*> objVec;
};

#endif