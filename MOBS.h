//Multiple Orbit Bombardment System
//As a kind of Bullet ask for it
//˵���˾����ӳٱ�ը= =
#ifndef _MOBS_H_
#define _MOBS_H_

#include "StdAfx.h"
#include "Projectile.h"

class CMOBS : public CProjectile
{
private:
	typedef CProjectile BaseClass;

public:
	CMOBS();
	~CMOBS();
	virtual void HandleEvent(const SGameObjectEvent &);
	virtual void Update(SEntityUpdateContext &ctx, int updateSlot);

private:
	Vec3 m_startPos;
	Vec3 m_dir;
	Vec3 m_hitPos;
	float m_intervalTime;
	float m_liveTime;
	bool bStart;
	bool bExStart;
};
#endif 