#ifndef _ENTITYROJECTILE_H_
#define _ENTITYROJECTILE_H_

#include "Projectile.h"

enum EntityProjectileType
{
	eBarbWireFence = 0		,
	eExplosive_Barrel		,
	eMediBox				,
	ePaperBox				,
	ePallet					,
	eBarrel1				,
	eBarrierConc			,		
	eFirehydrant			,		
	eBollard				,		
	eDumpster				,	
	eBench = 10				,
	ePlanter_1				,
	eCone					,
	eHotDog_Stand			,
	eBarbWireFence2			,
	eIronBollar				,
	eMetalCreat				,
	eBarrierMSmall			,
	eBusShelter				,
	eAdv_Board				,
	eGarbageCan = 20		,
	eFence_1				,
	eBusSign				,
	eMailBox				,
	ebarricade_fence_left	,
	eScaffold				,
	Garbage_Bag				,
	eFuel_Browser			,
	eBuilding_Wall			,
	ePoliceFence			,
	eOfficeTrolley = 30		,
	eBeer_Create			,
	eBig_Planter			,
	eCamera1				,
	eCanCap					,
	eCheckPoint_Fence		,
	eCheckPointWall			,
	eContainer				,
	eWood_Crate				,
	eDestroyed_Tank			,
	eHMMWV_Destroyed = 40	,
	eLarge_Sign				,
	eLong_Bench				,
	eMobileLight			,
	ePhoneBooth				,
	eParkLight				,
	ePailRail				,
	eSandBags				,
	eLarge_SandBeg			,
	eTruck					,




	eEntityType_Last		,
};

class CEntityProjectile : public CProjectile
{
private:
	int Type;
	int FunctionIndex;
	float Damage;
public:
	CEntityProjectile();
	virtual ~CEntityProjectile();
	// CProjectile
	virtual void HandleEvent(const SGameObjectEvent &event);
	virtual void Update(SEntityUpdateContext &ctx, int updateSlot);

	void SetType(int _type){Type = _type;}
	void SetFunctionIndex(int _index){FunctionIndex = _index;}
	void SetDamage(float _damage){Damage = _damage;}
	const float GetDamage(){return Damage;}
};

struct EntityProjectileParams
{
	//IParticleEffect *pTracerEffect;
	CTracerManager::STracerParams m_TracerParams;
	XmlString geom;
	XmlString effect;
	int FunctionIndex;
	int ammo_count;

	//WeaponParams
	int clipsize;
	int firerate;
	float min_spread;
	float max_spread;
	float attack_spread;
	float max_recoil;

	EntityProjectileParams &operator=(const EntityProjectileParams &rhs)
	{
		if(this == &rhs)
			return *this;
		m_TracerParams = rhs.m_TracerParams;
		geom = rhs.geom;
		effect = rhs.effect;
		FunctionIndex = rhs.FunctionIndex;
		clipsize = rhs.clipsize;
		ammo_count = rhs.ammo_count;
		firerate = rhs.firerate;
		min_spread = rhs.min_spread;
		max_spread = rhs.max_spread;
		attack_spread = rhs.attack_spread;
		max_recoil = rhs.max_recoil;
		return *this;
	}
};

struct EntityProjectileCache
{
	EntityProjectileParams *pParams;
	unsigned int unused_count;

	EntityProjectileCache &operator=(const EntityProjectileCache &rhs)
	{
		if(this == &rhs)
			return *this;
		pParams = rhs.pParams;
		unused_count = rhs.unused_count;
		return *this;
	}
};

struct ProcessFunParams
{
	CEntityProjectile *pProjectile;
	EventPhysCollision collision;
	IEntity *target; 
	float damage;
	int hitMatId;
	Vec3 hitDir;

	ProcessFunParams(CEntityProjectile *_pProjectile, const EventPhysCollision& _collision, IEntity *_target, float _damage, int _hitMatId, const Vec3& _hitDir)
	:	pProjectile(_pProjectile)
	,	collision(_collision)
	,	target(_target)
	,	damage(_damage)
	,	hitMatId(_hitMatId)
	,	hitDir(_hitDir)
	{}

	ProcessFunParams &operator=(const ProcessFunParams &rhs)
	{
		if(this == &rhs)
			return *this;
		pProjectile = rhs.pProjectile;
		collision = rhs.collision;
		target = rhs.target;
		damage = rhs.damage;
		hitMatId = rhs.hitMatId;
		hitDir = rhs.hitDir;
		return *this;
	}
};


typedef void (*pFun)(ProcessFunParams &);
typedef std::map<int,EntityProjectileParams> EProjectileMap;
typedef std::map<int,EntityProjectileCache> EPCacheMap;
typedef std::map<int,pFun> EProjectileFunctionMap;
typedef std::map<int,bool> EProjectileFunctionUnlockMap;

class EntityProjectileManager
{
private:
	EntityProjectileManager();
	EProjectileMap Projectiles;
	EPCacheMap Projectiles_Cache;
	EProjectileFunctionMap PFunctions;
	EProjectileFunctionUnlockMap FunctionUnlockState;
	XmlNodeRef root;
	XmlNodeRef UnlockList;
	unsigned int MaxCacheSlot;

	void RegisterFunction();
	void LRU_AddUnusedCount(int Type);
	void ReplaceWithLRU(int Type);
	void ReadUnlock();
	void SaveUnlockStatus(int index);
public:
	static EntityProjectileManager &Instance(){static EntityProjectileManager instance; return instance;}
	void RegisterProcessFunction(pFun _fun, int _index);
	void ReadParams();

	EntityProjectileParams &GetProjectileParams(int Type);
	void ProcessDiffCollision(int fun_index, ProcessFunParams& pfp);
	ILINE const bool IsProjectileFunctionUnlocked(int FunctionIndex){if(FunctionUnlockState.count(FunctionIndex)) return FunctionUnlockState[FunctionIndex]; return false;}
	ILINE void SetFunctionUnlockStatus(int index, bool unlock){if(FunctionUnlockState.count(index)) FunctionUnlockState[index] = unlock;SaveUnlockStatus(index);}
};


#endif