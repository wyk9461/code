#include "StdAfx.h"
#include "ProbeManager.h"
#include "Code_System/BasicRayCast.h"
#include "PlayerCamera.h"
#include "CameraModes.h"
#include "GameActions.h"
#include "Player.h"
#include "Code_System/ScreenEffectAdaptor.h"
#include "EnemyScan.h"


void ProbeManager::ControlProbe()
{
	if(!isProbeLaunched)
		LaunchProbe();
	else
	{
		if(isProbeActive)
		{
			SwitchToPlayer();
			//RestorePlayerInventory();
		}
		else
		{
			SwitchToProbe();
			//ActiveProbe();
			//StorePlayerInventory();
		}
	}
}

void ProbeManager::Update(float frameTime)
{
	if(isProbeActive)
	{
		primitives::sphere  sphere;
		sphere.r = 1;
		sphere.center = GetScanPos();
		PhysSkipList skipList;
		skipList.clear();
		hit_dist = gEnv->pPhysicalWorld->PrimitiveWorldIntersection(sphere.type, &sphere, Vec3Constants<float>::fVec3_Zero, ent_all, &pContact, 0, (rwi_ignore_noncolliding | rwi_stop_at_pierceable), 0, 0, 0, !skipList.empty() ? &skipList[0] : NULL, skipList.size());
	}

	if(isZoomStarted)
	{
		if(ZoomParams.curFovScale > ZoomParams.FinalFovScale)
		{
			ZoomParams.curFovScale -= min(ZoomParams.ZoomRate, abs(ZoomParams.FinalFovScale - ZoomParams.curFovScale) );
		}
		else
		{
			ZoomParams.curFovScale += min(ZoomParams.ZoomRate, abs(ZoomParams.FinalFovScale - ZoomParams.curFovScale) );
		}

		CActor *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetClientActor());
		SActorParams &actorParams = pActor->GetActorParams();
		actorParams.viewFoVScale = ZoomParams.curFovScale;

		if(abs(ZoomParams.curFovScale - ZoomParams.FinalFovScale) < 0.001)
			isZoomStarted = false;
	}

	if(isMovingStarted)
	{
		float TerrainHeight = gEnv->p3DEngine->GetTerrainElevation(ProbeScanPos.x, ProbeScanPos.y);
		float CenterHeight = TerrainHeight + DefaultHeight;
		float diffHeight = ProbeScanPos.z < CenterHeight ? ProbeScanPos.z - (TerrainHeight+3): (CenterHeight + 10) - ProbeScanPos.z;
		float factor = diffHeight;

		if(ProbeScanPos.z < CenterHeight && MovingParams.MovingDir.z > 0)
		{
			if(factor < 0.1)
			{
				factor = 0.1;
			}
		}
		else if(ProbeScanPos.z >= CenterHeight && MovingParams.MovingDir.z < 0)
		{
			if(factor < 0.1)
			{
				factor = 0.1;
			}
		}

		if(factor > 1)
			factor = 1;

		Vec3 NewVelocity = LERP(Vec3(0,0,0),MovingParams.MovingDir * 0.6,MovingParams.Interpolation * factor);
		MovingParams.Velocity = NewVelocity;
		ProbeScanPos += MovingParams.Velocity;
		if(!isMovingButtomHold)
			MovingParams.Interpolation -= MovingParams.deAcceleration;

		if(NewVelocity.GetLengthSquared() < 0.01 || MovingParams.Interpolation <= 0)
		{
			isMovingStarted = false;
			MovingParams.MovingDir = Vec3(0,0,0);
			MovingParams.Interpolation = 0;
		}
	}
}

void ProbeManager::ChangeFovScale(float _scale)
{
	ZoomParams.FinalFovScale += _scale;

	if(ZoomParams.FinalFovScale >= MAX_FOV_SCALE)
		ZoomParams.FinalFovScale = MAX_FOV_SCALE;

	if(ZoomParams.FinalFovScale < MIN_FOV_SCALE)
		ZoomParams.FinalFovScale = MIN_FOV_SCALE;

	isZoomStarted = true;
}

void ProbeManager::ChangeHeight(float _height)
{
	if(_height > 0)
	{
		if(!CollisionDetect(Vec3(0,0,1)))
		{
			MovingParams.MovingDir.z = 1;
			MovingParams.Interpolation += min(MovingParams.Acceleration,1-MovingParams.Interpolation);
			isMovingStarted = true;
		}
	}
	else
	{
		if(!CollisionDetect(Vec3(0,0,-1)))
		{
			MovingParams.MovingDir.z = -1;
			MovingParams.Interpolation += min(MovingParams.Acceleration,1-MovingParams.Interpolation);
			isMovingStarted = true;
		}
	}
	//MovingParams.MovingDir.normalize();
}

void ProbeManager::ChangeSpin(bool _left)
{
	//MovingParams.Interpolation += min(MovingParams.Acceleration,1-MovingParams.Interpolation);
	if(!_left)
	{
		MovingParams.SpinFcator += min((float)0.002,1-MovingParams.SpinFcator);
	}
	else
	{
		MovingParams.SpinFcator -= 0.002;
		if(MovingParams.SpinFcator < -1)
			MovingParams.SpinFcator = -1;
	}

	float r = 10 * (1+MovingParams.SpinFcator);
	float x = DefaultPos.x + r * cos(DEG2RAD(MovingParams.SpinFcator * 360));
	float y = DefaultPos.y + r * sin(DEG2RAD(MovingParams.SpinFcator * 360));
	float diff_x =  r * cos(DEG2RAD(MovingParams.SpinFcator * 360));
	float diff_y = r * sin(DEG2RAD(MovingParams.SpinFcator * 360));
	Vec3 NextPos = Vec3(x,y,ProbeScanPos.z);

	Vec3 dir = (NextPos - GetScanPos()).normalize();
	if(!CollisionDetect(dir))
	{
		ProbeScanPos = NextPos;
	}
	else
	{
		if(!_left)
		{
			MovingParams.SpinFcator -= 0.002;
		}
		else
		{
			MovingParams.SpinFcator += 0.002;
		}
	}
	/*MovingParams.MovingDir = NextPos - ProbeScanPos;
	MovingParams.MovingDir.normalize();*/
	//isMovingStarted = true;
}

void ProbeManager::SwitchToProbe()
{
	if(FirstSet)
	{
		ProbeScanPos = TheProbe->GetPos() + Vec3(0,0,DefaultHeight);
		DefaultPos = ProbeScanPos;
		FirstSet = false;
	}

	CPlayer *pPlayer = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	CPlayerCamera *pCamera = pPlayer->GetPlayerCamera();
	g_pGame->Actions().FilterNoMove()->Enable(true);
	pPlayer->SetThirdPerson(true);

	CSwitchToCodeSpecialCameraMode *pMode = static_cast<CSwitchToCodeSpecialCameraMode*>(pCamera->GetCameraMode(eCameraMode_SwitchToCodeSpecial));
	pMode->Init(gEnv->pRenderer->GetCamera().GetPosition(), ProbeScanPos, pPlayer->GetEntity()->GetRotation(),true);

	pCamera->SetCameraMode(eCameraMode_SwitchToCodeSpecial,"Code Special");

	TheProbe->EnablePhysics(false);
	isProbeActive = true;

	/*ScreenEffectAdaptor &Instance = ScreenEffectAdaptor::TheAdaptor();
	effect_params params;
	params.effect_name = "Global_User_Brightness";
	gEnv->p3DEngine->GetPostEffectParam(params.effect_name, params.cur_value);
	params.last_time = (float)(1 / 0.02 * gEnv->pTimer->GetFrameTime() + 1);
	params.end_value = -1;
	params.reverse = false;
	//params.reverseId = eETID_SwitchToProbeCamera_Reverse;
	Instance.AddToQueue(params,eETID_SwitchToProbeCamera);*/
}

void ProbeManager::SwitchToPlayer()
{
	CPlayer *pPlayer = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	CPlayerCamera *pCamera = pPlayer->GetPlayerCamera();
	/*g_pGame->Actions().FilterNoMove()->Enable(true);
	pPlayer->SetThirdPerson(false);*/

	CSwitchToCodeSpecialCameraMode *pMode = static_cast<CSwitchToCodeSpecialCameraMode*>(pCamera->GetCameraMode(eCameraMode_SwitchToCodeSpecial));
	pMode->Init(GetScanPos(), pPlayer->GetEntity()->GetPos() + pPlayer->GetEyeOffset(), pPlayer->GetEntity()->GetRotation(),false);

	pCamera->SetCameraMode(eCameraMode_SwitchToCodeSpecial,"Code Special");

	TheProbe->EnablePhysics(false);
}

void ProbeManager::ActiveProbe()
{
	CPlayer *pPlayer = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	CPlayerCamera *pCamera = pPlayer->GetPlayerCamera();
	g_pGame->Actions().FilterNoMove()->Enable(true);
	pPlayer->SetThirdPerson(true);
	pCamera->SetCameraMode(eCameraMode_CodeSpecial,"Code Special");


	BasicRayCast *pBRC = g_pGame->GetBasicRayCast();
	DefaultRayCastLength = pBRC->GetDistance();

	SActorParams &actorParams = pPlayer->GetActorParams();
	actorParams.viewFoVScale = ZoomParams.curFovScale;

	pBRC->ChangeDistance(ProbeRadius);

	//gEnv->pAISystem->GetNavigationSystem()->StartWorldMonitoring();

	/*ScreenEffectAdaptor &Instance = ScreenEffectAdaptor::TheAdaptor();
	effect_params params;
	params.effect_name = "Global_User_Brightness";
	gEnv->p3DEngine->GetPostEffectParam(params.effect_name, params.cur_value);
	params.last_time = 1.5;
	params.end_value = 1;
	params.reverse = false;
	//params.reverseId = eETID_SwitchToProbeCamera_Reverse;
	Instance.AddToQueue(params,eETID_SwitchToProbeCamera_Reverse);*/
}

void ProbeManager::DeactiveProbe()
{
	CPlayer *pPlayer = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	CPlayerCamera *pCamera = pPlayer->GetPlayerCamera();
	g_pGame->Actions().FilterNoMove()->Enable(false);
	pPlayer->SetThirdPerson(false);
	pCamera->SetCameraMode(eCameraMode_Default,"Back to Default");

	BasicRayCast *pBRC = g_pGame->GetBasicRayCast();
	pBRC->ChangeDistance(DefaultRayCastLength);

	SActorParams &actorParams = pPlayer->GetActorParams();
	actorParams.viewFoVScale = 1.0f;

	isProbeActive = false;

	//gEnv->pAISystem->GetNavigationSystem()->StopWorldMonitoring();

	/*ScreenEffectAdaptor &Instance = ScreenEffectAdaptor::TheAdaptor();
	effect_params params;
	params.effect_name = "ImageGhosting_Amount";
	gEnv->p3DEngine->GetPostEffectParam(params.effect_name, params.cur_value);
	params.init_value = 1.5;
	params.last_time = 1.5;
	params.end_value = 0;
	params.reverse = false;
	//params.reverseId = eETID_SwitchToProbeCamera_Reverse;
	Instance.AddToQueue(params,eEDIT_ProbeGrain);*/
}

void ProbeManager::LaunchProbe()
{
	IItem *pItem = g_pGame->GetIGameFramework()->GetClientActor()->GetCurrentItem(false);
	if(pItem)
	{
		CWeapon *pWeapon = static_cast<CWeapon*>(pItem->GetIWeapon());
		if(pWeapon)
		{
			BasicRayCast *pBRC = g_pGame->GetBasicRayCast();
			if(!TheProbe)
			{
				SEntitySpawnParams params;
				params.pClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("CodeProbe");
				params.sName = "CodeProbe";
				params.vPosition = pWeapon->GetSlotHelperPos(eIGS_FirstPerson,"silencer_attach",true);
				params.nFlags = ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CUSTOM_VIEWDIST_RATIO | ENTITY_FLAG_CALC_PHYSICS;
				TheProbe = gEnv->pEntitySystem->SpawnEntity(params);

				SEntityPhysicalizeParams pparams;
				pparams.mass = 15;
				pparams.density = -1;
				pparams.nSlot= -1;
				pparams.type = 2;
				TheProbe->Physicalize(pparams);
				TheProbe->EnablePhysics(true);
				TheProbe->SetUpdatePolicy(ENTITY_UPDATE_ALWAYS);
			}
			TheProbe->Hide(false);
			TheProbe->SetPos(pWeapon->GetSlotHelperPos(eIGS_FirstPerson,"silencer_attach",true));

			//Set ViewDist Ratio
			IEntityRenderProxy *pRenderProxy = (IEntityRenderProxy*)TheProbe->GetProxy(ENTITY_PROXY_RENDER);
			pRenderProxy = (IEntityRenderProxy*)TheProbe->GetProxy(ENTITY_PROXY_RENDER);
			if (pRenderProxy)
			{
				pRenderProxy->GetRenderNode()->SetViewDistRatio(255);
			}

			//Add Impulse
			IEntityPhysicalProxy *pPhysicsProxy = static_cast<IEntityPhysicalProxy*>(TheProbe->GetProxy(ENTITY_PROXY_PHYSICS));
			if(pPhysicsProxy)
			{
				pPhysicsProxy->AddImpulse(-1, TheProbe->GetPos(), gEnv->pRenderer->GetCamera().GetViewdir() * 500, false, 1);
			}

			//ProbeScanPos = TheProbe->GetPos() + Vec3(0,0,DefaultHeight);
			isProbeLaunched = true;
			DefaultPos = ProbeScanPos;
		}
	}
}

void ProbeManager::RecycleProbe()
{
	TheProbe->Hide(true);
	isProbeActive = false;
	isProbeLaunched = false;
	FirstSet = true;
	EnemyScan::theScan().ClearAllSign();
	DeactiveProbe();
}

//------------------------------------------------------------------------
void ProbeManager::StorePlayerInventory()
{
	CActor *pSelfActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	CItemSystem* pItemSystem = static_cast<CItemSystem*> (g_pGame->GetIGameFramework()->GetIItemSystem());
	/*IItem *pItem=pSelfActor->GetCurrentItem(false);
	store_weapon_name=pItem->GetEntity()->GetName();*/
	pItemSystem->SerializePlayerLTLInfo(false);
	IInventory* pInventory = pSelfActor->GetInventory();
	pInventory->RMIReqToServer_RemoveAllItems();
}

//------------------------------------------------------------------------
void ProbeManager::RestorePlayerInventory()
{
	CActor *pSelfActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	CItemSystem* pItemSystem = static_cast<CItemSystem*> (g_pGame->GetIGameFramework()->GetIItemSystem());
	pItemSystem->SerializePlayerLTLInfo(true);
	//pSelfActor->SelectItemByName(store_weapon_name,true);
}

//------------------------------------------------------------------------
void ProbeManager::Reset()
{
	DeactiveProbe();
	if(TheProbe)
	{
		gEnv->pEntitySystem->RemoveEntity(TheProbe->GetId(),true);
		TheProbe = NULL;
		isProbeActive = false;
		isProbeLaunched = false;
	}
}

//------------------------------------------------------------------------
const bool ProbeManager::CollisionDetect(const Vec3 &_moving_dir)
{
	if(hit_dist == 0)
		return false;
	Vec3 ContactDir = pContact->pt - GetScanPos();
	ContactDir.normalize();
	//gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(GetScanPos(), ColorB(255, 255, 255), GetScanPos() + ContactDir, ColorB(255, 255, 255));
	float cos_value = ContactDir.Dot(_moving_dir);
	float ang = RAD2DEG(acosf(cos_value));
	if(ang < 60)
		return true;
	return false;
}

//------------------------------------------------------------------------
void ProbeManager::LinkEntity()
{
	BasicRayCast *pBRC = g_pGame->GetBasicRayCast();
	IEntity *pEntity = pBRC->GetEntity();
	if(pLinkedEntity_1 == NULL)
		pLinkedEntity_1 = pEntity;
	else if(pLinkedEntity_2 == NULL)
		pLinkedEntity_2 = pEntity;

	if(pLinkedEntity_1 && pLinkedEntity_2)
	{
		if(pLinkedEntity_1->GetClass() == EntityClassPreLoad::Instance().pPressureTrigger)
		{
			pLinkedEntity_1->AddEntityLink("Trigger_To_Entity",pLinkedEntity_2->GetId());
			pLinkedEntity_2->AddEntityLink("Entity_To_Trigger",pLinkedEntity_1->GetId());
			SpawnLinkParticle(pLinkedEntity_1,pLinkedEntity_2);

		}
		else if(pLinkedEntity_2->GetClass() == EntityClassPreLoad::Instance().pPressureTrigger)
		{
			pLinkedEntity_2->AddEntityLink("Trigger_To_Entity",pLinkedEntity_1->GetId());
			pLinkedEntity_1->AddEntityLink("Entity_To_Trigger",pLinkedEntity_2->GetId());
			SpawnLinkParticle(pLinkedEntity_2,pLinkedEntity_1);
		}
		pLinkedEntity_1 = NULL;
		pLinkedEntity_2 = NULL;
	}
}

//------------------------------------------------------------------------
void ProbeManager::UnlinkEntity()
{
	BasicRayCast *pBRC = g_pGame->GetBasicRayCast();
	IEntity *pEntity = pBRC->GetEntity();
	if(pEntity)
	{
		IEntityLink *pLink = pEntity->GetEntityLinks();
		while(pLink)
		{
			IEntity *pLinkedEntity = gEnv->pEntitySystem->GetEntity(pLink->entityId);
			if(pLinkedEntity)
			{
				IEntityLink *pRemovedLink = GetRemovedLink(pLinkedEntity, pEntity->GetId());
				DeleteLinkParticle(pEntity,pLink->entityId);
				pLinkedEntity->RemoveEntityLink(pRemovedLink);
			}
			pLink = pLink->next;
		}
		pEntity->RemoveAllEntityLinks();
	}
}

//------------------------------------------------------------------------
void ProbeManager::UnlinkEntity(IEntity *pSource)
{
	if(pSource)
	{
		IEntityLink *pLink = pSource->GetEntityLinks();
		while(pLink)
		{
			IEntity *pLinkedEntity = gEnv->pEntitySystem->GetEntity(pLink->entityId);
			if(pLinkedEntity)
			{
				IEntityLink *pRemovedLink = GetRemovedLink(pLinkedEntity, pSource->GetId());
				DeleteLinkParticle(pSource,pLink->entityId);
				pLinkedEntity->RemoveEntityLink(pRemovedLink);
			}
			pLink = pLink->next;
		}
		pSource->RemoveAllEntityLinks();
	}
}

//------------------------------------------------------------------------
IEntityLink *ProbeManager::GetRemovedLink(IEntity *pEntity, EntityId Id)
{
	IEntityLink *pLink = pEntity->GetEntityLinks();
	while(pLink)
	{
		if(pLink->entityId == Id)
		{
			DeleteLinkParticle(pEntity,Id);
			return pLink;
		}	
		pLink = pLink->next;
	}
	return NULL;
}

//------------------------------------------------------------------------
void ProbeManager::SpawnLinkParticle(IEntity *pSource, IEntity *pTarget)
{
	IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Code_System.Trigger_Indicator.Indicator_1");
	int SlotNum = FindAvailableSlot(pSource);
	if(SlotNum == -1)
		return;

	pSource->LoadParticleEmitter(SlotNum,pEffect);
	IParticleEmitter *pEmitter=pSource->GetParticleEmitter(SlotNum);
	if(pEmitter)
	{
		AABB aabb;
		pSource->GetWorldBounds(aabb);
		pEmitter->SetLocation(IParticleEffect::ParticleLoc(aabb.GetCenter(),Vec3(0,0,1)));
		SpawnParams Emitter_Params;
		pEmitter->GetSpawnParams(Emitter_Params);
		Emitter_Params.eAttachType = GeomType_None;

		ParticleTarget target;
		target.bPriority = true;
		target.bTarget = true;
		target.fRadius = 0.1;
		target.vTarget = pTarget->GetPos();
		target.vVelocity = Vec3(ZERO);

		pEmitter->SetTarget(target);
		pEmitter->SetSpawnParams(Emitter_Params);
		pEmitter->Activate(true);
	}
}

//------------------------------------------------------------------------
void ProbeManager::DeleteLinkParticle(IEntity *pSource, EntityId EmiterId)
{
	for(int i=0; i<10; ++i)
	{
		IParticleEmitter *pEmitter=pSource->GetParticleEmitter(i);
		if(pEmitter)
		{
			gEnv->pParticleManager->DeleteEmitter(pEmitter);
		}
	}
}

const unsigned int ProbeManager::FindAvailableSlot(IEntity *pTarget)
{
	for(int i=1; i<10; ++i)
	{
		if(!pTarget->IsSlotValid(i))
			return i;
		//bool valid = pTarget->IsSlotValid(i);
	}
	return -1;
}