#include "StdAfx.h"
#include "StrongholdManager.h"
#include "Code_System/CodeObject/AISpawnPoint.h"
#include "Code_System/Unlock.h"

StrongholdManager::StrongholdManager()
:	update_timer(0.f)
{

}

void StrongholdManager::RegisterStronghold(EntityId Id, int curlevel, int levelcap, float refreash_time)
{
	if(!Strongholds.count(Id))
	{
		StrongholdStatus new_stronghold;
		new_stronghold.Level = curlevel;
		new_stronghold.LevelCap = levelcap;
		new_stronghold.RefreashTime = refreash_time;
		new_stronghold.refreash_timer = refreash_time;
		AssaignNewTraits(new_stronghold, Id);
		Strongholds[Id] = new_stronghold;
		if(Strongholds.size() == 1)
			Iter = Strongholds.begin();
	}
}

void StrongholdManager::ReceiveStrongholdClearMessage(EntityId Id)
{
	Strongholds[Id].isClear = true;

	CalculateExperience(Id);
	StopRecord(Strongholds[Id]);
	ResetStrongholdTraitsStatus(Strongholds[Id]);
	int PossibleUnlockLevel = 1;
	//int PossibleUnlockLevel = 1 + cry_rand() % Strongholds[Id].Level;
	XmlString UnlockName = UnlockSystem::Instance().Unlock(PossibleUnlockLevel);

	IUIElement *pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	if(pElement)
	{
		SUIArguments args;
		string MainString("STRONGHOLD CLEAR");
		string SecondaryString(UnlockName + " Unlocked");
		args.AddArgument(MainString.c_str());
		args.AddArgument(SecondaryString.c_str());
		args.AddArgument(4);
		pElement->CallFunction("SetNotice", args);
	}

}

void StrongholdManager::Update(float frameTime)
{
	if(update_timer < 0.4 || Strongholds.empty())
		update_timer += frameTime;
	else
	{
		update_timer = 0;
		StrongholdStatus &status = Iter->second;
		CalculateWithin(status);
		UpdateRefreashStatus(status);

		++Iter;
		if(Iter == Strongholds.end())
			Iter = Strongholds.begin();
	}

	if(!Strongholds.empty())
	{
		std::map<EntityId, StrongholdStatus>::iterator iter = Strongholds.begin();
		std::map<EntityId, StrongholdStatus>::iterator end = Strongholds.end();
		while(iter != end)
		{
			UpdateRecord(iter->second,frameTime);
			ActiveStrongholdTraits(iter->second, frameTime);
			++iter;
		}
	}
}

void StrongholdManager::CalculateWithin(StrongholdStatus &status)
{
	IActor *pPlayer = g_pGame->GetIGameFramework()->GetClientActor();
	if(pPlayer)
	{
		IAreaManager* pAreaManager = gEnv->pEntitySystem->GetAreaManager();
		const int k_shapeArraySize = 16;
		int shapeArrayCount = k_shapeArraySize;
		EntityId AreaId(0);
		pAreaManager->GetLinkedAreas(Iter->first,&AreaId, shapeArrayCount);
		IEntity *pArea = gEnv->pEntitySystem->GetEntity(AreaId);

		IEntityAreaProxy *pAreaProxy = (IEntityAreaProxy*)pArea->GetProxy(ENTITY_PROXY_AREA);
		IEntity *pPlayerEntity = pPlayer->GetEntity();
		const Vec3 &vPlayerPos = pPlayerEntity->GetPos();
		status.isWithin = pAreaProxy->CalcPointWithin(INVALID_ENTITYID, vPlayerPos, true);
	}
}

void StrongholdManager::UpdateRefreashStatus(StrongholdStatus &status)
{
	if(status.refreash_timer < status.RefreashTime)
	{
		if(status.isClear && !status.isWithin)
			status.refreash_timer += 1.f;		//1 second
	}
	else
	{
		if(status.isWithin)
		{
			status.isClear = false;
			status.refreash_timer = 0;
			RebootStronghold();
			StartRecord(Iter->second);
		}
	}

	if(!status.isClear && status.isWithin && !status.isRecordStarted)
		StartRecord(Iter->second);
}

void StrongholdManager::RebootStronghold()
{
	IEntity *pStronghold = gEnv->pEntitySystem->GetEntity(Iter->first);
	CRY_ASSERT_MESSAGE(pStronghold, "Stronghold not Available");
	IEntityLink *pLink = pStronghold->GetEntityLinks();
	while(pLink)
	{
		IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pLink->entityId);
		if(pEntity)
		{
			CAISpawnPoint* pAISpawnPoint = static_cast<CAISpawnPoint*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pLink->entityId, "AISpawnPoint"));
			if(pAISpawnPoint)
			{
				pAISpawnPoint->ClearSpawnStatus();
				pAISpawnPoint->SetEnable(true);
				pAISpawnPoint->SetLevel(Iter->second.Level);
			}
		}
		pLink = pLink->next;
	}

	Iter->second.Records.Reset();
}

void StrongholdManager::StartRecord(StrongholdStatus &status)
{
	status.isRecordStarted = true;
	if(!status.isRecordBreak)
	{
		status.Records.Init();
	}
}

void StrongholdManager::BreakRecord(StrongholdStatus &status)
{
	status.isRecordStarted = false;
	status.isRecordBreak = true;
}

void StrongholdManager::StopRecord(StrongholdStatus &status)
{
	status.isRecordStarted = false;
	status.isRecordBreak = false;
}

void StrongholdManager::UpdateRecord(StrongholdStatus &status, float frameTime)
{
	CodeExclusion &ExclusionInstance = CodeExclusion::Instance();
	if(status.isRecordStarted)
	{
		float ExclusionAmount = ExclusionInstance.GetExclusionAmount();
		if(ExclusionAmount > status.Records.MaxExclusionLevel)
			status.Records.MaxExclusionLevel  = ExclusionAmount;

		int StabilityAmount = g_pGame->GetWorldStability()->Getstability();
		if(StabilityAmount < status.Records.MinStabilityLevel)
			status.Records.MinStabilityLevel = StabilityAmount;

		status.Records.BattleDuration+=frameTime;

		if(!status.isWithin)
			BreakRecord(status);
	}
}


void StrongholdManager::CalculateExperience(EntityId Id)
{
	StrongholdStatus &status = Strongholds[Id];
	StrongholdStatus::BattleRecord &record = status.Records;
	record.ClearingEfficiency = record.EnemyCapability / record.BattleDuration;
	float ExperienceGain(0.f);
	ExperienceGain += record.EnemyCapability;
	ExperienceGain += static_cast<float>((record.InitStabilityLevel - record.MinStabilityLevel) * 0.5);
	ExperienceGain += static_cast<float>((record.MaxExclusionLevel - record.InitExclusionLevel) * 2);
	status.experience += ExperienceGain;
	while(status.experience > status.experience_needed)
	{
		status.experience -= status.experience_needed;
		StrongholdLevelUp(status, Id);
	}
}

void StrongholdManager::ReceiveStrongholdEliminateMessage(EntityId Id, float capability)
{
	Strongholds[Id].Records.EnemyCapability += capability;
}

void StrongholdManager::StrongholdLevelUp(StrongholdStatus &status, EntityId Id)
{
	if(status.Level >= status.LevelCap)
	{
		status.Level = status.LevelCap;
		return;
	}

	++status.Level;
	AssaignNewTraits(status, Id);
}

#define eST_Count(f) eST_Lv##f##_Count 
void StrongholdManager::AssaignNewTraits(StrongholdStatus &status, EntityId Id)
{
	int level = status.Level;
	switch (level)
	{
	case 1:
		{
			int type = cry_rand() % eST_Count(1);
			boost::shared_ptr<IStrongholdTraits> ptr = CreateTraits_Lv1(type, Id);
			CRY_ASSERT_MESSAGE(ptr !=0, "No Traits Generated!");
			status.Traits.push_back(ptr);
			break;
		}
	default:
		break;
	}
}

#define SWITCH_AND_CREATE_TRAITS(name,level,id)\
	case eST_##name##:\
	{\
		Stronghold##name##Traits *traits = new Stronghold##name##Traits;\
		boost::shared_ptr<IStrongholdTraits> ptr(traits);\
		ptr->SetStronghold(id);\
		ptr->SetEffectLevel(level);\
		return ptr;\
	}\

boost::shared_ptr<IStrongholdTraits> StrongholdManager::CreateTraits_Lv1(int type, EntityId Id)
{
	switch (type)
	{
		SWITCH_AND_CREATE_TRAITS(Regeneration,1,Id);
		SWITCH_AND_CREATE_TRAITS(Tough,1,Id);
	default:
		return NULL;
	}
}


void StrongholdManager::SetStrongholdTargets(EntityId StrongholdId, IEntity *pTarget, int flag)
{
	StrongholdStatus &status = Strongholds[StrongholdId];
	std::deque<boost::shared_ptr<IStrongholdTraits> >::iterator iter = status.Traits.begin();
	std::deque<boost::shared_ptr<IStrongholdTraits> >::iterator end = status.Traits.end();
	while(iter != end)
	{
		int TraitsFlag = (*iter)->GetAffectType();
		if(TraitsFlag == flag)
		{
			(*iter)->SetTarget(pTarget);
		}
		++iter;
	}
}

void StrongholdManager::ResetStrongholdTraitsStatus(StrongholdStatus &status)
{
	std::deque<boost::shared_ptr<IStrongholdTraits> >::iterator iter = status.Traits.begin();
	std::deque<boost::shared_ptr<IStrongholdTraits> >::iterator end = status.Traits.end();
	while(iter != end)
	{
		(*iter)->Reset();
		++iter;
	}
}

void StrongholdManager::ActiveStrongholdTraits(StrongholdStatus &status, float frameTime)
{
	std::deque<boost::shared_ptr<IStrongholdTraits> >::iterator iter = status.Traits.begin();
	std::deque<boost::shared_ptr<IStrongholdTraits> >::iterator end = status.Traits.end();
	while(iter != end)
	{
		(*iter)->Active(frameTime);
		++iter;
	}
}