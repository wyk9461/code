#include "StdAfx.h"
#include "FallenActorManager.h"


FallenActorManager::FallenActorManager()
	:	update_time(1.f)
	,	tick_timer(0.0){}


void FallenActorManager::Update(float frameTime)
{
	if(FallenActors.empty())
		return;


	if(tick_timer < update_time)
		tick_timer += frameTime;
	else
	{
		tick_timer = 0;
		FallenActorMap::iterator iter = FallenActors.begin();
		FallenActorMap::iterator end = FallenActors.end();
		while(iter != end)
		{
			FallenActorParams &params = iter->second;
			const Vec3 &currentPos = params.pActor->GetEntity()->GetPos();
			float length_seq = (currentPos - params.LastPos).GetLengthSquared();
			if(length_seq < 0.0001)
			{
				FallenRecovery(params.pActor);
				FallenActors.erase(iter++);
			}
			else
			{
				params.LastPos = currentPos;
				++iter;
			}
		}
	}
}

void FallenActorManager::AddToManager(CActor *pActor)
{
	if(pActor && !pActor->IsDead())
	{
		EntityId Id = pActor->GetEntityId();
		if(FallenActors.count(Id))
			return;
		FallenActorParams params;
		params.pActor = pActor;
		params.LastPos = pActor->GetEntity()->GetPos();
		FallenActors[Id] = params;
		pActor->Fall_Always();
	}
}

void FallenActorManager::FallenRecovery(CActor *pActor)
{
	if(!pActor->IsDead())
	{
		pActor->RecoveryFromFall();
	}
}