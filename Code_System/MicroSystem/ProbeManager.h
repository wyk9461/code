#ifndef _PROBEMANAGER_H_
#define _PROBEMANAGER_H_

#define MAX_FOV_SCALE 1.0f
#define MIN_FOV_SCALE 0.3f

struct ProbeZoomParams
{
	float curFovScale;
	float FinalFovScale;
	float ZoomRate;
	ProbeZoomParams()
	:	curFovScale(1.f)
	,	FinalFovScale(1.f)
	,	ZoomRate(0.01){}
};

struct ProbeMovingParams
{
	Vec3 Velocity;
	Vec3 MovingDir;
	float Acceleration;
	float deAcceleration;
	float Interpolation;
	float SpinFcator;
	ProbeMovingParams()
	:	Velocity(Vec3(0,0,0))
	,	MovingDir(0,0,1)
	,	Acceleration(0.1f)
	,	deAcceleration(0.06f)
	,	Interpolation(0.0)
	,	SpinFcator(-1.0){}
};


class ProbeManager
{
public:
	static ProbeManager &Instance(){static ProbeManager instance;return instance;}
	void Update(float frameTime);
	void ControlProbe();

	void LaunchProbe();
	void ActiveProbe();
	void DeactiveProbe();
	void RecycleProbe();

	void ChangeFovScale(float _scale);
	void ChangeHeight(float _height);
	void ChangeSpin(bool _left);

	void LinkEntity();
	void UnlinkEntity();
	void UnlinkEntity(IEntity *pSource);
	const bool CollisionDetect(const Vec3 &_moving_dir);

	void Reset();
	ILINE void SetDefaultRayCastLength(float _length){DefaultRayCastLength = _length;}
	ILINE const float GetDefaultRayCastLength(){return DefaultRayCastLength;}
	ILINE void SetProbeRadius(float _radius){ProbeRadius = _radius;}
	ILINE const float GetProbeRadius(){return ProbeRadius;}
	ILINE const IEntity *GetProbe(){return TheProbe;}
	ILINE const bool IsProbeActived(){return isProbeActive;}
	ILINE const Vec3 &GetScanPos(){return ProbeScanPos;}
	ILINE void SetButtomHold(bool _hold){isMovingButtomHold = _hold;}

	~ProbeManager(){}
private:
	ProbeManager()
	:	DefaultRayCastLength(15.f)
	,	ProbeRadius(50.f)
	,	isProbeActive(false)
	,	isProbeLaunched(false)
	,	TheProbe(NULL)
	,	ZoomParams()
	,	isZoomStarted(false)
	,	isMovingStarted(false)
	,	isMovingButtomHold(false)
	,	FirstSet(true)
	,	ProbeScanPos(Vec3(0,0,0))
	,	DefaultPos(Vec3(0,0,0))
	,	DefaultHeight(12.f)
	,	pContact(NULL)
	,	hit_dist(0.f)
	,	pLinkedEntity_1(NULL)
	,	pLinkedEntity_2(NULL){}

	void SwitchToProbe();
	void SwitchToPlayer();
	void StorePlayerInventory();
	void RestorePlayerInventory();
	const unsigned int FindAvailableSlot(IEntity *pTarget);

	IEntityLink *GetRemovedLink(IEntity *pEntity, EntityId Id);
	void SpawnLinkParticle(IEntity *pSource, IEntity *pTarget);
	void DeleteLinkParticle(IEntity *pSource, EntityId EmiterId);
	float DefaultRayCastLength;
	float ProbeRadius;

	ProbeZoomParams ZoomParams;
	ProbeMovingParams MovingParams;

	bool isProbeActive;
	bool isProbeLaunched;
	bool isZoomStarted;
	bool isMovingStarted;
	bool isMovingButtomHold;
	bool FirstSet;

	IEntity *TheProbe;
	Vec3 ProbeScanPos;
	Vec3 DefaultPos;
	geom_contact*  pContact;
	float hit_dist;
	float DefaultHeight;

	IEntity *pLinkedEntity_1;
	IEntity *pLinkedEntity_2;
};
#endif