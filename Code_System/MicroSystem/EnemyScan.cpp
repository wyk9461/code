#include "StdAfx.h"
#include "EnemyScan.h"
#include "Code_System/MapIndicator.h"
#include "IFlashUI.h"
#include "Code_System/BattleChallenge.h"
#include "ProbeManager.h"

EnemyScan::EnemyScan()
:	tick_timer(0)
,	scan_delay(1)
{
	pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	UI_IDPool.SetMaxIdAmount(200);
}

void EnemyScan::Update(float frameTime)
{
	if(g_pGame->GetIGameFramework()->IsEditing())
	{
		if(!ScanedEntities.empty())
			ClearAllSign();
	}

	if(ProbeManager::Instance().IsProbeActived())
	{

		BasicRayCast *pBRC = g_pGame->GetBasicRayCast();
	
		IEntity *pEntity = pBRC->GetEntity();
		if(pEntity)
		{
			if(tick_timer < scan_delay)
				tick_timer += frameTime;
			else
			{
				tick_timer = 0;
				if(!ScanedEntities.count(pEntity->GetId()))
				{
					if(int temp_type = GetEntityUIType(pEntity))
					{
						ScanedEntityParams params;
						params.pEntity = pEntity;
						params.IndicatorId = UI_IDPool.AssignId();			//Ensure there are plenty of id space
						params.MarkForRemove = false;
						ScanedEntities[pEntity->GetId()] = params;
					}
				}
			}
		}
		/*IActor *pPlayerActor = g_pGame->GetIGameFramework()->GetClientActor();
		const IEntity *pProbeEntity = ProbeManager::Instance().GetProbe();
		if(pPlayerActor)
		{
			if(pProbeEntity)
			{
				const Vec3 &ProbePos = pProbeEntity->GetPos();
				IPhysicalEntity **nearbyEntities;
				int num=gEnv->pPhysicalWorld->GetEntitiesInBox(ProbePos-Vec3(20), ProbePos+Vec3(20), nearbyEntities, ent_living);
				for(int i = 0; i < num; ++i)
				{
					IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
					if(pEntity && pEntity != pProbeEntity)
					{
						if(!enemies.count(pEntity->GetId()))
							enemies.insert(pEntity->GetId());
					}
				}
	
				//Double check
				std::set<EntityId>::iterator iter = enemies.begin();
				std::set<EntityId>::iterator end = enemies.end();
				while(iter != end)
				{
					IEntity *pEntity = gEnv->pEntitySystem->GetEntity(*iter);
					if(pEntity)
					{
						Vec3 EntityPos = pEntity->GetPos();
						float length = (ProbePos - EntityPos).GetLengthSquared();
						IActor *pActor = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(*iter);
						if(length > 1600 || (pActor && pActor->IsDead()))
						{
							Mapindicator::theIndicator().RemoveEnemyIndicator(*iter);
							pElement->CallFunction("RemoveIndicator", SUIArguments::Create(*iter));
							if(!pActor->IsDead())
								EnableEnemySign(false,*iter);
							else
								RemoveEnemySign(*iter);
							enemies.erase(iter++);
						}
						else
						{
							Mapindicator::theIndicator().AddEnemyIndicator(*iter);
							SUIArguments args;
							args.AddArgument(*iter);
							if(pActor->GetEntity()->GetAI()->GetFactionID() == pPlayerActor->GetEntity()->GetAI()->GetFactionID())
								args.AddArgument((int)eIT_Friendly);
							else
								args.AddArgument((int)eIT_Enemy);
							pElement->CallFunction("AddIndicator", args);
							if(!pActor->IsDead() && !pActor->IsFallen())
							{
								AABB aabb;
								pEntity->GetLocalBounds(aabb);
								EntityPos.z += 2.2;
							}
							Vec3 screenPos;
							CCamera camera = gEnv->pSystem->GetViewCamera();
	
							bool onScreen = camera.Project(EntityPos, screenPos, Vec2i(0,0), Vec2i(1920, 1080));
	
							if(BattleChallenge::Instance().IsChallangeTarget(pEntity->GetId()))
								SetUIEnemySign(screenPos.x, screenPos.y, onScreen, *iter, (int)eIT_Target);
							else
							{
								if(pActor->GetEntity()->GetAI()->GetFactionID() == pPlayerActor->GetEntity()->GetAI()->GetFactionID())
									SetUIEnemySign(screenPos.x, screenPos.y, onScreen, *iter, (int)eIT_Friendly);
								else
									SetUIEnemySign(screenPos.x, screenPos.y, onScreen, *iter, (int)eIT_Enemy);
							}
							++iter;
						}
					}
					else
					{
						Mapindicator::theIndicator().RemoveEnemyIndicator(*iter);
						pElement->CallFunction("RemoveIndicator", SUIArguments::Create(*iter));
						RemoveEnemySign(*iter);
						enemies.erase(iter++);
					}
				}
			}
		}*/
	}
	UpdateScanedEntityInfo();
}

void EnemyScan::SetUIEnemySign(float pos_x, float pos_y, bool _visible, EntityId Id, int enemy_type)
{
	if(!pElement)
		pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	else
	{
		SUIArguments args;
		args.AddArgument(pos_x);
		args.AddArgument(pos_y);
		args.AddArgument(_visible);
		args.AddArgument(Id);
		args.AddArgument(enemy_type);
		pElement->CallFunction("UpdateEnemySign",args);
	}
}

void EnemyScan::EnableEnemySign(bool _enable, EntityId Id)
{
	if(!pElement)
		pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	else
	{
		SUIArguments args;
		args.AddArgument(_enable);
		args.AddArgument(Id);
		pElement->CallFunction("EnableEnemySign",args);
	}
}

void EnemyScan::RemoveEnemySign(EntityId Id)
{
	if(!pElement)
		pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	else
	{
		SUIArguments args;
		args.AddArgument(Id);
		pElement->CallFunction("RemoveEnemySign",args);
	}
}

void EnemyScan::UpdateScanedEntityInfo()
{
	std::map<unsigned int,ScanedEntityParams>::iterator iter = ScanedEntities.begin();
	std::map<unsigned int,ScanedEntityParams>::iterator end = ScanedEntities.end();
	while(iter != end)
	{
		ScanedEntityParams &params = iter->second;
		IEntity *pEntity = params.pEntity;
		if(params.MarkForRemove)
		{
			RemoveEnemySign(iter->second.IndicatorId);
			ScanedEntities.erase(iter++);
			continue;
		}
		Vec3 EntityPos = pEntity->GetPos();

		Vec3 screenPos;
		CCamera camera = gEnv->pSystem->GetViewCamera();

		AABB aabb;
		pEntity->GetLocalBounds(aabb);
		EntityPos.z += static_cast<float>(aabb.GetSize().z * 1.5);

		bool onScreen = camera.Project(EntityPos, screenPos, Vec2i(0,0), Vec2i(1920, 1080));

		SetUIEnemySign(screenPos.x, screenPos.y, onScreen, params.IndicatorId, GetEntityUIType(pEntity));
		UpdateMapIndicator(pEntity);
		++iter;
	}
}

const unsigned int EnemyScan::GetEntityUIType(IEntity *pEntity)
{
	if(IActor *pActor = g_pGame->GetGameRules()->GetActorByEntityId(pEntity->GetId()))
	{
		if(!pActor->IsDead())
		{
			if(pActor->GetEntity()->GetAI()->GetFactionID() == g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetAI()->GetFactionID())
				return eIT_Friendly;
			else
				return eIT_Enemy;
		}
		else
			return eIT_Dead;
	}
	else
	{
		IEntityClass *pClass = pEntity->GetClass();
		if(pClass == EntityClassPreLoad::Instance().pDestroyableObjectClass)
			return eIT_Explosion;
		else if(pClass == EntityClassPreLoad::Instance().pTrap)
			return eIT_Trap;
	}
	return 0;
}

void EnemyScan::UpdateMapIndicator(IEntity *pEntity)
{
	if(IActor *pActor = g_pGame->GetGameRules()->GetActorByEntityId(pEntity->GetId()))
	{
		if(!pActor->IsDead())
		{
			Mapindicator::theIndicator().AddEnemyIndicator(pEntity->GetId());
			SUIArguments args;
			args.AddArgument(pEntity->GetId());
			if(pActor->GetEntity()->GetAI()->GetFactionID() == g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetAI()->GetFactionID())
				args.AddArgument((int)eIT_Friendly);
			else
				args.AddArgument((int)eIT_Enemy);
			pElement->CallFunction("AddIndicator", args);
		}
		else
		{
			Mapindicator::theIndicator().RemoveEnemyIndicator(pEntity->GetId());
			pElement->CallFunction("RemoveIndicator", SUIArguments::Create(pEntity->GetId()));
		}
	}
}

void EnemyScan::ClearAllSign()
{
	if(!ScanedEntities.empty())
	{
		std::map<unsigned int,ScanedEntityParams>::iterator iter = ScanedEntities.begin();
		std::map<unsigned int,ScanedEntityParams>::iterator end = ScanedEntities.end();
		while(iter != end)
		{
			RemoveEnemySign(iter->second.IndicatorId);
			ScanedEntities.erase(iter++);
		}
	}
}

void EnemyScan::MarkEntityForRemove(EntityId Id)
{
	if(ScanedEntities.count(Id))
	{
		ScanedEntities[Id].MarkForRemove = true;
	}
}