#include "StdAfx.h"
#include "StateManager.h"

StateManager::StateManager()
{
	pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	ProgressIndicatorPool.SetMaxIdAmount(30);
}

void StateManager::Update(float frameTime)
{
	if(!ProgressStates.empty())
	{
		std::map<EntityId, progressState>::iterator iter = ProgressStates.begin();
		std::map<EntityId, progressState>::iterator end = ProgressStates.end();
		while(iter != end)
		{
			progressState &state = iter->second;
			if(state.tick_timer < state.last_time)
			{
				EntityId Id = iter->second.Id;
				IEntity* pEntity = gEnv->pEntitySystem->GetEntity(Id);
				if(pEntity)
				{
					state.tick_timer += frameTime;
					int progress = static_cast<int>((state.tick_timer / state.last_time) * 99 + 1);
					AABB aabb;
					pEntity->GetWorldBounds(aabb);
					Vec3 EntityPos = aabb.GetCenter();

					Vec3 screenPos;
					CCamera camera = gEnv->pSystem->GetViewCamera();
					bool onScreen = camera.Project(EntityPos, screenPos, Vec2i(0,0), Vec2i(1920, 1080));

					UpdateIndicator(screenPos.x, screenPos.y, onScreen, iter->first, progress);
				}
			}
			else
			{
				RemoveIndicator(iter->first);
				ProgressIndicatorPool.RecoveryId(iter->first);
				ProgressStates.erase(iter++);
				continue;
			}
			++iter;
		}
	}
}

void StateManager::AddProgressState(EntityId Id, float _lasttime)
{
	if(!pElement)
		pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	if(pElement)
	{
		progressState state;
		state.tick_timer = 0;
		state.last_time = _lasttime;
		state.Id = Id;
		unsigned int IndicatorId = ProgressIndicatorPool.AssignId();
		ProgressStates[IndicatorId] = state;
		pElement->CallFunction("AddProgressIndicator", SUIArguments::Create(IndicatorId));
	}
}

void StateManager::UpdateIndicator(float pos_x, float pos_y, bool _visible, EntityId Id, int _progress)
{
	if(!pElement)
		pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	if(pElement)
	{
		SUIArguments args;
		args.AddArgument(pos_x);
		args.AddArgument(pos_y);
		args.AddArgument(_visible);
		args.AddArgument(Id);
		args.AddArgument(_progress);
		pElement->CallFunction("UpdateProgressIndicator",args);
	}
}

void StateManager::RemoveIndicator(EntityId Id)
{
	if(!pElement)
		pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	if(pElement)
	{
		pElement->CallFunction("RemoveProgressIndicator",SUIArguments::Create(Id));
	}
}