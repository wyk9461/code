#ifndef _STRONGHOLDMANAGER_H
#define _STRONGHOLDMANAGER_H

#include "IEntity.h"
#include "Code_System/CodeExclusion.h"
#include "StrongholdTraits.h"

struct StrongholdStatus
{
	bool isClear;
	bool isWithin;

	int Level;
	int LevelCap;
	std::deque<boost::shared_ptr<IStrongholdTraits> > Traits;

	float experience;
	float experience_needed;
	float RefreashTime;
	float refreash_timer;

	bool isRecordStarted;
	bool isRecordBreak;

	struct BattleRecord
	{
		float InitExclusionLevel;
		float MaxExclusionLevel;

		int InitStabilityLevel;
		int MinStabilityLevel;

		float BattleDuration;

		float EnemyCapability;
		float ClearingEfficiency;

		BattleRecord()
			:	InitExclusionLevel(0.f)
			,	MaxExclusionLevel(0.f)
			,	InitStabilityLevel(0)
			,	MinStabilityLevel(InitStabilityLevel)
			,	BattleDuration(0.f)
			,	EnemyCapability(0.f)
			,	ClearingEfficiency(0.f){}

		void Init()
		{
			InitExclusionLevel = CodeExclusion::Instance().GetExclusionAmount();
			InitStabilityLevel = g_pGame->GetWorldStability()->Getstability();
			MaxExclusionLevel = 0;
			MinStabilityLevel = InitStabilityLevel;
			BattleDuration = 0.f;
			EnemyCapability = 0.f;
			ClearingEfficiency = 0.f;
		}
		void Reset()
		{
			InitExclusionLevel = 0.f;
			MaxExclusionLevel = 0.f;
			InitStabilityLevel = 0;
			MinStabilityLevel = InitStabilityLevel;
			BattleDuration = 0.f;
			EnemyCapability = 0.f;
			ClearingEfficiency = 0.f;
		}
	}Records;

	StrongholdStatus()
	:	isClear(true)
	,	isWithin(false)
	,	Level(1)
	,	LevelCap(10)
	,	experience(0)
	,	experience_needed(1000)
	,	RefreashTime(30)
	,	refreash_timer(0)
	,	isRecordStarted(false)
	,	isRecordBreak(false){}
};


class StrongholdManager
{
public:
	static StrongholdManager &Instance(){static StrongholdManager instance;return instance;}
	void RegisterStronghold(EntityId Id, int curlevel, int levelcap, float refreash_time);
	void ReceiveStrongholdClearMessage(EntityId Id);
	void ReceiveStrongholdEliminateMessage(EntityId Id, float capability);
	void SetStrongholdTargets(EntityId StrongholdId, IEntity *pTarget, int flag);
	void Update(float frameTime);
	void ActiveStrongholdTraits(StrongholdStatus &status, float frameTime);
private:
	void CalculateWithin(StrongholdStatus &status);
	void UpdateRefreashStatus(StrongholdStatus &status);
	void RebootStronghold();

	void StartRecord(StrongholdStatus &status);
	void BreakRecord(StrongholdStatus &status);
	void StopRecord(StrongholdStatus &status);

	void StrongholdLevelUp(StrongholdStatus &status, EntityId Id);
	void AssaignNewTraits(StrongholdStatus &status, EntityId Id);
	void ResetStrongholdTraitsStatus(StrongholdStatus &status);

	void UpdateRecord(StrongholdStatus &status, float frameTime);
	void CalculateExperience(EntityId Id);

	StrongholdManager();
	std::map<EntityId, StrongholdStatus> Strongholds;
	std::map<EntityId, StrongholdStatus>::iterator Iter;
	boost::shared_ptr<IStrongholdTraits> CreateTraits_Lv1(int type, EntityId Id);
	float update_timer;
};


#endif