#ifndef _STATEMANAGER_H_
#define _STATEMANAGER_H_

#include "IFlashUI.h"

struct progressState
{
	float tick_timer;
	float last_time;
	EntityId Id;
	
	progressState &operator=(const progressState &rhs)
	{
		if(this == &rhs)
			return *this;
		tick_timer = rhs.tick_timer;
		last_time = rhs.last_time;
		Id = rhs.Id;
		return *this;
	}
};

class StateManager
{
public:
	~StateManager(){}
	static StateManager &theStateManager(){static StateManager manager;return manager;}

	void Update(float frameTime);
	void AddProgressState(EntityId Id, float _lasttime);
	std::map<unsigned int, progressState> &GetProgressStates(){return ProgressStates;}
private:
	std::map<unsigned int, progressState> ProgressStates;
	StateManager();
	IUIElement *pElement;
	IdPool ProgressIndicatorPool;
	void UpdateIndicator(float pos_x, float pos_y, bool _visible, EntityId Id, int _progress);
	void RemoveIndicator(EntityId Id);
};
#endif