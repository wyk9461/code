#ifndef _STRONGHOLDTRAITS_H_
#define _STRONGHOLDTRAITS_H_

#include "IEntity.h"
#include <list>
#include <map>
#include <deque>

enum EStrongholdTraits_Lv1
{
	eST_Regeneration			= 0,
	eST_Tough					= 1, 
	eST_Lv1_Count
};

enum EAffectdType
{
	eAT_SpawnedAI,
	eAT_Probe,
	eAT_Player,
	eAT_Debuff,
};


class IStrongholdTraits
{
public:
	virtual void Active(float frameTime = 0){};
	virtual void SetTarget(IEntity *pEntity){};
	virtual void SetActivePos(const Vec3 &pos){};
	virtual void SetEffectLevel(int level){};
	virtual void Reset(){};
	ILINE void SetStronghold(EntityId Id){StrongholdId = Id;}
	ILINE int GetAffectType(){return AffectType;}
protected:
	EntityId StrongholdId;
	static const int AffectType = eAT_SpawnedAI;
};


class StrongholdRegenerationTraits : public IStrongholdTraits
{
public:
	StrongholdRegenerationTraits();
	~StrongholdRegenerationTraits();
	void Active(float frameTime = 0);
	void SetTarget(IEntity *pEntity);
	void SetActivePos(const Vec3 &pos){}
	void SetEffectLevel(int level);
	virtual void Reset(){Targets.clear();}
private:
	std::map<EntityId, CActor*> Targets;
	static const int AffectType = eAT_SpawnedAI;
	float tick_timer;
	float delay;
	float percentage;
};

class StrongholdToughTraits : public IStrongholdTraits
{
public:
	StrongholdToughTraits();
	~StrongholdToughTraits();
	void Active(float frameTime = 0);
	void SetTarget(IEntity *pEntity);
	void SetActivePos(const Vec3 &pos){}
	void SetEffectLevel(int level);
	virtual void Reset(){Targets.clear();}
private:
	struct AffectdActor
	{
		bool hasSet;
		float DefaultHealth;
		AffectdActor()
		:	hasSet(false)
		,	DefaultHealth(500){}
	};

	std::map<EntityId, AffectdActor> Targets;
	static const int AffectType = eAT_SpawnedAI;
	float percentage;
	int HasProcessedNum;
};

#endif