#include "StdAfx.h"
#include "StrongholdTraits.h"


//------------------------------------------------------------------------
//Regeneration Traits
//------------------------------------------------------------------------

StrongholdRegenerationTraits::StrongholdRegenerationTraits()
:	tick_timer(0.f)
,	delay(1.f)
,	percentage(0.05)
{
	
}

StrongholdRegenerationTraits::~StrongholdRegenerationTraits()
{
	Targets.clear();
}

void StrongholdRegenerationTraits::Active(float frameTime /* = 0 */)
{
	if(!Targets.empty())
	{
		if(tick_timer < delay)
			tick_timer += frameTime;
		else
		{
			std::map<EntityId, CActor*>::iterator iter = Targets.begin();
			std::map<EntityId, CActor*>::iterator end = Targets.end();
			while(iter != end)
			{
				CActor *pActor = iter->second;
				if(!pActor->IsDead())
				{
					float maxHealth = pActor->GetMaxHealth();
					float regeneration_amount = maxHealth * percentage;
					float curHealth = pActor->GetHealth();
					pActor->SetHealth(curHealth + regeneration_amount);
					if(pActor->GetHealth() > maxHealth)
						pActor->SetHealth(maxHealth);
					++iter;
				}
				else
					Targets.erase(iter++);
			}
			tick_timer = 0;
		}
	}
}

void StrongholdRegenerationTraits::SetTarget(IEntity *pEntity)
{
	if(!Targets.count(pEntity->GetId()))
	{
		CActor *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId()));
		if(pActor && !pActor->IsDead())
		{
			Targets.insert(std::map<EntityId, CActor*>::value_type(pEntity->GetId(), pActor));
		}
	}
}

void StrongholdRegenerationTraits::SetEffectLevel(int level)
{
	//May add some randomness
	percentage = float(level) / 40;
	delay = 2 / float(level);
}

//------------------------------------------------------------------------
//Tough Traits
//------------------------------------------------------------------------
StrongholdToughTraits::StrongholdToughTraits()
:	percentage(0.f)
,	HasProcessedNum(0)
{

}

StrongholdToughTraits::~StrongholdToughTraits()
{
	Targets.clear();
}

void StrongholdToughTraits::Active(float frameTime /* = 0 */)
{
	if(!Targets.empty() && HasProcessedNum != Targets.size())
	{
		std::map<EntityId,AffectdActor>::iterator iter = Targets.begin();
		std::map<EntityId,AffectdActor>::iterator end = Targets.end();
		while(iter != end)
		{
			CActor *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(iter->first));
			if(pActor && !pActor->IsDead())
			{
				if(!iter->second.hasSet)
				{
					pActor->SetMaxHealth(static_cast<float>(iter->second.DefaultHealth * (1 + percentage)));
					pActor->SetHealth(static_cast<float>(iter->second.DefaultHealth * (1 + percentage)));
					iter->second.hasSet = true;
					++HasProcessedNum;
				}
			}
			else
			{
				++HasProcessedNum;
			}
			++iter;
		}
	}
}

void StrongholdToughTraits::SetTarget(IEntity *pEntity)
{
	if(!Targets.count(pEntity->GetId()))
	{
		CActor *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId()));
		if(pActor && !pActor->IsDead())
		{
			AffectdActor actor;
			actor.DefaultHealth = pActor->GetMaxHealth();
			Targets.insert(std::map<EntityId,AffectdActor>::value_type(pEntity->GetId(), actor));
		}
	}
}

void StrongholdToughTraits::SetEffectLevel(int level)
{
	percentage = float(level) / 10;
}