#ifndef _FALLENACTORMANAGER_H_
#define _FALLENACTORMANAGER_H_

#include <map>
#include "Actor.h"

struct FallenActorParams
{
	CActor *pActor;
	Vec3 LastPos;
	FallenActorParams()
	:	pActor(NULL)
	,	LastPos(Vec3(ZERO)){}
};

class FallenActorManager
{
public:
	static FallenActorManager &Instance(){static FallenActorManager instance;return instance;}
	~FallenActorManager(){}
	void Update(float frameTime);
	void AddToManager(CActor *pActor);
	void FallenRecovery(CActor *pActor);
private:
	FallenActorManager();
	typedef std::map<EntityId,FallenActorParams> FallenActorMap;
	FallenActorMap FallenActors;
	float update_time;
	float tick_timer;
};
#endif