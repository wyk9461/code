#ifndef _ENEMYSCAN_H_
#define _ENEMYSCAN_H_
#include "IFlashUI.h"

enum Indicator_Type
{
	eIT_Enemy = 1,
	eIT_Friendly,
	eIT_Target,
	eIT_Explosion,
	eIT_Trap,
	eIT_Dead,
};

struct ScanedEntityParams
{
	unsigned int IndicatorId;
	IEntity *pEntity;
	bool MarkForRemove;
};

class EnemyScan
{
public:
	~EnemyScan(){}

	static EnemyScan &theScan(){static EnemyScan scan;return scan;}
	void SetUIEnemySign(float pos_x, float pos_y, bool _visible, EntityId Id, int enemy_type);
	void EnableEnemySign(bool _enable, EntityId Id);
	void RemoveEnemySign(EntityId Id);
	void Update(float frameTime);
	const std::map<unsigned int,ScanedEntityParams> &GetScanedEntities() const {return ScanedEntities;}
	void ClearAllSign();
	void MarkEntityForRemove(EntityId Id);
private:
	EnemyScan();

	void UpdateScanedEntityInfo();
	const unsigned int GetEntityUIType(IEntity *pEntity);
	void UpdateMapIndicator(IEntity *pEntity);

	float tick_timer;
	float scan_delay;
	IUIElement* pElement;
	std::map<unsigned int ,ScanedEntityParams> ScanedEntities;
	IdPool UI_IDPool;
};
#endif