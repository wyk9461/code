#include "StdAfx.h"
#include "MapIndicator.h"
#include "Player.h"
#include "IFlashUI.h"

void Mapindicator::AddEnemyIndicator(EntityId enemyId)
{
	if(!enemy_indactors.count(enemyId))
	{
		enemy_indactors.insert(enemyId);
	}
}

void Mapindicator::RemoveEnemyIndicator(EntityId enemyId)
{
	if(enemy_indactors.count(enemyId))
	{
		enemy_indactors.erase(enemyId);
	}
}

void Mapindicator::Update(float frameTime)
{
	IActor *pPlayerActor = g_pGame->GetIGameFramework()->GetClientActor();
	if(pPlayerActor)
	{
		IEntity *pPlayerEntity = pPlayerActor->GetEntity();
		if(pPlayerEntity)
		{
			CPlayer *pPlayer = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
			Matrix34 mat = pPlayer->GetViewMatrix();
			const Vec3 &PlayerPos = pPlayerEntity->GetPos();
			Vec3 PlayerFwdDir(mat.m01,mat.m00,0);
			PlayerFwdDir.normalize();
			Vec3 LeftDir = -PlayerFwdDir.Cross(Vec3(0,0,1));

			//update Radar
			Vec3 NorthDir = Vec3(1,0,0);
			NorthDir.normalize();
			f32 dot_north = NorthDir.Dot(LeftDir);
			f32 fdot_north = NorthDir.Dot(PlayerFwdDir);
			float angle_north = RAD2DEG(cry_acosf(fdot_north));
			bool isLeft_north(dot_north >= 0);
			SUIArguments args;
			args.AddArgument(angle_north);
			args.AddArgument(isLeft_north);
			pElement->CallFunction("UpdateRader", args);

			std::set<EntityId>::iterator iter = enemy_indactors.begin();
			std::set<EntityId>::iterator end = enemy_indactors.end();
			while (iter != end)
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(*iter);
				if(pEntity)
				{
					const Vec3 &EntityPos = pEntity->GetPos();
					Vec3 enemydir = EntityPos - PlayerPos;
					enemydir.z = 0;
					enemydir.normalize();
					f32 dot = enemydir.Dot(LeftDir);
					f32 fdot = enemydir.Dot(PlayerFwdDir);
					float angle = RAD2DEG(cry_acosf(fdot));
					bool isLeft(dot >= 0);

					if(pElement)
					{
						//Code Note : Call UI Function
						SUIArguments args;
						args.AddArgument(angle);
						args.AddArgument(angle_north);
						args.AddArgument(isLeft_north);
						args.AddArgument(isLeft);
						args.AddArgument(*iter);
						if(pEntity->GetAI()->GetFactionID() == pPlayerEntity->GetAI()->GetFactionID())
							args.AddArgument(2);
						else
							args.AddArgument(1);
						pElement->CallFunction("UpdateIndicator", args);
					}
					++iter;
				}
				else
					enemy_indactors.erase(iter++);
			}
		}
	}
}