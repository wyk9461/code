#ifndef _TIMERMANAGER_H_
#define _TIMERMANAGER_H_

#include "StdAfx.h"


class MTimer
{
private:
	typedef void (*pf)();
	string Name;
	float TickTimer;
	float LastTime;
	float StartProgress;
	float MaxProgress;
	float rate;
	bool pause;
	bool RemoveAfterEnd;
	bool ShouldLoop;
	bool finish;
	pf ContinueFun;
	pf EndFun;

public:
	MTimer(const string name = "",const float LTime = 0 ,const float MProgress = 0 ,const float SProgress = 0)
	:	Name(name)
	,	TickTimer(0)
	,	LastTime(LTime)
	,	StartProgress(0)
	,	MaxProgress(SProgress)
	,	rate((MaxProgress-StartProgress)/LTime)
	,	pause(false)
	,	RemoveAfterEnd(true)
	,	ShouldLoop(false)
	,	finish(false)
	,	ContinueFun(0)
	,	EndFun(0){}

	MTimer(MTimer &mt);

	inline const float GetCurTimer(){return TickTimer;}
	inline const float GetLastTime(){return LastTime;}
	inline const float GetRate(){return rate;}
	inline const float GetProgress(){return StartProgress;}
	inline const bool ShouldRemove(){return finish && RemoveAfterEnd;}

	// Description : 
	//		Decide if this timer will be removed after it has finished tick.
	//		Will set ShouldLoop to false.
	// Arguments :
	//		enable - true or false.
	void SetRAE(const bool enable);

	// Description : 
	//		Decide if this timer will reset itself after it has finished tick.
	//		Will set RemoveAfterEnd to false.
	// Arguments :
	//		enable - true or false.
	void SetLoop(const bool enable);

	inline void PauseTimer(){pause=true;}

	inline void StartTimer(){pause=false;}

	// Description : 
	//		Set the function of this Timer.
	//		Only void function can apply.
	//		eg : void test();
	// Arguments :
	//		CFun - the function called at ticking time.
	//		EFun - the function called at the end of the period.
	inline void SetFunction(pf CFun , pf EFun){ContinueFun = CFun;EndFun=EFun;}

	inline const bool isPaused(){return pause;}

	void Update(const float frameTime);

	inline const string& GetName() const {return Name;} 

	const bool operator<(const MTimer & mt){return LastTime<mt.LastTime;}

};

typedef unsigned int TimerId;
typedef std::map<TimerId , MTimer> TimerMap;
typedef std::set<TimerId> FreeTimerId;

class TimerManager
{
private:
	TimerMap Timers;
	FreeTimerId IdList;
	unsigned int curIndex;
	TimerMap::iterator Timers_iter;

	TimerId FindId();
	void AddingTimer(MTimer Timer , const TimerId Id);

public:
	TimerManager()
	:	curIndex(1)
	,	Timers_iter(Timers.begin()){}

	// Description : 
	//		Add a new Timer to the Timer Map.
	// Arguments :
	//		MTimer - Timer Object.
	//		TimerId - Manually Choose Id.If set to 0 then program will find an Id for it.
	//		forceFindId - if is set to true,then when IdListis empty,program will find an Id from 1 to max of int.
	void AddTimer(MTimer Timer , TimerId Id=0 , const bool forceFindId = true);

	// Description : 
	//		Remove an Timer with exact Id.
	// Arguments :
	//		TimerId - The Id of a timer that will be removed.
	void RemoveTimer(TimerId Id);

	void Update(const float frameTime);

	inline MTimer &GetTimer(const TimerId Id){return Timers[Id];}

	const TimerId GetTimerIdFromName(const string name);
};
#endif