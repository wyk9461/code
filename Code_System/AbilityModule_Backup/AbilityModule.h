#ifndef _ABILITYMODULE_H_
#define _ABILITYMODULE_H_

#include "ScreenEffects.h"
#include "Code_System/BasicRayCast.h"


#include "Code_System/AbilityModule/TeleportModule.h"
#include "Code_System/AbilityModule/ShockWaveModule.h"
#include "Code_System/AbilityModule/BulletTimeModule.h"
#include "Code_System/AbilityModule/KineticShieldModule.h"
#include "Code_System/AbilityModule/ReflectionShieldModule.h"
#include "Code_System/AbilityModule/ExplodeShieldModule.h"
#include "Code_System/AbilityModule/DataBallModule.h"
#include "Code_System/AbilityModule/HealModule.h"
#include "Code_System/AbilityModule/BackupModule.h"
#include "Code_System/AbilityModule/RushModule.h"
#include "Code_System/AbilityModule/RushModule.h"
#include "Code_System/AbilityModule/NuclearRadModule.h"
#include "Code_System/AbilityModule/FloodModule.h"
#include "Code_System/AbilityModule/DataBiteModule.h"
#include "Code_System/AbilityModule/GrabModule.h"

enum ModuleType
{
	eMT_Teleport,
	eMT_ShockWave,
	eMT_BulletTime,
	eMT_Shield,
	eMT_Reflection,
	eMT_ExploShield,
	eMT_DataBall,
	eMT_Heal,
	eMT_Backup,
	eMT_Rush,
	eMT_Cloak,
	eMT_DeepCloak,
	eMT_NuclearRad,
	eMT_Flood,
	eMT_DataBite,
	eMT_Grab,
	eMT_Last
};


class AbilityModule
{
	private:
		int max_index;
		bool StartFunction;
		bool ShowChangeText;
		bool ModuleSelected[3];
		float text_timer;
		std::vector<int>::iterator it;
		ray_hit hit;
		int hits;
		int query_flag;
		IPhysicalWorld *pWorld;
		EntityId store_weapon_id;
		BasicRayCast *pBRC;

		//Module
		TeleportModule *m_Teleport;
		ShockWaveModule *m_ShockWave;
		BulletTimeModule *m_BulletTime;
		KineticShieldModule *m_Shield;
		ReflectionShieldModule *m_Reflection;
		ExplodeShieldModule *m_ExploShield;
		DataBallModule *m_DataBall;
		HealModule *m_Heal;
		BackupModule *m_Backup;
		RushModule *m_Rush;
		NuclearRadModule *m_NuclearRad;
		FloodModule *m_Flood;
		DataBiteModule *m_DataBite;
		GrabModule *m_Grab;
		//~Module

		IRenderer *pRenderer;
		float color[ 4 ];

		void InitModuleTimer();
		void InitModuleName();
		void DisplayModule();
		void AfterFinished(float frameTime);
		const bool HasShield();

		void PerProcessAllModule();
		void ProcessAllModule(const float frameTime);
		void PostProcessAllModule(const float frameTime);

	public:
		std::vector<int> InstlledModule;
		std::map<int,Timer_Cd*> ModuleTimer;
		std::map<int,string*> ModuleName;
		AbilityModule();
		~AbilityModule();
		void Update(float frameTime);

		//Shield Inline
		inline const bool ShieldHasSet(){return m_Shield->isSet();}
		inline void CallKineticShieldDamage(HitInfo &info){m_Shield->ShieldDamage(info);}
		//Reflection Inline
		inline const bool ReflectionHasSet(){return m_Reflection->isSet();}
		//ExploShield Inline
		inline const bool ExploShieldHasSet(){return m_ExploShield->isSet();}
		inline void CallExploShieldDamage(HitInfo &info){m_ExploShield->ExploShieldDamage(info);}
		//BackUp Inline
		inline const bool BackUpHasSet(){return m_Backup->isSet();}
		//DataBite Inline
		inline const bool IsBitePreProcessed(){return m_DataBite->isSet();}
		inline const bool IsBiteLaunched(){return m_DataBite->isSet();}
		inline void setBite(){m_DataBite->setHasHit();}
		inline void SetBiteTarget(CActor *target){m_DataBite->setHitTarget(target);}

		//Main Function Inline
		inline void SetStartFunction(bool enable){StartFunction = enable;}
		inline void SetTextChanged(bool enable){ShowChangeText=enable;}
		inline void SetModule(int i,bool enable){ModuleSelected[i]=enable;}
		inline void Setindex(const int i){it=InstlledModule.begin()+i;}
		inline void AddIndex(){++it;}
		inline void MinuesIndex(){--it;}
		inline void ZeroIndex(){it=InstlledModule.begin();}
		inline void ZeroTextTimer(){text_timer=0;}
		inline const bool IsModuleSelected(int i){return ModuleSelected[i];}
		inline std::vector<int>::iterator GetIndex(){return it;}
		//Get Module Interface
		inline const bool IsModule_1(){return ModuleSelected[0];}
		inline const bool IsModule_2(){return ModuleSelected[1];}
		inline const bool IsModule_3(){return ModuleSelected[2];}
};

#endif
