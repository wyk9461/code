#ifndef _RUSHMODULE_H_
#define _RUSHMODULE_H_

#include "TargetBasedAbility_Charge.h"
#include "IParticles.h"
#include "Code_System/BasicRayCast.h"


class RushModule : public TargetBasedAbility_Charge
{
private:
	IParticleEffect *pEffect;
	float curhealth;
	float maxhealth;
	Vec3 dir;
	Vec3 pos;
	float speed;
	float damage;
	bool hasSet;
	bool once;

	IView *default_View;
	IView *rush_View;

	BasicRayCast *pBRC;

public:
	RushModule()
	:	TargetBasedAbility_Charge("Rush",100)
	,	pEffect(NULL)
	,	curhealth(1000)
	,	maxhealth(1000)
	,	dir(0,0,0)
	,	pos(0,0,0)
	,	speed(50)
	,	damage(300)
	,	hasSet(false)
	,	once(false){}

	void PreProcess();
	void Process(ray_hit &hit,const float frameTime);
	void PostProcess(const float frameTime = 0);

	void Cool_Down(const float frameTime);

	inline const bool isSet(){return hasSet;}
};
#endif