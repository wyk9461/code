#include "StdAfx.h"
#include "TeleportModule.h"

void TeleportModule::PreProcess()
{
	if(reInit)
	{
		Timer.Cd=10;
		reInit=false;
	}
	HasPreProcessed=true;

	g_pGame->GetBasicRayCast()->ChangeFlag(ent_all);
	IEntity *pEntity=gEnv->pEntitySystem->FindEntityByName("Teleport_Particle");
	if(!pEntity)
	{
		SEntitySpawnParams params;
		params.sName="Teleport_Particle";
		params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
		params.vScale=Vec3(1,1,1);
		params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
		params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
		pEffect=gEnv->pEntitySystem->SpawnEntity(params);
		SmartScriptTable proot=pEffect->GetScriptTable();
		SmartScriptTable pprops;
		proot->GetValue("Properties",pprops);
		pprops->SetValue("ParticleEffect","Code_System.Module_Effect.Teleport");
		Script::CallMethod(proot,"OnReset");
	}
	else
		pEntity->Hide(false);
}

void TeleportModule::Process(ray_hit &hit)
{
	//Code Notes : Character Animation
	/*ICharacterInstance *character = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetCharacter(0);
	ISkeletonAnim* pSkeleton;
	if(character && (pSkeleton=character->GetISkeletonAnim()))
	{
		string animName="swim_tac_melee_nw_1p_01";
		CryCharAnimationParams Params;
		Params.m_fAllowMultilayerAnim = 1;
		Params.m_nLayerID = 10;

		//Params.m_nFlags |= CA_LOOP_ANIMATION;
		//Params.m_nFlags |= CA_REMOVE_FROM_FIFO;
		Params.m_nUserToken = 0;

		Params.m_fTransTime = 0.1;
		Params.m_fPlaybackSpeed = 1;
		//CryLogAlways("ANIMATION");
		pSkeleton->StartAnimation(animName.c_str(), Params);
	}*/
	pEffect->SetPos(hit.pt);
	pos=hit.pt;
}

void TeleportModule::PostProcess(const float frameTime /* = 0 */ )
{
	HasPreProcessed=false;
	IActor *player=g_pGame->GetIGameFramework()->GetClientActor();
	player->GetEntity()->SetPos(pos);
	IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.Teleport_Finish");
	pEffect->Spawn(true,IParticleEffect::ParticleLoc(player->GetEntity()->GetPos(),Vec3(0,0,1)));
	g_pGame->GetWorldStability()->StabilityCost(cost);
	finished=true;
	blurAmount=1;
	gEnv->p3DEngine->SetPostEffectParam( "ImageGhosting_Amount", blurAmount );
	g_pGame->GetBasicRayCast()->Load();
	IEntity *pEntity=gEnv->pEntitySystem->FindEntityByName("Teleport_Particle");
	if(pEntity)
		pEntity->Hide(true);
}

void TeleportModule::Cool_Down(const float frameTime)
{
	if(Timer.timer>=0.5f && Timer.timer<=1.5f)
		gEnv->p3DEngine->SetPostEffectParam( "ImageGhosting_Amount",blurAmount-=1*frameTime );
	if(Timer.timer>=1.5f)
		gEnv->p3DEngine->SetPostEffectParam( "ImageGhosting_Amount",0.0f);
	Timer.Cdtimer+=frameTime;
	Timer.timer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.timer=0;
		Timer.Cdtimer=0;
	}
}