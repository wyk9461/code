#ifndef _SHOCKWAVEMODULE_H_
#define _SHOCKWAVEMODULE_H_

#include "PlayerBasedAbility.h"

class ShockWaveModule : public PlayerBasedAbility
{
private:
	float radius;
	float pressure;
	IEntity *pEffect;
	IPhysicalEntity **nearbyEntities;

public:
	ShockWaveModule()
	:	PlayerBasedAbility("ShockWave" , 100)
	,	radius(8.0)
	,	pressure(1000.0)
	,	pEffect(NULL)
	,	nearbyEntities(NULL){}

	~ShockWaveModule(){};

	void PreProcess();
	void Process();
	void PostProcess(const float frameTime = 0);
	void Cool_Down(const float frameTime);
};
#endif