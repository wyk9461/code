#ifndef _FLOODMODULE_H_
#define _FLOODMODULE_H_

class FloodModule : public TargetBasedAbility
{
private:
	IEntity *pEffect;
	bool hasSet;
public:
	FloodModule()
	:	TargetBasedAbility("Flood",100)
	,	pEffect(NULL)
	,	hasSet(false){}

	~FloodModule(){}

	void PreProcess();
	void Process(ray_hit &hit);
	void PostProcess(const float frameTime = 0);
	void Cool_Down(const float frameTime);

	inline const bool isSet(){return hasSet;}
};
#endif