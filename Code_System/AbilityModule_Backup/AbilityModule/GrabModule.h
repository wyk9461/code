#ifndef _GRABMODULE_H_
#define _GRABMODULE_H_

#include "BaseAbility.h"


class GrabModule : public BaseAbility
{
private:
	IEntity *pEntity;
	IEntity* pEffect;
	float rate;
	float damage;
	Vec3 finalpos;
	bool isGrab;
	bool hasSet;
	bool Detected;

public:
	GrabModule()
		:	BaseAbility("Grab",100)
		,	pEntity(NULL)
		,	pEffect(NULL)
		,	rate(0)
		,	damage(0)
		,	finalpos(0,0,0)
		,	isGrab(false)
		,	hasSet(false)
		,	Detected(false){}

	~GrabModule(){}

	void PreProcess(ray_hit &hit);
	void Process(ray_hit &hit);
	void PostProcess(ray_hit &hit,float frameTime);
	void Throw(ray_hit &hit);
	void ThrowEntityExplode(float frameTime);

	void Cool_Down(const float frameTime);

	inline const bool IsGrab(){return isGrab;}
	inline const bool IsDetected(){return Detected;}
};
#endif