#ifndef _HEALMODULE_H_
#define _HEALMODULE_H_

#include "PlayerBasedAbility.h"
#include "IParticles.h"

class HealModule : public PlayerBasedAbility
{
private:
	IParticleEffect *pEffect;
	int cost;
	float heal_amount;

public:
	HealModule()
		:	PlayerBasedAbility("Heal",100)
		,	pEffect(NULL)
		,	cost(100)
		,	heal_amount(300){}

	~HealModule(){}

	void PreProcess();
	void Process();
	void PostProcess(const float frameTime = 0);
	void Cool_Down(const float frameTime);

};
#endif