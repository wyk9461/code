#ifndef _TELEPORT_H_
#define _TELEPORT_H_

#include "TargetBasedAbility.h"
#include "IEntity.h"

class TeleportModule : public TargetBasedAbility
{
private:
	Vec3				 pos;
	IEntity			*pEffect;
	float				blurAmount;

public:
	TeleportModule()
	:	pos(0,0,0)
	,	pEffect(NULL)
	,	TargetBasedAbility("Teleport" , 100){}

	~TeleportModule(){}

	void PreProcess();
	void Process(ray_hit &hit);
	void PostProcess(const float frameTime = 0);
	void Cool_Down(const float frameTime);

};
#endif