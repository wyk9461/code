#ifndef _BASEABILITY_H_
#define _BASEABILITY_H_

#include "Game.h"
#include "ItemSystem.h"


struct Timer_Cd
{
	float Last_Time;
	float timer;
	float Cdtimer;
	float Cd;
	const bool operator<(const Timer_Cd &tc) {return timer<tc.timer;}
	Timer_Cd &operator=(const Timer_Cd &tc);
	Timer_Cd()
		:	Last_Time(0)
		,	timer(0)
		,	Cdtimer(0)
		,	Cd(0){}
};

Timer_Cd& Timer_Cd::operator=(const Timer_Cd &tc)
{
	if(this==&tc)
		return *this;
	Last_Time=tc.Last_Time;
	timer=tc.timer;
	Cdtimer=tc.Cdtimer;
	Cd=tc.Cd;
	return *this;
}


class BaseAbility
{
public:
	BaseAbility(const string str , const int co)
		:	Name(str)
		,	cost(co)
		,	finished(false)
		,	HasPreProcessed(false)
		,	reInit(true)
		,	Timer()
		,	Priority(1){}

	virtual ~BaseAbility(){m_Component.clear();}


	virtual void Cool_Down(const float frameTime){};

	inline const bool isFinished(){return finished;}
	inline const bool isPreProcessed(){return HasPreProcessed;}
	inline Timer_Cd &GetTimer(){return Timer;}

	inline string &GetName(){return Name;}

	void StoreInventory();
	void RestoreInventory();

	virtual bool AddComponent(BaseAbility *new_module);
	virtual bool RemoveComponent(BaseAbility *old_module);
	
protected:
	string Name;
	Timer_Cd Timer;
	int cost;						// The stability cost.
	bool finished;				//	if this module has finished its job.
	bool HasPreProcessed;			
	bool reInit;					// If this module need init its params.
	int Priority;
	std::list<BaseAbility*> m_Component;
};
#endif