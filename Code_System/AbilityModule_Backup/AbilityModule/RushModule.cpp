#include "StdAfx.h"
#include "RushModule.h"

void RushModule::PreProcess()
{
	default_View=g_pGame->GetIGameFramework()->GetIViewSystem()->GetActiveView();
	rush_View=g_pGame->GetIGameFramework()->GetIViewSystem()->CreateView();
	if(reInit)
	{
		Timer.Cd=10.f;
		reInit=false;
	}
	pBRC=g_pGame->GetBasicRayCast();
	pBRC->ChangeFlag(ent_all);
	pBRC->ChangeDistance(2.5);
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	HasPreProcessed=true;
	Timer.Last_Time=0.05f;
	Timer.Cdtimer=0;
	Timer.timer=0;
	pEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.Rush");
	pEffect->LoadResources();
	speed=50;
	maxhealth=pActor->GetMaxHealth();
	once=false;
}

void RushModule::Process(ray_hit &hit,const float frameTime)
{
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	if(Timer.Last_Time<=0.75f)
		Timer.Last_Time+=frameTime/3;
	gEnv->pRenderer->DrawLabel(hit.pt,1.5f,"Rush Time : %.2f",Timer.Last_Time);
	dir=gEnv->pRenderer->GetCamera().GetViewdir();
	pos=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
	curhealth=pActor->GetHealth();
}

void RushModule::PostProcess(const float frameTime /* = 0 */)
{
	if(!hasSet)
	{
		hasSet=true;
		finished=true;
		gEnv->p3DEngine->SetPostEffectParam( "ImageGhosting_Amount", 0.5 );
		//Code Node : a new particle spawn method.
		IEntity *pEntity=gEnv->pEntitySystem->FindEntityByName("Rush_Particle");
		if(!pEntity)
		{
			SEntitySpawnParams params;
			params.sName="Rush_Particle";
			params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
			params.vScale=Vec3(1,1,1);
			params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
			params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
			pEntity=gEnv->pEntitySystem->SpawnEntity(params);
			SmartScriptTable proot=pEntity->GetScriptTable();
			SmartScriptTable pprops;
			proot->GetValue("Properties",pprops);
			pprops->SetValue("ParticleEffect","Code_System.Module_Effect.Rush");
			Script::CallMethod(proot,"OnReset");
		}
		else
			pEntity->Hide(false);
		StoreInventory(); 
		/*IViewSystem *pSystem=g_pGame->GetIGameFramework()->GetIViewSystem();
		SViewParams *curParams=const_cast<SViewParams*>(default_View->GetCurrentParams());
		SViewParams newParams=*curParams;
		Vec3 eyePos=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
		eyePos.z+=1.8;
		Vec3 fordir=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetForwardDir();
		newParams.position=eyePos-2*fordir;
		rush_View->SetCurrentParams(newParams);
		if(pSystem->GetActiveView() != rush_View)
			pSystem->SetActiveView(rush_View);*/
	}
	Timer.timer+=frameTime;
	if(Timer.timer <Timer.Last_Time)
	{
		dir=gEnv->pRenderer->GetCamera().GetViewdir();
		if(pBRC->GetHits() != 1)
		{
			pos+=speed*dir*frameTime;
			float z_off=gEnv->p3DEngine->GetTerrainElevation(pos.x,pos.y);
			if(pos.z-z_off<0.1)
				pos.z=(float)(z_off+0.1);
			dir=gEnv->pRenderer->GetCamera().GetViewdir();
			IEntity *player=gEnv->pEntitySystem->GetEntity(g_pGame->GetClientActorId());
			player->SetPos(pos);
		}
	}
	if(Timer.timer>=Timer.Last_Time)
	{
		if(!once)
		{
			once=true;
			CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
			pActor->SetMaxHealth(100000);
			pActor->SetHealth((float)(curhealth+damage));
			ExplosionInfo einfo(pActor->GetEntityId(), 0, 0, damage , pActor->GetEntity()->GetPos(), Vec3(0,0,0),0.1f, 5.0f,  0.1,  5.0f , 0 , 1000.0f , 0.0f , 0);
			einfo.SetEffect("explosions.Grenade_SCAR.character",1.0f,0.2f);  //Need to change
			einfo.type = g_pGame->GetGameRules()->GetHitTypeId( "Explosion" );
			g_pGame->GetGameRules()->QueueExplosion(einfo);
			RestoreInventory();
			gEnv->p3DEngine->SetPostEffectParam( "ImageGhosting_Amount", 0 );
		}
	}
	if(Timer.timer>=Timer.Last_Time+0.1)
	{
		g_pGame->GetIGameFramework()->GetIViewSystem()->SetActiveView(default_View);
		g_pGame->GetIGameFramework()->GetIViewSystem()->RemoveView(rush_View);
		HasPreProcessed=false;
		hasSet=false;
		Timer.timer=0;
		CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
		pActor->SetMaxHealth(maxhealth);
		pActor->SetHealth(curhealth);
		pBRC->Load();
		IEntity *pEntity=gEnv->pEntitySystem->FindEntityByName("Rush_Particle");
		if(pEntity)
			pEntity->Hide(true);
	}
}

void RushModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}