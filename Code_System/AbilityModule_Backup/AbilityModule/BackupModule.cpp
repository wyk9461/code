#include "StdAfx.h"
#include "BackupModule.h"
#include "ItemSystem.h"


void BackupModule::PreProcess()
{
	if(reInit)
	{
		Timer.Last_Time=5.f;
		Timer.Cd=10.f;
		reInit=false;
	}
	Timer.Cd=0;
	Timer.timer=0;
	HasPreProcessed=true;
	hasSet=false;
}

void BackupModule::Process()
{

}

void BackupModule::PostProcess(const float frameTime /* = 0 */)
{
	if(!hasSet)
	{
		HasPreProcessed=false;
		finished=true;
		hasSet=true;
		SEntitySpawnParams params;
		params.sName="Backup";
		params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
		params.vScale=Vec3(1,1,1);
		params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("BasicEntity");

		pEntity=gEnv->pEntitySystem->SpawnEntity(params);
		pEntity->LoadGeometry(0,"Editor/Objects/spawnpointhelper.cgf");
		pEntity->EnablePhysics(false);

		//pEntity->SetMaterial(gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("spawn"));
		CActor *pSelfActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
		CItemSystem* pItemSystem = static_cast<CItemSystem*> (g_pGame->GetIGameFramework()->GetIItemSystem());
		IItem *pItem=pSelfActor->GetCurrentItem(false);
		//store_weapon_id=pItem->GetEntity()->GetId();
		pItemSystem->SerializePlayerLTLInfo(false);
		CActor *player=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
		health=player->GetHealth();
	}
	Timer.timer+=frameTime;
	if(Timer.timer>=Timer.Last_Time)
	{
		CActor *player=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
		player->GetEntity()->SetPos(pEntity->GetPos());
		player->SetHealth((float)(health*backup_multiplier));
		RestoreInventory();
		hasSet=false;
		gEnv->pEntitySystem->RemoveEntity(pEntity->GetId());
		pEntity=NULL;
	}
}


void BackupModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}