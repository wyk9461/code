#ifndef _BACKUPMODULE_H_
#define _BACKUPMODULE_H_

#include "PlayerBasedAbility.h"

class BackupModule : public PlayerBasedAbility
{
private:
	IEntity *pEntity;
	float backup_multiplier;
	float health;
	bool hasSet;

public:
	BackupModule()
		:	PlayerBasedAbility("Backup",100)
		,	pEntity(NULL)
		,	backup_multiplier(0.8)
		,	health(1000)
		,	hasSet(false){}

	~BackupModule(){}

	void PreProcess();
	void Process();
	void PostProcess(const float frameTime = 0);
	void Cool_Down(const float frameTime);

	inline const bool isSet(){return hasSet;}
};
#endif
