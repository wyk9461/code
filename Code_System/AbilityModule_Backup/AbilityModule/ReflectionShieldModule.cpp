#include "StdAfx.h"
#include "ReflectionShieldModule.h"

void ReflectionShieldModule::PreProcess()
{
	if(reInit)
	{
		Timer.Last_Time=6.f;
		Timer.Cd=10.f;
		reInit=false;
	}
	HasPreProcessed=true;
	Timer.timer=0;
	hasSet=false;
	multiplier=0.6;
}

void ReflectionShieldModule::Process()
{

}

void ReflectionShieldModule::PostProcess(const float frameTime /* = 0 */)
{
	Timer.timer+=frameTime;
	if(Timer.timer>=Timer.Last_Time)
	{
		hasSet=false;
		HasPreProcessed=false;
		Timer.Cdtimer=0;
		pEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.ShieldBreak");
		pEffect->Spawn(true,IParticleEffect::ParticleLoc(g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos()));
		Timer.timer=0;
		gEnv->p3DEngine->SetPostEffectParam( "VolumetricScattering_Amount", 0.0f );
		return;
	}
	if(!hasSet)
	{
		finished=true;
		hasSet=true;
		gEnv->p3DEngine->SetPostEffectParam( "VolumetricScattering_Amount", 1.0f );
		gEnv->p3DEngine->SetPostEffectParam( "VolumetricScattering_Tilling", 5.0f );
		gEnv->p3DEngine->SetPostEffectParam( "VolumetricScattering_Speed", 10.0f );
		gEnv->p3DEngine->SetPostEffectParamVec4( "clr_VolumetricScattering_Color", Vec4(0.6f, 0.75f, 1.0f,1.0f));
	}
}

void ReflectionShieldModule::ReflectionDamage(const HitInfo &hit)
{
	HitInfo info=hit;
	info.targetId=hit.shooterId;
	info.shooterId=g_pGame->GetClientActorId();
	info.damage=hit.damage*multiplier;
	g_pGame->GetGameRules()->ClientHit(info);
	Vec3 shieldPos=hit.pos;
	shieldPos.x+=-1*hit.dir.x;
	shieldPos.y+=-1*hit.dir.y;
	pEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.Reflection");
	pEffect->Spawn(true,IParticleEffect::ParticleLoc(shieldPos,-hit.dir));
}

void ReflectionShieldModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}