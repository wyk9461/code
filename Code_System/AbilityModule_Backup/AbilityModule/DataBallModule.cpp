#include "StdAfx.h"
#include "DataBallModule.h"


void DataBallModule::PreProcess()
{
	if(reInit)
	{
		Timer.Last_Time=2.f;
		Timer.Cd=10.f;
		reInit=false;
	}
	Timer.timer=0;
	stacollected=0;
	speed=0.1;
	HitEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.DataBall_Hit");
	SEntitySpawnParams params;
	params.sName="DataBall";
	params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
	params.vScale=Vec3(1,1,1);
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	pEffect=gEnv->pEntitySystem->SpawnEntity(params);
	SmartScriptTable proot=pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.DataBall");
	Script::CallMethod(proot,"OnReset");
	HasPreProcessed=true;
	hasSet=false;
}


void DataBallModule::Process(ray_hit &hit)
{
	dir=gEnv->pRenderer->GetCamera().GetViewdir();
	pos=hit.pt;
	pEffect->SetPos(pos);
	//pEffect->ParticleLoc(hit.pt,dir);
}

void DataBallModule::PostProcess(const float frameTime /* = 0 */)
{
	if(!hasSet)
	{
		hasSet=true;
		finished=true;
		Timer.Cdtimer=0;
		g_pGame->GetWorldStability()->StabilityCost(cost);
	}
	if(Timer.timer<Timer.Last_Time)
	{
		pos+=(dir*speed);
		float z_off=gEnv->p3DEngine->GetTerrainElevation(pos.x,pos.y);
		if(pos.z-z_off<0.5)
			pos.z=(float)(z_off+0.5);
		pEffect->SetPos(pos);
		//pEffect->ParticleLoc(pos,dir);
		Timer.timer+=frameTime;
	}
	else
	{
		Collectdata();
		BallExplode();
	}
}

void DataBallModule::Collectdata()
{
	IPhysicalEntity **nearbyEntities;
	int n=gEnv->pPhysicalWorld->GetEntitiesInBox(pos-Vec3(10),pos+Vec3(10),nearbyEntities,ent_living);
	for(int i=0;i<n;i++)
	{
		IEntity* pEntity=gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
		HitInfo hit;
		hit.shooterId=g_pGame->GetClientActorId();
		hit.targetId=pEntity->GetId();
		hit.dir=-(gEnv->pEntitySystem->GetEntity(hit.targetId)->GetPos()-pos).normalize();
		hit.bulletType=0;
		hit.hitViaProxy=false;
		hit.partId=0;
		hit.type=1;
		hit.projectileId=1;
		hit.weaponId=1;
		if(hit.targetId!=hit.shooterId)
		{
			HitEffect->Spawn(true,IParticleEffect::ParticleLoc(gEnv->pEntitySystem->GetEntity(hit.targetId)->GetPos(),hit.dir));
			hit.damage=damage;
			g_pGame->GetGameRules()->ClientHit(hit);
			stacollected+=(int)(damage*multiplier);
		}
	}
}

void DataBallModule::BallExplode()
{
	IEntity *player=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity();
	ExplosionInfo einfo(player->GetId(), 0, 0, (float)(stacollected) , pos, Vec3(0,0,0),0.1f, 10.0f,  0.1,  10.0f , 0 , 1000.0f , 0.0f , 0);
	einfo.SetEffect("Code_System.Module_Effect.DataBall_Explosion",1.0f,0.2f);
	einfo.type = g_pGame->GetGameRules()->GetHitTypeId( "Explosion" );
	g_pGame->GetGameRules()->QueueExplosion(einfo);
	stacollected/=2;
	g_pGame->GetWorldStability()->StabilityGain(stacollected);
	hasSet=false;
	HasPreProcessed=false;
	gEnv->pEntitySystem->RemoveEntity(pEffect->GetId());
	g_pGame->GetWorldStability()->StabilityGain(stacollected);
}

void DataBallModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}