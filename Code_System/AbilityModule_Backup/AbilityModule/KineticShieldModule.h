#ifndef _KINETICSHIELDMODULE_H_
#define _KINETICSHIELDMODULE_H_

#include "PlayerBasedAbility.h"
#include "IParticles.h"


class KineticShieldModule : public PlayerBasedAbility
{
private:
	IParticleEffect *pEffect;
	float maxhealth;
	float health;
	bool hasSet;
public:
	KineticShieldModule()
	:	PlayerBasedAbility("KineticShield",100)
	,	pEffect(NULL)
	,	maxhealth(2000)
	,	health(500)
	,	hasSet(false){}

	~KineticShieldModule(){}

	void PreProcess();
	void Process();
	void PostProcess(const float frameTime = 0);
	void Cool_Down(const float frameTime);

	void ShieldDamage(const HitInfo &hit);
	void BreakShield();

	inline const bool isSet(){return hasSet;}
};
#endif