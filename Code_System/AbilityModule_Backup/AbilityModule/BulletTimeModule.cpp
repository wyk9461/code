#include "StdAfx.h"
#include "BulletTimeModule.h"

void BulletTimeModule::PreProcess()
{
	if(reInit)
	{
		Timer.Last_Time=3.0f;
		Timer.Cd=10;
		reInit=false;
	}
	HasPreProcessed=true;
	Timer.timer=0;
	hasSet=false;
}

void BulletTimeModule::Process()
{
	gEnv->p3DEngine->SetPostEffectParam("Global_Contrast", 0.3f);
}

void BulletTimeModule::PostProcess(const float frameTime /* = 0 */)
{
	ICVar *timeScaleCVar=gEnv->pConsole->GetCVar("t_Scale");
	float scale=timeScaleCVar->GetFVal();
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	if(!hasSet)
	{
		g_pGame->GetWorldStability()->StabilityCost(cost);
		finished=true;
		timeScaleCVar->Set(0.3f);
		gEnv->pRenderer->EF_SetPostEffectParam("Global_User_Saturation", -0.5);
		hasSet=true;
	}
	if(Timer.timer<Timer.Last_Time)
	{
		Timer.timer+=frameTime;
		pActor->SetSpeedMultipler(SActorParams::eSMR_Internal,(float)(1/scale));
		pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,(float)(1/scale));
		pActor->SetSpeedMultipler(SActorParams::eSMR_Item,(float)(1/scale));
	}
	if(Timer.timer>Timer.Last_Time-1 && Timer.timer<Timer.Last_Time)
		timeScaleCVar->Set(scale+=1/1*frameTime);
	if(Timer.timer>Timer.Last_Time)
	{
		gEnv->pRenderer->EF_SetPostEffectParam("Global_User_Saturation", 1);
		ICVar *timeScaleCVar=gEnv->pConsole->GetCVar("t_Scale");
		timeScaleCVar->Set(1.0f);
		Timer.Cdtimer=0;
		HasPreProcessed=false;
		pActor->SetSpeedMultipler(SActorParams::eSMR_Internal,1);
		pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,1);
		pActor->SetSpeedMultipler(SActorParams::eSMR_Item,1);
		hasSet=false;
	}
}

void BulletTimeModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}