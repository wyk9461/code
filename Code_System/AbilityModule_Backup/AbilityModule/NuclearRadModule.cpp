#include "StdAfx.h"
#include "NuclearRadModule.h"

void NuclearRadModule::PreProcess()
{
	if(reInit)
	{
		Timer.Last_Time=10.f;
		Timer.Cd=10.f;
		reInit=false;
	}
	g_pGame->GetBasicRayCast()->ChangeFlag(ent_terrain);
	HasPreProcessed=true;
	Timer.Cdtimer=0;
	Timer.timer=0;
	SEntitySpawnParams params;
	params.sName="NuclearRad";
	params.vScale=Vec3(1,1,1);
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	pEffect=gEnv->pEntitySystem->SpawnEntity(params);
	SmartScriptTable proot=pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.Nuclear_Logo");
	Script::CallMethod(proot,"OnReset");
}


void NuclearRadModule::Process(ray_hit &hit)
{
	pEffect->SetPos(hit.pt);
	pos=pEffect->GetPos();
}

void NuclearRadModule::PostProcess(const float frameTime /* = 0 */)
{
	if(!hasSet)
	{
		HasPreProcessed=false;
		finished=true;
		SmartScriptTable proot=pEffect->GetScriptTable();
		SmartScriptTable pprops;
		proot->GetValue("Properties",pprops);
		pprops->SetValue("ParticleEffect","Code_System.Module_Effect.NuclearRad");
		Script::CallMethod(proot,"OnReset");
		g_pGame->GetBasicRayCast()->Load();
	}
	if(Timer.timer<Timer.Last_Time)
	{
		IPhysicalEntity **nearbyEntities;
		int num=gEnv->pPhysicalWorld->GetEntitiesInBox(pos-Vec3(10),pos+Vec3(10),nearbyEntities,ent_living);
		for(int i=0;i<num;i++)
		{
			IEntity* tEntity=gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
			CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(tEntity->GetId());
			if((tEntity->GetPos()-pos).GetLength() < 5.0f )
			{
				if(pActor->GetHealth() > 10)
					pActor->SetHealth(pActor->GetHealth()-damage);
				if(!pActor->IsDead() && !pActor->IsPlayer())
				{
					pActor->SetSpeedMultipler(SActorParams::eSMR_Internal,speed_multi);
					pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,speed_multi);
					pActor->SetSpeedMultipler(SActorParams::eSMR_Item,speed_multi);
				}
			}
			else
			{
				if(!pActor->IsDead())
				{
					pActor->SetSpeedMultipler(SActorParams::eSMR_Internal,1);
					pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,1);
					pActor->SetSpeedMultipler(SActorParams::eSMR_Item,1);
				}
			}
		}
	}
	else
	{
		IPhysicalEntity **nearbyEntities;
		int num=gEnv->pPhysicalWorld->GetEntitiesInBox(pos-Vec3(6),pos+Vec3(6),nearbyEntities,ent_living);
		for(int i=0;i<num;i++)
		{
			IEntity* tEntity=gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
			CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(tEntity->GetId());
			if(!pActor->IsDead())
			{
				pActor->SetSpeedMultipler(SActorParams::eSMR_Internal,1);
				pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,1);
				pActor->SetSpeedMultipler(SActorParams::eSMR_Item,1);
			}
		}
		hasSet=false;
		gEnv->pEntitySystem->RemoveEntity(pEffect->GetId());
		pEffect=NULL;
	}
	Timer.timer+=frameTime;
}

void NuclearRadModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}