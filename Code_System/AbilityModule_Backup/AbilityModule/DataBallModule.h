#ifndef _DATABALLMODULE_H_
#define _DATABALLMODULE_H_

#include "TargetBasedAbility.h"

class DataBallModule : public TargetBasedAbility
{
private:
	IEntity *pEffect;
	IParticleEffect *HitEffect;
	float speed;
	Vec3 dir;
	Vec3 pos;
	float damage;
	int stacollected;
	float multiplier;
	bool hasSet;
public:
	DataBallModule()
		:	TargetBasedAbility("DataBall",100)
		,	pEffect(NULL)
		,	speed(10.f)
		,	dir(0,0,0)
		,	pos(0,0,0)
		,	damage(100.f)
		,	stacollected(0)
		,	multiplier(0.5)
		,	hasSet(false){}
	~DataBallModule(){}

	void PreProcess();
	void Process(ray_hit &hit);
	void PostProcess(const float frameTime = 0);
	void Cool_Down(const float frameTime);

	void Collectdata();
	void BallExplode();

	inline const bool isSet(){return hasSet;}
};
#endif