#include "StdAfx.h"
#include "FloodModule.h"
#include "Code_System/BasicRayCast.h"

void FloodModule::PreProcess()
{
	if(reInit)
	{
		Timer.Last_Time=5.f;
		Timer.Cd=10.f;
		reInit=false;
	}
	g_pGame->GetBasicRayCast()->ChangeFlag(ent_all);
	g_pGame->GetBasicRayCast()->ChangeDistance(1.5);
	Timer.Cdtimer=0;
	Timer.timer=0;
	Timer.Cdtimer=0;
	HasPreProcessed=true;
	hasSet=false;
	SEntitySpawnParams params;
	params.sName="Flood";
	params.vScale=Vec3(1,1,1);
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	pEffect=gEnv->pEntitySystem->SpawnEntity(params);
	SmartScriptTable proot=pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.PreFlood");
	Script::CallMethod(proot,"OnReset");
}

void FloodModule::Process(ray_hit &hit)
{
		pEffect->SetPos(hit.pt);
}

void FloodModule::PostProcess(const float frameTime /* = 0 */)
{
	if(!hasSet)
	{
		pEffect->SetRotation(Quat::CreateRotationVDir(gEnv->pRenderer->GetCamera().GetViewdir()));
		HasPreProcessed=false; 
		finished=true;
		hasSet=true;
		SmartScriptTable proot=pEffect->GetScriptTable();
		SmartScriptTable pprops;
		proot->GetValue("Properties",pprops);
		pprops->SetValue("ParticleEffect","Code_System.Module_Effect.Flood");
		Script::CallMethod(proot,"OnReset");
		g_pGame->GetBasicRayCast()->Load();
	}
	if(Timer.timer >=Timer.Last_Time)
	{
		hasSet=false;
		gEnv->pEntitySystem->RemoveEntity(pEffect->GetId());
		pEffect=NULL;
	}
	Timer.timer+=frameTime;
}

void FloodModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}