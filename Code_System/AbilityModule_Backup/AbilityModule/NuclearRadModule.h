#ifndef _NUCLEARRADMODULE_H_
#define _NUCLEARRADMODULE_H_

#include "TargetBasedAbility.h"
#include "IEntity.h"


class NuclearRadModule : public TargetBasedAbility
{
private:
	IEntity *pEffect;
	float damage;
	float speed_multi;
	Vec3 pos;
	bool hasSet;

public:
	NuclearRadModule()
		:	TargetBasedAbility("NuclearRad",100)
		,	pEffect(NULL)
		,	damage(1)
		,	speed_multi(0.8)
		,	pos(0,0,0)
		,	hasSet(false){}
	~NuclearRadModule(){}

	void PreProcess();
	void Process(ray_hit &hit);
	void PostProcess(const float frameTime = 0);
	void Cool_Down(const float frameTime);

	inline const bool isSet(){return hasSet;}
};
#endif