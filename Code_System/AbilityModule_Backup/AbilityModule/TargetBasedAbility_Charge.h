#ifndef _TARGETBASEDABILITY_CHARGE_H_
#define _TARGETBASEDABILITY_CHARGE_H_

#include "BaseAbility.h"

class TargetBasedAbility_Charge : public BaseAbility
{

public:
	TargetBasedAbility_Charge(const string str , const int co)
		:	BaseAbility(str , co){}

	// Description :
	//		When player press the ability button.
	//		Reset module's params.
	virtual void PreProcess() = 0;


	// Description :
	//		When player hold the ability button.
	//		Process position information and so on.
	//	Arguments : 
	//		hit - See ray_hit structure.
	virtual void Process(ray_hit &hit,const float frameTime) = 0;

	// Description :
	//		When player release the ability button.
	//		Do what a module need to do.
	//	Arguments : 
	//		frameTime - time per frame
	virtual void PostProcess(const float frameTime = 0) = 0;

};
#endif