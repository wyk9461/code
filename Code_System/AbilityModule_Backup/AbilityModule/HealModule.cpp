#include "StdAfx.h"
#include "HealModule.h"

void HealModule::PreProcess()
{
	if(reInit)
	{
		Timer.Cd=10.f;
		reInit=false;
	}
	Timer.Cdtimer=0;
	HasPreProcessed=true;
	pEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.DataBall_Explosion");
}

void HealModule::Process()
{

}

void HealModule::PostProcess(const float frameTime /* = 0 */)
{
	finished=true;
	HasPreProcessed=false;
	g_pGame->GetWorldStability()->StabilityCost(cost);
	CActor *player=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	player->SetHealth(player->GetHealth()+heal_amount);
}

void HealModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}