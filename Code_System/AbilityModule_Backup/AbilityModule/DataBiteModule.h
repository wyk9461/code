#ifndef _DATABITEMODULE_H_
#define _DATABITEMODULE_H_

#include "PlayerBasedAbility.h"

class DataBiteModule : public PlayerBasedAbility
{
private:
	IEntity *pEffect;
	CActor *pTarget;
	float heal_rate;
	float heal_log;
	float maxhealth;
	bool hasLaunched;
	bool hasSet;

public:
	DataBiteModule()
		:	PlayerBasedAbility("DataBite",100)
		,	pEffect(NULL)
		,	pTarget(NULL)
		,	heal_rate(1)
		,	heal_log(0)
		,	maxhealth(100)
		,	hasLaunched(false)
		,	hasSet(false){}

	~DataBiteModule(){}

	void PreProcess();
	void Process();
	void PostProcess(const float frameTime = 0);
	void Cool_Down(const float frameTime);

	void DataExplode();

	inline const bool isSet(){return hasSet;}
	inline const bool isLaunched(){return hasLaunched;}
	inline const CActor* getTarget(){return pTarget;}

	inline void setHasHit(){hasSet=true;}
	inline void setHitTarget(CActor *target){pTarget=target;}
};
#endif