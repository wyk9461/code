#include "StdAfx.h"
#include "BaseAbility.h"
#include "Game.h"


void BaseAbility::StoreInventory()
{
	CActor *pSelfActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	CItemSystem* pItemSystem = static_cast<CItemSystem*> (g_pGame->GetIGameFramework()->GetIItemSystem());
	IItem *pItem=pSelfActor->GetCurrentItem(false);
	/*if(pItem)
	store_weapon_id=pItem->GetEntity()->GetId();*/
	pItemSystem->SerializePlayerLTLInfo(false);
	IInventory* pInventory = pSelfActor->GetInventory();
	pInventory->RMIReqToServer_RemoveAllItems();
}

void BaseAbility::RestoreInventory()
{
	CActor *pSelfActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	CItemSystem* pItemSystem = static_cast<CItemSystem*> (g_pGame->GetIGameFramework()->GetIItemSystem());
	pItemSystem->SerializePlayerLTLInfo(true);
	//pSelfActor->SelectLastItem(true);
	/*if(store_weapon_id)
		pSelfActor->SelectItem(store_weapon_id,true,true);*/
}

bool BaseAbility::AddComponent(BaseAbility *new_module)
{
	std::list<BaseAbility*>::iterator iter = std::find(m_Component.begin(),m_Component.end(),new_module);
	if(iter == m_Component.end())
	{
		m_Component.push_back(new_module);
		return true;
	}
	return false;
}

bool BaseAbility::RemoveComponent(BaseAbility *old_module)
{
	std::list<BaseAbility*>::iterator iter = std::find(m_Component.begin(),m_Component.end(),old_module);
	if(iter != m_Component.end())
	{
		m_Component.remove(old_module);
		return true;
	}
	return false;
}