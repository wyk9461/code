#ifndef _BULLETTIMEMODULE_H_
#define _BULLETTIMEMODULE_H_

#include "PlayerBasedAbility.h"

class BulletTimeModule : public PlayerBasedAbility
{
private:
	bool hasSet;
public:
	BulletTimeModule()
	:	PlayerBasedAbility("BulletTime",100)
	,	hasSet(false){}

	~BulletTimeModule(){}

	void PreProcess();
	void Process();
	void PostProcess(const float frameTime = 0);
	void Cool_Down(const float frameTime);

	inline const bool isSet(){return hasSet;}
};
#endif