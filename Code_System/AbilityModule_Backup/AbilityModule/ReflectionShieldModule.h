#ifndef _REFLECTIONSHIELDMODULE_H_
#define _REFLECTIONSHIELDMODULE_H_

#include "PlayerBasedAbility.h"
#include "IParticles.h"


class ReflectionShieldModule : public PlayerBasedAbility
{
private:
	IParticleEffect *pEffect;
	float multiplier;
	bool hasSet;
public:
	ReflectionShieldModule()
	:	PlayerBasedAbility("ReflectionShield",100)
	,	pEffect(NULL)
	,	multiplier(0.6)
	,	hasSet(false){}

	~ReflectionShieldModule(){}

	void PreProcess();
	void Process();
	void PostProcess(const float frameTime = 0);
	void Cool_Down(const float frameTime);

	void ReflectionDamage(const HitInfo &hit);

	inline const bool isSet(){return hasSet;}
};
#endif