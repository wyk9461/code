#ifndef _EXPLODESHIELDMODULE_H_
#define _EXPLODESHIELDMODULE_H_

#include "PlayerBasedAbility.h"
#include "IParticles.h"

class ExplodeShieldModule : public PlayerBasedAbility
{
private:
	IParticleEffect *pEffect;
	float damage;
	float multiplier;
	float curhealth;
	float maxhealth;
	bool hasSet;
	bool is_not_explode;

public:
	ExplodeShieldModule()
	:	PlayerBasedAbility("ExplodeShield",100)
	,	damage(0)
	,	multiplier(0.5)
	,	curhealth(1000)
	,	maxhealth(1000)
	,	hasSet(false)
	,	is_not_explode(true){}

	void PreProcess();
	void Process();
	void PostProcess(const float frameTime = 0);
	void Cool_Down(const float frameTime);

	void ExploShieldDamage(const HitInfo &hit);

	inline const bool isSet(){return hasSet;}

};

#endif