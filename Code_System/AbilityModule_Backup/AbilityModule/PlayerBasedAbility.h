#ifndef _PLAYERBASEDABILITY_H_
#define _PLAYERBASEDABILITY_H_

#include "BaseAbility.h"


class PlayerBasedAbility : public BaseAbility
{

public:
	PlayerBasedAbility(const string str , const int co)
		:	BaseAbility(str,co){}

	// Description :
	//		When player press the ability button.
	//		Reset module's params.
	virtual void PreProcess() = 0;


	// Description :
	//		When player hold the ability button.
	//		Process position information and so on.
	//	Arguments : 
	//		hit - See ray_hit structure.
	virtual void Process() = 0;

	// Description :
	//		When player release the ability button.
	//		Do what a module need to do.
	//	Arguments : 
	//		frameTime - time per frame
	virtual void PostProcess(const float frameTime = 0) = 0;


};
#endif