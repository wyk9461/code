#include "StdAfx.h"
#include "ShockWaveModule.h"

void ShockWaveModule::PreProcess()
{
	if(reInit)
	{
		Timer.Cd=10;
		reInit=false;
	}
	HasPreProcessed=true;
	SEntitySpawnParams params;
	params.sName="ShockWave";
	params.vScale=Vec3(1,1,1);
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	pEffect=gEnv->pEntitySystem->SpawnEntity(params);
	SmartScriptTable proot=pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.ShockWave");
	Script::CallMethod(proot,"OnReset");
}

void ShockWaveModule::Process()
{
	pEffect->SetPos(g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos());
}

void ShockWaveModule::PostProcess(const float frameTime /* = 0 */)
{
	g_pGame->GetWorldStability()->StabilityCost(cost);
	HasPreProcessed=false;
	pressure=5000;
	IEntity *player=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity();
	int num=gEnv->pPhysicalWorld->GetEntitiesInBox(player->GetPos()-Vec3(radius),player->GetPos()+Vec3(radius),nearbyEntities,ent_living);
	for(int i=0;i<num;i++)
	{
		IEntity* pEntity=gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
		CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());
		if(pActor && !pActor->IsFallen() && !pActor->IsDead() && !pActor->IsPlayer())
			pActor->Fall();
	}
	ExplosionInfo einfo(player->GetId(), 0, 0, 0, player->GetPos(), Vec3(0,0,0),1, radius,  2.0f,  radius,0, pressure,0.0f, 0);
	einfo.SetEffect("Code_System.Module_Effect.ShcokWave_Finished",1.0f,0.2f);
	einfo.type = g_pGame->GetGameRules()->GetHitTypeId( "Explosion" );
	g_pGame->GetGameRules()->QueueExplosion(einfo);
	pEffect->Hide(true);
	g_pGame->GetWorldStability()->StabilityCost(cost);
	gEnv->pEntitySystem->RemoveEntity(pEffect->GetId());
	pEffect=NULL;
	finished=true;
}

void ShockWaveModule::Cool_Down(const float frameTime)
{
	Timer.timer+=frameTime;
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.timer=0;
		Timer.Cdtimer=0;
	}
}