#include "StdAfx.h"
#include "AbilityModule.h"

#include "BlendTypes.h"
#include "BlendedEffect.h"
#include "BlendNode.h"
#include "ItemSystem.h"
#include "Weapon.h"

//------------------------------------------------------------------------
AbilityModule::AbilityModule()
{
	//Init
	max_index=3;
	InstlledModule.push_back(eMT_Teleport);
	InstlledModule.push_back(eMT_Rush);
	InstlledModule.push_back(eMT_Grab);
	StartFunction=false;
	pWorld=gEnv->pPhysicalWorld;
	ModuleSelected[0]=false;
	ModuleSelected[1]=false;
	ModuleSelected[2]=false;
	query_flag=ent_all;
	it=InstlledModule.begin();
	pRenderer=gEnv->pRenderer;
	color[0] =0.258824f;
	color[1] =0.258824f;
	color[2] =0.435294f;
	color[3] =1.0f;
	text_timer=0;
	ShowChangeText=false;
	m_Teleport = new TeleportModule();
	m_ShockWave = new ShockWaveModule();
	m_BulletTime = new BulletTimeModule();
	m_Shield = new KineticShieldModule();
	m_Reflection = new ReflectionShieldModule();
	m_ExploShield = new ExplodeShieldModule();
	m_DataBall = new DataBallModule();
	m_Heal = new HealModule();
	m_Backup = new BackupModule();
	m_Rush = new RushModule();
	m_NuclearRad = new NuclearRadModule();
	m_Flood = new FloodModule();
	m_DataBite = new DataBiteModule();
	m_Grab = new GrabModule();
	InitModuleTimer();
	InitModuleName();
}

//------------------------------------------------------------------------
AbilityModule::~AbilityModule()
{

}

//------------------------------------------------------------------------
void AbilityModule::Update(float frameTime)
{
	if(StartFunction)
	{
		pBRC=g_pGame->GetBasicRayCast();
		PerProcessAllModule();
		ProcessAllModule(frameTime);
	}
	PostProcessAllModule(frameTime);
	//ScreenEffect && finished
	AfterFinished(frameTime);
}

//------------------------------------------------------------------------
void AbilityModule::DisplayModule()
{
	if(*it==eMT_Teleport)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : Teleport");
	if(*it==eMT_ShockWave)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : ShockWave");
	if(*it==eMT_BulletTime)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : BulletTime");
	if(*it==eMT_Shield)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : Shield");
	if(*it==eMT_Reflection)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : Reflection");
	if(*it==eMT_ExploShield)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : ExploShield");
	if(*it==eMT_DataBall)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : DataBall");
	if(*it==eMT_Heal)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : Heal");
	if(*it==eMT_Backup)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : Backup");
	if(*it==eMT_Rush)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : Rush");
	if(*it==eMT_NuclearRad)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : NuclearRad");
	if(*it==eMT_Flood)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : Flood");
	if(*it==eMT_DataBite)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : DataBite");
}

//------------------------------------------------------------------------
void AbilityModule::AfterFinished(float frameTime)
{
	//Teleport
	if(m_Teleport->isFinished())
		m_Teleport->Cool_Down(frameTime);
	//ShockWave
	if(m_ShockWave->isFinished())
		m_ShockWave->Cool_Down(frameTime);
	//BulletTime
	if(m_BulletTime->isFinished())
		m_BulletTime->Cool_Down(frameTime);
	//Shield
	if(m_Shield->isFinished())
		m_Shield->Cool_Down(frameTime);
	//Reflection
	if(m_Reflection->isFinished())
		m_Reflection->Cool_Down(frameTime);
	//ExploShield
	if(m_ExploShield->isFinished())
		m_ExploShield->Cool_Down(frameTime);
	//DataBall
	if(m_DataBall->isFinished())
		m_DataBall->Cool_Down(frameTime);
	//Heal
	if(m_Heal->isFinished())
		m_Heal->Cool_Down(frameTime);
	//Backup
	if(m_Backup->isFinished())
		m_Backup->Cool_Down(frameTime);
	//Rush
	if(m_Rush->isFinished())
		m_Rush->Cool_Down(frameTime);
	//NuclearRad
	if(m_NuclearRad->isFinished())
		m_NuclearRad->Cool_Down(frameTime);
	//Flood
	if(m_Flood->isFinished())
		m_Flood->Cool_Down(frameTime);
	//DataBite
	if(m_DataBite->isFinished())
		m_DataBite->Cool_Down(frameTime);
	//Grab
	if(m_Grab->isFinished())
		m_Grab->Cool_Down(frameTime);
}

//------------------------------------------------------------------------
const bool AbilityModule::HasShield()
{
	for(int i=0;i<3;i++)
	{
		if(InstlledModule[i] == eMT_Shield || InstlledModule[i] == eMT_Reflection || InstlledModule[i] == eMT_ExploShield)
			return true;
	}
	return false;
}

//------------------------------------------------------------------------
void AbilityModule::InitModuleTimer()
{
	ModuleTimer[0]=&m_Teleport->GetTimer();
	ModuleTimer[1]=&m_ShockWave->GetTimer();
	ModuleTimer[2]=&m_BulletTime->GetTimer();
	ModuleTimer[3]=&m_Shield->GetTimer();
	ModuleTimer[4]=&m_Reflection->GetTimer();
	ModuleTimer[5]=&m_ExploShield->GetTimer();
	ModuleTimer[6]=&m_DataBall->GetTimer();
	ModuleTimer[7]=&m_Heal->GetTimer();
	ModuleTimer[8]=&m_Backup->GetTimer();
	ModuleTimer[9]=&m_Rush->GetTimer();
	ModuleTimer[10]=NULL;
	ModuleTimer[11]=NULL;
	ModuleTimer[12]=&m_NuclearRad->GetTimer();
	ModuleTimer[13]=&m_Flood->GetTimer();
	ModuleTimer[14]=&m_DataBite->GetTimer();
	ModuleTimer[15]=&m_Grab->GetTimer();
}

//------------------------------------------------------------------------
void AbilityModule::InitModuleName()
{
	ModuleName[0]=&m_Teleport->GetName();
	ModuleName[1]=&m_ShockWave->GetName();
	ModuleName[2]=&m_BulletTime->GetName();
	ModuleName[3]=&m_Shield->GetName();
	ModuleName[4]=&m_Reflection->GetName();
	ModuleName[5]=&m_ExploShield->GetName();
	ModuleName[6]=&m_DataBall->GetName();
	ModuleName[7]=&m_Heal->GetName();
	ModuleName[8]=&m_Backup->GetName();
	ModuleName[9]=&m_Rush->GetName();
	ModuleName[10]=NULL;
	ModuleName[11]=NULL;
	ModuleName[12]=&m_NuclearRad->GetName();
	ModuleName[13]=&m_Flood->GetName();
	ModuleName[14]=&m_DataBite->GetName();
	ModuleName[15]=&m_Grab->GetName();
}


void AbilityModule::PerProcessAllModule()
{
	if(*it==eMT_Teleport && !m_Teleport->isFinished() && !m_Teleport->isPreProcessed())
		m_Teleport->PreProcess();

	if(*it==eMT_ShockWave && !m_ShockWave->isFinished() && !m_ShockWave->isPreProcessed())
		m_ShockWave->PreProcess();

	if(*it==eMT_BulletTime && !m_BulletTime->isFinished())
		m_BulletTime->PreProcess();

	if(*it==eMT_Shield && !m_Shield->isFinished())
		m_Shield->PreProcess();

	if(*it==eMT_Reflection && !m_Reflection->isFinished())
		m_Reflection->PreProcess();

	if(*it==eMT_ExploShield && !m_ExploShield->isFinished())
		m_ExploShield->PreProcess();

	if(*it==eMT_DataBall && !m_DataBall->isPreProcessed() && !m_DataBall->isFinished())
		m_DataBall->PreProcess();

	if(*it==eMT_Heal && !m_Heal->isPreProcessed() && !m_Heal->isFinished())
		m_Heal->PreProcess();

	if(*it==eMT_Backup && !m_Backup->isPreProcessed() && !m_Backup->isFinished())
		m_Backup->PreProcess();

	if(*it==eMT_Rush && !m_Rush->isPreProcessed() && !m_Rush->isFinished())
		m_Rush->PreProcess();

	if(*it==eMT_NuclearRad && !m_NuclearRad->isPreProcessed() && !m_NuclearRad->isFinished())
		m_NuclearRad->PreProcess();

	if(*it==eMT_Flood && !m_Flood->isPreProcessed() && !m_Flood->isFinished())
		m_Flood->PreProcess();

	if(*it==eMT_DataBite && !m_DataBite->isPreProcessed() && !m_DataBite->isFinished())
		m_DataBite->PreProcess();

	if((*it==eMT_Grab && !m_Grab->isPreProcessed() && !m_Grab->isFinished()) || m_Grab->IsGrab())
		m_Grab->PreProcess(pBRC->GetRay_hit());
}

void AbilityModule::ProcessAllModule(const float frameTime)
{
	if(*it==eMT_Teleport && m_Teleport->isPreProcessed())
		m_Teleport->Process(pBRC->GetRay_hit());

	if(*it==eMT_ShockWave && m_ShockWave->isPreProcessed())
		m_ShockWave->Process();

	if(*it==eMT_BulletTime && m_BulletTime->isPreProcessed())
		m_BulletTime->Process();

	if(*it==eMT_Shield && m_Shield->isPreProcessed())
		m_Shield->Process();

	if(*it==eMT_Reflection && m_Reflection->isPreProcessed())
		m_Reflection->Process();

	if(*it==eMT_ExploShield && m_ExploShield->isPreProcessed())
		m_ExploShield->Process();

	if(*it==eMT_DataBall && m_DataBall->isPreProcessed())
		m_DataBall->Process(pBRC->GetRay_hit());

	if(*it==eMT_Heal && m_Heal->isPreProcessed())
		m_Heal->Process();

	if(*it==eMT_Backup && m_Backup->isPreProcessed())
		m_Backup->Process();

	if(*it==eMT_Rush && m_Rush->isPreProcessed())
		m_Rush->Process(pBRC->GetRay_hit(),frameTime);

	if(*it==eMT_NuclearRad && m_NuclearRad->isPreProcessed())
		m_NuclearRad->Process(pBRC->GetRay_hit());

	if(*it==eMT_Flood && m_Flood->isPreProcessed())
		m_Flood->Process(pBRC->GetRay_hit());

	if(*it==eMT_DataBite && m_DataBite->isPreProcessed())
		m_DataBite->Process();

	if(*it==eMT_Grab && m_Grab->isPreProcessed() && !m_Grab->IsGrab() && !m_Grab->IsDetected())
		m_Grab->Process(pBRC->GetRay_hit());
}

void AbilityModule::PostProcessAllModule(const float frameTime)
{
	if(!StartFunction)
	{
		if(m_Teleport->isPreProcessed())
			m_Teleport->PostProcess();

		if(m_ShockWave->isPreProcessed())
			m_ShockWave->PostProcess();

		if(m_Heal->isPreProcessed())
			m_Heal->PostProcess();

		if((m_BulletTime->isPreProcessed()) || m_BulletTime->isSet())
			m_BulletTime->PostProcess(frameTime);

		if((m_Shield->isPreProcessed()) || m_Shield->isSet())
			m_Shield->PostProcess(frameTime);

		if((m_Reflection->isPreProcessed()) || m_Reflection->isSet())
			m_Reflection->PostProcess(frameTime);

		if((m_ExploShield->isPreProcessed()) || m_ExploShield->isSet())
			m_ExploShield->PostProcess(frameTime);

		if((m_DataBall->isPreProcessed()) || m_DataBall->isSet())
			m_DataBall->PostProcess(frameTime);

		if((m_Backup->isPreProcessed()) || m_Backup->isSet())
			m_Backup->PostProcess(frameTime);

		if((m_Rush->isPreProcessed()) || m_Rush->isSet())
			m_Rush->PostProcess(frameTime);

		if((m_NuclearRad->isPreProcessed()) || m_NuclearRad->isSet())
			m_NuclearRad->PostProcess(frameTime);

		if((m_Flood->isPreProcessed()) || m_Flood->isSet())
			m_Flood->PostProcess(frameTime);

	}
	// For DataBite
	if(m_DataBite->isSet())
		m_DataBite->PostProcess(frameTime);

	//For Grab
	if(!StartFunction && !m_Grab->IsDetected() && m_Grab->isPreProcessed())
		m_Grab->PostProcess(pBRC->GetRay_hit(),frameTime);

	if(m_Grab->IsDetected())
		m_Grab->ThrowEntityExplode(frameTime);
}
