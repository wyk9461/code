#ifndef _DYNAMICNAVMESH_H_
#define _DYNAMICNAVMESH_H_

struct NevVec2
{
	int x;
	int y;
	NevVec2()
	:	x(0)
	,	y(0){}

	NevVec2(int _x, int _y)
	:	x(_x)
	,	y(_y){}

	friend const bool operator<(const NevVec2 &lhs, const NevVec2 &rhs)
	{
		return lhs.x < rhs.x && lhs.y < rhs.y;
	}

	NevVec2 &operator=(const NevVec2 &rhs)
	{
		if(this == &rhs)
			return *this;
		x = rhs.x;
		y = rhs.y;
		return *this;
	}

	const bool operator==(const NevVec2 &rhs){return x == rhs.x && y ==rhs.y;}
	const bool operator!=(const NevVec2 &rhs){return !operator==(rhs);}
};

struct DynamicNavNode
{
	int Id;
	Vec3 pos;
	std::list<int> accessible;
	int parent;
	bool hasAdjust;
	DynamicNavNode()
	:	pos(Vec3(0,0,0))
	,	parent(0)
	,	hasAdjust(false){}
	DynamicNavNode &operator= (const DynamicNavNode &rhs)
	{
		if(this == &rhs)
			return *this;
		Id = rhs.Id;
		pos = rhs.pos;
		accessible = rhs.accessible;
		parent = rhs.parent;
		hasAdjust = rhs.hasAdjust;
		return *this;
	}

	friend const bool operator<(const DynamicNavNode &lhs, const DynamicNavNode &rhs){return lhs.Id < rhs.Id;}
};

class DynamicNavMesh
{
private:
	std::map<int,DynamicNavNode> NevMesh;
	std::list<DynamicNavNode> ProcessQueue;
	std::set<int> ProcessIdSet;
	std::map<int,DynamicNavNode>::iterator iter;
	
	std::map<int,DynamicNavNode>::iterator GeneratePosIter;

	ray_hit hit;
	float tick_timer;
	float refresh_time;
	int MeshSize;
	int Density;
	bool HasMesh;

	void AddNewNodeToQueue(const DynamicNavNode &old_node, float density);
	void GenerateLink();
	void JudgeAcce(int curIndex, int index);

public:
	DynamicNavMesh()
	:	tick_timer(0)
	,	refresh_time(1)
	,	HasMesh(false){};
	~DynamicNavMesh(){};
	static DynamicNavMesh &Instance(){static DynamicNavMesh instance;return instance;}
	std::map<int,DynamicNavNode> &GetNevMesh(){return NevMesh;}
	int GetNearestNode(const Vec3 &pos);
	void CreateNavMesh(Vec3 Start_Pos, int density, int size, float fremtTime);
	void DebugMesh();
	void ClearMesh(){NevMesh.clear(); HasMesh = false;}
	Vec3 GenerateRandomPos();
	Vec3 GenerateRandomPosPreFrame();
};
#endif