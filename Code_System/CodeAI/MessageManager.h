#ifndef _MESSAGEMANAGER_H_
#define _MESSAGEMANAGER_H_

#include "CodeAIObject.h"

class ObjectMessageManager
{
public:
	~ObjectMessageManager(){}
	static ObjectMessageManager &Instance(){static ObjectMessageManager manager; return manager;}
	const bool SendObjectMessage(EntityId _sender, EntityId _receiver, int _msg, void* _extrainfo);
	const bool ReceiveMessage(CodeAIObject *pObject, const ObjectMessage &msg);
private:
	ObjectMessageManager(){};
};


#endif