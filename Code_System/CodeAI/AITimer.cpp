#include "StdAfx.h"
#include "AITimer.h"

void AITimerManager::AddTimer(EntityId eId, unsigned int tId, float time)
{
	if(!AITimers[eId].count(tId))
	{
		AITimer Timer(time);
		AITimers[eId][tId] = Timer;
	}
}

void AITimerManager::RemoveTimer(EntityId eId, unsigned int Id)
{
	if(!AITimers.count(eId))
		return;
	else
	{
		if(!AITimers[eId].count(Id))
			return;
		else
		{
			AITimers[eId].erase(Id);
			//Needed?
			if(AITimers[eId].empty())
				AITimers.erase(eId);
		}
	}
}
const bool AITimerManager::HasTimer(EntityId eId, TimerId tId)
{
	if(AITimers[eId].count(tId))
		return true;
	return false;
}

const bool AITimerManager::TimeOut(EntityId eId, TimerId tId)
{
	if(HasTimer(eId,tId))
	{
		AITimer &Timer = AITimers[eId][tId];
		if(Timer.tick_timer > Timer.LastTime)
			return true;
		return false;
	}
	return true;
}

void AITimerManager::ResetTimer(EntityId eId, TimerId tId, float time)
{
	if(HasTimer(eId,tId))
	{
		AITimer &Timer = AITimers[eId][tId];
		Timer.tick_timer = 0;
		Timer.LastTime = time;
	}
}

void AITimerManager::Update(float frameTime)
{
	std::map<EntityId, std::map<unsigned int, AITimer> >::iterator iter = AITimers.begin();
	std::map<EntityId, std::map<unsigned int, AITimer> >::iterator end = AITimers.end();
	while(iter != end)
	{
		std::map<unsigned int,AITimer>::iterator timer_iter = iter->second.begin();
		std::map<unsigned int,AITimer>::iterator timer_end = iter->second.end();
		while(timer_iter != timer_end)
		{
			if(timer_iter->second.tick_timer < timer_iter->second.LastTime)
				timer_iter->second.tick_timer += frameTime;
			++timer_iter;
		}
		++iter;
	}
}