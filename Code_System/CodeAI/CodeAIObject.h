#ifndef _CODEAIOBJECT_H_
#define _CODEAIOBJECT_H_

struct ObjectMessage
{
	EntityId Sender;
	EntityId Receiver;
	int Msg;
	void* ExtraInfo;
	ObjectMessage(EntityId _sender, EntityId _receiver, int _msg, void* _extra)
		:	Sender(_sender)
		,	Receiver(_receiver)
		,	Msg(_msg)
		,	ExtraInfo(_extra){}
};

class CodeAIObject
{
protected:
	EntityId Id;		//For convenience
public:
	CodeAIObject():Id(0){}
	virtual ~CodeAIObject(){}
	const EntityId GetId(){return Id;}

	virtual void Init(IEntity *pEntity) = 0;

	virtual void RegisterState() = 0;

	virtual void Update(float frameTime) = 0;

	virtual void UpdateObjectParams() = 0;

	virtual IEntity *GetEntity() = 0;

	virtual const bool HandleObjectMessage(const ObjectMessage &msg) = 0;


};

class CodeAIObjectManager
{
private:
	std::map<EntityId, CodeAIObject*> AI_Objects;
	CodeAIObjectManager(){}
public:
	~CodeAIObjectManager(){AI_Objects.clear();}
	static CodeAIObjectManager &Instance(){static CodeAIObjectManager manager; return manager;}

	CodeAIObject *GetCodeAIObject(EntityId Id)
	{
		if(AI_Objects.count(Id))
			return AI_Objects[Id];
		return NULL;
	}

	void InsertObject(CodeAIObject *pObject)
	{
		EntityId Id = pObject->GetId();
		if(!AI_Objects.count(Id))
			AI_Objects.insert(std::map<EntityId, CodeAIObject*>::value_type(Id,pObject));
	}

	void RemoveObject(EntityId Id)
	{
		if(AI_Objects.count(Id))
			AI_Objects.erase(Id);
	}
};

#endif