#include "StdAfx.h"
#include "MessageManager.h"

const bool ObjectMessageManager::SendObjectMessage(EntityId _sender, EntityId _receiver, int _msg, void* _extrainfo)
{
	CodeAIObjectManager &the_manager = CodeAIObjectManager::Instance();
	CodeAIObject *pObject = the_manager.GetCodeAIObject(_receiver);
	if(pObject)
	{
		ObjectMessage msg(_sender,_receiver,_msg,_extrainfo);
		ReceiveMessage(pObject,msg);
		return true;
	}
	return false;
}

const bool ObjectMessageManager::ReceiveMessage(CodeAIObject *pObject, const ObjectMessage &msg)
{
	return pObject->HandleObjectMessage(msg);
}