#include "StdAfx.h"
#include "DynamicNavMesh.h"

void DynamicNavMesh::CreateNavMesh(Vec3 Start_Pos, int density, int size, float fremtTime)
{
	tick_timer += fremtTime;
	if(tick_timer > refresh_time && !HasMesh)
	{
		MeshSize = size;
		Density = density;
		NevMesh.clear();
		Start_Pos.z += 0.2;
		float curX = Start_Pos.x - MeshSize;
		float curY = Start_Pos.y - MeshSize;
		float FinalX = curX + MeshSize*2;
		float finalY = curY + MeshSize*2;
		int Index = 0;
		while(curX < FinalX)
		{
			while(curY < finalY)
			{
				DynamicNavNode node;
				float terrain_z = static_cast<float>(gEnv->p3DEngine->GetTerrainElevation(curX,curY) + 0.3);
				node.pos = Vec3(curX,curY,terrain_z);
				node.Id = Index;
				node.parent = 0;
				NevMesh[Index] = node;
				curY += Density;
				++Index;
			}
			curY = Start_Pos.y - MeshSize;
			curX += Density;
		}
		tick_timer = 0;
		iter = NevMesh.begin();
		GeneratePosIter = NevMesh.begin();
		HasMesh = true;
	}
	if(HasMesh && iter != NevMesh.end())
		GenerateLink();

	/*NevMesh.clear();
	if(NevMesh.empty() && ProcessQueue.empty())
	{
		DynamicNavNode node;
		node.pos = Start_Pos;
		node.parent = parent;
		node.Id = Id;
		ProcessQueue.push_back(node);
	}
	while(!ProcessQueue.empty() && NevMesh.size() < size*size*2/density)
	{
		IEntity *pPlayer = gEnv->pEntitySystem->GetEntity(g_pGame->GetClientActorId());
		IPhysicalEntity *pPhysics = pPlayer->GetPhysics();
		int rayFlags(rwi_stop_at_pierceable|rwi_colltype_any);

		DynamicNavNode node = ProcessQueue.front();
		node.pos.z = gEnv->p3DEngine->GetTerrainElevation(node.pos.x,node.pos.y) + height;

		if(!NevMesh.empty())
		{
			if(abs(node.pos.x - NevMesh[5000].pos.x) > size/2 || abs(node.pos.y -NevMesh[5000].pos.y) > size/2 )
			{
				ProcessQueue.pop_front();
				continue;
			}
		}
		hits = gEnv->pPhysicalWorld->RayWorldIntersection(node.pos,Vec3(0,0,1)*height,ent_all,rayFlags,&hit,1,&pPhysics,1);
		if(!hits)
		{
			NevMesh[node.Id] = node;
			if(node.parent != 0)
			{
				Vec3 dir = (node.pos - NevMesh[node.parent].pos).normalize();
				hits = gEnv->pPhysicalWorld->RayWorldIntersection(node.pos,dir*density,ent_all,rayFlags,&hit,1,&pPhysics,1);
				if(!hits)
					NevMesh[node.parent].accessible.push_back(Id);
			}
			AddNewNodeToQueue(node,density);
		}
		else
		{
			node.pos.z += height;
			hits = gEnv->pPhysicalWorld->RayWorldIntersection(node.pos,Vec3(0,0,1)*height,ent_all,rayFlags,&hit,1,&pPhysics,1);
			if(!hits)
			{
				NevMesh[node.Id] = node;
				if(node.parent != 0)
				{
					Vec3 dir = (node.pos - NevMesh[node.parent].pos).normalize();
					hits = gEnv->pPhysicalWorld->RayWorldIntersection(node.pos,dir*density,ent_all,rayFlags,&hit,1,&pPhysics,1);
					if(!hits)
						NevMesh[node.parent].accessible.push_back(Id);
				}
				AddNewNodeToQueue(node,density);
			}
		}
		ProcessQueue.pop_front();
	}
	ProcessQueue.clear();
	ProcessIdSet.clear();*/
}

void DynamicNavMesh::DebugMesh()
{
	if(!NevMesh.empty())
	{
		std::map<int,DynamicNavNode>::iterator iter = NevMesh.begin();
		std::map<int,DynamicNavNode>::iterator end = NevMesh.end();
		while(iter != end)
		{
			gEnv->pRenderer->DrawLabel(iter->second.pos,1.5,"%d",iter->second.Id);
			DynamicNavNode &node = iter->second;
			std::list<int>::iterator Listiter = node.accessible.begin();
			std::list<int>::iterator Listend = node.accessible.end();
			while(Listiter != Listend)
			{
				DynamicNavNode &child_node = NevMesh[*Listiter];
				gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(node.pos, ColorB(255, 255, 255), child_node.pos, ColorB(255, 255, 255));
				++Listiter;
			}
			++iter;
		}
	}
}

void DynamicNavMesh::AddNewNodeToQueue(const DynamicNavNode &old_node, float density)
{

}

void DynamicNavMesh::GenerateLink()
{
	int offset = MeshSize*2/Density;
	if(iter != NevMesh.end())
	{
		int curIndex = iter->first;
		int up = curIndex - offset;
		int down = curIndex + offset;
		int left = curIndex - 1;
		int right = curIndex + 1;
		int left_up = curIndex - offset - 1;
		int left_down = curIndex + offset - 1;
		int right_up = curIndex - offset + 1;
		int right_down = curIndex + offset + 1;

		if(curIndex % offset == 0)
		{
			JudgeAcce(curIndex,up);
			JudgeAcce(curIndex,right_up);
			JudgeAcce(curIndex,right);
			JudgeAcce(curIndex,down);
			JudgeAcce(curIndex,right_down);
		}
		else if(curIndex % offset == offset-1)
		{
			JudgeAcce(curIndex,left_up);
			JudgeAcce(curIndex,up);
			JudgeAcce(curIndex,left);
			JudgeAcce(curIndex,left_down);
			JudgeAcce(curIndex,down);
		}
		else
		{
			JudgeAcce(curIndex,left_up);
			JudgeAcce(curIndex,up);
			JudgeAcce(curIndex,right_up);
			JudgeAcce(curIndex,left);
			JudgeAcce(curIndex,right);
			JudgeAcce(curIndex,left_down);
			JudgeAcce(curIndex,down);
			JudgeAcce(curIndex,right_down);
		}
		++iter;
	}
}

void DynamicNavMesh::JudgeAcce(int curIndex, int index)
{
	if(NevMesh.count(index))
	{
		DynamicNavNode &curNode = NevMesh[curIndex];
		DynamicNavNode &childNode = NevMesh[index];
		Vec3 dir = (childNode.pos - curNode.pos).normalize();

		IEntity *pPlayer = gEnv->pEntitySystem->GetEntity(g_pGame->GetClientActorId());
		IPhysicalEntity *pPhysics = pPlayer->GetPhysics();
		int rayFlags(rwi_stop_at_pierceable|rwi_colltype_any);
		f32 detect_length = static_cast<f32>(Density / 0.707);
		int hits = gEnv->pPhysicalWorld->RayWorldIntersection(curNode.pos,dir*detect_length,ent_rigid | ent_static | ent_sleeping_rigid,rayFlags,&hit,1,&pPhysics,1);
		if(hits && !curNode.hasAdjust)
		{
			/*if(hit.pCollider)
			{
				IEntity *pEntity=(IEntity*)hit.pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);
				if(pEntity)
				{
					AABB aabb;
					pEntity->GetWorldBounds(aabb);
					childNode.pos.z = static_cast<f32>(aabb.GetCenter().z + aabb.GetSize().z + 0.3);

					dir = (childNode.pos - curNode.pos).normalize();
					detect_length =(childNode.pos - curNode.pos).GetLengthFast();
					int hits = gEnv->pPhysicalWorld->RayWorldIntersection(curNode.pos,dir*detect_length,ent_rigid | ent_static | ent_sleeping_rigid,rayFlags,&hit,1,&pPhysics,1);

					if(!hits)
						NevMesh[curIndex].accessible.push_back(index);
				}
				else
				{
					//Add z_offset to check if reachable
					childNode.pos.z += 2; 

					dir = (childNode.pos - curNode.pos).normalize();
					detect_length =(childNode.pos - curNode.pos).GetLengthFast();
					int hits = gEnv->pPhysicalWorld->RayWorldIntersection(curNode.pos,dir*detect_length,ent_rigid | ent_static | ent_sleeping_rigid,rayFlags,&hit,1,&pPhysics,1);

					if(!hits)
						NevMesh[curIndex].accessible.push_back(index);
					else
						childNode.pos.z -= 2;	//reset z
				}
			}*/
			/*Vec3 diff = curNode.pos - hit.pt;
			float length = diff.GetLengthFast();
			if(length < Density / 2)
			{
				Vec3 dir = diff.normalize();
				curNode.pos += dir * (Density / 2);
				curNode.hasAdjust = true;
			}*/
		}
		int hits2 = gEnv->pPhysicalWorld->RayWorldIntersection(curNode.pos,Vec3(0,0,1)*0.1,ent_rigid | ent_static | ent_sleeping_rigid,rayFlags,&hit,1,&pPhysics,1);
		if(!hits && !hits2)
			NevMesh[curIndex].accessible.push_back(index);

	}
}

int DynamicNavMesh::GetNearestNode(const Vec3 &pos)
{
	const Vec3 &Start_Pos = NevMesh[0].pos;
	const float x_offset = (pos.x - Start_Pos.x) / Density;
	const float y_offset = (pos.y - Start_Pos.y) / Density;

	const int x_index_offset = (int)(x_offset + 0.5);
	if(x_index_offset > MeshSize*2)
		return -1;

	const int y_index_offset = (int)(y_offset + 0.5);
	if(y_index_offset > MeshSize*2)
		return -1;

	return (MeshSize * x_index_offset * 2 / Density + y_index_offset);
}

Vec3 DynamicNavMesh::GenerateRandomPos()
{
	int checkd_count = 0;
	do 
	{
		int index = cry_rand() % NevMesh.size();
		DynamicNavNode &node = NevMesh[index];
		if(!node.accessible.empty())
		{
			return node.pos;
		}
		++checkd_count;
	} while (checkd_count > 30);

	return Vec3(ZERO);
}

Vec3 DynamicNavMesh::GenerateRandomPosPreFrame()
{
	int index = cry_rand() % NevMesh.size();
	DynamicNavNode &node = NevMesh[index];

	if(!node.accessible.empty())
	{
		++GeneratePosIter;
		return node.pos;
	}

	++GeneratePosIter;
	return Vec3(ZERO);
}