#ifndef _AITIMER_H_
#define _AITIMER_H_

typedef unsigned int TimerId;
struct AITimer
{
	float tick_timer;
	float LastTime;


	AITimer()
	:	tick_timer(0)
	,	LastTime(0){}

	AITimer(float last_time)
	:	tick_timer(0)
	,	LastTime(last_time){}

	AITimer &operator=(const AITimer &rhs)
	{
		if(this == &rhs)
			return *this;
		tick_timer = rhs.tick_timer;
		LastTime = rhs.LastTime;
		return *this;
	}

};

class AITimerManager
{
private:
	std::map<EntityId, std::map<TimerId, AITimer> > AITimers;
	AITimerManager(){};
public:
	~AITimerManager(){};
	static AITimerManager &Instance(){static AITimerManager manager;return manager;}

	void AddTimer(EntityId eId, TimerId tId, float time);
	void RemoveTimer(EntityId eId, TimerId tId);
	const bool HasTimer(EntityId eId, TimerId tId);
	const bool TimeOut(EntityId eId, TimerId tId);
	void ResetTimer(EntityId eId, TimerId tId, float time);
	void Update(float frameTime);
};
#endif