#include "StdAfx.h"
#include "FlyingAI_Behavior.h"

void FlyingAI_Behavior::Floating(FlyingAI *pObject, float floatingHeight, float *_fParams)
{
	IEntity *pEntity = pObject->GetEntity();
	float curHeight = pEntity->GetPos().z;
	float terrainHeight = gEnv->p3DEngine->GetTerrainElevation(pEntity->GetPos().x,pEntity->GetPos().y);
	float offset = floatingHeight -(curHeight - terrainHeight);
	Vec3 dir_up(0,0,1);
	Vec3 FloatPosition = pEntity->GetPos();
	FloatPosition.z = terrainHeight + floatingHeight;
	Move(pObject,FloatPosition, 0.1, false);
	_fParams[0] = curHeight - terrainHeight;
}

const float FlyingAI_Behavior::GetDistanceToTarget(FlyingAI *pObject, IEntity *pTarget)
{
	if(pObject && pTarget)
	{
		IEntity *pEntity = pObject->GetEntity();
		AABB ObjectAABB;
		AABB TargetAABB;
		pEntity->GetWorldBounds(ObjectAABB);
		pTarget->GetWorldBounds(TargetAABB);
		Vec3 ObjectPosition = ObjectAABB.GetCenter();
		Vec3 TargetPosition = TargetAABB.GetCenter();
		return (ObjectPosition - TargetPosition).GetLengthFast();
	}
	return -1.0f;
}

const bool FlyingAI_Behavior::Move(FlyingAI *pObject, IEntity *pTarget, float fStopDistance)
{
	if(pObject && pTarget)
	{
		FlyingAIPamras *params = pObject->GetAIParams();

		IEntity *pEntity = pObject->GetEntity();
		AABB ObjectAABB;
		AABB TargetAABB;

		pEntity->GetWorldBounds(ObjectAABB);
		pTarget->GetWorldBounds(TargetAABB);
		Vec3 ObjectPosition = ObjectAABB.GetCenter();
		Vec3 TargetPosition = TargetAABB.GetCenter()/* + BiRandom(Vec3(ObjectAABB.GetRadius()))*/;
		TargetPosition.z = gEnv->p3DEngine->GetTerrainElevation(TargetPosition.x,TargetPosition.y) + params->floatingHeight;

		Vec3 dir = (TargetPosition - ObjectPosition).normalize();
		Vec3 Velocity = params->velocity;
		Vec3 final_dir = (dir - Velocity.GetNormalizedSafe()).normalize();

		float distance = (TargetPosition-ObjectPosition).GetLengthFast();

		if(distance < fStopDistance)
			return true;

		FlyingAI_Behavior::Rotate_vidDir(pObject,dir);

		float speed = sqrt(2*params->deceleration*(distance-fStopDistance));
		float dot_dir = dir.Dot(Velocity.GetNormalizedSafe());

		bool should_acceleration = speed > params->curSpeed && dot_dir < 1;
		if(should_acceleration)
		{
			float new_speed = params->curSpeed + (dir*params->acceleration).GetLengthFast();
			if(new_speed > speed)
			{
				Velocity += dir*new_speed;
			}
			else if(new_speed > params->maxSpeed)
			{
				new_speed = params->maxSpeed;
				Velocity += dir*params->maxSpeed;
			}
			else
				Velocity += dir*params->acceleration;
			params->UpdateVelocity(Velocity);
			return false;
		}
		else if(!should_acceleration)
		{
			float new_speed = params->curSpeed - (Velocity.GetNormalizedSafe()*params->deceleration).GetLengthFast();
			if(new_speed < speed)
			{
				new_speed = speed;
				Velocity.normalize() = dir;
				Velocity = Velocity.GetNormalizedSafe()*speed;
			}
			else if(new_speed < 0)
			{
				new_speed = 0;
				Velocity = Vec3(0,0,0);
			}
			else
			{
				Velocity = dir*speed;
				Velocity -= Velocity.GetNormalizedSafe()*params->deceleration;
			}
			params->UpdateVelocity(Velocity);
			return false;
		}
		return false;
	}
	return true;
}

const bool FlyingAI_Behavior::Move(FlyingAI *pObject, const Vec3 vDestination, float fStopDistance,bool bRotation)
{
	if(pObject)
	{
		FlyingAIPamras *params = pObject->GetAIParams();

		IEntity *pEntity = pObject->GetEntity();
		AABB ObjectAABB;

		pEntity->GetWorldBounds(ObjectAABB);
		Vec3 ObjectPosition = ObjectAABB.GetCenter();

		Vec3 dir = (vDestination - ObjectPosition).normalize();
		Vec3 Velocity = params->velocity;
		Vec3 final_dir = (dir - Velocity.GetNormalizedSafe()).normalize();

		float distance = (vDestination-ObjectPosition).GetLengthFast();

		if(distance < fStopDistance)
			return true;

		if(bRotation)
			FlyingAI_Behavior::Rotate_vidDir(pObject,dir);

		float speed = sqrt(2*params->deceleration*(distance-fStopDistance));
		float dot_dir = dir.Dot(Velocity.GetNormalizedSafe());
		bool should_acceleration = speed > params->curSpeed && dot_dir < 1;

		if(should_acceleration)
		{
			float new_speed = params->curSpeed + (final_dir*params->acceleration).GetLengthFast();
			if(new_speed > speed)
			{
				Velocity += dir*new_speed;
			}
			else if(new_speed > params->maxSpeed)
			{
				new_speed = params->maxSpeed;
				Velocity += dir*params->maxSpeed;
			}
			else
				Velocity += dir*params->acceleration;
			params->UpdateVelocity(Velocity);
			return false;
		}
		else if(!should_acceleration)
		{
			float new_speed = params->curSpeed - (Velocity.GetNormalizedSafe()*params->deceleration).GetLengthFast();
			if(new_speed < speed)
			{
				new_speed = speed;
				Velocity.normalize() = dir;
				Velocity = Velocity.GetNormalizedSafe()*speed;
			}
			else if(new_speed < 0)
			{
				new_speed = 0;
				Velocity = Vec3(0,0,0);
			}
			else
			{
				Velocity = dir*speed;
				Velocity -= Velocity.GetNormalizedSafe()*params->deceleration;
			}
			params->UpdateVelocity(Velocity);
			return false;
		}
		return false;
	}
	return true;
}

const bool FlyingAI_Behavior::Shift(FlyingAI *pObject, const Vec3 vDestination)
{
	if(pObject)
	{
		FlyingAIPamras *params = pObject->GetAIParams();

		IEntity *pEntity = pObject->GetEntity();
		AABB ObjectAABB;

		pEntity->GetWorldBounds(ObjectAABB);
		Vec3 ObjectPosition = ObjectAABB.GetCenter();

		Vec3 dir = (vDestination - ObjectPosition).normalize();
		Vec3 Velocity = params->velocity;

		if(!params->isShifting)
		{
			Velocity = dir*params->maxSpeed;
			params->UpdateVelocity(Velocity);
			params->isShifting = true;
		}
		else
		{
			Velocity -=Velocity.GetNormalizedSafe() * params->curSpeed/20;
			params->UpdateVelocity(Velocity);
		}
		if(Velocity.GetLengthFast() < 1)
		{
			params->isShifting = false;
			return true;
		}
		return false;
	}
	return true;
}

const bool FlyingAI_Behavior::Rotate_vidDir(FlyingAI *pObject, Vec3 vDirection)
{
	if(pObject)
	{
		FlyingAIPamras *params = pObject->GetAIParams();
		IEntity *pEntity = pObject->GetEntity();

		Vec3 CurDirection = pEntity->GetForwardDir();
		Vec3 ObjectPosition = pEntity->GetPos();

		Vec3 dir_diff = ((ObjectPosition + CurDirection + ((ObjectPosition + vDirection)  - (ObjectPosition + CurDirection)) * 0.07) - ObjectPosition).normalize();

		//gEnv->pRenderer->DrawLabel(ObjectPosition,1.5,"%.2f",angle);

		Quat rot = Quat::CreateRotationVDir(dir_diff);

		pEntity->SetRotation(rot);
		return false;
	}
	return true;
}

const bool FlyingAI_Behavior::FindTarget(FlyingAI *pObject, float radius)
{
	if(!pObject)
		return false;

	IEntity *pEntity = pObject->GetEntity();
	IEntity *pTarget = 0;
	const FlyingAIPamras *params = pObject->GetAIParams();
	int ray_flag = rwi_stop_at_pierceable|rwi_colltype_any;

	if(pEntity)
	{
		IPhysicalEntity **nearbyEntities;
		float minDistance = 999999;
		int num=gEnv->pPhysicalWorld->GetEntitiesInBox(pEntity->GetPos()-Vec3(radius), pEntity->GetPos()+Vec3(radius), nearbyEntities, ent_living);
		for(int i = 0;i < num;++i)
		{
			IEntity *pScanedEntity = gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
			if(pScanedEntity)
			{
				IActor *pActor = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pScanedEntity->GetId());
				if(pActor && !pActor->IsDead())
				{
					const Vec3 &fwddir = pEntity->GetForwardDir();
					Vec3 checked_dir = pScanedEntity->GetPos() - pEntity->GetPos();
					float cheched_z = abs(checked_dir.z);		//Check Z offset.May add to AI Params

					checked_dir.z = 0;
					checked_dir.normalize();

					f32 dot = fwddir.Dot(checked_dir);
					float angle = RAD2DEG(cry_acosf(dot));

					if(angle < params->viewAngle && cheched_z < 6)
					{
						float length = (pScanedEntity->GetPos() - pEntity->GetPos()).GetLengthSquared();
						if(length < minDistance)
						{
							AABB aabb;
							pScanedEntity->GetWorldBounds(aabb);
							ray_hit hit;

							gEnv->pPhysicalWorld->RayWorldIntersection(pEntity->GetPos(),checked_dir*length,ent_static,ray_flag,&hit,1,pEntity->GetPhysics());
							float hit_length = (pEntity->GetPos() - aabb.GetCenter()).GetLengthSquared();

							if(hit_length < length)
							{
								pTarget = pScanedEntity;
								pObject->SetTarget(pScanedEntity);
							}
						}
					}
				}
			}
		}
	}
	if(pTarget)
		return true;
	return false;
}