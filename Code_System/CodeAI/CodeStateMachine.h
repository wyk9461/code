#ifndef _CODESTATEMACHINE_H_
#define _CODESTATEMACHINE_H_

#include "Code_System/CommonUsage.h"
#include "MessageManager.h"

template<typename ObjectType>
class ObjectState
{
public:
	ObjectState(){}
	virtual ~ObjectState(){}
	virtual void Enter(ObjectType *pObject) = 0;
	virtual void Execute(ObjectType *pObject) = 0;
	virtual void Exit(ObjectType *pObject) = 0;
	virtual bool OnMessage(ObjectType *pObject, const ObjectMessage &msg) = 0;
};

template<typename ObjectType>
class ObjectStateMachine
{
	typedef int StateId;
public:
	ObjectStateMachine()
	{
		m_pCurState = NULL;
		m_pPrevState = NULL;
		m_pGlobalState = NULL;
	}
	~ObjectStateMachine()
	{

	}

	
	void RegisterObjectState(StateId type, ObjectState<ObjectType>* the_State)
	{
		if(!m_mStates.count(type))
			m_mStates.insert(std::map<StateId,ObjectState<ObjectType>*>::value_type(type,the_State));
	}

	void Init(ObjectType *pObject)
	{
		m_pOwner = pObject;
		m_pPrevState = NULL;
	}

	void InitState(StateId _curState, StateId _globalState)
	{
		if(_curState != -1 && m_mStates.count(_curState))
			m_pCurState = m_mStates[_curState];
		if(_globalState != -1 && m_mStates.count(_globalState))
			m_pGlobalState = m_mStates[_globalState];
	}

	void ChangeState(StateId type)
	{
		if(m_mStates.count(type))
		{
			if(m_pPrevState != m_pCurState)
				m_pPrevState = m_pCurState;
			m_pCurState->Exit(m_pOwner);
			m_pCurState = m_mStates[type];
			m_pCurState->Enter(m_pOwner);
		}
	}

	void RevertToPreviousState()
	{
		if(m_pPrevState)
		{
			ObjectState<ObjectType>* tempState = m_pPrevState;
			m_pPrevState = m_pCurState;
			m_pCurState->Exit(m_pOwner);
			m_pCurState = tempState;
			m_pCurState->Enter(m_pOwner);
		}
	}

	void Update(float frameTime)
	{
		if(m_pCurState)
			m_pCurState->Execute(m_pOwner);
		if(m_pGlobalState)
			m_pGlobalState->Execute(m_pOwner);
	}

	ObjectState<ObjectType>* CurState(){return m_pCurState;}
	ObjectState<ObjectType>* PrevState(){return m_pPrevState;}
	ObjectState<ObjectType>* GlobalState(){return m_pGlobalState;}
private:
	std::map<StateId, ObjectState<ObjectType>*> m_mStates;
	IdPool m_Idpool;
	ObjectType *m_pOwner;
	ObjectState<ObjectType>* m_pCurState;
	ObjectState<ObjectType>* m_pPrevState;
	ObjectState<ObjectType>* m_pGlobalState;
};
#endif