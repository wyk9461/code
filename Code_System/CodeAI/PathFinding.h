#ifndef _CODEPATHFINDING_H_
#define _CODEPATHFINDING_H_

#include "DynamicNavMesh.h"

struct AStar_Vec
{
	Vec3 pos;
	int index;
	float Gvalue;
	float Hvalue;
	float Fvalue;
	int Parent;
	std::list<int> *accessible;
	AStar_Vec()
		:	pos(Vec3(0,0,0))
		,	index(-1)
		,	Gvalue(0)
		,	Hvalue(0)
		,	Fvalue(0)
		,	Parent(0)
		,	accessible(NULL){}

	AStar_Vec(Vec3 _pos)
	:	pos(_pos)
	,	index(-1)
	,	Gvalue(0)
	,	Hvalue(0)
	,	Fvalue(0)
	,	Parent(0)
	,	accessible(NULL){}

	AStar_Vec &operator=(const AStar_Vec &rhs)
	{
		if(this == &rhs)
			return *this;
		pos = rhs.pos;
		index = rhs.index;
		Gvalue = rhs.Gvalue;
		Hvalue = rhs.Hvalue;
		Fvalue = rhs.Fvalue;
		Parent = rhs.Parent;
		accessible = rhs.accessible;
		return *this;
	}

	void CalculateValue(const Vec3 &vStart_Pos, const Vec3 &vDest);

};

enum dir_enum
{
	CENTER = 0,
	RIGHT = 1,
	LEFT = -1,
	FL = 2,
	BR = -2,
	FORWARD = 3,
	BACK = -3,
	FR = 4,
	BL = -4
};

class PathFinder
{
private:
	DynamicNavMesh *mesh;
public:
	PathFinder();
	~PathFinder(){SAFE_DELETE(mesh);}

	static PathFinder &DebugInstance(){static PathFinder instance;return instance;}
	void ImportNevMesh();
	void FindPath(const Vec3 &vStart_Pos, const Vec3 &vDestination);
	std::deque<int> *GetPath(){return &path;}
	bool hasPath(){return !path.empty();}

	/*void CreateMesh(Vec3 Start_Pos, int Id,float height, float density, float size,int parent)
	{
		mesh->CreateNavMesh(Start_Pos,Id,height,density,size,parent);
	}*/
	void DebugMesh(){mesh->DebugMesh();}
private:
	std::deque<int> path;
	std::deque<int> smooth_path;
	std::map<int,AStar_Vec> open_map;
	std::map<int,AStar_Vec> close_map;

	void CheckAndAddToOpenMap(AStar_Vec &Avec, std::map<int,DynamicNavNode> &mesh, const Vec3 &vStart_Pos, const Vec3 &vDestination);
	void CalculateFGH(AStar_Vec &Avec, const Vec3 &vStart_Pos, const Vec3 &vDestination);
	int findMinF();
	void ChangeParent(int index, int _DIR,const Vec3 &vStart_Pos);

	void PathSmooth();


	std::map<int,Vec3> Debug_open_map;
	bool needChangeIndex;

public:
	int testing_press_count;
	Vec3 testing_start_pos;
	Vec3 testing_dest;

	void DebugOpenMap();
	void DebugPath();
};
#endif