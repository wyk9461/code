#ifndef _FLYINGAI_BEHAVIOR_H
#define _FLYINGAI_BEHAVIOR_H

#include "FlyingAI.h"

namespace FlyingAI_Behavior
{
	// Description:
	//     Float the entity.
	// Arguments:
	//     pObject					  - Pointer to the floating entity
	//     floatingHeight			  - Floating hight
	//	   _fParam[0] = Current floating hight
	void Floating(FlyingAI *pObject, float floatingHeight, float *_fParams);

	// Description:
	//     Get the distance between AIObject and desired target.
	// Arguments:
	//     pEntity					  - Pointer to the AIObject
	//     pTarget					  - Pointer to the Target
	// Return Value:
	//     The Distance.
	//	   Return -1 if one of the pointer is null.
	const float GetDistanceToTarget(FlyingAI *pObject, IEntity *pTarget);

	// Description:
	//     Get next position while moving.With ||Constant Speed||
	// Arguments:
	//     pObject					- Pointer to the AIObject
	//     pTarget					- Pointer to the Target
	//     fAcce				    - Representing the acceleration amount 
	//							    - can't be negative value 
	//     fStopDistance            - Representing the distance that this entity will stop moving
	//								- The value Shouldn't be TOO SMALL
	// Return Value:
	//     A boolean value representing the success of moving
	const bool Move(FlyingAI *pObject, IEntity *pTarget, float fStopDistance);

	// Description:
	//     Get next position while moving.With ||Constant Speed||
	// Arguments:
	//     pObject					- Pointer to the AIObject
	//     vDestination				- Reference to position of the destination
	//     fAcce				    - Representing the acceleration amount 
	//							    - can't be negative value 
	//     fStopDistance            - Representing the distance that this entity will stop moving
	//								- The value Shouldn't be TOO SMALL
	// Return Value:
	//     A boolean value representing the success of moving
	const bool Move(FlyingAI *pObject, const Vec3 vDestination, float fStopDistance, bool bRotation);

	const bool Shift(FlyingAI *pObject, const Vec3 vDestination);

	const bool Rotate_vidDir(FlyingAI *pObject, Vec3 vDirection);

	const bool FindTarget(FlyingAI *pObject, float radius);
};
#endif