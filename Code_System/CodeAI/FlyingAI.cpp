#include "StdAfx.h"
#include "FlyingAI.h"
#include "FlyingAI_Behavior.h"


FlyingAI::FlyingAI()
{
	m_lockedMoving = false;
	m_isMovingAlongThePath = false;
	m_floatingHeight = 2;
	m_maxHeight = 10;
	finder = new PathFinder;
	m_pStateMachine = new ObjectStateMachine<FlyingAI>;
	RegisterState();
} 

void FlyingAI::RegisterState()
{
	m_pStateMachine->RegisterObjectState(STATE_IDLE,&FlyingAI_State_Idle::Instance());
	m_pStateMachine->RegisterObjectState(STATE_Moving,&FlyingAI_State_Moving::Instance());
	m_pStateMachine->RegisterObjectState(STATE_GLOBAL,&FlyingAI_State_Global::Instance());
	m_pStateMachine->RegisterObjectState(STATE_Shift,&FlyingAI_State_Shift::Instance());
	m_pStateMachine->RegisterObjectState(STATE_Wander,&FlyingAI_State_Wander::Instance());
}

FlyingAI::~FlyingAI()
{
	SAFE_DELETE(finder);
	SAFE_DELETE(m_pStateMachine);
}

void FlyingAI::Init(IEntity *pEntity)
{
	m_params.maxSpeed = 10;
	m_params.curSpeed = 0;
	m_params.velocity = Vec3(0,0,0);
	m_params.acceleration = 0.2;
	m_params.rotationRate = 4;
	m_params.deceleration = 1.5;
	m_params.isShifting = false;
	m_params.scanRadius = 10;
	m_params.viewAngle = 90;
	m_params.floatingHeight = 3;

	m_params.adaptionStatus.ObstacleAhead = false;
	m_params.adaptionStatus.WanderTarget = Vec3(ZERO);
	m_params.adaptionStatus.isWandering = false;

	Id = pEntity->GetId();
	m_pOwner = pEntity;
	m_pStateMachine->Init(this);
	m_pStateMachine->InitState(STATE_IDLE,-1);
	CodeAIObjectManager::Instance().InsertObject(this);
}

void FlyingAI::Update(float frameTime)
{
	if(m_pStateMachine)
		m_pStateMachine->Update(frameTime);
	UpdateObjectParams();
	ApplyVelocity();
}


const bool FlyingAI::HandleObjectMessage(const ObjectMessage &message)
{
	if(m_pStateMachine->CurState() && m_pStateMachine->CurState()->OnMessage(this,message))
	{
		return true;
	}
	// If current state can't handle this message
	// Send to global state instead
	if(m_pStateMachine->GlobalState() && m_pStateMachine->GlobalState()->OnMessage(this,message))
	{
		return true;
	}
	return false;
}

void FlyingAI::UpdateObjectParams()
{
	/*pe_status_dynamics params;
	m_pOwner->GetPhysics()->GetStatus(&params);
	m_curVelocity = params.v;*/
}

void FlyingAI::DebugDraw()
{
	IEntity *pEntity = GetEntity();
	Vec3 EntityPos = pEntity->GetPos();

	Vec3 fwd_dir = pEntity->GetForwardDir();

	Vec3 back_dir = -fwd_dir;
	Vec3 right_dir = fwd_dir.Cross(Vec3(0,0,1));
	Vec3 left_dir = -right_dir;
	Vec3 fr_dir = (fwd_dir+right_dir).normalize();
	Vec3 fl_dir = (fwd_dir+left_dir).normalize();
	Vec3 br_dir = (back_dir+right_dir).normalize();
	Vec3 bl_dir = (back_dir+left_dir).normalize();

	gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(EntityPos, ColorB(255, 100, 255), EntityPos + fwd_dir*1000, ColorB(255, 100, 255));
	gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(EntityPos, ColorB(255, 100, 255), EntityPos + back_dir*1000, ColorB(255, 100, 255));
	gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(EntityPos, ColorB(255, 100, 255), EntityPos + right_dir*1000, ColorB(255, 100, 255));
	gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(EntityPos, ColorB(255, 100, 255), EntityPos + left_dir*1000, ColorB(255, 100, 255));
	gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(EntityPos, ColorB(255, 100, 255), EntityPos + fr_dir*1000, ColorB(255, 100, 255));
	gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(EntityPos, ColorB(255, 100, 255), EntityPos + fl_dir*1000, ColorB(255, 100, 255));
	gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(EntityPos, ColorB(255, 100, 255), EntityPos + br_dir*1000, ColorB(255, 100, 255));
	gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(EntityPos, ColorB(255, 100, 255), EntityPos + bl_dir*1000, ColorB(255, 100, 255));

	gEnv->pRenderer->DrawLabel(EntityPos + fwd_dir,1.5,"F");
	gEnv->pRenderer->DrawLabel(EntityPos + back_dir,1.5,"B");
	gEnv->pRenderer->DrawLabel(EntityPos + right_dir,1.5,"R");
	gEnv->pRenderer->DrawLabel(EntityPos + left_dir,1.5,"L");
	gEnv->pRenderer->DrawLabel(EntityPos + fr_dir,1.5,"FR");
	gEnv->pRenderer->DrawLabel(EntityPos + fl_dir,1.5,"FL");
	gEnv->pRenderer->DrawLabel(EntityPos + br_dir,1.5,"BR");
	gEnv->pRenderer->DrawLabel(EntityPos + bl_dir,1.5,"BL");

	Vec3 DebugPos = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();

	Vec3 vDirection = (DebugPos - (pEntity->GetPos() + Vec3(0,0,1.8))).normalize();
	Vec3 vDirection_x = Vec3(0,vDirection.y,vDirection.z).normalize();
	Vec3 vDirection_y = Vec3(vDirection.x,0,vDirection.z).normalize();
	Vec3 vDirection_z = Vec3(vDirection.x,vDirection.y,0).normalize();

	Vec3 vDirection_left = -vDirection.Cross(Vec3(0,0,1));
	Vec3 vDirection_up = vDirection.Cross(vDirection_left);

	Vec3 CurDirection = pEntity->GetForwardDir();
	Vec3 CurDirection_x = Vec3(0,CurDirection.y,CurDirection.z).normalize();
	Vec3 CurDirection_y = Vec3(CurDirection.x,0,CurDirection.z).normalize();
	Vec3 CurDirection_z = Vec3(CurDirection.x,CurDirection.y,0).normalize();

	float dot_x = CurDirection_x.Dot(vDirection_x);
	float dot_y = CurDirection_y.Dot(vDirection_y);
	float dot_z = CurDirection_z.Dot(vDirection_z);

	float angle_x = RAD2DEG(cry_acosf(dot_x));
	float angle_y = RAD2DEG(cry_acosf(dot_y));
	float angle_z = RAD2DEG(cry_acosf(dot_z));

	//gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(EntityPos, ColorB(255, 255, 255), EntityPos + vDirection*1000, ColorB(255, 255, 255));

	/*gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(EntityPos, ColorB(255, 255, 255), EntityPos + vDirection_left*1000, ColorB(255, 255, 255));
	gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(EntityPos, ColorB(255, 255, 255), EntityPos + vDirection_up*1000, ColorB(255, 255, 255));*/

	/*gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(EntityPos, ColorB(0, 255, 255), EntityPos + vDirection_x*2, ColorB(255, 255, 255));
	gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(EntityPos, ColorB(255, 0, 255), EntityPos + vDirection_y*2, ColorB(255, 255, 255));
	gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(EntityPos, ColorB(255, 255, 0), EntityPos + vDirection_z*2, ColorB(255, 255, 255));*/

	//gEnv->pRenderer->DrawLabel(EntityPos + vDirection,1.5,"DIR");
	/*gEnv->pRenderer->DrawLabel(EntityPos + vDirection_x,1.5,"DX");
	gEnv->pRenderer->DrawLabel(EntityPos + vDirection_y,1.5,"DY");
	gEnv->pRenderer->DrawLabel(EntityPos + vDirection_z,1.5,"DZ");*/

	/*gEnv->pRenderer->DrawLabel(EntityPos + Vec3(0,0,1),1.5,"%.2f",angle_x);
	gEnv->pRenderer->DrawLabel(EntityPos + Vec3(0,0,2),1.5,"%.2f",angle_y);
	gEnv->pRenderer->DrawLabel(EntityPos + Vec3(0,0,3),1.5,"%.2f",angle_z);*/

	/*Quat rot=pEntity->GetRotation();
	Quat rot2=rot.CreateRotationX(DEG2RAD(0.5f));
	rot*=rot2;
	pEntity->SetRotation(rot);*/


}

void FlyingAI::ApplyVelocity()
{
	pe_action_set_velocity ve;
	ve.v = m_params.velocity;
	m_pOwner->GetPhysics()->Action(&ve);
}

void FlyingAI::ProcessCollision()
{
	if(!m_params.isShifting)
		GetStateMachine()->ChangeState(STATE_Shift);
}


//==================================================================================================
// Global State
//==================================================================================================

void FlyingAI_State_Global::Enter(FlyingAI *pObject)
{
	//CryLogAlways("Entering Idle State");
}

void FlyingAI_State_Global::Execute(FlyingAI *pObject)
{
	AABB aabb;
	IEntity *pEntity = pObject->GetEntity();
	FlyingAIPamras *params = pObject->GetAIParams();
	IPhysicalEntity *pPhysicsEnt = pEntity->GetPhysics();
	ray_hit &hit = params->hit;
	pEntity->GetWorldBounds(aabb);

	Vec3 fwd_dir = pEntity->GetForwardDir();
	Vec3 back_dir = -fwd_dir;
	Vec3 right_dir = fwd_dir.Cross(Vec3(0,0,1));
	Vec3 left_dir = -right_dir;
	Matrix34 mat = pEntity->GetLocalTM();
	Vec3 UpDir = Vec3(-mat.m20,-mat.m21,mat.m22);

	Vec3 Fwd_pos = aabb.GetCenter()+aabb.GetRadius() * fwd_dir;
	Vec3 Back_pos = aabb.GetCenter()+aabb.GetRadius() * back_dir;
	Vec3 Right_pos = aabb.GetCenter()+aabb.GetRadius() * right_dir;
	Vec3 Left_pos = aabb.GetCenter()+aabb.GetRadius() * left_dir;
	Vec3 Up_pos = aabb.GetCenter()+aabb.GetRadius() * UpDir;
	Vec3 Down_pos = aabb.GetCenter()+aabb.GetRadius() * (-UpDir);

	int ray_flag = rwi_stop_at_pierceable|rwi_colltype_any;
	//Terrain detection
	/*Vec3 BasePos;
	int hits = gEnv->pPhysicalWorld->RayWorldIntersection(pEntity->GetPos(),Vec3(0,0,-1)*100000,ent_terrain,ray_flag,&hit,1,&pPhysicsEnt);
	BasePos = hit.pt;
	float length = (BasePos - aabb.GetCenter()).GetLengthFast();
	if(length < params->floatingHeight / 2)
	{
		Vec3 Velocity = params->velocity;
		Velocity += Vec3(0,0,1) * length;
		//Velocity -= Velocity.GetNormalizedSafe() * length / 2;
		params->UpdateVelocity(Velocity);
	}*/

	//Fwd detection
	int hits_fwd = gEnv->pPhysicalWorld->RayWorldIntersection(Fwd_pos,fwd_dir*3,ent_all,ray_flag,&hit,1,&pPhysicsEnt);
	if(!hits_fwd)
		params->adaptionStatus.fwd_free = true;
	else
		params->adaptionStatus.fwd_free = false;

	//Back detection
	int hits_back = gEnv->pPhysicalWorld->RayWorldIntersection(Back_pos,back_dir*3,ent_all,ray_flag,&hit,1,&pPhysicsEnt);
	if(!hits_back)
		params->adaptionStatus.back_free = true;
	else
		params->adaptionStatus.back_free = false;

	//Right detection
	int hits_right = gEnv->pPhysicalWorld->RayWorldIntersection(Right_pos,right_dir*3,ent_all,ray_flag,&hit,1,&pPhysicsEnt);
	if(!hits_right)
		params->adaptionStatus.right_free = true;
	else
		params->adaptionStatus.right_free = false;

	//Left detection
	int hits_left = gEnv->pPhysicalWorld->RayWorldIntersection(Left_pos,left_dir*3,ent_all,ray_flag,&hit,1,&pPhysicsEnt);
	if(!hits_left)
		params->adaptionStatus.left_free = true;
	else
		params->adaptionStatus.left_free = false;


	//Avoid Wall
	int hits_fwd_wall = gEnv->pPhysicalWorld->RayWorldIntersection(Fwd_pos,fwd_dir*2,ent_static,ray_flag,&hit,1,&pPhysicsEnt);
	if(hits_fwd_wall)
	{
		params->adaptionStatus.isWandering = false;
		params->adaptionStatus.ObstacleAhead = true;
		Vec3 Velocity = params->velocity;
		Vec3 dir = hit.n;
		float force = params->curSpeed - (hit.pt - Fwd_pos).GetLengthFast(); 
		Velocity += dir * force;
		Velocity -= Velocity.GetNormalizedSafe() * force / 2;
		params->UpdateVelocity(Velocity);
	}
	else
		params->adaptionStatus.ObstacleAhead = false;
	//Avoid Rigid Entity
	ray_hit hit_fwd;
	ray_hit hit_right;
	ray_hit hit_left;
	ray_hit hit_up;
	ray_hit hit_down;
	Vec3 velocity_dir = params->velocity.normalize();
	int hits_fwd_rigid = gEnv->pPhysicalWorld->RayWorldIntersection(Fwd_pos,velocity_dir*params->curSpeed,ent_rigid | ent_sleeping_rigid,ray_flag,&hit_fwd,1,&pPhysicsEnt);
	int hits_left_rigid = gEnv->pPhysicalWorld->RayWorldIntersection(Left_pos,velocity_dir*params->curSpeed,ent_rigid | ent_sleeping_rigid,ray_flag,&hit_right,1,&pPhysicsEnt);
	int hits_right_rigid = gEnv->pPhysicalWorld->RayWorldIntersection(Right_pos,velocity_dir*params->curSpeed,ent_rigid | ent_sleeping_rigid,ray_flag,&hit_left,1,&pPhysicsEnt);
	int hits_up_rigid = gEnv->pPhysicalWorld->RayWorldIntersection(Up_pos,velocity_dir*params->curSpeed,ent_rigid | ent_sleeping_rigid,ray_flag,&hit_up,1,&pPhysicsEnt);
	int hits_down_rigid = gEnv->pPhysicalWorld->RayWorldIntersection(Down_pos,velocity_dir*params->curSpeed,ent_rigid | ent_sleeping_rigid,ray_flag,&hit_down,1,&pPhysicsEnt);

	gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(Fwd_pos, ColorB(255, 255, 255), Fwd_pos + velocity_dir*params->curSpeed, ColorB(255, 255, 255));
	gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(Left_pos, ColorB(255, 255, 255), Left_pos + velocity_dir*params->curSpeed, ColorB(255, 255, 255));
	gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(Right_pos, ColorB(255, 255, 255), Right_pos + velocity_dir*params->curSpeed, ColorB(255, 255, 255));
	gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(Up_pos, ColorB(255, 255, 255), Up_pos + velocity_dir*params->curSpeed, ColorB(255, 255, 255));
	gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(Down_pos, ColorB(255, 255, 255), Down_pos + velocity_dir*params->curSpeed, ColorB(255, 255, 255));
	if(hits_fwd_rigid || hits_left_rigid || hits_right_rigid || hits_up_rigid || hits_down_rigid)
	{
		if(hits_down_rigid)
			hit = hit_down;
		params->adaptionStatus.isWandering = false;
		params->adaptionStatus.ObstacleAhead = true;
		if(hit.pCollider)
		{
			IEntity *pEntity=(IEntity*)hit.pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);
			if(pEntity)
			{
				AABB Obstacleaabb;
				pEntity->GetWorldBounds(Obstacleaabb);
				float radius = Obstacleaabb.GetRadius();
				Vec3 ObstacleCenter = Obstacleaabb.GetCenter();
				Vec3 hit_normal = hit.n;
				Vec3 normal_right = hit_normal.Cross(Vec3(0,0,1));
				Vec3 normal_left = -normal_right;

				Vec3 Right_Available_Pos = Vec3(ZERO);
				Vec3 Left_Available_Pos = Vec3(ZERO);

				/*gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(hit.pt, ColorB(255, 255, 255), hit.pt + normal_right*radius*100, ColorB(255, 255, 255));
				gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(hit.pt, ColorB(255, 255, 255), hit.pt + normal_left*radius*100, ColorB(255, 255, 255));*/

				int hits_right = gEnv->pPhysicalWorld->RayWorldIntersection(hit.pt + normal_right * radius,Vec3(0,0,1)/2,ent_all,ray_flag,&hit,1,&pPhysicsEnt);
				if(!hits_right)
					Right_Available_Pos = hit.pt + normal_right * (radius + aabb.GetRadius()*2);

				int hits_left = gEnv->pPhysicalWorld->RayWorldIntersection(hit.pt + normal_left * radius,Vec3(0,0,1)/2,ent_all,ray_flag,&hit,1,&pPhysicsEnt);
				if(!hits_left)
					Left_Available_Pos = hit.pt + normal_left * (radius + aabb.GetRadius()*2);

				if(Right_Available_Pos != Vec3(ZERO))
				{
					Vec3 Velocity = params->velocity;
					Vec3 dir = ((Right_Available_Pos - pEntity->GetPos()).normalize() - Velocity.GetNormalizedFast()).normalize();
					Vec3 dir_2 = (hit.pt - pEntity->GetPos()).normalize();

					//gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(hit.pt, ColorB(255, 255, 255), hit.pt + dir_2*100, ColorB(255, 255, 255));

					//FlyingAI_Behavior::Move(pObject,Right_Available_Pos,aabb.GetRadius(),false);
					float force = params->curSpeed / 2; 
					Velocity = dir * params->curSpeed;
					Velocity -= Velocity.GetNormalizedSafe() * force / 2;
					params->UpdateVelocity(Velocity);
				}
			}
		}
	}
}

void FlyingAI_State_Global::Exit(FlyingAI *pObject)
{
	//CryLogAlways("Exit Idle State");
}

bool FlyingAI_State_Global::OnMessage(FlyingAI *pObject, const ObjectMessage &msg)
{
	int type = msg.Msg;
	switch(type)
	{
	case(High_enough):
		{
			pObject->GetStateMachine()->ChangeState(STATE_Moving);
			break;
		}
	}
	//CryLogAlways("Receive Message");
	return true;
}


//==================================================================================================
// Idle State
//==================================================================================================

void FlyingAI_State_Idle::Enter(FlyingAI *pObject)
{
	//CryLogAlways("Entering Idle State");
}

void FlyingAI_State_Idle::Execute(FlyingAI *pObject)
{
	FlyingAI_Behavior::Floating(pObject, pObject->GetAIParams()->floatingHeight, fParams);
	if(fParams[0] > 2.8)
		ObjectMessageManager::Instance().SendObjectMessage(pObject->GetId(),pObject->GetId(),High_enough,0);
}

void FlyingAI_State_Idle::Exit(FlyingAI *pObject)
{
	//CryLogAlways("Exit Idle State");
}

bool FlyingAI_State_Idle::OnMessage(FlyingAI *pObject, const ObjectMessage &msg)
{
	int type = msg.Msg;
	switch(type)
	{
		case(High_enough):
		{
			AITimerManager &manager = AITimerManager::Instance();
			if(!manager.HasTimer(pObject->GetId(), Wander_Waiting))
				manager.AddTimer(pObject->GetId(),Wander_Waiting,1);
			else
			{
				if(manager.TimeOut(pObject->GetId(),Wander_Waiting))
				{
					pObject->GetStateMachine()->ChangeState(STATE_Wander);
					manager.RemoveTimer(pObject->GetId(),Wander_Waiting);
					break;
				}
			}
		}
	}
	//CryLogAlways("Receive Message");
	return true;
}

//==================================================================================================
// Moving State
//==================================================================================================
void FlyingAI_State_Moving::Enter(FlyingAI *pObject)
{
	//CryLogAlways("Entering Moving State");
}

void FlyingAI_State_Moving::Execute(FlyingAI *pObject)
{
	if(!pObject->GetAIParams()->adaptionStatus.ObstacleAhead)
	{
		if(FlyingAI_Behavior::Move(pObject, pObject->GetTarget(),4))
		{
			pObject->GetStateMachine()->ChangeState(STATE_IDLE);
		}
	}
}

void FlyingAI_State_Moving::Exit(FlyingAI *pObject)
{
	//CryLogAlways("Exit Moving State");
}

bool FlyingAI_State_Moving::OnMessage(FlyingAI *pObject, const ObjectMessage &msg)
{
	int type = msg.Msg;
	switch(type)
	{
	case(Arrive):
		{
			pObject->GetStateMachine()->ChangeState(STATE_Moving);
			break;
		}
	}
	//CryLogAlways("Receive Message");
	return true;
}

//==================================================================================================
// Shifting State
//==================================================================================================
void FlyingAI_State_Shift::Enter(FlyingAI *pObject)
{
	IEntity *pEntity = pObject->GetEntity();
	Vec3 EntityPos = pEntity->GetPos();
	FlyingAIPamras *params = pObject->GetAIParams();

	Vec3 fwd_dir = pEntity->GetForwardDir();
	Vec3 back_dir = -fwd_dir;
	Vec3 right_dir = fwd_dir.Cross(Vec3(0,0,1));
	Vec3 left_dir = -right_dir;

	std::deque<Vec3> randomShiftList;
	if(params->adaptionStatus.fwd_free)
		randomShiftList.push_back(EntityPos+fwd_dir);
	if(params->adaptionStatus.back_free)
		randomShiftList.push_back(EntityPos+back_dir);
	if(params->adaptionStatus.right_free)
		randomShiftList.push_back(EntityPos+right_dir);
	if(params->adaptionStatus.left_free)
		randomShiftList.push_back(EntityPos+left_dir);
	if(!randomShiftList.empty())
	{
		int random_index= cry_rand32() % randomShiftList.size();
		FlyingAI_Behavior::Shift(pObject,randomShiftList[random_index]);
	}
	else
		pObject->GetStateMachine()->ChangeState(STATE_IDLE);
}

void FlyingAI_State_Shift::Execute(FlyingAI *pObject)
{
	IEntity *pEntity = pObject->GetEntity();
	
	Vec3 EntityPos = pEntity->GetPos();

	Vec3 fwd_dir = pEntity->GetForwardDir();
	Vec3 back_dir = -fwd_dir;
	Vec3 right_dir = fwd_dir.Cross(Vec3(0,0,1));
	Vec3 left_dir = -right_dir;


	if(FlyingAI_Behavior::Shift(pObject,EntityPos+left_dir))
		pObject->GetStateMachine()->RevertToPreviousState();
}

void FlyingAI_State_Shift::Exit(FlyingAI *pObject)
{
	//CryLogAlways("Exit Moving State");
}

bool FlyingAI_State_Shift::OnMessage(FlyingAI *pObject, const ObjectMessage &msg)
{
	int type = msg.Msg;
	switch(type)
	{
	case(Arrive):
		{
			pObject->GetStateMachine()->ChangeState(STATE_Moving);
			break;
		}
	}
	//CryLogAlways("Receive Message");
	return true;
}


//==================================================================================================
// Shifting State
//==================================================================================================
void FlyingAI_State_Wander::Enter(FlyingAI *pObject)
{

}

void FlyingAI_State_Wander::Execute(FlyingAI *pObject)
{
	FlyingAIPamras *params = pObject->GetAIParams();
	if(!params->adaptionStatus.isWandering)
	{
		float radius = 2;
		float distance = 3;
		float jitter = 1.4;
		IEntity *pEntity = pObject->GetEntity();

		Vec3 EntityPos = pEntity->GetPos();
		Vec3 fwdDir = pEntity->GetForwardDir();

		Vec3 Targetdir = fwdDir;
		Targetdir.x = BiRandom(jitter);
		Targetdir.y = BiRandom(jitter);
		Targetdir.z = 0;

		Targetdir.normalize();
		Targetdir *= radius;

		params->adaptionStatus.WanderTarget = EntityPos + (Targetdir * distance);

		int ray_flag = rwi_stop_at_pierceable|rwi_colltype_any;
		ray_hit hit;
		int hits = gEnv->pPhysicalWorld->RayWorldIntersection(pEntity->GetPos(),Vec3(0,0,1),ent_all,ray_flag,&hit,1,pEntity->GetPhysics());
		if(!hits)
			params->adaptionStatus.isWandering = true;

		if(params->adaptionStatus.ObstacleAhead)
			params->adaptionStatus.WanderTarget = EntityPos - fwdDir * jitter;
	}
	if(params->adaptionStatus.isWandering)
	{
		if(FlyingAI_Behavior::Move(pObject,params->adaptionStatus.WanderTarget,1,true))
			params->adaptionStatus.isWandering = false;
		if(FlyingAI_Behavior::FindTarget(pObject,params->scanRadius))
			pObject->GetStateMachine()->ChangeState(STATE_Moving);
	}
}

void FlyingAI_State_Wander::Exit(FlyingAI *pObject)
{
	FlyingAIPamras *params = pObject->GetAIParams();
	params->adaptionStatus.isWandering = false;
	params->adaptionStatus.WanderTarget = Vec3(0,0,0);
}

bool FlyingAI_State_Wander::OnMessage(FlyingAI *pObject, const ObjectMessage &msg)
{
	int type = msg.Msg;
	switch(type)
	{
	case(Arrive):
		{
			pObject->GetStateMachine()->ChangeState(STATE_Moving);
			break;
		}
	}
	//CryLogAlways("Receive Message");
	return true;
}

