#ifndef _FLYINGAI_H_
#define _FLYINGAI_H_

#include "PathFinding.h"
#include "AITimer.h"
#include "CodeStateMachine.h"

enum FlyingAI_State
{
	STATE_GLOBAL,
	STATE_IDLE,
	STATE_Moving,
	STATE_Shift,
	STATE_Wander,
};

enum FlyingAI_Message
{
	High_enough = 0,
	Arrive,
};

enum Timers
{
	Wander_Waiting = 0,
	ConfirmTarget,
};


struct FlyingAIPamras
{
private:
	struct Adaption_status
	{
		bool fwd_free;
		bool back_free;
		bool left_free;
		bool right_free;

		bool ObstacleAhead;

		Vec3 WanderTarget;
		bool isWandering;
	};

public:
	float maxSpeed;
	float curSpeed;
	float rotationRate;
	float acceleration;
	float deceleration;
	float floatingHeight;

	float scanRadius;
	float viewAngle;

	bool isShifting;
	Adaption_status adaptionStatus;

	Vec3 velocity;
	ray_hit hit;

	void UpdateVelocity(const Vec3 newVelocity)
	{
		velocity = newVelocity;
		curSpeed = newVelocity.GetLengthFast();
	}
};

class FlyingAI : public CodeAIObject
{
public:
	FlyingAI();
	~FlyingAI();

public:
	//CodeAIObject Interface

	virtual void Init(IEntity *pEntity);
	virtual void RegisterState();
	virtual const bool HandleObjectMessage(const ObjectMessage &msg);

	virtual void Update(float frameTime);
	virtual void UpdateObjectParams();

	virtual IEntity *GetEntity(){return m_pOwner;}
	virtual FlyingAIPamras *GetAIParams(){return &m_params;}
	//~CodeAIObject Interface

	void ProcessCollision();

	void DebugDraw();

	ObjectStateMachine<FlyingAI> *GetStateMachine(){return m_pStateMachine;}

	void SetTarget(IEntity *pEntity){pTarget = pEntity;}

	IEntity *GetTarget()
	{
		if(pTarget) 
			return pTarget;
		return 0;
	}

protected:
	FlyingAIPamras m_params;
	IEntity *pTarget;
	virtual void ApplyVelocity();
private:
	Vec3 m_locked_Destination;
	bool m_lockedMoving;
	bool m_isMovingAlongThePath;

	IEntity *m_pOwner;
	float m_floatingHeight;
	float m_maxHeight;
	PathFinder *finder;
	ObjectStateMachine<FlyingAI> *m_pStateMachine;
};


//==================================================================================================
// Global State
//==================================================================================================
class FlyingAI_State_Global : public ObjectState<FlyingAI>
{
private:
	FlyingAI_State_Global(){}
	float fParams[3];
	int nParams[3];
public:
	static FlyingAI_State_Global &Instance(){static FlyingAI_State_Global state; return state;}
	~FlyingAI_State_Global(){}
	virtual void Enter(FlyingAI *pObject);
	virtual void Execute(FlyingAI *pObject);
	virtual void Exit(FlyingAI *pObject);
	virtual bool OnMessage(FlyingAI *pObject, const ObjectMessage &msg);
};


//==================================================================================================
// Idle State
//==================================================================================================
class FlyingAI_State_Idle : public ObjectState<FlyingAI>
{
private:
	FlyingAI_State_Idle(){}
	float fParams[3];
	int nParams[3];
public:
	static FlyingAI_State_Idle &Instance(){static FlyingAI_State_Idle state; return state;}
	~FlyingAI_State_Idle(){}
	virtual void Enter(FlyingAI *pObject);
	virtual void Execute(FlyingAI *pObject);
	virtual void Exit(FlyingAI *pObject);
	virtual bool OnMessage(FlyingAI *pObject, const ObjectMessage &msg);
};


//==================================================================================================
// Moving State
//==================================================================================================
class FlyingAI_State_Moving : public ObjectState<FlyingAI>
{
private:
	FlyingAI_State_Moving(){}
	float fParams[3];
	int nParams[3];
public:
	static FlyingAI_State_Moving &Instance(){static FlyingAI_State_Moving state; return state;}
	~FlyingAI_State_Moving(){}
	virtual void Enter(FlyingAI *pObject);
	virtual void Execute(FlyingAI *pObject);
	virtual void Exit(FlyingAI *pObject);
	virtual bool OnMessage(FlyingAI *pObject, const ObjectMessage &msg);
};

//==================================================================================================
// Shifting State
//==================================================================================================
class FlyingAI_State_Shift : public ObjectState<FlyingAI>
{
private:
	FlyingAI_State_Shift(){}
	float fParams[3];
	int nParams[3];
public:
	static FlyingAI_State_Shift &Instance(){static FlyingAI_State_Shift state; return state;}
	~FlyingAI_State_Shift(){}
	virtual void Enter(FlyingAI *pObject);
	virtual void Execute(FlyingAI *pObject);
	virtual void Exit(FlyingAI *pObject);
	virtual bool OnMessage(FlyingAI *pObject, const ObjectMessage &msg);
};

//==================================================================================================
// Wandering State
//==================================================================================================
class FlyingAI_State_Wander : public ObjectState<FlyingAI>
{
private:
	FlyingAI_State_Wander(){}
	float fParams[3];
	int nParams[3];
public:
	static FlyingAI_State_Wander &Instance(){static FlyingAI_State_Wander state; return state;}
	~FlyingAI_State_Wander(){}
	virtual void Enter(FlyingAI *pObject);
	virtual void Execute(FlyingAI *pObject);
	virtual void Exit(FlyingAI *pObject);
	virtual bool OnMessage(FlyingAI *pObject, const ObjectMessage &msg);
};
#endif