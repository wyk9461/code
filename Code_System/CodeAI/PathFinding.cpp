#include "StdAfx.h"
#include "PathFinding.h"

void AStar_Vec::CalculateValue(const Vec3 &vStart_Pos, const Vec3 &vDest)
{
	Gvalue = (pos - vStart_Pos).GetLengthFast();
	Hvalue = (vDest - pos).GetLengthFast();
	Fvalue = Gvalue + Hvalue;
}

PathFinder::PathFinder()
{
	needChangeIndex = true;
	mesh = new DynamicNavMesh;
}

void PathFinder::FindPath(const Vec3 &vStart_Pos, const Vec3 &vDestination)
{
	DynamicNavMesh &MeshManager = DynamicNavMesh::Instance();
	std::map<int,DynamicNavNode> mesh = MeshManager.GetNevMesh();
	int Start_Index = MeshManager.GetNearestNode(vStart_Pos);
	int Dest_Index = MeshManager.GetNearestNode(vDestination);
	if(Start_Index == -1 || Dest_Index == -1)
	{
		CryLogAlways("Not in NevMesh");
		return;
	}
	open_map.clear();
	close_map.clear();
	path.clear();
	smooth_path.clear();

	CryLogAlways("Start_Index: %d", Start_Index);
	CryLogAlways("Dest_Index: %d", Dest_Index);
	AStar_Vec Avec;
	Avec.pos = mesh[Start_Index].pos;
	Avec.index = Start_Index;
	Avec.accessible = &mesh[Start_Index].accessible;
	close_map[Start_Index] = Avec;

	CheckAndAddToOpenMap(Avec,mesh,vStart_Pos,vDestination);
	while(!close_map.count(Dest_Index))
	{
		int new_index = findMinF();
		AStar_Vec new_Avec;
		new_Avec.pos = mesh[new_index].pos;
		new_Avec.index = new_index;
		new_Avec.accessible = &mesh[new_index].accessible;
		CalculateFGH(new_Avec,vStart_Pos,vDestination);
		CheckAndAddToOpenMap(new_Avec,mesh,vStart_Pos,vDestination);
		//CryLogAlways("Index: %d",new_Avec.index);
	}
	
	int path_index = Dest_Index;
	while(path_index != 0)
	{
		path.push_front(path_index);
		path_index = close_map[path_index].Parent;
	}
	PathSmooth();
/*
	IPhysicalEntity *PhysicsEntity = pEntity->GetPhysics();
	//CreateMesh(vStart_Pos,5000,2,0.5,10,0);
	if(pEntity)
	{
		open_map.clear();
		close_map.clear();
		Debug_open_map.clear();
		path.clear();
		AABB aabb;
		pEntity->GetLocalBounds(aabb);
		float volume_offset = aabb.GetRadius() *2;
		IPhysicalEntity *PhysicsEntity = pEntity->GetPhysics();
		AStar_Vec Avec(pEntity->GetPos());
		Avec.Fvalue = 0;
		close_map[5000] = Avec;
		int curindex = 5000;

		float length = (vDestination - Avec.pos).GetLengthFast(); 
		float last_length = length;

		float grid_length(length / 2);
		Vec3 fwd_dir = (vDestination - close_map[curindex].pos).normalize();
		fwd_dir.z = 0;
		fwd_dir.normalize();
		Vec3 back_dir = -fwd_dir;
		Vec3 right_dir = fwd_dir.Cross(Vec3(0,0,1));
		Vec3 left_dir = -right_dir;
		Vec3 fr_dir = (fwd_dir+right_dir).normalize();
		Vec3 fl_dir = (fwd_dir+left_dir).normalize();
		Vec3 br_dir = (back_dir+right_dir).normalize();
		Vec3 bl_dir = (back_dir+left_dir).normalize();

		CheckAndAddToOpenMap(curindex,fwd_dir,vDestination,volume_offset,grid_length,PhysicsEntity,FORWARD);
		CheckAndAddToOpenMap(curindex,back_dir,vDestination,volume_offset,grid_length,PhysicsEntity,BACK);
		CheckAndAddToOpenMap(curindex,right_dir,vDestination,volume_offset,grid_length,PhysicsEntity,RIGHT);
		CheckAndAddToOpenMap(curindex,left_dir,vDestination,volume_offset,grid_length,PhysicsEntity,LEFT);
		CheckAndAddToOpenMap(curindex,fr_dir,vDestination,volume_offset,grid_length,PhysicsEntity,FR);
		CheckAndAddToOpenMap(curindex,fl_dir,vDestination,volume_offset,grid_length,PhysicsEntity,FL);
		CheckAndAddToOpenMap(curindex,br_dir,vDestination,volume_offset,grid_length,PhysicsEntity,BR);
		CheckAndAddToOpenMap(curindex,bl_dir,vDestination,volume_offset,grid_length,PhysicsEntity,BL);

		int parent_index;
		while(grid_length > 1.5)
		{
			while(length > grid_length)
			{
				if(needChangeIndex)
				{
					parent_index = curindex;
					curindex = findMinF();
				}

				length = (vDestination - close_map[curindex].pos).GetLengthFast(); 
				if(length / grid_length < 1.3)
				{
					grid_length = length / 2;
					needChangeIndex = false;
					break;
				}
				if(close_map.size() > 200)
				{
					//path.push_back(vDestination);
					break;
				}
				Vec3 fwd_dir = (close_map[parent_index].pos - close_map[curindex].pos).normalize();
				fwd_dir.z = 0;
				fwd_dir.normalize();
				Vec3 back_dir = -fwd_dir;
				Vec3 right_dir = fwd_dir.Cross(Vec3(0,0,1));
				Vec3 left_dir = -right_dir;
				Vec3 fr_dir = (fwd_dir+right_dir).normalize();
				Vec3 fl_dir = (fwd_dir+left_dir).normalize();
				Vec3 br_dir = (back_dir+right_dir).normalize();
				Vec3 bl_dir = (back_dir+left_dir).normalize();

				CheckAndAddToOpenMap(curindex,fwd_dir,vDestination,volume_offset,grid_length,PhysicsEntity,FORWARD);
				CheckAndAddToOpenMap(curindex,back_dir,vDestination,volume_offset,grid_length,PhysicsEntity,BACK);
				CheckAndAddToOpenMap(curindex,right_dir,vDestination,volume_offset,grid_length,PhysicsEntity,RIGHT);
				CheckAndAddToOpenMap(curindex,left_dir,vDestination,volume_offset,grid_length,PhysicsEntity,LEFT);
				CheckAndAddToOpenMap(curindex,fr_dir,vDestination,volume_offset,grid_length,PhysicsEntity,FR);
				CheckAndAddToOpenMap(curindex,fl_dir,vDestination,volume_offset,grid_length,PhysicsEntity,FL);
				CheckAndAddToOpenMap(curindex,br_dir,vDestination,volume_offset,grid_length,PhysicsEntity,BR);
				CheckAndAddToOpenMap(curindex,bl_dir,vDestination,volume_offset,grid_length,PhysicsEntity,BL);

				for(int i=1,j=-1; i<=4,j>=-4;++i,--j)
				{
					ChangeParent(curindex,i,vStart_Pos);
					ChangeParent(curindex,j,vStart_Pos);
				}
				needChangeIndex = true;
			}
			if(last_length / length < 1.1)
				grid_length /= 2;
			else
			{
				grid_length = length / 2;
				last_length = length;
			}
		}
		int out_index = curindex;
		while(out_index != 5000)
		{
			path.push_back(close_map[out_index].pos);
			out_index = close_map[out_index].Parent;
		}
	}*/
}

void PathFinder::CheckAndAddToOpenMap(AStar_Vec &Avec, std::map<int,DynamicNavNode> &mesh, const Vec3 &vStart_Pos, const Vec3 &vDestination)
{
	std::list<int>::iterator iter = Avec.accessible->begin();
	std::list<int>::iterator end = Avec.accessible->end();
	while(iter != end)
	{
		if(!close_map.count(*iter))
		{
			AStar_Vec temp_vec(mesh[*iter].pos);
			temp_vec.index = *iter;
			temp_vec.Parent = Avec.index;
			CalculateFGH(temp_vec,vStart_Pos,vDestination);
			if(!open_map.count(*iter))
			{
				open_map[*iter] = temp_vec;
			}
			else
			{
				float checked_Gvalue = (Avec.pos - open_map[*iter].pos).GetLengthSquared() + Avec.Gvalue;
				if(checked_Gvalue < open_map[*iter].Gvalue)
				{
					open_map[*iter].Parent = Avec.index;
					open_map[*iter].Gvalue = checked_Gvalue;
				}
			}
		}
		++iter;
	}
/*
	if(close_map.count(index + _DIR))
		return;
	ray_hit hit;
	int rayFlags(rwi_stop_at_pierceable|rwi_colltype_any);
	int hits = gEnv->pPhysicalWorld->RayWorldIntersection(close_map[index].pos,dir*(grid_length+volume_offset),ent_all,rayFlags,&hit,1,&pPhysics,1);
	if(!hits)
	{
		Vec3 pos = close_map[index].pos + dir*(grid_length+volume_offset); 
		AStar_Vec new_vec(pos);
		new_vec.Parent = index;
		new_vec.CalculateValue(close_map[5000].pos,vDest);
		if(!open_map.count(index+_DIR))
		{
			open_map[index+_DIR] = new_vec;
			Debug_open_map[index + _DIR] = new_vec.pos;
		}
	}
	else
	{
		Vec3 pos = hit.pt;
		AStar_Vec new_vec(pos);
		new_vec.Parent = index;
		new_vec.Hvalue = 99999;
		new_vec.Gvalue = 99999;
		new_vec.Fvalue = 99999;
		//new_vec.CalculateValue(close_map[5000].pos,vDest);
		if(!open_map.count(index+_DIR))
		{
			open_map[index+_DIR] = new_vec;
			Debug_open_map[index + _DIR] = new_vec.pos;
		}
	}*/
}

void PathFinder::CalculateFGH(AStar_Vec &Avec, const Vec3 &vStart_Pos, const Vec3 &vDestination)
{
	Avec.Gvalue = (Avec.pos - vStart_Pos).GetLengthFast();
	Avec.Hvalue = (Avec.pos - vDestination).GetLengthFast();
	Avec.Fvalue = Avec.Hvalue + Avec.Gvalue;
}

int PathFinder::findMinF()
{
	std::map<int,AStar_Vec>::iterator iter = open_map.begin();
	std::map<int,AStar_Vec>::iterator end = open_map.end();
	float min_length = 9999999;
	int index = 0;
	while(iter != end)
	{
		if(iter->second.Fvalue < min_length)
		{
			min_length = iter->second.Fvalue;
			index = iter->first;
		}
		++iter;
	}
	close_map[index] = open_map[index];
	open_map.erase(index);
	return index;
}

void PathFinder::ChangeParent(int index, int _DIR,const Vec3 &vStart_Pos)
{
	if(!close_map.count(index + _DIR) && open_map[index+_DIR].Fvalue != 99999)
	{
		float old_length = (open_map[index+_DIR].pos - vStart_Pos).GetLengthFast();
		float new_length = (close_map[index].pos - vStart_Pos).GetLengthFast();
		new_length += (close_map[index].pos - open_map[index+_DIR].pos).GetLengthFast();
		if(new_length < old_length)
		{
			open_map[index+_DIR].Parent = index;
			open_map[index+_DIR].Gvalue = new_length;
		}
	}
}

void PathFinder::DebugOpenMap()
{
	if(!Debug_open_map.empty())
	{
		std::map<int,Vec3>::iterator iter = Debug_open_map.begin();
		std::map<int,Vec3>::iterator end = Debug_open_map.end();
		while(iter != end)
		{
			gEnv->pRenderer->DrawLabel(iter->second,1.5,"%d",iter->first);
			++iter;
		}
	}
}

void PathFinder::DebugPath()
{
	if(!path.empty())
	{
		std::map<int,DynamicNavNode> mesh =  DynamicNavMesh::Instance().GetNevMesh();

		std::deque<int>::iterator iter = path.begin();
		std::deque<int>::iterator iter_next = iter + 1;
		std::deque<int>::iterator end = path.end();
		while(iter_next != end)
		{
			gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(mesh[*iter].pos, ColorB(255, 100, 255), mesh[*iter_next].pos, ColorB(255, 100, 255));
			++iter;
			++iter_next;
		}
	}
}

void PathFinder::ImportNevMesh()
{
	std::map<int,DynamicNavNode> mesh = DynamicNavMesh::Instance().GetNevMesh();
	if(!mesh.empty())
	{
		std::map<int,DynamicNavNode>::iterator iter = mesh.begin();
		std::map<int,DynamicNavNode>::iterator end = mesh.end();
		while(iter != end)
		{
			AStar_Vec temp_vec(iter->second.pos);
			temp_vec.accessible = &iter->second.accessible;
		}
	}
}

void PathFinder::PathSmooth()
{
	if(path.size() > 1)
	{
		std::deque<int>::iterator iter = path.begin();
		std::deque<int>::iterator iter_next = iter + 1;
		std::deque<int>::iterator iter_next_dest = iter + 2;
		std::deque<int>::iterator end = path.end();
		while(iter_next_dest != end)
		{
			ray_hit hit;
			Vec3 diff = close_map[*iter_next_dest].pos - close_map[*iter].pos;
			Vec3 dir = diff.GetNormalized();
			IEntity *pPlayer = gEnv->pEntitySystem->GetEntity(g_pGame->GetClientActorId());
			IPhysicalEntity *pPhysics = pPlayer->GetPhysics();
			int rayFlags(rwi_stop_at_pierceable|rwi_colltype_any);
			//f32 detect_length = (close_map[*iter_next_dest].pos - close_map[*iter].pos).GetLengthFast();
			f32 detect_length = 2.23;
			int hits = gEnv->pPhysicalWorld->RayWorldIntersection(close_map[*iter].pos,dir*detect_length,ent_rigid | ent_static | ent_sleeping_rigid,rayFlags,&hit,1,&pPhysics,1);
			if(!hits)
			{
				iter = path.erase(iter_next);
				end = path.end();
				if(iter == end)
					return;
				iter_next = iter + 1;
				if(iter_next != end)
					iter_next_dest = iter_next + 1;
				else
					return;
			}
			else
			{
				++iter;
				++iter_next;
				++iter_next_dest;
			}
		}
	}
}