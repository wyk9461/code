#ifndef _CODEEXCLUSION_H_
#define _CODEEXCLUSION_H_

#include "StdAfx.h"
#include "StlUtils.h"
#include "TimerManager.h"

struct DynamicExplosionParams
{
	float damage;
	float trigger_time;
	float tick_timer;
	IParticleEmitter *pEmitter;
	DynamicExplosionParams()
	:	damage(300)
	,	trigger_time(1)
	,	tick_timer(0)
	,	pEmitter(NULL){}
};


class CodeExclusion
{
private:
	CodeExclusion();
	static const int NoticeDT_Id=40;

	enum ExclusionLevel
	{
		eExL_Safe = 0,
		eExL_Notice,
		eExL_Warning,
		eExL_Exclusion,
		eExL_Eliminate
	};

	enum Delays
	{
		eDNotice_Delete = 60,
		eDNotice_DeleteCrossHair = 40,
		eDNotice_Beacon = 50,
		eDNotice_Entity = 30,
		eDNotice_Explosion = 40,

		eDWarning_Delete = 30,
		eDWarning_DeleteCrossHair = 20,
		eDWarning_Entity = 20,
		eDWarning_Explosion = 26,

		eDExclusion_Delete = 15,
		eDExclusion_DeleteCrossHair = 10,
		eDExclusion_Entity = 10,
		eDExclusion_Explosion = 10,

		eDEliminate_Delete = 6,
		eDEliminate_DeleteCrossHair = 2,
		eDEliminate_Entity = 5,
		eDEliminate_Explosion = 3,
	};

	enum SpawnTimes
	{
		eSNotice_Entity = 2,
		eSNotice_Explosion = 3,
	};

	struct ExTimers
	{
		float BeaconSpawnTimer;
		float EntitySpawnTimer;
		float DeleteTimer;
		float DeleteCrossHairTimer;
		float SystemRobotTimer;
		float DynamicExplosionTimer;
	};

	struct SystemSpawnEntityParams
	{
		float timer;
		float spawntime;
		float glow_amount;
		IMaterial *SpawnMat;
		IMaterial *DefaultEntityMat;
	};

	typedef std::map<float ,EntityId> Distancemap;
	typedef std::set<IEntity*> BeaconSet;
	typedef std::map<EntityId,SystemSpawnEntityParams> SystemEntityMap;
	typedef std::map<unsigned int, DynamicExplosionParams> DynamicExplosionMap;

	ExclusionLevel CurExclusionLevel;
	ExTimers ExclusionTimers;

	float ExclusionAmount;
	float maxExclusionAmount;

	IEntity *ProcessEntity;
	IPhysicalEntity **NearbyEntity;

	Distancemap DisEntities;
	Distancemap::iterator distance_iter;
	BeaconSet Beacons;
	BeaconSet::iterator beacons_iter;
	SystemEntityMap SystemEntities;
	SystemEntityMap::iterator entities_iter;
	std::deque<EntityId> DeleteQueue;

	DynamicExplosionMap DynamicExplosions;
	IdPool DynamicExplosionPool;

	IUIElement *pElement;

	float ExclusionTimer;
	float ExclusionFadeTimer;
	float ExclusionFadeRate;

	float Delete_Delay;
	float DeleteCrossHair_Delay;
	float BeaconSpawn_Delay;
	float EntitySpawn_Delay;
	float DynamicExplosion_Delay;

	float EntitySpawnTime;
	float ExplosionSpawnTime;

	bool CanDelete;
	bool CanDeleteCrossHair;
	bool CanSpawnBeacon;
	bool CanSpawnEntity;
	bool CanSpawnSystemRobot;
	bool CanCreateDynamicExplosion;

	Vec3 PlayerPos;
	Vec3 PlayerDir;
	Vec3 DetectPos;
	Vec3 BeaconSpawnPos;
	Vec3 EntitySpawnPos;

	IMaterial *DefaultSpawnMat;

	IEntityClass *BasicEntityClass;

	EntityId SpawnId;
	EntityId SpawnId_min;

	float STA_DecayRate;
	bool ModuleEnable[3];

	void GetNearbyEntityForDelete();

	void GetDeleteEntity();
	void AddToDeleteList();
	void AddToDeleteList(EntityId Id);
	void InitTimers();
	void UpdateExclusiongTimer(const float frameTime);
	void UpdateTimers(const float frameTime);
	void UpdateNoticeTimer(const float frameTime);
	bool FindSpawnPoint();
	void SpawnBeacons();
	void SystemSpawnObstacle();
	void SpawnSystemRobot();
	bool FindSpawnPosByPlayerSpeed();
	const Vec3 RetriveSpawnPosByActorSpeed(CActor *pActor);
	const Vec3 RetriveSpawnPosByActorSpeed(CActor *pActor, float moving_time);
	void SystemPostSpawn(const float frameTime);
	void DeleteExEntity();
	void DoubleSpawnPointNumber();
	void UpdateUIInfo();
	void DeleteCrossHairEntity();
	void SetSTA_DecayRate(float rate){STA_DecayRate = rate;}
	void SetModuleEnable(int index, bool available){if(index > 2) return; ModuleEnable[index] = available;}
	void SetupTrap();
	void CreateDynamicExplosion();
	void UpdateDynamicExplosion(float frameTime);
public:
	static CodeExclusion &Instance(){static CodeExclusion instance;return instance;}
	void Update(const float frameTime);

	void SystemSpawn(SEntitySpawnParams &params, float _spawntime = 5.0f);

	void AddExclusionAmount(float amount){ExclusionAmount += amount;}
	void SetExclusionLevel();
	const float GetSystemRobotRefreshTime();
	const bool CheckAndDeleteAbsouredEntity(EntityId Id);

	ILINE const float GetSTA_DecayRate(){return STA_DecayRate;}
	ILINE const bool IsModuleEnabled(int index){if(index > 2) return false; return ModuleEnable[index];}
	ILINE const float GetExclusionAmount(){return ExclusionAmount;}
};

#endif