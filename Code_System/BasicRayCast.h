#ifndef _BASICRAYCAST_H_
#define _BASICRAYCAST_H_ 

#include "IFlashUI.h"

struct RayCastBackup
{
	int hits;
	bool Enable;
	float distance;
	int query_flag;
	IPhysicalWorld *pWorld;
	IActor *pSelfActor;
	CActor *pActor;
	IEntity *pSelf;
	int rayFlags;
};

class BasicRayCast
{
private:
	bool Enable;
	Vec3 left_up;
	Vec3 left_down;
	Vec3 right_up;
	Vec3 right_down;
	Vec3 screen_left_up;
	Vec3 screen_left_down;
	Vec3 screen_right_up;
	Vec3 screen_right_down;

	IUIElement *pElement;
	void Calculate_Screen_Pos(IEntity *pEntity);
protected:
	ray_hit hit;
	int hits;
	int query_flag;
	float distance;
	SMovementState info;
	IEntity *pEntity;
	IEntity *pLastEntity;
	IPhysicalWorld *pWorld;
	IActor *pSelfActor;
	CActor *pActor;
	IEntity *pSelf;
	IMovementController *pMC;
	int rayFlags;
	IPhysicalEntity *pSelfPhysics;

	IEntity *GetNearestEntity(const Vec3 &pos);
	std::map<float,IEntity*> DistanceEntity;
public:
	BasicRayCast()
	:	hits(0)
	,	Enable(true)
	,	pEntity(NULL)
	,	pLastEntity(NULL)
	,	distance(20)
	,	query_flag(ent_rigid | ent_sleeping_rigid | ent_living | ent_independent | ent_static | ent_terrain)
	,	pWorld(gEnv->pPhysicalWorld)
	,	pSelfActor(NULL)
	,	pActor(NULL)
	,	pSelf(NULL)
	,	rayFlags(rwi_stop_at_pierceable|rwi_colltype_any)
	{
		pElement = gEnv->pFlashUI->GetUIElement("HUD_Final");
	}
	~BasicRayCast(){pSelfActor=NULL;pActor=NULL;pSelf=NULL;}

	void Update(float frametime);
	void Save(RayCastBackup &backup);
	void Load(RayCastBackup &backup);
	void Load();


	inline void ChangeFlag(int flag){query_flag=flag;}
	inline void ChangeDistance(float dist){distance=dist;}
	inline const float GetDistance(){return distance;}
	inline IEntity* GetEntity(){return pEntity;}
	void SetScreenEntity(IEntity *sEntity){pEntity = sEntity;}
	inline ray_hit &GetRay_hit(){return hit;}
	inline SMovementState& GetInfo(){return info;}
	inline void SetEnable(const bool enable){Enable=enable;}
	inline const Vec3& GetScreen_Left_up(){return screen_left_up;}
	inline const Vec3& GetScreen_Left_down(){return screen_left_down;}
	inline const Vec3& GetScreen_Right_up(){return screen_right_up;}
	inline const Vec3& GetScreen_Right_down(){return screen_right_down;}
	inline const int& GetHits(){return hits;}
	void SetUIAimPointType(int _type);

	void SetUIEnemyState(float pos_x, float pos_y, bool _visible, EntityId Id, float _chealth, float _mhealth, string _class);
	void EnableEnemyState(bool _enable, EntityId Id);
	void ClearLastEntity(EntityId Id){if(pLastEntity) pLastEntity = NULL;}

	void Reset();
};



#endif