#include "StdAfx.h"
#include "MetaData.h"
#include "Recovery.h"
#include "Perk.h"

void MetaData::GainMetaData(unsigned int amount)
{
	//For Debug
	PerkManager::Instance().GenerateRandomPerks();
	metadata += amount;

	IUIElement *pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	if(pElement)
	{
		SUIArguments args;
		args.AddArgument(metadata);
		args.AddArgument(1000);
		pElement->CallFunction("SetMetaData", args);
		args.Clear();
		args.AddArgument(DataRecipient);
		pElement->CallFunction("SetRecipient",args);
		args.Clear();
	}


	while(metadata >= 1000)
	{
		FillRecipient();
	}

	if(CheckedSet.size() > 100)
		CheckedSet.clear();
}

void MetaData::FillRecipient()
{
	++DataRecipient;
	CostMetaData(1000);
	IUIElement *pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	if(pElement)
	{
		SUIArguments args;
		args.AddArgument(metadata);
		args.AddArgument(1000);
		pElement->CallFunction("SetMetaData", args);
		args.Clear();
		args.AddArgument(DataRecipient);
		pElement->CallFunction("SetRecipient",args);
		args.Clear();
	}
}