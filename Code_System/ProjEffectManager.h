#ifndef _PROJEFFECTMANAGER_H_
#define _PROJEFFECTMANAGER_H_

#include "Code_System/NewProjEffect/DataLeak.h"
#include "Code_System/NewProjEffect/DataRedundance.h"
#include "Code_System/NewProjEffect/InterfaceTrans.h"
#include "Code_System/NewProjEffect/DataExplode.h"
#include "Code_System/NewProjEffect/DataFreeze.h"
#include "Code_System/NewProjEffect/DataBlock.h"
#include "Code_System/NewProjEffect/BulletReflection.h"

enum Effect_Type
{
	Data_Leak = 0,
	Data_Redundance,
	Interface_Trans,
	Data_Explode,
	Data_Freeze,
	Data_Block,
	Redundance_Leak,
	Trans_Leak,
	Trans_Redundance,
	Bullet_Reflection,
};

class ProjEffectManager
{
private:
	typedef std::set<Effect_Type> ProjEffects_Set;
	ProjEffects_Set curProjEffects;

	DataLeak *m_DataLeak; 
	DataRedundance *m_DataRedundance;
	InterfaceTrans *m_InterfaceTrans;
	DataExplode *m_DataExplode;
	DataFreeze *m_DataFreeze;
	DataBlock *m_DataBlock;
	BulletReflection *m_BulletReflection;

	const bool HasAdvancedEffect();
public:
	ProjEffectManager()
	:	m_DataLeak(new DataLeak())
	,	m_DataRedundance(new DataRedundance())
	,	m_InterfaceTrans(new InterfaceTrans())
	,	m_DataExplode(new DataExplode())
	,	m_DataFreeze(new DataFreeze())
	,	m_DataBlock(new DataBlock())
	,	m_BulletReflection(new BulletReflection()){}

	~ProjEffectManager();
	void Update(const float &frameTime);

	void Add_DataLeak_Effect();
	inline const bool HasDataLeakEffect(){if(curProjEffects.count(Data_Leak)) return true; return false;}

	void Add_DataRedundance_Effect();
	inline const bool HasDataRedundanceEffect(){if(curProjEffects.count(Data_Redundance)) return true; return false;}

	void Add_InterfaceTrans_Effect();
	inline const bool HasInterfaceTransEffect(){if(curProjEffects.count(Interface_Trans)) return true; return false;}

	void Add_DataExplode_Effect();
	inline const bool HasDataExplodeEffect(){if(curProjEffects.count(Data_Explode)) return true; return false;}

	void Add_DataFreeze_Effect();
	inline const bool HasDataFreezeEffect(){if(curProjEffects.count(Data_Freeze)) return true; return false;}

	void Add_DataBlock_Effect();
	inline const bool HasDataBlockEffect(){if(curProjEffects.count(Data_Block)) return true; return false;}

	void Add_RedundanceLeak_Effect();
	inline const bool HasRedundanceLeakEffect(){if(curProjEffects.count(Redundance_Leak)) return true; return false;}

	void Add_TransLeak_Effect();
	inline const bool HasTransLeakLeakEffect(){if(curProjEffects.count(Trans_Leak)) return true; return false;}

	void Add_TransRedundance_Effect();
	inline const bool HasTransRedundanceEffect(){if(curProjEffects.count(Trans_Redundance)) return true; return false;}

	void Add_BulletReflection_Effect();
	inline const bool HasBulletReflectionEffect(){if(curProjEffects.count(Bullet_Reflection)) return true; return false;}

	void AddDataLeakTarget(HitInfo &info , float fValue = 0);
	void Add_DataRedundanceTarget(HitInfo &info , float fValue = 0);
	void Add_InterfaceTransTarget(HitInfo &info , int iValue = 0);

	void ProcessDataExplode(HitInfo &info);
	void AddDataFreezeTarget(HitInfo &info);
	void AddDataBlockTarget(HitInfo &info);

	void ProcessBulletReflection(HitInfo &info);
};
#endif