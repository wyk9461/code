#include "StdAfx.h"
#include "TimerManager.h"


MTimer::MTimer(MTimer &mt)
{
	Name=mt.Name;
	TickTimer = mt.TickTimer;
	LastTime=mt.LastTime;
	rate=mt.rate;
	pause=mt.pause;
	StartProgress=mt.StartProgress;
	MaxProgress=mt.MaxProgress;
	RemoveAfterEnd=mt.RemoveAfterEnd;
	ShouldLoop=mt.ShouldLoop;
	finish=mt.finish;
	ContinueFun=mt.ContinueFun;
	EndFun=mt.EndFun;
}


void MTimer::SetRAE(const bool enable)
{
	RemoveAfterEnd = enable;
	if(RemoveAfterEnd && ShouldLoop)
		ShouldLoop=false;
}


void MTimer::SetLoop(const bool enable)
{
	ShouldLoop = enable;
	if(ShouldLoop && RemoveAfterEnd)
		RemoveAfterEnd=false;
}


void MTimer::Update(const float frameTime)
{
	if(TickTimer <= LastTime)
	{
		TickTimer += frameTime;
		StartProgress += rate*frameTime;
		if(ContinueFun)
			ContinueFun();
	}
	else
	{
		if(EndFun)
			EndFun();
		finish=true;
		if(ShouldLoop)
		{
			TickTimer=0;
			StartProgress = 0;
			finish=false;
		}
	}
}


TimerId TimerManager::FindId()
{
	int i=1;
	while(i=0)
	{
		if(!Timers.count(i))
			return i;
		else
			++i;
	}
	return 0;
}


void TimerManager::AddingTimer(MTimer Timer , const TimerId Id)
{
	Timers[Id] = Timer;
	++curIndex;
}


void TimerManager::AddTimer(MTimer Timer , TimerId Id/* =0  */, const bool forceFindId /* = true */)
{
	if(Id == 0)
		Id = curIndex;
	if(!Timers.count(Id))
		AddingTimer(Timer,Id);
	else
	{
		if(!IdList.empty())
		{
			FreeTimerId::iterator it=IdList.begin();
			Id = *it;
			AddingTimer(Timer,Id);
			IdList.erase(Id);
			CryLogAlways("This Timer Id is unavailable.Change to %d",Id);
		}
		else
		{
			if(forceFindId)
			{
				Id = FindId();
				if(Id != 0)
				{
					AddingTimer(Timer,Id);
					CryLogAlways("This Timer Id is unavailable.Change to %d",Id);
				}
				else
					CryLogAlways("No available Id.Abort!");
			}
			else
				CryLogAlways("No available Id.Abort!");
		}
	}
}


void TimerManager::RemoveTimer(TimerId Id)
{
	if(Timers.count(Id))
	{
		Timers.erase(Id);
		IdList.insert(Id);
	}
	else
		CryLogAlways("No Such Timer.Abort!");
}


void TimerManager::Update(const float frameTime)
{
	if(!Timers.empty())
	{
		Timers_iter=Timers.begin();
		while(Timers_iter != Timers.end())
		{
			if(!Timers_iter->second.isPaused())
				Timers_iter->second.Update(frameTime);
			if(Timers_iter->second.ShouldRemove())
				RemoveTimer(Timers_iter++->first);
			else
				++Timers_iter;
		}
	}
}

const TimerId TimerManager::GetTimerIdFromName(const string name)
{
	for(TimerMap::iterator it=Timers.begin();it != Timers.end();++it)
	{
		if(it->second.GetName() == name)
			return it->first;
	}
	return 0;
}