#ifndef _SPAWNEDAIMANAGER_H_
#define _SPAWNEDAIMANAGER_H_

#include <list>
#include <map>
#include "Code_System/PlayerInventory.h"

struct AIBindParams
{
	EntityId AI_Id;
	EntityId SpawnerId;
	AIBindParams()
	:	AI_Id(0)
	,	SpawnerId(0){}
	AIBindParams(const AIBindParams &cps)
	{
		AI_Id = cps.AI_Id;
		SpawnerId = cps.SpawnerId;
	}

	AIBindParams &operator=(const AIBindParams &rhs)
	{
		if(this == &rhs)
			return *this;
		AI_Id = rhs.AI_Id;
		SpawnerId = rhs.SpawnerId;
		return *this;
	}

	const bool operator==(const AIBindParams &rhs)
	{
		return AI_Id == rhs.AI_Id;
	}
};

struct Spawner
{
	bool Enable;
	int SpawnCount;
	int MaxSpawn;
	int eliminateCount;

	Spawner()
	:	Enable(true)
	,	SpawnCount(0)
	,	MaxSpawn(1)
	,	eliminateCount(0){}

	Spawner::Spawner(const Spawner &cps)
	{
		Enable = cps.Enable;
		SpawnCount = cps.SpawnCount;
		MaxSpawn = cps.MaxSpawn;
	}

	Spawner &operator=(const Spawner &rhs)
	{
		if(this == &rhs)
			return *this;
		Enable = rhs.Enable;
		SpawnCount = rhs.SpawnCount;
		MaxSpawn = rhs.MaxSpawn;
		return *this;
	}
};


class SpawnedAIManager
{
public:
	SpawnedAIManager();
	~SpawnedAIManager();
	AIBindParams *GetBindParams();
	bool AddToList(AIBindParams param);
	void AddSpawnerData(unsigned int _count , unsigned int _max ,const EntityId _Id);
	void RemoveSpawner(EntityId _Id);
	const Spawner *GetSpawnerData(EntityId _Id) ;
	void AddSpawnCount(EntityId _Id);
	bool removeCurData();
	void Update();
	void ClearList();
	const bool CheckSpawner(EntityId _Id);

	void ResetSpawnCount();
private:
	std::list<AIBindParams> ManagedAI;
	std::list<AIBindParams>::iterator iter;
	std::map<EntityId,Spawner> Spawners;
	std::map<EntityId,Spawner>::iterator Spawner_iter;
};

#endif