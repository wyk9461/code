#include "stdAfx.h"
#include "Code_System/EntityParamsUtils.h"

void EntityParamsUtils::Utils::clear()
{
	nslot=0;
	phys_type=0;
	stabilitycost=0;
	name="";
	entityclass="";
}

void EntityParamsUtils::BasicPhysParams::clear()
{
	mass=0;
	density=0;
	rigid=false;
}

void EntityParamsUtils::ExplosiveParams::clear()
{
	explode=false;
	invulnerable=false;
	effectscale=0;
	minradius=0;
	radius=0;
	minphysradius=0;
	physradius=0;
	soundradius=0;
	damage=0;
	pressure=0;
	health=0;
	effect=0;
	model_destroyed=0;
}

void EntityParamsUtils::LightParams::clear()
{
	turn_on=false;
	lightstyle=0;
	lightradius=0;
	diffusemultiplier=0;
	specularmultiplier=0;
	HDRDynamic=0;
	colordiffuse=Vec3(0,0,0);
}

void EntityParamsUtils::AIParams::clear()
{
	aibehavior="";
}

void EntityParamsUtils::BasicWeaponParams::clear()
{
	weaponName="";
	weaponClass="";
	firerate=0;
	player_damage=0;
	ai_damage=0;
	clip_size=0;
}

void EntityParamsUtils::ShotgunParams::clear()
{
	pellets=0;
	spread=0;
	pellet_damage=0;
}

void EntityParamsUtils::LandVehiceParams::clear()
{
	decceleration=0;
	topSpeed=0;
	reverseSpeed=0;
	defaultxml=0;
	disablephysics=0;
	velocitymultiplier=0;
	degree=0;
	backfriction=0;
	frontfriction=0;
	inertia=0;
}