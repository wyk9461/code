#include "StdAfx.h"
#include "CommonUsage.h"

const unsigned int IdPool::AssignId()
{
	if(Pool.size() >= maxIdAmount)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_ERROR,"Id Pool is full.");
		return 0;
	}
	if(!ReterivedIdPool.empty())
	{
		unsigned int tId = *ReterivedIdPool.begin();
		ReterivedIdPool.erase(tId);
		return tId;
	}
	Pool.insert(curId);
	return curId++;
}

const unsigned int IdPool::AssignSpecialId(unsigned int Id)
{
	if(Id == 0)
		return 0;
	if(Pool.size() >= maxIdAmount)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_ERROR,"Id Pool is full.");
		return 0;
	}
	if(!CheckIdUsable(Id))
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_ERROR,"Id is been used in ID Pool.");
		return 0;
	}
	Pool.insert(Id);
	return Id;
}

const bool IdPool::CheckIdUsable(unsigned int Id)
{
	if(Pool.count(Id))
		return false;
	return true;
}

const bool IdPool::RecoveryId(unsigned int Id)
{
	if(!Pool.count(Id))
		return false;
	ReterivedIdPool.insert(Id);
	Pool.erase(Id);
	return true;
}




EntityId CommonUsage::GetNerestEntityIdInBox(Vec3 pos, float radius, int entitytype)
{
	IPhysicalEntity **nearbyEntities;
	int num=gEnv->pPhysicalWorld->GetEntitiesInBox(pos-Vec3(radius), pos+Vec3(radius), nearbyEntities, entitytype);
	float Min_Distance = radius*5;
	EntityId nearestId = 0;
	AABB aabb;
	for(int i = 0; i < num; ++i)
	{
		IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
		if(pEntity)
		{
			float the_length = (pEntity->GetPos() - pos).GetLengthSquared();
			if(the_length < Min_Distance)
			{
				Min_Distance = the_length;
				nearestId = pEntity->GetId();
			}
		}
	}
	return nearestId;
}

void CommonUsage::CreateExplosion(EntityId shooterId, Vec3 pos, float damage, float radius, float pressure, unsigned int HitType /*= CGameRules::EHitType::Explosion*/,
								  char *effectName /* = "explosions.Grenade_SCAR.character" */)
{
	float min_radius = (float)(radius * 0.3);
	float phys_radius = radius;
	float min_phys_radius = (float)(phys_radius * 0.4);

	ExplosionInfo einfo(shooterId, 0, 0, damage , pos, Vec3(0,0,0),min_radius, radius,  phys_radius,  phys_radius , 0 , pressure , 0.0f , HitType);
	einfo.SetEffect(effectName,1.0f,0.2f);
	SExplosionContainer SExplosion;
	SExplosion.m_explosionInfo = einfo;
	g_pGame->GetGameRules()->QueueExplosion(einfo);
}

void CommonUsage::CreateQueueExplosion(EntityId shooterId, Vec3 pos, float damage, float radius, float pressure, unsigned int HitType /* = CGameRules::EHitType::Explosion */,
									   char *effectName /* = "explosions.Grenade_SCAR.character" */)
{
	float min_radius = (float)(radius * 0.3);
	float phys_radius = radius;
	float min_phys_radius = (float)(phys_radius * 0.4);

	ExplosionInfo einfo(g_pGame->GetClientActorId(), 0, 0, damage , pos, Vec3(0,0,0),min_radius, radius,  phys_radius,  phys_radius , 0 , pressure , 0.0f , HitType);
	einfo.SetEffect(effectName,1.0f,0.2f);
	g_pGame->GetGameRules()->QueueExplosion(einfo);
}

Vec3 CommonUsage::MoveAroundPoint(IEntity *pEntity, Vec3 pos, float radius, float angle, float offset)
{
	Vec3 StartPos = pos;
	float new_x = radius * cos(DEG2RAD(angle));
	float new_y = radius * sin(DEG2RAD(angle));
	float new_z = radius * sin(DEG2RAD(offset));
	StartPos.x += new_x;
	StartPos.y +=new_y;
	StartPos.z += new_z;

	return StartPos;
}

const bool CommonUsage::MoveToPosition(IEntity *pEntity, Vec3 Destination, float speed, float stopDistance)
{
	if(pEntity)
	{
		Vec3 CurPos = pEntity->GetPos();
		float length = (Destination - CurPos).GetLengthFast();
		if(length <= stopDistance)
			return true;
		float length_factor = length - stopDistance;
		Vec3 dir = (Destination - CurPos).normalize();
		float finalSpeed = speed;
		if(length_factor < speed)
			finalSpeed = length_factor / 2;
		Vec3 NextPos = CurPos + dir * finalSpeed * gEnv->pTimer->GetFrameTime();
		pEntity->SetPos(NextPos);
	}
	return false;
}