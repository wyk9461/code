#include "StdAfx.h"
#include "ScanSystem.h"
#include "MapIndicator.h"
#include "CryPath.h"
#include "EntityDataBase.h"



ScanSystem::ScanSystem()
{
	DataBase=gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
	StartScan=false;
	DefaultScanMtl=gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("Scan");
	Available=true;
	preLoad = EntityClassPreLoad::Instance();
}

//------------------------------------------------------------------------
void ScanSystem::Update(float &frameTime)
{
	if(StartScan)
	{
		Available=false;
		Scan.timer+=frameTime;
		Scan.rate=100/Scan.scan_time;
		if(Scan.timer<=Scan.scan_time)
		{
			IRenderer *pRenderer=gEnv->pRenderer;
			pRenderer->DrawLabel(Scan.ScanEntity->GetPos(),1.5f,"Scanning....%.0f%%",Scan.progress);
			Scan.progress+=Scan.rate*frameTime;
		}
		if(Scan.timer>Scan.scan_time)
		{
			Scan.ScanEntity->SetMaterial(Scan.DefaultMtl);
			StartScan=false;
			Scan.timer=0;
			Scan.progress=0;
			Available=true;
			AddAndSave();
		}
	}
}

//------------------------------------------------------------------------
const bool ScanSystem::CheckExist() const
{
	XmlNodeRef TempDB=DataBase;
	XmlNodeRef temp;
	XmlNodeRef tSet;
	if(strcmp(Scan.SpawnSetName,"") != 0)
	{
		tSet=DataBase->findChild(Scan.SpawnSetName);
		if(tSet)
			TempDB = tSet;
		else
			return false;
	}
	if(Scan.SpawnName)
		temp=TempDB->findChild(Scan.SpawnName);
	if(temp)
		return true;
	else
		return false;
}

//------------------------------------------------------------------------
const bool ScanSystem::IsScanable() const
{
	/*if(Scan.className != "Human" && Scan.className !="HMMWV")		//Need to change
		return true;
	return false;*/
	return true;
}

//------------------------------------------------------------------------
void ScanSystem::ProcessScanEntity(IEntity *SEntity)
{
	//Only one at a time
	if(StartScan)
		return;
	Scan.ScanEntity=SEntity;
	Scan.timer=0;
	SmartScriptTable root=Scan.ScanEntity->GetScriptTable();
	SmartScriptTable props;
	root->GetValue("Properties",props);
	if(!props->HaveValue("SpawnName"))
	{
		/*Scan.scan_time = 1;
		StartScan=true;*/
		return;
	}
	props->GetValue("SpawnName",Scan.SpawnName);
	if(props->HaveValue("SpawnSet"))
		props->GetValue("SpawnSet",Scan.SpawnSetName);
	if(CheckExist() || strcmp(Scan.SpawnName,"") == 0)
		return;


	if(IsScanable())
	{
		AABB aabb;
		Scan.ScanEntity->GetLocalBounds(aabb);
		Scan.ScanMtl=gEnv->p3DEngine->GetMaterialManager()->CloneMaterial(DefaultScanMtl,0);
		Scan.DefaultMtl=Scan.ScanEntity->GetMaterial();
		Scan.ScanEntity->SetMaterial(Scan.ScanMtl);
		IEntityClass *pClass = Scan.ScanEntity->GetClass();
		Scan.Archetype = Scan.ScanEntity->GetArchetype();
		//Common Params
		/*SmartScriptTable root=Scan.ScanEntity->GetScriptTable();
		SmartScriptTable props;
		SmartScriptTable physprops;
		root->GetValue("Properties",props);
		props->GetValue("Physics",physprops);
		char* name("");		//Convert to XMLString later.GetValue can't take XMLString as params
		char* model_name("");		//Convert to XMLString later.GetValue can't take XMLString as params
		//Scan.SpawnSetName="";		//Put into the Constructor later.
		props->GetValue("SpawnName",name);
		props->GetValue("SpawnSet",Scan.SpawnSetName);
		props->GetValue("object_Model",model_name);
		props->GetValue("SpawnTime",Scan.spawntime);
		props->GetValue("StabilityCost",Scan.Utils.stabilitycost);
		props->GetValue("ProjectileType",Scan.Utils.projectile_type);

		Scan.Utils.phys_type = Scan.ScanEntity->GetPhysics()->GetType();
		Scan.Utils.name=name;
		Scan.model=model_name;


		//BasicEntity
		if(pClass == preLoad.pBasicEntityClass)
		{
			ScanBasicEntity(physprops);
		}
		//DestroyableObject
		else if(pClass == preLoad.pDestroyableObjectClass)
		{
			ScanDestroyableObject();
		}
		else if(pClass == preLoad.pLightClass)
		{
			ScanLight();
		}
		else if(pClass == preLoad.pTrap)
		{
			ScanTrap();
		}
		else if (pClass == preLoad.pForceField)
		{
			ScanForceField();
		}
		else if (pClass == preLoad.pLeakHole)
		{
			ScanLeakHole();
		}
		else if (pClass == preLoad.pForceDevice)
		{
			ScanForceDevice();
		}
		else if (pClass == preLoad.pPowerBall)
		{
			ScanPowerBall();
		}*/
		Scan.scan_time = Scan.spawntime + aabb.GetVolume()/3;
		StartScan=true;
	}
}

void* ScanSystem::GetbasicPhysicsParams(SmartScriptTable physTable)
{
	EntityParamsUtils::BasicPhysParams *bpp = new EntityParamsUtils::BasicPhysParams;

	physTable->GetValue("Mass",bpp->mass);
	physTable->GetValue("Density",bpp->density);

	return (void*)bpp;
}

void* ScanSystem::GetExplosionParams(SmartScriptTable propTable, SmartScriptTable exploTable, SmartScriptTable healthTable)
{
	EntityParamsUtils::ExplosiveParams *ep = new EntityParamsUtils::ExplosiveParams;
	char* model_destroyed;
	char* effect;
	propTable->GetValue("bExplode",ep->explode);
	propTable->GetValue("object_ModelDestroyed",model_destroyed);
	exploTable->GetValue("ParticleEffect",effect); 
	exploTable->GetValue("MinRadius",ep->minradius);
	exploTable->GetValue("Radius",ep->radius);
	exploTable->GetValue("MinPhysRadius",ep->minphysradius);
	exploTable->GetValue("PhysRadius",ep->physradius);
	exploTable->GetValue("SoundRadius",ep->soundradius);
	exploTable->GetValue("Pressure",ep->pressure);
	exploTable->GetValue("Damage",ep->damage);
	healthTable->GetValue("MaxHealth",ep->health);
	ep->model_destroyed=model_destroyed;
	ep->effect=effect;
	return (void*)ep;
}

void ScanSystem::ScanBasicEntity(SmartScriptTable physTable)
{
	void *bpp = GetbasicPhysicsParams(physTable);
	ParamsList.push_back(bpp);
}

void ScanSystem::ScanDestroyableObject()
{
	void *bpp;
	void *ep;

	SmartScriptTable root=Scan.ScanEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable physprops;
	SmartScriptTable explosion;
	SmartScriptTable health;
	//Init Table
	root->GetValue("Properties",props);
	props->GetValue("Physics",physprops);
	props->GetValue("Explosion",explosion);
	props->GetValue("Health",health);

	// Get Params
	bpp = GetbasicPhysicsParams(physprops);
	ep = GetExplosionParams(props,explosion,health);

	ParamsList.push_back(bpp);
	ParamsList.push_back(ep);
	//health->GetValue("bInvulnerable",0);
}

void ScanSystem::ScanLight()
{
	EntityParamsUtils::BasicPhysParams *bpp = new EntityParamsUtils::BasicPhysParams;
	EntityParamsUtils::LightParams *lp = new EntityParamsUtils::LightParams;
	EntityParamsUtils::ExplosiveParams *ep = new EntityParamsUtils::ExplosiveParams;

	SmartScriptTable root=Scan.ScanEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable physprops;
	SmartScriptTable explosion;
	SmartScriptTable modelparams;
	SmartScriptTable health;
	SmartScriptTable damage;
	SmartScriptTable props2;
	SmartScriptTable lightparams;
	SmartScriptTable style;
	SmartScriptTable color;
	char* model_destroyed;
	char* effect;
	//Init Table
	root->GetValue("Properties",props);
	root->GetValue("PropertiesInstance",props2);
	props2->GetValue("LightProperties_Base",lightparams);
	lightparams->GetValue("Style",style);
	lightparams->GetValue("Color",color);
	props->GetValue("Physics",physprops);
	props->GetValue("Explosion",explosion);
	props->GetValue("Health",health);
	props->GetValue("Model",modelparams);
	props->GetValue("Damage",damage);
	//Get Value
	physprops->GetValue("Mass",bpp->mass);
	physprops->GetValue("Density",bpp->density);
	//Light Params
	lightparams->GetValue("Radius",lp->lightradius);
	style->GetValue("nLightStyle",lp->lightstyle);
	color->GetValue("fDiffuseMultiplier",lp->diffusemultiplier);
	color->GetValue("fSpecularMultiplier",lp->specularmultiplier);
	color->GetValue("fHDRDynamic",lp->HDRDynamic);
	color->GetValue("clrDiffuse",lp->colordiffuse);
	//Explosion Params
	modelparams->GetValue("object_ModelDestroyed",model_destroyed);
	explosion->GetValue("Effect",effect); 
	explosion->GetValue("MinRadius",ep->minradius);
	explosion->GetValue("Radius",ep->radius);
	explosion->GetValue("MinPhysRadius",ep->minphysradius);
	explosion->GetValue("PhysRadius",ep->physradius);
	explosion->GetValue("Pressure",ep->pressure);
	explosion->GetValue("Damage",ep->damage);
	damage->GetValue("bExplode",ep->explode);
	damage->GetValue("fHealth",ep->health);
	ep->model_destroyed=model_destroyed;
	ep->effect=effect;

	ParamsList.push_back((void*)bpp);
	ParamsList.push_back((void*)lp);
	ParamsList.push_back((void*)ep);
}

void ScanSystem::ScanLadder()
{
	SmartScriptTable root=Scan.ScanEntity->GetScriptTable();
	SmartScriptTable props;
	//Init Table
	root->GetValue("Properties",props);
	props->GetValue("height",Scan.height);
}

void ScanSystem::ScanTrap()
{
	void *bpp;
	EntityParamsUtils::ExplosiveParams *ep = new EntityParamsUtils::ExplosiveParams;
	EntityParamsUtils::TrapParams *tp = new EntityParamsUtils::TrapParams;
	EntityParamsUtils::AIParams *ap = new EntityParamsUtils::AIParams;

	SmartScriptTable root=Scan.ScanEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable explosion;
	SmartScriptTable FunctionTable;
	char* effect;
	char* faction;

	root->GetValue("Properties",props);
	props->GetValue("FunctionParams",FunctionTable);
	props->GetValue("Explosion",explosion);

	bpp = GetbasicPhysicsParams(props);

	explosion->GetValue("Effect",effect); 
	explosion->GetValue("MinRadius",ep->minradius);
	explosion->GetValue("Radius",ep->radius);
	explosion->GetValue("MinPhysRadius",ep->minphysradius);
	explosion->GetValue("PhysRadius",ep->physradius);
	explosion->GetValue("SoundRadius",ep->soundradius);
	explosion->GetValue("Pressure",ep->pressure);
	explosion->GetValue("Damage",ep->damage);

	FunctionTable->GetValue("EffectScale",tp->effectScale);
	FunctionTable->GetValue("isDataLeakage",tp->isDataLeakage);
	FunctionTable->GetValue("isSlowDown",tp->isSlowDown);
	FunctionTable->GetValue("isDedication",tp->isDedication);
	FunctionTable->GetValue("isRecycle",tp->isRecycle);
	FunctionTable->GetValue("isDamageCov",tp->isDamageCov);

	ep->effect = effect;

	props->GetValue("esFaction",faction);
	ap->faction = faction;

	ParamsList.push_back(bpp);
	ParamsList.push_back((void*)ep);
	ParamsList.push_back((void*)tp);
	ParamsList.push_back((void*)ap);
	//props->GetValue("esFaction", Scan.AP.faction);
	//FunctionTable->GetValue(damageScale, Scan.BPP)

}

void ScanSystem::ScanLeakHole()
{
	void *bpp;
	EntityParamsUtils::LeakHoleParams *dhp = new EntityParamsUtils::LeakHoleParams;

	SmartScriptTable root = Scan.ScanEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable functionTable;

	root->GetValue("Properties", props);
	props->GetValue("FunctionParams", functionTable);

	functionTable->GetValue("Radius", dhp->radius);
	functionTable->GetValue("Damage", dhp->damage);
	functionTable->GetValue("Cd", dhp->Cd);
	bpp = GetbasicPhysicsParams(props);

	ParamsList.push_back(bpp);
	ParamsList.push_back((void*)dhp);
}

void ScanSystem::ScanForceField()
{
	void* bpp;
	EntityParamsUtils::ForceFieldParams *dpp = new EntityParamsUtils::ForceFieldParams;

	SmartScriptTable root = Scan.ScanEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable functionTable;

	root->GetValue("Properties", props);
	props->GetValue("FunctionParams", functionTable);

	functionTable->GetValue("Radius", dpp->radius);
	functionTable->GetValue("AccScale", dpp->accScale);

	bpp = GetbasicPhysicsParams(props);

	ParamsList.push_back(bpp);
	ParamsList.push_back((void*)dpp);
}

void ScanSystem::ScanForceDevice()
{
	void* bpp;
	EntityParamsUtils::ForceDeviceParams *kfp = new EntityParamsUtils::ForceDeviceParams;

	SmartScriptTable root = Scan.ScanEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable physprops;
	SmartScriptTable functionTable;
	root->GetValue("Properties", props);
	props->GetValue("Physics",physprops);
	props->GetValue("FunctionParams", functionTable);

	functionTable->GetValue("Enable", kfp->enable);
	functionTable->GetValue("HitTimesLimit", kfp->hitTimesLimit);
	functionTable->GetValue("Damage", kfp->damage);
	functionTable->GetValue("ExploRadius", kfp->exploRadius);
	functionTable->GetValue("Force", kfp->force);

	bpp = GetbasicPhysicsParams(physprops);

	ParamsList.push_back(bpp);
	ParamsList.push_back((void*)kfp);
}

void ScanSystem::ScanPowerBall()
{
	void* bpp;
	EntityParamsUtils::PowerBallParams *pbp = new EntityParamsUtils::PowerBallParams;

	SmartScriptTable root = Scan.ScanEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable physprops;
	SmartScriptTable functionTable;

	root->GetValue("Properties", props);
	props->GetValue("Physics",physprops);
	props->GetValue("FunctionParams", functionTable);

	functionTable->GetValue("Damage", pbp->damage);
	functionTable->GetValue("PowerUpperBound", pbp->powerUpperBound);
	functionTable->GetValue("ExploRadius", pbp->exploRadius);

	bpp = GetbasicPhysicsParams(physprops);

	ParamsList.push_back(bpp);
	ParamsList.push_back((void*)pbp);
}
//------------------------------------------------------------------------
void ScanSystem::AddAndSave()
{
	XmlNodeRef TempDB(DataBase);
	XmlNodeRef SpawnSet;
	XmlNodeRef temp;
	XmlNodeRef tparams;
	int index;
	index=TempDB->getChildCount();

	string className = Scan.ScanEntity->GetClass()->GetName();
	if(strcmp(Scan.SpawnSetName,"") != 0)
	{
		SpawnSet = TempDB->findChild(Scan.SpawnSetName);
		if(SpawnSet)
		{
			index = SpawnSet->getChildCount();
			TempDB = SpawnSet;
		}
		else
		{
			SpawnSet = DataBase->createNode(Scan.SpawnSetName);
			DataBase->addChild(SpawnSet);
			SpawnSet->setAttr("set",1);
			SpawnSet->setAttr("Class",className);
			SpawnSet->setAttr("Favorite", 0);
			index = SpawnSet->getChildCount();
			TempDB = SpawnSet;
		}
	}
	temp=TempDB->createNode(Scan.SpawnName);
	TempDB->addChild(temp);
	temp->setAttr("Archetype", (XmlString)(Scan.Archetype->GetName()));
	temp->setAttr("Favorite", 0);
	/*tparams=temp->createNode("params");
	temp->addChild(tparams);
	IEntityClass *pClass = Scan.ScanEntity->GetClass();

	if(pClass == preLoad.pBasicEntityClass)
		AddBasicEntity(tparams,index);

	else if(pClass == preLoad.pDestroyableObjectClass)
		AddDestroyableObject(tparams,index);

	else if(pClass == preLoad.pLightClass)
		AddLight(tparams,index);

	else if(pClass == preLoad.pTrap)
		AddTrap(tparams,index);

	else if(pClass == preLoad.pLeakHole)
		AddLeakHole(tparams,index);

	else if(pClass == preLoad.pForceDevice)
		AddForceDevice(tparams,index);

	else if(pClass == preLoad.pForceField)
		AddForceField(tparams,index);

	else if(pClass == preLoad.pPowerBall)
		AddPowerBall(tparams,index);

	if(pClass == preLoad.pBasicEntityClass)
		AddLadder(tparams,index);*/

	DataBase->saveToFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
	g_pGame->GetSpawnSystem()->SetXMLRoot(DataBase);
	EntityDataBase::Instance().AddNewItem(DataBase->getChildCount()-1, DataBase);
}

//------------------------------------------------------------------------
void ScanSystem::AddAndSave(XmlString &archetype)
{
	XmlNodeRef TempDB(DataBase);
	XmlNodeRef SpawnSet;
	XmlNodeRef temp;
	XmlNodeRef tparams;
	int index;
	index=TempDB->getChildCount();

	IEntityArchetype *pArcheType = gEnv->pEntitySystem->LoadEntityArchetype(archetype);

	CRY_ASSERT_MESSAGE(pArcheType, "No ArcheType Found!");

	SmartScriptTable pProps = pArcheType->GetProperties();
	pProps->GetValue("SpawnName",Scan.SpawnName);
	if(CheckExist() || strcmp(Scan.SpawnName,"") == 0)
		return;

	temp=TempDB->createNode(Scan.SpawnName);
	TempDB->addChild(temp);
	temp->setAttr("Archetype", archetype);
	temp->setAttr("Favorite", 0);

	DataBase->saveToFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
	g_pGame->GetSpawnSystem()->SetXMLRoot(DataBase);
	EntityDataBase::Instance().AddNewItem(DataBase->getChildCount()-1, DataBase);
}

//------------------------------------------------------------------------
void ScanSystem::AddBasicEntity(XmlNodeRef params,const int index)
{
	EntityParamsUtils::BasicPhysParams *bpp = (EntityParamsUtils::BasicPhysParams*)ParamsList.front();
	params->setAttr("Index",index+1);
	params->setAttr("Name",Scan.Utils.name);
	params->setAttr("Mass",bpp->mass);
	params->setAttr("Density",bpp->density);
	//params->setAttr("nSlot",-1);
	params->setAttr("Type",Scan.Utils.phys_type);
	params->setAttr("Model",Scan.model);
	params->setAttr("Class","BasicEntity");
	params->setAttr("Favorite",0);
	params->setAttr("ProjectileType",Scan.Utils.projectile_type);
	//Common Params 2
	params->setAttr("SpawnTime",Scan.spawntime);
	params->setAttr("StabilityCost",(int)Scan.Utils.stabilitycost);
	ParamsList.clear();
}

//------------------------------------------------------------------------
void ScanSystem::AddDestroyableObject(XmlNodeRef params,const int index)
{
	EntityParamsUtils::BasicPhysParams *bpp = (EntityParamsUtils::BasicPhysParams*)ParamsList.front();
	//Common Params
	params->setAttr("Index",index+1);
	params->setAttr("Name",Scan.Utils.name);
	params->setAttr("Mass",bpp->mass);
	params->setAttr("Density",bpp->density);
	//params->setAttr("nSlot",-1);
	params->setAttr("Type",Scan.Utils.phys_type);
	params->setAttr("Model",Scan.model);
	params->setAttr("Class","DestroyableObject");
	params->setAttr("Favorite",0);
	params->setAttr("ProjectileType",Scan.Utils.projectile_type);
	ParamsList.pop_front();
	//Explosion Params
	EntityParamsUtils::ExplosiveParams *ep = (EntityParamsUtils::ExplosiveParams*)ParamsList.front();
	params->setAttr("ModelDestroyed",ep->model_destroyed);
	params->setAttr("Explode",(int)ep->explode);
	params->setAttr("Damage",ep->damage);
	params->setAttr("Effect",ep->effect);
	params->setAttr("MinPhysRadius",ep->minphysradius);
	params->setAttr("MinRadius",ep->minradius);
	params->setAttr("PhysRadius",ep->physradius);
	params->setAttr("Radius",ep->radius);
	params->setAttr("SoundRadius",ep->soundradius);
	params->setAttr("Pressure",ep->pressure);
	params->setAttr("Health",ep->health);
	//Common Params 2
	params->setAttr("SpawnTime",Scan.spawntime);
	params->setAttr("StabilityCost",(int)Scan.Utils.stabilitycost);
	ParamsList.clear();
}

//------------------------------------------------------------------------
void ScanSystem::AddLight(XmlNodeRef params,const int index)
{
	EntityParamsUtils::BasicPhysParams *bpp = (EntityParamsUtils::BasicPhysParams*)ParamsList.front();
	//Common Params
	params->setAttr("Index",index+1);
	params->setAttr("Name",Scan.Utils.name);
	params->setAttr("Mass",bpp->mass);
	params->setAttr("Density",bpp->density);
	//params->setAttr("nSlot",-1);
	params->setAttr("Type",Scan.Utils.phys_type);
	params->setAttr("Model",Scan.model);
	params->setAttr("Class","DestroyableLight");
	params->setAttr("Favorite",0);
	params->setAttr("ProjectileType",Scan.Utils.projectile_type);
	ParamsList.pop_front();
	//Light Params
	EntityParamsUtils::LightParams *lp = (EntityParamsUtils::LightParams*)ParamsList.front();
	params->setAttr("Light_Radius",lp->lightradius);
	params->setAttr("LightStyle",(int)lp->lightstyle);
	params->setAttr("DiffuseMultiplier",lp->diffusemultiplier);
	params->setAttr("SpecularMultiplier",lp->specularmultiplier);
	params->setAttr("HDRDynamic",lp->HDRDynamic);
	params->setAttr("Color_Diffuse",lp->colordiffuse);
	ParamsList.pop_front();
	//Explosion Params
	EntityParamsUtils::ExplosiveParams *ep = (EntityParamsUtils::ExplosiveParams*)ParamsList.front();
	params->setAttr("ModelDestroyed",ep->model_destroyed);
	params->setAttr("Explode",(int)ep->explode);
	params->setAttr("Damage",ep->damage);
	params->setAttr("Effect",ep->effect);
	params->setAttr("MinPhysRadius",ep->minphysradius);
	params->setAttr("MinRadius",ep->minradius);
	params->setAttr("PhysRadius",ep->physradius);
	params->setAttr("Radius",ep->radius);
	params->setAttr("SoundRadius",ep->soundradius);
	params->setAttr("Pressure",ep->pressure);
	params->setAttr("Health",ep->health);
	//Common Params 2
	params->setAttr("SpawnTime",Scan.spawntime);
	params->setAttr("StabilityCost",(int)Scan.Utils.stabilitycost);
	ParamsList.clear();
}

//------------------------------------------------------------------------
void ScanSystem::AddLadder(XmlNodeRef params,const int index)
{
	//Common Params
	params->setAttr("Index",index+1);
	params->setAttr("Name",Scan.Utils.name);
	//params->setAttr("nSlot",-1);
	params->setAttr("Model",Scan.model);
	params->setAttr("Class","Ladder");
	params->setAttr("Favorite",0);
	params->setAttr("ProjectileType",Scan.Utils.projectile_type);
	//Common Params 2
	params->setAttr("SpawnTime",Scan.spawntime);
	params->setAttr("StabilityCost",(int)Scan.Utils.stabilitycost);
	//Ladder Params
	params->setAttr("Height",Scan.height);
}

void ScanSystem::AddTrap(XmlNodeRef params, const int index)
{
	EntityParamsUtils::BasicPhysParams *bpp = (EntityParamsUtils::BasicPhysParams*)ParamsList.front();
	params->setAttr("Index", index + 1);
	params->setAttr("Name", Scan.Utils.name);
	//params->setAttr("nSlot", -1);
	params->setAttr("Model", Scan.model);
	params->setAttr("Mass", bpp->mass);
	params->setAttr("Class", "Trap");
	params->setAttr("Favorite",0);
	params->setAttr("ProjectileType",Scan.Utils.projectile_type);
	ParamsList.pop_front();

	EntityParamsUtils::ExplosiveParams *ep = (EntityParamsUtils::ExplosiveParams *)ParamsList.front();
	params->setAttr("ParticleEffect", ep->effect);
	params->setAttr("MinRadius", ep->minradius);
	params->setAttr("Radius", ep->radius);
	params->setAttr("MinPhysRadius", ep->minphysradius);
	params->setAttr("SoundRadius", ep->soundradius);
	params->setAttr("Pressure", ep->pressure);
	params->setAttr("Damage", ep->damage);
	ParamsList.pop_front();

	EntityParamsUtils::TrapParams *tp = (EntityParamsUtils::TrapParams *)ParamsList.front();
	params->setAttr("EffectScale", tp->effectScale);
	params->setAttr("isDataLeakage", tp->isDataLeakage);
	params->setAttr("isSlowDown", tp->isSlowDown);
	params->setAttr("isDedication", tp->isDedication);
	params->setAttr("isRecycle", tp->isRecycle);
	params->setAttr("isDamageCov", tp->isDamageCov);
	ParamsList.pop_front();

	EntityParamsUtils::AIParams *ap = (EntityParamsUtils::AIParams*)ParamsList.front();
	params->setAttr("Faction", ap->faction);

	params->setAttr("SpawnTime",Scan.spawntime);
	params->setAttr("StabilityCost",(int)Scan.Utils.stabilitycost);
	ParamsList.clear();
}

void ScanSystem::AddLeakHole(XmlNodeRef params, const int index)
{
	EntityParamsUtils::BasicPhysParams *bpp = (EntityParamsUtils::BasicPhysParams*)ParamsList.front();
	params->setAttr("Index", index + 1);
	params->setAttr("Name", Scan.Utils.name);
	//params->setAttr("nSlot", -1);
	params->setAttr("Model", Scan.model);
	params->setAttr("Mass", bpp->mass);
	params->setAttr("Class", "LeakHole");
	params->setAttr("Favorite",0);
	params->setAttr("ProjectileType",Scan.Utils.projectile_type);
	ParamsList.pop_front();

	EntityParamsUtils::LeakHoleParams *dhp = (EntityParamsUtils::LeakHoleParams*)ParamsList.front();
	params->setAttr("Radius",dhp->radius);
	params->setAttr("Damage",dhp->damage);
	params->setAttr("Cd",dhp->Cd);

	params->setAttr("SpawnTime",Scan.spawntime);
	params->setAttr("StabilityCost",(int)Scan.Utils.stabilitycost);
	ParamsList.clear();
}

void ScanSystem::AddForceDevice(XmlNodeRef params, const int index)
{
	EntityParamsUtils::BasicPhysParams *bpp = (EntityParamsUtils::BasicPhysParams*)ParamsList.front();
	params->setAttr("Index",index+1);
	params->setAttr("Name",Scan.Utils.name);
	params->setAttr("Mass",bpp->mass);
	params->setAttr("Density",bpp->density);
	//params->setAttr("nSlot",-1);
	params->setAttr("Type",Scan.Utils.phys_type);
	params->setAttr("Model",Scan.model);
	params->setAttr("Class","ForceDevice");
	params->setAttr("Favorite",0);
	params->setAttr("ProjectileType",Scan.Utils.projectile_type);
	ParamsList.pop_front();

	EntityParamsUtils::ForceDeviceParams *kfp = (EntityParamsUtils::ForceDeviceParams*)ParamsList.front();

	if(kfp->enable == true)
		params->setAttr("Enable",1);
	else
		params->setAttr("Enable",0);
	params->setAttr("HitTimesLimit",kfp->hitTimesLimit);
	params->setAttr("Damage",kfp->damage);
	params->setAttr("ExploRadius",kfp->exploRadius);
	params->setAttr("Force",kfp->force);


	params->setAttr("SpawnTime",Scan.spawntime);
	params->setAttr("StabilityCost",(int)Scan.Utils.stabilitycost);
	ParamsList.clear();
}

void ScanSystem::AddPowerBall(XmlNodeRef params, const int index)
{
	EntityParamsUtils::BasicPhysParams *bpp = (EntityParamsUtils::BasicPhysParams*)ParamsList.front();
	params->setAttr("Index",index+1);
	params->setAttr("Name",Scan.Utils.name);
	params->setAttr("Mass",bpp->mass);
	params->setAttr("Density",bpp->density);
	//params->setAttr("nSlot",-1);
	params->setAttr("Type",Scan.Utils.phys_type);
	params->setAttr("Model",Scan.model);
	params->setAttr("Class","PowerBall");
	params->setAttr("Favorite",0);
	params->setAttr("ProjectileType",Scan.Utils.projectile_type);
	ParamsList.pop_front();

	EntityParamsUtils::PowerBallParams *pbp = (EntityParamsUtils::PowerBallParams*)ParamsList.front();

	params->setAttr("Damage",pbp->damage);
	params->setAttr("PowerUpperBound",pbp->powerUpperBound);
	params->setAttr("ExploRadius",pbp->exploRadius);


	params->setAttr("SpawnTime",Scan.spawntime);
	params->setAttr("StabilityCost",(int)Scan.Utils.stabilitycost);
	ParamsList.clear();
}

void ScanSystem::AddForceField(XmlNodeRef params, const int index)
{
	EntityParamsUtils::BasicPhysParams *bpp = (EntityParamsUtils::BasicPhysParams*)ParamsList.front();
	params->setAttr("Index", index + 1);
	params->setAttr("Name", Scan.Utils.name);
	//params->setAttr("nSlot", -1);
	params->setAttr("Model", Scan.model);
	params->setAttr("Mass", bpp->mass);
	params->setAttr("Class", "LeakHole");
	params->setAttr("Favorite",0);
	params->setAttr("ProjectileType",Scan.Utils.projectile_type);
	ParamsList.pop_front();

	EntityParamsUtils::ForceFieldParams *dp = (EntityParamsUtils::ForceFieldParams*)ParamsList.front();

	params->setAttr("Radius",dp->radius);
	params->setAttr("AccScale",dp->accScale);

	params->setAttr("SpawnTime",Scan.spawntime);
	params->setAttr("StabilityCost",(int)Scan.Utils.stabilitycost);
	ParamsList.clear();
}
