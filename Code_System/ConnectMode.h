#ifndef _COONECTMODE_H_
#define _COONECTMODE_H_

#include "IFlashUI.h"

namespace eConnectMode
{
	enum eCMode
	{
		Normal = 0,
		Deep,
	};

	enum eCLevel
	{
		eCLevel_None = 0,
		eCLevel_SpawnMenu,
		eCLevel_EntityDataBase,
		eCLevel_ChooseSystem,
		eCLevel_MidMenu,
		eCLevel_Modify_ChooseSlot,
		eCLevel_Modify_ComplexModify,
		eCLevel_Disable_ChooseSlot,
		eCLevel_Disable_Menu,
		eCLevel_Creating,
	};
};




class ConnectMode
{
private:

	unsigned short Level;
	unsigned short mode;
	IUIElement *pElement;

	void SetTimeScale(const short level);

public:
	ConnectMode()
	:	Level(eConnectMode::eCLevel::eCLevel_None)
	,	mode(eConnectMode::eCMode::Normal)
	{
		pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	}


	~ConnectMode(){}

	inline void SetMode(const int & m){mode = m;}
	inline const short GetMode(){return mode;}
	inline void SetLevel(const int &lev){Level = lev;SetTimeScale(Level);}
	inline const short GetLevel(){return Level;}

	void SetUIConnectMode(bool isDeepMode);
	const bool PopLevel();

};
#endif