#ifndef _SCANSYSTEM_H_
#define _SCANSYSTEM_H_

#include "Item.h"
#include "ISystem.h"
#include "physinterface.h"
#include "IMovementController.h"
#include "IInput.h"
#include "IEntityClass.h"
#include "IEntitySystem.h"
#include "IXml.h"
#include "IVehicleSystem.h"
#include "Actor.h"
#include "Code_System/WorldStability.h"
#include "Code_System/EntityParamsUtils.h"


class ScanSystem
{
private:
	struct Scans
	{
		IEntity* ScanEntity;
		IMaterial* ScanMtl;    // Scan Material
		IMaterial* DefaultMtl;    // Default Material of this entity
		IEntityArchetype *Archetype;
		char* SpawnName;    // SpawnName(From Lua)
		char* SpawnSetName;
		float  timer;    // Scan Timer
		float scan_time;    // Scan time
		float rate;    // Count Rate
		float progress;    // Scan Progress
		float spawntime;
		float height;		//For Ladder
		XmlString model;

		EntityParamsUtils::Utils Utils;

		Scans()
		:	ScanEntity(NULL)
		,	ScanMtl(NULL)
		,	DefaultMtl(NULL)
		,	Archetype(NULL)
		,	SpawnName("")
		,	SpawnSetName("")
		,	timer(0)
		,	scan_time(0)
		,	rate(0)
		,	progress(0)
		,	spawntime(0)
		,	height(0){}
	};

	std::deque<void*> ParamsList;

	EntityClassPreLoad preLoad;
	XmlNodeRef DataBase;
	Scans Scan;
	IMaterial *DefaultScanMtl;
	bool StartScan;
	bool Available;

	// Description :
	//		Check if this entity is already in the database.
	const bool CheckExist() const;

	// Description :
	//		Check if this entity is AI or Vehicle(you can not scan).
	const bool IsScanable() const;

	void* GetbasicPhysicsParams(SmartScriptTable physTable);
	void* GetExplosionParams(SmartScriptTable propTable, SmartScriptTable exploTable, SmartScriptTable healthTable);

	void ScanBasicEntity(SmartScriptTable physTable);
	void ScanDestroyableObject();
	void ScanLight();
	void ScanLadder();
	void ScanTrap();
	void ScanLeakHole();
	void ScanForceDevice();
	void ScanPowerBall();
	void ScanForceField();
	// Description :
	//		Save to the database
	void AddAndSave();

	void AddBasicEntity(XmlNodeRef params,const int index);

	void AddDestroyableObject(XmlNodeRef params,const int index);

	void AddLight(XmlNodeRef params,const int index);

	void AddLadder(XmlNodeRef params,const int index);

	void AddTrap(XmlNodeRef params, const int index);
	void AddLeakHole(XmlNodeRef params, const int index);
	void AddForceDevice(XmlNodeRef params, const int index);
	void AddPowerBall(XmlNodeRef params, const int index);
	void AddForceField(XmlNodeRef params, const int index);

public:
	ScanSystem();
	~ScanSystem();

	// Description :
	//		Get the information of the entity which will be scanned later.
	//	Arguments : 
	//		SEntity - The entity which will be scanned later.
	void ProcessScanEntity(IEntity *SEntity);

	void Update(float &frameTime);

	void AddAndSave(XmlString &archetype);
};

ScanSystem::~ScanSystem()
{
}


#endif