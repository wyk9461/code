#include "StdAfx.h"
#include "IRenderer.h"
#include "SpawnSystem.h"
#include "ItemSystem.h"
#include "Code_System/MicroSystem/StateManager.h"
#include "BattleChallenge.h"
#include "Code_System/EntityDataBase.h"
#include "CodeObject/Trap.h"
#include "CodeObject/LeakHole.h"
#include "CodeObject/ForceDevice.h"
#include "CodeObject/PowerBall.h"
#include "CodeObject/ForceField.h"
#include "MicroSystem/ProbeManager.h"

SpawnSystem::SpawnSystem()
{
	SlotIndex=0;
	hits = 0;
	dir(0.0f,0.0f,1.0f);
	StartFunction = false;
	locked=false;
	root = gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
	index=0;
	Maxnumber=5;
	defspawnmat=gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("spawn");
	lockedmat=gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("locked");
	zChange=false;
	query_flag=ent_all;
	pRenderer=gEnv->pRenderer;
	needXMLUpdate=true;
	RayCastEnabled=true;
	AligntoEntity=false;
	manual_offset=0;
	last_dir=Vec3(0,0,0);

	pEntity=NULL;
	placedir = eD_Nothing;
	last_placedir = eD_Nothing;

	SpawnId_min = 1000000000;
	SpawnId = 1000000000;

	rotationReset_timer = 0;

	//EntityClassPreLoad
	preLoad = EntityClassPreLoad::Instance();

	/*offset_left=0;
	offset_right=0;
	offset_forward=0;
	offset_back=0;
	offset_up=0;
	offset_down;*/

	/*for(int i=0;i<10;i++)
	{
		SP[i].SpawnEntity=NULL;
		SP[i].name="";
		SP[i].mass=0;
		SP[i].density=-1;
		SP[i].nslot=-1;
		SP[i].type=1;
		SP[i].model="";
		SP[i].entityclass="";
		SP[i].mat="";
		SP[i].aibehavior="";
		SP[i].defaultmat=NULL;
		SP[i].spawnmat=NULL;
		SP[i].lmat=NULL;
		SP[i].timer=0;
		SP[i].rate=0;
		SP[i].spawntime=0;
		SP[i].amount=50;
		SP[i].available=true;
		SP[i].Spawned=false;
		SP[i].Utils.stabilitycost=0;
	}*/
	curindex=0;
	lastindex=-1;
	iter = SpawnProcessMap.begin();
}

void SpawnSystem::Update(float frametime)
{
	PostSpawned(frametime);

	if(StartFunction && curindex!=-1)
	{
		/*BattleChallenge &instance = BattleChallenge::Instance();
		if(instance.GetRestrictType() == eRT_NoCreate)
			instance.FailRestrictChallenge();*/
		SMovementState info;
		if(RayCastEnabled)
		{
			IActor *pSelfActor=g_pGame->GetIGameFramework()->GetClientActor();
			CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
			IEntity *pSelf=pSelfActor->GetEntity();
			IMovementController *pMC=pSelfActor->GetMovementController();
			pMC->GetMovementState(info);
			pWorld=gEnv->pPhysicalWorld;
			int rayFlags(rwi_stop_at_pierceable|rwi_colltype_any);
			if(!ProbeManager::Instance().IsProbeActived())
			{
				IPhysicalEntity *pSelfPhysics=pSelf->GetPhysics();
				hits = pWorld->RayWorldIntersection(info.eyePosition,info.eyeDirection*13.f,query_flag,rayFlags,&hit,1,&pSelfPhysics,1);
			}
			else
			{
				IPhysicalEntity *pSelfPhysics=ProbeManager::Instance().GetProbe()->GetPhysics();
				float distance = ProbeManager::Instance().GetProbeRadius();
				hits = pWorld->RayWorldIntersection(ProbeManager::Instance().GetScanPos(),info.eyeDirection*distance,query_flag,rayFlags,&hit,1,&pSelfPhysics,1);
			}
			dir=hit.n;
			/*AABB hitBox=AABB(0.1);
			hitBox.Move(hit.pt);
			gEnv->pRenderer->GetIRenderAuxGeom()->DrawAABB(hitBox,false,ColorB(255,255,255,255),eBBD_Faceted);*/
			if(hit.pCollider)
			{
				//hit.pCollider->GetiForeignData()
				if(hit.pCollider->GetiForeignData() != PHYS_FOREIGN_ID_TERRAIN)
					pEntity=(IEntity*)hit.pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);
				if(aabb.IsNonZero())
					hit_dir=(hit.pt-aabb.GetCenter()).normalize();
				if(pEntity)
				{
					GetEntityDir(pEntity);

					/*gEnv->pRenderer->DrawLabel(aabb.GetCenter()+left_dir*localaabb.GetSize().x*0.5,1.5f,"left");
					gEnv->pRenderer->DrawLabel(aabb.GetCenter()+right_dir*localaabb.GetSize().x*0.5,1.5f,"right");
					gEnv->pRenderer->DrawLabel(aabb.GetCenter()+forward_dir*localaabb.GetSize().y*0.5,1.5f,"forward");
					gEnv->pRenderer->DrawLabel(aabb.GetCenter()+back_dir*localaabb.GetSize().y*0.5,1.5f,"back");
					gEnv->pRenderer->DrawLabel(aabb.GetCenter()+Vec3(0,0,1)*localaabb.GetSize().z*0.5,1.5f,"up");
					gEnv->pRenderer->DrawLabel(aabb.GetCenter()+Vec3(0,0,-1)*localaabb.GetSize().z*0.5,1.5f,"down");*/
				}
				if(localaabb.IsNonZero() && pEntity && left_dir.IsValid())
				{
					CalculateOffset(pEntity,localaabb);
				}
			}
		}
		/*if(!edit && g_pGame->GetConnectMode()->GetMode() == eConnectMode::eCMode::Deep)
			Spawn(EntityDataBase::Instance().GetCurSlotDBIndex());*/
		if(edit)
		{
			CurrentSpawn.SpawnEntity->Activate(false);
			editMode(frametime);
		}
	}
}


void SpawnSystem::PostSpawned(float &frametime)
{
	if(!SpawnProcessMap.empty())
	{
		iter = SpawnProcessMap.begin();
		ProcessMap::iterator end_iter = SpawnProcessMap.end();
		while(iter != end_iter)
		{
			Spawns &SP = iter->second;
			if(SP.timer == 0)
			{
				//CreateSpawnParticle(i);
				SP.amount=0;
			}
			SP.timer+=frametime;
			SP.rate=100/SP.spawntime;

			if(SP.timer<SP.spawntime)
			{
				SP.progress+=SP.rate*frametime;
				SP.SpawnEntity->Activate(false);
				//pRenderer->DrawLabel(SP.SpawnEntity->GetPos(),1.5f,"Spawning....%.0f%%",SP.progress);
			}

			if(SP.timer<SP.spawntime/2)
			{
				SP.spawnmat->SetGetMaterialParamFloat("glow",SP.amount,false); 
				//pRenderProxy->SetOpacity(SP.amount);
				SP.amount+=SP.rate*frametime;
			}

			SP.rate=50/SP.spawntime;
			if(SP.timer>SP.spawntime/2 && SP.timer<SP.spawntime)
			{		
				SP.amount-=SP.rate*frametime;
				SP.spawnmat->SetGetMaterialParamFloat("glow",SP.amount,false); 
			}

			if(SP.timer>SP.spawntime)
			{
				SP.timer=0;
				SP.amount=10;
				SP.spawnmat->SetGetMaterialParamFloat("glow",SP.amount,false); 

				SP.SpawnEntity->SetMaterial(SP.defaultmat);

				SEntityPhysicalizeParams pparams;
				pe_status_dynamics status;
				SP.SpawnEntity->GetPhysics()->GetStatus(&status);
				pparams.mass = status.mass;
				pparams.density = -1;
				pparams.nSlot= -1;
				pparams.type = SP.SpawnEntity->GetPhysics()->GetType();
				SP.SpawnEntity->Physicalize(pparams);

				SP.SpawnEntity->EnablePhysics(true);
				SP.SpawnEntity->SetUpdatePolicy(ENTITY_UPDATE_VISIBLE);
				SP.SpawnEntity->Activate(true);

				SmartScriptTable pScript = SP.SpawnEntity->GetScriptTable();
				Script::CallMethod(pScript,"OnReset");
				/*SmartScriptTable pScript = SP.SpawnEntity->GetScriptTable();
				Script::CallMethod(pScript,"OnReset");*/
				//const char * entity_class_name = SP.pEntityClass->GetName();

				/*if(SP.pEntityClass == preLoad.pBasicEntityClass)
				{
					BasicPhysParams BPP;
					ProcessBasicEntity(BPP,SP.XMLIndex);
					PostSpawnPhysics(SP,BPP);
				}

				else if(strcmp(entity_class_name,"Item") == 0)
				{
					BasicPhysParams BPP;
					ProcessItem(BPP,SP.XMLIndex);
					PostSpawnPhysics(SP,BPP);
					CItem *pItem=static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(SP.SpawnEntity->GetId()));
					pItem->Pickalize(true,false);
					pItem->Physicalize(true,true);
					Vec3 dir(0,1,0);
					pItem->Impulse(SP.SpawnEntity->GetPos(),dir,5);
				}

				else if(strcmp(entity_class_name,"Vehicle") == 0)
				{
					ProcessVehicle(SP.XMLIndex);
					PostSpawnVehiclePhysics(SP);
				}

				else if(strcmp(entity_class_name, "AI") == 0)
				{
					AIParams AP;
					ProcessAI(AP,SP.XMLIndex);
					PostSpawnAIPhysics(SP,AP);
				}

				else if(SP.pEntityClass == preLoad.pLightClass)
				{
					BasicPhysParams BPP;
					LightParams LP;
					ExplosiveParams EP;
					ProcessLight(BPP,LP,EP,SP.XMLIndex);
					PostSpawnLight(SP,BPP,LP,EP);
				}

				else if(SP.pEntityClass == preLoad.pDestroyableObjectClass)
				{
					BasicPhysParams BPP;
					ExplosiveParams EP;
					ProcessDestroyableObject(BPP,EP,SP.XMLIndex);
					PostSpawnDestroyableObject(SP,BPP,EP);
				}

				else if(strcmp(entity_class_name,"Ladder") == 0)
				{
					float height;
					ProcessLadder(height,SP.XMLIndex);
					PostSpawnLadder(SP,height);
				}

				else if(SP.pEntityClass == preLoad.pTrap)
				{
					BasicPhysParams BPP;
					ExplosiveParams EP;
					TrapParams TP;
					AIParams AP;
					ProcessTrap(BPP,EP,TP,AP,SP.XMLIndex);
					PostSpawnTrap(SP,BPP,EP,TP,AP);
				}

				else if(SP.pEntityClass == preLoad.pLeakHole)
				{
					BasicPhysParams BPP;
					LeakHoleParams LHP;
					ProcessLeakHole(BPP,LHP,SP.XMLIndex);
					PostSpawnLeakHole(SP,LHP);
				}

				else if(SP.pEntityClass == preLoad.pForceDevice)
				{
					BasicPhysParams BPP;
					ForceDeviceParams FDP;
					ProcessForceDevice(BPP,FDP,SP.XMLIndex);
					PostSpawnForceDevice(SP,BPP,FDP);
				}

				else if(SP.pEntityClass == preLoad.pPowerBall)
				{
					BasicPhysParams BPP;
					PowerBallParams PBP;
					ProcessPowerBall(BPP,PBP,SP.XMLIndex);
					PostSpawnPowerBall(SP,BPP,PBP);
				}

				else if(SP.pEntityClass == preLoad.pForceField)
				{
					BasicPhysParams BPP;
					ForceFieldParams DP;
					ProcessForceField(BPP,DP,SP.XMLIndex);
					PostSpawnForceField(SP,DP);
				}*/

				//CreateSpawnParticle(i);
				SpawnedEntityIdSet.push_back(SP.SpawnEntity->GetId());		//Add to SpawnSet so SystemRobot can delete later.
				ClearSpawnData(SP);
				SpawnProcessMap.erase(iter++);
			}
			else
				++iter;
		}
	}
}

//--------Spawn An Temp Entity--------
void SpawnSystem::Spawn(const int & SIndex /* = -1 */)
{

	SEntitySpawnParams params;
	if(!hit.pt.IsValid())
		hit.pt=Vec3(0,0,0);
	if(curindex != -1)
	{
		bool isSet(false);
		if(SIndex != -1)
		{
			EntityNode = root->getChild(SIndex);
			EntityNode->getAttr("set",isSet);
			if(!isSet)
				Params = EntityNode;
			else
			{
				int maxChild=EntityNode->getChildCount();
				int randomIndex=cry_rand32() % maxChild;
				Params = EntityNode->getChild(randomIndex);
			}
		}
		/*else
		{
			EntityNode = root->getChild(index);
			if(!isSet)
				Params = EntityNode->findChild("params");
			else
				Params = EntityNode;
		}*/
		//XmlString entityclass;
		//Params->getAttr("Class",entityclass);

		//CurrentSpawn.pEntityClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(entityclass);
		//const char * entity_class_name = CurrentSpawn.pEntityClass->GetName();
		Params->getAttr("Archetype", CurrentSpawn.ArchetypeName);
		CurrentSpawn.name = EntityNode->getContent();
		CurrentSpawn.XMLIndex = SIndex;
		params.id = SpawnId;
		params.pArchetype = gEnv->pEntitySystem->LoadEntityArchetype(CurrentSpawn.ArchetypeName);
		params.pPropertiesTable = params.pArchetype->GetProperties();
		params.pPropertiesTable->GetValue("SpawnTime",CurrentSpawn.spawntime);
		params.pPropertiesTable->GetValue("StabilityCost",CurrentSpawn.Utils.stabilitycost);

		//PreProcessModelAndName(SIndex);
		while(gEnv->pEntitySystem->IsIDUsed(SpawnId))
			++SpawnId; 
		/*if(CurrentSpawn.pEntityClass == preLoad.pBasicEntityClass)
		{
			PreProcessModelAndName(SIndex);
			params.pClass=CurrentSpawn.pEntityClass;
			SpawnBasicEntityandItem(hit,params);
		}

		else if(strcmp(entity_class_name,"Item") == 0)
		{
			PreProcessModelAndName(SIndex);
			params.pClass=CurrentSpawn.pEntityClass;
			SpawnBasicEntityandItem(hit,params);
			CItem *pItem=static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(CurrentSpawn.SpawnEntity->GetId()));
			if(pItem)
				pItem->Pickalize(false,false);
		}

		else if(strcmp(entity_class_name,"Vehicle") == 0)
		{
			PreProcessModelAndName(SIndex);
			params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("BasicEntity");
			SpawnVehicle(hit,params);
		}

		else if(strcmp(entity_class_name, "AI") == 0)
		{
			PreProcessModelAndName(SIndex);
			params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("BasicEntity");
			SpawnAI(hit,params);
		}

		else if(CurrentSpawn.pEntityClass == preLoad.pLightClass)
		{
			PreProcessModelAndName(SIndex);
			params.pClass=CurrentSpawn.pEntityClass;
			SpawnLight(hit,params);
		}

		else if(CurrentSpawn.pEntityClass == preLoad.pDestroyableObjectClass)
		{
			PreProcessModelAndName(SIndex);
			params.pClass=CurrentSpawn.pEntityClass;
			SpawnDestroyableObject(hit,params);
		}

		else if(strcmp(entity_class_name,"Ladder") == 0)
		{
			PreProcessModelAndName(SIndex);
			params.pClass=CurrentSpawn.pEntityClass;
			SpawnLadder(hit,params);
		}

		else if(CurrentSpawn.pEntityClass == preLoad.pTrap)
		{
			PreProcessModelAndName(SIndex);
			params.pClass=CurrentSpawn.pEntityClass;
			SpawnTrap(hit,params);
		}

		else if(CurrentSpawn.pEntityClass == preLoad.pLeakHole)
		{
			PreProcessModelAndName(SIndex);
			params.pClass=CurrentSpawn.pEntityClass;
			SpawnLeakHole(hit,params);
		}

		else if(CurrentSpawn.pEntityClass == preLoad.pForceDevice)
		{
			PreProcessModelAndName(SIndex);
			params.pClass=CurrentSpawn.pEntityClass;
			SpawnForceDevice(hit,params);
		}

		else if(CurrentSpawn.pEntityClass == preLoad.pPowerBall)
		{
			PreProcessModelAndName(SIndex);
			params.pClass=CurrentSpawn.pEntityClass;
			SpawnPowerBall(hit,params);
		}

		else if(CurrentSpawn.pEntityClass == preLoad.pForceField)
		{
			PreProcessModelAndName(SIndex);
			params.pClass=CurrentSpawn.pEntityClass;
			SpawnForceField(hit,params);
		}*/
		SpawnTheEntity(hit, params, ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CUSTOM_VIEWDIST_RATIO | ENTITY_FLAG_CALC_PHYSICS);
		ProcessSpawnMat();
		CurrentSpawn.progress=0;
		if(lastindex != -1 && LastSpawn.SpawnEntity)
			CurrentSpawn.SpawnEntity->SetRotation(LastSpawn.SpawnEntity->GetRotation());
		edit=true;

		++SpawnId;
		//Something goes wrong.
		/*if(!CurrentSpawn.SpawnEntity->GetParticleEmitter(50))
		{
			IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Code_System.SpawnEntity");
			CurrentSpawn.SpawnEntity->LoadParticleEmitter(50,pEffect);
		}*/

	}
	else
	{
		StartFunction=false;
		edit=false;
	}
}


//--------Edit Mode--------
void SpawnSystem::editMode(float frameTime)
{
	if(AligntoEntity)
	{
		if(pEntity)
		{
			static const float ResetTime = 0.5;
			if(placedir == eD_Left && last_placedir != placedir)
			{
				CurrentSpawn.SpawnEntity->SetPos(offset_left);
				AABB aabb;
				CurrentSpawn.SpawnEntity->GetWorldBounds(aabb);
				Vec3 diff = aabb.GetCenter() - offset_left;
				CurrentSpawn.SpawnEntity->SetPos(offset_left - diff);
				if(rotationReset_timer > ResetTime)
					CurrentSpawn.SpawnEntity->SetRotation(pEntity->GetRotation());
				last_placedir = eD_Left;
			}
			else if(placedir == eD_Right && last_placedir != placedir)
			{
				CurrentSpawn.SpawnEntity->SetPos(offset_right);
				AABB aabb;
				CurrentSpawn.SpawnEntity->GetWorldBounds(aabb);
				Vec3 diff = aabb.GetCenter() - offset_right;
				CurrentSpawn.SpawnEntity->SetPos(offset_right - diff);
				if(rotationReset_timer > ResetTime)
					CurrentSpawn.SpawnEntity->SetRotation(pEntity->GetRotation());
				last_placedir = eD_Right;
			}
			else if(placedir == eD_Forward && last_placedir != placedir)
			{
				CurrentSpawn.SpawnEntity->SetPos(offset_forward);
				AABB aabb;
				CurrentSpawn.SpawnEntity->GetWorldBounds(aabb);
				Vec3 diff = aabb.GetCenter() - offset_forward;
				CurrentSpawn.SpawnEntity->SetPos(offset_forward - diff);
				gEnv->pRenderer->DrawLabel(aabb.GetCenter(), 1.5f, "center");
				gEnv->pRenderer->DrawLabel(offset_forward - diff, 1.5f, "adjust_fwd");
				if(rotationReset_timer > ResetTime)
					CurrentSpawn.SpawnEntity->SetRotation(pEntity->GetRotation());
				last_placedir = eD_Forward;
			}
			else if(placedir == eD_Backward && last_placedir != placedir)
			{
				CurrentSpawn.SpawnEntity->SetPos(offset_back);
				AABB aabb;
				CurrentSpawn.SpawnEntity->GetWorldBounds(aabb);
				Vec3 diff = aabb.GetCenter() - offset_back;
				CurrentSpawn.SpawnEntity->SetPos(offset_back - diff);
				if(rotationReset_timer > ResetTime)
					CurrentSpawn.SpawnEntity->SetRotation(pEntity->GetRotation());
				last_placedir = eD_Backward;
			}
			else if(placedir == eD_Up && last_placedir != placedir)
			{
				CurrentSpawn.SpawnEntity->SetPos(offset_up);
				AABB aabb;
				CurrentSpawn.SpawnEntity->GetWorldBounds(aabb);
				Vec3 diff = aabb.GetCenter() - offset_up;
				CurrentSpawn.SpawnEntity->SetPos(offset_up - diff);
				if(rotationReset_timer > ResetTime)
					CurrentSpawn.SpawnEntity->SetRotation(pEntity->GetRotation());
				last_placedir = eD_Up;
			}
			else if(placedir == eD_Down && last_placedir != placedir)
			{
				CurrentSpawn.SpawnEntity->SetPos(offset_down);
				AABB aabb;
				CurrentSpawn.SpawnEntity->GetWorldBounds(aabb);
				Vec3 diff = aabb.GetCenter() - offset_down;
				CurrentSpawn.SpawnEntity->SetPos(offset_down - diff);
				if(rotationReset_timer > ResetTime)
					CurrentSpawn.SpawnEntity->SetRotation(pEntity->GetRotation());
				last_placedir = eD_Down;
			}
			else
			{
				//last_placedir = eD_Nothing;
				return;
			}
			rotationReset_timer += frameTime;
		}
	}
	if((edit && !AligntoEntity))
	{
		//Align to the ground if player doesn't manual change Z.
		if(zChange==false)
		{
			Vec3 finalPos=hit.pt;
			if(hits == 0)
				finalPos.z=gEnv->p3DEngine->GetTerrainElevation(hit.pt.x,hit.pt.y);
			CurrentSpawn.SpawnEntity->SetPos(finalPos);
		}
		//Set Z pos if player manual change Z.
		else
		{
			hit.pt.z=CurrentSpawn.SpawnEntity->GetPos().z;
			CurrentSpawn.SpawnEntity->SetPos(hit.pt);
		}
		//Rotate the entity.
		if(last_dir.x != dir.x || last_dir.y != dir.y ||  last_dir.z != dir.z)    //need to change later
		{
			Quat EntityRot=CurrentSpawn.SpawnEntity->GetRotation();
		    Quat rot = Quat::CreateRotationVDir(dir);
			Quat new_rot=rot*Quat::CreateRotationX(DEG2RAD(-90));
			//new_rot.SetRotationZ(EntityRot.GetRotZ());
			CurrentSpawn.SpawnEntity->SetRotation(new_rot);
			last_dir=dir;
		}
	}
	else 
		return;
}	


//--------Process XML Function--------
void SpawnSystem::ProcessBasicEntity(BasicPhysParams &BPP , const int SIndex /* = -1 */)
{
	if(SIndex == -1)
		EntityNode = root->getChild(index);
	else
		EntityNode = root->getChild(SIndex);
	bool isSet(false);
	EntityNode->getAttr("set",isSet);
	if(isSet)
	{
		int maxChild=EntityNode->getChildCount();
		int randomIndex=cry_rand32() % maxChild;
		EntityNode = EntityNode->getChild(randomIndex);
	}
	Params = EntityNode->findChild("params");
	Params->getAttr("Name",CurrentSpawn.Utils.name);
	Params->getAttr("Mass",BPP.mass);
	Params->getAttr("Density",BPP.density);
	//Params->getAttr("nSlot",CurrentSpawn.Utils.nslot);
	Params->getAttr("Type",CurrentSpawn.Utils.phys_type);
	Params->getAttr("Model",CurrentSpawn.model);
	Params->getAttr("SpawnTime",CurrentSpawn.spawntime);
	Params->getAttr("StabilityCost",CurrentSpawn.Utils.stabilitycost);
}


void SpawnSystem::ProcessItem(BasicPhysParams &BPP , const int SIndex /* = -1 */)
{
	if(SIndex == -1)
		EntityNode = root->getChild(index);
	else
		EntityNode = root->getChild(SIndex);

	Params = EntityNode->findChild("params");

	Params->getAttr("Name",CurrentSpawn.Utils.name);
	//Params->getAttr("nSlot",CurrentSpawn.Utils.nslot);
	Params->getAttr("Type",CurrentSpawn.Utils.phys_type);
	Params->getAttr("StabilityCost",CurrentSpawn.Utils.stabilitycost);

	Params->getAttr("Mass",BPP.mass);
	Params->getAttr("Density",BPP.density);

	Params->getAttr("Model",CurrentSpawn.model);
	Params->getAttr("SpawnTime",CurrentSpawn.spawntime);
}


void SpawnSystem::ProcessVehicle(const int SIndex /* = -1 */)
{
	if(SIndex == -1)
		EntityNode = root->getChild(index);
	else
		EntityNode = root->getChild(SIndex);
	Params = EntityNode->findChild("params");

	Params->getAttr("Name",CurrentSpawn.Utils.name);
	//Params->getAttr("nSlot",CurrentSpawn.Utils.nslot);
	Params->getAttr("Type",CurrentSpawn.Utils.phys_type);
	Params->getAttr("StabilityCost",CurrentSpawn.Utils.stabilitycost);

	/*Params->getAttr("Mass",BPP.mass);
	Params->getAttr("Density",BPP.density);*/

	Params->getAttr("Model",CurrentSpawn.model);
	Params->getAttr("SpawnTime",CurrentSpawn.spawntime);

	Params->getAttr("ProjectileType",CurrentSpawn.Utils.projectile_type);
}

void SpawnSystem::ProcessAI(AIParams &AP , const int SIndex /* = -1 */)
{
	if(SIndex == -1)
		EntityNode = root->getChild(index);
	else
		EntityNode = root->getChild(SIndex);
	bool isSet(false);
	EntityNode->getAttr("set",isSet);
	if(isSet)
	{
		int maxChild=EntityNode->getChildCount();
		int randomIndex=cry_rand32() % maxChild;
		EntityNode = EntityNode->getChild(randomIndex);
	}
	Params = EntityNode->findChild("params");

	Params->getAttr("Name",CurrentSpawn.Utils.name);
	//Params->getAttr("nSlot",CurrentSpawn.Utils.nslot);
	Params->getAttr("Type",CurrentSpawn.Utils.phys_type);
	Params->getAttr("StabilityCost",CurrentSpawn.Utils.stabilitycost);

	/*Params->getAttr("Mass",BPP.mass);
	Params->getAttr("Density",BPP.density);*/
	Params->getAttr("AIBehavior",AP.aibehavior);

	Params->getAttr("Model",CurrentSpawn.model);
	Params->getAttr("SpawnTime",CurrentSpawn.spawntime);
}

void SpawnSystem::ProcessLight(BasicPhysParams &BPP , LightParams &LP , ExplosiveParams &EP , const int SIndex /* = -1 */)
{
	if(SIndex == -1)
		EntityNode = root->getChild(index);
	else
		EntityNode = root->getChild(SIndex);
	bool isSet(false);
	EntityNode->getAttr("set",isSet);
	if(isSet)
	{
		int maxChild=EntityNode->getChildCount();
		int randomIndex=cry_rand32() % maxChild;
		EntityNode = EntityNode->getChild(randomIndex);
	}
	Params = EntityNode->findChild("params");

	Params->getAttr("Name",CurrentSpawn.Utils.name);
	//Params->getAttr("nSlot",CurrentSpawn.Utils.nslot);
	Params->getAttr("Type",CurrentSpawn.Utils.phys_type);
	Params->getAttr("StabilityCost",CurrentSpawn.Utils.stabilitycost);

	Params->getAttr("Mass",BPP.mass);
	Params->getAttr("Density",BPP.density);

	Params->getAttr("ModelDestroyed",EP.model_destroyed);
	Params->getAttr("Explode",EP.explode);
	Params->getAttr("Damage",EP.damage);
	Params->getAttr("Effect",EP.effect);
	Params->getAttr("MinPhysRadius",EP.minphysradius);
	Params->getAttr("MinRadius",EP.minradius);
	Params->getAttr("PhysRadius",EP.physradius);
	Params->getAttr("SoundRadius",EP.soundradius);
	Params->getAttr("Pressure",EP.pressure);
	Params->getAttr("Health",EP.health);
	Params->getAttr("Radius",EP.radius);

	Params->getAttr("Light_Radius",LP.lightradius);
	Params->getAttr("LightStyle",LP.lightstyle);
	Params->getAttr("DiffuseMultiplier",LP.diffusemultiplier);
	Params->getAttr("SpecularMultiplier",LP.specularmultiplier);
	Params->getAttr("HDRDynamic",LP.HDRDynamic);
	Params->getAttr("Color_Diffuse",LP.colordiffuse);

	Params->getAttr("Model",CurrentSpawn.model);
	Params->getAttr("SpawnTime",CurrentSpawn.spawntime);
}

void SpawnSystem::ProcessDestroyableObject(BasicPhysParams & BPP , ExplosiveParams &EP , const int SIndex /* = -1 */)
{
	if(SIndex == -1)
		EntityNode = root->getChild(index);
	else
		EntityNode = root->getChild(SIndex);
	bool isSet(false);
	EntityNode->getAttr("set",isSet);
	if(isSet)
	{
		int maxChild=EntityNode->getChildCount();
		int randomIndex=cry_rand32() % maxChild;
		EntityNode = EntityNode->getChild(randomIndex);
	}
	Params = EntityNode->findChild("params");

	Params->getAttr("Name",CurrentSpawn.Utils.name);
	//Params->getAttr("nSlot",CurrentSpawn.Utils.nslot);
	Params->getAttr("Type",CurrentSpawn.Utils.phys_type);
	Params->getAttr("StabilityCost",CurrentSpawn.Utils.stabilitycost);
	Params->getAttr("Model",CurrentSpawn.model);
	Params->getAttr("SpawnTime",CurrentSpawn.spawntime);

	Params->getAttr("Mass",BPP.mass);
	Params->getAttr("Density",BPP.density);

	Params->getAttr("ModelDestroyed",EP.model_destroyed);
	Params->getAttr("Explode",EP.explode);
	Params->getAttr("Damage",EP.damage);
	Params->getAttr("Effect",EP.effect);
	Params->getAttr("MinPhysRadius",EP.minphysradius);
	Params->getAttr("MinRadius",EP.minradius);
	Params->getAttr("PhysRadius",EP.physradius);
	Params->getAttr("Radius",EP.radius);
	Params->getAttr("Pressure",EP.pressure);
	Params->getAttr("Health",EP.health);
}

void SpawnSystem::ProcessLadder(float &height , const int SIndex /* = -1 */ )
{
	if(SIndex == -1)
		EntityNode = root->getChild(index);
	else
		EntityNode = root->getChild(SIndex);
	Params = EntityNode->findChild("params");
	Params->getAttr("Name",CurrentSpawn.Utils.name);
	//Params->getAttr("nSlot",CurrentSpawn.Utils.nslot);
	Params->getAttr("Model",CurrentSpawn.model);
	Params->getAttr("Height",height);
	Params->getAttr("SpawnTime",CurrentSpawn.spawntime);
	Params->getAttr("StabilityCost",CurrentSpawn.Utils.stabilitycost);
}

void SpawnSystem::ProcessTrap(BasicPhysParams & BPP , ExplosiveParams &EP , TrapParams &TP, AIParams &AP, const int SIndex /* = -1 */)
{
	if(SIndex == -1)
		EntityNode = root->getChild(index);
	else
		EntityNode = root->getChild(SIndex);
	bool isSet(false);
	EntityNode->getAttr("set",isSet);
	if(isSet)
	{
		int maxChild=EntityNode->getChildCount();
		int randomIndex=cry_rand32() % maxChild;
		EntityNode = EntityNode->getChild(randomIndex);
	}

	Params = EntityNode->findChild("params");

	Params->getAttr("Name",CurrentSpawn.Utils.name);
	//Params->getAttr("nSlot",CurrentSpawn.Utils.nslot);
	Params->getAttr("Type",CurrentSpawn.Utils.phys_type);
	Params->getAttr("StabilityCost",CurrentSpawn.Utils.stabilitycost);
	Params->getAttr("SpawnTime",CurrentSpawn.spawntime);

	Params->getAttr("ParticleEffect",EP.effect);
	Params->getAttr("MinRadius",EP.minradius);
	Params->getAttr("Radius",EP.radius);
	Params->getAttr("MinPhysRadius",EP.minphysradius);
	Params->getAttr("PhysRadius",EP.physradius);
	Params->getAttr("SoundRadius",EP.soundradius);
	Params->getAttr("Pressure",EP.pressure);
	Params->getAttr("Damage",EP.damage);

	Params->getAttr("EffectScale", TP.effectScale);
	Params->getAttr("isDataLeakage", TP.isDataLeakage);
	Params->getAttr("isSlowDown", TP.isSlowDown);
	Params->getAttr("isDedication", TP.isDedication);
	Params->getAttr("isRecycle", TP.isRecycle);
	Params->getAttr("isDamageCov", TP.isDamageCov);

	Params->getAttr("Faction",AP.faction);
}

void SpawnSystem::ProcessLeakHole(BasicPhysParams & BPP , LeakHoleParams &LHP, const int SIndex /* = -1 */)
{
	if(SIndex == -1)
		EntityNode = root->getChild(index);
	else
		EntityNode = root->getChild(SIndex);
	bool isSet(false);
	EntityNode->getAttr("set",isSet);
	if(isSet)
	{
		int maxChild=EntityNode->getChildCount();
		int randomIndex=cry_rand32() % maxChild;
		EntityNode = EntityNode->getChild(randomIndex);
	}

	Params->getAttr("Name",CurrentSpawn.Utils.name);
	//Params->getAttr("nSlot",CurrentSpawn.Utils.nslot);
	Params->getAttr("Type",CurrentSpawn.Utils.phys_type);
	Params->getAttr("StabilityCost",CurrentSpawn.Utils.stabilitycost);
	Params->getAttr("SpawnTime",CurrentSpawn.spawntime);

	Params = EntityNode->findChild("params");
	Params->getAttr("Mass", BPP.mass);
	Params->getAttr("Radius",LHP.radius);
	Params->getAttr("Damage",LHP.damage);
}

void SpawnSystem::ProcessForceDevice(BasicPhysParams &BPP , ForceDeviceParams &FDP, const int SIndex /* = -1 */)
{
	if(SIndex == -1)
		EntityNode = root->getChild(index);
	else
		EntityNode = root->getChild(SIndex);
	bool isSet(false);
	EntityNode->getAttr("set",isSet);
	if(isSet)
	{
		int maxChild=EntityNode->getChildCount();
		int randomIndex=cry_rand32() % maxChild;
		EntityNode = EntityNode->getChild(randomIndex);
	}

	Params->getAttr("Name",CurrentSpawn.Utils.name);
	//Params->getAttr("nSlot",CurrentSpawn.Utils.nslot);
	Params->getAttr("Type",CurrentSpawn.Utils.phys_type);
	Params->getAttr("StabilityCost",CurrentSpawn.Utils.stabilitycost);
	Params->getAttr("SpawnTime",CurrentSpawn.spawntime);

	Params->getAttr("Mass",BPP.mass);
	Params->getAttr("Density",BPP.density);

	Params->getAttr("Enable",FDP.enable);
	Params->getAttr("HitTimesLimit",FDP.hitTimesLimit);
	Params->getAttr("Damage",FDP.damage);
	Params->getAttr("MinExploRadius",FDP.minExploRadius);
	Params->getAttr("ExploRadius",FDP.exploRadius);
	Params->getAttr("HitTimeInterval",FDP.hitTimeInterval);
	Params->getAttr("Force",FDP.force);
}

void SpawnSystem::ProcessPowerBall(BasicPhysParams &BPP , PowerBallParams &PBP, const int SIndex /* = -1 */)
{
	if(SIndex == -1)
		EntityNode = root->getChild(index);
	else
		EntityNode = root->getChild(SIndex);
	bool isSet(false);
	EntityNode->getAttr("set",isSet);
	if(isSet)
	{
		int maxChild=EntityNode->getChildCount();
		int randomIndex=cry_rand32() % maxChild;
		EntityNode = EntityNode->getChild(randomIndex);
	}

	Params->getAttr("Name",CurrentSpawn.Utils.name);
	//Params->getAttr("nSlot",CurrentSpawn.Utils.nslot);
	Params->getAttr("Type",CurrentSpawn.Utils.phys_type);
	Params->getAttr("StabilityCost",CurrentSpawn.Utils.stabilitycost);
	Params->getAttr("SpawnTime",CurrentSpawn.spawntime);

	Params->getAttr("Mass",BPP.mass);
	Params->getAttr("Density",BPP.density);

	Params->getAttr("Damage",PBP.damage);
	Params->getAttr("PowerUpperBound",PBP.powerUpperBound);
	Params->getAttr("ExploRadius",PBP.exploRadius);
}

void SpawnSystem::ProcessForceField(BasicPhysParams & BPP , ForceFieldParams &DP, const int SIndex /* = -1 */)
{
	if(SIndex == -1)
		EntityNode = root->getChild(index);
	else
		EntityNode = root->getChild(SIndex);
	bool isSet(false);
	EntityNode->getAttr("set",isSet);
	if(isSet)
	{
		int maxChild=EntityNode->getChildCount();
		int randomIndex=cry_rand32() % maxChild;
		EntityNode = EntityNode->getChild(randomIndex);
	}

	Params->getAttr("Name",CurrentSpawn.Utils.name);
	//Params->getAttr("nSlot",CurrentSpawn.Utils.nslot);
	Params->getAttr("Type",CurrentSpawn.Utils.phys_type);
	Params->getAttr("StabilityCost",CurrentSpawn.Utils.stabilitycost);
	Params->getAttr("SpawnTime",CurrentSpawn.spawntime);

	Params->getAttr("Radius",DP.radius);
	Params->getAttr("AccScale",DP.accScale);
}

//--------Spawn--------
void SpawnSystem::SpawnBasicEntityandItem(const ray_hit &hit,SEntitySpawnParams& params)
{
	SpawnTheEntity(hit, params, ENTITY_FLAG_CLIENT_ONLY | ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CALC_PHYSICS | ENTITY_FLAG_AI_HIDEABLE);
}


void SpawnSystem::SpawnVehicle(const ray_hit &hit,SEntitySpawnParams& params)
{
	SpawnTheEntity(hit, params, ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CUSTOM_VIEWDIST_RATIO | ENTITY_FLAG_AI_HIDEABLE | ENTITY_FLAG_CALC_PHYSICS);
}

void SpawnSystem::SpawnAI(const ray_hit &hit,SEntitySpawnParams& params)
{
	SpawnTheEntity(hit, params, ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CALC_PHYSICS);
	/*GetMat();
	params.vPosition = hit.pt;
	if(LastQuat.IsValid())
		params.qRotation = LastQuat;
	params.vScale(0.16,0.16,0.16);
	params.sName=CurrentSpawn.Utils.name;
	params.nFlags =  ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CALC_PHYSICS;
	if(CurrentSpawn.SpawnEntity)
		gEnv->pEntitySystem->RemoveEntity(CurrentSpawn.SpawnEntity->GetId(),true);
	CurrentSpawn.SpawnEntity=gEnv->pEntitySystem->SpawnEntity(params);
	CurrentSpawn.defaultmat=CurrentSpawn.SpawnEntity->GetMaterial();
	if(zChange)
	{
		Vec3 temp=CurrentSpawn.SpawnEntity->GetPos();
		temp.z=LastSpawn.SpawnEntity->GetPos().z;
		CurrentSpawn.SpawnEntity->SetPos(temp);
	}
	CurrentSpawn.SpawnEntity->EnablePhysics(false);
	CurrentSpawn.SpawnEntity->LoadGeometry(0, "objects/props/misc/colossal_statue/colossus.cgf");*/
}

void SpawnSystem::SpawnLight(const ray_hit &hit,SEntitySpawnParams& params)
{
	SpawnTheEntity(hit, params, ENTITY_FLAG_CLIENT_ONLY | ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CALC_PHYSICS);
}

void SpawnSystem::SpawnDestroyableObject(const ray_hit &hit,SEntitySpawnParams& params)
{
	SpawnTheEntity(hit, params, ENTITY_FLAG_CLIENT_ONLY | ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CALC_PHYSICS);
}

void SpawnSystem::SpawnLadder(const ray_hit &hit,SEntitySpawnParams& params)
{
	SpawnTheEntity(hit, params, ENTITY_FLAG_CLIENT_ONLY | ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CALC_PHYSICS);
}

void SpawnSystem::SpawnTrap(const ray_hit &hit,SEntitySpawnParams& params)
{
	SpawnTheEntity(hit, params, ENTITY_FLAG_CLIENT_ONLY | ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CALC_PHYSICS);
}

void SpawnSystem::SpawnLeakHole(const ray_hit &hit,SEntitySpawnParams& params)
{
	SpawnTheEntity(hit, params, ENTITY_FLAG_CLIENT_ONLY | ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CALC_PHYSICS);
}

void SpawnSystem::SpawnForceDevice(const ray_hit &hit,SEntitySpawnParams& params)
{
	SpawnTheEntity(hit, params, ENTITY_FLAG_CLIENT_ONLY | ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CALC_PHYSICS);
}

void SpawnSystem::SpawnPowerBall(const ray_hit &hit,SEntitySpawnParams& params)
{
	SpawnTheEntity(hit, params, ENTITY_FLAG_CLIENT_ONLY | ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CALC_PHYSICS);
}

void SpawnSystem::SpawnForceField(const ray_hit &hit,SEntitySpawnParams& params)
{
	SpawnTheEntity(hit, params, ENTITY_FLAG_CLIENT_ONLY | ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CALC_PHYSICS);
}


void SpawnSystem::SpawnTheEntity(const ray_hit &hit,SEntitySpawnParams& params, int spawnflag)
{
	GetMat();
	params.vPosition = hit.pt;
	if(LastQuat.IsValid())
		params.qRotation = LastQuat;
	params.vScale(1,1,1);
	params.sName=CurrentSpawn.Utils.name;
	params.nFlags = spawnflag;
	/*if(CurrentSpawn.SpawnEntity)
		gEnv->pEntitySystem->RemoveEntity(CurrentSpawn.SpawnEntity->GetId(),true);*/
	CurrentSpawn.SpawnEntity=gEnv->pEntitySystem->SpawnEntity(params);
	CurrentSpawn.defaultmat=CurrentSpawn.SpawnEntity->GetMaterial();
	if(zChange)
	{
		Vec3 temp=CurrentSpawn.SpawnEntity->GetPos();
		temp.z=LastSpawn.SpawnEntity->GetPos().z;
		CurrentSpawn.SpawnEntity->SetPos(temp);
	}
	CurrentSpawn.SpawnEntity->EnablePhysics(false);
	CurrentSpawn.SpawnEntity->Activate(false);
	//CurrentSpawn.SpawnEntity->LoadGeometry(0, CurrentSpawn.model);
}

//--------Process Function--------
void SpawnSystem::ProcessSpawnMat()
{
	if(CurrentSpawn.spawnmat)
		CurrentSpawn.SpawnEntity->SetMaterial(CurrentSpawn.spawnmat);
}


void SpawnSystem::PostSpawnPhysics(Spawns &SP , const BasicPhysParams &BPP)
{
	SEntityPhysicalizeParams pparams;
	pparams.mass = BPP.mass;
	pparams.density = BPP.density;
	pparams.nSlot= -1;
	pparams.type = SP.Utils.phys_type;
	SP.SpawnEntity->Physicalize(pparams);
	SP.SpawnEntity->EnablePhysics(true);
	SP.SpawnEntity->SetUpdatePolicy(ENTITY_UPDATE_ALWAYS);

	SmartScriptTable root=SP.SpawnEntity->GetScriptTable();
	SmartScriptTable Properties;
	root->GetValue("Properties",Properties);
	Properties->SetValue("ProjectileType",SP.Utils.projectile_type);
}


void SpawnSystem::PostSpawnVehiclePhysics(Spawns &SP)
{
	SEntitySpawnParams vparams;
	Vec3 tVec=SP.SpawnEntity->GetPos();
	Quat rot=SP.SpawnEntity->GetRotation();
	gEnv->pEntitySystem->RemoveEntity(SP.SpawnEntity->GetId(),true);
	vparams.vPosition = tVec;
	vparams.vScale(1,1,1);
	vparams.sName=SP.Utils.name;
	vparams.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass(SP.Utils.name);
	vparams.nFlags = ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CUSTOM_VIEWDIST_RATIO | ENTITY_FLAG_AI_HIDEABLE | ENTITY_FLAG_CALC_PHYSICS;
	SP.SpawnEntity=gEnv->pEntitySystem->SpawnEntity(vparams);
	SP.SpawnEntity->SetRotation(rot);
	IVehicle *pVehicle=g_pGame->GetIGameFramework()->GetIVehicleSystem()->GetVehicle(SP.SpawnEntity->GetId());
	pVehicle->NeedsUpdate(IVehicle::eVUF_AwakePhysics);
	int partcount=pVehicle->GetPartCount();
	Matrix34 tm=SP.SpawnEntity->GetLocalTM();
	for(int t=0;t<partcount;t++)
	{
		pVehicle->GetPart(t)->Physicalize();
	}
	SP.SpawnEntity->EnablePhysics(true);
	SP.SpawnEntity->SetUpdatePolicy(ENTITY_UPDATE_ALWAYS);
}


void SpawnSystem::PostSpawnAIPhysics(Spawns &SP , const AIParams &AP)
{
	SEntitySpawnParams aparams;
	Vec3 tVec=SP.SpawnEntity->GetPos();
	Quat rot=SP.SpawnEntity->GetRotation();
	gEnv->pEntitySystem->RemoveEntity(SP.SpawnEntity->GetId(),true);
	aparams.vPosition = tVec;
	aparams.vScale(1,1,1);
	aparams.sName=SP.Utils.name;
	aparams.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass(SP.Utils.name);
	aparams.nFlags = ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CUSTOM_VIEWDIST_RATIO  | ENTITY_FLAG_CALC_PHYSICS;
	IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Code_System.SpawnAI");
	if(pEffect)
		pEffect->Spawn(true,IParticleEffect::ParticleLoc(aparams.vPosition,dir));
	SP.SpawnEntity=gEnv->pEntitySystem->SpawnEntity(aparams);
	SP.SpawnEntity->SetRotation(rot);
	SmartScriptTable pScript=SP.SpawnEntity->GetScriptTable();
	SmartScriptTable props;
	pScript->GetValue("Properties",props);
	props->SetValue("esModularBehaviorTree",AP.aibehavior.c_str());
	Script::CallMethod(pScript,"Reset");
	SP.SpawnEntity->EnablePhysics(true);
	SP.SpawnEntity->SetUpdatePolicy(ENTITY_UPDATE_ALWAYS);
}

void SpawnSystem::PostSpawnLight(Spawns &SP , const BasicPhysParams &BPP , const LightParams &LP , const ExplosiveParams &EP)
{
	PostSpawnPhysics(SP,BPP);
	SmartScriptTable root=SP.SpawnEntity->GetScriptTable();
	SmartScriptTable Properties;
	SmartScriptTable ModelParams;;
	SmartScriptTable Damage;
	SmartScriptTable PropertiesInstance;
	SmartScriptTable Explosion;
	SmartScriptTable Health;
	SmartScriptTable LightParams;
	SmartScriptTable Style;
	SmartScriptTable Color;
	string teffect=EP.effect;
	string tmodel;
	string tmodel_destroyed;
	tmodel=SP.model;
	tmodel_destroyed=EP.model_destroyed;
	//Init Table
	root->GetValue("Properties",Properties);
	root->GetValue("PropertiesInstance",PropertiesInstance);
	Properties->GetValue("Explosion",Explosion);
	Properties->GetValue("Damage",Damage);
	Properties->GetValue("Health",Health);
	Properties->GetValue("Model",ModelParams);
	PropertiesInstance->GetValue("LightProperties_Base",LightParams);
	Properties->SetValue("ProjectileType",SP.Utils.projectile_type);
	LightParams->GetValue("Style",Style);
	LightParams->GetValue("Color",Color);
	//Set Params
	//Model Params
	ModelParams->SetValue("object_Model",tmodel.c_str()); 
	ModelParams->SetValue("object_ModelDestroyed",tmodel_destroyed.c_str()); 
	//Explosion Params
	Explosion->SetValue("Effect",teffect.c_str()); 
	Explosion->SetValue("MinRadius",EP.minradius);
	Explosion->SetValue("Radius",EP.radius);
	Explosion->SetValue("MinPhysRadius",EP.minphysradius);
	Explosion->SetValue("PhysRadius",EP.physradius);
	Explosion->SetValue("Pressure",EP.pressure);
	Explosion->SetValue("Damage",EP.damage);
	//Light Params
	LightParams->SetValue("Radius",LP.lightradius);
	Style->SetValue("nLightStyle",LP.lightstyle);
	Color->SetValue("fDiffuseMultiplier",LP.diffusemultiplier);
	Color->SetValue("fSpecularMultiplier",LP.specularmultiplier);
	Color->SetValue("fHDRDynamic",LP.HDRDynamic);
	Color->SetValue("clrDiffuse",LP.colordiffuse);
	//Health Params
	Damage->SetValue("bExplode",(int)EP.explode);
	Damage->SetValue("Health",EP.health);
	Health->SetValue("bInvulnerable",false);
	Script::CallMethod(root,"Reload");
}

void SpawnSystem::PostSpawnDestroyableObject(Spawns &SP , const BasicPhysParams & BPP , const ExplosiveParams &EP)
{
	PostSpawnPhysics(SP,BPP);
	SmartScriptTable root=SP.SpawnEntity->GetScriptTable();
	SmartScriptTable Properties;
	SmartScriptTable Explosion;
	SmartScriptTable Health;
	string teffect=EP.effect;
	string tmodel;
	string tmodel_destroyed;
	tmodel=SP.model;
	tmodel_destroyed=EP.model_destroyed;
	root->GetValue("Properties",Properties);
	Properties->GetValue("Health",Health);
	if(EP.explode)
		Properties->SetValue("bExplode",1);
	else
		Properties->SetValue("bExplode",0);
	Properties->SetValue("object_Model",tmodel.c_str());
	Properties->SetValue("object_ModelDestroyed",tmodel_destroyed.c_str());
	Properties->GetValue("Explosion",Explosion);
	Properties->SetValue("ProjectileType",SP.Utils.projectile_type);

	Explosion->SetValue("Effect",teffect.c_str()); 
	Explosion->SetValue("MinRadius",EP.minradius);
	Explosion->SetValue("Radius",EP.radius);
	Explosion->SetValue("MinPhysRadius",EP.minphysradius);
	Explosion->SetValue("PhysRadius",EP.physradius);
	Explosion->SetValue("SoundRadius",EP.soundradius);
	Explosion->SetValue("Pressure",EP.pressure);
	Explosion->SetValue("Damage",EP.damage);
	Health->SetValue("MaxHealth",EP.health);
	Health->SetValue("bInvulnerable",0);
	Script::CallMethod(root,"OnReset");
}


void SpawnSystem::PostSpawnLadder(Spawns &SP , float height)
{
	SEntityPhysicalizeParams pparams;
	pparams.mass = -1;
	pparams.density = -1;
	pparams.nSlot= -1;
	pparams.type = 1;
	SP.SpawnEntity->Physicalize(pparams);
	SP.SpawnEntity->EnablePhysics(true);
	SP.SpawnEntity->SetUpdatePolicy(ENTITY_UPDATE_ALWAYS);
	SmartScriptTable root=SP.SpawnEntity->GetScriptTable();
	SmartScriptTable props;
	root->GetValue("Properties",props);
	props->SetValue("fileModel",SP.model.c_str());
	props->SetValue("height",height);
	Script::CallMethod(root,"OnPropertyChange");
	props->SetValue("ProjectileType",SP.Utils.projectile_type);
	//May add more staff
}

void SpawnSystem::PostSpawnTrap(Spawns &SP, const BasicPhysParams & BPP , const ExplosiveParams &EP , const TrapParams &TP, const AIParams &AP)
{
	SEntityPhysicalizeParams pparams;
	pparams.mass = -1;
	pparams.density = -1;
	pparams.nSlot= -1;
	pparams.type = 1;
	SP.SpawnEntity->Physicalize(pparams);
	SP.SpawnEntity->EnablePhysics(true);
	SP.SpawnEntity->SetUpdatePolicy(ENTITY_UPDATE_ALWAYS);
	SmartScriptTable root=SP.SpawnEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable explosion;
	SmartScriptTable function_props;

	root->GetValue("Properties",props);
	props->GetValue("Explosion",explosion);
	props->GetValue("FunctionParams",function_props);
	props->SetValue("ProjectileType",SP.Utils.projectile_type);

	explosion->SetValue("Effect",EP.effect.c_str());
	explosion->SetValue("MinRadius",EP.minradius);
	explosion->SetValue("Radius",EP.radius);
	explosion->SetValue("MinPhysRadius",EP.minphysradius);
	explosion->SetValue("PhysRadius",EP.physradius);
	explosion->SetValue("SoundRadius",EP.soundradius);
	explosion->SetValue("Pressure",EP.pressure);
	explosion->SetValue("Damage",EP.damage);

	function_props->SetValue("EffectScale", TP.effectScale);
	function_props->SetValue("isDataLeakage", TP.isDataLeakage);
	function_props->SetValue("isSlowDown", TP.isSlowDown);
	function_props->SetValue("isDedication", TP.isDedication);
	function_props->SetValue("isRecycle", TP.isRecycle);
	function_props->SetValue("isDamageCov", TP.isDamageCov);

	props->SetValue("esFaction",AP.faction.c_str());

	Script::CallMethod(root,"OnReset");

	CTrap* pTrap = static_cast<CTrap*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(SP.SpawnEntity->GetId(), "Trap"));
	pTrap->ReadScript();
}

void SpawnSystem::PostSpawnLeakHole(Spawns &SP , const LeakHoleParams &LHP)
{
	SmartScriptTable root=SP.SpawnEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable function_props;

	root->GetValue("Properties",props);
	props->GetValue("FunctionParams",function_props);
	props->SetValue("ProjectileType",SP.Utils.projectile_type);

	function_props->SetValue("Radius",LHP.radius);
	function_props->SetValue("Damage",LHP.damage);
	function_props->SetValue("Cd",LHP.damage);

	Script::CallMethod(root,"OnReset");
	CLeakHole* pLeakHole = static_cast<CLeakHole*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(SP.SpawnEntity->GetId(), "LeakHole"));
	pLeakHole->ReadScript();

	SEntityPhysicalizeParams pparams;
	pparams.mass = -1;
	pparams.density = -1;
	pparams.nSlot= -1;
	pparams.type = 1;
	SP.SpawnEntity->Physicalize(pparams);
	SP.SpawnEntity->EnablePhysics(true);
	SP.SpawnEntity->SetUpdatePolicy(ENTITY_UPDATE_ALWAYS);
}

void SpawnSystem::PostSpawnForceDevice(Spawns &SP,const BasicPhysParams & BPP, const ForceDeviceParams &FDP)
{
	SmartScriptTable root=SP.SpawnEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable function_props;

	root->GetValue("Properties",props);
	props->GetValue("FunctionParams",function_props);
	props->SetValue("ProjectileType",SP.Utils.projectile_type);

	function_props->SetValue("Enable",FDP.enable);
	function_props->SetValue("HitTimesLimit",FDP.hitTimesLimit);
	function_props->SetValue("Damage",FDP.damage);
	function_props->SetValue("ExploRadius",FDP.exploRadius);
	function_props->SetValue("Force",FDP.force);

	Script::CallMethod(root,"OnReset");
	CForceDevice* pForceDevice = static_cast<CForceDevice*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(SP.SpawnEntity->GetId(), "ForceDevice"));
	pForceDevice->ReadScript();

	PostSpawnPhysics(SP,BPP);
}

void SpawnSystem::PostSpawnPowerBall(Spawns &SP,const BasicPhysParams & BPP, const PowerBallParams &PBP)
{
	PostSpawnPhysics(SP,BPP);
	SmartScriptTable root=SP.SpawnEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable function_props;

	root->GetValue("Properties",props);
	props->GetValue("FunctionParams",function_props);
	props->SetValue("ProjectileType",SP.Utils.projectile_type);

	function_props->SetValue("Damage",PBP.damage);
	function_props->SetValue("PowerUpperBound",PBP.powerUpperBound);
	function_props->SetValue("ExploRadius",PBP.exploRadius);

	Script::CallMethod(root,"OnReset");
	CPowerBall* pPowerBall = static_cast<CPowerBall*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(SP.SpawnEntity->GetId(), "PowerBall"));
	pPowerBall->ReadScript();
}

void SpawnSystem::PostSpawnForceField(Spawns &SP,const ForceFieldParams &DP)
{
	SEntityPhysicalizeParams pparams;
	pparams.mass = -1;
	pparams.density = -1;
	pparams.nSlot= -1;
	pparams.type = 1;
	SP.SpawnEntity->Physicalize(pparams);
	SP.SpawnEntity->EnablePhysics(true);
	SP.SpawnEntity->SetUpdatePolicy(ENTITY_UPDATE_ALWAYS);

	SmartScriptTable root=SP.SpawnEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable function_props;

	root->GetValue("Properties",props);
	props->GetValue("FunctionParams",function_props);
	props->SetValue("ProjectileType",SP.Utils.projectile_type);

	function_props->SetValue("Radius",DP.radius);
	function_props->SetValue("AccScale",DP.accScale);

	Script::CallMethod(root,"OnReset");
	CPowerBall* pPowerBall = static_cast<CPowerBall*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(SP.SpawnEntity->GetId(), "PowerBall"));
	pPowerBall->ReadScript();
}

void SpawnSystem::GetMat()
{
	defspawnmat=gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("spawn");
	lockedmat=gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("locked");
	CurrentSpawn.spawnmat=gEnv->p3DEngine->GetMaterialManager()->CloneMaterial(defspawnmat,0);
	CurrentSpawn.lmat=gEnv->p3DEngine->GetMaterialManager()->CloneMaterial(lockedmat,0);
}


void SpawnSystem::ClearSpawnData(Spawns &SP)
{
	IParticleEmitter *pEmitter=NULL;
	if(SP.SpawnEntity)
	{
		pEmitter=SP.SpawnEntity->GetParticleEmitter(50);
		if(pEmitter)
			gEnv->pParticleManager->DeleteEmitter(pEmitter);
		SP.SpawnEntity=NULL;
		SP.model="";
		SP.defaultmat=NULL;
		SP.spawnmat=NULL;
		SP.lmat=NULL;
		SP.timer=0;
		SP.rate=0;
		SP.spawntime=0;
		SP.amount=50;
		SP.Utils.stabilitycost=0;
		SP.progress=0;
	}
}

void SpawnSystem::ReInit()
{
	hits = 0;
	dir(0.0f,0.0f,1.0f);
	StartFunction = false;
	locked=false;
	root = gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
	index=0;
	//Maxnumber=5;
	defspawnmat=gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("spawn");
	lockedmat=gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("locked");
	zChange=false;
	query_flag=ent_all;
	pRenderer=gEnv->pRenderer;
}


void SpawnSystem::Shutdown()
{
	SetStartFunction(false);
	Setedit(false);
	SetZchange(false);
	//pEntity=NULL;
	last_placedir = eD_Nothing;
	//AligntoEntity=false;
	//RestoreInventory();
	if(CurrentSpawn.SpawnEntity)
	{
		gEnv->pEntitySystem->RemoveEntity(CurrentSpawn.SpawnEntity->GetId(),true);
		CurrentSpawn.SpawnEntity=NULL;
	}
}

//Need more development
const short SpawnSystem::GetNearestDir(Vec3 &pos)
{
	Vec3 Length_Vec=Vec3(pos.x-aabb.GetCenter().x,pos.y-aabb.GetCenter().y,pos.z-aabb.GetCenter().z);
	float z_up=static_cast<float>(aabb.GetCenter().z+aabb.GetSize().z/2);
	float z_down=static_cast<float>(aabb.GetCenter().z-aabb.GetSize().z/2);
	short direction(0);
	if(Length_Vec.GetLengthFast() > aabb.GetRadius()*2)
	{
		pEntity = NULL;
		return eD_Nothing;
	}
	if(pos.z - z_up > 0.01)
	{
		pos.z -= aabb.GetSize().z/2;
		offset_up.z -= aabb.GetSize().z/2;
	}

	float left_dis=(pos-aabb.GetCenter()+right_dir*localaabb.GetSize().x*0.5).GetLengthSquared();
	float right_dis=(pos-aabb.GetCenter()+left_dir*localaabb.GetSize().x*0.5).GetLengthSquared();
	float forward_dis=(pos-aabb.GetCenter()+back_dir*localaabb.GetSize().y*0.5).GetLengthSquared();
	float backward_dis=(pos-aabb.GetCenter()+forward_dir*localaabb.GetSize().y*0.5).GetLengthSquared();
	float up_dis=(pos-offset_up).GetLengthSquared();
	float down_dis=(pos-offset_down).GetLengthSquared();

	float min1=left_dis-right_dis < 0 ? left_dis : right_dis;
	float min2=forward_dis-backward_dis < 0 ? forward_dis : backward_dis;
	float min3=up_dis-down_dis < 0 ? up_dis : down_dis;
	float min_final_1=min1-min2 < 0 ? min1 : min2;
	float min_final=min_final_1-min3 < 0 ? min_final_1 : min3;



	if(min_final == left_dis)
		direction = eD_Left;
	else if(min_final == right_dis)
		direction = eD_Right;
	else if(min_final == forward_dis)
		direction = eD_Forward;
	else if(min_final == backward_dis)
		direction = eD_Backward;
	else if(min_final == up_dis)
		direction = eD_Up;
	else if(min_final == down_dis)
		direction= eD_Down;
	return direction;
}

void SpawnSystem::CreateSpawnParticle(int index)
{
	IEntity *pEntity=gEnv->pEntitySystem->FindEntityByName("Spawn_Particle"+index);
	if(!pEntity)
	{
		SEntitySpawnParams params;
		params.sName="Spawn_Particle"+index;
		params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
		params.vScale=Vec3(1,1,1);
		params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
		params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
		pEntity=gEnv->pEntitySystem->SpawnEntity(params);
		SmartScriptTable proot=pEntity->GetScriptTable();
		SmartScriptTable pprops;
		proot->GetValue("Properties",pprops);
		pprops->SetValue("ParticleEffect","Code_System.WarpSpawn_2");
		Script::CallMethod(proot,"OnReset");
	}
	else
	{
		pEntity->Hide(false);
		SmartScriptTable proot=pEntity->GetScriptTable();
		Script::CallMethod(proot,"OnReset");
	}
	CurrentSpawn.SpawnEntity->AttachChild(pEntity);
}

void SpawnSystem::PreProcessModelAndName(int SIndex /* = -1 */)
{
	if(SIndex == -1)
		EntityNode = root->getChild(index);
	else
		EntityNode = root->getChild(SIndex);
	bool isSet(false);
	EntityNode->getAttr("set",isSet);
	if(isSet)
	{
		int maxChild=EntityNode->getChildCount();
		int randomIndex=cry_rand32() % maxChild;
		EntityNode = EntityNode->getChild(randomIndex);
	}
	Params = EntityNode->findChild("params");
	Params->getAttr("Name",CurrentSpawn.Utils.name);
	//Params->getAttr("nSlot",CurrentSpawn.Utils.nslot);
	Params->getAttr("Type",CurrentSpawn.Utils.phys_type);
	Params->getAttr("Model",CurrentSpawn.model);
	Params->getAttr("SpawnTime",CurrentSpawn.spawntime);
	Params->getAttr("StabilityCost",CurrentSpawn.Utils.stabilitycost);
	Params->getAttr("ProjectileType",CurrentSpawn.Utils.projectile_type);
}

void SpawnSystem::AddtoSpawnProcessMap(const Spawns &SP)
{
	if(!SpawnProcessMap.count(SP.SpawnEntity->GetId()))
	{
		SpawnProcessMap[SP.SpawnEntity->GetId()] = SP;
		StateManager::theStateManager().AddProgressState(SP.SpawnEntity->GetId(), SP.spawntime);
	}
}

void SpawnSystem::GetEntityDir(IEntity *pEntity)
{
	pEntity->GetWorldBounds(aabb);
	pEntity->GetLocalBounds(localaabb);
	forward_dir=pEntity->GetForwardDir(); 
	Matrix34 mat = pEntity->GetWorldTM();
	Vec3 UpDir = Vec3(-mat.m20,-mat.m21,mat.m22);

	left_dir = forward_dir.Cross(UpDir);

	left_dir.normalize();
	right_dir=-left_dir;
	right_dir.normalize();
	hit_dir.normalize();
	back_dir=-forward_dir;

	/*gEnv->pRenderer->DrawLabel(aabb.GetCenter() + forward_dir, 1.5f, "fwd");
	gEnv->pRenderer->DrawLabel(aabb.GetCenter() + back_dir, 1.5f, "back");
	gEnv->pRenderer->DrawLabel(aabb.GetCenter() + left_dir, 1.5f, "left");
	gEnv->pRenderer->DrawLabel(aabb.GetCenter() + right_dir, 1.5f, "right");*/
}

void SpawnSystem::CalculateOffset(IEntity *pEntity, AABB &localAABB)
{
	offset_left=aabb.GetCenter()+left_dir*float(localaabb.GetSize().x+manual_offset);
	offset_right=aabb.GetCenter()+right_dir*float(localaabb.GetSize().x+manual_offset);
	offset_forward=aabb.GetCenter()+forward_dir*float(localaabb.GetSize().y+manual_offset);
	offset_back=aabb.GetCenter()+back_dir*float(localaabb.GetSize().y+manual_offset);
	Matrix34 mat = pEntity->GetWorldTM();
	Vec3 UpDir = Vec3(-mat.m20,-mat.m21,mat.m22);
	offset_up=aabb.GetCenter()+UpDir*localaabb.GetSize().z;
	offset_down=aabb.GetCenter()-UpDir*localaabb.GetSize().z;

	placedir=GetNearestDir(hit.pt);

	/*gEnv->pRenderer->DrawLabel(offset_forward, 1.5f, "fwd");
	gEnv->pRenderer->DrawLabel(offset_back + back_dir, 1.5f, "back");
	gEnv->pRenderer->DrawLabel(offset_left + left_dir, 1.5f, "left");
	gEnv->pRenderer->DrawLabel(offset_right + right_dir, 1.5f, "right");*/
}

void SpawnSystem::RemoveFromSpawnEntitySet(EntityId Id)
{
	if(SpawnedEntityIdSet.empty() || !pEntity)
		return;
	std::list<EntityId>::iterator iter = find(SpawnedEntityIdSet.begin(),SpawnedEntityIdSet.end(),pEntity->GetId());
	if(iter != SpawnedEntityIdSet.end())
	{
		SpawnedEntityIdSet.erase(iter);
	}
}