#ifndef _MAPINDICATOR_H_
#define _MAPINDICATOR_H_

#include "IFlashUI.h"

class Mapindicator
{
private:
	std::set<EntityId> enemy_indactors;
	IUIElement* pElement;
public:
	Mapindicator()
	{
		pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	}
	~Mapindicator(){}

	void AddEnemyIndicator(EntityId enemyId);
	void RemoveEnemyIndicator(EntityId enemyId);
	static Mapindicator &theIndicator(){static Mapindicator indicator; return indicator;}
	void Update(float frameTime);
};
#endif