#include "StdAfx.h"
#include "BasicRayCast.h"
#include "Weapon.h"
#include "MicroSystem/ProbeManager.h"

void BasicRayCast::Update(float frametime)
{
	if(g_pGame->GetIGameFramework()->IsGamePaused() || g_pGame->GetIGameFramework()->IsEditing())
	{
		pEntity = NULL;
		pLastEntity=NULL;
	}
	ConnectMode *pCM=g_pGame->GetConnectMode();
	if(pCM->GetMode() == eConnectMode::eCMode::Normal || pCM->GetMode() == eConnectMode::eCMode::Deep && 
		( pCM->GetLevel() == eConnectMode::eCLevel::eCLevel_None || pCM->GetLevel() == eConnectMode::eCLevel::eCLevel_ChooseSystem))
	{
		pSelfActor=g_pGame->GetIGameFramework()->GetClientActor();
		if(pSelfActor)
		{
			pActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
			pSelf=pSelfActor->GetEntity();
			pMC=pSelfActor->GetMovementController();
			pMC->GetMovementState(info);
			if(!ProbeManager::Instance().IsProbeActived())
			{
				pSelfPhysics=pSelf->GetPhysics();
				hits = pWorld->RayWorldIntersection(info.eyePosition,info.eyeDirection*distance,query_flag,rayFlags,&hit,1,&pSelfPhysics,1);
			}
			//For Probe RayCast
			else
			{
				pSelfPhysics=ProbeManager::Instance().GetProbe()->GetPhysics();
				hits = pWorld->RayWorldIntersection(ProbeManager::Instance().GetScanPos(),info.eyeDirection*ProbeManager::Instance().GetProbeRadius(),query_flag,rayFlags,&hit,1,&pSelfPhysics,1);
			}
			//CryLogAlways("%.2f %.2f %.2f",hit.n.x,hit.n.y,hit.n.z);
			if(hit.pCollider)
			{
				pEntity=(IEntity*)hit.pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);
				if(pEntity == pSelf)
				{
					pEntity = NULL;
					return;
				}
				if(pEntity)
				{
					CActor *pActor = (CActor*)g_pGame->GetGameRules()->GetActorByEntityId(pEntity->GetId());
					OBB obb;
					AABB aabb;
					GetEntity()->GetLocalBounds(aabb);
					aabb.Expand(Vec3(aabb.GetRadius() / 30));
					obb.SetOBBfromAABB(GetEntity()->GetRotation(), aabb);
					Vec3 center = GetEntity()->GetPos();
					gEnv->pRenderer->GetIRenderAuxGeom()->DrawOBB( obb, center, false, Col_Gray, eBBD_Faceted);		
					/*IEntityRenderProxy *proxy=static_cast<IEntityRenderProxy*>(pEntity->GetProxy(ENTITY_PROXY_RENDER));
					if(proxy)
						proxy->SetHUDSilhouettesParams(80,205,255,255);*/
					if(pActor)
					{
						IAIObject *pObject = pEntity->GetAI();
						if(pObject)
						{
							if(pObject->IsHostile(pSelf->GetAI(),false))
							{
								SetUIAimPointType(2);
								/*Vec3 EntityPos = pEntity->GetPos();
								if(!pActor->IsDead() && !pActor->IsFallen())
								{
									AABB aabb;
									pEntity->GetLocalBounds(aabb);
									EntityPos.z += aabb.GetRadius()*2;
								}
								Vec3 screenPos;
								CCamera camera = gEnv->pSystem->GetViewCamera();
								bool onScreen = camera.Project(EntityPos, screenPos, Vec2i(0,0), Vec2i(1920, 1080));

								SetUIEnemyState(screenPos.x, screenPos.y, onScreen, 1, pActor->GetHealth(), pActor->GetMaxHealth(), pEntity->GetClass()->GetName());*/
							}
						}
					}
					else
					{
						SetUIAimPointType(1);
						//EnableEnemyState(false,1);
					}
				}
			}
			if(!pEntity)
			{
				int Id = CommonUsage::GetNerestEntityIdInBox(hit.pt,5,ent_living);
				if(!Id || Id == g_pGame->GetClientActorId())
					Id = CommonUsage::GetNerestEntityIdInBox(hit.pt,5,ent_rigid | ent_static | ent_sleeping_rigid);
				pEntity=gEnv->pEntitySystem->GetEntity(Id);
				//Draw Lock Effect
				if(pEntity)
				{
					OBB obb;
					AABB aabb;
					GetEntity()->GetLocalBounds(aabb);
					aabb.Expand(Vec3(aabb.GetRadius() / 30));
					obb.SetOBBfromAABB(GetEntity()->GetRotation(), aabb);
					Vec3 center = GetEntity()->GetPos();
					gEnv->pRenderer->GetIRenderAuxGeom()->DrawOBB( obb, center, false, Col_Gray, eBBD_Faceted);		
					/*IEntityRenderProxy *proxy=static_cast<IEntityRenderProxy*>(pEntity->GetProxy(ENTITY_PROXY_RENDER));
					if(proxy)
						proxy->SetHUDSilhouettesParams(80,205,255,255);*/
				}
				SetUIAimPointType(1);
				//EnableEnemyState(false,1);
			}
			if(pEntity)
			{
				//Remove Lock Effect
				if(pLastEntity && pLastEntity != pEntity)
				{
					/*IEntityRenderProxy *proxy=static_cast<IEntityRenderProxy*>(pLastEntity->GetProxy(ENTITY_PROXY_RENDER));
					if(proxy)
						proxy->SetHUDSilhouettesParams(0,0,0,0);*/
				}
				pLastEntity=pEntity;
				//Without this,some system will be unavailable.
				if(pCM->GetMode() == eConnectMode::eCMode::Deep)
				{
					g_pGame->GetConnectMode()->SetLevel(eConnectMode::eCLevel::eCLevel_ChooseSystem);

				}
				//Calculate_Screen_Pos(pEntity);
				/*gEnv->pRenderer->DrawLabel(left_up,2.0f,"0");
				gEnv->pRenderer->DrawLabel(left_down,2.0f,"1");
				gEnv->pRenderer->DrawLabel(right_up,2.0f,"2");
				gEnv->pRenderer->DrawLabel(right_down,2.0f,"3");*/
				//CryLogAlways("%.2f %.2f %.2f %.2f",screen_left_up.x,screen_left_up.y,screen_right_up.x,screen_right_up.y);
			}
			else
			{
				if(pCM->GetMode() == eConnectMode::eCMode::Deep)
					g_pGame->GetConnectMode()->SetLevel(eConnectMode::eCLevel::eCLevel_None);
				pEntity=NULL;
				SetUIAimPointType(1);
				EnableEnemyState(false,1);
			}
		}
	}
}

void BasicRayCast::Save(RayCastBackup &backup)
{
	backup.distance = distance;
	backup.Enable =Enable;
	backup.hits = hits;
	backup.pActor = pActor;
	backup.pSelf = pSelf;
	backup.pSelfActor = pSelfActor;
	backup.pWorld = pWorld;
	backup.query_flag = query_flag;
	backup.rayFlags = rayFlags;
}

void BasicRayCast::Load(RayCastBackup &backup)
{
	distance = backup.distance;
	Enable = backup.Enable;
	hits = backup.hits;
	pActor = backup.pActor;
	pSelf = backup.pSelf;
	pSelfActor = backup.pSelfActor;
	pWorld = backup.pWorld;
	query_flag = backup.query_flag;
	rayFlags = backup.rayFlags;
}

void BasicRayCast::Load()
{
	hits = 0 ;
	Enable = true ;
	distance = 20 ;
	query_flag = ent_all;
	pWorld = gEnv->pPhysicalWorld ;
	pSelfActor = NULL ;
	pActor = NULL ;
	pSelf = NULL ;
	rayFlags = rwi_pierceability_mask | rwi_colltype_any;
}


void BasicRayCast::Calculate_Screen_Pos(IEntity *pEntity)
{
	if(!pEntity)
		return;
	AABB aabb;
	AABB localaabb;
	pEntity->GetWorldBounds(aabb);
	pEntity->GetLocalBounds(localaabb);
	Vec3 dir1=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetForwardDir();    //玩家朝向
	Vec3 dir2=pEntity->GetForwardDir();    //实体朝向
	Vec3 tdir;    //与玩家朝向垂直的向量
	tdir.x=1;
	tdir.y=-float(dir1.x*tdir.x/dir1.y);
	tdir.z=0;
	tdir.normalize();

	//Get Helper Position
	/*IInventory *Inventory=pSelfActor->GetInventory();
	IItem *curItem=pSelfActor->GetCurrentItem(false);
	CWeapon *weapon=(CWeapon*)curItem->GetIWeapon();
	Vec3 pos=weapon->GetSlotHelperPos(eIGS_FirstPerson,"silencer_attach",true);
	gEnv->pRenderer->DrawLabel(pos,1.5f,"silencer_helper");*/
	//----------------

	/*CCamera pCamera;
	pCamera.SetPosition(pos);
	gEnv->pRenderer->SetCamera(pCamera);*/

	float dir1_2=dir1.x*dir2.x+dir1.y*dir2.y+dir1.z*dir2.z;    
	float cosa=abs(dir1_2/dir1.GetLength()*dir2.GetLength());
	float rad=cry_acosf(cosa);
	float sina=sin(rad);
	float length_x=localaabb.GetSize().x*cosa+localaabb.GetSize().y*sina;
	float length_y=localaabb.GetSize().x*sina+localaabb.GetSize().y*cosa;
	float radius=float ( ( sqrt( length_x*length_x+length_y*length_y ) ) / 1.6);

	//CryLogAlways("%.2f",radius);
	left_up=aabb.GetCenter()-tdir*radius;
	left_up.z=(float)(aabb.GetCenter().z+aabb.GetSize().z/2);
	left_down=aabb.GetCenter()-tdir*radius;
	left_down.z=(float)(aabb.GetCenter().z-aabb.GetSize().z/2);
	right_up=aabb.GetCenter()+tdir*radius;
	right_up.z=(float)(aabb.GetCenter().z+aabb.GetSize().z/2);
	right_down=aabb.GetCenter()+tdir*radius;
	right_down.z=(float)(aabb.GetCenter().z-aabb.GetSize().z/2);

	gEnv->pRenderer->GetCamera().Project(left_up,screen_left_up);
	gEnv->pRenderer->GetCamera().Project(left_down,screen_left_down);
	gEnv->pRenderer->GetCamera().Project(right_up,screen_right_up);
	gEnv->pRenderer->GetCamera().Project(right_down,screen_right_down);

	int width=gEnv->pRenderer->GetWidth();
	int height=gEnv->pRenderer->GetHeight();
	float ratio_x=1280.f/width;
	float ratio_y=720.f/height;

	screen_left_up.x=(screen_left_up.x-width/2)*ratio_x;
	screen_left_up.y=(screen_left_up.y-height/2)*ratio_y-25;
	screen_left_down.x=(screen_left_down.x-width/2)*ratio_x;
	screen_left_down.y=(screen_left_down.y-height/2)*ratio_y+25;
	screen_right_up.x=(screen_right_up.x-width/2)*ratio_x;
	screen_right_up.y=(screen_right_up.y-height/2)*ratio_y-25;
	screen_right_down.x=(screen_right_down.x-width/2)*ratio_x;
	screen_right_down.y=(screen_right_down.y-height/2)*ratio_y+25;
	if(screen_left_up.x>screen_right_up.x)
	{
		Vec3 t1=screen_left_up;
		Vec3 t2=screen_left_down;
		screen_left_up=screen_right_up;
		screen_left_down=screen_right_down;
		screen_right_up=t1;
		screen_right_down=t2;
	}
	pEntity=NULL;
}

IEntity *BasicRayCast::GetNearestEntity(const Vec3 &pos)
{
	IPhysicalEntity **nearbyEntities;
	int num=gEnv->pPhysicalWorld->GetEntitiesInBox(pos-Vec3(5),pos+Vec3(5),nearbyEntities,ent_all);
	if(!num)
		return NULL;
	for(int i=0;i<num;++i)
	{
		IEntity *pEntity=gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
		if(pEntity)
		{
			Vec3 dir=pEntity->GetPos() - pos;
			float length=dir.GetLengthSquared();
			DistanceEntity[length] = pEntity;
		}
	}
	if(!DistanceEntity.empty())
	{
		std::map<float,IEntity*>::iterator it=DistanceEntity.begin();
		IEntity *pEntity=it->second;
		DistanceEntity.clear();
		return pEntity;
	}
	return NULL;
}

void BasicRayCast::SetUIAimPointType(int _type)
{
	if(!pElement)
		pElement = gEnv->pFlashUI->GetUIElement("HUD_Final");
	else
		pElement->CallFunction("SetAimPoint",SUIArguments::Create(_type));
}

void BasicRayCast::SetUIEnemyState(float pos_x, float pos_y, bool _visible, EntityId Id, float _chealth, float _mhealth, string _class)
{
	if(!pElement)
		pElement = gEnv->pFlashUI->GetUIElement("HUD_Final");
	else
	{
		SUIArguments args;
		args.AddArgument(pos_x);
		args.AddArgument(pos_y);
		args.AddArgument(_visible);
		args.AddArgument(Id);
		args.AddArgument(_chealth);
		args.AddArgument(_mhealth);
		args.AddArgument(_class);
		pElement->CallFunction("UpdateEnemyState",args);
	}
}

void BasicRayCast::EnableEnemyState(bool _enable, EntityId Id)
{
	if(!pElement)
		pElement = gEnv->pFlashUI->GetUIElement("HUD_Final");
	else
	{
		SUIArguments args;
		args.AddArgument(_enable);
		args.AddArgument(Id);
		pElement->CallFunction("EnableEnemyState",args);
	}
}

void BasicRayCast::Reset()
{
	pEntity = NULL;
	pLastEntity = NULL;

}