#ifndef _CONSECUTIVESPAWN_H_
#define _CONSECUTIVESPAWN_H_

#include "StdAfx.h"

typedef void (*fun_ptr)(SEntitySpawnParams &, Vec3 &);

struct ConsecutiveSpawnParams
{
	fun_ptr pfun;
	IEntity *prevEntity;
	int StabilityCost;
	int maxCount;
	int curCount;
	ConsecutiveSpawnParams(fun_ptr _pfun, IEntity *pEntity, int _cost, int _max)
	:	pfun(0)
	,	prevEntity(0)
	,	StabilityCost(0)
	,	maxCount(_max)
	,	curCount(0){}

	ConsecutiveSpawnParams &operator=(const ConsecutiveSpawnParams &rhs)
	{
		if(this == &rhs)
			return *this;
		pfun = rhs.pfun;
		prevEntity = rhs.prevEntity;
		StabilityCost = rhs.StabilityCost;
		maxCount = rhs.maxCount;
		curCount = rhs.curCount;
		return *this;
	}
};


class ConsecutiveSpawn
{
public:
	ConsecutiveSpawn();
	~ConsecutiveSpawn(){}

	void Update(float frameTime);
	void AddToList(ConsecutiveSpawn);
private:
	std::list<ConsecutiveSpawn> CSpawnList;
};



#endif