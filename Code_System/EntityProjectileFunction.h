#ifndef _ENTITYPROJECTILEFUNCTION_H_
#define _ENTITYPROJECTILEFUNCTION_H_

//EntityProjectileFunction
//EPF For short
struct ProcessFunParams;

namespace EPF
{
	void ProcessHit_None(ProcessFunParams &);
	void ProcessHit_General(ProcessFunParams &);
	void ProcessHit_Explode(ProcessFunParams &);
	void ProcessHit_Heal(ProcessFunParams &);
	void ProcessHit_Impact(ProcessFunParams &);
	void ProcessHit_Stick(ProcessFunParams &);
	void ProcessHit_MicroExplode(ProcessFunParams &);
	void ProcessHit_Deterioration(ProcessFunParams &);
	void ProcessHit_InfectionVirus(ProcessFunParams &);
	void ProcessHit_ReplicationVirus(ProcessFunParams &);
}

#endif