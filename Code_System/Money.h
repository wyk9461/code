#ifndef _MONEY_H_
#define  _MONEY_H_


class Money
{
private:
	uint64 money;
	float rate;
	float timer;
	float gaintime;
public:
	Money();
	~Money(){}
	void Update(float frametime);
	inline uint64 Getmoney() const {return money;}
	inline void MoneyCost(int cost){money -= cost;}
	inline void MoneyGain(int gain){money += gain;}
};

#endif



