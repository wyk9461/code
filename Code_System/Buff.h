#ifndef _BUFF_H_
#define _BUFF_H_

#include "StdAfx.h"
#include "IFlashUI.h"
enum BuffType
{
	Damage_Buff = 0,
	Resistance_Buff,
	Stability_Buff,
	Reflect_Buff,
	Cd_Buff,
	Shield_Buff,
	FireEffect_Buff,
	ExplosionEffect_Buff,
	DataPile_Buff,
	CriticalStrike_Buff

};


class BuffManager
{
private:
	struct  Buff
	{
		float tick_timer;
		float Last_Time;
		float fValue;
		bool Available;

		Buff &operator=(const Buff &rhs)
		{
			if(this == &rhs)
				return *this;
			tick_timer=rhs.tick_timer;
			Last_Time=rhs.Last_Time;
			fValue=rhs.fValue;
			Available=rhs.Available;
			return *this;
		}

	};
	typedef std::map<BuffType,Buff> BuffMap;
	BuffMap Buffs;
	BuffMap::iterator iter;
	BuffManager();

	IUIElement *pElement;
	void AddUIBuff(float _lasttime, int _type);
	void GenerateUIMessage(int type, float effect);
public:
	~BuffManager();
	static BuffManager& TheBuffManager();

	void Update(const float frameTime);

	void CreateHealthBuff(IEntity *pEntity);
	void CreateExplosionBuff(IEntity *pEntity);
	void CreateDamageBuff(IEntity *pEntity);
	void CreateExplosionEffect_Buff(IEntity *pEntity);
	void CreateFireEffect_Buff(IEntity *pEntity);
	void CreateDataPile_Buff(IEntity *pEntity);
	void CreateCriticalStrike_Buff(float multi);

	inline const bool isDamageBuffAvailable(){if(Buffs.count(Damage_Buff)) return Buffs[Damage_Buff].Available; return false;}
	inline const float GetDamageBuffValue(){return Buffs[Damage_Buff].fValue;}

	inline const bool isExplosionEffectBuffAvailable(){if(Buffs.count(ExplosionEffect_Buff)) return Buffs[ExplosionEffect_Buff].Available; return false;}
	inline const float GetExplosionEffectBuffValue(){return Buffs[ExplosionEffect_Buff].fValue;}

	inline const bool isFireEffectBuffAvailable(){if(Buffs.count(FireEffect_Buff)) return Buffs[FireEffect_Buff].Available; return false;}
	inline const float GetFireEffectBuffValue(){return Buffs[FireEffect_Buff].fValue;}

	inline const bool isDataPileBuffAvailable(){if(Buffs.count(DataPile_Buff)) return Buffs[DataPile_Buff].Available; return false;}
	inline const float GetDataPileBuffValue(){return Buffs[DataPile_Buff].fValue;}

	inline const bool isCriticalStrikeAvailable(){if(Buffs.count(CriticalStrike_Buff)) return Buffs[CriticalStrike_Buff].Available; return false;}
	inline const float GetCriticalStrikeValue(){return Buffs[CriticalStrike_Buff].fValue;}
	inline void DisableCriticalStrike(){Buffs[CriticalStrike_Buff].Available = false;}
};

#endif