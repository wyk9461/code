#include "StdAfx.h"
#include "EntityProjectile.h"
#include "EntityProjectileFunction.h"

#define REGISTER_ENTITYPROJECTILE_FUNCTION(Function_Name,index)\
{\
	pFun fun;\
	fun = Function_Name;\
	EntityProjectileManager::Instance().RegisterProcessFunction(fun,index);\
}


EntityProjectileManager::EntityProjectileManager()
{
	root = gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/EntityProjectile.xml");
	UnlockList = gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/FunctionUnlock.xml");
	MaxCacheSlot = 10;
	if(root)
		ReadParams();
	if(UnlockList)
		ReadUnlock();

}

void EntityProjectileManager::RegisterProcessFunction(pFun _fun, int _index)
{
	if(!PFunctions.count(_index))
		PFunctions[_index] = _fun;
}

void EntityProjectileManager::ReadParams()
{
	root = gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/EntityProjectile.xml");
	int child_count = root->getChildCount();
	for(int i=eBarbWireFence; i< child_count; ++i)
	{
		XmlNodeRef child = root->getChild(i);
		XmlNodeRef params = child->getChild(0);
		EntityProjectileParams epParams;
		params->getAttr("geom",epParams.geom);
		params->getAttr("effect",epParams.effect);
		params->getAttr("speed",epParams.m_TracerParams.speed);
		params->getAttr("lifetime",epParams.m_TracerParams.lifetime);
		params->getAttr("delayBeforeDestroy",epParams.m_TracerParams.delayBeforeDestroy);
		params->getAttr("slideFraction",epParams.m_TracerParams.slideFraction);
		params->getAttr("functionIndex",epParams.FunctionIndex);
		params->getAttr("clipsize",epParams.clipsize);
		params->getAttr("ammo_conut",epParams.ammo_count); 
		params->getAttr("firerate",epParams.firerate); 
		params->getAttr("min_spread",epParams.min_spread);
		params->getAttr("max_spread",epParams.max_spread); 
		params->getAttr("attack_spread",epParams.attack_spread); 
		params->getAttr("max_recoil",epParams.max_recoil); 

		Projectiles[i] = epParams;
	}
	RegisterFunction();


	//Example here:
	//---------------------------------------------------------------------------------------------------------------------------------
	/*EntityProjectileParams &params = EntityProjectileManager::Instance().GetProjectileParams(0);
	CTracerManager::STracerParams tracer_params = params.m_TracerParams;
	tracer_params.effect = params.effect.c_str();

	IEntityClass *pEnityProjectileClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("EntityProjectile");
	if(pEnityProjectileClass)
	{
		CEntityProjectile *pProjectile = (CEntityProjectile*)g_pGame->GetWeaponSystem()->SpawnAmmo(pEnityProjectileClass);
		pProjectile->SetFunctionIndex(params.FunctionIndex);
		pProjectile->SetType(0);
		pProjectile->SetDamage(300);


		EntityId OwnerId = g_pGame->GetClientActorId();
		CActor *pPlayer = (CActor*)g_pGame->GetGameRules()->GetActorByEntityId(OwnerId);
		EntityId WeaponId = pPlayer->GetCurrentItemId();

		CProjectile::SProjectileDesc desc(
			1, 0, 2, (int)pProjectile->GetDamage(), 0, 0, 0, 1,
			0, false);
	}*/
	//-----------------------------------------------------------------------------------------------------------------------------------


}

void EntityProjectileManager::ReadUnlock()
{
	UnlockList = gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/FunctionUnlock.xml");
	int child_count = root->getChildCount();
	for(int i=0; i< child_count; ++i)
	{
		XmlNodeRef child = UnlockList->getChild(i);
		int index(0);
		bool unlock_status(false);
		child->getAttr("Index", index);
		child->getAttr("Unlock", unlock_status);
		FunctionUnlockState[index] = unlock_status;
	}
}


void EntityProjectileManager::RegisterFunction()
{
	REGISTER_ENTITYPROJECTILE_FUNCTION(EPF::ProcessHit_None,-1);
	REGISTER_ENTITYPROJECTILE_FUNCTION(EPF::ProcessHit_General,0);
	REGISTER_ENTITYPROJECTILE_FUNCTION(EPF::ProcessHit_Explode,1);
	REGISTER_ENTITYPROJECTILE_FUNCTION(EPF::ProcessHit_Heal,2);
	REGISTER_ENTITYPROJECTILE_FUNCTION(EPF::ProcessHit_Impact,3);
	REGISTER_ENTITYPROJECTILE_FUNCTION(EPF::ProcessHit_Stick,4);
	REGISTER_ENTITYPROJECTILE_FUNCTION(EPF::ProcessHit_MicroExplode,5);
	REGISTER_ENTITYPROJECTILE_FUNCTION(EPF::ProcessHit_Deterioration,6);
	REGISTER_ENTITYPROJECTILE_FUNCTION(EPF::ProcessHit_InfectionVirus,7);
	REGISTER_ENTITYPROJECTILE_FUNCTION(EPF::ProcessHit_ReplicationVirus,8);
}

EntityProjectileParams& EntityProjectileManager::GetProjectileParams(int Type)
{
	const bool UseCache = Projectiles.size() > 100;

	if(UseCache && Projectiles.count(Type))
	{
		//Find Cache First
		if(Projectiles_Cache.count(Type))
		{
			LRU_AddUnusedCount(Type);
			return *(Projectiles_Cache[Type].pParams);
		}
		else
		{
			if(Projectiles_Cache.size() < MaxCacheSlot)
			{
				EntityProjectileParams &params = Projectiles[Type];
				EntityProjectileCache cache;
				cache.pParams = &params;
				cache.unused_count = 0;

				Projectiles_Cache[Type] = cache;
				LRU_AddUnusedCount(Type);
				return params;
			}
			else
			{
				ReplaceWithLRU(Type);
				return Projectiles[Type];
			}
		}
	}
	else if(!UseCache && Projectiles.count(Type))
	{
		return Projectiles[Type];
	}
	return Projectiles[0];
}

void EntityProjectileManager::LRU_AddUnusedCount(int Type)
{
	EPCacheMap::iterator iter = Projectiles_Cache.begin();
	EPCacheMap::iterator end = Projectiles_Cache.end();
	while(iter != end)
	{
		if(iter->first != Type)
			++iter->second.unused_count;
		else
			iter->second.unused_count = 0;
		++iter;
	}
}

void EntityProjectileManager::ReplaceWithLRU(int Type)
{
	EPCacheMap::iterator iter = Projectiles_Cache.begin();
	EPCacheMap::iterator end = Projectiles_Cache.end();
	unsigned int maxUsed = 0;
	int replacedIndex;
	while(iter != end)
	{
		if(iter->second.unused_count > maxUsed)
		{
			maxUsed = iter->second.unused_count;
			replacedIndex = iter->first;
		}
		++iter;
	}

	EntityProjectileCache cache;
	cache.pParams = &Projectiles[Type];
	cache.unused_count = 0;
	Projectiles_Cache[replacedIndex] = cache;
}

void EntityProjectileManager::ProcessDiffCollision(int fun_index, ProcessFunParams &pfp)
{
	(*PFunctions[fun_index])(pfp);
}

void EntityProjectileManager::SaveUnlockStatus(int index)
{
	XmlNodeRef ListItem = UnlockList->getChild(index);
	ListItem->setAttr("Unlock", FunctionUnlockState[index]);
	UnlockList->saveToFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/FunctionUnlock.xml");
}

//------------------------------------------------------------------------
//CEntityProjectile
//------------------------------------------------------------------------
CEntityProjectile::CEntityProjectile()
	:	Type(0)
	,	FunctionIndex(0)
	,	Damage(0){}

CEntityProjectile::~CEntityProjectile()
{

}

void CEntityProjectile::HandleEvent(const SGameObjectEvent &event)
{
	CProjectile::HandleEvent(event);
	if (event.event == eGFE_OnCollision)
	{		
		EventPhysCollision *pCollision = (EventPhysCollision*)(event.ptr);
		if(!pCollision)
			return;
		IEntity *pTarget=pCollision->iForeignData[1] ==PHYS_FOREIGN_ID_ENTITY ? (IEntity*)pCollision->pForeignData[1] : 0;  
		const int hitMatId = pCollision->idmat[1];

		Vec3 hitDir(ZERO);
		if (pCollision->vloc[0].GetLengthSquared() > 1e-6f)
		{
			hitDir = pCollision->vloc[0].GetNormalized();
		}

		ProcessFunParams params(this,*pCollision,pTarget,Damage,hitMatId,hitDir);
		EntityProjectileManager::Instance().ProcessDiffCollision(FunctionIndex,params);
	}
}

void CEntityProjectile::Update(SEntityUpdateContext &ctx, int updateSlot)
{
	CProjectile::Update(ctx,updateSlot);
	if(Type == eBarbWireFence)
	{

	}
}
