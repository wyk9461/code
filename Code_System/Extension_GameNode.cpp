#include "StdAfx.h"
#include "Game.h"
#include "Nodes/G2FlowBaseNode.h"


class CFlowNode_WorldPosToScreenPos : public CFlowBaseNode<eNCT_Instanced>, public IGameFrameworkListener
{
	enum INPUTS
	{
		EIP_ENABLE = 0,
		EIP_DISABLE,
		EIP_POSX,
	};

	enum OUTPUTS
	{
		EOP_PX = 0,
		EOP_PY,
		EOP_VISIBLE,
		EOP_FALLOFFX,
		EOP_FALLOFFY,
		EOP_WIDTH,
		EOP_HEIGHT,
	};

public:
	CFlowNode_WorldPosToScreenPos( SActivationInfo * pActInfo )
	{
	}

	~CFlowNode_WorldPosToScreenPos()
	{
		if (gEnv && gEnv->pGame)
			gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
	}

	void GetConfiguration( SFlowNodeConfig& config )
	{
		static const SInputPortConfig in_ports[] = 
		{
			InputPortConfig_Void   ( "Enable", _HELP("Trigger to enable this node" )),
			InputPortConfig_Void   ( "Disable", _HELP("Trigger to disable this node" )),
			InputPortConfig_AnyType   ( "Pos", _HELP("Trigger to disable this node" )),
			{0}
		};
		static const SOutputPortConfig out_ports[] = 
		{
			OutputPortConfig<float>  ( "Px", _HELP("X pos on screen (0-1)") ),
			OutputPortConfig<float>  ( "Py", _HELP("Y pos on screen (0-1)") ),
			OutputPortConfig<bool>   ( "VisibleOnScreen", _HELP("True if visible on screen, otherwise false") ),
			OutputPortConfig<int>  ( "FalloffX", _HELP("-1 if entity is left from viewfrustrum, 0 if inside, 1 if right") ),
			OutputPortConfig<int>  ( "FalloffY", _HELP("-1 if entity is above the viewfrustrum, 0 if inside, 1 if beneath") ),
			OutputPortConfig<int>  ( "Width", _HELP("-1 if entity is above the viewfrustrum, 0 if inside, 1 if beneath") ),
			OutputPortConfig<int>  ( "Height", _HELP("-1 if entity is above the viewfrustrum, 0 if inside, 1 if beneath") ),
			{0}
		};
		config.pInputPorts = in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("Get Screen pos for current entity");
		config.SetCategory(EFLN_ADVANCED);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	IFlowNodePtr Clone( SActivationInfo *pActInfo )
	{
		return new CFlowNode_WorldPosToScreenPos(pActInfo);
	}

	void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			m_actInfo = *pActInfo;
			gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
			break;

		/*case eFE_SetEntityId:
			if(pActInfo->pEntity)
				m_entityId = pActInfo->pEntity->GetId();
			break;*/

		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_ENABLE))
			{
				gEnv->pGame->GetIGameFramework()->RegisterListener(this, "CFlowNode_EntityToScreenPos", FRAMEWORKLISTENERPRIORITY_HUD);
			}
			if (IsPortActive(pActInfo, EIP_DISABLE))
			{
				gEnv->pGame->GetIGameFramework()->UnregisterListener(this);
			}
			break;
		}
	}

	// IGameFrameworkListener
	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) {}
	virtual void OnPostUpdate(float fDelta)
	{
		Vec3 entityPos=GetPortVec3(&m_actInfo,EIP_POSX);

		Vec3 screenPos;
		gEnv->pRenderer->ProjectToScreen(entityPos.x,entityPos.y,entityPos.z,&screenPos.x,&screenPos.y,&screenPos.z);
		screenPos.x *= 0.01f;
		screenPos.y *= 0.01f;
		int falloffx = screenPos.x < 0 ? -1 : screenPos.x > 1 ? 1 : screenPos.z < 1 ? 0 : -1;
		int falloffy = screenPos.y < 0 ? -1 : screenPos.y > 1 ? 1 : screenPos.z < 1 ? 0 : -1;
		screenPos.x = clamp(screenPos.x,0,1.f);
		screenPos.y = clamp(screenPos.y,0,1.f);
		ActivateOutput(&m_actInfo, EOP_PX, screenPos.x);
		ActivateOutput(&m_actInfo, EOP_PY, screenPos.y);
		ActivateOutput(&m_actInfo, EOP_VISIBLE, falloffx + falloffy == 0);
		ActivateOutput(&m_actInfo, EOP_FALLOFFX, falloffx);
		ActivateOutput(&m_actInfo, EOP_FALLOFFY, falloffy);
		ActivateOutput(&m_actInfo,EOP_WIDTH,gEnv->pRenderer->GetWidth());
		ActivateOutput(&m_actInfo,EOP_HEIGHT,gEnv->pRenderer->GetHeight());
	}
	// ~IGameFrameworkListener

private:
	EntityId m_entityId;
	SActivationInfo m_actInfo;
};



class CFlowSetInventorySlotPos : public CFlowBaseNode<eNCT_Singleton>
{
	enum INPUTS
	{
		EIP_SET=0,
		EIP_POS,
		EIP_INDEX,
	};
	enum OUTPUTS
	{
	};

public:
	CFlowSetInventorySlotPos(SActivationInfo * pActInfo) { }

	void GetConfiguration(SFlowNodeConfig & config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_Void("Set",_HELP("Get Stability")),
			InputPortConfig<Vec3>("WorldPos",Vec3(0,0,0)),
			InputPortConfig<int>("Index",0),
			{0}
		};
		static const SOutputPortConfig out_ports[] =
		{
			{0}
		};
		config.pInputPorts=in_ports;
		config.pOutputPorts = out_ports;
		config.sDescription = _HELP("Get Stability Number");
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent( EFlowEvent flowEvent, SActivationInfo *pActInfo )
	{
		switch(flowEvent)
		{
		case eFE_Activate:
			{
				if(IsPortActive(pActInfo,EIP_SET))
				{
					int i=GetPortInt(pActInfo,EIP_INDEX);
					if(i<5)
					{
						//g_pGame->GetPlayerInventory()->SlotPos[i]=GetPortVec3(pActInfo,EIP_POS);
					}
				}
			}
			break;
		}
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};


REGISTER_FLOW_NODE("Entity:WorldScreenPos", CFlowNode_WorldPosToScreenPos);
REGISTER_FLOW_NODE("Code:SetInventorySlotPos", CFlowSetInventorySlotPos);