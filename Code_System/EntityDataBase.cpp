#include "StdAfx.h"
#include "EntityDataBase.h"
#include "GameActions.h"
#include "Code_System/ScreenEffectAdaptor.h"

EntityDataBase::EntityDataBase()
{
	pElement = NULL;
	preLoad = EntityClassPreLoad::Instance();
	curListIndex = -1;
	curDBIndex = 0;
	ShouldInitEntityDB = true;
	SpawnSlot[0] = 0;
	SpawnSlot[1] = 1;
	SpawnSlot[2] = 2;
	SpawnSlot[3] = 3;
	SpawnSlot[4] = 4;
	SpawnSlot[5] = 5;
}


void EntityDataBase::InitDataBase()
{
	XmlNodeRef root = gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
	if(root)
	{
		int ChildCount = root->getChildCount();
		//root->getAttr("max",ChildCount);
		for(int i=0; i< ChildCount;++i)
		{
			AddNewItem(i,root);
		}
	}
}

void EntityDataBase::AddNewItem(int xml_index, XmlNodeRef &root)
{
	if(!pElement)
	{
		pElement = gEnv->pFlashUI->GetUIElement("EntityDataBase");
	}
	if(pElement)
	{
		UIEntityInfo info;
		ScanEntityInfo(root,xml_index,info);

		SUIArguments args;
		args.AddArgument(xml_index);
		args.AddArgument(info.Name.c_str());
		args.AddArgument(info.EntityType);
		args.AddArgument(info.ClassName.c_str());
		args.AddArgument(info.Autho);
		args.AddArgument(info.Mass);
		args.AddArgument(info.Damage);
		args.AddArgument(info.Radius);
		args.AddArgument(info.Cost);

		pElement->CallFunction("AddEntityInfo",args);
		SUIArguments favoargs;
		favoargs.AddArgument(xml_index);
		favoargs.AddArgument(info.Favorite);
		pElement->CallFunction("UpdateFavorite", favoargs);
	}
	
}

void EntityDataBase::ScanEntityInfo(XmlNodeRef &root, int xml_index, UIEntityInfo &info)
{
	XmlNodeRef Child = root->getChild(xml_index);
	bool isSet(false);
	Child->getAttr("set",isSet);
	if(isSet)
		Child = Child->getChild(0);
	XmlNodeRef Params;

	XmlString ArchetypeName;
	Child->getAttr("Archetype", ArchetypeName);
	IEntityArchetype *pArchetype = gEnv->pEntitySystem->LoadEntityArchetype(ArchetypeName);

	info.Name = Child->getTag();
	info.ClassName = pArchetype->GetClass()->GetName();
	info.EntityType = GetEntityType(info.ClassName);
	SmartScriptTable Props = pArchetype->GetProperties();
	Props->GetValue("StabilityCost", info.Cost);
	Props->GetValue("ProjectileType", info.ProjectileType);
	Child->getAttr("Favorite",info.Favorite);

	info.Mass = 0;
	info.Damage = 0;
	info.Radius = 0;
	info.Autho = 2;
	/*Params = Child->getChild(0);

	Params->getAttr("Name",info.Name);
	Params->getAttr("Class",info.ClassName);
	info.EntityType = GetEntityType(info.ClassName);
	Params->getAttr("Mass",info.Mass);
	Params->getAttr("StabilityCost",info.Cost);
	Params->getAttr("Favorite",info.Favorite);
	Params->getAttr("ProjectileType",info.ProjectileType);
	if(info.EntityType == eET_Destroyable)
	{
		Params->getAttr("Damage",info.Damage);
		Params->getAttr("Radius",info.Radius);
	}

	if(info.EntityType == eET_Vehicle)
		info.ClassName = info.Name;*/
}


const int EntityDataBase::GetEntityType(XmlString &str)const
{
	if(str == "BasicEntity")
		return eET_BasicEntity;
	else if(str =="DestroyableObject")
	{
		str = "Destroyable";
		return eET_Destroyable;
	}
	else if(str =="Light")
		return eET_Light;
	else if(str =="Vehicle")
		return eET_Vehicle;
	else if(str =="Weapon")
		return eET_Weapon;
	else if(str =="Trap")
		return eET_Functional;
	return eET_BasicEntity;
}

void EntityDataBase::OpenDataBase()
{
	if(!pElement)
	{
		pElement = gEnv->pFlashUI->GetUIElement("EntityDataBase");
		if(pElement)
		{
			pElement->AddEventListener(&UIListener,"EntityDataBase_Listener");
			if(ShouldInitEntityDB)
			{
				InitDataBase();
				ShouldInitEntityDB = false;
			}
			pElement->CallFunction("DrawEntityList",SUIArguments::Create(0));
		}
	}
	if(pElement)
	{
		pElement->AddEventListener(&UIListener,"EntityDataBase_Listener");
		if(ShouldInitEntityDB)
		{
			InitDataBase();
			ShouldInitEntityDB = false;
		}
		pElement->SetVisible(true);
		pElement->CallFunction("DrawEntityList",SUIArguments::Create(NULL));
		g_pGame->Actions().FilterNoMouse()->Enable(true);
		g_pGame->Actions().FilterNoMove()->Enable(true);

		/*gEnv->p3DEngine->SetPostEffectParam("Dof_Active", 1.0f);
		gEnv->p3DEngine->SetPostEffectParam("Dof_BlurAmount", 100.0f);
		gEnv->p3DEngine->SetPostEffectParam("Dof_FocusRange", 0.0f);
		gEnv->p3DEngine->SetPostEffectParam("Dof_FocusDistance", 0.0f);*/

		IUIElement *pHud = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
		if(pHud)
			pHud->SetVisible(false);

		/*ScreenEffectAdaptor &theAdaptor = ScreenEffectAdaptor::TheAdaptor();
		effect_params params;
		params.effect_name = "Dof_BlurAmount";
		gEnv->p3DEngine->GetPostEffectParam(params.effect_name, params.cur_value);
		params.last_time = 0;
		params.end_value = 2.5;
		params.reverse = false;
		params.reverseId = eETID_EntityDataBase;
		theAdaptor.AddToQueue(params,eETID_EntityDataBase);*/
	}

}

void EntityDataBase::CloseDataBase()
{
	if(pElement)
	{
		pElement->SetVisible(false);
		g_pGame->Actions().FilterNoMouse()->Enable(false);
		g_pGame->Actions().FilterNoMove()->Enable(false);

		pElement->RemoveEventListener(&UIListener);

		gEnv->p3DEngine->SetPostEffectParam("FilterRadialBlurring_Radius", 10.0f);
		gEnv->p3DEngine->SetPostEffectParam("FilterRadialBlurring_Amount", 0.0f);

		/*gEnv->p3DEngine->SetPostEffectParam("Dof_Active", 0.0f);
		gEnv->p3DEngine->SetPostEffectParam("Dof_BlurAmount", 0.0f);
		gEnv->p3DEngine->SetPostEffectParam("Dof_FocusRange", 0.0f);
		gEnv->p3DEngine->SetPostEffectParam("Dof_FocusDistance", 0.0f);*/

		IUIElement *pHud = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
		if(pHud)
			pHud->SetVisible(true);
		/*ScreenEffectAdaptor &theAdaptor = ScreenEffectAdaptor::TheAdaptor();
		effect_params params;
		params.effect_name = "Dof_BlurAmount";
		gEnv->p3DEngine->GetPostEffectParam(params.effect_name, params.cur_value);
		params.last_time = 2.5;
		params.end_value = 0;
		params.reverse = false;
		params.reverseId = eETID_EntityDataBase;
		theAdaptor.AddToQueue(params,eETID_EntityDataBase);*/
	}
}

void EntityDataBase::OpenCreateMenu()
{
	if(!pCreateMenu)
	{
		pCreateMenu = gEnv->pFlashUI->GetUIElement("HUD_Create_Menu");
	}
	if(pCreateMenu)
	{
		pCreateMenu->AddEventListener(&UIListener,"EntityDataBase_Listener");
		pCreateMenu->SetVisible(true);
		g_pGame->Actions().FilterNoMouse()->Enable(true);

		SetStability(g_pGame->GetWorldStability()->Getstability());
	}
}

void EntityDataBase::CloseCreateMenu()
{
	if(!pCreateMenu)
	{
		pCreateMenu = gEnv->pFlashUI->GetUIElement("HUD_Create_Menu");
	}
	if(pCreateMenu)
	{
		g_pGame->Actions().FilterNoMouse()->Enable(false);
		pCreateMenu->RemoveEventListener(&UIListener);
		pCreateMenu->SetVisible(false);
	}
}

const bool EntityDataBase::IsDBOpened()
{
	if(pElement)
	{
		return pElement->IsVisible();
	}
	return false;
}

const bool EntityDataBase::IsCreateMenuOpened()
{
	if(pCreateMenu)
	{
		return pCreateMenu->IsVisible();
	}
	return false;
}

void EntityDataBase::SetSlotInfo(int slot_index, int db_index)
{
	IUIElement *pHud = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	if(pHud)
	{
		XmlNodeRef root = gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
		SpawnSlot[slot_index] = db_index;
		UIEntityInfo info;
		ScanEntityInfo(root,db_index,info);

		SUIArguments args;
		args.AddArgument(slot_index);
		args.AddArgument(info.ProjectileType);
		pHud->CallFunction("SetCreateSlot", args);

		SUIArguments args_db;
		args_db.AddArgument(curListIndex);
		args_db.AddArgument(slot_index);

		pElement->CallFunction("AddSlotIndicator", args_db);

		g_pGame->GetSpawnSystem()->SetQuickSpawnIndex(SpawnSlot[0]);
	}


/*	if(!pCreateMenu)
	{
		pCreateMenu = gEnv->pFlashUI->GetUIElement("HUD_Create_Menu");
	}
	if(pCreateMenu)
	{
		XmlNodeRef root = gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
		SpawnSlot[slot_index] = db_index;
		UIEntityInfo info;
		ScanEntityInfo(root,db_index,info);

		//For old UI
		SUIArguments args;
		args.AddArgument(slot_index);
		args.AddArgument(db_index);
		args.AddArgument(info.EntityType);
		args.AddArgument(info.Cost);
		args.AddArgument(info.Name.c_str());

		pCreateMenu->CallFunction("SetSlotItem", args);

		SUIArguments args_db;
		args_db.AddArgument(curListIndex);
		args_db.AddArgument(slot_index);

		pElement->CallFunction("AddSlotIndicator", args_db);

		g_pGame->GetSpawnSystem()->SetQuickSpawnIndex(SpawnSlot[0]);
	}*/
}


void EntityDataBase::SetFavorite()
{
	XmlNodeRef root = gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
	XmlNodeRef Child = root->getChild(curDBIndex);
	bool isSet(false);
	Child->getAttr("set",isSet);
	if(isSet)
		Child = Child->getChild(0);
	XmlNodeRef Params;

	Params = Child->getChild(0);
	bool Favorite;
	Params->getAttr("Favorite",Favorite);
	if(Favorite)
		Params->setAttr("Favorite",0);
	else
		Params->setAttr("Favorite",1);

	root->saveToFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/SpawnList.xml");
}

void EntityDataBase::SetStability(int sta)
{
	if(!pCreateMenu)
	{
		pCreateMenu = gEnv->pFlashUI->GetUIElement("HUD_Create_Menu");
	}
	if(pCreateMenu)
	{
		pCreateMenu->CallFunction("SetStaCount", SUIArguments::Create(sta));
	}
}

void CEntityDBListener::OnUIEvent( IUIElement* pSender, const SUIEventDesc& event, const SUIArguments& args )
{
	SpawnSystem *pSystem = g_pGame->GetSpawnSystem();
	if (strcmp(event.sDisplayName, "ItemRollOver") == 0 )
	{
		int index;
		args.GetArg(0, index);
		EntityDataBase::Instance().SetCurListIndex(index);
	}
	else if (strcmp(event.sDisplayName, "RollDBIndex") == 0 )
	{
		int index;
		args.GetArg(0, index);
		EntityDataBase::Instance().SetCurDBIndex(index);
	}
	else if (strcmp(event.sDisplayName, "Selected") == 0 )
	{
		int index;
		args.GetArg(0, index);
		EntityDataBase::Instance().SetCurDBIndex(index);
	}
	else if (strcmp(event.sDisplayName, "DBIndex") == 0 )
	{
		int index;
		args.GetArg(0, index);
		EntityDataBase::Instance().SetCurSlotDBIndex(index);
	}
}