#ifndef _SCRIPTMODIFY_AI_H_
#define _SCRIPTMODIFY_AI_H_

namespace ScriptModify_AI
{
	float ChangeAccuracy(IActor *pActor , const float v_accuracy);
	int ChangeSightRange(IActor *pActor , const int v_range);
	int ChangeFaction(IActor *pActor , const char* v_faction);
	int ChangeFaction(IActor *pActor , int v_faction);
	float ChangeSpeed(IActor *pActor , const float v_speed);
}
#endif