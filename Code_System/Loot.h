#ifndef _LOOT_H_
#define _LOOT_H_

#include "Code_System/WorldStability.h"
#include "Money.h"


enum LootType
{
	eLT_Money,
	eLT_Health,
	eLT_Stability,
	eLT_Ammo,
	eLT_Data,
	eLT_Filler,
	eLT_Accessoris,
};


struct DropParams
{
	int money_num;
	float health_num;
	int stability_num;
	IEntityClass *AmmoClass;
	int ammo_num;
	int data_num;
	IEntity *pEffect;
	float maxTime;
	float timer;
	float percentage;
	float opacity;
	float hidetimer;
	DropParams &operator=(const DropParams &dp);
	DropParams()
		:	money_num(0)
		,	health_num(0)
		,	stability_num(0)
		,	ammo_num(0)
		,	AmmoClass(NULL)
		,	data_num(0)
		,	maxTime(1)
		,	timer(0)
		,	percentage(0)
		,	opacity(100)
		,	hidetimer(0){}
};


class LootSystem
{
private:
	IEntity *pEntity;
	EntityId Id;
	IActor *pActor;
	bool isBody;
	//Ray Cast and Common
	IEntity *pEffect;
	ray_hit hit;
	int hits;
	int query_flag;
	IPhysicalWorld *pWorld;
	IRenderer *pRenderer;
	//Probability
	int Money_Drop_Probability;
	int Health_Drop_Pobability;
	int Stability_Drop_Probability;
	int Ammo_Drop_Stability;
	int Acessoris_Drop_Probability;
	int Data_Drop_Probability;
	int AbilityEnhance_Drop_Probability;
	IActor *player;
	int MaxProbability;
	//std::vector<DropParams> Dropped;
	bool Is_Scanning;
	bool After_Scanning;
	bool Scaned_Once;
	bool once;
	bool finished;
	SEntitySpawnParams eParams;
	IUIElement *pElement;
	//std::vector<DropParams>::iterator it;

	// Description : 
	//		Set the money count.
	void DropMoney(int level,DropParams &param);

	// Description : 
	//		Set the Health count.
	void DropHealth(int level,DropParams &param);

	// Description : 
	//		Set the Stability count.
	void DropStability(int level,DropParams &param);

	// Description : 
	//		Process the Ammo data.
	void DropAmmo(int level,CActor *pActor,DropParams &param);

	// Description : 
	//		Set the Data count.
	void DropData(int level,DropParams &param);

	void UpdatePendingQueue(float frameTime);

public:
	LootSystem();
	~LootSystem();
	std::map<EntityId,DropParams> Drops;
	void Update(float frametime);

	// Description : 
	//		Process trophy after an enemy is dead.
	//		Call in the GameRules.cpp(Line 3726)
	void ProcessLoot(CActor *pActor);

	// Description : 
	//		Get Items after scanning.
	void GetLootItems(DropParams &param);

	// Description : 
	//		Clear the Drops map.
	inline void clearDropped(){Drops.clear();}

	inline void SetScanningState(bool enable){Is_Scanning=enable;}
	inline void SetScanedOnce(bool enable){Scaned_Once=enable;}
	inline const bool Isbody(){return isBody;}
	inline IEntity *getLootEntity(){return pEntity;}
	inline std::map<EntityId,DropParams> GetDrops(){return Drops;}
	inline const bool isAfterScan(){return After_Scanning;}
	inline const bool isFinished(){return finished;}

	std::deque<std::string> PendingMessage;
	std::deque<std::string>::iterator Pending_Iter;

	float max_pending_time;
	float tick_timer;
};


#endif