#ifndef _FUNCTIONALMENU_H_
#define _FUNCTIONALMENU_H_

#include "IFlashUI.h"

class CFunctionalMenuListener : public IUIElementEventListener
{
public:
	virtual void OnUIEvent( IUIElement* pSender, const SUIEventDesc& event, const SUIArguments& args );
};


class FunctionalMenu
{
public:
	static FunctionalMenu &Instance(){static FunctionalMenu instance;return instance;}

	void OpenMenu();
	void CloseMenu();
	const bool  IsMenuOpened();
private:
	FunctionalMenu(){}
	IUIElement *pElement;
	CFunctionalMenuListener UIListener;
};

#endif