#include "StdAfx.h"
#include "ModifySystem.h"
#include "Code_System/WorldStability.h"
#include "GameActions.h"
#include "BattleChallenge.h"
//------------------------------------------------------------------------
ModifySystem::ModifySystem()
{
	StartFunction=false;
	preStore=true;
	EntityLocked=false;
	HasApply=false;
	NeedParams=true;
	isplayer=false;
	OnLocking=false;
	HasLockedOnce=false;

	hits=0;
	Maxnumber=5;
	curindex=0;
	lastindex=0;
	timescale_timer=0;
	max_timescale_timer=5;
	timescale_rate=0;
	fade_timescale_timer=3;
	Locking_time=1;
	SlotIndex=0;

	defWeaponModifyLastTime=25.0f;
	defVehicleModifyLastTime=60.0f;

	LastEntity=NULL;
	LastEntityMtl=NULL;
	pTarget=NULL;
	modifytype=eECT_Last;
	query_flag=ent_static | ent_rigid | ent_sleeping_rigid | ent_living | ent_independent;

	DefaultDeleteMtl=gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("delete");
	timeScaleCVar = gEnv->pConsole->GetCVar("t_Scale");
	className="";


	Modifiers = new ModifierManager();
	ModifySlot[1]=LEG_WEAK_10S;

	preLoad = EntityClassPreLoad::Instance();
	pElement = 0;
	bMultiModify = false;
}

//------------------------------------------------------------------------
void ModifySystem::Update(float frametime)
{
	/*if(StartFunction==true)
	{
		IRenderer *pRenderer=gEnv->pRenderer;
		if(!pTarget)
			MP.MEntity=NULL;
		if(pTarget)
		{
			MP.MEntity=pTarget;
			if(pTarget != LastEntity && LastEntity)
				NeedParams=true;
			ChooseModifyType();
		}
		if(OnLocking)
		{
			Locking_timer+=frametime;
			Locking_progress+=Locking_rate*frametime;
			if(Locking_timer<=Locking_time)
				pRenderer->DrawLabel(MP.MEntity->GetPos(),1.5f,"Locking...%.0f%%",Locking_progress);
			if(Locking_timer>Locking_time)
			{
				Locking_timer=0;
				Locking_progress=0;
				EntityLocked=true;
				if(!SlotIndex)
				{
					timeScaleCVar->Set(0.1f);
					OnLocking=false;
				}
				else
				{
					ApplyCustomizeSlot();
					OnLocking=false;
				}
			}
		}
		if(EntityLocked && !isplayer)
			pRenderer->Draw2dLabel(50,100,1.5f,ColorF(255,255,255),false,"Cost : %d",MP.cost);
		if(modifytype == eECT_Last)
			if(!OnLocking && MP.MEntity)
				pRenderer->DrawLabel(hit.pt,1.5f,"%s",MP.MEntity->GetClass()->GetName());
		if(modifytype == eECT_BasciEntity)
		{
			if(!EntityLocked)
			{
				if(NeedParams)
					GetBasicEntityParams();
				if(!OnLocking && MP.MEntity)
					pRenderer->DrawLabel(hit.pt,1.5f,"%s",MP.MEntity->GetClass()->GetName());
			}
			else
				DrawBasicEntityParams(pRenderer);
		}
		if(modifytype == eECT_DestroyableObject)
		{
			if(!EntityLocked)
			{
				if(NeedParams)
					GetDestroyableObjectParams();
				if(!OnLocking && MP.MEntity)
					pRenderer->DrawLabel(hit.pt,1.5f,"%s",MP.MEntity->GetClass()->GetName());
			}
			else
				DrawDestroyableObjectParams(pRenderer);
		}
		if(modifytype == eECT_Vehicles)
		{
			if(!EntityLocked && !MP.needupdate)
			{
				if(NeedParams)
					GetVehicleParams();
			}
			if(!EntityLocked)
				if(!OnLocking && MP.MEntity)
					pRenderer->DrawLabel(hit.pt,1.5f,"%s",MP.MEntity->GetClass()->GetName());
			if(EntityLocked)
			{
				DrawVehicleParams(pRenderer);
				pRenderer->Draw2dLabel(50,125,1.5f,ColorF(255,255,255),false,"LastTime: %.2f",VehicleModifyLastTime);
			}
		}
		if(modifytype == eECT_Light)
		{
			if(!EntityLocked)
			{
				if(NeedParams)
					GetLightParams();
				if(!OnLocking && MP.MEntity)
					pRenderer->DrawLabel(hit.pt,1.5f,"%s",MP.MEntity->GetClass()->GetName());
			}
			else
				DrawLightParams(pRenderer);
		}
		if(modifytype == eECT_AI_Weapon)
		{
			if(!EntityLocked)
			{
				if(MP.MEntity)
					MP.pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(MP.MEntity->GetId());
				if(MP.pActor->GetHealth()>0)
				{
					MP.pItem=(CItem*)MP.pActor->GetCurrentItem(false);
					if(MP.pItem)
					{
						MP.BWP.weaponClass=MP.pItem->GetEntity()->GetClass()->GetName();
						if(NeedParams)
							GetNormalWeaponParams(MP.BWP.weaponClass);
						if(!OnLocking && MP.MEntity)
							pRenderer->DrawLabel(hit.pt,1.5f,"%s",MP.BWP.weaponClass);
					}
				}
			}
			if(EntityLocked && MP.pActor->GetHealth()>0)
			{
				DrawNormalWeaponParams(pRenderer,MP.BWP.weaponClass);
				pRenderer->Draw2dLabel(50,125,1.5f,ColorF(255,255,255),false,"LastTime: %.2f",WeaponModifyLastTime);
			}
		}
		if(isplayer)
		{
			const float color[ 4 ] = { 0.258824f, 0.258824f, 0.435294f , 1.0f};
			modifytype = eECT_Player_Weapon;
			SetPlayerWeaponClass();
			if(!EntityLocked)
			{
				GetNormalWeaponParams(MP.BWP.weaponClass);
				pRenderer->Draw2dLabel(50,100,1.5f,color,false,"%s",MP.BWP.weaponClass);
			}
			else
			{
				DrawNormalWeaponParams(pRenderer,MP.BWP.weaponClass);
				pRenderer->Draw2dLabel(50,125,1.5f,color,false,"Cost : %d",MP.cost);
				pRenderer->Draw2dLabel(50,150,1.5f,color,false,"LastTime: %.2f",WeaponModifyLastTime);
			}
		}
	}*/
	if(StartFunction)
	{
		IRenderer *pRenderer=gEnv->pRenderer;
		if(OnLocking)
		{
			Locking_timer+=frametime;
			Locking_progress+=Locking_rate*frametime;
			if(Locking_timer<=Locking_time)
				pRenderer->DrawLabel(MP.MEntity->GetPos(),1.5f,"Locking...%.0f%%",Locking_progress);
			if(Locking_timer>Locking_time)
			{
				Locking_timer=0;
				Locking_progress=0;
				EntityLocked=true;
				OnLocking=false;
				StartFunction=false;
				g_pGame->GetConnectMode()->SetLevel(eConnectMode::eCLevel::eCLevel_Modify_ComplexModify);
			}
		}
	}
	if(MP.WeaponInTime)
	{
		MP.WeaponTimer+=frametime;

		if(MP.WeaponTimer<WeaponModifyLastTime && MP.WeaponTimer>WeaponModifyLastTime-0.2)
			if(preStore)
			{
				StoreInventory();
				preStore=false;
			}

		if(MP.WeaponTimer>WeaponModifyLastTime)
		{
			AfterModify(eECT_Player_Weapon);
			MP.WeaponTimer=0;
			preStore=true;
		}
	}

	if(MP.VehicleInTime)
	{
		MP.VehicleTimer+=frametime;
		if(MP.VehicleTimer>VehicleModifyLastTime)
		{
			AfterModify(eECT_Vehicles);
			MP.VehicleTimer=0;
		}
	}
}

//------------------------------------------------------------------------
void ModifySystem::StoreInventory()
{
	CActor *pSelfActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	CItemSystem* pItemSystem = static_cast<CItemSystem*> (g_pGame->GetIGameFramework()->GetIItemSystem());
	/*IItem *pItem=pSelfActor->GetCurrentItem(false);
	store_weapon_name=pItem->GetEntity()->GetName();*/
	pItemSystem->SerializePlayerLTLInfo(false);
	IInventory* pInventory = pSelfActor->GetInventory();
	pInventory->RMIReqToServer_RemoveAllItems();
}

//------------------------------------------------------------------------
void ModifySystem::RestoreInventory()
{
	CActor *pSelfActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	CItemSystem* pItemSystem = static_cast<CItemSystem*> (g_pGame->GetIGameFramework()->GetIItemSystem());
	pItemSystem->SerializePlayerLTLInfo(true);
	//pSelfActor->SelectItemByName(store_weapon_name,true);
}

//------------------------------------------------------------------------
void ModifySystem::ProcessModifyEntity(IEntity *MEntity)
{
	if(MEntity)
	{
		BattleChallenge &instance = BattleChallenge::Instance();
		if(instance.GetRestrictType() == eRT_NoModify)
			instance.FailRestrictChallenge();
		MP.MEntity=MEntity;
		IEntityClass *pClass = MP.MEntity->GetClass();
		InsertModifierToUIMenu(pClass);
		if(bMultiModify)
		{
			FindSameEntityClassInRange(MEntity);
			std::list<EntityId>::iterator iter = MultiModifyList.begin();
			std::list<EntityId>::iterator end = MultiModifyList.end();
			while(iter != end)
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(*iter);
				IEntityRenderProxy *proxy=static_cast<IEntityRenderProxy*>(pEntity->GetProxy(ENTITY_PROXY_RENDER));
				if(proxy)
					proxy->SetHUDSilhouettesParams(255,255,255,255);
				++iter;
			}
		}
		else
		{
			if(MP.MEntity)
			{
				IEntityRenderProxy *proxy=static_cast<IEntityRenderProxy*>(MEntity->GetProxy(ENTITY_PROXY_RENDER));
				if(proxy)
					proxy->SetHUDSilhouettesParams(255,255,255,255);
			}
		}
	}
	/*if(className == "BasicEntity")
	{
		modifytype = eECT_BasciEntity;
		GetBasicEntityParams();
	}
	else if(className == "DestroyableObject")
	{
		modifytype = eECT_DestroyableObject;
		GetDestroyableObjectParams();
	}
	else if(className == "Abrams" || className == "HMMWV" || className == "MH60_Blackhawk" || className == "SpeedBoat")
	{
		modifytype = eECT_Vehicles;
		GetVehicleParams();
	}
	else if(className == "DestroyableLight")
	{
		modifytype = eECT_Light;
		GetLightParams();
	}
	else if(AI_Type(className))
	{
		modifytype = eECT_AI_Weapon;
		GetNormalWeaponParams(className);
	}
	else
		modifytype = eECT_Last;*/
}

//------------------------------------------------------------------------
bool ModifySystem::AI_Type(string & className)
{
	if(className == "Human")
		return true;
	else
		return false;
}

//------------------------------------------------------------------------
void ModifySystem::ClearMP()
{
	MP.MEntity=NULL;
	NeedParams=true;
	MP.BPP.clear();
	MP.EP.clear();
	MP.LP.clear();
	MP.LVP.clear();
	MP.BWP.clear();
	MP.SGP.clear();
}

//------------------------------------------------------------------------
ModifySystem::ModifyParams ModifySystem::ModifyParams::operator=(const ModifySystem::ModifyParams &MP)
{
	this->BPP.mass=MP.BPP.mass;
	this->BPP.density=MP.BPP.density;
	this->BPP.rigid=MP.BPP.rigid;

	this->EP.explode=MP.EP.explode;
	this->EP.effectscale=MP.EP.effectscale;
	this->EP.minradius=MP.EP.minradius;
	this->EP.radius=MP.EP.radius;
	this->EP.minphysradius=MP.EP.minphysradius;
	this->EP.physradius=MP.EP.physradius;
	this->EP.pressure=MP.EP.pressure;
	this->EP.damage=MP.EP.damage;
	this->EP.invulnerable=MP.EP.invulnerable;
	this->EP.health=MP.EP.health;

	this->LVP.acceleration=MP.LVP.acceleration;
	this->LVP.decceleration=MP.LVP.decceleration;
	this->LVP.topSpeed=MP.LVP.topSpeed;
	this->LVP.reverseSpeed=MP.LVP.reverseSpeed;
	this->LVP.backfriction=MP.LVP.backfriction;
	this->LVP.frontfriction=MP.LVP.frontfriction;
	this->LVP.inertia=MP.LVP.inertia;

	this->LP.turn_on=MP.LP.turn_on;
	this->LP.lightradius=MP.LP.lightradius;
	this->LP.lightstyle=MP.LP.lightstyle;
	this->LP.diffusemultiplier=MP.LP.diffusemultiplier;
	this->LP.specularmultiplier=MP.LP.specularmultiplier;
	this->LP.HDRDynamic=MP.LP.HDRDynamic;
	this->LP.colordiffuse=MP.LP.colordiffuse;

	this->BWP.firerate=MP.BWP.firerate;
	this->BWP.player_damage=MP.BWP.player_damage;
	this->BWP.ai_damage=MP.BWP.ai_damage;
	this->BWP.clip_size=MP.BWP.clip_size;

	this->SGP.pellets=MP.SGP.pellets;
	this->SGP.spread=MP.SGP.spread;

	return *this;
}


//------------------------------------------------------------------------
void ModifySystem::GetBasicEntityParams()
{
	SmartScriptTable pScript = MP.MEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable physics_props;
	pScript->GetValue("Properties",props);
	props->GetValue("Physics",physics_props);
	physics_props->GetValue("Mass",MP.BPP.mass);
	physics_props->GetValue("Density",MP.BPP.density);
	physics_props->GetValue("bRigidBody",MP.BPP.rigid);
	NeedParams=false;
}

//------------------------------------------------------------------------
void ModifySystem::GetDestroyableObjectParams()
{
	MP.pScript = MP.MEntity->GetScriptTable();
	MP.pScript->GetValue("Properties",MP.props);
	MP.props->GetValue("Physics",MP.physics_props);
	MP.props->GetValue("Explosion",MP.explosion_props);
	MP.props->GetValue("Health",MP.health_props);

	MP.physics_props->GetValue("Mass",MP.BPP.mass);    //实体质量
	MP.physics_props->GetValue("Density",MP.BPP.density);    //实体密度
	MP.physics_props->GetValue("bRigidBody",MP.BPP.rigid);    //是否固定

	MP.props->GetValue("bExplode",MP.EP.explode);  //实体是否爆炸
	MP.explosion_props->GetValue("EffectScale",MP.EP.effectscale);    //粒子特效的大小
	MP.explosion_props->GetValue("MinRadius",MP.EP.minradius);    //最小影响范围
	MP.explosion_props->GetValue("Radius",MP.EP.radius);    //爆炸影响范围
	MP.explosion_props->GetValue("MinPhysRadius",MP.EP.minphysradius);    //最小物理影响范围
	MP.explosion_props->GetValue("PhysRadius",MP.EP.physradius);    //物理影响范围
	MP.explosion_props->GetValue("Pressure",MP.EP.pressure);    //冲击力
	MP.explosion_props->GetValue("Damage",MP.EP.damage);    //伤害
	MP.health_props->GetValue("bInvulnerable",MP.EP.invulnerable);    //是否无敌
	MP.health_props->GetValue("MaxHealth",MP.EP.health);    //血量

	NeedParams=false;
}


//------------------------------------------------------------------------
void ModifySystem::GetVehicleParams()
{
	IVehicle *pVehicle=g_pGame->GetIGameFramework()->GetIVehicleSystem()->GetVehicle(MP.MEntity->GetId());
	if(className=="HMMWV" ||className == "Abrams")
	{
		XmlNodeRef root=gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Vehicles/Implementations/Xml/HMMWV.xml");
		XmlNodeRef MovementParams=root->findChild("MovementParams");
		XmlNodeRef PartsParams=root->findChild("Parts");
		int massboxindex= ( className == "HMMWV"? 1 : 3) ;
		XmlNodeRef MassBoxParams=PartsParams->getChild(massboxindex);
		MassBoxParams->getAttr("mass",MP.BPP.mass);
		MassBoxParams->getAttr("disablePhysics",MP.BPP.rigid);
		XmlNodeRef ArcadeWheeledParams=MovementParams->findChild("ArcadeWheeled");
		XmlNodeRef HandlingParams=ArcadeWheeledParams->findChild("Handling");
		XmlNodeRef PowerParams=HandlingParams->findChild("Power");
		XmlNodeRef FrictionPrams=HandlingParams->findChild("Friction");
		PowerParams->getAttr("acceleration",MP.LVP.acceleration);
		PowerParams->getAttr("decceleration",MP.LVP.decceleration);
		PowerParams->getAttr("topSpeed",MP.LVP.topSpeed);
		PowerParams->getAttr("reverseSpeed",MP.LVP.reverseSpeed);
		FrictionPrams->getAttr("back",MP.LVP.backfriction);
		FrictionPrams->getAttr("front",MP.LVP.frontfriction);
		NeedParams=false;
	}
}

//------------------------------------------------------------------------
void ModifySystem::GetLightParams()
{
	MP.pScript=MP.MEntity->GetScriptTable();
	MP.pScript->GetValue("Properties",MP.props);
	MP.pScript->GetValue("PropertiesInstance",MP.PropertiesInstance);
	MP.PropertiesInstance->GetValue("LightProperties_Base",MP.LightParams);
	MP.LightParams->GetValue("Style",MP.Style);
	MP.LightParams->GetValue("Color",MP.Color);
	MP.props->GetValue("Physics",MP.physics_props);
	MP.props->GetValue("Explosion",MP.explosion_props);
	MP.props->GetValue("Health",MP.health_props);
	MP.props->GetValue("Damage",MP.Damage);

	MP.physics_props->GetValue("Mass",MP.BPP.mass);
	MP.physics_props->GetValue("Density",MP.BPP.density);
	MP.physics_props->GetValue("bRigidBody",MP.BPP.rigid);

	MP.Damage->GetValue("bExplode",MP.EP.explode);
	MP.Damage->GetValue("fHealth",MP.EP.health);
	MP.explosion_props->GetValue("EffectScale",MP.EP.effectscale);
	MP.explosion_props->GetValue("MinRadius",MP.EP.minradius);
	MP.explosion_props->GetValue("Radius",MP.EP.radius);
	MP.explosion_props->GetValue("MinPhysRadius",MP.EP.minphysradius);
	MP.explosion_props->GetValue("PhysRadius",MP.EP.physradius);
	MP.explosion_props->GetValue("Pressure",MP.EP.pressure);
	MP.explosion_props->GetValue("Damage",MP.EP.damage);
	MP.health_props->GetValue("Invulnerable",MP.EP.invulnerable);

	MP.LightParams->GetValue("TurnedOn",MP.LP.turn_on);
	MP.LightParams->GetValue("Radius",MP.LP.lightradius);
	MP.Style->GetValue("nLightStyle",MP.LP.lightstyle);
	MP.Color->GetValue("fDiffuseMultiplier",MP.LP.diffusemultiplier);
	MP.Color->GetValue("fSpecularMultiplier",MP.LP.specularmultiplier);
	MP.Color->GetValue("fHDRDynamic",MP.LP.HDRDynamic);
	MP.Color->GetValue("clrDiffuse",MP.LP.colordiffuse);

	NeedParams=false;
}

//------------------------------------------------------------------------
void ModifySystem::GetNormalWeaponParams(string weaponName)
{
	XmlNodeRef WeaponParams=gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/WeaponParams.xml");
	XmlNodeRef weaponNode=WeaponParams->findChild(weaponName);
	if(weaponNode)
		weaponNode->findChild("BasicParams")->getChild(0)->getAttr("value",MP.BWP.weaponClass);

	IItemSystem *pItemSystem=g_pGame->GetIGameFramework()->GetIItemSystem();
	const char *itemfile=pItemSystem->GetItemParamsDescriptionFile(weaponName);
	XmlNodeRef item_root = gEnv->pSystem->LoadXmlFromFile(itemfile);
	XmlNodeRef firemodes=item_root->findChild("firemodes");
	XmlNodeRef defaultfiremode=firemodes->getChild(0);
	XmlNodeRef fire=defaultfiremode->getChild(0);

	int maxChild=fire->getChildCount();
	for(int i=0;i<maxChild;i++)
	{
		XmlString tattr;
		fire->getChild(i)->getAttr("name",tattr);
		if(tattr == "rate")
		{
			fire->getChild(i)->getAttr("value",MP.BWP.firerate);
			continue;
		}
		else if(tattr == "damage")
		{
			fire->getChild(i)->getAttr("value",MP.BWP.player_damage);
			continue;
		}
		else if(tattr == "ai_vs_player_damage")
		{
			fire->getChild(i)->getAttr("value",MP.BWP.ai_damage);
			continue;
		}
		else if(tattr == "clip_size")
		{
			fire->getChild(i)->getAttr("value",MP.BWP.clip_size);
			continue;
		}
	}

	if(MP.BWP.weaponClass == "Shotgun")
	{
		XmlNodeRef shotgun=defaultfiremode->findChild("shotgun");
		shotgun->getChild(0)->getAttr("value",MP.SGP.pellets);
		shotgun->getChild(1)->getAttr("value",MP.SGP.pellet_damage);
	}

	NeedParams=false;
}


//------------------------------------------------------------------------
void ModifySystem::SetPlayerWeaponClass()
{
	if(MP.PlayerWeaponIndex==0)
		MP.BWP.weaponClass="Rifle";
	if(MP.PlayerWeaponIndex==1)
		MP.BWP.weaponClass="Shotgun";
	if(MP.PlayerWeaponIndex==2)
		MP.BWP.weaponClass="Pistol";
}

//------------------------------------------------------------------------
void ModifySystem::AfterModify(const int type)
{
	if(type == eECT_BasciEntity)
	{
		XmlNodeRef default_root=gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Vehicles/Implementations/Xml/Default/HMMWV.xml");
		default_root->saveToFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Vehicles/Implementations/Xml/HMMWV.xml");
		MP.sharedName+=static_cast<float>(0.001);
		MP.needupdate=true;
		MP.VehicleInTime=false;
	}
	if(type == eECT_AI_Weapon || type == eECT_Player_Weapon)
	{
		//Rifle
		XmlNodeRef default_weapon_root=gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/Weapon_Default/Rifle.xml");
		XmlNodeRef default_ammo_root=gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/Ammo_Default/RifleBullet.xml");
		default_weapon_root->saveToFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/Weapons/Rifle.xml");
		default_ammo_root->saveToFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/Ammo/RifleBullet.xml");
		//Shotgun
		default_weapon_root=gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/Weapon_Default/Shotgun.xml");
		default_ammo_root=gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/Ammo_Default/ShotgunShell.xml");
		default_weapon_root->saveToFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/Weapons/Shotgun.xml");
		default_ammo_root->saveToFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/Ammo/ShotgunShell.xml");
		//Pistol
		default_weapon_root=gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/Weapon_Default/Pistol.xml");
		default_ammo_root=gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/Ammo_Default/PistolBullet.xml");
		default_weapon_root->saveToFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/Weapons/Pistol.xml");
		default_ammo_root->saveToFile(PathUtil::GetGameFolder() + "/Scripts/Entities/Items/Ammo/PistolBullet.xml");
		ReloadItems();
		RestoreInventory();
		MP.WeaponInTime=false;
	}
}

//------------------------------------------------------------------------
void ModifySystem::CalculateCostandTime(const int type,const long index/* =0 */)
{
	if(type == eECT_BasciEntity)
	{
		MP.cost=0;
		MP.cost+=(int)(MP.BPP.mass-defMP.BPP.mass)/2;
		MP.cost+=(int)(MP.BPP.density-defMP.BPP.density)/2;
		if(MP.BPP.rigid!=defMP.BPP.rigid)
			MP.cost+=20;
		return;
	}
	if(type == eECT_DestroyableObject)
	{
		MP.cost=0;
		MP.cost+=(int)abs(MP.BPP.mass-defMP.BPP.mass)/2;
		MP.cost+=(int)abs(MP.BPP.density-defMP.BPP.density)/2;
		if(MP.BPP.rigid!=defMP.BPP.rigid)
			MP.cost+=20;
		if(MP.EP.explode!=defMP.EP.explode)
			MP.cost+=50;
		MP.cost+=(int)abs(MP.EP.effectscale-defMP.EP.effectscale)/2;
		MP.cost+=(int)abs(MP.EP.minradius-defMP.EP.minradius)*8;
		MP.cost+=(int)abs(MP.EP.radius-defMP.EP.radius)*10;
		MP.cost+=(int)abs(MP.EP.minphysradius-defMP.EP.minphysradius)*5;
		MP.cost+=(int)abs(MP.EP.physradius-defMP.EP.physradius)*8;
		MP.cost+=(int)abs(MP.EP.pressure-defMP.EP.pressure)/2;
		MP.cost+=(int)abs(MP.EP.damage-defMP.EP.damage)/2;
		if(MP.EP.invulnerable!=defMP.EP.invulnerable)
			MP.cost+=50;
		MP.cost+=(int)abs(MP.EP.health-defMP.EP.health)/2;
		if(MP.defaultxml != defMP.defaultxml)
			MP.cost+=100;
		return;
	}
	if(type == eECT_Vehicles)
	{
		VehicleModifyLastTime=defVehicleModifyLastTime;
		MP.cost=0;
		MP.cost+=(int)abs(MP.LVP.acceleration-defMP.LVP.acceleration)*10;
		MP.cost+=(int)abs(MP.LVP.decceleration-defMP.LVP.decceleration)*5;
		MP.cost+=(int)abs(MP.LVP.topSpeed-defMP.LVP.topSpeed)*10;
		MP.cost+=(int)abs(MP.LVP.reverseSpeed-defMP.LVP.reverseSpeed)*5;
		MP.cost+=(int)abs(MP.LVP.velocitymultiplier-defMP.LVP.velocitymultiplier)*5;
		MP.cost+=(int)abs(MP.LVP.degree-defMP.LVP.degree)*1;
		MP.cost+=(int)abs(MP.LVP.backfriction-defMP.LVP.backfriction)*10;
		MP.cost+=(int)abs(MP.LVP.frontfriction-defMP.LVP.frontfriction)*10;
		if(MP.defaultxml != defMP.defaultxml)
			MP.cost+=100;
		VehicleModifyLastTime-=static_cast<float>(abs(MP.LVP.acceleration-defMP.LVP.acceleration)*2);
		VehicleModifyLastTime-=static_cast<float>(abs(MP.LVP.decceleration-defMP.LVP.decceleration)*1);
		VehicleModifyLastTime-=static_cast<float>(abs(MP.LVP.topSpeed-defMP.LVP.topSpeed)*1);
		VehicleModifyLastTime-=static_cast<float>(abs(MP.LVP.reverseSpeed-defMP.LVP.reverseSpeed)*1);
		VehicleModifyLastTime-=static_cast<float>(abs(MP.LVP.velocitymultiplier-defMP.LVP.velocitymultiplier)*0.1);
		VehicleModifyLastTime-=static_cast<float>(abs(MP.LVP.degree-defMP.LVP.degree)*0.01);
		VehicleModifyLastTime-=static_cast<float>(abs(MP.LVP.backfriction-defMP.LVP.backfriction)*3);
		VehicleModifyLastTime-=static_cast<float>(abs(MP.LVP.frontfriction-defMP.LVP.frontfriction)*3);
		return;
	}
	if(type == eECT_Light)
	{
		MP.cost=0;
		MP.cost+=(int)abs(MP.BPP.mass-defMP.BPP.mass)/2;
		MP.cost+=(int)abs(MP.BPP.density-defMP.BPP.density)/2;
		if(MP.BPP.rigid!=defMP.BPP.rigid)
			MP.cost+=20;
		if(MP.EP.explode!=defMP.EP.explode)
			MP.cost+=50;
		MP.cost+=(int)abs(MP.EP.effectscale-defMP.EP.effectscale)/2;
		MP.cost+=(int)abs(MP.EP.minradius-defMP.EP.minradius)*8;
		MP.cost+=(int)abs(MP.EP.radius-defMP.EP.radius)*10;
		MP.cost+=(int)abs(MP.EP.minphysradius-defMP.EP.minphysradius)*5;
		MP.cost+=(int)abs(MP.EP.physradius-defMP.EP.physradius)*8;
		MP.cost+=(int)abs(MP.EP.pressure-defMP.EP.pressure)/2;
		MP.cost+=(int)abs(MP.EP.damage-defMP.EP.damage)/2;
		if(MP.EP.invulnerable!=defMP.EP.invulnerable)
			MP.cost+=50;
		MP.cost+=(int)abs(MP.EP.health-defMP.EP.health)/2;
		if(MP.LP.turn_on!=defMP.LP.turn_on)
			MP.cost+=20;
		MP.cost+=(int)abs(MP.LP.lightradius-defMP.LP.lightradius)*5;
		MP.cost+=(int)abs(MP.LP.lightstyle-defMP.LP.lightstyle)*1;
		MP.cost+=(int)abs(MP.LP.diffusemultiplier-defMP.LP.diffusemultiplier)*30;
		MP.cost+=(int)abs(MP.LP.specularmultiplier-defMP.LP.specularmultiplier)*30;
		MP.cost+=(int)abs(MP.LP.HDRDynamic-defMP.LP.HDRDynamic)*30;
		MP.cost+=(int)abs(MP.LP.colordiffuse.x-defMP.LP.colordiffuse.x)*50;
		MP.cost+=(int)abs(MP.LP.colordiffuse.y-defMP.LP.colordiffuse.y)*50;
		MP.cost+=(int)abs(MP.LP.colordiffuse.z-defMP.LP.colordiffuse.z)*50;
		return;
	}
	if(type == eECT_AI_Weapon || type == eECT_Player_Weapon)
	{
		if(MP.BWP.weaponClass == "Rifle")
		{
			WeaponModifyLastTime=defWeaponModifyLastTime;
			MP.cost=0;
			MP.cost+=(int)abs(MP.BWP.firerate-defMP.BWP.firerate)*2;
			MP.cost+=(int)abs(MP.BWP.player_damage-defMP.BWP.player_damage)*4;
			MP.cost+=(int)abs(MP.BWP.ai_damage-defMP.BWP.ai_damage)*4;
			MP.cost+=(int)abs(MP.BWP.clip_size-defMP.BWP.clip_size)*10;
			if(MP.defaultxml != defMP.defaultxml)
				MP.cost+=100;
			WeaponModifyLastTime-=static_cast<float>(abs(MP.BWP.firerate-defMP.BWP.firerate)*0.01);
			WeaponModifyLastTime-=static_cast<float>(abs(MP.BWP.player_damage-defMP.BWP.player_damage)*0.1);
			WeaponModifyLastTime-=static_cast<float>(abs(MP.BWP.ai_damage-defMP.BWP.ai_damage)*0.1);
			WeaponModifyLastTime-=static_cast<float>(abs(MP.BWP.clip_size-defMP.BWP.clip_size)*0.2);
			return;
		}
		if(MP.BWP.weaponClass == "Shotgun")
		{
			WeaponModifyLastTime=defWeaponModifyLastTime;
			MP.cost=0;
			MP.cost+=(int)abs(MP.BWP.firerate-defMP.BWP.firerate)*4;
			MP.cost+=(int)abs(MP.SGP.pellets-defMP.SGP.pellets)*10;
			MP.cost+=(int)abs(MP.BWP.player_damage-defMP.BWP.player_damage)*10;
			MP.cost+=(int)abs(MP.BWP.clip_size-defMP.BWP.clip_size)*5;
			if(MP.defaultxml != defMP.defaultxml)
				MP.cost+=100;
			WeaponModifyLastTime-=static_cast<float>(abs(MP.BWP.firerate-defMP.BWP.firerate)*0.01);
			WeaponModifyLastTime-=static_cast<float>(abs(MP.SGP.pellets-defMP.SGP.pellets)*1);
			WeaponModifyLastTime-=static_cast<float>(abs(MP.BWP.player_damage-defMP.BWP.player_damage)*0.1);
			WeaponModifyLastTime-=static_cast<float>(abs(MP.BWP.clip_size-defMP.BWP.clip_size)*0.2);
			return;
		}
		if(MP.BWP.weaponClass == "Pistol")
		{
			WeaponModifyLastTime=defWeaponModifyLastTime;
			MP.cost=0;
			MP.cost+=(int)abs(MP.BWP.firerate-defMP.BWP.firerate)*4;
			MP.cost+=(int)abs(MP.BWP.player_damage-defMP.BWP.player_damage)*3;
			MP.cost+=(int)abs(MP.BWP.ai_damage-defMP.BWP.ai_damage)*3;
			MP.cost+=(int)abs(MP.BWP.clip_size-defMP.BWP.clip_size)*5;
			if(MP.defaultxml != defMP.defaultxml)
				MP.cost+=100;
			WeaponModifyLastTime-=static_cast<float>(abs(MP.BWP.firerate-defMP.BWP.firerate)*0.01);
			WeaponModifyLastTime-=static_cast<float>(abs(MP.BWP.player_damage-defMP.BWP.player_damage)*0.1);
			WeaponModifyLastTime-=static_cast<float>(abs(MP.BWP.ai_damage-defMP.BWP.ai_damage)*0.1);
			WeaponModifyLastTime-=static_cast<float>(abs(MP.BWP.clip_size-defMP.BWP.clip_size)*0.2);
			return;
		}
	}
}

//------------------------------------------------------------------------
void ModifySystem::ReloadItems()
{
	/*IItemSystem* pItemSystem = g_pGame->GetIGameFramework()->GetIItemSystem();
	pItemSystem->PreReload();

	g_pGame->GetGameSharedParametersStorage()->ResetItemParameters();
	g_pGame->GetGameSharedParametersStorage()->ResetWeaponParameters();
	g_pGame->GetGameSharedParametersStorage()->GetItemResourceCache().FlushCaches();

	g_pGame->GetWeaponSystem()->Reload();
	pItemSystem->PostReload();*/
}

//------------------------------------------------------------------------
void ModifySystem::Shutdown()
{
	SetStartFunction(false);
	ResetType();
	ClearMP();

	NeedParams=true;
	HasApply=true;
	EntityLocked=false;
	OnLocking=false;
	timescale_timer=0;
	modifytype=eECT_Last;

	g_pGame->GetConnectMode()->SetLevel(eConnectMode::eCLevel::eCLevel_ChooseSystem);
}

//------------------------------------------------------------------------
void ModifySystem::ApplyChange(const long index /* = 0 */)
{
	//System may handle this but not robot

	/*g_pGame->GetSpawnSystem()->AddtoSpawnEntitySet(MP.MEntity->GetId());
	g_pGame->GetDeleteSystem()->AddToDeleteRecovery(MP.MEntity,false);*/
	//IModifier *pModifier = Modifiers->GetModifier(ModifySlot[index]);
	IModifier *pModifier = Modifiers->GetModifier(index);
	if(pModifier)
	{
		pModifier->Apply(MP.MEntity);
		if(bMultiModify)
		{
			std::list<EntityId>::iterator iter = MultiModifyList.begin();
			std::list<EntityId>::iterator end = MultiModifyList.end();
			while(iter != end)
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(*iter);
				pModifier->Apply(pEntity);
				++iter;
			}
		}
		//CloseMenu();
	}
	else
	{
		if(MP.MEntity)
		{
			IEntityRenderProxy *proxy=static_cast<IEntityRenderProxy*>(MP.MEntity->GetProxy(ENTITY_PROXY_RENDER));
			if(proxy)
				proxy->SetHUDSilhouettesParams(0,0,0,0);
		}
	}
	/*if(MP.cost > g_pGame->GetWorldStability()->Getstability())
		CryLogAlways("You didn't have enough Stability!");
	//--------BasicEntity--------
	if(modifytype == eECT_BasciEntity)
		ApplyBasicParams(index);
	//--------DestroyableObject--------
	if(modifytype == eECT_DestroyableObject)
		ApplyDestroyableParams(index);
	//--------Vehicles--------
	if(modifytype == eECT_Vehicles)
		ApplyLandVehicleParams(index);
	//--------Light--------
	if(modifytype == eECT_Light)
		ApplyLightParams(index);
	//--------Weapon--------
	if(modifytype == eECT_Player_Weapon || modifytype == eECT_AI_Weapon)
		ApplyBasicWeaponParams(index);
	PostApply();
	g_pGame->GetConnectMode()->SetLevel(eConnectMode::eCLevel::eCLevel_ChooseSystem);*/
}

//------------------------------------------------------------------------
void ModifySystem::PostApply()
{
	HasApply=true;
	EntityLocked=false;
	OnLocking=false;
	timescale_timer=0;
	modifytype=eECT_Last;
	ClearMP();
}

//------------------------------------------------------------------------
void ModifySystem::LockEntity()
{
	defMP=MP;
	StartFunction=true;
	VehicleModifyLastTime=defVehicleModifyLastTime;
	WeaponModifyLastTime=defWeaponModifyLastTime;
	Locking_timer=0;
	Locking_progress=0;
	Locking_rate=100/Locking_time;
	OnLocking=true;
	HasLockedOnce=true;
}

//------------------------------------------------------------------------
void ModifySystem::FindSameEntityClassInRange(IEntity *pEntity)
{
	AABB parentAABB;
	AABB childAABB;
	float range = 5;
	Vec3 pos = pEntity->GetPos();
	IEntityClass *pClass =pEntity->GetClass();
	IPhysicalEntity **nearbyEntities;
	pEntity->GetLocalBounds(parentAABB);
	int n=gEnv->pPhysicalWorld->GetEntitiesInBox(pos-Vec3(range),pos+Vec3(range),nearbyEntities,ent_all);
	for(int i=0;i<n;++i)
	{
		IEntity *pAnotherEntity = gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
		if(pAnotherEntity)
		{
			IEntityClass *pNearByClass = pAnotherEntity->GetClass();
			if(pNearByClass == pClass)
			{
				pAnotherEntity->GetLocalBounds(childAABB);
				float parent_radius = parentAABB.GetRadius();
				float child_radius = childAABB.GetRadius();
				float ratio = parent_radius / child_radius;
				if(ratio > 0.5 && ratio < 1.5)
					MultiModifyList.push_back(pAnotherEntity->GetId());
			}
		}
	}
}

//------------------------------------------------------------------------
void ModifySystem::UnLockEntity()
{
	OnLocking=false;
	EntityLocked=false;
	ResetType();
	ClearMP();
	NeedParams=true;
	g_pGame->GetConnectMode()->SetLevel(eConnectMode::eCLevel::eCLevel_Modify_ChooseSlot);
}

void ModifySystem::ShowMenu()
{
	if(!pElement)
	{
		pElement = gEnv->pFlashUI->GetUIElement("Modify_Menu");
		pElement->AddEventListener(&UIListener,"Modify_Listener");
	}
	if(pElement)
	{
		pElement->SetVisible(true);
		g_pGame->Actions().FilterNoMouse()->Enable(true);
		pElement->CallFunction("SetSelectionMode",SUIArguments::Create((int)bMultiModify));
	}
}

void ModifySystem::CloseMenu()
{
	if(!pElement)
	{
		pElement = gEnv->pFlashUI->GetUIElement("Modify_Menu");
		pElement->AddEventListener(&UIListener,"Modify_Listener");
	}
	if(pElement)
	{
		pElement->SetVisible(false);
		g_pGame->GetConnectMode()->PopLevel();
		pElement->CallFunction("ClearModifiers",SUIArguments());
		pElement->CallFunction("ClearClassification",SUIArguments());
		g_pGame->Actions().FilterNoMouse()->Enable(false);
		if(bMultiModify)
		{
			std::list<EntityId>::iterator iter = MultiModifyList.begin();
			std::list<EntityId>::iterator end = MultiModifyList.end();
			while(iter != end)
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(*iter);
				IEntityRenderProxy *proxy=static_cast<IEntityRenderProxy*>(pEntity->GetProxy(ENTITY_PROXY_RENDER));
				if(proxy)
					proxy->SetHUDSilhouettesParams(0,0,0,0);
				++iter;
			}
			MultiModifyList.clear();
		}
		else
		{
			if(MP.MEntity)
			{
				IEntityRenderProxy *proxy=static_cast<IEntityRenderProxy*>(MP.MEntity->GetProxy(ENTITY_PROXY_RENDER));
				if(proxy)
					proxy->SetHUDSilhouettesParams(0,0,0,0);
			}
		}
	}
	MP.MEntity = NULL;
}

void ModifySystem::SetSelectionMode(bool _mode)
{
	if(bMultiModify == _mode)
		return;
	else
	{
		if(bMultiModify == true)
		{
			bMultiModify = false;
			if(!MultiModifyList.empty())
			{
				std::list<EntityId>::iterator iter = MultiModifyList.begin();
				std::list<EntityId>::iterator end = MultiModifyList.end();
				while(iter != end)
				{
					IEntity *pEntity = gEnv->pEntitySystem->GetEntity(*iter);
					IEntityRenderProxy *proxy=static_cast<IEntityRenderProxy*>(pEntity->GetProxy(ENTITY_PROXY_RENDER));
					if(proxy)
						proxy->SetHUDSilhouettesParams(0,0,0,0);
					++iter;
				}
				MultiModifyList.clear();
			}
			IEntityRenderProxy *proxy=static_cast<IEntityRenderProxy*>(MP.MEntity->GetProxy(ENTITY_PROXY_RENDER));
			if(proxy)
				proxy->SetHUDSilhouettesParams(255,255,255,255);
		}
		else
		{
			bMultiModify = true;
			FindSameEntityClassInRange(MP.MEntity);
			std::list<EntityId>::iterator iter = MultiModifyList.begin();
			std::list<EntityId>::iterator end = MultiModifyList.end();
			while(iter != end)
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(*iter);
				IEntityRenderProxy *proxy=static_cast<IEntityRenderProxy*>(pEntity->GetProxy(ENTITY_PROXY_RENDER));
				if(proxy)
					proxy->SetHUDSilhouettesParams(255,255,255,255);
				++iter;
			}
		}
	}
}
void ModifySystem::InsertModifierToUIMenu(IEntityClass *pEntityClass)
{
	if(!pElement)
	{
		pElement = gEnv->pFlashUI->GetUIElement("Modify_Menu");
		pElement->AddEventListener(&UIListener,"Modify_Listener");
	}
	if(pElement)
	{
		if(pEntityClass == preLoad.pBasicEntityClass)
		{
			InsertModifier(MASS_10_PERCENT, RIGID_OFF);

			InsertPhysicsClassification();

		}
		else if(pEntityClass == preLoad.pDestroyableObjectClass)
		{
			InsertModifier(MASS_10_PERCENT, HEALTH_10_PERCENT);
			InsertPhysicsClassification();
			InsertDestroyableClassification();
		}
		else if(pEntityClass == preLoad.pLightClass)
		{

		}
		else if(pEntityClass == preLoad.pHMMWV)
		{
			InsertModifier(ACCELERATION_10_PERCENT, ROTATION_TURNTO_180);
			InsertVehicleClassification();
		}
		else if(pEntityClass == preLoad.pHumanClass)
		{
			InsertModifier(LEG_DISABLE_10S, LEG_WEAK_10S);
			InsertModifier(FIRERATE_10_PERCENT, CLIPSIZE_10_PERCENT);
			InsertHumanClassification();
		}
		else if(pEntityClass == preLoad.pTrap)
		{
			InsertModifier(TRAP_RADIUS_30, TRAP_DAMAGECOV_DISABLE);
			InsertTrapClassification();
		}
		else if(pEntityClass == preLoad.pLeakHole)
		{
			InsertModifier(LEAKHOLE_CD_30, LEAKHOLE_LENGTH_50);
			InsertLeakHoleClassification();
		}
		else if(pEntityClass == preLoad.pForceDevice)
		{
			InsertModifier(MASS_10_PERCENT, RIGID_OFF);
			InsertModifier(FORCEDEVICE_HITLIMITS_200, FORCEDEVICE_FORCE_200);
			InsertForceDeviceClassification();
		}
		else if(pEntityClass == preLoad.pPowerBall)
		{
			InsertModifier(MASS_10_PERCENT, RIGID_OFF);
			InsertModifier(POWERBALL_DAMAGE_200, POWERBALL_CRITICAL_200);
			InsertPowerBallClassification();
		}
		else if(pEntityClass == preLoad.pForceField)
		{
			InsertModifier(MASS_10_PERCENT, RIGID_OFF);
			InsertModifier(FORCEFIELD_RADIUS_150, FORCEFIELD_SCALE_50);
			InsertForceFieldClassification();
		}

		pElement->CallFunction("Draw",SUIArguments());
		pElement->CallFunction("DrawClassification",SUIArguments());
	}
}
 
void ModifySystem::InsertModifier(int _begin, int _end)
{
	for(int i = _begin;i <= _end;++i)
	{
		if(IModifier *pModifier = Modifiers->GetModifier(i))
		{
			bool percentage = pModifier->Percentage();
			float fvalue = pModifier->GetfValue();
			int ivalue = pModifier->GetiValue();
			ModifyID Id = pModifier->GetModifierId();
			const int cost = pModifier->GetCost();
			const int type = pModifier->GetType();
			SUIArguments args;

			//Id and Classification
			args.AddArgument(Id);
			args.AddArgument(type);

			//Modifier_State : Increase pr Decrease
			if(percentage && fvalue > 1)
			{
				args.AddArgument(1);
				fvalue *= 100;
			}
			else if(percentage && fvalue < 1)
			{
				args.AddArgument(2);
				fvalue *= 100;
			}
			else if(!percentage)
				args.AddArgument(3);

			//Value and Cost
			if(fvalue != ivalue)
				args.AddArgument(fvalue);
			else
				args.AddArgument(ivalue);

			pElement->CallFunction("InsertModifier",args);
		}
	}
}


void ModifySystem::InsertPhysicsClassification()
{
	SUIArguments args;
	int type = eMODT_Physics;
	args.AddArgument(type);
	args.AddArgument("Physics");
	pElement->CallFunction("AddClassification",args);
}

void ModifySystem::InsertDestroyableClassification()
{
	SUIArguments args;
	int type = eMODT_Damage;
	args.AddArgument(type);
	args.AddArgument("Damage");
	pElement->CallFunction("AddClassification",args);
	args.Clear();
	type = eMODT_Health;
	args.AddArgument(type);
	args.AddArgument("Health");
	pElement->CallFunction("AddClassification",args);
}

void ModifySystem::InsertVehicleClassification()
{
	SUIArguments args;
	int type = eMODT_Kinetic;
	args.AddArgument(type);
	args.AddArgument("Kinetic");
	pElement->CallFunction("AddClassification",args);
	args.Clear();
	type = eMODT_Control;
	args.AddArgument(type);
	args.AddArgument("Control");
	pElement->CallFunction("AddClassification",args);
}

void ModifySystem::InsertHumanClassification()
{
	SUIArguments args;
	int type = eMODT_Disable;
	args.AddArgument(type);
	args.AddArgument("Disable");
	pElement->CallFunction("AddClassification",args);
	args.Clear();
	type = eMODT_Weak;
	args.AddArgument(type);
	args.AddArgument("Weak");
	pElement->CallFunction("AddClassification",args);
	args.Clear();
	type = eMODT_Control;
	args.AddArgument(type);
	args.AddArgument("Control");
	pElement->CallFunction("AddClassification",args);
	args.Clear();
	type = eMODT_Damage;
	args.AddArgument(type);
	args.AddArgument("Damage");
	pElement->CallFunction("AddClassification",args);
}

void ModifySystem::InsertTrapClassification()
{
	SUIArguments args;
	int type = eMODT_Control;
	args.AddArgument(type);
	args.AddArgument("Control");
	pElement->CallFunction("AddClassification",args);
	args.Clear();
	type = eMODT_Disable;
	args.AddArgument(type);
	args.AddArgument("Type");
	pElement->CallFunction("AddClassification",args);
}

void ModifySystem::InsertLeakHoleClassification()
{
	SUIArguments args;
	int type = eMODT_Control;
	args.AddArgument(type);
	args.AddArgument("Control");
	pElement->CallFunction("AddClassification",args);
	args.Clear();
	type = eMODT_Damage;
	args.AddArgument(type);
	args.AddArgument("Damage");
	pElement->CallFunction("AddClassification",args);
}

void ModifySystem::InsertForceDeviceClassification()
{
	SUIArguments args;
	int type = eMODT_Control;
	args.AddArgument(type);
	args.AddArgument("Control");
	pElement->CallFunction("AddClassification",args);
	args.Clear();
	type = eMODT_Damage;
	args.AddArgument(type);
	args.AddArgument("Type");
	pElement->CallFunction("AddClassification",args);
}

void ModifySystem::InsertPowerBallClassification()
{
	SUIArguments args;
	int type = eMODT_Damage;
	args.AddArgument(type);
	args.AddArgument("Damage");
	pElement->CallFunction("AddClassification",args);
}

void ModifySystem::InsertForceFieldClassification()
{
	SUIArguments args;
	int type = eMODT_Control;
	args.AddArgument(type);
	args.AddArgument("Control");
	pElement->CallFunction("AddClassification",args);
}




void CModifyMenuListener::OnUIEvent( IUIElement* pSender, const SUIEventDesc& event, const SUIArguments& args )
{
	ModifySystem *pSystem = g_pGame->GetModifySystem();
	if ( strcmp(event.sDisplayName, "RollOver") == 0 )
	{
		int Id;
		args.GetArg( 0, Id);
		if(pSystem)
		{
			IModifier *pModifier = pSystem->GetModifierManager()->GetModifier(Id);
			if(pModifier)
			{
				float prev_fvalue;
				float fvalue;
				string desc = pModifier->GetDesc();
				int cost = pModifier->GetCost();
				pModifier->GetPrevValue(pSystem->GetModifiedEntity(), prev_fvalue,fvalue);

				if(Id < FIRERATE_10_PERCENT || (Id >= TRAP_RADIUS_30 && Id < LEG_DISABLE_10S))
				{
					SUIArguments args;
					args.AddArgument(desc);
					args.AddArgument(cost);
					args.AddArgument(prev_fvalue);
					args.AddArgument(fvalue);
					pSystem->GetModifyMenu()->CallFunction("SetText",args);
				}
				else
				{
					float prev_last_time;
					float last_time;
					pModifier->GetLastTime(pSystem->GetModifiedEntity(), prev_last_time,last_time);
					SUIArguments args;
					args.AddArgument(desc);
					args.AddArgument(cost);
					args.AddArgument(prev_fvalue);
					args.AddArgument(fvalue);
					args.AddArgument(prev_last_time);
					args.AddArgument(last_time);
					pSystem->GetModifyMenu()->CallFunction("SetTextWithTimer",args);
				}
				pSystem->GetModifyMenu()->CallFunction("UpdateSta",SUIArguments::Create(g_pGame->GetWorldStability()->Getstability()));
			}
		}
	}
	else if( strcmp(event.sDisplayName, "Selected") == 0 )
	{
		int Id;
		args.GetArg( 0, Id);
		if(pSystem)
		{
			pSystem->ApplyChange(Id);
		}
	}

	else if( strcmp(event.sDisplayName, "ChangeSelectionMode") == 0 )
	{
		bool mode;
		args.GetArg( 0, mode);
		if(pSystem)
		{
			pSystem->SetSelectionMode(mode);
		}
	}
}