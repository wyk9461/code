#include "StdAfx.h"
#include "Buff.h"
#include "ModifySlot/IModifier.h"

BuffManager::BuffManager()
{
	iter=Buffs.begin();
	pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
};

BuffManager &BuffManager::TheBuffManager()
{
	static BuffManager Manager;
	return Manager;
}

BuffManager::~BuffManager()
{
	Buffs.clear();	
}


void BuffManager::Update(const float frameTime)
{
	if(!Buffs.empty())
	{
		BuffMap::iterator end=Buffs.end();
		iter=Buffs.begin();
		while(iter != end)
		{
			Buff &t_Buff=iter->second;
			if(t_Buff.tick_timer < t_Buff.Last_Time)
				t_Buff.tick_timer+=frameTime;
			else
				t_Buff.Available=false;
			pElement->CallFunction("UpdateBuff", SUIArguments::Create(frameTime));
			++iter;
		}
	}
}

void BuffManager::AddUIBuff(float _lasttime, int _type)
{
	if(pElement)
	{
		SUIArguments args;
		args.AddArgument(_lasttime);
		args.AddArgument(_type);
		pElement->CallFunction("AddBuff",args);
	}
}


void BuffManager::GenerateUIMessage(int type, float effect)
{
	switch (type)
	{
	case Damage_Buff:
		{
			SUIArguments args;

			string temp_str;
			char temp[5];
			sprintf(temp,"%.1f",effect*100);
			temp_str.reserve(30);
			temp_str = "Damage up for ";
			temp_str += temp;
			temp_str += "%";

			args.AddArgument(temp_str.c_str());
			args.AddArgument(5);

			pElement->CallFunction("AddMessage",args);
			break;
		}
	default:
		break;
	}
}

void BuffManager::CreateHealthBuff(IEntity *pEntity)
{
	IActor *pActor=g_pGame->GetIGameFramework()->GetClientActor();
	if(pActor)
	{
		float fValue=50;
		float curHealth=pActor->GetHealth();
		float newHealth=curHealth+fValue;
		pActor->SetHealth(newHealth);
	}
}

void BuffManager::CreateExplosionBuff(IEntity *pEntity)
{

}

void BuffManager::CreateDamageBuff(IEntity *pEntity)
{
	SmartScriptTable root=pEntity->GetScriptTable();
	SmartScriptTable Explosion=MScriptBind::GetExploTable(root);
	float damage;
	Explosion->GetValue("Damage",damage);
	float fValue=static_cast<float>(damage*0.0001);
	if(fValue > 0.5)
		fValue=0.5;
	Buff DamageBuff;
	DamageBuff.Available=true;
	if(Buffs.count(Damage_Buff))
	{
		if(Buffs[Damage_Buff].fValue < fValue)
			DamageBuff.fValue=fValue;
		else
			DamageBuff.fValue=Buffs[Damage_Buff].fValue;
		if(Buffs[Damage_Buff].Last_Time < 60)
			DamageBuff.Last_Time=60;
		else
			DamageBuff.Last_Time=Buffs[Damage_Buff].Last_Time;		//May be no use.Just in case.
	}
	else
	{
		DamageBuff.fValue=fValue;
		DamageBuff.Last_Time=60;
	}
	DamageBuff.tick_timer=0;
	Buffs[Damage_Buff]=DamageBuff;
	AddUIBuff(DamageBuff.Last_Time,Damage_Buff);
	GenerateUIMessage(Damage_Buff,fValue);
}


void BuffManager::CreateExplosionEffect_Buff(IEntity *pEntity)
{
	SmartScriptTable root=pEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable destruction;
	root->GetValue("Properties",props);
	props->GetValue("Destruction",destruction);
	float damage;
	destruction->GetValue("Damage",damage);
	float fValue=static_cast<float>(damage*0.1);
	Buff ExplosionEffect;

	ExplosionEffect.Available=true;
	if(Buffs.count(Damage_Buff))
	{
		if(Buffs[Damage_Buff].fValue < fValue)
			ExplosionEffect.fValue=fValue;
		else
			ExplosionEffect.fValue=Buffs[Damage_Buff].fValue;
		if(Buffs[Damage_Buff].Last_Time < 60)
			ExplosionEffect.Last_Time=60;
		else
			ExplosionEffect.Last_Time=Buffs[Damage_Buff].Last_Time;		//May be no use.Just in case.
	}
	else
	{
		ExplosionEffect.fValue=fValue;
		ExplosionEffect.Last_Time=60;
	}
	ExplosionEffect.tick_timer=0;
	Buffs[ExplosionEffect_Buff]=ExplosionEffect;
	AddUIBuff(ExplosionEffect.Last_Time,ExplosionEffect_Buff);
}


void BuffManager::CreateFireEffect_Buff(IEntity *pEntity)
{
	SmartScriptTable root=pEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable destruction;
	root->GetValue("Properties",props);
	props->GetValue("Destruction",destruction);
	float damage;
	destruction->GetValue("Damage",damage);
	float fValue=static_cast<float>(damage*0.1);
	Buff FireEffect;

	FireEffect.Available=true;
	if(Buffs.count(Damage_Buff))
	{
		if(Buffs[Damage_Buff].fValue < fValue)
			FireEffect.fValue=fValue;
		else
			FireEffect.fValue=Buffs[Damage_Buff].fValue;
		if(Buffs[Damage_Buff].Last_Time < 60)
			FireEffect.Last_Time=60;
		else
			FireEffect.Last_Time=Buffs[Damage_Buff].Last_Time;		//May be no use.Just in case.
	}
	else
	{
		FireEffect.fValue=fValue;
		FireEffect.Last_Time=60;
	}
	FireEffect.tick_timer=0;
	Buffs[FireEffect_Buff]=FireEffect;
	AddUIBuff(FireEffect.Last_Time,FireEffect_Buff);
}

void BuffManager::CreateDataPile_Buff(IEntity *pEntity)
{
	Buff Pile;
	Pile.Available = true;
	Pile.Last_Time = 60;
	Pile.tick_timer = 0;
	Pile.fValue = 200;

	Buffs[DataPile_Buff] = Pile;
	AddUIBuff(Pile.Last_Time,DataPile_Buff);
}


void BuffManager::CreateCriticalStrike_Buff(float multi)
{
	Buff CriticalStrike;
	CriticalStrike.Available = true;
	CriticalStrike.Last_Time = 30;
	CriticalStrike.tick_timer = 0;
	CriticalStrike.fValue = multi;

	Buffs[CriticalStrike_Buff] = CriticalStrike;
	AddUIBuff(CriticalStrike.Last_Time,CriticalStrike_Buff);
}