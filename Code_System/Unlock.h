#ifndef _UNLOCK_H_
#define _UNLOCK_H_

#include "IXml.h"
#include <map>
#include <list>

enum eUnlockType
{
	eUT_Entity,
	eUT_Projectile,
};

class UnlockSystem
{
public:
	static UnlockSystem &Instance(){static UnlockSystem instance;return instance;}
	XmlString Unlock(int level);
private:
	UnlockSystem();
	XmlString UnlockEntity(XmlNodeRef &ref);
	XmlString UnlockProjectile(XmlNodeRef &ref);
	void ReadLockedSequences();

	typedef std::deque<int> LockedItemSequence;
	typedef std::map<int,LockedItemSequence> LockedSequenceMap;

	LockedSequenceMap LockedSequences;
	XmlNodeRef UnlockList;

	float multiPossibility;
};


#endif