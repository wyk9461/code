#include "StdAfx.h"
#include "ScreenEffectAdaptor.h"


ScreenEffectAdaptor::ScreenEffectAdaptor()
{

}

void ScreenEffectAdaptor::Update(float frameTime)
{
	if(!params_queue.empty())
	{
		std::map<effectId,effect_params>::iterator iter = params_queue.begin();
		std::map<effectId,effect_params>::iterator end = params_queue.end();
		while(iter != end)
		{
			effect_params &the_param = iter->second;
			if(the_param.shouldStart)
			{
				if(the_param.tick_timer < the_param.last_time)
				{
					gEnv->p3DEngine->SetPostEffectParam(the_param.effect_name, the_param.cur_value);
					the_param.cur_value += the_param.rate * frameTime;
					the_param.tick_timer += frameTime;
				}
				if(the_param.tick_timer >= the_param.last_time)
				{
					if(the_param.finished == false)
					{
						the_param.finished = true;
						the_param.shouldStart = false;
						gEnv->p3DEngine->SetPostEffectParam(the_param.effect_name, the_param.end_value);
						if(the_param.reverse && params_queue.count(the_param.reverseId))
						{
							params_queue[the_param.reverseId].finished = false;
						}
					}
					if(the_param.reverse && the_param.finished == true)
					{
						if(params_queue.count(the_param.reverseId))
						{
							if(params_queue[the_param.reverseId].finished == false)
							{
								AddToQueue(params_queue[the_param.reverseId],the_param.reverseId);
							}
						}
						else
						{
							effect_params new_param = the_param;
							new_param.cur_value = the_param.end_value;
							new_param.end_value = the_param.init_value;
							new_param.tick_timer = 0;
							new_param.reverse = false;
							new_param.shouldStart = true;
							AddToQueue(new_param,new_param.reverseId);
						}
					}
				}
			}
			++iter;
		}
	}
}

void ScreenEffectAdaptor::AddToQueue(effect_params params, effectId Id)
{
	if(params_queue.count(Id))
	{
		params_queue[Id].tick_timer = 0;
		params_queue[Id].cur_value = params_queue[Id].init_value;
		params_queue[Id].finished = false;
		params_queue[Id].shouldStart = true;
	}
	else
	{
		params.init_value = params.cur_value;
		params.rate = (params.end_value - params.cur_value) / params.last_time;
		params_queue.insert(std::map<effectId, effect_params>::value_type(Id,params));
	}
}