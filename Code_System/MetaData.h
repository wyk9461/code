#ifndef _METADATA_H_
#define _METADATA_H_

#include <set>

class MetaData
{
public:
	static MetaData &Instance(){static MetaData instance;return instance;}

	void GainMetaData(unsigned int amount);
	inline void CostMetaData(unsigned int amount){metadata -= amount;}

	inline unsigned int GetMetaDataCount(){return metadata;}
	inline unsigned int GetDataRecipientCount(){return DataRecipient;}

	inline void AddCheckedSetItem(EntityId id){CheckedSet.insert(id);}
	inline int isChecked(EntityId id){return CheckedSet.count(id);}
private:
	MetaData()
	:	metadata(0)
	,	DataRecipient(0){}
	unsigned int metadata;
	unsigned int DataRecipient;
	void FillRecipient();
	std::set<EntityId> CheckedSet;
};
#endif