#include "StdAfx.h"
#include "FloodModule.h"
#include "Code_System/BasicRayCast.h"


FloodModule::FloodModule()
	:	BaseAbility("Flood",100,eMT_Flood)
	,	pEffect(NULL)
	,	hasSet(false)
{
	SetTimer(10,5);
}

FloodModule::FloodModule(int _cost)
	:	BaseAbility("Flood",_cost,eMT_Flood)
	,	pEffect(NULL)
	,	hasSet(false)
{
	SetTimer(10,5);
}

void FloodModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	g_pGame->GetBasicRayCast()->ChangeFlag(ent_all);
	g_pGame->GetBasicRayCast()->ChangeDistance(1.5);

	Timer.timer=0;
	HasPreProcessed=true;
	hasSet=false;
	SEntitySpawnParams params;
	params.sName="Flood";
	params.vScale=Vec3(1,1,1);
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	pEffect=gEnv->pEntitySystem->SpawnEntity(params);
	SmartScriptTable proot=pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.PreFlood");
	Script::CallMethod(proot,"OnReset");
	BaseAbility::PreProcess(_rdata);
}

void FloodModule::Process(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	pEffect->SetPos(_rdata.hit.pt);
}

void FloodModule::PostProcess(Process_Data &_rdata)
{
	if(!hasSet)
	{
		BaseAbility::PostProcess(_rdata);
		pEffect->SetRotation(Quat::CreateRotationVDir(gEnv->pRenderer->GetCamera().GetViewdir()));
		HasPreProcessed=false; 
		finished=true;
		hasSet=true;
		SmartScriptTable proot=pEffect->GetScriptTable();
		SmartScriptTable pprops;
		proot->GetValue("Properties",pprops);
		pprops->SetValue("ParticleEffect","Code_System.Module_Effect.Flood");
		Script::CallMethod(proot,"OnReset");
		g_pGame->GetBasicRayCast()->Load();
	}
	else
	{
		if(Timer.timer >=Timer.Last_Time)
		{
			hasSet=false;
			gEnv->pEntitySystem->RemoveEntity(pEffect->GetId());
			pEffect=NULL;
		}
		Timer.timer+=_rdata.frameTime;
	}
}

void FloodModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}