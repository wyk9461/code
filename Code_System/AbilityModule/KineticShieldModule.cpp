#include "StdAfx.h"
#include "KineticShieldModule.h"

KineticShieldModule::KineticShieldModule()
	:	BaseAbility("KineticShield",100,eMT_Shield)
	,	pEffect(NULL)
	,	maxhealth(2000)
	,	health(2000)
	,	hasSet(false)
{
	SetTimer(10,30);
}

KineticShieldModule::KineticShieldModule(int _cost, float _health)
	:	BaseAbility("KineticShield",_cost,eMT_Shield)
	,	pEffect(NULL)
	,	maxhealth(_health)
	,	health(_health)
	,	hasSet(false)
{
	SetTimer(10,30);
}

void KineticShieldModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	HasPreProcessed=true;
	Timer.timer=0;
	hasSet=false;
	health=maxhealth;
	BaseAbility::PreProcess(_rdata);
}

void KineticShieldModule::Process(Process_Data &_rdata)
{

}

void KineticShieldModule::PostProcess(Process_Data &_rdata)
{
	BaseAbility::PostProcess(_rdata);
	if(!hasSet)
	{
		finished=true;
		hasSet=true;
		gEnv->p3DEngine->SetPostEffectParam( "VolumetricScattering_Amount", 1.0f );
		gEnv->p3DEngine->SetPostEffectParam( "VolumetricScattering_Tilling", 5.0f );
		gEnv->p3DEngine->SetPostEffectParam( "VolumetricScattering_Speed", 10.0f );
		gEnv->p3DEngine->SetPostEffectParamVec4( "clr_VolumetricScattering_Color", Vec4(1.0f, 1.0f, 0.0f,1.0f));
		CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	}
	else
	{
		Timer.timer+=_rdata.frameTime;
		if(Timer.timer>=Timer.Last_Time)
		{
			BreakShield();
			return;
		}
	}
}

void KineticShieldModule::ShieldDamage(const HitInfo &hit)
{
	health-=hit.damage;
	pEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.Shield");
	IParticleEffect *bulletEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.Shield_Bullet");
	Vec3 shieldPos=hit.pos;
	shieldPos.x+=-1*hit.dir.x;
	shieldPos.y+=-1*hit.dir.y;
	pEffect->Spawn(true,IParticleEffect::ParticleLoc(shieldPos));
	bulletEffect->Spawn(true,IParticleEffect::ParticleLoc(shieldPos));
	if(health<=0)
		BreakShield();
}

void KineticShieldModule::BreakShield()
{
	hasSet=false;
	pEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.ShieldBreak");
	pEffect->Spawn(true,IParticleEffect::ParticleLoc(g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos()));
	HasPreProcessed=false;
	Timer.Cdtimer=0;
	gEnv->p3DEngine->SetPostEffectParam( "VolumetricScattering_Amount", 0.0f );
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
}

void KineticShieldModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}