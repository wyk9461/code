#include "StdAfx.h"
#include "ReflectionShieldModule.h"

ReflectionShieldModule::ReflectionShieldModule()
	:	BaseAbility("ReflectionShield",100,eMT_Reflection)
	,	pEffect(NULL)
	,	multiplier(0.6)
	,	hasSet(false)
{
	hit_type = MAGICHITTYPE + eMT_Reflection;
	SetTimer(10,6);
}

ReflectionShieldModule::ReflectionShieldModule(int _cost, float _multi)
	:	BaseAbility("ReflectionShield",_cost,eMT_Reflection)
	,	pEffect(NULL)
	,	multiplier(_multi)
	,	hasSet(false)
{
	hit_type = MAGICHITTYPE + eMT_Reflection;
	SetTimer(10,6);
}

void ReflectionShieldModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	HasPreProcessed=true;
	Timer.timer=0;
	hasSet=false;
	multiplier=0.6;
	BaseAbility::PreProcess(_rdata);
}

void ReflectionShieldModule::Process(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
}

void ReflectionShieldModule::PostProcess(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	BaseAbility::PostProcess(_rdata);
	if(!hasSet)
	{
		finished=true;
		hasSet=true;
		gEnv->p3DEngine->SetPostEffectParam( "VolumetricScattering_Amount", 1.0f );
		gEnv->p3DEngine->SetPostEffectParam( "VolumetricScattering_Tilling", 5.0f );
		gEnv->p3DEngine->SetPostEffectParam( "VolumetricScattering_Speed", 10.0f );
		gEnv->p3DEngine->SetPostEffectParamVec4( "clr_VolumetricScattering_Color", Vec4(0.6f, 0.75f, 1.0f,1.0f));
	}
	else
	{
		Timer.timer+=_rdata.frameTime;
		if(Timer.timer>=Timer.Last_Time)
		{
			hasSet=false;
			HasPreProcessed=false;
			Timer.Cdtimer=0;
			pEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.ShieldBreak");
			pEffect->Spawn(true,IParticleEffect::ParticleLoc(g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos()));
			Timer.timer=0;
			gEnv->p3DEngine->SetPostEffectParam( "VolumetricScattering_Amount", 0.0f );
			return;
		}
	}
}

void ReflectionShieldModule::ReflectionDamage(const HitInfo &hit)
{
	HitInfo info=hit;
	info.targetId=hit.shooterId;
	info.shooterId=g_pGame->GetClientActorId();
	info.damage=hit.damage*multiplier;
	info.type = hit_type;
	g_pGame->GetGameRules()->ClientHit(info);
	Vec3 shieldPos=hit.pos;
	shieldPos.x+=-1*hit.dir.x;
	shieldPos.y+=-1*hit.dir.y;
	pEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.Reflection");
	pEffect->Spawn(true,IParticleEffect::ParticleLoc(shieldPos,-hit.dir));
}

void ReflectionShieldModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}