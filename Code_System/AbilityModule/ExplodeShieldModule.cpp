#include "StdAfx.h"
#include "ExplodeShieldModule.h"

ExplodeShieldModule::ExplodeShieldModule()
	:	BaseAbility("ExplodeShield",100,eMT_ExploShield)
	,	damage(0)
	,	multiplier(0.5)
	,	curhealth(1000)
	,	maxhealth(1000)
	,	hasSet(false)
	,	is_not_explode(true)
{
	hit_type = MAGICHITTYPE + eMT_ExploShield;
	SetTimer(10,3);
}

ExplodeShieldModule::ExplodeShieldModule(int _cost, float _multi)
	:	BaseAbility("ExplodeShield",_cost,eMT_ExploShield)
	,	damage(0)
	,	multiplier(_multi)
	,	curhealth(1000)
	,	maxhealth(1000)
	,	hasSet(false)
	,	is_not_explode(true)
{
	hit_type = MAGICHITTYPE + eMT_ExploShield;
	SetTimer(10,3);
}

void ExplodeShieldModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	HasPreProcessed=true;
	Timer.timer=0;
	hasSet=false;
	damage=0;
	is_not_explode=false;
	BaseAbility::PreProcess(_rdata);
}

void ExplodeShieldModule::Process(Process_Data &_rdata)
{

}

void ExplodeShieldModule::PostProcess(Process_Data &_rdata)
{
	if(!HasPreProcessed || !hasSet)
		return;
	BaseAbility::PostProcess(_rdata);
	if(Timer.timer<Timer.Last_Time)
		gEnv->pRenderer->Draw2dLabel(50,100,1.5f,ColorF(255,255,255,0),false,"Damage : %.0f",damage);
	if(Timer.timer>=Timer.Last_Time)
	{
		if(!is_not_explode)
		{
			IEntity *player=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity();
			CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
			is_not_explode=true;
			Timer.Cdtimer=0;
			Timer.timer=0;
			maxhealth=pActor->GetMaxHealth();
			curhealth=pActor->GetHealth();
			pActor->SetMaxHealth(100000);
			pActor->SetHealth((float)(pActor->GetHealth()+damage*(multiplier)));
			ExplosionInfo einfo(player->GetId(), 0, 0, (float)(damage), player->GetPos(), Vec3(0,0,0),0.1f, 10.0f,  0.1,  10.0f , 0 , 1000.0f , 0.0f , 0);
			einfo.SetEffect("explosions.Grenade_SCAR.character",1.0f,0.2f);
			einfo.type = g_pGame->GetGameRules()->GetHitTypeId( "Explosion" );
			SExplosionContainer SExplosion;
			SExplosion.m_explosionInfo = einfo;
			g_pGame->GetGameRules()->ClientExplosion(SExplosion);
			einfo.SetEffect("explosions.Grenade_SCAR.character",0.0f,0.2f);
			g_pGame->GetGameRules()->ClientExplosion(SExplosion);
			gEnv->p3DEngine->SetPostEffectParam( "VolumetricScattering_Amount", 0.0f );

			pActor->SetMaxHealth(maxhealth);
			pActor->SetHealth(curhealth);
			hasSet=false;
			HasPreProcessed=false;
		}
	}
	Timer.timer+=_rdata.frameTime;
	if(!hasSet)
	{
		gEnv->p3DEngine->SetPostEffectParam( "VolumetricScattering_Amount", 1.0f );
		gEnv->p3DEngine->SetPostEffectParam( "VolumetricScattering_Tilling", 5.0f );
		gEnv->p3DEngine->SetPostEffectParam( "VolumetricScattering_Speed", 10.0f );
		gEnv->p3DEngine->SetPostEffectParamVec4( "clr_VolumetricScattering_Color", Vec4(0.8f, 0.196078f, 0.196078f,1.0f));
		finished=true;
		hasSet=true;
	}
}

void ExplodeShieldModule::ExploShieldDamage(const HitInfo &hit)
{
	Vec3 shieldPos=hit.pos;
	shieldPos.x+=-1*hit.dir.x;
	shieldPos.y+=-1*hit.dir.y;
	pEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.Reflection");
	pEffect->Spawn(true,IParticleEffect::ParticleLoc(shieldPos,-hit.dir));
	damage+=(float)(hit.damage*0.3);
}

void ExplodeShieldModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}