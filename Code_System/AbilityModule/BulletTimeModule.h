#ifndef _BULLETTIMEMODULE_H_
#define _BULLETTIMEMODULE_H_

#include "BaseAbility.h"

class BulletTimeModule : public BaseAbility
{
private:
	bool hasSet;
public:
	BulletTimeModule();

	BulletTimeModule(int _cost);

	~BulletTimeModule(){}
	virtual const bool isLethal(){return false;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);
	void RegularUpdate(Process_Data &_rdata){}

	void Cool_Down(float frameTime);

	inline const bool isSet(){return hasSet;}

	void PreActive(Process_Data &_rdata);
	void Active(Process_Data &_rdata){};
	void PostActive(Process_Data &_rdata){};
};
#endif