#ifndef _GRAVITYPOOLMODULE_H_
#define _GRAVITYPOOLMODULE_H_

#include <map>

class GravityPoolModule : public BaseAbility
{
public:
	GravityPoolModule();

	GravityPoolModule(int _cost, float _force);
	~GravityPoolModule(){}
	virtual const bool isLethal(){return false;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);
	void RegularUpdate(Process_Data &_rdata);


	void PreActive(Process_Data &_rdata){}
	void Active(Process_Data &_rdata){}
	void PostActive(Process_Data &_rdata);

	void Cool_Down(float frameTime);
	void SpawnIndicateParticle();
private:
	bool hasSet;
	float z_force;
	IEntity *pEffect;
	EntityId EffectId;
	std::map<EntityId,EntityId> Forced_EntitySet;
};
#endif