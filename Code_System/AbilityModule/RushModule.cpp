#include "StdAfx.h"
#include "RushModule.h"

RushModule::RushModule()
	:	BaseAbility("Rush",100,eMT_Rush)
	,	pEffect(NULL)
	,	curhealth(1000)
	,	maxhealth(1000)
	,	dir(0,0,0)
	,	pos(0,0,0)
	,	speed(50)
	,	damage(300)
	,	hasSet(false)
	,	once(false)
{
	SetTimer(10,0);
}

RushModule::RushModule(int _cost, float _speed)
	:	BaseAbility("Rush",_cost,eMT_Rush)
	,	pEffect(NULL)
	,	curhealth(1000)
	,	maxhealth(1000)
	,	dir(0,0,0)
	,	pos(0,0,0)
	,	speed(_speed)
	,	damage(300)
	,	hasSet(false)
	,	once(false)
{
	SetTimer(10,0);
}

void RushModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;
	/*default_View=g_pGame->GetIGameFramework()->GetIViewSystem()->GetActiveView();
	rush_View=g_pGame->GetIGameFramework()->GetIViewSystem()->CreateView();*/
	pBRC=g_pGame->GetBasicRayCast();
	pBRC->ChangeFlag(ent_all);
	pBRC->ChangeDistance(2.5);
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	HasPreProcessed=true;
	Timer.Last_Time=0.05f;
	Timer.timer=0;
	pEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.Rush");
	pEffect->LoadResources();
	speed=50;
	maxhealth=pActor->GetMaxHealth();
	once=false;

	BaseAbility::SendMessage(eMT_Rush, eET_ChangeProperty,_rdata);
	BaseAbility::SendMessage(eMT_Common, eET_ChangeProperty,_rdata);
	BaseAbility::PreProcess(_rdata);
}

void RushModule::Process(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	if(Timer.Last_Time<=0.75f)
		Timer.Last_Time+=_rdata.frameTime/3;
	gEnv->pRenderer->DrawLabel(_rdata.hit.pt,1.5f,"Rush Time : %.2f",Timer.Last_Time);
	dir=gEnv->pRenderer->GetCamera().GetViewdir();
	pos=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
	curhealth=pActor->GetHealth();
	BaseAbility::SendMessage(eMT_Rush, eET_Charge,_rdata);
}

void RushModule::PostProcess(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	BaseAbility::PostProcess(_rdata);
	if(!hasSet)
	{
		hasSet=true;
		finished=true;
		gEnv->p3DEngine->SetPostEffectParam( "ImageGhosting_Amount", 0.5 );
		//Code Node : a new particle spawn method.
		IEntity *pEntity=gEnv->pEntitySystem->FindEntityByName("Rush_Particle");
		if(!pEntity)
		{
			SEntitySpawnParams params;
			params.sName="Rush_Particle";
			params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
			params.vScale=Vec3(1,1,1);
			params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
			params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
			pEntity=gEnv->pEntitySystem->SpawnEntity(params);
			SmartScriptTable proot=pEntity->GetScriptTable();
			SmartScriptTable pprops;
			proot->GetValue("Properties",pprops);
			pprops->SetValue("ParticleEffect","Code_System.Module_Effect.Rush");
			Script::CallMethod(proot,"OnReset");
		}
		else
			pEntity->Hide(false);
		StoreInventory(); 
		/*IViewSystem *pSystem=g_pGame->GetIGameFramework()->GetIViewSystem();
		SViewParams *curParams=const_cast<SViewParams*>(default_View->GetCurrentParams());
		SViewParams newParams=*curParams;
		Vec3 eyePos=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
		eyePos.z+=1.8;
		Vec3 fordir=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetForwardDir();
		newParams.position=eyePos-2*fordir;
		rush_View->SetCurrentParams(newParams);
		if(pSystem->GetActiveView() != rush_View)
			pSystem->SetActiveView(rush_View);*/
	}
	else
	{
		Timer.timer+=_rdata.frameTime;
		if(Timer.timer <Timer.Last_Time)
		{
			dir=gEnv->pRenderer->GetCamera().GetViewdir();
			if(pBRC->GetHits() != 1)
			{
				pos+=speed*dir*_rdata.frameTime;
				float z_off=gEnv->p3DEngine->GetTerrainElevation(pos.x,pos.y);
				if(pos.z-z_off<0.1)
					pos.z=(float)(z_off+0.1);
				dir=gEnv->pRenderer->GetCamera().GetViewdir();
				IEntity *player=gEnv->pEntitySystem->GetEntity(g_pGame->GetClientActorId());
				player->SetPos(pos);
			}
		}
		if(Timer.timer>=Timer.Last_Time)
		{
			if(!once)
			{
				once=true;
				CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();

				/*pActor->GetEntity()->EnablePhysics(false);
				ExplosionInfo einfo(pActor->GetEntityId(), 0, 0, damage , pActor->GetEntity()->GetPos(), Vec3(0,0,0),0.1f, 5.0f,  0.1,  5.0f , 0 , 1000.0f , 0.0f , 0);
				einfo.SetEffect("explosions.Grenade_SCAR.character",1.0f,0.2f);  //Need to change
				einfo.type = g_pGame->GetGameRules()->GetHitTypeId( "Explosion" );
				SExplosionContainer SExplosion;
				SExplosion.m_explosionInfo = einfo;
				g_pGame->GetGameRules()->ClientExplosion(SExplosion);
				pActor->GetEntity()->EnablePhysics(true);*/

				RestoreInventory();
				gEnv->p3DEngine->SetPostEffectParam( "ImageGhosting_Amount", 0 );
				pBRC->Load();
				IEntity *pEntity=gEnv->pEntitySystem->FindEntityByName("Rush_Particle");
				if(pEntity)
					pEntity->Hide(true);

				HasPreProcessed=false;
				hasSet=false;
				Timer.timer=0;
				_rdata.Finish_Pos = pos;
				BaseAbility::SendMessage(eMT_Rush, eET_Finished,_rdata);
			}
		}
	}
}

void RushModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}

void RushModule::PreActive(Process_Data &_rdata)
{
	BasicRayCast *pBRC = g_pGame->GetBasicRayCast();
	const float tDist = pBRC->GetDistance();
	pBRC->ChangeDistance(static_cast<float>(tDist*1.5));
	BaseAbility::PreActive(_rdata);
}

void RushModule::PostActive(Process_Data &_rdata)
{
	BasicRayCast *pBRC = g_pGame->GetBasicRayCast();
	pBRC->Load();
	BaseAbility::PostActive(_rdata);
}

void RushModule::ReceiveMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata)
{
	if(_mtype == eMT_Common && _etype == eET_ChangeProperty)
	{
		TeleportModule *parent = (TeleportModule*)GetParent();
		if(parent)
		{
			float length = g_pGame->GetBasicRayCast()->GetDistance();
			parent->ChangeTeleportLength(static_cast<float>(length * 1.5));
		}
	}

	if(_mtype == eMT_DataBall && _etype == eET_ChangeProperty)
	{
		DataBallModule *parent = (DataBallModule*)GetParent();
		if(parent)
		{
			const float speed = parent->GetSpeed();
			parent->SetSpeed(static_cast<float>(speed*1.5));
		}
	}
}