#include "StdAfx.h"
#include "BlinkStrike.h"
#include "Code_System/Buff.h"

BlinkStrikeModule::BlinkStrikeModule()
:	BaseAbility("BlinkStrike", 100,eMT_BlinkStrike)
,	pEffect(0)
,	EffectId(0)
,	pTarget(0)
,	damage_multi(3)
{
	g_pGame->GetBasicRayCast()->Save(init_backup);
	g_pGame->GetBasicRayCast()->Save(BlinkStrike_backup);
	BlinkStrike_backup.query_flag = ent_living | ent_terrain;
	SetTimer(4,0);
}

BlinkStrikeModule::BlinkStrikeModule(int _cost, float _multi)
	:	BaseAbility("BlinkStrike", _cost,eMT_BlinkStrike)
	,	pEffect(0)
	,	EffectId(0)
	,	pTarget(0)
	,	damage_multi(_multi)
{
	g_pGame->GetBasicRayCast()->Save(init_backup);
	g_pGame->GetBasicRayCast()->Save(BlinkStrike_backup);
	BlinkStrike_backup.query_flag = ent_living | ent_terrain;
	SetTimer(4,0);
}

void BlinkStrikeModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	pEffect = gEnv->pEntitySystem->GetEntity(EffectId);
	if(!pEffect)
	{
		pEffect = gEnv->pEntitySystem->FindEntityByName("BLSP");
		if(!pEffect)
		{
			SpawnIndicateParticle();
		}
	}
	else
		pEffect->Hide(false);
	HasPreProcessed = true;
	BaseAbility::PreProcess(_rdata);
	BaseAbility::Active(_rdata);
	g_pGame->GetBasicRayCast()->Load(BlinkStrike_backup);
}

void BlinkStrikeModule::Process(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	BaseAbility::PostProcess(_rdata);
	Vec3 &Position = _rdata.hit.pt;
	EntityId nearestId = CommonUsage::GetNerestEntityIdInBox(Position,4,ent_living);
		if(nearestId != 0 && nearestId != g_pGame->GetClientActorId())
	{
		AABB aabb;
		pTarget = gEnv->pEntitySystem->GetEntity(nearestId);
		pTarget->GetWorldBounds(aabb);
		if(pEffect)
		{
			pEffect->SetPos(aabb.GetCenter());
		}
	}
}

void BlinkStrikeModule::PostProcess(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	if(pTarget)
	{
		CActor *pPlayerActor = (CActor*)g_pGame->GetIGameFramework()->GetClientActor();
		IEntity *pPlayer = pPlayerActor->GetEntity();
		Vec3 PlayerPos = pPlayer->GetPos();
		Quat targetRotation = pTarget->GetRotation();
		Vec3 TargetDir = pTarget->GetForwardDir();
		Vec3 TelePos = pTarget->GetPos() - TargetDir * 1.5;

		pPlayer->SetPos(TelePos);
		pPlayerActor->SetViewRotation(targetRotation);

		BuffManager::TheBuffManager().CreateCriticalStrike_Buff(damage_multi);
		HasPreProcessed = false;
		finished = true;
	}
	if(pEffect)
		pEffect->Hide(true);

}


void BlinkStrikeModule::PreActive(Process_Data &_rdata){}
void BlinkStrikeModule::Active(Process_Data &_rdata){}
void BlinkStrikeModule::PostActive(Process_Data &_rdata){}

void BlinkStrikeModule::Cool_Down(float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}

void BlinkStrikeModule::SpawnIndicateParticle()
{
	SEntitySpawnParams params;
	params.sName = "BLSP";

	params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
	params.vScale=Vec3(1,1,1);
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	pEffect=gEnv->pEntitySystem->SpawnEntity(params);

	SmartScriptTable proot=pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.Teleport");
	Script::CallMethod(proot,"OnReset");

	EffectId = pEffect->GetId();
}