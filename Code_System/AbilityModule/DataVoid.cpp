#include "StdAfx.h"
#include "DataVoid.h"

DataVoidModule::DataVoidModule()
:	BaseAbility("DatVoid", 100,eMT_DataVoid)
,	pEffect(0)
,	EffectId(0)
,	damage_multi(0.3)
{
	hit_type = MAGICHITTYPE + eMT_DataGuard;
	SetTimer(2,0);
}

DataVoidModule::DataVoidModule(int _cost, float _multi)
	:	BaseAbility("DatVoid", _cost,eMT_DataVoid)
	,	pEffect(0)
	,	EffectId(0)
	,	damage_multi(_multi)
{
	hit_type = MAGICHITTYPE + eMT_DataGuard;
	SetTimer(2,0);
}

void DataVoidModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	pEffect = gEnv->pEntitySystem->GetEntity(EffectId);
	if(!pEffect)
	{
		pEffect = gEnv->pEntitySystem->FindEntityByName("DVP");
		if(!pEffect)
		{
			SpawnIndicateParticle();
		}
	}
	else
		pEffect->Hide(false);
	HasPreProcessed = true;
	BaseAbility::SendMessage(eMT_DataVoid, eET_ChangeProperty,_rdata);
	BaseAbility::SendMessage(eMT_Common, eET_ChangeProperty,_rdata);

}

void DataVoidModule::Process(Process_Data &_rdata)
{
	EntityId nerestId = CommonUsage::GetNerestEntityIdInBox(_rdata.hit.pt, 3, ent_living);
	if(nerestId && nerestId != g_pGame->GetClientActorId())
	{
		pTarget = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(nerestId));
		if(pTarget)
		{
			AABB aabb;
			pTarget->GetEntity()->GetWorldBounds(aabb);
			if(pEffect)
				pEffect->SetPos(aabb.GetCenter());
		}
	}
}

void DataVoidModule::PostProcess(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	BaseAbility::PostProcess(_rdata);
	finished = true;
	HasPreProcessed=false;

	float maxHealth = pTarget->GetMaxHealth();
	float curHealth = pTarget->GetHealth();
	float damage = (maxHealth - curHealth) * damage_multi;

	const Vec3 &explosion_pos = pTarget->GetEntity()->GetPos();
	CommonUsage::CreateExplosion(g_pGame->GetClientActorId(), explosion_pos,damage,3.5,300,hit_type);
	if(pEffect)
		pEffect->Hide(true);

	pTarget = 0;
}

void DataVoidModule::PreActive(Process_Data &_rdata){}
void DataVoidModule::Active(Process_Data &_rdata){}
void DataVoidModule::PostActive(Process_Data &_rdata){}

void DataVoidModule::Cool_Down(float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}

void DataVoidModule::SpawnIndicateParticle()
{
	SEntitySpawnParams params;
	params.sName = "DVP";

	params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
	params.vScale=Vec3(1,1,1);
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	pEffect=gEnv->pEntitySystem->SpawnEntity(params);

	SmartScriptTable proot=pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.Teleport");
	Script::CallMethod(proot,"OnReset");

	EffectId = pEffect->GetId();
}

void DataVoidModule::ReceiveMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata)
{

}