#include "StdAfx.h"
#include "NuclearRadModule.h"

NuclearRadModule::NuclearRadModule()
	:	BaseAbility("NuclearRad",100,eMT_NuclearRad)
	,	pEffect(NULL)
	,	damage(1)
	,	speed_multi(0.8)
	,	pos(0,0,0)
	,	hasSet(false)
{
	hit_type = MAGICHITTYPE + eMT_NuclearRad;
	SetTimer(10,10);
}

NuclearRadModule::NuclearRadModule(int _cost, float _damage, float _multi)
	:	BaseAbility("NuclearRad",_cost,eMT_NuclearRad)
	,	pEffect(NULL)
	,	damage(_damage)
	,	speed_multi(_multi)
	,	pos(0,0,0)
	,	hasSet(false)
{
	hit_type = MAGICHITTYPE + eMT_NuclearRad;
	SetTimer(10,10);
}

void NuclearRadModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	g_pGame->GetBasicRayCast()->ChangeFlag(ent_terrain);
	HasPreProcessed=true;
	Timer.Cdtimer=0;
	Timer.timer=0;
	SEntitySpawnParams params;
	params.sName="NuclearRad";
	params.vScale=Vec3(1,1,1);
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	pEffect=gEnv->pEntitySystem->SpawnEntity(params);
	SmartScriptTable proot=pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.Nuclear_Logo");
	Script::CallMethod(proot,"OnReset");
	BaseAbility::PreProcess(_rdata);
}


void NuclearRadModule::Process(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	pEffect->SetPos(_rdata.hit.pt);
	pos=pEffect->GetPos();
}

void NuclearRadModule::PostProcess(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	BaseAbility::PostProcess(_rdata);
	if(!hasSet)
	{
		HasPreProcessed=false;
		finished=true;
		SmartScriptTable proot=pEffect->GetScriptTable();
		SmartScriptTable pprops;
		proot->GetValue("Properties",pprops);
		pprops->SetValue("ParticleEffect","Code_System.Module_Effect.NuclearRad");
		Script::CallMethod(proot,"OnReset");
		g_pGame->GetBasicRayCast()->Load();
	}
	else
	{
		if(Timer.timer<Timer.Last_Time)
		{
			IPhysicalEntity **nearbyEntities;
			int num=gEnv->pPhysicalWorld->GetEntitiesInBox(pos-Vec3(10),pos+Vec3(10),nearbyEntities,ent_living);
			for(int i=0;i<num;i++)
			{
				IEntity* tEntity=gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
				CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(tEntity->GetId());
				if((tEntity->GetPos()-pos).GetLength() < 5.0f )
				{
					if(pActor->GetHealth() > 10)
						pActor->SetHealth(pActor->GetHealth()-damage);
					if(!pActor->IsDead() && !pActor->IsPlayer())
					{
						pActor->SetSpeedMultipler(SActorParams::eSMR_Internal,speed_multi);
						pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,speed_multi);
						pActor->SetSpeedMultipler(SActorParams::eSMR_Item,speed_multi);
					}
				}
				else
				{
					if(!pActor->IsDead())
					{
						pActor->SetSpeedMultipler(SActorParams::eSMR_Internal,1);
						pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,1);
						pActor->SetSpeedMultipler(SActorParams::eSMR_Item,1);
					}
				}
			}
		}
		else
		{
			IPhysicalEntity **nearbyEntities;
			int num=gEnv->pPhysicalWorld->GetEntitiesInBox(pos-Vec3(6),pos+Vec3(6),nearbyEntities,ent_living);
			for(int i=0;i<num;i++)
			{
				IEntity* tEntity=gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
				CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(tEntity->GetId());
				if(!pActor->IsDead())
				{
					pActor->SetSpeedMultipler(SActorParams::eSMR_Internal,1);
					pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,1);
					pActor->SetSpeedMultipler(SActorParams::eSMR_Item,1);
				}
			}
			hasSet=false;
			gEnv->pEntitySystem->RemoveEntity(pEffect->GetId());
			pEffect=NULL;
		}
		Timer.timer+=_rdata.frameTime;
	}
}

void NuclearRadModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}