#ifndef _FLOODMODULE_H_
#define _FLOODMODULE_H_

#include "BaseAbility.h"

class FloodModule : public BaseAbility
{
private:
	IEntity *pEffect;
	bool hasSet;
public:
	FloodModule();

	FloodModule(int _cost);

	~FloodModule(){}
	virtual const bool isLethal(){return false;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);
	void RegularUpdate(Process_Data &_rdata){}

	void Cool_Down(float frameTime);

	inline const bool isSet(){return hasSet;}
};
#endif