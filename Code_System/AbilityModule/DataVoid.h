#ifndef _DATAVOID_H_
#define _DATAVOID_H_

#include "BaseAbility.h"


class DataVoidModule : public BaseAbility
{
public:
	DataVoidModule();

	DataVoidModule(int _cost, float _multi);

	~DataVoidModule(){}
	virtual const bool isLethal(){return true;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);

	void PreActive(Process_Data &_rdata);
	void Active(Process_Data &_rdata);
	void PostActive(Process_Data &_rdata);

	void Cool_Down(float frameTime);

	void RegularUpdate(Process_Data &_rdata){};

	void ReceiveMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata);

private:
	void SpawnIndicateParticle();
	IEntity *pEffect;
	EntityId EffectId;
	CActor *pTarget;
	float damage_multi;

};

#endif