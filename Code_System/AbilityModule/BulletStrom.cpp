#include "StdAfx.h"
#include "BulletStrom.h"
#include "Projectile.h"

BulletStromModule::BulletStromModule()
:	BaseAbility("bulletStrom", 100,eMT_BulletStrom)
,	pEffect(0)
,	EffectId(0)
,	dir(Vec3(0,0,0))
,	bullet_count(0)
,	max_bullet(200)
,	hasSet(false)
{
	pBulletClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("RifleBullet");
	hit_type = MAGICHITTYPE + eMT_BulletStrom;
	SetTimer(10,2);
}

BulletStromModule::BulletStromModule(int _cost, unsigned int _maxBullet)
	:	BaseAbility("bulletStrom", _cost,eMT_BulletStrom)
	,	pEffect(0)
	,	EffectId(0)
	,	dir(Vec3(0,0,0))
	,	bullet_count(0)
	,	max_bullet(_maxBullet)
	,	hasSet(false)
{
	pBulletClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("RifleBullet");
	hit_type = MAGICHITTYPE + eMT_BulletStrom;
	SetTimer(10,2);
}

void BulletStromModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	HasPreProcessed = true;
	BaseAbility::PreProcess(_rdata);
	BaseAbility::Active(_rdata);
}

void BulletStromModule::Process(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	Vec3 Pos = _rdata.hit.pt;
	Vec3 PlayerPos = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
	Pos.z = PlayerPos.z;
	dir = (Pos - PlayerPos).normalize();
}

void BulletStromModule::PostProcess(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	BaseAbility::PostProcess(_rdata);
	if(!hasSet)
	{
		hasSet = true;
		finished = true;
		pEffect = gEnv->pEntitySystem->GetEntity(EffectId);
		if(!pEffect)
		{
			pEffect = gEnv->pEntitySystem->FindEntityByName("BSP");
			if(!pEffect)
			{
				SpawnIndicateParticle();
				Vec3 PlayerPos = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
				pEffect->SetPos(PlayerPos);
				Quat rot=pEffect->GetRotation();
				Quat rot2=rot.CreateRotationX(DEG2RAD(90.0f));
				rot*=rot2;
				pEffect->SetRotation(rot);
			}
		}
		else
		{
			pEffect->Hide(false);
			Vec3 PlayerPos = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
			pEffect->SetPos(PlayerPos);
			Quat rot=pEffect->GetRotation();
			Quat rot2=rot.CreateRotationZ(DEG2RAD(0));
			pEffect->SetRotation(rot2);
		}
	}
	else
	{
		if(pEffect && pEffect->IsHidden())
		{
			pEffect->Hide(false);
			Vec3 PlayerPos = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
			pEffect->SetPos(PlayerPos);
		}
		if(Timer.timer < Timer.Last_Time)
		{
			if(pEffect && !pEffect->IsHidden())
			{
				Vec3 curPos = pEffect->GetPos();
				Vec3 nextPos = curPos;
				nextPos += dir * 3 *_rdata.frameTime;
				nextPos.z = static_cast<f32>(gEnv->p3DEngine->GetTerrainElevation(nextPos.x,nextPos.y) + 1.4);
				pEffect->SetPos(nextPos);
				Timer.timer += _rdata.frameTime;
			}
		}
		if(Timer.timer >= Timer.Last_Time)
		{
			if(bullet_count <= max_bullet && pEffect && !pEffect->IsHidden())
			{
				Quat rot=pEffect->GetRotation();
				Quat rot2=rot.CreateRotationZ(DEG2RAD(720/max_bullet));
				rot*=rot2;
				pEffect->SetRotation(rot);

				CWeaponSystem *pSystem=g_pGame->GetWeaponSystem();
				CProjectile *new_Projectile=pSystem->SpawnAmmo(pBulletClass);
				if(new_Projectile)
				{
					CProjectile::SProjectileDesc projectileDesc(
						pEffect->GetId(), 0, 0, 100, 0, 0, 0, hit_type,
						0, false);

					new_Projectile->SetParams(projectileDesc);
					new_Projectile->Launch(pEffect->GetPos(),pEffect->GetForwardDir(),Vec3(ZERO));
					IParticleEffect *bulletEffect=gEnv->pParticleManager->FindEffect("Projectile_Effect.Reflection_Indicator");
					bulletEffect->Spawn(true,IParticleEffect::ParticleLoc(pEffect->GetPos(),pEffect->GetForwardDir()));
					++bullet_count;
				}
			}
			else
			{
				IParticleEffect *pSpawnEffect=gEnv->pParticleManager->FindEffect("Code_System.WarpSpawn");
				pSpawnEffect->Spawn(true,IParticleEffect::ParticleLoc(pEffect->GetPos()));
				pEffect->Hide(true);
				HasPreProcessed = false;
			}
		}
	}
}

void BulletStromModule::PreActive(Process_Data &_rdata){}
void BulletStromModule::Active(Process_Data &_rdata){}
void BulletStromModule::PostActive(Process_Data &_rdata){}

void BulletStromModule::Cool_Down(float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
		Timer.timer = 0;
		bullet_count = 0;
		hasSet = false;
		dir = Vec3(0,0,0);
	}
}

void BulletStromModule::SpawnIndicateParticle()
{
	SEntitySpawnParams params;
	params.sName = "BSP";

	params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
	params.vScale=Vec3(1,1,1);
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	pEffect=gEnv->pEntitySystem->SpawnEntity(params);

	SmartScriptTable proot=pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.Teleport");
	Script::CallMethod(proot,"OnReset");

	EffectId = pEffect->GetId();
}