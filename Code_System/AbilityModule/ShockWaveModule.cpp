#include "StdAfx.h"
#include "ShockWaveModule.h"
#include "Code_System/MicroSystem/FallenActorManager.h"

ShockWaveModule::ShockWaveModule()
	:	BaseAbility("ShockWave" , 100,eMT_ShockWave)
	,	radius(8.0)
	,	pressure(1000.0)
	,	pEffect(NULL)
	,	nearbyEntities(NULL)
{
	SetTimer(10,0);
}

ShockWaveModule::ShockWaveModule(int _cost, float _radius, float _pressure)
	:	BaseAbility("ShockWave" , _cost,eMT_ShockWave)
	,	radius(_radius)
	,	pressure(_pressure)
	,	pEffect(NULL)
	,	nearbyEntities(NULL)
{
	SetTimer(10,0);
}

void ShockWaveModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	HasPreProcessed=true;
	SpawnIndicateParticle();

	BaseAbility::SendMessage(eMT_ShockWave, eET_ChangeProperty,_rdata);
	BaseAbility::SendMessage(eMT_Common, eET_ChangeProperty,_rdata);
	BaseAbility::PreProcess(_rdata);
}

void ShockWaveModule::Process(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	pEffect->SetPos(g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos());
	//BaseAbility::SendMessage(eMT_ShockWave, eET_Charge,_rdata);
}

void ShockWaveModule::PostProcess(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	g_pGame->GetWorldStability()->StabilityCost(cost);
	HasPreProcessed=false;
	pressure=5000;
	IEntity *player=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity();
	int num=gEnv->pPhysicalWorld->GetEntitiesInBox(player->GetPos()-Vec3(radius),player->GetPos()+Vec3(radius),nearbyEntities,ent_living | ent_sleeping_rigid | ent_rigid);
	for(int i=0;i<num;i++)
	{
		IEntity* pEntity=gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
		if(pEntity)
		{
			CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());
			if(pActor && !pActor->IsDead() && !pActor->IsPlayer())
			{
				if(!pActor->IsFallen())
					FallenActorManager::Instance().AddToManager(pActor);
				IEntityPhysicalProxy *pPhysicsProxy = static_cast<IEntityPhysicalProxy*>(pActor->GetEntity()->GetProxy(ENTITY_PROXY_PHYSICS));
				if(pPhysicsProxy)
				{
					Vec3 dir = (pActor->GetEntity()->GetPos() - player->GetPos()).normalize();
					dir.z += 1;
					dir.normalize();
					pPhysicsProxy->AddImpulse(-1,pActor->GetEntity()->GetPos(),dir*1200,true,1);
					pActor->RagDollize(true);
				}
			}
		}
	}
	ExplosionInfo einfo(player->GetId(), 0, 0, 0, player->GetPos(), Vec3(0,0,0),1, radius,  2.0f,  radius,0, pressure,0.0f, 0);
	einfo.SetEffect("Code_System.Module_Effect.ShcokWave_Finished",1.0f,0.2f);
	einfo.type = g_pGame->GetGameRules()->GetHitTypeId( "Explosion" );
	g_pGame->GetGameRules()->QueueExplosion(einfo);
	IEntity *pEntity=gEnv->pEntitySystem->FindEntityByName("ShockWave_Particle");
	if(pEntity)
		pEntity->Hide(true);
	g_pGame->GetWorldStability()->StabilityCost(cost);
	BaseAbility::SendMessage(eMT_ShockWave, eET_Finished,_rdata);
	finished=true;
}

void ShockWaveModule::Cool_Down(const float frameTime)
{
	Timer.timer+=frameTime;
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.timer=0;
		Timer.Cdtimer=0;
	}
}

void ShockWaveModule::PostActive(Process_Data &_rdata)
{
	SpawnIndicateParticle();
	BaseAbility::PostProcess(_rdata);
	//StabilityCost may combine into one
	g_pGame->GetWorldStability()->StabilityCost(cost);
	pressure=5000;
	IEntity *player=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity();
	int num=gEnv->pPhysicalWorld->GetEntitiesInBox(player->GetPos()-Vec3(radius),player->GetPos()+Vec3(radius),nearbyEntities,ent_living);
	for(int i=0;i<num;i++)
	{
		IEntity* pEntity=gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
		CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());
		if(pActor && !pActor->IsFallen() && !pActor->IsDead() && !pActor->IsPlayer())
			FallenActorManager::Instance().AddToManager(pActor);
	}
	ExplosionInfo einfo(player->GetId(), 0, 0, 0, player->GetPos(), Vec3(0,0,0), 1, radius,  2.0f,  radius,0, pressure,0.0f, 0);
	einfo.SetEffect("Code_System.Module_Effect.ShcokWave_Finished",1.0f,0.2f);
	einfo.type = g_pGame->GetGameRules()->GetHitTypeId( "Explosion" );
	g_pGame->GetGameRules()->QueueExplosion(einfo);
	IEntity *pEntity=gEnv->pEntitySystem->FindEntityByName("ShockWave_Particle");

	if(pEntity)
		pEntity->Hide(true);
	g_pGame->GetWorldStability()->StabilityCost(cost);
}

void ShockWaveModule::SpawnIndicateParticle()
{
	pEffect=gEnv->pEntitySystem->FindEntityByName("ShockWave_Particle");
	if(!pEffect)
	{
		SEntitySpawnParams params;
		params.sName="ShockWave_Particle";
		params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
		params.vScale=Vec3(1,1,1);
		params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
		params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
		pEffect=gEnv->pEntitySystem->SpawnEntity(params);
		SmartScriptTable proot=pEffect->GetScriptTable();
		SmartScriptTable pprops;
		proot->GetValue("Properties",pprops);
		pprops->SetValue("ParticleEffect","Code_System.Module_Effect.ShockWave");
		Script::CallMethod(proot,"OnReset");
	}
	else
		pEffect->Hide(false);
}

void ShockWaveModule::ReceiveMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata)
{
	//BaseAbility::SendMessage(eMT_ShockWave, eET_SubModule, _rdata);

	if(_mtype == eMT_Common && _etype == eET_Finished)
	{
		Combined_ShockWave(_rdata.Finish_Pos);
	}

	if(_mtype == eMT_ShockWave && _etype == eET_ChangeProperty)
	{
		ShockWaveModule *parent = (ShockWaveModule*)GetParent();
		if(parent)
		{
			parent->SetRadius(static_cast<float>(parent->GetRadius() * 1.5));
		}
	}

	if((_mtype == eMT_Backup || _mtype == eMT_Reposition)&& _etype == eET_Finished)
	{
		Combined_ShockWave(_rdata.Finish_Pos);
		Combined_ShockWave(_rdata.Start_Pos);
	}
}


void ShockWaveModule::Combined_ShockWave(Vec3 &pos)
{
	g_pGame->GetWorldStability()->StabilityCost(cost);
	pressure=5000;
	IEntity *player=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity();
	int num=gEnv->pPhysicalWorld->GetEntitiesInBox(pos-Vec3(radius),pos+Vec3(radius),nearbyEntities,ent_living);
	for(int i=0;i<num;i++)
	{
		IEntity* pEntity=gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
		CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());
		if(pActor && !pActor->IsFallen() && !pActor->IsDead() && !pActor->IsPlayer())
			FallenActorManager::Instance().AddToManager(pActor);
	}
	ExplosionInfo einfo(player->GetId(), 0, 0, 0, pos, Vec3(0,0,0),1, radius,  2.0f,  radius,0, pressure,0.0f, 0);
	einfo.SetEffect("Code_System.Module_Effect.ShcokWave_Finished",1.0f,0.2f);
	einfo.type = g_pGame->GetGameRules()->GetHitTypeId( "Explosion" );
	g_pGame->GetGameRules()->QueueExplosion(einfo);
	IEntity *pEntity=gEnv->pEntitySystem->FindEntityByName("ShockWave_Particle");

	if(pEntity)
		pEntity->Hide(true);
	g_pGame->GetWorldStability()->StabilityCost(cost);
}