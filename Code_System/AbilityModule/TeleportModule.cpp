#include "StdAfx.h"
#include "TeleportModule.h"


TeleportModule::TeleportModule()
	:	pos(0,0,0)
	,	pEffect(NULL)
	,	BaseAbility("Teleport" , 100,eMT_Teleport)
{
	SetTimer(10,0);
}

TeleportModule::TeleportModule(int _cost)
	:	BaseAbility("Teleport" , _cost,eMT_Teleport)
	,	pos(0,0,0)
	,	pEffect(NULL)
{
	SetTimer(10,0);
}

void TeleportModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	HasPreProcessed=true;
	DefaultLength = g_pGame->GetBasicRayCast()->GetDistance();
	g_pGame->GetBasicRayCast()->ChangeFlag(ent_all);
	IEntity *pEntity=gEnv->pEntitySystem->FindEntityByName("Teleport_Particle");
	if(!pEntity)
	{
		SEntitySpawnParams params;
		params.sName="Teleport_Particle";
		params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
		params.vScale=Vec3(1,1,1);
		params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
		params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
		pEffect=gEnv->pEntitySystem->SpawnEntity(params);
		SmartScriptTable proot=pEffect->GetScriptTable();
		SmartScriptTable pprops;
		proot->GetValue("Properties",pprops);
		pprops->SetValue("ParticleEffect","Code_System.Module_Effect.Teleport");
		Script::CallMethod(proot,"OnReset");
	}
	else
		pEntity->Hide(false);

	BaseAbility::SendMessage(eMT_Teleport, eET_ChangeProperty,_rdata);
	BaseAbility::SendMessage(eMT_Common, eET_ChangeProperty,_rdata);
	BaseAbility::PreProcess(_rdata);
}

void TeleportModule::Process(Process_Data &_rdata)
{
	//Code Notes : Character Animation
	/*ICharacterInstance *character = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetCharacter(0);
	ISkeletonAnim* pSkeleton;
	if(character && (pSkeleton=character->GetISkeletonAnim()))
	{
		string animName="swim_tac_melee_nw_1p_01";
		CryCharAnimationParams Params;
		Params.m_fAllowMultilayerAnim = 1;
		Params.m_nLayerID = 10;

		//Params.m_nFlags |= CA_LOOP_ANIMATION;
		//Params.m_nFlags |= CA_REMOVE_FROM_FIFO;
		Params.m_nUserToken = 0;

		Params.m_fTransTime = 0.1;
		Params.m_fPlaybackSpeed = 1;
		//CryLogAlways("ANIMATION");
		pSkeleton->StartAnimation(animName.c_str(), Params);
	}*/
	if(!HasPreProcessed)
		return;
	pEffect->SetPos(_rdata.hit.pt);
	pos=_rdata.hit.pt;

	BaseAbility::SendMessage(eMT_Teleport, eET_Charge, _rdata);
}

void TeleportModule::PostProcess(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	BaseAbility::PostProcess(_rdata);
	HasPreProcessed=false;
	IActor *player=g_pGame->GetIGameFramework()->GetClientActor();
	player->GetEntity()->SetPos(pos);
	IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.Teleport_Finish");
	pEffect->Spawn(true,IParticleEffect::ParticleLoc(player->GetEntity()->GetPos(),Vec3(0,0,1)));
	g_pGame->GetWorldStability()->StabilityCost(cost);
	finished=true;
	blurAmount=1;
	gEnv->p3DEngine->SetPostEffectParam( "ImageGhosting_Amount", blurAmount );
	g_pGame->GetBasicRayCast()->Load();
	IEntity *pEntity=gEnv->pEntitySystem->FindEntityByName("Teleport_Particle");
	if(pEntity)
		pEntity->Hide(true);
	ChangeTeleportLength(DefaultLength);

	_rdata.Finish_Pos = pos;
	BaseAbility::SendMessage(eMT_Teleport, eET_Finished, _rdata);
}

void TeleportModule::Cool_Down(const float frameTime)
{
	if(Timer.timer>=0.5f && Timer.timer<=1.5f)
		gEnv->p3DEngine->SetPostEffectParam( "ImageGhosting_Amount",blurAmount-=1*frameTime );
	if(Timer.timer>=1.5f)
		gEnv->p3DEngine->SetPostEffectParam( "ImageGhosting_Amount",0.0f);
	Timer.Cdtimer+=frameTime;
	Timer.timer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.timer=0;
		Timer.Cdtimer=0;
	}
}

void TeleportModule::PreActive(Process_Data &_rdata)
{
	BaseAbility *parent = (BaseAbility*)GetParent();
	if(parent)
	{
		Timer_Cd &Timer = parent->GetTimer();
		Timer.Cdtimer += static_cast<float>(Timer.Cd *0.25);
	}
	BaseAbility::PreActive(_rdata);
}

void TeleportModule::ChangeTeleportLength(float length)
{
	DefaultLength = g_pGame->GetBasicRayCast()->GetDistance();
	g_pGame->GetBasicRayCast()->ChangeDistance(length);
}

void TeleportModule::ReceiveMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata)
{
	//BaseAbility::SendMessage(eMT_Teleport, eET_SubModule, _rdata);

	if(_mtype == eMT_Common && _etype == eET_ChangeProperty)
	{
		BaseAbility *parent = (BaseAbility*)GetParent();
		if(parent)
		{
			Timer_Cd &Timer = parent->GetTimer();
			Timer.Cdtimer += static_cast<float>(Timer.Cd *0.25);
		}
	}

	if(_mtype == eMT_Explosion && _etype == eET_Set)
	{
		ExplosionModule *parent = (ExplosionModule*)GetParent();
		if(parent)
			parent->ExplodeAll();
	}
}