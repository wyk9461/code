#include "StdAfx.h"
#include "DataBallModule.h"

DataBallModule::DataBallModule()
	:	BaseAbility("DataBall",100,eMT_DataBall)
	,	HitEffect(NULL)
	,	pEffect(NULL)
	,	speed(10.f)
	,	dir(0,0,0)
	,	pos(0,0,0)
	,	damage(100.f)
	,	stacollected(0)
	,	multiplier(0.5)
	,	hasSet(false)
{
	hit_type = MAGICHITTYPE + eMT_DataBall;
	SetTimer(10,2);
}

DataBallModule::DataBallModule(int _cost, float _damage, float _multi)
	:	BaseAbility("DataBall",_cost,eMT_DataBall)
	,	HitEffect(NULL)
	,	pEffect(NULL)
	,	speed(10.f)
	,	dir(0,0,0)
	,	pos(0,0,0)
	,	damage(_damage)
	,	stacollected(0)
	,	multiplier(_multi)
	,	hasSet(false)
{
	hit_type = MAGICHITTYPE + eMT_DataBall;
	SetTimer(10,2);
}
void DataBallModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	Timer.timer=0;
	stacollected=0;
	speed=0.1;
	HitEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.DataBall_Hit");
	SEntitySpawnParams params;
	params.sName="DataBall";
	params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
	params.vScale=Vec3(1,1,1);
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	pEffect=gEnv->pEntitySystem->SpawnEntity(params);
	SmartScriptTable proot=pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.DataBall");
	Script::CallMethod(proot,"OnReset");
	HasPreProcessed=true;
	hasSet=false;

	BaseAbility::PreProcess(_rdata);
	BaseAbility::SendMessage(eMT_DataBall, eET_ChangeProperty, _rdata);
	BaseAbility::SendMessage(eMT_Common, eET_ChangeProperty, _rdata);
}


void DataBallModule::Process(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	BaseAbility::PostProcess(_rdata);
	dir=gEnv->pRenderer->GetCamera().GetViewdir();
	pos=_rdata.hit.pt;
	pEffect->SetPos(pos);

	BaseAbility::SendMessage(eMT_DataBall, eET_ChangeProperty, _rdata);
	//pEffect->ParticleLoc(hit.pt,dir);
}

void DataBallModule::PostProcess(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	if(!hasSet)
	{
		hasSet=true;
		finished=true;
		Timer.Cdtimer=0;
		g_pGame->GetWorldStability()->StabilityCost(cost);
	}
	else
	{
		if(Timer.timer<Timer.Last_Time)
		{
			pos+=(dir*speed);
			float z_off=gEnv->p3DEngine->GetTerrainElevation(pos.x,pos.y);
			if(pos.z-z_off<0.5)
				pos.z=(float)(z_off+0.5);
			pEffect->SetPos(pos);
			BaseAbility::SendMessage(eMT_DataBall, eET_Moving, _rdata);
			//pEffect->ParticleLoc(pos,dir);
			Timer.timer+=_rdata.frameTime;
		}
		else
		{
			Collectdata();
			BallExplode(_rdata);
		}
	}
	_rdata.Finish_Pos = pos;
	BaseAbility::PostActive(_rdata);
}

void DataBallModule::Collectdata()
{
	IPhysicalEntity **nearbyEntities;
	int n=gEnv->pPhysicalWorld->GetEntitiesInBox(pos-Vec3(10),pos+Vec3(10),nearbyEntities,ent_living);
	for(int i=0;i<n;i++)
	{
		IEntity* pEntity=gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
		HitInfo hit;
		hit.shooterId = g_pGame->GetClientActorId();
		hit.targetId = pEntity->GetId();
		hit.dir = -(gEnv->pEntitySystem->GetEntity(hit.targetId)->GetPos()-pos).normalize();
		hit.bulletType = 0;
		hit.hitViaProxy = false;
		hit.partId = 0;
		hit.type = hit_type;
		hit.projectileId = 1;
		hit.weaponId = 1;
		if(hit.targetId!=hit.shooterId)
		{
			HitEffect->Spawn(true,IParticleEffect::ParticleLoc(gEnv->pEntitySystem->GetEntity(hit.targetId)->GetPos(),hit.dir));
			hit.damage=damage;
			g_pGame->GetGameRules()->ClientHit(hit);
			stacollected+=(int)(damage*multiplier);
		}
	}
}

void DataBallModule::Collectdata(Vec3 &pos)
{
	IPhysicalEntity **nearbyEntities;
	int n=gEnv->pPhysicalWorld->GetEntitiesInBox(pos-Vec3(10),pos+Vec3(10),nearbyEntities,ent_living);
	for(int i=0;i<n;i++)
	{
		IEntity* pEntity=gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
		HitInfo hit;
		hit.shooterId = g_pGame->GetClientActorId();
		hit.targetId = pEntity->GetId();
		hit.dir = -(gEnv->pEntitySystem->GetEntity(hit.targetId)->GetPos()-pos).normalize();
		hit.bulletType = 0;
		hit.hitViaProxy = false;
		hit.partId = 0;
		hit.type = hit_type;
		hit.projectileId = 1;
		hit.weaponId = 1;
		if(hit.targetId!=hit.shooterId)
		{
			if(!HitEffect)
				HitEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.DataBall_Hit");
			HitEffect->Spawn(true,IParticleEffect::ParticleLoc(gEnv->pEntitySystem->GetEntity(hit.targetId)->GetPos(),hit.dir));
			hit.damage=damage;
			g_pGame->GetGameRules()->ClientHit(hit);
			stacollected+=(int)(damage*multiplier);
		}
	}
	stacollected /=2;
	g_pGame->GetWorldStability()->StabilityGain(stacollected);
	stacollected = 0;
}

void DataBallModule::BallExplode(Process_Data &_rdata)
{
	IEntity *player=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity();
	ExplosionInfo einfo(player->GetId(), 0, 0, (float)(stacollected) , pos, Vec3(0,0,0),0.1f, 10.0f,  0.1,  10.0f , 0 , 1000.0f , 0.0f , hit_type);
	einfo.SetEffect("Code_System.Module_Effect.DataBall_Explosion",1.0f,0.2f);
	einfo.type = g_pGame->GetGameRules()->GetHitTypeId( "Explosion" );
	g_pGame->GetGameRules()->QueueExplosion(einfo);
	stacollected/=2;
	g_pGame->GetWorldStability()->StabilityGain(stacollected);
	hasSet=false;
	HasPreProcessed=false;
	gEnv->pEntitySystem->RemoveEntity(pEffect->GetId());
	g_pGame->GetWorldStability()->StabilityGain(stacollected);

	_rdata.Finish_Pos = pos;
	BaseAbility::SendMessage(eMT_DataBall, eET_Finished, _rdata);
}

void DataBallModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}

void DataBallModule::PostActive(Process_Data &_rdata)
{
	Collectdata(_rdata.Finish_Pos);
	BaseAbility::PostActive(_rdata);
}

void DataBallModule::ReceiveMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata)
{
	if((_mtype == eMT_Teleport || _mtype == eMT_Reposition)&& _etype == eET_Finished)
	{
		Collectdata(_rdata.Finish_Pos);
	}

	if((_mtype == eMT_Explosion || _mtype == eMT_ShockWave) && _etype == eET_Finished)
	{
		Collectdata(_rdata.Finish_Pos);
	}
}