#include "StdAfx.h"
#include "DataArrow.h"

DataArrow::DataArrow()
:	BaseAbility("DataArrow", 100,eMT_DataArrow)
,	hitCount(0)
,	maxHit(10)
,	hasTarget(false)
,	pTarget(NULL)
,	damage(500)
{
	HitSet.clear();
	hit_type = MAGICHITTYPE + eMT_DataArrow;
	SetTimer(4,0);
}

DataArrow::DataArrow(int _cost, int _maxhit, float _damage)
	:	BaseAbility("DataArrow", _cost,eMT_DataArrow)
	,	hitCount(0)
	,	maxHit(_maxhit)
	,	hasTarget(false)
	,	pTarget(NULL)
	,	damage(_damage)
{
	HitSet.clear();
	hit_type = MAGICHITTYPE + eMT_DataArrow;
	SetTimer(4,0);
}

void DataArrow::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	pEffect = gEnv->pEntitySystem->GetEntity(EffectId);
	if(!pEffect)
	{
		pEffect = gEnv->pEntitySystem->FindEntityByName("DA");
		if(!pEffect)
		{
			SpawnIndicateParticle();
		}
	}
	else
		pEffect->Hide(false);
	HasPreProcessed = true;
	hitCount = 0;
	HitSet.insert(g_pGame->GetClientActorId());
	IEntity *pPlayer = gEnv->pEntitySystem->GetEntity(g_pGame->GetClientActorId());
	pEffect->SetPos(pPlayer->GetPos());
	BaseAbility::SendMessage(eMT_DataArrow, eET_ChangeProperty,_rdata);
	BaseAbility::SendMessage(eMT_Common, eET_ChangeProperty,_rdata);
}

void DataArrow::Process(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	IEntity *pPlayer = gEnv->pEntitySystem->GetEntity(g_pGame->GetClientActorId());
	pEffect->SetPos(pPlayer->GetPos());
}


void DataArrow::PostProcess(Process_Data &_rdata)
{
	if(!HasPreProcessed || finished)
		return;
	if(hitCount < maxHit)
	{
		if(!hasTarget)
		{
			IPhysicalEntity **nearbyEntities;
			int num=gEnv->pPhysicalWorld->GetEntitiesInBox(pEffect->GetPos()-Vec3(20), pEffect->GetPos()+Vec3(20), nearbyEntities, ent_living);
			int count = 0;
			while(!hasTarget && count < maxHit*2)
			{
				if(num > 0)
				{
					int index = cry_rand32() % num;
					IEntity *pTempEntity = gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[index]);
					EntityId _id = pTempEntity->GetId();
					if(!HitSet.count(_id))
					{
						HitSet.insert(_id);
						hasTarget = true;
						pTarget = gEnv->pEntitySystem->GetEntity(_id);
					}
					else
						++count;
				}
			}
			if(count >= maxHit*2)
			{
				HitSet.clear();
				finished = true;
				HasPreProcessed = false;
				pTarget = NULL;
				pEffect->SetPos(Vec3(0,0,0));
				IParticleEffect *pEndEffect=gEnv->pParticleManager->FindEffect("Code_System.WarpSpawn");
				if(pEndEffect)
					pEndEffect->Spawn(true,IParticleEffect::ParticleLoc(pEffect->GetPos(),Vec3(0,0,1)));
			}
		}
		//Moving Phase
		else
		{
			AABB aabb;
			pTarget->GetWorldBounds(aabb);

			Vec3 pos = pEffect->GetPos();
			Vec3 targetPos = aabb.GetCenter();
			Vec3 dir = (targetPos-pos).normalize();
			float length = (targetPos - pos).GetLengthFast();
			if(length > 50*_rdata.frameTime*2)
			{
				Quat rot = Quat::CreateRotationVDir(dir);
				pEffect->SetRotation(rot);
				pos += dir*50*_rdata.frameTime;
				pEffect->SetPos(pos);
			}
			else
			{
				IEntity *pPlayer = gEnv->pEntitySystem->GetEntity(g_pGame->GetClientActorId());
				HitInfo hit;
				hit.shooterId = pPlayer->GetId();
				hit.targetId = pTarget->GetId();
				hit.dir = Vec3(0,0,1);
				hit.weaponId = 0;
				hit.damage = damage;
				hit.type = hit_type;
				g_pGame->GetGameRules()->ClientHit(hit);
				IParticleEffect *pCoreEffect = gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.DataArrow_Hit");
				pCoreEffect->Spawn(true,IParticleEffect::ParticleLoc(aabb.GetCenter(),dir));
				hasTarget = false;
				++hitCount;
			}
		}
	}
	else
	{
		HitSet.clear();
		finished = true;
		HasPreProcessed = false;
		pTarget = NULL;
		pEffect->SetPos(Vec3(0,0,0));
		IParticleEffect *pEndEffect=gEnv->pParticleManager->FindEffect("Code_System.WarpSpawn");
		if(pEndEffect)
			pEndEffect->Spawn(true,IParticleEffect::ParticleLoc(pEffect->GetPos(),Vec3(0,0,1)));
	}
}

void DataArrow::PreActive(Process_Data &_rdata){}
void DataArrow::Active(Process_Data &_rdata){}
void DataArrow::PostActive(Process_Data &_rdata){}

void DataArrow::Cool_Down(float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}

void DataArrow::SpawnIndicateParticle()
{
	SEntitySpawnParams params;
	params.sName = "DA";

	params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
	params.vScale=Vec3(1,1,1);
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	pEffect=gEnv->pEntitySystem->SpawnEntity(params);

	SmartScriptTable proot=pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.DataArrow");
	Script::CallMethod(proot,"OnReset");

	EffectId = pEffect->GetId();
}