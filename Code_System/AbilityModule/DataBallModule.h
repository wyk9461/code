#ifndef _DATABALLMODULE_H_
#define _DATABALLMODULE_H_

#include "BaseAbility.h"

class DataBallModule : public BaseAbility
{
private:
	IEntity *pEffect;
	IParticleEffect *HitEffect;
	float speed;
	Vec3 dir;
	Vec3 pos;
	float damage;
	int stacollected;
	float multiplier;
	bool hasSet;
public:
	DataBallModule();

	DataBallModule(int _cost, float _damage, float _multi);

	~DataBallModule(){}
	virtual const bool isLethal(){return true;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);
	void RegularUpdate(Process_Data &_rdata){}

	void Cool_Down(float frameTime);

	void Collectdata();
	void Collectdata(Vec3 &pos);
	void BallExplode(Process_Data &_rdata);

	inline const bool isSet(){return hasSet;}

	void PreActive(Process_Data &_rdata){}
	void Active(Process_Data &_rdata){}
	void PostActive(Process_Data &_rdata);

	void ReceiveMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata);

	void SetSpeed(float _speed){speed = _speed;}
	const float GetSpeed(){return speed;}

	void SetDamage(float _damage){damage = _damage;}
	const float GetDamage(){return damage;}
};
#endif