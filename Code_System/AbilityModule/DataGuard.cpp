#include "StdAfx.h"
#include "DataGuard.h"

DataGuardModule::DataGuardModule()
:	BaseAbility("Data_Guard",100,eMT_DataGuard)
,	CanReleaseData(false)
,	data_count(0)
,	maxDataGuard(3)
,	Explosiondamage(200)
{
	the_guards.reserve(5);
	hit_type = MAGICHITTYPE + eMT_DataGuard;
	SetTimer(4,0);
}

DataGuardModule::DataGuardModule(int _cost, int _max_guard, float _edamage)
	:	BaseAbility("Data_Guard",_cost,eMT_DataGuard)
	,	CanReleaseData(false)
	,	data_count(0)
	,	maxDataGuard(_max_guard)
	,	Explosiondamage(_edamage)
{
	the_guards.reserve(5);
	hit_type = MAGICHITTYPE + eMT_DataGuard;
	SetTimer(4,0);
}


void DataGuardModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed)
		return;

	if(data_count > 0)
	{
		CanReleaseData = true;
		ReleaseData(_rdata);
		return;
	}


	for(int i=0; i < maxDataGuard; ++i)
	{
		the_guards[i].pEffect = gEnv->pEntitySystem->GetEntity(the_guards[i].EffectId);
		if(!the_guards[i].pEffect)
		{
			the_guards[i].pEffect = gEnv->pEntitySystem->FindEntityByName("DGP");
			if(!the_guards[i].pEffect)
			{
				SpawnIndicateParticle(i);
				data_count = maxDataGuard;
				CanReleaseData = true;
			}
		}
		else
		{
			the_guards[i].pEffect->Hide(false);
			the_guards[i].available = true;
			data_count = maxDataGuard;
			CanReleaseData = true;
		}
	}
	HasPreProcessed = true;
	BaseAbility::Active(_rdata);
	BaseAbility::PreProcess(_rdata);
}


void DataGuardModule::Process(Process_Data &_rdata)
{
	for(int i=0; i<the_guards.size(); ++i)
	{
		if(the_guards[i].available)
			the_guards[i].targetPos = _rdata.hit.pt;
	}
}


void DataGuardModule::PostProcess(Process_Data &_rdata)
{
	HasPreProcessed = false;
	BaseAbility::PostProcess(_rdata);
	if(data_count > 0)
	{
		for(int i=0; i<the_guards.size(); ++i)
		{
			if(!the_guards[i].available)
			{
				Vec3 dir = the_guards[i].targetPos - the_guards[i].pEffect->GetPos();
				dir.normalize();
				Vec3 new_pos = the_guards[i].pEffect->GetPos() + dir*1;
				the_guards[i].pEffect->SetPos(new_pos);
				float length = (the_guards[i].targetPos - new_pos).GetLengthSquared();
				if(length <0.5)
				{
					CommonUsage::CreateExplosion(g_pGame->GetClientActorId(), the_guards[i].targetPos,Explosiondamage,5,300,hit_type);
					--data_count;
					the_guards[i].pEffect->Hide(true);
					the_guards[i].pEffect->SetPos(Vec3(0,0,0));
					if(data_count == 0)
					{
						data_count = 0;
						finished = true;
						CanReleaseData = false;
					}
					return;
				}
			}
			else
			{
				AABB aabb;
				g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetWorldBounds(aabb);
				Vec3 player_pos = aabb.GetCenter();
				Vec3 new_pos = CommonUsage::MoveAroundPoint(the_guards[i].pEffect, player_pos, (float)(1*i), the_guards[i].angle, the_guards[i].z_offset);
				the_guards[i].pEffect->SetPos(new_pos);
				the_guards[i].angle += 2 * i;
				the_guards[i].z_offset += (float)(0.7 * i);
			}
		}
	}
}

void DataGuardModule::ReleaseData(Process_Data &_rdata)
{
	HasPreProcessed = true;
	for(int i=0; i<the_guards.size(); ++i)
	{
		if(the_guards[i].available)
		{
			the_guards[i].available = false;
			the_guards[i].targetPos = _rdata.hit.pt;
			return;
		}
	}
}

void DataGuardModule::RegularUpdate(Process_Data &_rdata)
{

}

void DataGuardModule::PreActive(Process_Data &_rdata){}
void DataGuardModule::Active(Process_Data &_rdata){}
void DataGuardModule::PostActive(Process_Data &_rdata){}

void DataGuardModule::Cool_Down(float frameTime)
{
	if(finished)
	{
		Timer.Cdtimer+=frameTime;
		if(Timer.Cdtimer>=Timer.Cd)
		{
			finished=false;
			Timer.Cdtimer=0;
			data_count = 0;
		}
	}
}


void DataGuardModule::SpawnIndicateParticle(int index)
{
	SEntitySpawnParams params;
	Guard new_guard;
	params.sName = "DGP";

	params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
	params.vScale=Vec3(1,1,1);
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	new_guard.pEffect=gEnv->pEntitySystem->SpawnEntity(params);

	SmartScriptTable proot=new_guard.pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.Teleport");
	Script::CallMethod(proot,"OnReset");

	new_guard.EffectId = new_guard.pEffect->GetId();
	new_guard.available = true;
	the_guards.push_back(new_guard);
}

const bool DataGuardModule::AllVanished()
{
	for(int i=0; i<the_guards.size(); ++i)
	{
		if(the_guards[i].available)
			return false;
	}
	return true;
}