#ifndef _BULLETSTROM_H_
#define _BULLETSTROM_H_

#include "BaseAbility.h"

class BulletStromModule : public BaseAbility
{
public:
	BulletStromModule();

	BulletStromModule(int _cost, unsigned int _maxBullet);

	~BulletStromModule(){}
	virtual const bool isLethal(){return true;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);

	void PreActive(Process_Data &_rdata);
	void Active(Process_Data &_rdata);
	void PostActive(Process_Data &_rdata);

	void Cool_Down(float frameTime);

	void RegularUpdate(Process_Data &_rdata){};

private:
	IEntity *pEffect;
	EntityId EffectId;
	Vec3 dir;
	bool hasSet;
	unsigned int bullet_count;
	unsigned int max_bullet;
	IEntityClass *pBulletClass;
	void SpawnIndicateParticle();
};
#endif