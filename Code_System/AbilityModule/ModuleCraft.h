#ifndef _MODULECRAFT_H_
#define _MODULECRAFT_H_

#include "BaseAbility.h"

class ModuleCraft
{
public:
	ModuleCraft();
	~ModuleCraft();
	void Combine(AbilityComponent *parent , AbilityComponent *child);
	void Remove(AbilityComponent *parent , AbilityComponent *child);
	BaseAbility* Craft(BaseAbility *parent , BaseAbility *child);
private:

};
#endif