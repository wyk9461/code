#include "StdAfx.h"
#include "DataBiteModule.h"
#include "Weapon.h"

DataBiteModule::DataBiteModule()
	:	BaseAbility("DataBite",100,eMT_DataBite)
	,	pEffect(NULL)
	,	pTarget(NULL)
	,	heal_rate(1)
	,	heal_log(0)
	,	maxhealth(100)
	,	hasLaunched(false)
	,	hasSet(false)
{
	hit_type = MAGICHITTYPE + eMT_DataBite;
	SetTimer(10,5);
}

DataBiteModule::DataBiteModule(int _cost, float _rate)
	:	BaseAbility("DataBite",_cost,eMT_DataBite)
	,	pEffect(NULL)
	,	pTarget(NULL)
	,	heal_rate(_rate)
	,	heal_log(0)
	,	maxhealth(100)
	,	hasLaunched(false)
	,	hasSet(false)
{
	hit_type = MAGICHITTYPE + eMT_DataBite;
	SetTimer(10,5);
}

void DataBiteModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	Timer.timer=0;
	g_pGame->GetBasicRayCast()->ChangeDistance(1.0);
	g_pGame->GetBasicRayCast()->ChangeFlag(ent_all);
	HasPreProcessed=true;
	heal_log=0;
	maxhealth=0;
	hasLaunched=false;
	heal_rate=1;
	SEntitySpawnParams params;
	params.sName="PreDataBite";
	params.vScale=Vec3(1,1,1);
	params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetCurrentItem(false)->GetEntity()->GetPos();
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	pEffect=gEnv->pEntitySystem->SpawnEntity(params);
	SmartScriptTable proot=pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.PreDataBite");
	Script::CallMethod(proot,"OnReset");
	g_pGame->GetIGameFramework()->GetClientActor()->GetCurrentItem()->GetEntity()->AttachChild(pEffect);
	BaseAbility::PreProcess(_rdata);
}

void DataBiteModule::Process(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	IInventory *Inventory=g_pGame->GetIGameFramework()->GetClientActor()->GetInventory();
	IItem *curItem=g_pGame->GetIGameFramework()->GetClientActor()->GetCurrentItem(false);
	CWeapon *weapon=(CWeapon*)curItem->GetIWeapon();
	Vec3 pos=weapon->GetSlotHelperPos(eIGS_FirstPerson,"silencer_attach",true);
	pEffect->SetPos(pos);
}

void DataBiteModule::PostProcess(Process_Data &_rdata)
{
	if(!HasPreProcessed || !hasSet)
		return;
	BaseAbility::PostProcess(_rdata);
	if(!hasLaunched)
	{
		IEntity *pEntity=pTarget->GetEntity();
		g_pGame->GetIGameFramework()->GetClientActor()->GetCurrentItem()->GetEntity()->DetachAll();
		pEntity->AttachChild(pEffect);
		pEffect->SetPos(pTarget->GetEntity()->GetPos());
		float curhealth=pTarget->GetHealth();
		maxhealth=pTarget->GetMaxHealth();
		pTarget->SetMaxHealth(maxhealth*5);
		pTarget->SetHealth(curhealth);
		SmartScriptTable proot=pEffect->GetScriptTable();
		SmartScriptTable pprops;
		proot->GetValue("Properties",pprops);
		pprops->SetValue("ParticleEffect","Code_System.Module_Effect.DataBite");
		Script::CallMethod(proot,"OnReset");
		hasLaunched=true;
		g_pGame->GetBasicRayCast()->Load();
	}
	if(hasSet)
	{
		finished=true;
		if(Timer.timer<Timer.Last_Time)
		{
			if(!pTarget->IsDead())
			{
				pTarget->SetHealth(pTarget->GetHealth()+heal_rate);
				heal_log+=heal_rate;
				Timer.timer+=_rdata.frameTime;
			}
			else
				DataExplode();
		}
		else
		{
			gEnv->pEntitySystem->RemoveEntity(pEffect->GetId());
			pTarget=NULL;
			hasSet=false;
			HasPreProcessed=false;
		}
	}
}

void DataBiteModule::DataExplode()
{
	gEnv->pEntitySystem->RemoveEntity(pEffect->GetId());
	ExplosionInfo einfo(pTarget->GetEntityId(), 0, 0, heal_log , pTarget->GetEntity()->GetPos(), Vec3(0,0,0),0.1f, 5.0f,  0.1,  5.0f , 0 , 1000.0f , 0.0f , hit_type);
	einfo.SetEffect("explosions.Grenade_SCAR.character",1.0f,0.2f);  //Need to change
	einfo.type = g_pGame->GetGameRules()->GetHitTypeId( "Explosion" );
	g_pGame->GetGameRules()->QueueExplosion(einfo);
	pTarget=NULL;
	hasSet=false;
	HasPreProcessed=false;
}


void DataBiteModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}