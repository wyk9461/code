#include "StdAfx.h"
#include "BaseAbility.h"
#include "Game.h"
#include "IFlashUI.h"


void BaseAbility::StoreInventory()
{
	CActor *pSelfActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	CItemSystem* pItemSystem = static_cast<CItemSystem*> (g_pGame->GetIGameFramework()->GetIItemSystem());
	IItem *pItem=pSelfActor->GetCurrentItem(false);
	/*if(pItem)
	store_weapon_id=pItem->GetEntity()->GetId();*/
	pItemSystem->SerializePlayerLTLInfo(false);
	IInventory* pInventory = pSelfActor->GetInventory();
	pInventory->RMIReqToServer_RemoveAllItems();
}

void BaseAbility::RestoreInventory()
{
	CActor *pSelfActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	CItemSystem* pItemSystem = static_cast<CItemSystem*> (g_pGame->GetIGameFramework()->GetIItemSystem());
	pItemSystem->SerializePlayerLTLInfo(true);
	//pSelfActor->SelectLastItem(true);
	/*if(store_weapon_id)
		pSelfActor->SelectItem(store_weapon_id,true,true);*/
}


const int BaseAbility::GetStaRemain()
{
	return g_pGame->GetWorldStability()->Getstability() - cost;
}

void BaseAbility::PreProcess(Process_Data &_rdata)
{
	/*IUIElement *pElement = gEnv->pFlashUI->GetUIElement("HUD_Final");
	if(pElement)
	{
		SUIArguments args;
		args.AddArgument(GetCost());
		args.AddArgument(GetStaRemain());
		pElement->CallFunction("SetMidStability", args);
	}*/
}

void BaseAbility::PostProcess(Process_Data &_rdata)
{
	/*IUIElement *pElement = gEnv->pFlashUI->GetUIElement("HUD_Final");
	pElement->CallFunction("EnableMidIndicator",SUIArguments::Create(false));*/
}

bool BaseAbility::AddComponent(AbilityComponent *new_module)
{
	std::list<AbilityComponent*>::iterator iter = std::find(m_Component.begin(),m_Component.end(),new_module);
	if(iter == m_Component.end())
	{
		unsigned int new_id = Child_Id_Pool.AssignId();
		if(new_id)
		{
			m_Component.push_back(new_module);
			new_module->SetComponentId(new_id);
			return true;
		}
	}
	return false;
}

bool BaseAbility::RemoveComponent(AbilityComponent *old_module)
{
	std::list<AbilityComponent*>::iterator iter = std::find(m_Component.begin(),m_Component.end(),old_module);
	if(iter != m_Component.end())
	{
		m_Component.remove(old_module);
		Child_Id_Pool.RecoveryId(old_module->GetComponentId());
		return true;
	}
	return false;
}

void BaseAbility::PreActive(Process_Data &_rdata)
{
	if(!m_Component.empty())
	{
		std::list<AbilityComponent*>::iterator iter = m_Component.begin();
		std::list<AbilityComponent*>::iterator end = m_Component.end();
		while(iter != end)
		{
			(*iter)->PreActive(_rdata);
			++iter;
		}
	}
}

void BaseAbility::Active(Process_Data &_rdata)
{
	if(!m_Component.empty())
	{
		std::list<AbilityComponent*>::iterator iter = m_Component.begin();
		std::list<AbilityComponent*>::iterator end = m_Component.end();
		while(iter != end)
		{
			(*iter)->Active(_rdata);
			++iter;
		}
	}
}

void BaseAbility::PostActive(Process_Data &_rdata)
{
	if(!m_Component.empty())
	{
		std::list<AbilityComponent*>::iterator iter = m_Component.begin();
		std::list<AbilityComponent*>::iterator end = m_Component.end();
		while(iter != end)
		{
			(*iter)->PostActive(_rdata);
			++iter;
		}
	}
}

void BaseAbility::SendMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata)
{
	if(!m_Component.empty())
	{
		std::list<AbilityComponent*>::iterator iter = m_Component.begin();
		std::list<AbilityComponent*>::iterator end = m_Component.end();
		while(iter != end)
		{
			(*iter)->ReceiveMessage(_mtype, _etype, _rdata);
			++iter;
		}
	}
}

const bool BaseAbility::Lethal()
{
	if(isLethal())
		return true;
	std::list<AbilityComponent*>::iterator iter = m_Component.begin();
	std::list<AbilityComponent*>::iterator end = m_Component.end();
	while(iter != end)
	{
		if((*iter)->isLethal())
			return true;
		++iter;
	}
	return false;
}


AbilityEnhancement::AbilityEnhancement(float _cooldown, float _damage, float _lasttime, float _cost, float _radius, char *name, int _amount, unsigned int _Id)
:	Cool_Down_Bonus(_cooldown)
,	Damage_Bonus(_damage)
,	Last_Time_Bonus(_lasttime)
,	Cost_Bonus(_cost)
,	Radius_Bonus(_radius)
,	AbilityComponent(name)
,	BaseInventory(name,_amount,eIT_Enhancement)
{
	SetId(_Id);
}