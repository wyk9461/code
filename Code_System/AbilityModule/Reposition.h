#ifndef _REPOSITION_H_
#define _REPOSITION_H_

#include "BaseAbility.h"
#include "Code_System/BasicRayCast.h"

class RepositionModule : public BaseAbility
{
public:
	RepositionModule();

	RepositionModule(int _cost);

	~RepositionModule(){}
	virtual const bool isLethal(){return false;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);

	void PreActive(Process_Data &_rdata);
	void Active(Process_Data &_rdata);
	void PostActive(Process_Data &_rdata);

	void Cool_Down(float frameTime);

	void RegularUpdate(Process_Data &_rdata){};

	void ReceiveMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata);
private:
	void SpawnIndicateParticle();
	void ChangePosition(IEntity *pTarget);
	IEntity *pEffect;
	EntityId EffectId;
	IEntity *pTarget;

	RayCastBackup init_backup;
	RayCastBackup Reposition_backup;
};
#endif