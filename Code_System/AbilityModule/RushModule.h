#ifndef _RUSHMODULE_H_
#define _RUSHMODULE_H_

#include "BaseAbility.h"
#include "IParticles.h"
#include "Code_System/BasicRayCast.h"


class RushModule : public BaseAbility
{
private:
	IParticleEffect *pEffect;
	float curhealth;
	float maxhealth;
	Vec3 dir;
	Vec3 pos;
	float speed;
	float damage;
	bool hasSet;
	bool once;

	IView *default_View;
	IView *rush_View;

	BasicRayCast *pBRC;

public:
	RushModule();

	RushModule(int _cost, float _speed);

	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);
	void RegularUpdate(Process_Data &_rdata){}

	virtual const bool isLethal(){return false;}
	void Cool_Down(float frameTime);

	inline const bool isSet(){return hasSet;}

	void PreActive(Process_Data &_rdata);
	void Active(Process_Data &_rdata){};
	void PostActive(Process_Data &_rdata);

	void ReceiveMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata);
};

#endif