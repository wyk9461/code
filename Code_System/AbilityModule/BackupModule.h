#ifndef _BACKUPMODULE_H_
#define _BACKUPMODULE_H_

#include "BaseAbility.h"

class BackupModule : public BaseAbility
{
private:
	IEntity *pEntity;
	float backup_multiplier;
	float health;
	bool hasSet;

	Vec3 OrinPos;
public:
	BackupModule();

	BackupModule(int _cost, float _multi);

	~BackupModule(){}
	virtual const bool isLethal(){return false;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);
	void RegularUpdate(Process_Data &_rdata){}
	virtual const bool Lethal(){return false;}

	void Cool_Down(float frameTime);

	inline const bool isSet(){return hasSet;}

	void PreActive(Process_Data &_rdata){}
	void Active(Process_Data &_rdata);
	void PostActive(Process_Data &_rdata);
};
#endif
