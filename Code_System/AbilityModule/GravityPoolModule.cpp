#include "StdAfx.h"
#include "GravityPoolModule.h"
#include "Code_System/MicroSystem/FallenActorManager.h"


GravityPoolModule::GravityPoolModule()
:	BaseAbility("Gravity_Pool", 100,eMT_GravityPool)
,	z_force(10)
,	hasSet(false)
{
	SetTimer(10,3);
}

GravityPoolModule::GravityPoolModule(int _cost, float _force)
	:	BaseAbility("Gravity_Pool", _cost,eMT_GravityPool)
	,	z_force(_force)
	,	hasSet(false)
{
	SetTimer(10,3);
}

void GravityPoolModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	pEffect = gEnv->pEntitySystem->GetEntity(EffectId);
	if(!pEffect)
	{
		pEffect = gEnv->pEntitySystem->FindEntityByName("GPIP");
		if(!pEffect)
		{
			SpawnIndicateParticle();
		}
	}
	else
		pEffect->Hide(false);
	HasPreProcessed = true;
	BaseAbility::Active(_rdata);
	BaseAbility::PreProcess(_rdata);

}

void GravityPoolModule::Process(Process_Data &_rdata)
{
	if(HasPreProcessed)
	{
		if(pEffect)
		{
			pEffect->SetPos(_rdata.hit.pt);
			BaseAbility::Active(_rdata);
		}
	}
}


void GravityPoolModule::PostProcess(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	BaseAbility::PostProcess(_rdata);
	finished = true;
	HasPreProcessed=false;
	hasSet = true;
	if(pEffect)
		pEffect->Hide(true);
}

void GravityPoolModule::RegularUpdate(Process_Data &_rdata)
{
	if(hasSet == true)
	{
		Timer.timer += _rdata.frameTime;
		Vec3 Pool_Position = pEffect->GetPos();
		IPhysicalEntity** nearbyEntities;
		int num=gEnv->pPhysicalWorld->GetEntitiesInBox(Pool_Position-Vec3(5),Pool_Position+Vec3(5),nearbyEntities,ent_rigid | ent_sleeping_rigid | ent_living);
		for(int itera=0; itera<num; ++itera)
		{
			IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[itera]);
			if(pEntity)
			{
				//Code Note: Add Impulse
				IEntityPhysicalProxy *pPhysicsProxy = static_cast<IEntityPhysicalProxy*>(pEntity->GetProxy(ENTITY_PROXY_PHYSICS));
				if(pPhysicsProxy)
				{
					pEntity->EnablePhysics(true);
					pe_status_dynamics params;
					pEntity->GetPhysics()->GetStatus(&params);
					float &mass = params.mass;
					CActor *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId()));
					if(pActor && pActor != g_pGame->GetIGameFramework()->GetClientActor())
					{
						if(!pActor->IsFallen())
							FallenActorManager::Instance().AddToManager(pActor);
					}
					Vec3 dir(0,0,1);
					dir.x = BiRandom(1);
					dir.y = BiRandom(1);
					dir.normalize();
					if(!pActor)
						pPhysicsProxy->AddImpulse(-1, pEntity->GetPos(), dir*mass*0.5, false, 1);
					else
						pPhysicsProxy->AddImpulse(-1, pEntity->GetPos(), dir*mass*0.1, false, 1);
				}
			}
		}
		if(Timer.timer >= Timer.Last_Time)
		{
			hasSet = false;
		}
	}
	if(!Forced_EntitySet.empty())
	{
		std::map<EntityId, EntityId>::iterator iter= Forced_EntitySet.begin();
		std::map<EntityId, EntityId>::iterator end = Forced_EntitySet.end();
		while(iter != end)
		{
			CActor *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(iter->first));
			if(pActor)
			{
				CItem *pItem = (CItem *)g_pGame->GetIGameFramework()->GetIItemSystem()->GetItem(iter->second);
				if(pItem)
				{
					float z_distance;
					z_distance = pItem->GetEntity()->GetPos().z - pActor->GetEntity()->GetPos().z;
					if(z_distance < 1)
					{
						pActor->PickUpItem(iter->second,true,true);
						Forced_EntitySet.erase(iter++);
					}
					else
						++iter;
				}
				else
					Forced_EntitySet.erase(iter++);
			}
			else
				++iter;
		}
		if(Forced_EntitySet.size() > 100)
			Forced_EntitySet.clear();
	}
}

void GravityPoolModule::PostActive(Process_Data &_rdata)
{
	BaseAbility::PostActive(_rdata);
}

void GravityPoolModule::Cool_Down(float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		hasSet = false;
		Timer.Cdtimer=0;
		Timer.timer = 0;
	}
}


void GravityPoolModule::SpawnIndicateParticle()
{
	SEntitySpawnParams params;
	params.sName = "GPIP";

	params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
	params.vScale=Vec3(1,1,1);
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	pEffect=gEnv->pEntitySystem->SpawnEntity(params);

	SmartScriptTable proot=pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.Teleport");
	Script::CallMethod(proot,"OnReset");

	EffectId = pEffect->GetId();
}