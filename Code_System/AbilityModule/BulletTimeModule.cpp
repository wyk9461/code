#include "StdAfx.h"
#include "BulletTimeModule.h"
#include "Code_System/Perk.h"

BulletTimeModule::BulletTimeModule()
	:	BaseAbility("BulletTime",100,eMT_BulletTime)
	,	hasSet(false)
{
	SetTimer(10,3);
}

BulletTimeModule::BulletTimeModule(int _cost)
	:	BaseAbility("BulletTime",_cost,eMT_BulletTime)
	,	hasSet(false)
{
	SetTimer(10,3);
}
void BulletTimeModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;
	HasPreProcessed=true;
	Timer.timer=0;
	hasSet=false;

	BaseAbility::PreProcess(_rdata);
	BaseAbility::PreActive(_rdata);
}

void BulletTimeModule::Process(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	gEnv->p3DEngine->SetPostEffectParam("Global_Contrast", 0.3f);
	BaseAbility::Active(_rdata);
}

void BulletTimeModule::PostProcess(Process_Data &_rdata)
{
	if(!hasSet || !HasPreProcessed)
		return;
	BaseAbility::PostProcess(_rdata);
	ICVar *timeScaleCVar=gEnv->pConsole->GetCVar("t_Scale");
	float scale=timeScaleCVar->GetFVal();
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	if(!hasSet)
	{
		g_pGame->GetWorldStability()->StabilityCost(cost);
		finished=true;
		timeScaleCVar->Set(0.3f);
		gEnv->pRenderer->EF_SetPostEffectParam("Global_User_Saturation", -0.5);
		hasSet=true;
	}
	if(Timer.timer<Timer.Last_Time)
	{
		Timer.timer+=_rdata.frameTime;
		pActor->SetSpeedMultipler(SActorParams::eSMR_Internal,(float)(1/scale));
		pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,(float)(1/scale));
		pActor->SetSpeedMultipler(SActorParams::eSMR_Item,(float)(1/scale));
	}
	if(Timer.timer>Timer.Last_Time-1 && Timer.timer<Timer.Last_Time)
		timeScaleCVar->Set(scale+=1/1*_rdata.frameTime);
	if(Timer.timer>Timer.Last_Time)
	{
		gEnv->pRenderer->EF_SetPostEffectParam("Global_User_Saturation", 1);
		ICVar *timeScaleCVar=gEnv->pConsole->GetCVar("t_Scale");
		timeScaleCVar->Set(1.0f);
		Timer.Cdtimer=0;
		HasPreProcessed=false;
		pActor->SetSpeedMultipler(SActorParams::eSMR_Internal,PerkManager::Instance().GetBonusMoveSpeed());
		pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,PerkManager::Instance().GetBonusMoveSpeed());
		pActor->SetSpeedMultipler(SActorParams::eSMR_Item,PerkManager::Instance().GetBonusMoveSpeed());
		hasSet=false;
		BaseAbility::PostActive(_rdata);
	}
}

void BulletTimeModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}

void BulletTimeModule::PreActive(Process_Data &_rdata)
{
	BaseAbility *parent = (BaseAbility*)GetParent();
	if(parent)
	{
		Timer_Cd &Timer = parent->GetTimer();
		Timer.Last_Time += static_cast<float>(Timer.Last_Time *0.25);
	}
}