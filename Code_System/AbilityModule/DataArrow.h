#ifndef _DATAARROW_H_
#define _DATAARROW_H_

#include "BaseAbility.h"

class DataArrow : public BaseAbility
{
public:
	DataArrow();

	DataArrow(int _cost, int _maxhit, float _damage);
	~DataArrow(){}
	virtual const bool isLethal(){return true;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);

	void PreActive(Process_Data &_rdata);
	void Active(Process_Data &_rdata);
	void PostActive(Process_Data &_rdata);

	void Cool_Down(float frameTime);

	void RegularUpdate(Process_Data &_rdata){};

private:
	int hitCount;
	int maxHit;
	bool hasTarget;
	float damage;
	IEntity *pEffect;
	IEntity *pTarget;
	EntityId EffectId;
	IEntityClass *pBulletClass;
	void SpawnIndicateParticle();

	std::set<EntityId> HitSet;
};
#endif