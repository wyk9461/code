#ifndef _GRABMODULE_H_
#define _GRABMODULE_H_

#include "BaseAbility.h"


class GrabModule : public BaseAbility
{
private:
	IEntity *pEntity;
	IEntity* pEffect;
	float rate;
	float damage;
	Vec3 finalpos;
	bool isGrab;
	bool hasSet;
	bool Detected;

public:
	GrabModule();

	GrabModule(int _cost);

	~GrabModule(){}
	virtual const bool isLethal(){return false;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);
	void Throw(ray_hit &hit);
	void ThrowEntityExplode(float frameTime);
	void RegularUpdate(Process_Data &_rdata){}


	void Cool_Down(float frameTime);

	inline const bool IsGrab(){return isGrab;}
	inline const bool IsDetected(){return Detected;}
};
#endif