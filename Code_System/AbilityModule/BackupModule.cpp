#include "StdAfx.h"
#include "BackupModule.h"
#include "ItemSystem.h"

BackupModule::BackupModule()
	:	BaseAbility("Backup",100,eMT_Backup)
	,	pEntity(NULL)
	,	backup_multiplier(0.8)
	,	hasSet(false)
{
	SetTimer(10,5);
}

BackupModule::BackupModule(int _cost, float _multi) 
	:	BaseAbility("Backup",_cost,eMT_Backup)
	,	pEntity(NULL)
	,	backup_multiplier(_multi)
	,	hasSet(false)
{
	SetTimer(10,5);
}


void BackupModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;
	Timer.timer=0;
	HasPreProcessed=true;
	hasSet=false;
	BaseAbility::PreProcess(_rdata);
	BaseAbility::PreActive(_rdata);
}

void BackupModule::Process(Process_Data &_rdata)
{
	BaseAbility::Active(_rdata);
}

void BackupModule::PostProcess(Process_Data &_rdata)
{
	if(!hasSet)
	{
		BaseAbility::PostProcess(_rdata);
		HasPreProcessed=false;
		finished=true;
		hasSet=true;
		SEntitySpawnParams params;
		params.sName="Backup";
		params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
		params.vScale=Vec3(1,1,1);
		params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("BasicEntity");

		pEntity=gEnv->pEntitySystem->SpawnEntity(params);
		pEntity->LoadGeometry(0,"Editor/Objects/spawnpointhelper.cgf");
		pEntity->EnablePhysics(false);

		//pEntity->SetMaterial(gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("spawn"));
		CActor *pSelfActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
		CItemSystem* pItemSystem = static_cast<CItemSystem*> (g_pGame->GetIGameFramework()->GetIItemSystem());
		IItem *pItem=pSelfActor->GetCurrentItem(false);
		//store_weapon_id=pItem->GetEntity()->GetId();
		pItemSystem->SerializePlayerLTLInfo(false);
		CActor *player=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
		health=player->GetHealth();
	}
	else
	{
		Timer.timer+=_rdata.frameTime;
		if(Timer.timer>=Timer.Last_Time)
		{
			CActor *player=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
			player->GetEntity()->SetPos(pEntity->GetPos());
			player->SetHealth((float)(health*backup_multiplier));
			RestoreInventory();
			hasSet=false;
			gEnv->pEntitySystem->RemoveEntity(pEntity->GetId());
			pEntity=NULL;
			BaseAbility::PostActive(_rdata);
		}
	}

}


void BackupModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}

void BackupModule::Active(Process_Data &_rdata)
{
	OrinPos = _rdata.Finish_Pos;
	BaseAbility::Active(_rdata);
}

void BackupModule::PostActive(Process_Data &_rdata)
{
	IEntity* pPlayer = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity();
	pPlayer->SetPos(OrinPos);
	BaseAbility::PostActive(_rdata);
}