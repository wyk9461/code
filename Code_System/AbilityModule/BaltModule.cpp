#include "StdAfx.h"
#include "BaltModule.h"

BaltModule::BaltModule()
:	BaseAbility("BaleModule", 100,eMT_Balt)
,	hasSet(false)
{
	SetTimer(60,30);
}

BaltModule::BaltModule(int _cost)
:	BaseAbility("BaleModule", _cost,eMT_Balt)
,	hasSet(false)
{
	SetTimer(60,30);
}



void BaltModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	EnemyFactionId = gEnv->pAISystem->GetFactionMap().GetFactionID("Grunt");
	PlayerFactionId = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetAI()->GetFactionID();
	HasPreProcessed = true;
	BaseAbility::PreProcess(_rdata);
}

void BaltModule::Process(Process_Data &_rdata)
{

}

void BaltModule::PostProcess(Process_Data &_rdata)
{	
	if(!HasPreProcessed)
		return;
	BaseAbility::PostProcess(_rdata);
	if(!hasSet)
	{
		hasSet = true;
		finished = true;
		HasPreProcessed = false;
		SpawnBalt();
	}
}

void BaltModule::PreActive(Process_Data &_rdata){}
void BaltModule::Active(Process_Data &_rdata){}
void BaltModule::PostActive(Process_Data &_rdata){}

void BaltModule::RegularUpdate(Process_Data &_rdata)
{
	if(hasSet)
	{
		Timer.timer += _rdata.frameTime;
		if(Timer.timer > Timer.Last_Time)
		{
			Timer.timer = 0;
			g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetAI()->SetFactionID(PlayerFactionId);
			HideBalt();
		}
	}
}

void BaltModule::Cool_Down(float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
		hasSet = false;
	}
}

void BaltModule::SpawnBalt()
{
	if(Balts.empty())
	{
		Vec3 SpawnPos = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
		SpawnPos.x += BiRandom(1.5);
		SpawnPos.y +=BiRandom(1.5);
		SpawnPos.z = gEnv->p3DEngine->GetTerrainElevation(SpawnPos.x,SpawnPos.y);
		IEntityArchetype *pArchetype = gEnv->pEntitySystem->LoadEntityArchetype("SystemSpawnList.Balt");
		if(pArchetype)
		{
			SEntitySpawnParams params;
			params.vPosition=SpawnPos;
			params.pClass=pArchetype->GetClass();
			params.sName=pArchetype->GetName();
			params.pPropertiesTable=pArchetype->GetProperties();
			params.pPropertiesTable=pArchetype->GetProperties();
			IEntity *SpawnAI=gEnv->pEntitySystem->SpawnEntity(params);
			IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Code_System.SpawnAI");
			if(pEffect)
				pEffect->Spawn(true,IParticleEffect::ParticleLoc(SpawnPos,Vec3(0,0,1)));

			IEntityRenderProxy *proxy=static_cast<IEntityRenderProxy*>(SpawnAI->GetProxy(ENTITY_PROXY_RENDER));
			if(proxy)
			{
				proxy->SetOpacity(0.6);
				/*IMaterial *pDefaultMaterial = proxy->GetRenderMaterial(-1);
				IMaterial *newMaterial = gEnv->p3DEngine->GetMaterialManager()->CloneMaterial(pDefaultMaterial,0);
				if(newMaterial)
				{
					int num = newMaterial->GetSubMtlCount();
					for(int i = 0; i < num; ++i)
					{
						IMaterial *pMaterial = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("delete");
						if(pMaterial)
						{
							IMaterial *pMaterial = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("delete");
							IMaterial *ClonedMaterial =gEnv->p3DEngine->GetMaterialManager()->CloneMaterial(pMaterial,0);
							newMaterial->SetSubMtl(i,ClonedMaterial);
							proxy->SetSlotMaterial(-1,newMaterial);
							//SpawnAI->SetMaterial(newMaterial);
						}
					}
				}*/
			}
			Balts.insert(SpawnAI->GetId());
			g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetAI()->SetFactionID(EnemyFactionId);
			ICharacterInstance *pInstance = SpawnAI->GetCharacter(0);
			IAttachmentManager *pAttachmentManager = pInstance->GetIAttachmentManager();
			if(pInstance)
			{
				IAttachmentManager *pAttachmentManager = pInstance->GetIAttachmentManager();
				int count = pAttachmentManager->GetAttachmentCount();
				for(int i=0;i<count;++i)
				{
					IAttachment *pAttachment = NULL;
					pAttachment = pAttachmentManager->GetInterfaceByIndex(i);
					if(pAttachment)
					{
						CEntityAttachment *pEntityAttachment = (CEntityAttachment *) pAttachment->GetIAttachmentObject();
						if(pEntityAttachment)
						{
							IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pEntityAttachment->GetEntityId());
							if(pEntity)
							{			
								IMaterial *pMaterial = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("delete");
								IMaterial *ClonedMaterial =gEnv->p3DEngine->GetMaterialManager()->CloneMaterial(pMaterial,0);
								pEntity->SetMaterial(ClonedMaterial);
							}
						}
					}
				}
			}
		}
	}
	else
	{
		std::set<EntityId>::iterator iter = Balts.begin();
		std::set<EntityId>::iterator end = Balts.end();
		while(iter != end)
		{
			Vec3 SpawnPos = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
			SpawnPos.x += BiRandom(1.5);
			SpawnPos.y +=BiRandom(1.5);
			IEntity *pEntity = gEnv->pEntitySystem->GetEntity(*iter);
			pEntity->SetPos(SpawnPos);
			IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Code_System.SpawnAI");
			if(pEffect)
				pEffect->Spawn(true,IParticleEffect::ParticleLoc(SpawnPos,Vec3(0,0,1)));	
			pEntity->Hide(false);
			++iter;
		}
	}
}

void BaltModule::HideBalt()
{
	std::set<EntityId>::iterator iter = Balts.begin();
	std::set<EntityId>::iterator end = Balts.end();
	while(iter != end)
	{
		IEntity *pEntity = gEnv->pEntitySystem->GetEntity(*iter);
		if(pEntity)
			pEntity->Hide(true);
		++iter;
	}
}