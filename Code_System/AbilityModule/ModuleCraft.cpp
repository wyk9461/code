#include "StdAfx.h"
#include "ModuleCraft.h"


ModuleCraft::ModuleCraft()
{

}


void ModuleCraft::Combine(AbilityComponent *parent , AbilityComponent *child)
{
	parent->AddComponent(child);
	AbilityComponent *temp_patent = parent;
	while (temp_patent->GetParent() != NULL)
	{
		temp_patent = temp_patent->GetParent();
	}
	child->SetParent(temp_patent);
}

void ModuleCraft::Remove(AbilityComponent *parent , AbilityComponent *child)
{
	parent->RemoveComponent(child);
	child->SetParent(NULL);
}