#ifndef _EXPLOSIONMODULE_H_
#define _EXPLOSIONMODULE_H_

#include "BaseAbility.h"


struct ExplosionCore
{
	Vec3 pos;
	float tick_timer;
	float last_time;
	ExplosionCore()
	:	pos(Vec3(0,0,0))
	,	tick_timer(0)
	,	last_time(5){}

	ExplosionCore &operator= (const ExplosionCore &rhs)
	{
		if(this == &rhs)
			return *this;
		pos = rhs.pos;
		tick_timer = rhs.tick_timer;
		last_time = rhs.last_time;
		return *this;
	}
};

class ExplosionModule : public BaseAbility
{
public:
	ExplosionModule();

	ExplosionModule(int _cost, float damage);


	~ExplosionModule(){}
	virtual const bool isLethal(){return true;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);
	void RegularUpdate(Process_Data &_rdata);


	void PreActive(Process_Data &_rdata){}
	void Active(Process_Data &_rdata){}
	void PostActive(Process_Data &_rdata);

	void Cool_Down(float frameTime);
	void ExplodeAll();
private:
	void SpawnIndicationParticle();
	void Explode(Vec3 &pos);
	std::list<ExplosionCore> ExplosionCoreList;
	float damage;
	IEntity *pEffect;
	EntityId EffectId;
};
#endif