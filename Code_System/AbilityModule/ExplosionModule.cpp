#include "StdAfx.h"
#include "ExplosionModule.h"

ExplosionModule::ExplosionModule()
	:	BaseAbility("Explosion",100,eMT_Explosion)
	,	damage(200)
{
	hit_type = MAGICHITTYPE + eMT_Explosion;
	SetTimer(2,0);
}

ExplosionModule::ExplosionModule(int _cost, float damage)
	:	BaseAbility("Explosion",_cost,eMT_Explosion)
	,	damage(damage)
{
	hit_type = MAGICHITTYPE + eMT_Explosion;
	SetTimer(2,0);
}

void ExplosionModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;
	/*default_View=g_pGame->GetIGameFramework()->GetIViewSystem()->GetActiveView();
	rush_View=g_pGame->GetIGameFramework()->GetIViewSystem()->CreateView();*/

	//two-level search
	pEffect = gEnv->pEntitySystem->GetEntity(EffectId);
	if(!pEffect)
	{
		pEffect = gEnv->pEntitySystem->FindEntityByName("ExplosionModuleEffect");
		if(!pEffect)
		{
			SpawnIndicationParticle();
		}
	}

	pEffect->Hide(false);
	HasPreProcessed=true;
	BaseAbility::PreProcess(_rdata);
}

void ExplosionModule::Process(Process_Data &_rdata)
{
	if(HasPreProcessed)
	{
		BaseAbility::PostProcess(_rdata);
		if(pEffect)
		{
			pEffect->SetPos(_rdata.hit.pt);
		}
	}

}

void ExplosionModule::PostProcess(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	finished = true;
	HasPreProcessed=false;

	pEffect->Hide(true);
	ExplosionCore core;
	core.pos = _rdata.hit.pt;

	ExplosionCoreList.push_back(core);
	IParticleEffect *pCoreEffect = gEnv->pParticleManager->FindEffect("Projectile_Effect.Phase");
	pCoreEffect->Spawn(true,IParticleEffect::ParticleLoc(core.pos));
	g_pGame->GetWorldStability()->StabilityCost(cost);
	_rdata.Finish_Pos = core.pos;
	BaseAbility::SendMessage(eMT_Explosion, eET_Set, _rdata);
}

void ExplosionModule::RegularUpdate(Process_Data &_rdata)
{
	if(ExplosionCoreList.empty())
		return;
	std::list<ExplosionCore>::iterator iter = ExplosionCoreList.begin();
	std::list<ExplosionCore>::iterator end = ExplosionCoreList.end();

	while(iter != end)
	{
		iter->tick_timer += _rdata.frameTime;
		if(iter->tick_timer >= iter->last_time)
		{
			Explode(iter->pos);
			_rdata.Finish_Pos = iter->pos;
			BaseAbility::SendMessage(eMT_Explosion, eET_Finished, _rdata);
			ExplosionCoreList.erase(iter++);
		}
		else
			++iter;
	}
}

void ExplosionModule::PostActive(Process_Data &_rdata)
{
	Explode(_rdata.Finish_Pos);
	BaseAbility::PostActive(_rdata);
}

void ExplosionModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}

void ExplosionModule::SpawnIndicationParticle()
{
	SEntitySpawnParams params;
	params.sName="ExplosionModuleEffect";
	params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
	params.vScale=Vec3(1,1,1);
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	pEffect=gEnv->pEntitySystem->SpawnEntity(params);
	SmartScriptTable proot=pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.Teleport");
	Script::CallMethod(proot,"OnReset");

	EffectId = pEffect->GetId();
}

void ExplosionModule::Explode(Vec3 &pos)
{
	ExplosionInfo einfo(g_pGame->GetClientActorId(), 0, 0, damage , pos, Vec3(0,0,0),3.0f, 5.0f,  0.1,  5.0f , 0 , 1000.0f , 0.0f , hit_type);
	einfo.SetEffect("explosions.Grenade_SCAR.character",1.0f,0.2f);  //Need to change
	einfo.type = g_pGame->GetGameRules()->GetHitTypeId( "Explosion" );
	SExplosionContainer SExplosion;
	SExplosion.m_explosionInfo = einfo;
	g_pGame->GetGameRules()->ClientExplosion(SExplosion);
}

void ExplosionModule::ExplodeAll()
{
	if(ExplosionCoreList.empty())
		return;
	std::list<ExplosionCore>::iterator iter = ExplosionCoreList.begin();
	std::list<ExplosionCore>::iterator end = ExplosionCoreList.end();

	while(iter != end)
	{
		Explode(iter->pos);
		++iter;
	}
	ExplosionCoreList.clear();
}