#ifndef _DATABITEMODULE_H_
#define _DATABITEMODULE_H_

#include "BaseAbility.h"

class DataBiteModule : public BaseAbility
{
private:
	IEntity *pEffect;
	CActor *pTarget;
	float heal_rate;
	float heal_log;
	float maxhealth;
	bool hasLaunched;
	bool hasSet;

public:
	DataBiteModule();

	DataBiteModule(int _cost, float _rate);
	~DataBiteModule(){}

	virtual const bool isLethal(){return true;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);
	void RegularUpdate(Process_Data &_rdata){}

	void Cool_Down(float frameTime);

	void DataExplode();

	inline const bool isSet(){return hasSet;}
	inline const bool isLaunched(){return hasLaunched;}
	inline const CActor* getTarget(){return pTarget;}

	inline void setHasHit(){hasSet=true;}
	inline void setHitTarget(CActor *target){pTarget=target;}
};
#endif