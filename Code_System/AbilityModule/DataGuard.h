#ifndef _DATAGUARD_H_
#define _DATAGUARD_H_

#include "BaseAbility.h"

class DataGuardModule : public BaseAbility
{
public:
	DataGuardModule();

	DataGuardModule(int _cost, int _max_guard, float _edamage);

	~DataGuardModule(){}
	virtual const bool isLethal(){return true;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);

	void PreActive(Process_Data &_rdata);
	void Active(Process_Data &_rdata);
	void PostActive(Process_Data &_rdata);

	void Cool_Down(float frameTime);

	void RegularUpdate(Process_Data &_rdata);

private:
	struct Guard
	{
		IEntity *pEffect;
		EntityId EffectId;
		float angle;
		float z_offset;
		bool available;
		Vec3 targetPos;

		Guard()
			:	pEffect(0)
			,	EffectId(0)
			,	angle(0)
			,	z_offset(0)
			,	available(false)
			,	targetPos(Vec3(0,0,0)){}

		Guard &operator=(const Guard &rhs)
		{
			if(this == &rhs)
				return *this;
			pEffect = rhs.pEffect;
			EffectId = rhs.EffectId;
			angle = rhs.angle;
			z_offset = rhs.z_offset;
			available = rhs.available;
			targetPos = rhs.targetPos;
			return *this;
		}
	};

	std::vector<Guard> the_guards;

	void SpawnIndicateParticle(int index);
	const bool AllVanished();
	void ReleaseData(Process_Data &_rdata);
	bool CanReleaseData;
	unsigned int data_count;

	int maxDataGuard;
	float Explosiondamage;

};
#endif