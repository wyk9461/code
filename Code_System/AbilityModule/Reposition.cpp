#include "StdAfx.h"
#include "Reposition.h"


RepositionModule::RepositionModule()
	:	BaseAbility("Reposition",100,eMT_Reposition)
	,	pEffect(0)
{
	g_pGame->GetBasicRayCast()->Save(init_backup);
	g_pGame->GetBasicRayCast()->Save(Reposition_backup);
	Reposition_backup.query_flag = ent_living | ent_terrain;
	SetTimer(4,0);
}

RepositionModule::RepositionModule(int _cost)
	:	BaseAbility("Reposition",_cost,eMT_Reposition)
	,	pEffect(0)
{
	g_pGame->GetBasicRayCast()->Save(init_backup);
	g_pGame->GetBasicRayCast()->Save(Reposition_backup);
	Reposition_backup.query_flag = ent_living | ent_terrain;
	SetTimer(4,0);
}

void RepositionModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	pTarget = NULL;
	pEffect = gEnv->pEntitySystem->GetEntity(EffectId);
	if(!pEffect)
	{
		pEffect = gEnv->pEntitySystem->FindEntityByName("RPP");
		if(!pEffect)
		{
			SpawnIndicateParticle();
		}
	}
	else
		pEffect->Hide(false);
	HasPreProcessed = true;
	BaseAbility::Active(_rdata);
	g_pGame->GetBasicRayCast()->Load(Reposition_backup);
	BaseAbility::SendMessage(eMT_Reposition, eET_ChangeProperty, _rdata);
	BaseAbility::SendMessage(eMT_Common, eET_ChangeProperty, _rdata);
	BaseAbility::PreProcess(_rdata);
}

void RepositionModule::Process(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	BaseAbility::PostProcess(_rdata);
	Vec3 &Position = _rdata.hit.pt;
	EntityId nearestId = CommonUsage::GetNerestEntityIdInBox(Position,4,ent_living);
	/*int num=gEnv->pPhysicalWorld->GetEntitiesInBox(Position-Vec3(4), Position+Vec3(4), nearbyEntities, ent_living);
	float Min_Distance = 99999;
	EntityId nearestId = 0;
	AABB aabb;
	for(int i = 0; i < num; ++i)
	{
		IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
		float the_length = (pEntity->GetPos() - Position).GetLengthSquared();
		if(the_length < Min_Distance)
		{
			Min_Distance = the_length;
			nearestId = pEntity->GetId();
			pEntity->GetWorldBounds(aabb);
		}
	}*/
	if(nearestId != 0 && nearestId != g_pGame->GetClientActorId())
	{
		AABB aabb;
		pTarget = gEnv->pEntitySystem->GetEntity(nearestId);
		pTarget->GetWorldBounds(aabb);
		if(pEffect)
		{
			/*static float angle = 0;
			static float z_off = 0;
			Vec3 new_pos = CommonUsage::MoveAroundPoint(pEffect,aabb.GetCenter(),0.5,angle,z_off);
			angle +=3;
			if(z_off < 180)
				z_off +=3;
			else
				z_off = 0;*/
			pEffect->SetPos(aabb.GetCenter());
		}
	}
	BaseAbility::SendMessage(eMT_Reposition, eET_Charge, _rdata);
}


void RepositionModule::PostProcess(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;

	if(pTarget)
	{
		ChangePosition(pTarget);
		BaseAbility::SendMessage(eMT_Reposition, eET_Finished, _rdata);
	}
	if(pEffect)
		pEffect->Hide(true);
	g_pGame->GetBasicRayCast()->Load(init_backup);
	pTarget = 0;
}

void RepositionModule::PreActive(Process_Data &_rdata){}
void RepositionModule::Active(Process_Data &_rdata){}
void RepositionModule::PostActive(Process_Data &_rdata){}

void RepositionModule::Cool_Down(float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}

void RepositionModule::SpawnIndicateParticle()
{
	SEntitySpawnParams params;
	params.sName = "RPP";

	params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
	params.vScale=Vec3(1,1,1);
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	pEffect=gEnv->pEntitySystem->SpawnEntity(params);

	SmartScriptTable proot=pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.Teleport");
	Script::CallMethod(proot,"OnReset");

	EffectId = pEffect->GetId();
}

void RepositionModule::ReceiveMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata)
{
	if(_mtype == eMT_Teleport && _etype == eET_Finished)
	{
		EntityId nearestId = CommonUsage::GetNerestEntityIdInBox(_rdata.Finish_Pos,4,ent_living);
		if(nearestId)
		{
			IEntity *pTarget = gEnv->pEntitySystem->GetEntity(nearestId);
			if(pTarget)
			{
				ChangePosition(pTarget);
			}
		}
	}

	if(_mtype == eMT_Explosion && _etype == eET_Finished)
	{
		CActor *pPlayerActor = (CActor*)g_pGame->GetIGameFramework()->GetClientActor();
		IEntity *pPlayer = pPlayerActor->GetEntity();
		Vec3 dir = _rdata.Finish_Pos - pPlayer->GetPos();
		dir.normalize();
		Quat rot = Quat::CreateRotationVDir(dir);
		pPlayer->SetPos(_rdata.Finish_Pos);
		pPlayerActor->SetViewRotation(rot);
	}
}


void RepositionModule::ChangePosition(IEntity *pTarget)
{
	if(pTarget)
	{
		Vec3 Target_Pos = pTarget->GetPos();
		Quat Target_Rotation = pTarget->GetRotation();
		IEntity *pPlayer = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity();
		Vec3 PlayerPos = pPlayer->GetPos();
		Quat PlayerRotation = pPlayer->GetRotation();
		pTarget->SetPos(PlayerPos);
		pTarget->SetRotation(PlayerRotation);
		pPlayer->SetPos(Target_Pos);
		CActor *pPlayerActor = (CActor*)g_pGame->GetIGameFramework()->GetClientActor();
		pPlayerActor->SetViewRotation(Target_Rotation);
		CActor *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId()));

		if(pActor && !pActor->IsDead())
		{
			pActor->Fall(PlayerPos);
			HitInfo hits(pPlayer->GetId(),pTarget->GetId(), 0, 100, 0, 0, -1, 1);
			g_pGame->GetGameRules()->ClientHit(hits);
			finished = true;
			HasPreProcessed=false;
		}
	}
}