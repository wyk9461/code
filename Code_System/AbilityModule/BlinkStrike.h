#ifndef _BLINKSTRIKE_H_
#define _BLINKSTRIKE_H_

#include "BaseAbility.h"

class BlinkStrikeModule : public BaseAbility
{
public:
	BlinkStrikeModule();

	BlinkStrikeModule(int _cost, float _multi);

	~BlinkStrikeModule(){}
	virtual const bool isLethal(){return false;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);

	void PreActive(Process_Data &_rdata);
	void Active(Process_Data &_rdata);
	void PostActive(Process_Data &_rdata);

	void Cool_Down(float frameTime);

	void RegularUpdate(Process_Data &_rdata){};
private:
	void SpawnIndicateParticle();
	float damage_multi;
	IEntity *pEffect;
	EntityId EffectId;
	IEntity *pTarget;

	RayCastBackup init_backup;
	RayCastBackup BlinkStrike_backup;
};
#endif