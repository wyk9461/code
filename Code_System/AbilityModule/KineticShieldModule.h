#ifndef _KINETICSHIELDMODULE_H_
#define _KINETICSHIELDMODULE_H_

#include "BaseAbility.h"
#include "IParticles.h"


class KineticShieldModule : public BaseAbility
{
private:
	IParticleEffect *pEffect;
	float maxhealth;
	float health;
	bool hasSet;
public:
	KineticShieldModule();

	KineticShieldModule(int _cost, float _health);

	~KineticShieldModule(){}
	virtual const bool isLethal(){return false;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);
	void RegularUpdate(Process_Data &_rdata){}

	void Cool_Down(float frameTime);

	void ShieldDamage(const HitInfo &hit);
	void BreakShield();

	inline const bool isSet(){return hasSet;}
};
#endif