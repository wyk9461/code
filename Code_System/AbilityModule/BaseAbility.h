#ifndef _BASEABILITY_H_
#define _BASEABILITY_H_

#include "Code_System/AbilityModule.h"
#include "ItemSystem.h"
#include "Code_System/PlayerInventory.h"
#include "Code_System/CommonUsage.h"
#include "IFlashUI.h"

#define MAGICHITTYPE 57525449

enum ModuleType
{
	eMT_Teleport,
	eMT_ShockWave,
	eMT_BulletTime,
	eMT_Shield,
	eMT_Reflection,
	eMT_ExploShield,
	eMT_DataBall,
	eMT_Heal,
	eMT_Backup,
	eMT_Rush,
	eMT_Cloak,
	eMT_DeepCloak,
	eMT_NuclearRad,
	eMT_Flood,
	eMT_DataBite,
	eMT_Grab,
	eMT_Explosion,
	eMT_GravityPool,
	eMT_Reposition,
	eMT_DataVoid,
	eMT_DataGuard,
	eMT_Balt,
	eMT_BulletStrom,
	eMT_BlinkStrike,
	eMT_DataArrow,
	eMT_Common,
};

enum ExcuteType
{
	eET_ChangeProperty,
	eET_Charge,		//Process Step
	eET_Moving,		//Projectile or player
	eET_Set,
	eET_Finished,
	eET_SubModule,
};

struct Timer_Cd
{
	float Last_Time;
	float timer;
	float Cdtimer;
	float Cd;
	const bool operator<(const Timer_Cd &tc) {return timer<tc.timer;}
	Timer_Cd &operator=(const Timer_Cd &tc);
	Timer_Cd()
		:	Last_Time(0)
		,	timer(0)
		,	Cdtimer(0)
		,	Cd(0){}
};

struct Process_Data
{
	float frameTime;
	ray_hit hit;

	IEntity *pActive_Entity;
	Vec3 Start_Pos;
	Vec3 Finish_Pos;
	float fValue;			//For data like length and damage

	Process_Data()
		:	frameTime(0)
		,	pActive_Entity(NULL)
		,	Start_Pos(Vec3(0,0,0))
		,	Finish_Pos(Vec3(0,0,0)){}
	Process_Data(const Process_Data &rhs)
	{
		frameTime = rhs.frameTime;
		hit = rhs.hit;
		pActive_Entity = rhs.pActive_Entity;
		Start_Pos = rhs.Start_Pos;
		Finish_Pos = rhs.Finish_Pos;
		fValue = rhs.fValue;
	}
};

Timer_Cd& Timer_Cd::operator=(const Timer_Cd &tc)
{
	if(this==&tc)
		return *this;
	Last_Time=tc.Last_Time;
	timer=tc.timer;
	Cdtimer=tc.Cdtimer;
	Cd=tc.Cd;
	return *this;
}

class AbilityComponent
{
public:
	AbilityComponent(){}
	AbilityComponent(const string &str)
		:	m_name(str)
		,	ParentComponent(NULL){}
	virtual ~AbilityComponent(){};

	string &GetName(){return m_name;}
	const unsigned int GetComponentId() const{return Id;}
	void SetComponentId(unsigned int _Id){Id = _Id;}

	virtual const bool isLethal(){return false;}
	virtual void PreActive(Process_Data &_rdata){};
	virtual void Active(Process_Data &_rdata){};
	virtual void PostActive(Process_Data &_rdata){};

	virtual void ReceiveMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata){};

	virtual bool AddComponent(AbilityComponent *new_module){return false;}
	virtual bool RemoveComponent(AbilityComponent *old_module){return false;}

	void SetParent(AbilityComponent *parent) {ParentComponent = parent;}
	AbilityComponent *GetParent(){return ParentComponent;}
private:
	string m_name;
	unsigned int Id;
	AbilityComponent *ParentComponent;
};


class BaseAbility : public AbilityComponent
{
public:
	BaseAbility(const string &_str , const int co, unsigned int _type)
		:	AbilityComponent(_str)
		,	Type(_type)
		,	cost(co)
		,	finished(false)
		,	HasPreProcessed(false)
		,	Timer()
		,	Complexity(3)
		,	Child_Id_Pool(10){}

	virtual ~BaseAbility(){m_Component.clear();}


	virtual void Cool_Down(float frameTime){};

	inline const bool isFinished(){return finished;}
	inline const bool isPreProcessed(){return HasPreProcessed;}
	virtual const bool Lethal();

	inline Timer_Cd &GetTimer(){return Timer;}

	void StoreInventory();
	void RestoreInventory();

	virtual void PreProcess(Process_Data &_rdata);
	virtual void Process(Process_Data &_rdata){}
	virtual void PostProcess(Process_Data &_rdata);
	virtual void RegularUpdate(Process_Data &_rdata){}

	virtual void PreActive(Process_Data &_rdata);
	virtual void Active(Process_Data &_rdata);
	virtual void PostActive(Process_Data &_rdata);

	virtual void SendMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata);

	virtual bool AddComponent(AbilityComponent *new_module);
	virtual bool RemoveComponent(AbilityComponent *old_module);

	const unsigned int &GetType(){return Type;}

	const unsigned int GetHitType(){return hit_type;}

	void SetTimer(float _cd, float _lasttime)
	{
		Timer.Cd = _cd;
		Timer.Last_Time = _lasttime;
	}

protected:
	Timer_Cd Timer;
	unsigned int Type;
	int cost;						// The stability cost.
	bool finished;				//	if this module has finished its job.
	bool HasPreProcessed;			
	int Complexity;
	unsigned int hit_type;
	std::list<AbilityComponent*> m_Component;
	IdPool Child_Id_Pool;
	const int GetCost(){return cost;}
	const int GetStaRemain();
	static IUIElement *pElement;
};

class AbilityEnhancement : public AbilityComponent , public BaseInventory
{
public:
	AbilityEnhancement(){}
	virtual ~AbilityEnhancement(){};

	AbilityEnhancement(float _cooldown, float _damage, float _lasttime, float _cost, float _radius, char *name, int _amount, unsigned int _Id);
private:
	float Cool_Down_Bonus;
	float Damage_Bonus;
	float Last_Time_Bonus;
	float Cost_Bonus;
	float Radius_Bonus; 
};

class ParticleEnhancement : public AbilityComponent , public BaseInventory
{
public:
	ParticleEnhancement(const char* _name)
		:	ParticleName(_name)
		,	AbilityComponent(_name)
		,	BaseInventory(const_cast<char*>(_name),1,eIT_ParticleEnhance){}
	virtual ~ParticleEnhancement(){};
private:
	const char* ParticleName;
};
#endif