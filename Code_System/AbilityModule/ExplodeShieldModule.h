#ifndef _EXPLODESHIELDMODULE_H_
#define _EXPLODESHIELDMODULE_H_

#include "BaseAbility.h"
#include "IParticles.h"

class ExplodeShieldModule : public BaseAbility
{
private:
	IParticleEffect *pEffect;
	float damage;
	float multiplier;
	float curhealth;
	float maxhealth;
	bool hasSet;
	bool is_not_explode;

public:
	ExplodeShieldModule();

	ExplodeShieldModule(int _cost, float _multi);

	virtual const bool isLethal(){return true;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);
	void RegularUpdate(Process_Data &_rdata){}

	void Cool_Down(float frameTime);

	void ExploShieldDamage(const HitInfo &hit);

	inline const bool isSet(){return hasSet;}

};

#endif