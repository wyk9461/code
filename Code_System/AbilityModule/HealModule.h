#ifndef _HEALMODULE_H_
#define _HEALMODULE_H_

#include "BaseAbility.h"
#include "IParticles.h"

class HealModule : public BaseAbility
{
private:
	IParticleEffect *pEffect;
	float heal_amount;

public:
	HealModule();

	HealModule(int _cost, float _amount);
	~HealModule(){}
	virtual const bool isLethal(){return false;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);
	void RegularUpdate(Process_Data &_rdata){}

	void Cool_Down(float frameTime);

	void PreActive(Process_Data &_rdata){};
	void Active(Process_Data &_rdata){};
	void PostActive(Process_Data &_rdata);

	void ReceiveMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata);
};
#endif