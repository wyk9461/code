#include "StdAfx.h"
#include "HealModule.h"

HealModule::HealModule()
	:	BaseAbility("Heal",100,eMT_Heal)
	,	pEffect(NULL)
	,	heal_amount(300)
{
	SetTimer(10,0);
}

HealModule::HealModule(int _cost, float _amount)
	:	BaseAbility("Heal",_cost,eMT_Heal)
	,	pEffect(NULL)
	,	heal_amount(_amount)
{
	SetTimer(10,0);
}
void HealModule::PreProcess(Process_Data &_rdata)
{
	if(HasPreProcessed || finished)
		return;

	Timer.Cdtimer=0;
	HasPreProcessed=true;
	pEffect=gEnv->pParticleManager->FindEffect("Code_System.Module_Effect.DataBall_Explosion");
	BaseAbility::PreActive(_rdata);
	BaseAbility::PreProcess(_rdata);
}

void HealModule::Process(Process_Data &_rdata)
{
	BaseAbility::Active(_rdata);
}

void HealModule::PostProcess(Process_Data &_rdata)
{
	if(!HasPreProcessed)
		return;
	BaseAbility::PostProcess(_rdata);
	finished=true;
	HasPreProcessed=false;
	g_pGame->GetWorldStability()->StabilityCost(cost);
	CActor *player=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	player->SetHealth(player->GetHealth()+heal_amount);
	BaseAbility::PostActive(_rdata);
}

void HealModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}

void HealModule::PostActive(Process_Data &_rdata)
{
	PostProcess(_rdata);
	g_pGame->GetWorldStability()->StabilityCost(cost);
	CActor *player=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	player->SetHealth(player->GetHealth()+heal_amount);
	BaseAbility::PostActive(_rdata);
}

void HealModule::ReceiveMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata)
{
	if(_mtype == eMT_Teleport && _etype == eET_Finished)
	{
		g_pGame->GetWorldStability()->StabilityCost(cost);
		CActor *player=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
		player->SetHealth(player->GetHealth()+heal_amount);
	}
}