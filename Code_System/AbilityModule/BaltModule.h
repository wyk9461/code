#ifndef _BALTMODULE_H_
#define _BALTMODULE_H_

#include "BaseAbility.h"


class BaltModule : public BaseAbility
{
public:
	BaltModule();

	BaltModule(int _cost);

	~BaltModule(){}
	virtual const bool isLethal(){return false;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);

	void PreActive(Process_Data &_rdata);
	void Active(Process_Data &_rdata);
	void PostActive(Process_Data &_rdata);

	void Cool_Down(float frameTime);

	void RegularUpdate(Process_Data &_rdata);
private:
	std::set<EntityId> Balts;
	bool hasSet;
	int PlayerFactionId;
	int EnemyFactionId;
	void SpawnBalt();
	void HideBalt();
};
#endif