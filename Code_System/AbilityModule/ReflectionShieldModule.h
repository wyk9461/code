#ifndef _REFLECTIONSHIELDMODULE_H_
#define _REFLECTIONSHIELDMODULE_H_

#include "BaseAbility.h"
#include "IParticles.h"


class ReflectionShieldModule : public BaseAbility
{
private:
	IParticleEffect *pEffect;
	float multiplier;
	bool hasSet;
public:
	ReflectionShieldModule();

	ReflectionShieldModule(int _cost, float _multi);

	~ReflectionShieldModule(){}
	virtual const bool isLethal(){return true;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);
	void RegularUpdate(Process_Data &_rdata){}

	void Cool_Down(float frameTime);

	void ReflectionDamage(const HitInfo &hit);

	inline const bool isSet(){return hasSet;}
};
#endif