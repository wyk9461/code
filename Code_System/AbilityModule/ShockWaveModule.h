#ifndef _SHOCKWAVEMODULE_H_
#define _SHOCKWAVEMODULE_H_

#include "BaseAbility.h"

class ShockWaveModule : public BaseAbility
{
private:
	float radius;
	float pressure;
	IEntity *pEffect;
	IPhysicalEntity **nearbyEntities;

	void SpawnIndicateParticle();
public:
	ShockWaveModule();

	ShockWaveModule(int _cost, float _radius, float _pressure);

	~ShockWaveModule(){};
	virtual const bool isLethal(){return false;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);
	void RegularUpdate(Process_Data &_rdata){}

	void Cool_Down(float frameTime);

	void PreActive(Process_Data &_rdata){};
	void Active(Process_Data &_rdata){};
	void PostActive(Process_Data &_rdata);

	void ReceiveMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata);
	void Combined_ShockWave(Vec3 &pos);

	void SetRadius(float _radius){radius = _radius;}
	const float &GetRadius(){return radius;}
};
#endif