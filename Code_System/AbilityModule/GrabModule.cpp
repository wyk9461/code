#include "StdAfx.h"
#include "GrabModule.h"


GrabModule::GrabModule()
	:	BaseAbility("Grab",100,eMT_Grab)
	,	pEntity(NULL)
	,	pEffect(NULL)
	,	rate(0)
	,	damage(0)
	,	finalpos(0,0,0)
	,	isGrab(false)
	,	hasSet(false)
	,	Detected(false)
{
	SetTimer(3,5);
}

GrabModule::GrabModule(int _cost)
	:	BaseAbility("Grab",_cost,eMT_Grab)
	,	pEntity(NULL)
	,	pEffect(NULL)
	,	rate(0)
	,	damage(0)
	,	finalpos(0,0,0)
	,	isGrab(false)
	,	hasSet(false)
	,	Detected(false)
{
	SetTimer(3,5);
}

void GrabModule::PreProcess(Process_Data &_rdata)
{
	if((HasPreProcessed || finished) && !isGrab)
		return;
	g_pGame->GetBasicRayCast()->ChangeFlag(ent_rigid | ent_sleeping_rigid);
	g_pGame->GetBasicRayCast()->ChangeDistance(15.0);
	SEntitySpawnParams ParticleParams;
	ParticleParams.vPosition=Vec3(0,0,0);
	ParticleParams.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	ParticleParams.sName="DragEffect";
	ParticleParams.vScale=Vec3(1,1,1);
	pEffect=gEnv->pEntitySystem->SpawnEntity(ParticleParams);
	SmartScriptTable root=pEffect->GetScriptTable();
	SmartScriptTable props;
	root->GetValue("Properties",props);
	props->SetValue("ParticleEffect","Code_System.Module_Effect.Grab_Charge");
	Script::CallMethod(root,"OnReset");
	if(!isGrab)
	{
		Timer.timer=0;
		HasPreProcessed=true;
		BaseAbility::PreProcess(_rdata);
	}
	else
	{
		finished=true;
		Throw(_rdata.hit);
	}
}

void GrabModule::Process(Process_Data &_rdata)
{
	if(!HasPreProcessed || isGrab || Detected)
		return;
	if(!isGrab && !finished)
	{
		if(_rdata.hit.pCollider)
			pEntity=(IEntity*)_rdata.hit.pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);
	}
}

void GrabModule::PostProcess(Process_Data &_rdata)
{
	if(Detected || !HasPreProcessed)
		return;
	BaseAbility::PostProcess(_rdata);
	if(pEntity && !finished)
	{
		AABB aabb;
		pEntity->GetWorldBounds(aabb);
		if(!isGrab)
		{
			g_pGame->GetBasicRayCast()->ChangeDistance(2.0);
			Vec3 eyepos=_rdata.hit.pt;
			Vec3 curPos=aabb.GetCenter();
			Vec3 dir=eyepos-curPos;
			float length=dir.GetLength();
			dir.normalize();
			Vec3 nextPos=curPos+dir;
			pEntity->SetPos(nextPos);
			if((curPos-_rdata.hit.pt).GetLength()<1)
				isGrab=true;
		}
		else
		{
			pEntity->AttachChild(pEffect);
			pEffect->Hide(false);
			AABB localaabb;
			pEntity->GetLocalBounds(localaabb);
			Vec3 dir=gEnv->pRenderer->GetCamera().GetViewdir();
			Vec3 center=aabb.GetCenter();
			Vec3 centerdir=pEntity->GetPos()-center;
			centerdir.normalize();
			float offset=(pEntity->GetPos()-center).GetLength();
			finalpos=_rdata.hit.pt+centerdir*offset;
			pEntity->SetPos(finalpos+dir*aabb.GetRadius());

			Quat curQuat=pEntity->GetRotation();
			Quat nextQuat=curQuat.CreateRotationZ(DEG2RAD(3.0f));
			curQuat*=nextQuat;
			pEntity->SetRotation(curQuat);	

			if(Timer.timer<Timer.Last_Time)
			{
				Timer.timer+=_rdata.frameTime;
				damage=Timer.timer*200;
			}
		}
	}
}

void GrabModule::Throw(ray_hit &hit)
{
	IEntityPhysicalProxy *pp=(IEntityPhysicalProxy*)pEntity->GetProxy(ENTITY_PROXY_PHYSICS);
	Vec3 dir=gEnv->pRenderer->GetCamera().GetViewdir();
	pp->AddImpulse(-1,hit.pt,dir*3000,true,1,10);
	//pEntity=NULL;
	HasPreProcessed=false;
	isGrab=false;
	Detected=true;
	Timer.timer=0;
	g_pGame->GetBasicRayCast()->Load();

	//No explosion in new edition
	pEntity->DetachAll();
	finished=true;
	Detected=false;
	pEntity=NULL;
	Timer.timer=0;
	pEffect=NULL;
}

void GrabModule::ThrowEntityExplode(float frameTime)
{
/*	Timer.timer+=frameTime;
	AABB aabb;
	pEntity->GetLocalBounds(aabb);
	IPhysicalEntity **nearbyEntities;
	int n=gEnv->pPhysicalWorld->GetEntitiesInBox(pEntity->GetPos()-Vec3(aabb.GetRadius()/2),pEntity->GetPos()+Vec3(aabb.GetRadius()/2),nearbyEntities,ent_living);
	for(int i=0;i<n;i++)
	{
		IEntity* tEntity=gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
		if(tEntity)
		{
			if(tEntity->GetId() == g_pGame->GetClientActorId())
			{
				--n;
				break;
			}
		}
	}
	if(n>=2 || Timer.timer>5.0f)
	{
		pEntity->DetachAll();
		finished=true;
		Detected=false;
		ExplosionInfo einfo(pEntity->GetId(), 0, 0, damage , pEntity->GetPos(), Vec3(0,0,0),0.1f, 5.0f,  0.1,  5.0f , 0 , 1000.0f , 0.0f , 0);
		einfo.SetEffect("Code_System.Module_Effect.Grab_Explode",1.0f,1.0f);  //Need to change
		einfo.type = g_pGame->GetGameRules()->GetHitTypeId( "Explosion" );
		g_pGame->GetGameRules()->QueueExplosion(einfo);
		gEnv->pEntitySystem->RemoveEntity(pEntity->GetId());
		pEntity=NULL;
		Timer.timer=0;
		gEnv->pEntitySystem->RemoveEntity(pEffect->GetId());
		pEffect=NULL;
	}*/
}

void GrabModule::Cool_Down(const float frameTime)
{
	Timer.Cdtimer+=frameTime;
	if(Timer.Cdtimer>=Timer.Cd)
	{
		finished=false;
		Timer.Cdtimer=0;
	}
}