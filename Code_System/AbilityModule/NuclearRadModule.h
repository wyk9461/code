#ifndef _NUCLEARRADMODULE_H_
#define _NUCLEARRADMODULE_H_

#include "BaseAbility.h"
#include "IEntity.h"


class NuclearRadModule : public BaseAbility
{
private:
	IEntity *pEffect;
	float damage;
	float speed_multi;
	Vec3 pos;
	bool hasSet;

public:
	NuclearRadModule();
	NuclearRadModule(int _cost, float _damage, float _multi);

	~NuclearRadModule(){}
	virtual const bool isLethal(){return true;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);
	void RegularUpdate(Process_Data &_rdata){}

	void Cool_Down(float frameTime);

	inline const bool isSet(){return hasSet;}
};
#endif