#ifndef _TELEPORT_H_
#define _TELEPORT_H_

#include "BaseAbility.h"
#include "IEntity.h"

class TeleportModule : public BaseAbility
{
private:
	Vec3 pos;
	IEntity	*pEffect;
	float blurAmount;
	float DefaultLength;
public:
	TeleportModule();

	TeleportModule(int _cost);

	~TeleportModule(){}
	virtual const bool isLethal(){return false;}
	void PreProcess(Process_Data &_rdata);
	void Process(Process_Data &_rdata);
	void PostProcess(Process_Data &_rdata);
	void RegularUpdate(Process_Data &_rdata){}

	void Cool_Down(float frameTime);

	void PreActive(Process_Data &_rdata);
	void Active(Process_Data &_rdata){};
	void PostActive(Process_Data &_rdata){};

	void ReceiveMessage(ModuleType _mtype, ExcuteType _etype, Process_Data &_rdata);

	//For Composite
	void ChangeTeleportLength(float length);
};

#endif