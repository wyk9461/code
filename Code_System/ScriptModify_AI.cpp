#include "StdAfx.h"
#include "Code_System/ScriptModify_AI.h"
#include "IAIActor.h"

// Description : 
//		Change the accuracy of an AI entity
//	Arguments :
//		pActor - a pointer to an IActor object
//		v_accuracy - the accuracy value
float ScriptModify_AI::ChangeAccuracy(IActor *pActor , const float v_accuracy)
{
	if(pActor->IsDead())		// if this AI is dead
		return 0;						// then do nothing
	IEntity *pEntity=pActor->GetEntity();		// Get a pointer to the AI entity

	float health=pActor->GetHealth();		// Get the current health of the AI
	float defAccuracy;
	SmartScriptTable root=pEntity->GetScriptTable();		// Get the lua table of this AI
	SmartScriptTable props;
	SmartScriptTable RoD;
	root->GetValue("Properties",props);		// Get the Properties table
	props->GetValue("RateOfDeath",RoD);		//	Get the RateOfdeath table
	RoD->GetValue("accuracy",defAccuracy);		
	RoD->SetValue("accuracy",v_accuracy);		//	Set the accuracy value
	Script::CallMethod(root,"OnReset");		// Apply change
	pActor->SetHealth(health);		// Reset the health value of this AI
	return defAccuracy;
}

// Description : 
//		Change the sight range and FOV(Field of View) of an AI entity
//	Arguments :
//		pActor - a pointer to an IActor object
//		v_FOV - the FOV value
//		v_range - the sight range value
int ScriptModify_AI::ChangeSightRange(IActor *pActor , const int v_range)
{
	if(pActor->IsDead())		// if this AI is dead
		return 0;						// then do nothing
	IEntity *pEntity=pActor->GetEntity();		// Get a pointer to the AI entity

	float health=pActor->GetHealth();		// Get the current health of the AI
	SmartScriptTable root=pEntity->GetScriptTable();		// Get the lua table of this AI
	SmartScriptTable props;
	SmartScriptTable perce;
	int defSightRange;
	root->GetValue("Properties",props);		// Get the Properties table
	props->GetValue("Perception",perce);		// Get the Perception table
	perce->GetValue("sightrange",defSightRange);		//	Set the sight range value
	perce->SetValue("sightrange",v_range);		//	Set the sight range value
	Script::CallMethod(root,"OnReset");		// Apply change
	pActor->SetHealth(health);		// Reset the health value of this AI
	return defSightRange;
}

int ScriptModify_AI::ChangeFaction(IActor *pActor , const char* v_faction)
{
	if(pActor->IsDead())
		return -1;
	IEntity *pEntity=pActor->GetEntity();

	IAIObject *pObject = pEntity->GetAI();
	uint8 prev_Id = pObject->GetFactionID();
	uint8 fId = gEnv->pAISystem->GetFactionMap().GetFactionID(v_faction);
	pObject->SetFactionID(fId);
	return prev_Id;
}

int ScriptModify_AI::ChangeFaction(IActor *pActor , int v_faction)
{
	if(pActor->IsDead())
		return -1;
	IEntity *pEntity=pActor->GetEntity();
  
	IAIObject *pObject = pEntity->GetAI();
	pObject->SetFactionID(v_faction);
	return 1;
}



float ScriptModify_AI::ChangeSpeed(IActor *pActor , const float v_speed)
{
	CActor *pCActor=(CActor*)pActor;
	float defSpeedMulti=pCActor->GetSpeedMultiplier(SActorParams::eSMR_Internal);
	pCActor->SetSpeedMultipler(SActorParams::eSMR_Internal,v_speed);
	pCActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,v_speed);
	pCActor->SetSpeedMultipler(SActorParams::eSMR_Item,v_speed);
	IAIActor *pAIActor = CastToIAIActorSafe(pActor->GetEntity()->GetAI());
	if(pAIActor)
		pAIActor->SetSpeed(v_speed);
	return defSpeedMulti;
}