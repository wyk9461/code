#ifndef _WEAPONUPGRADE_H_
#define _WEAPONUPGRADE_H_

#include "StdAfx.h"
#include "IEntity.h"

struct WeaponBonus
{
	float DamageBonus;
	float FireRateBonus;
	float ClipSizeBonus;

	WeaponBonus()
		:	DamageBonus(1.0)
		,	FireRateBonus(1.0)
		,	ClipSizeBonus(1.0){}

	WeaponBonus &operator=(const WeaponBonus &rhs)
	{
		if(this == &rhs)
			return *this;
		DamageBonus = rhs.DamageBonus;
		FireRateBonus = rhs.FireRateBonus;
		ClipSizeBonus = rhs.ClipSizeBonus;
		return *this;
	}
};

struct WeaponUpgradeState
{
	unsigned int level;
	unsigned int cur_exp;
	unsigned int next_exp;
	WeaponBonus bonus;

	WeaponUpgradeState()
	:	level(1)
	,	cur_exp(0)
	,	next_exp(1000){}

	WeaponUpgradeState(const WeaponUpgradeState &new_wus)
	{
		level = new_wus.level;
		cur_exp=new_wus.cur_exp;
		next_exp=new_wus.next_exp;
		bonus = new_wus.bonus;
	}

	WeaponUpgradeState &operator=(const WeaponUpgradeState &rhs)
	{
		if(this == &rhs)
			return *this;
		level = rhs.level;
		cur_exp = rhs.cur_exp;
		next_exp=rhs.next_exp;
		bonus = rhs.bonus;
		return *this;

	}
};

class WeaponUpgrade
{
private:
	XmlNodeRef Player_Profile;
	XmlNodeRef UpgradeSetting;
	std::map<IEntityClass*,WeaponUpgradeState> wusMap;
	IEntityClass *curItem;
	WeaponBonus tempBonus;
	void UpdateProfile(IEntityClass *weaponclass , WeaponUpgradeState &cur_state);
	const bool Update();
public:
	WeaponUpgrade();
	~WeaponUpgrade(){wusMap.clear();}
	void Init();
	const unsigned int GetWeaponLevel(IEntityClass* weaponclass);
	const unsigned int GetCurExp(IEntityClass* weaponclass);
	const unsigned int GetNextExp(IEntityClass *weaponclsass);
	const WeaponBonus *GetWeaponBonusByName(const string &weaponname);
	void SetCurItem(IEntityClass* weaponclass);
	void GainExp(int g_exp);
};
#endif