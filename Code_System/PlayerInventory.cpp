#include "StdAfx.h"
#include "PlayerInventory.h"

//------------------------------------------------------------------------------------------
BaseInventory::BaseInventory(char *_name, int _amount, InventoryType _type)
	:	InventoryName(_name)
	,	InventoryAmount(_amount)
	,	Type(_type)
	,	Id(0)
	,	maxAmount(1)
{
	if(Type == eIT_Enhancement)
		maxAmount = 999;
}

BaseInventory::BaseInventory(const BaseInventory &cps)
{
	InventoryName = cps.InventoryName;
	InventoryAmount = cps.InventoryAmount;
	maxAmount = cps.maxAmount;
	Type = cps.Type;
	Id = cps.Id;
}

BaseInventory &BaseInventory::operator=(const BaseInventory &rhs)
{
	if(this == &rhs)
		return *this;
	InventoryName = rhs.InventoryName;
	InventoryAmount = rhs.InventoryAmount;
	maxAmount = rhs.maxAmount;
	Type = rhs.Type;
	Id = rhs.Id;
	return *this;
}

//------------------------------------------------------------------------------------------
PlayerInventory::PlayerInventory()
{
	InventoryIdPool = IdPool(500);
	int a = 0;
}


const bool PlayerInventory::AddNewInventory(BaseInventory *new_inventory)
{
	if(InventoryIdPool.CheckIdUsable(new_inventory->GetId()))
	{
		if(InventoryIdPool.AssignSpecialId(new_inventory->GetId()))
		{
			Inventories.insert(std::map<InventoryId,BaseInventory*>::value_type(new_inventory->GetId(),new_inventory));
			return true;
		}
		else
		{
			int new_id = InventoryIdPool.AssignId();
			if(new_id)
			{
				Inventories.insert(std::map<InventoryId,BaseInventory*>::value_type(new_id,new_inventory));
				return true;
			}
		}
		return false;
	}
	else
	{
		ChangeInventory(new_inventory);
		return true;
	}
}

const bool PlayerInventory::RemoveInventory(BaseInventory *dump_inventory)
{
	if(Inventories.count(dump_inventory->GetId()))
	{
		Inventories.erase(dump_inventory->GetId());
		InventoryIdPool.RecoveryId(dump_inventory->GetId());
		SAFE_DELETE(dump_inventory);
		return true;
	}
	return false;
}

const bool PlayerInventory::SpliteInventory(InventoryId _Id, int num)
{
	if(Inventories.count(_Id))
	{
		BaseInventory &SplitedInventory = *Inventories[_Id];
		if(num < SplitedInventory.GetInventoryAmount())
		{
			BaseInventory *new_Inventory = new BaseInventory(SplitedInventory.GetInventoryName(),num,SplitedInventory.GetInventoryType());
			SplitedInventory.ChangeAmount(-num);
			int new_id = InventoryIdPool.AssignId();
			new_Inventory->SetId(new_id);
			Inventories.insert(std::map<InventoryId,BaseInventory*>::value_type(new_id,new_Inventory));
			return true;
		}
	}
	return false;
}


const bool PlayerInventory::CombineInventory(InventoryId _parent_id, InventoryId _child_id)
{
	if(Inventories.count(_parent_id) && Inventories.count(_child_id))
	{
		BaseInventory &parentInventory = *Inventories[_parent_id];
		BaseInventory &childInventory = *Inventories[_child_id];
		if(parentInventory.GetInventoryAmount() + childInventory.GetInventoryAmount() <= parentInventory.GetMaxAmount())
		{
			parentInventory.ChangeAmount(childInventory.GetInventoryAmount());
			Inventories.erase(_child_id);
			InventoryIdPool.RecoveryId(_child_id);
			return true;
		}
		else if(parentInventory.GetInventoryAmount() + childInventory.GetInventoryAmount() > parentInventory.GetMaxAmount())
		{
			int _amount = parentInventory.GetMaxAmount() - parentInventory.GetInventoryAmount();
			parentInventory.ChangeAmount(_amount);
			childInventory.ChangeAmount(-(_amount));
			return true;
		}
		return false;
	}
	return false;
}


const bool PlayerInventory::ChangeInventory(BaseInventory *new_inventory)
{
	BaseInventory &old_inventory = *Inventories[new_inventory->GetId()];
	if(old_inventory.GetInventoryAmount() + new_inventory->GetInventoryAmount() < old_inventory.GetMaxAmount())
	{
		Inventories[old_inventory.GetId()]->ChangeAmount(new_inventory->GetInventoryAmount());
		SAFE_DELETE(new_inventory);
		return true;
	}
	else if(old_inventory.GetInventoryAmount() + new_inventory->GetInventoryAmount() > old_inventory.GetMaxAmount())
	{
		new_inventory->ChangeAmount(old_inventory.GetInventoryAmount() - old_inventory.GetMaxAmount());
		old_inventory.SetAmount(old_inventory.GetMaxAmount());
		int new_id = InventoryIdPool.AssignId();
		new_inventory->SetId(new_id);
		Inventories.insert(std::map<InventoryId,BaseInventory*>::value_type(new_id,new_inventory));
		return true;
	}
	else
	{
		old_inventory.SetAmount(old_inventory.GetMaxAmount());
		SAFE_DELETE(new_inventory);
		return true;
	}
	return false;
}