#ifndef _SCREENEFFECTADAPTOR_H_
#define _SCREENEFFECTADAPTOR_H_

#include <map>

typedef unsigned int effectId;

enum Effect_Type_Id
{
	eETID_DeepMode_In = 1,
	eETID_DeepMode_In_Reverse = 2,
	eETID_EntityDataBase,
	eETID_SwitchToProbeCamera,
	eETID_SwitchToProbeCamera_Reverse,
	eEDIT_ProbeGrain,
};

struct effect_params
{
	char *effect_name;
	float end_value;
	float tick_timer;
	float last_time;
	float cur_value;
	float rate;
	float init_value;
	effectId reverseId;
	bool reverse;
	bool finished;
	bool shouldStart;
	effect_params()
		:	effect_name("")
		,	end_value(0)
		,	tick_timer(0)
		,	last_time(0)
		,	cur_value(0)
		,	rate(0)
		,	init_value(cur_value)
		,	reverse(false)
		,	reverseId(0)
		,	finished(false)
		,	shouldStart(true){}

	effect_params(char* _name, float _fvalue, float _cur, float _last, bool _bvalue = false)
		:	effect_name(_name)
		,	end_value(_fvalue)
		,	tick_timer(0)
		,	last_time(_last)
		,	cur_value(_cur)
		,	rate(0)
		,	init_value(cur_value)
		,	reverse(_bvalue)
		,	reverseId(0)
		,	finished(false)
		,	shouldStart(true){}

	effect_params &operator=(const effect_params &rhs)
	{
		if(this == &rhs)
			return *this;
		effect_name = rhs.effect_name;
		end_value = rhs.end_value;
		tick_timer = rhs.tick_timer;
		last_time = rhs.last_time;
		cur_value = rhs.cur_value;
		rate = rhs.rate;
		init_value = rhs.init_value;
		reverse = rhs.reverse;
		finished = rhs.finished;
		shouldStart = rhs.shouldStart;
		return *this;
	}
};

class ScreenEffectAdaptor
{
private:

	std::map<effectId,effect_params> params_queue;

public:
	ScreenEffectAdaptor();
	~ScreenEffectAdaptor(){params_queue.clear();}

	static ScreenEffectAdaptor &TheAdaptor(){static ScreenEffectAdaptor adaptor; return adaptor;}

	void AddToQueue(effect_params params,effectId Id);
	void Update(float frameTime);
};
#endif