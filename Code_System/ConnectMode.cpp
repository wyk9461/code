#include "StdAfx.h"
#include "Code_System/ConnectMode.h"

void ConnectMode::SetTimeScale(const short level)
{
	ICVar * timeScaleCVar = gEnv->pConsole->GetCVar("t_Scale");
	if(level == eConnectMode::eCLevel::eCLevel_None)
	{
		timeScaleCVar->Set(1.0f);    //May change to fit BulletTime Module

		//try to clear delete list when change to normal mode
		g_pGame->GetDeleteSystem()->ClearDeleteList(false);
	}
	if(mode == eConnectMode::eCMode::Deep)
	{
		if(level == eConnectMode::eCLevel::eCLevel_None || level == eConnectMode::eCLevel::eCLevel_ChooseSystem)
			timeScaleCVar->Set(1.0f);
		if(level == eConnectMode::eCLevel::eCLevel_MidMenu)
			timeScaleCVar->Set(0.1f);
		else if(level == eConnectMode::eCLevel::eCLevel_SpawnMenu)
			timeScaleCVar->Set(0.1f);
		else if(level == eConnectMode::eCLevel::eCLevel_EntityDataBase)
			timeScaleCVar->Set(0.1f);
		else if(level == eConnectMode::eCLevel::eCLevel_Modify_ChooseSlot || level == eConnectMode::eCLevel::eCLevel_Modify_ComplexModify)
			timeScaleCVar->Set(0.1f);
		else if(level == eConnectMode::eCLevel::eCLevel_Disable_ChooseSlot || level == eConnectMode::eCLevel::eCLevel_Disable_Menu)
			timeScaleCVar->Set(0.1f);
		else if(level == eConnectMode::eCLevel::eCLevel_Creating)
			timeScaleCVar->Set(0.1f);
	}
	/*if(mode == eConnectMode::eCMode::Normal)
	{

	}*/
}

//------------------------------------------------------------------------
const bool ConnectMode::PopLevel()
{
	if(mode == eConnectMode::eCMode::Deep)
	{
		//Global Level 1
		if(Level == eConnectMode::eCLevel::eCLevel_None || Level == eConnectMode::eCLevel::eCLevel_ChooseSystem)
			return false;
		//Global Level 1, MidMune Level 1
		else if(Level == eConnectMode::eCLevel::eCLevel_MidMenu)
		{
			Level = eConnectMode::eCLevel::eCLevel_None;
			SetTimeScale(eConnectMode::eCLevel::eCLevel_None);
			return true;
		}
		//Global Level 1, SpawnSystem Level 1
		else if(Level == eConnectMode::eCLevel::eCLevel_SpawnMenu)
		{
			Level = eConnectMode::eCLevel::eCLevel_None;
			SetTimeScale(eConnectMode::eCLevel::eCLevel_None);
			return true;
		}
		//Global Level 1, EntityDB
		else if(Level == eConnectMode::eCLevel::eCLevel_EntityDataBase)
		{
			Level = eConnectMode::eCLevel::eCLevel_None;
			SetTimeScale(eConnectMode::eCLevel::eCLevel_None);
			return true;
		}
		//Global Level 2, ModifySystem Level 1
		else if(Level == eConnectMode::eCLevel::eCLevel_Modify_ChooseSlot)
		{
			Level = eConnectMode::eCLevel::eCLevel_ChooseSystem;
			SetTimeScale(eConnectMode::eCLevel::eCLevel_ChooseSystem);
			return true;
		}
		//Global Level 2, ModifySystem Level 2
		else if(Level == eConnectMode::eCLevel::eCLevel_Modify_ComplexModify)
		{
			Level = eConnectMode::eCLevel::eCLevel_Modify_ChooseSlot;
			SetTimeScale(eConnectMode::eCLevel::eCLevel_Modify_ChooseSlot);
			return true;
		}
		//Currently no use
		else if(Level == eConnectMode::eCLevel::eCLevel_Disable_ChooseSlot)
		{
			Level = eConnectMode::eCLevel::eCLevel_ChooseSystem;
			SetTimeScale(eConnectMode::eCLevel::eCLevel_ChooseSystem);
			return true;
		}
		else if(Level == eConnectMode::eCLevel::eCLevel_Disable_Menu)
		{
			Level = eConnectMode::eCLevel::eCLevel_Disable_ChooseSlot;
			SetTimeScale(eConnectMode::eCLevel::eCLevel_Disable_ChooseSlot);
			return true;
		}
		else if(Level == eConnectMode::eCLevel::eCLevel_Creating)
		{
			Level = eConnectMode::eCLevel::eCLevel_None;
			SetTimeScale(eConnectMode::eCLevel::eCLevel_None);
			return true;
		}
	}
	if(mode == eConnectMode::eCMode::Normal)
	{
		if(Level == eConnectMode::eCLevel::eCLevel_Creating)
		{
			Level = eConnectMode::eCLevel::eCLevel_None;
			SetTimeScale(eConnectMode::eCLevel::eCLevel_None);
			return true;
		}
	}
	return false;
}


void ConnectMode::SetUIConnectMode(bool isDeepMode)
{
	if(!pElement)
		pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	if(pElement)
	{
		pElement->CallFunction("SwitchMode", SUIArguments::Create(isDeepMode));
	}
}