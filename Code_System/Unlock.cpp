#include "StdAfx.h"
#include "Unlock.h"
#include "EntityProjectile.h"


UnlockSystem::UnlockSystem()
	:	multiPossibility(0.5)
{
	UnlockList = gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/UnlockList.xml");
	ReadLockedSequences();
}

void UnlockSystem::ReadLockedSequences()
{
	int Max_Level_Count = UnlockList->getChildCount();
	for(int i=0;i < Max_Level_Count; ++i)
	{
		XmlNodeRef LevelList = UnlockList->getChild(i);
		int ListLength = LevelList->getChildCount();
		for(int j=0; j<ListLength; ++j)
		{
			XmlNodeRef ListItem = LevelList->getChild(j);
			bool UnlockStatus(true);
			ListItem->getAttr("Unlock", UnlockStatus);
			if(!UnlockStatus)
				LockedSequences[i].push_back(j);
		}
	}
}

XmlString UnlockSystem::Unlock(int level)
{
	if(!UnlockList)
		UnlockList = gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/UnlockList.xml");
	if(UnlockList)
	{
		XmlString UnlockName("");
		XmlNodeRef LevelList = UnlockList->getChild(level - 1);
		int ListLength = LevelList->getChildCount();

		int unlock_index(0);
		if(LockedSequences[level - 1].size() > 0)
			unlock_index = cry_rand() % LockedSequences[level - 1].size();
		else
			unlock_index = 0;

		int final_index = LockedSequences[level - 1][unlock_index];
		XmlNodeRef UnlockItem = LevelList->getChild(final_index);
		int UnlockType(0);
		UnlockItem->getAttr("Type", UnlockType);
		if(UnlockType == eUT_Entity)
			UnlockName = UnlockEntity(UnlockItem);
		else if(UnlockType == eUT_Projectile)
			UnlockName = UnlockProjectile(UnlockItem);

		UnlockItem->setAttr("Unlock", 1);

		float next_possibility = cry_frand();
		if(next_possibility < multiPossibility)
			Unlock(level);

		LockedItemSequence::iterator erase_iter = LockedSequences[level - 1].begin() + unlock_index;
		LockedSequences[level - 1].erase(erase_iter);

		UnlockList->saveToFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/UnlockList.xml");
		return UnlockName;
	}
	return XmlString("");
}

XmlString UnlockSystem::UnlockEntity(XmlNodeRef &ref)
{
	XmlString EntityName;
	XmlString archetype;
	ref->getAttr("Archetype", archetype);
	ref->getAttr("Name", EntityName);
	g_pGame->GetScanSystem()->AddAndSave(archetype);
	return EntityName;
}

XmlString UnlockSystem::UnlockProjectile(XmlNodeRef &ref)
{
	XmlString ProjectileName;
	EntityProjectileManager &Instance = EntityProjectileManager::Instance();
	int index;
	ref->getAttr("ProjectileFunctionType", index);
	ref->getAttr("Name", ProjectileName);
	Instance.SetFunctionUnlockStatus(index, true);
	return ProjectileName;
}