#include "StdAfx.h"
#include "BattleChallenge.h"
#include "Code_System/MicroSystem/EnemyScan.h"

#include "WorldStability.h"
BattleChallenge::BattleChallenge()
{
	HasGeneratedChallenge = false;
	CanGenerateChallenge = false;
	Reset_Timer = 0;
	Reset_Time = 5;
}


//------------------------------------------------------------------------
void BattleChallenge::GenerateChallenge(int enemyCount)
{
	if(HasGeneratedChallenge || !CanGenerateChallenge)
		return;

	InitChallengeParams();
	int Possible_Challenge_Num = PossibleChallange.size();
	int Challange_Index = cry_rand() % Possible_Challenge_Num;
	if(PossibleChallange[Challange_Index] == eCT_Eliminate)
	{
		GenerateEliminateChallenge(enemyCount);
		std::vector<int>::iterator iter = std::find(PossibleChallange.begin(),PossibleChallange.end(),eCT_Eliminate);
		PossibleChallange.erase(iter);
	}
	else if(PossibleChallange[Challange_Index] == eCT_Survivor)
	{
		GenerateSurvivorChallenge(enemyCount);
		std::vector<int>::iterator iter = std::find(PossibleChallange.begin(),PossibleChallange.end(),eCT_Survivor);
		PossibleChallange.erase(iter);
	}
	else if(PossibleChallange[Challange_Index] == eCT_Restrict)
	{
		GenerateRestrictChallenge(enemyCount);
		std::vector<int>::iterator iter = std::find(PossibleChallange.begin(),PossibleChallange.end(),eCT_Restrict);
		PossibleChallange.erase(iter);
	}

	int random_seed = cry_rand() % 100;
	if(random_seed > 100 - 40 * Challenges.size())
		GenerateChallenge(enemyCount);

	ClearChallenge();
	HasGeneratedChallenge = true;
	CanGenerateChallenge = false;
}


//------------------------------------------------------------------------
void BattleChallenge::Update(float frameTime)
{
	if(HasGeneratedChallenge == false)
	{
		Reset_Timer += frameTime;
		if(Reset_Timer > Reset_Time)
		{
			Reset_Timer = 0;
			CanGenerateChallenge = true;
		}
	}

	if(Challenges.count(eCT_Eliminate))
	{
		UpdateEliminate(frameTime);
	}

	if(Challenges.count(eCT_Survivor))
	{
		UpdateSurvivor(frameTime);
	}

	if(Challenges.count(eCT_Restrict))
	{
		UpdateRestrict(frameTime);
	}
}


//------------------------------------------------------------------------
void BattleChallenge::UpdateEliminate(float frameTime)
{
	int TargetCount = m_EChallange.Targets.size();
	string Name("");
	if(m_EChallange.Type == eET_WithWeapon && m_EChallange.WeaponId != -1)
	{
		IEntity *pWeaponEntity = gEnv->pEntitySystem->GetEntity(m_EChallange.WeaponId);
		if(pWeaponEntity)
			Name = pWeaponEntity->GetClass()->GetName();
	}
	if(m_EChallange.Type == eET_WithAbility && m_EChallange.AbilityType != -1)
	{
		AbilityModule *pModuleManager = g_pGame->GetAbilityModule();
		BaseAbility *pModule = pModuleManager->GetSlot(m_EChallange.AbilitySlotIndex);
		Name = pModule->GetName();
	}


	const float white[4]	= {1.0f, 10.f, 1.0f, 1.0f};
	if(Name != "")
		gEnv->pRenderer->Draw2dLabel(50,125,1.5f,white,false,"Kill %d enemies with %s",TargetCount,Name);
}


//------------------------------------------------------------------------
void BattleChallenge::UpdateSurvivor(float frameTime)
{
	CActor *pPlayer = (CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	float curHealth = pPlayer->GetHealth();
	if(curHealth < m_SChallange.HealthLimit)
	{
		Challenges.erase(eCT_Survivor);
	}

	const float white[4]	= {1.0f, 10.f, 1.0f, 1.0f};
	gEnv->pRenderer->Draw2dLabel(50,160,1.5f,white,false,"Keep your health above %.0f",m_SChallange.HealthLimit);
}


//------------------------------------------------------------------------
void BattleChallenge::UpdateRestrict(float frameTime)
{
	m_RChallange.tick_timer += frameTime;
	if(m_RChallange.tick_timer > m_RChallange.last_time)
	{
		RewardRestrict();
		Challenges.erase(eCT_Restrict);
	}

	const float white[4]	= {1.0f, 10.f, 1.0f, 1.0f};
	gEnv->pRenderer->Draw2dLabel(50,160,1.5f,white,false,"Keep your health above %.0f",m_SChallange.HealthLimit);
}


//------------------------------------------------------------------------
void BattleChallenge::InitChallengeParams()
{
	if(!PossibleChallange.empty())
		return;

	CActor *pPlayer = (CActor *)g_pGame->GetIGameFramework()->GetClientActor();
	if(pPlayer)
	{
		float PlayerHealth = pPlayer->GetHealth();
		float PlayerMaxHealth = pPlayer->GetMaxHealth();
		float healthRatio = PlayerHealth / PlayerMaxHealth;
		if(healthRatio > 0.3 && healthRatio <= 1)
			PossibleChallange.push_back(eCT_Survivor);
		PossibleChallange.push_back(eCT_Eliminate);
		PossibleChallange.push_back(eCT_Restrict);
		ScanWeaponStatus(pPlayer);
		ScanAbilityStatus();
	}
}


//------------------------------------------------------------------------
void BattleChallenge::GenerateEliminateChallenge(int enemyCount)
{
/*	m_EChallange.WeaponId = -1;
	m_EChallange.AbilityType = -1;
	int random_seed = cry_rand();
	int EliminateNum =  random_seed % enemyCount;
	if(EliminateNum == 0)
		EliminateNum = enemyCount;	
	std::set<EntityId> tempSet = EnemyScan::theScan().GetScanedEnemies();
	std::set<EntityId>::iterator origin_iter = tempSet.begin();
	for(int i=0;i<EliminateNum;++i)
	{
		m_EChallange.Targets.insert(*origin_iter);
		++origin_iter;
	}

	random_seed = cry_rand();
	m_EChallange.Type = random_seed % 2;
	if(!CurrentWeaponId.empty())
	{
		random_seed = cry_rand();
		int WeaponIndex = random_seed % CurrentWeaponId.size();
		IEntity *pEntity = gEnv->pEntitySystem->GetEntity(CurrentWeaponId[WeaponIndex]);
		const char *pClassName = pEntity->GetClass()->GetName();
		m_EChallange.WeaponId = CurrentWeaponId[WeaponIndex];
	}

	if(!CurrentAbilityType.empty())
	{
		random_seed = cry_rand();
		int AbilityIndex = random_seed % CurrentAbilityType.size();
		AbilityModule *pModuleManager = g_pGame->GetAbilityModule();
		BaseAbility *pModule = pModuleManager->GetSlot(CurrentAbilityType[AbilityIndex]);
		m_EChallange.AbilityType = pModule->GetHitType();
		m_EChallange.AbilitySlotIndex = CurrentAbilityType[AbilityIndex];
	}

	if(m_EChallange.AbilityType == -1)
	{
		m_EChallange.Type = eET_WithWeapon;
	}

	m_EChallange.EmemyCount = enemyCount;
	Challenges.insert(eCT_Eliminate);*/
}


//------------------------------------------------------------------------
void BattleChallenge::GenerateSurvivorChallenge(int enemyCount)
{
	CActor *pPlayer = (CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	if(pPlayer)
	{
		m_SChallange.CheckedHealth = pPlayer->GetHealth();
		double percentage = 1;
		if(enemyCount < 4)
			percentage = 1 - enemyCount * 0.1;
		else if(enemyCount <= 8)
			percentage = 1 - enemyCount * 0.05;
		else
			percentage = 0.3;
		m_SChallange.HealthLimit = static_cast<float>(m_SChallange.CheckedHealth * percentage);
		Challenges.insert(eCT_Survivor);
	}
}


//------------------------------------------------------------------------
void BattleChallenge::GenerateRestrictChallenge(int enemyCount)
{
	int random_seed = cry_rand();
	m_RChallange.RestrictType = random_seed % eRT_Last;

	if(Challenges.count(eCT_Eliminate) && m_EChallange.Type == eET_WithWeapon)
		while(m_RChallange.RestrictType == eRT_NoFire)
		{
			random_seed = cry_rand();
			m_RChallange.RestrictType = random_seed % eRT_Last;
		}

	if(Challenges.count(eCT_Eliminate) && m_EChallange.Type == eET_WithAbility)
		while(m_RChallange.RestrictType == eRT_NoAbility)
		{
			random_seed = cry_rand();
			m_RChallange.RestrictType = random_seed % eRT_Last;
		}

	m_RChallange.last_time = 50.0f - 5 * enemyCount;
	if(m_RChallange.last_time <= 5)
	{
		m_RChallange.last_time = 10;
	}
	m_RChallange.tick_timer = 0;

	Challenges.insert(eCT_Restrict);
}


//------------------------------------------------------------------------
void BattleChallenge::RewardEliminate()
{
	int random_seed = cry_rand() % 100;
	if(random_seed >= 0 && random_seed < 30)
	{
		g_pGame->GetWorldStability()->StabilityGain(m_EChallange.EmemyCount * 30);
	}
	else if(random_seed >= 30 && random_seed <= 70)
	{

	}
	else
	{

	}
}


//------------------------------------------------------------------------
void BattleChallenge::RewardSurvivor()
{

}


//------------------------------------------------------------------------
void BattleChallenge::RewardRestrict()
{

}


//------------------------------------------------------------------------
void BattleChallenge::CheckHitForEliminteChallenge(const HitInfo &hit)
{
	if(!Challenges.count(eCT_Eliminate))
		return;
	if(m_EChallange.AbilityType != -1 || m_EChallange.WeaponId != -1)
	{
		if(m_EChallange.Type == eET_WithWeapon)
		{
			if(hit.weaponId == m_EChallange.WeaponId && m_EChallange.Targets.count(hit.targetId))
			{
				m_EChallange.Targets.erase(hit.targetId);
			}
			else if(hit.weaponId != m_EChallange.WeaponId && m_EChallange.Targets.count(hit.targetId))
			{
				Challenges.erase(eCT_Eliminate);
				ClearEliminateChallenge();
			}
		}
		else if(m_EChallange.Type == eET_WithAbility)
		{
			if(hit.type == m_EChallange.AbilityType && m_EChallange.Targets.count(hit.targetId))
			{
				m_EChallange.Targets.erase(hit.targetId);
			}
			else if(hit.weaponId != m_EChallange.AbilityType && m_EChallange.Targets.count(hit.targetId))
			{
				Challenges.erase(eCT_Eliminate);
				ClearEliminateChallenge();
			}
		}
	}

	if(m_EChallange.Targets.empty() && Challenges.count(eCT_Eliminate))
	{
		//Reward for Eliminate Challenge
		RewardEliminate();
		Challenges.erase(eCT_Eliminate);
		ClearEliminateChallenge();
	}
}


//------------------------------------------------------------------------
void BattleChallenge::ScanWeaponStatus(CActor *pPlayer)
{
	IInventory *pInventory = pPlayer->GetInventory();
	int InventoryCount = pInventory->GetCount();
	for(int i = 0;i < InventoryCount;++i)
	{
		IItem *pItem = g_pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pInventory->GetItem(i));
		if(pItem)
		{
			CWeapon *pWeapon = (CWeapon*)(pItem->GetIWeapon());
			if(pWeapon)
			{
				const char *pClassName = pWeapon->GetEntity()->GetClass()->GetName();
				if(strcmp(pClassName,"NoWeapon") != 0)
					CurrentWeaponId.push_back(pWeapon->GetEntity()->GetId());
			}
		}
	}
}


//------------------------------------------------------------------------
void BattleChallenge::ScanAbilityStatus()
{
	AbilityModule *pModuleManager = g_pGame->GetAbilityModule();
	if(pModuleManager)
	{
		for(int i=0;i<3;++i)
		{
			BaseAbility *pAbility = pModuleManager->GetSlot(i);
			if(pAbility && pAbility->Lethal())
			{
				CurrentAbilityType.push_back(i);
			}
		}
	}
}


//------------------------------------------------------------------------
void BattleChallenge::ClearEliminateChallenge()
{
	m_EChallange.AbilitySlotIndex = -1;
	m_EChallange.AbilityType = -1;
	m_EChallange.Targets.clear();
	m_EChallange.Type = -1;
	m_EChallange.WeaponId = -1;
}


//------------------------------------------------------------------------
void BattleChallenge::ClearChallenge()
{
	PossibleChallange.clear();
	CurrentAbilityType.clear();
	CurrentWeaponId.clear();
}


//------------------------------------------------------------------------
void BattleChallenge::ResetChallengeStatus()
{
	//Reward for Survivor Challenge
	if(Challenges.count(eCT_Survivor))
	{
		RewardSurvivor();
	}

	//Reward for Restrict Challenge
	if(Challenges.count(eCT_Restrict))
	{
		RewardRestrict();
	}

	Reset_Timer = 0;
	if(HasGeneratedChallenge)
	{
		Challenges.clear();
		ClearChallenge();
		ClearEliminateChallenge();
		HasGeneratedChallenge = false;
		CanGenerateChallenge = false;
	}
}


//------------------------------------------------------------------------
const bool BattleChallenge::IsChallangeTarget(const EntityId Id)
{
	if(m_EChallange.Targets.count(Id))
		return true;
	return false;
}

const int BattleChallenge::GetRestrictType()
{
	if(Challenges.count(eCT_Restrict))
	{
		return m_RChallange.RestrictType;
	}
	return eRT_Last;
}

void BattleChallenge::FailRestrictChallenge()
{
	Challenges.erase(eCT_Restrict);
}