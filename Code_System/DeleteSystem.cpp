#include "StdAfx.h"
#include "DeleteSystem.h"
#include "Buff.h"
#include "IBreakableManager.h"
#include "Code_System/MicroSystem/StateManager.h"
#include "BattleChallenge.h"
#include "CodeObject/Trap.h"
#include "Code_System/CodeObject/AISpawnPoint.h"
#include "Code_System/MicroSystem/EnemyScan.h"
#include "CodeExclusion.h"

DeleteSystem::DeleteSystem()
{
	curindex=0;
	Maxnumber=5;
	OnDelete=false;
	DefaultDeleteMtl=gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("delete");
	m_RobotClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("SystemRobot");

	preLoad = EntityClassPreLoad::Instance();
}

//------------------------------------------------------------------------
const int DeleteSystem::FindAvailable(const bool IsSystemDeleting /* = false */)
{
	if(!IsSystemDeleting)
	{
		if(DP[curindex].available && curindex!=-1)
			return curindex;
		int i=0;
		for(;i<Maxnumber;++i)
			if(DP[i].available)
				return i;
		return -1;
	}
	else
	{
		int i=10;
		if(DP[i].available && i != -1)
			return i;
		for(;i<maxSlot;++i)
			if(DP[i].available)
				return i;
		return -1;
	}
}

//------------------------------------------------------------------------
const bool DeleteSystem::IsDeleting(IEntity *pEntity)
{
	for (int i=0;i<maxSlot;++i)
	{
		if(DP[i].DEntity)
			if(DP[i].DEntity== pEntity)
				return true;
	}
	return false;
}

//------------------------------------------------------------------------
void DeleteSystem::ProcessDeleteEntity(IEntity *DEntity,bool IsSystemDeleting /* = false */)
{
	if(DEntity)
	{
		if(DEntity->GetId() == g_pGame->GetClientActorId())
			return;
		if(IsDeleting(DEntity))
			return;

		if(!IsSystemDeleting)
			curindex=FindAvailable();
		else
			curindex=FindAvailable(true);

		if(curindex==-1)
			return;
		else
		{
			BattleChallenge &instance = BattleChallenge::Instance();
			if(instance.GetRestrictType() == eRT_NoDelete)
				instance.FailRestrictChallenge();
			DP[curindex].DEntity = DEntity;
			IEntityClass *pClass = DEntity->GetClass();
			DP[curindex].AI = (pClass == preLoad.pHumanClass);    //Remember to change if there are new AI class added.
			if(DP[curindex].AI)
				return;
			if(pClass == preLoad.pHMMWV)
				DEntity->EnablePhysics(false);
			DP[curindex].DEntity=DEntity;
			AABB localaabb;
			DP[curindex].DEntity->GetLocalBounds(localaabb);
			DP[curindex].deleteTime=localaabb.GetVolume()/3;    //Need more calculate.REMBERER!!!
			if(DP[curindex].deleteTime < 1)
				DP[curindex].deleteTime = 1;
			if(DP[curindex].deleteTime > 10)
				DP[curindex].deleteTime = 10;
			if(!IsSystemDeleting)
			{
				StateManager::theStateManager().AddProgressState(DEntity->GetId(), DP[curindex].deleteTime);
			}
			DP[curindex].deletemtl=gEnv->p3DEngine->GetMaterialManager()->CloneMaterial(DefaultDeleteMtl,0);


			DP[curindex].DEntity->SetMaterial(DP[curindex].deletemtl);
			//DP[curindex].DEntity->EnablePhysics(false);
			DP[curindex].available=false;
			OnDelete=true;
			IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Code_System.DeleteEntity");
			DP[curindex].DEntity->LoadParticleEmitter(10,pEffect);

			if(IEntityLink *pLink = DEntity->GetEntityLinks())
			{
				CAISpawnPoint* pSpawnPoint = static_cast<CAISpawnPoint*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pLink->entityId, "AISpawnPoint"));
				if(pSpawnPoint)
					pSpawnPoint->ClearEntitySpawn();
			}
			else
			{
				AddToDeleteRecovery(DP[curindex].DEntity, IsSystemDeleting);
			}

			ProcessScript(IsSystemDeleting);
		}
	}
}


void DeleteSystem::ProcessDeleteEntityForFastDelete(IEntity *DEntity)
{
	if(DEntity)
	{
		if(DEntity->GetId() == g_pGame->GetClientActorId())
			return;
		if(IsDeleting(DEntity))
			return;

		curindex=FindAvailable();

		if(curindex==-1)
			return;
		else
		{
			BattleChallenge &instance = BattleChallenge::Instance();
			if(instance.GetRestrictType() == eRT_NoDelete)
				instance.FailRestrictChallenge();
			DP[curindex].DEntity = DEntity;
			IEntityClass *pClass = DEntity->GetClass();
			if(pClass == preLoad.pHMMWV)
				DEntity->EnablePhysics(false);

			DP[curindex].DEntity=DEntity;
			DP[curindex].deleteTime=1;    //Need more calculate.REMBERER!!!
			DP[curindex].deletemtl=gEnv->p3DEngine->GetMaterialManager()->CloneMaterial(DefaultDeleteMtl,0);
			DP[curindex].DEntity->SetMaterial(DP[curindex].deletemtl);
			DP[curindex].available=false;

			OnDelete=true;
			IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Code_System.DeleteEntity");

			if(IEntity *pParent = DEntity->GetParent())
			{
				CAISpawnPoint* pSpawnPoint = static_cast<CAISpawnPoint*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pParent->GetId(), "AISpawnPoint"));
				pSpawnPoint->ClearEntitySpawn();
			}
			else
			{
				AddToDeleteRecovery(DP[curindex].DEntity, false);
			}

			EnemyScan::theScan().MarkEntityForRemove(DEntity->GetId());
			//ProcessScript();
		}
	}
}

//------------------------------------------------------------------------
void DeleteSystem::ClearDP(const int i)
{
	DP[i].DEntity=NULL;
	DP[i].cost=0;
	DP[i].amount=10;
	DP[i].deletemtl=NULL;
	DP[i].rate=0;
	DP[i].timer=0;
	DP[i].available=true;
}

//------------------------------------------------------------------------
void DeleteSystem::ProcessScript(const bool IsSystemDeleting)
{
	if(DP[curindex].DEntity && !DP[curindex].AI)
	{
		AABB localaabb;
		DP[curindex].DEntity->GetLocalBounds(localaabb);
		SmartScriptTable pScript=DP[curindex].DEntity->GetScriptTable();
		SmartScriptTable propTable;
		pScript->GetValue("Properties",propTable);
		if(propTable)
			propTable->GetValue("DeleteCost",DP[curindex].cost);
		DP[curindex].cost+=(int)localaabb.GetVolume();
		if(!IsSystemDeleting)
			g_pGame->GetWorldStability()->StabilityCost(DP[curindex].cost);

		//Add Exclusion Amount
		CodeExclusion::Instance().AddExclusionAmount(localaabb.GetVolume());
	}
}

//------------------------------------------------------------------------
const bool DeleteSystem::NeedDelete()
{
	for(int i=0;i<maxSlot;i++)
		if(!DP[i].available)
			return true;
	return false;
}

//------------------------------------------------------------------------
void DeleteSystem::Update(float &frameTime)
{
	if(OnDelete)
	{
		for(int i=0;i<Maxnumber;++i)
			UpdateDP(i,frameTime);

		for(int j=10;j<maxSlot;++j)
			UpdateDP(j,frameTime);
	}
}



//------------------------------------------------------------------------
void DeleteSystem::UpdateDP(const int i,const float frameTime)
{
	if(!DP[i].available)
	{
		UpdateDPStep_1(i,frameTime);
		UpdateDPStep_2(i,frameTime);
		UpdateDPStep_3(i,frameTime);
	}
}

//------------------------------------------------------------------------
void DeleteSystem::UpdateDPStep_1(const int i,const float frameTime)
{
	//If vehicles explode while deleting,deletemtl will be wrong
	//REMEMBER TO FIX IT LATER
	DP[i].timer+=frameTime;
	DP[i].rate=15/DP[i].deleteTime/3;
	if(DP[i].timer<DP[i].deleteTime/3)
	{
		DP[i].amount+=DP[i].rate*frameTime;
		DP[i].deletemtl->SetGetMaterialParamFloat("glow",DP[i].amount,false);
	}
}

//------------------------------------------------------------------------
void DeleteSystem::UpdateDPStep_2(const int i,const float frameTime)
{
	DP[i].rate=25/(DP[i].deleteTime-DP[i].deleteTime/3);
	if(DP[i].timer>DP[i].deleteTime/2 && DP[i].timer<DP[i].deleteTime && DP[i].amount>=0)
	{
		DP[i].amount-=DP[i].rate*frameTime;
		DP[i].deletemtl->SetGetMaterialParamFloat("glow",DP[i].amount,false); 
	}
}

//------------------------------------------------------------------------
void DeleteSystem::UpdateDPStep_3(const int i,const float frameTime)
{
	if(DP[i].timer>=DP[i].deleteTime)
	{
		//DP[i].DEntity->SetMaterial(DP[i].DefaultMtl);
		EnemyScan::theScan().MarkEntityForRemove(DP[i].DEntity->GetId());
		IParticleEmitter *pEmitter=DP[i].DEntity->GetParticleEmitter(10);
		if(pEmitter)
			gEnv->pParticleManager->DeleteEmitter(pEmitter);
		DP[i].DEntity->SetUpdatePolicy(ENTITY_UPDATE_NEVER);
		if(!DP[i].AI)
		{
			/*gEnv->pEntitySystem->RemoveEntity(DP[i].DEntity->GetId(),true);
			gEnv->pEntitySystem->DeletePendingEntities();*/
			IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Code_System.DeleteFinished");
			AABB aabb;
			DP[i].DEntity->GetWorldBounds(aabb);
			pEffect->Spawn(true,IParticleEffect::ParticleLoc(aabb.GetCenter(),Vec3(0,0,1)));
			DP[i].DEntity->EnablePhysics(false);
			DP[i].DEntity->SetPos(Vec3(0,0,0));
			DP[i].DEntity->Hide(true);
			if(DP[i].DEntity->GetClass() == preLoad.pTrap)
			{
				CTrap* pTrap = static_cast<CTrap*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(DP[i].DEntity->GetId(), "Trap"));
				pTrap->Disable();
			}
			to_delete_list.insert(DP[i].DEntity->GetId());
			IParticleEmitter *pEmitter=DP[i].DEntity->GetParticleEmitter(10);
			if(pEmitter)
				pEmitter->Activate(false);
			//gEnv->pEntitySystem->RemoveEntity(DP[i].DEntity->GetId());
		}
		if(DP[i].AI)
		{
			return;
			/*CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(DP[i].DEntity->GetId());
			pActor->DropItem(pActor->GetCurrentItemId());
			pActor->Kill();
			DP[i].DEntity->Hide(true);
			DP[i].DEntity->SetPos(Vec3(0,0,0));*/
		}
		if(i < Maxnumber)
			ProcessBuff(DP[i].DEntity);
		ClearDP(i);
		OnDelete=NeedDelete();
	}
}

void DeleteSystem::ProcessBuff(IEntity *pEntity) 
{
	BuffManager &TheBuffManager=BuffManager::TheBuffManager();
	IEntityClass *pEntityClass=pEntity->GetClass();

	if(pEntityClass == preLoad.pBasicEntityClass)
		TheBuffManager.CreateHealthBuff(pEntity);

	if(pEntityClass == preLoad.pDestroyableObjectClass)
		TheBuffManager.CreateDamageBuff(pEntity);


	//ProjEffects
	IEntityClass *MineClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("Mine");
	if(pEntityClass == MineClass)
		g_pGame->GetProjEffectManager()->Add_BulletReflection_Effect();
}

void DeleteSystem::AddToDeleteRecovery(IEntity *pEntity, bool isSystemDeleting)
{
	if(!pEntity)
		return;
	std::list<EntityId> &SpawnEntitySet=g_pGame->GetSpawnSystem()->GetSpawnEntitySet();
	std::list<EntityId>::iterator it=find(SpawnEntitySet.begin(),SpawnEntitySet.end(),pEntity->GetId());
	if(it == SpawnEntitySet.end() && !isSystemDeleting && pEntity->GetClass() != m_RobotClass)
	{
		//Communicate with SpawnRobot
		SEntitySpawnParams param;
		param.sName = pEntity->GetName();
		param.vPosition = pEntity->GetPos();
		param.vScale = pEntity->GetScale();
		param.pClass = pEntity->GetClass();
		param.nFlags = pEntity->GetFlags();
		param.qRotation = pEntity->GetRotation();
		param.prevId = pEntity->GetId();
		DeleteRecoveryInfo.push_back(param);
	}
	if(it != SpawnEntitySet.end())
	{
		SpawnEntitySet.erase(it);
	}
}

void DeleteSystem::ClearDeleteList(bool force /* = false */)
{
	if(to_delete_list.size() >= 100 || force)
	{
		std::set<EntityId>::iterator iter = to_delete_list.begin();
		std::set<EntityId>::iterator end = to_delete_list.end();
		IEntitySystem *pSystem = gEnv->pEntitySystem;
		while(iter != end)
		{
			if(!DP[curindex].DEntity)
			{
				++iter;
				continue;
			}
			g_pGame->GetBasicRayCast()->ClearLastEntity(*iter);
			if(DP[curindex].DEntity->GetId() != *iter)
				pSystem->RemoveEntity(*iter,true);
			++iter;
		}
		to_delete_list.clear();
	}
};