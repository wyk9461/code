#include "StdAfx.h"
#include "EntityProjectileFunction.h"
#include "EntityProjectile.h"

#include "Projectile.h"
#include "Code_System/NewProjEffect/StickProjectileManager.h"
#include "Code_System/NewProjEffect/DeteriorationManager.h"
#include "Code_System/NewProjEffect/VirusManager.h"
#include "Code_System/MicroSystem/FallenActorManager.h"

//-----------------------------------------------------------------------------------------------------------------------------------
void EPF::ProcessHit_None(ProcessFunParams &params)
{
	CEntityProjectile *pProjectile = params.pProjectile;
	pProjectile->Destroy();
}

//-----------------------------------------------------------------------------------------------------------------------------------
void EPF::ProcessHit_General(ProcessFunParams &params)
{
	CActor *pPlayer=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	CWeapon *pWeapon=(CWeapon*)pPlayer->GetCurrentItem();
	if(params.target && pWeapon)
	{
		EntityId targetId = params.target->GetId();
		CEntityProjectile *pProjectile = params.pProjectile;

		int ownerId = params.pProjectile->GetOwnerId();
		int hostId = pWeapon->GetHostId();
		int hitTypeId = 1;
		HitInfo hitInfo(ownerId ? ownerId : hostId, targetId, pWeapon->GetEntityId(),
			params.damage, 0.0f, params.hitMatId, params.collision.partid[1],
			hitTypeId, params.collision.pt, params.hitDir, params.collision.n);

		hitInfo.remote = pProjectile->IsRemote();
		hitInfo.projectileId = pProjectile->GetEntityId();
		hitInfo.bulletType = pProjectile->GetAmmoParams().bulletType;
		hitInfo.knocksDown = false;
		hitInfo.knocksDownLeg = false;
		hitInfo.hitViaProxy =false;
		hitInfo.aimed = false;

		g_pGame->GetGameRules()->ClientHit(hitInfo);
		pProjectile->Destroy();
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------
void EPF::ProcessHit_Explode(ProcessFunParams &params)
{
	EventPhysCollision *pCollision = &params.collision;
	CEntityProjectile *pProjectile = params.pProjectile;

	CommonUsage::CreateQueueExplosion(pProjectile->GetWeapon()->GetOwnerId(),pCollision->pt,pProjectile->GetDamage(),2,100);
	pProjectile->Destroy();
}

//-----------------------------------------------------------------------------------------------------------------------------------
void EPF::ProcessHit_Heal(ProcessFunParams &params)
{
	CActor *pPlayer=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	pPlayer->SetHealth(pPlayer->GetHealth() + params.damage);
	params.pProjectile->Destroy();
}

//-----------------------------------------------------------------------------------------------------------------------------------
void EPF::ProcessHit_Impact(ProcessFunParams &params)
{
	EventPhysCollision *pCollision = &params.collision;
	CEntityProjectile *pProjectile = params.pProjectile;

	IEntity *pTarget = params.target;
	if(pTarget)
	{
		CActor *pActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId());
		if(pActor)
		{
			CActor *pPlayer=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
			CWeapon *pWeapon=(CWeapon*)pPlayer->GetCurrentItem();
			if(pWeapon)
			{
				int ownerId = params.pProjectile->GetOwnerId();
				int hostId = pWeapon->GetHostId();
				int hitTypeId = 1;
				HitInfo hitInfo(ownerId ? ownerId : hostId, pTarget->GetId(), pWeapon->GetEntityId(),
					0.0f, 0.0f, params.hitMatId, params.collision.partid[1],
					hitTypeId, params.collision.pt, params.hitDir, params.collision.n);

				hitInfo.remote = pProjectile->IsRemote();
				hitInfo.projectileId = pProjectile->GetEntityId();
				hitInfo.bulletType = pProjectile->GetAmmoParams().bulletType;
				hitInfo.knocksDown = false;
				hitInfo.knocksDownLeg = false;
				hitInfo.hitViaProxy =false;
				hitInfo.aimed = false;

				//pActor->Fall(hitInfo);
				if(!pActor->IsFallen())
					FallenActorManager::Instance().AddToManager(pActor);
				pProjectile->Destroy();
				return;
			}
		}
		IEntityPhysicalProxy *pProxy = (IEntityPhysicalProxy*)pTarget->GetProxy(ENTITY_PROXY_PHYSICS);
		if (pProxy)
		{
			pe_status_dynamics params;
			pTarget->GetPhysics()->GetStatus(&params);
			float &mass = params.mass;
			AABB aabb;
			pTarget->GetWorldBounds(aabb);
			pProxy->AddImpulse(-1, pCollision->pt, -pCollision->n*mass*10, true, 1, 1);
		}
	}
	//CommonUsage::CreateQueueExplosion(pProjectile->GetWeapon()->GetOwnerId(),pCollision->pt,0,1,300);
	pProjectile->Destroy();
}

//-----------------------------------------------------------------------------------------------------------------------------------
void EPF::ProcessHit_Stick(ProcessFunParams &params)
{
	EventPhysCollision *pCollision = &params.collision;
	CEntityProjectile *pProjectile = params.pProjectile;

	IEntity *pTarget = params.target;
	Vec3 offset;
	if(pTarget)
	{
		Matrix34 oldLocalTM = pTarget->GetLocalTM();
		offset = pCollision->pt - oldLocalTM.GetTranslation();
	}
	else
	{
		offset = pCollision->pt;
	}

	StickParams sParams;
	sParams.damage = 100;
	sParams.tick_timer = 0;
	sParams.explode_time = 3;
	sParams.pTarget = pTarget;
	sParams.offset = offset;

	StickProjectileManager::Instance().AddToDeque(sParams);
}

//-----------------------------------------------------------------------------------------------------------------------------------
void EPF::ProcessHit_MicroExplode(ProcessFunParams &params)
{
	EventPhysCollision *pCollision = &params.collision;
	CEntityProjectile *pProjectile = params.pProjectile;

	float damage = pProjectile->GetDamage();
	if(damage > 100)
		damage = 100;
	CommonUsage::CreateQueueExplosion(pProjectile->GetWeapon()->GetOwnerId(),pCollision->pt,damage,1 , 0, CGameRules::EHitType::Explosion, 
		"EntityProjectile.StickBomb_Explode");
	pProjectile->Destroy();
}

//-----------------------------------------------------------------------------------------------------------------------------------
void EPF::ProcessHit_Deterioration(ProcessFunParams &params)
{
	EventPhysCollision *pCollision = &params.collision;
	CEntityProjectile *pProjectile = params.pProjectile;

	DeteriorationManager &manager = DeteriorationManager::Instance();
	IEntity *pTarget = params.target;
	float damage = pProjectile->GetDamage();
	if(pTarget)
	{
		if(manager.HitSameTarget(pTarget))
		{
			damage *= (1 + manager.GetDeterValue());
			if(manager.GetDeterValue() < 2)
				manager.IncDeterValue();
		}
		else
		{
			manager.SetTarget(pTarget);
			manager.ClearDeterValue();
			manager.IncDeterValue();
		}
	}

	CActor *pPlayer=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	CWeapon *pWeapon=(CWeapon*)pPlayer->GetCurrentItem();
	if(params.target && pWeapon)
	{
		EntityId targetId = params.target->GetId();
		CEntityProjectile *pProjectile = params.pProjectile;

		int ownerId = params.pProjectile->GetOwnerId();
		int hostId = pWeapon->GetHostId();
		int hitTypeId = 1;
		HitInfo hitInfo(ownerId ? ownerId : hostId, targetId, pWeapon->GetEntityId(),
			damage, 0.0f, params.hitMatId, params.collision.partid[1],
			hitTypeId, params.collision.pt, params.hitDir, params.collision.n);

		hitInfo.remote = pProjectile->IsRemote();
		hitInfo.projectileId = pProjectile->GetEntityId();
		hitInfo.bulletType = pProjectile->GetAmmoParams().bulletType;
		hitInfo.knocksDown = true;
		hitInfo.knocksDownLeg = true;
		hitInfo.hitViaProxy =false;
		hitInfo.aimed = false;

		g_pGame->GetGameRules()->ClientHit(hitInfo);
		pProjectile->Destroy();
	}
}


//-----------------------------------------------------------------------------------------------------------------------------------
void EPF::ProcessHit_InfectionVirus(ProcessFunParams &params)
{
	EventPhysCollision *pCollision = &params.collision;
	CEntityProjectile *pProjectile = params.pProjectile;

	if(params.target)
	{
		InfectionVirusManager &manager = InfectionVirusManager::Instance();
		InfectionVirusParams *vParams = new InfectionVirusParams;
		vParams->damage = 1;
		vParams->tick_timer = 0;
		vParams->InfectionTime = 3;
		vParams->pHost = params.target;

		manager.AddNewCoreVirus(*vParams);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------
void EPF::ProcessHit_ReplicationVirus(ProcessFunParams &params)
{
	EventPhysCollision *pCollision = &params.collision;
	CEntityProjectile *pProjectile = params.pProjectile;

	if(params.target)
	{
		ReplicationVirusManager &manager = ReplicationVirusManager::Instance();
		ReplicationVirusParams *vParams = new ReplicationVirusParams;
		pe_status_dynamics status;
		params.target->GetPhysics()->GetStatus(&status);

		vParams->mass = status.mass;
		vParams->damage = 1;
		vParams->tick_timer = 0;
		vParams->InfectionTime = 3;
		vParams->pHost = params.target;

		manager.AddNewCoreVirus(*vParams);
	}
}