#ifndef _RECOVERY_H_
#define _RECOVERY_H_

#include <queue>

class CRecovery
{
private:
	float HealthRecovery_Timer;
	float HealthRecovery_Delay;
	float Default_HRDelay;
	float HealthRecovery_Amount;

	float StabiityRecovery_Timer;

	struct MetaStability
	{
		int amount;
		float delay;
		MetaStability()
		:	amount(0)
		,	delay(0){}
	};

	float curHealth;
	float prevHealth;
	std::queue<MetaStability> StabilityRecovery_Queue;

public:
	CRecovery()
	:	HealthRecovery_Timer(0)
	,	HealthRecovery_Delay(0)
	,	Default_HRDelay(5)
	,	HealthRecovery_Amount(0)
	,	StabiityRecovery_Timer(0)
	,	curHealth(1000)
	,	prevHealth(0){}
	~CRecovery();

	void Update(float frameTime);

	// Description :
	//		Set the health recovery state
	//	Arguments : 
	//		amount - the health amount recovered per frame
	//		delay - the delay before the player start recover
	inline void SetHealthRecoveryState(const float& amount , const float& delay){	HealthRecovery_Amount=amount;HealthRecovery_Delay=Default_HRDelay-delay;}

	// Description :
	//		Push the StabilityRecovery queue
	//	Arguments : 
	//		amount - the stability amount
	void PushStabilityRecoveryQueue(const MetaStability ms){StabilityRecovery_Queue.push(ms);}

	// Description :
	//		If the player isn't under attack
	const bool IsPlayerHealthChanged();

};
#endif