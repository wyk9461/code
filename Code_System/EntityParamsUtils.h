#ifndef _ENTITYPARAMSUTILS_H_
#define _ENTITYPARAMSUTILS_H_

enum EntityClassType
{
	eECT_BasciEntity					= 0x000001,
	eECT_DestroyableObject		= 0x000002,
	eECT_Vehicles						= 0x000004,
	eECT_Light							= 0x000008,
	eECT_AI_Weapon					= 0x000010,
	eECT_Player_Weapon			= 0x000020,

	eECT_BasicWeapon				= 0x000040,
	eECT_Shotgun						= 0x000080,

	eECT_Last								= 0x000100,
};

struct EntityClassPreLoad
{
private:

	void Init()
	{
		pBasicEntityClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("BasicEntity");
		pDestroyableObjectClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("DestroyableObject");
		pLightClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("DestroyableLight");
		pDoorClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("Door");
		pHumanClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("Human");
		pHMMWV = gEnv->pEntitySystem->GetClassRegistry()->FindClass("HMMWV");

		pTrap = gEnv->pEntitySystem->GetClassRegistry()->FindClass("Trap");
		pLeakHole = gEnv->pEntitySystem->GetClassRegistry()->FindClass("LeakHole");
		pPowerBall = gEnv->pEntitySystem->GetClassRegistry()->FindClass("PowerBall");
		pForceField = gEnv->pEntitySystem->GetClassRegistry()->FindClass("ForceField");
		pForceDevice = gEnv->pEntitySystem->GetClassRegistry()->FindClass("ForceDevice");
		pPressureTrigger = gEnv->pEntitySystem->GetClassRegistry()->FindClass("PressureTrigger");

		pEntityGun = gEnv->pEntitySystem->GetClassRegistry()->FindClass("EntityGun");
	}

public:
	static EntityClassPreLoad& Instance()
	{
		static EntityClassPreLoad preload;
		preload.Init();
		return preload;
	}

	IEntityClass *pBasicEntityClass;
	IEntityClass *pDestroyableObjectClass;
	IEntityClass *pLightClass;
	IEntityClass *pDoorClass;
	IEntityClass *pHumanClass;
	IEntityClass *pHMMWV;

	IEntityClass *pForceDevice;
	IEntityClass *pLeakHole;
	IEntityClass *pForceField;
	IEntityClass *pTrap;
	IEntityClass *pPowerBall;

	IEntityClass *pPressureTrigger;
	IEntityClass *pEntityGun;
};

class EntityParamsUtils
{
private:
	friend class SpawnSystem;
	friend class ModifySystem;
	friend class ScanSystem;
	char* EntityClassName;

	// Description :
	//		Entity's universal params.
	//		Most of them is used in SpawnSystem.
	struct Utils
	{
		int nslot;						// May be no use.
		int phys_type;				// If this entity is a static entity(1) or a rigid entity(2)
		int stabilitycost;			// Stability cost
		int projectile_type;		//Projectile type for EntityProjectile
		XmlString name;			// Entity's name
		XmlString entityclass;	// The entity class name
		Utils()
		:	nslot(0)
		,	phys_type(1)
		,	stabilitycost(0)
		,	projectile_type(0)
		,	entityclass(""){}

		Utils& operator=(const Utils &ut)
		{
			if(this == &ut)
				return *this;
			nslot=ut.nslot;
			phys_type=ut.phys_type;
			stabilitycost=ut.stabilitycost;
			projectile_type = ut.projectile_type;
			name=ut.name;
			entityclass=ut.entityclass;
			return *this;
		}

		void clear();
	};

	// Description :
	//		Entity's physical params.
	struct BasicPhysParams
	{
		float mass;			// You know it.
		float density;			// You also know it.
		bool rigid;				// if the entity can be affected by physical effects.
		BasicPhysParams()
		:	mass(-1)
		,	density(-1)
		,	rigid(false){}

		BasicPhysParams& operator=(const BasicPhysParams& bpp)
		{
			if(this == &bpp)
				return *this;
			mass=bpp.mass;
			density=bpp.density;
			rigid=bpp.rigid;
			return *this;
		}

		void clear();
	};

	// Description :
	//		Explosive params.
	struct ExplosiveParams
	{
		bool explode;					// if the entity can explode.
		bool invulnerable;				// if the entity isn't take any damage.
		float effectscale;				// the size of explosion effect.
		float minradius;					// min damage radius(entity within this range will take full damage).
		float radius;						// max damage range(entity without this range will take no damage).
		float minphysradius;			// min physics radius(entity within this range will take full physical impact).
		float physradius;				// max physics radius(entity without this range will take no physical impact).
		float soundradius;
		float damage;
		float pressure;
		float health;
		XmlString effect;
		XmlString model_destroyed;
		ExplosiveParams()
		:	explode(false)
		,	invulnerable(false)
		,	effectscale(0)
		,	minradius(0)
		,	radius(0)
		,	minphysradius(0)
		,	physradius(0)
		,	soundradius(0)
		,	damage(0)
		,	pressure(0)
		,	health(0)
		,	effect("")
		,	model_destroyed(""){}

		ExplosiveParams& operator=(const ExplosiveParams &ep)
		{
			if(this == &ep)
				return *this;
			explode=ep.explode;
			invulnerable=ep.invulnerable;
			effectscale=ep.effectscale;
			minradius=ep.minradius;
			radius=ep.radius;
			minphysradius=ep.minphysradius;
			physradius=ep.physradius;
			soundradius=ep.soundradius;
			damage=ep.damage;
			pressure=ep.pressure;
			health=ep.health;
			effect=ep.effect;
			model_destroyed=ep.model_destroyed;
			return *this;
		}

		void clear();
	};

	// Description :
	//		Light params.
	struct LightParams
	{
		bool turn_on;
		int lightstyle;
		float lightradius;
		float diffusemultiplier;
		float specularmultiplier;
		float HDRDynamic;
		Vec3 colordiffuse;
		LightParams()
		:	turn_on(false)
		,	lightstyle(0)
		,	lightradius(0)
		,	diffusemultiplier(0)
		,	specularmultiplier(0)
		,	HDRDynamic(0)
		,	colordiffuse(Vec3(0,0,0)){}

		LightParams& operator=(const LightParams& lp)
		{
			if(this == &lp)
				return *this;
			turn_on=lp.turn_on;
			lightstyle=lp.lightstyle;
			lightradius=lp.lightradius;
			diffusemultiplier=lp.diffusemultiplier;
			specularmultiplier=lp.specularmultiplier;
			HDRDynamic=lp.HDRDynamic;
			colordiffuse=lp.colordiffuse;
			return *this;
		}

		void clear();
	};

	//	Description : 
	//		AI Params.
	struct AIParams
	{
		XmlString aibehavior;
		XmlString faction;
		AIParams()
			:	aibehavior("")
			,	faction(""){}
		AIParams& operator=(const AIParams& ap)
		{
			if(this == &ap)
				return *this;
			aibehavior=ap.aibehavior;
			faction = ap.faction;
			return *this;
		}

		void clear();
	};

	//	Description : 
	//		Basic Weapon Params.
	//		For ModifySystem and PlayerInvntory.
	struct BasicWeaponParams
	{
		XmlString weaponName;
		XmlString weaponClass;
		float firerate;
		float player_damage;
		float ai_damage;
		float clip_size;
		BasicWeaponParams()
		:	firerate(0)
		,	player_damage(0)
		,	ai_damage(0)
		,	clip_size(0){}

		BasicWeaponParams& operator=(const BasicWeaponParams& bwp)
		{
			if(this == &bwp)
				return *this;
			weaponName=bwp.weaponName;
			weaponClass=bwp.weaponClass;
			firerate=bwp.firerate;
			player_damage=bwp.player_damage;
			ai_damage=bwp.ai_damage;
			clip_size=bwp.clip_size;
			return *this;
		}

		void clear();
	};

	//	Description : 
	//		Basic Weapon Params.
	//		For ModifySystem and PlayerInvntory.
	struct ShotgunParams
	{
		float pellets;
		float spread;
		float pellet_damage;
		ShotgunParams()
		:	pellets(0)
		,	spread(0)
		,	pellet_damage(0){}

		ShotgunParams& operator=(const ShotgunParams& sgp)
		{
			if(this == &sgp)
				return *this;
			pellets=sgp.pellets;
			spread=sgp.spread;
			pellet_damage=sgp.pellet_damage;
			return *this;
		}

		void clear();
	};

	//	Description : 
	//		Land Vehicle Params.
	struct LandVehiceParams
	{
		float acceleration;
		float decceleration;
		float topSpeed;
		float reverseSpeed;
		bool defaultxml;
		bool disablephysics;
		float velocitymultiplier;
		float degree;
		float backfriction;
		float frontfriction;
		float inertia;
		LandVehiceParams()
		:	acceleration(0)
		,	decceleration(0)
		,	topSpeed(0)
		,	reverseSpeed(0)
		,	defaultxml(false)
		,	disablephysics(false)
		,	velocitymultiplier(0)
		,	degree(0)
		,	backfriction(0)
		,	frontfriction(0)
		,	inertia(0){}
		LandVehiceParams& operator=(const LandVehiceParams& lvp)
		{
			if(this == &lvp)
				return *this ;
			acceleration=lvp.acceleration;
			decceleration=lvp.decceleration;
			topSpeed=lvp.topSpeed;
			reverseSpeed=lvp.reverseSpeed;
			defaultxml=lvp.defaultxml;
			disablephysics=lvp.disablephysics;
			velocitymultiplier=lvp.velocitymultiplier;
			degree=lvp.degree;
			backfriction=lvp.backfriction;
			frontfriction=lvp.frontfriction;
			inertia=lvp.inertia;
			return *this;
		}

		void clear();
	};

	struct TrapParams
	{
		float effectScale;

		bool isDataLeakage;
		bool isSlowDown;
		bool isDedication;
		bool isRecycle;
		bool isDamageCov;

		TrapParams &operator=(const TrapParams& rhs)
		{
			if(this == &rhs)
				return *this;
			effectScale = rhs.effectScale;
			isDataLeakage = rhs.isDataLeakage;
			isSlowDown = rhs.isSlowDown;
			isDedication = rhs.isDedication;
			isRecycle = rhs.isRecycle;
			isDamageCov = rhs.isDamageCov;
			return *this;
		}
	};
	struct LeakHoleParams
	{
		float damage;
		float radius;
		float Cd;

		LeakHoleParams& operator=(const LeakHoleParams& rhs)
		{
			if (this == &rhs)
			{
				return *this;
			}
			damage = rhs.damage;
			radius = rhs.radius;
			Cd = rhs.Cd;
			return *this;
		}
	};
	struct ForceDeviceParams
	{
		bool enable;
		float hitTimesLimit;
		float damage;
		float minExploRadius;
		float exploRadius;
		float force;
		float hitTimeInterval;

		ForceDeviceParams& operator=(const ForceDeviceParams& rhs)
		{
			if (this == &rhs)
				return *this;
			enable = rhs.enable;
			hitTimeInterval = rhs.hitTimeInterval;
			hitTimesLimit = rhs.hitTimesLimit;
			damage = rhs.damage;
			minExploRadius = rhs.minExploRadius;
			exploRadius = rhs.exploRadius;
			force = rhs.force;
			return *this;
		}
	};
	struct PowerBallParams
	{
		float damage;
		float powerUpperBound;
		float exploRadius;

		PowerBallParams& operator=(const PowerBallParams& rhs)
		{
			if (this == &rhs)
				return *this;
			damage = rhs.damage;
			powerUpperBound = rhs.powerUpperBound;
			exploRadius = rhs.exploRadius;
			return *this;
		}
	};
	struct ForceFieldParams
	{
		float radius;
		float accScale;

		ForceFieldParams& operator=(const ForceFieldParams& rhs)
		{
			if (this == &rhs)
				return *this;
			radius = rhs.radius;
			accScale = rhs.accScale;
			return *this;
		}
	};
};


#endif