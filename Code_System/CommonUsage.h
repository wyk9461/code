#ifndef _COMMONUSAGE_H_
#define _COMMONUSAGE_H_


namespace CommonUsage
{
	// Description : 
	//		Get the nearest entity around a position within an area
	//	Arguments :
	//		pos - the position
	//		radius - area radius
	//		entitytype - which entitytype do we want to get(eg: ent_rigid)
	EntityId GetNerestEntityIdInBox(Vec3 pos, float radius, int entitytype);

	void CreateExplosion(EntityId shooterId, Vec3 pos, float damage, float radius, float pressure, unsigned int HitType = CGameRules::EHitType::Explosion, char *effectName = "explosions.Grenade_SCAR.character");

	void CreateQueueExplosion(EntityId shooterId, Vec3 pos, float damage, float radius, float pressure, unsigned int HitType = CGameRules::EHitType::Explosion, 
							char *effectName = "explosions.Grenade_SCAR.character");

	Vec3 MoveAroundPoint(IEntity *pEntity, Vec3 pos, float radius, float angle, float offset);

	// Description : 
	//		Move entity to an exact position
	//	Arguments :
	//		pEntity - Entity to move
	//		Destination - Where to go
	//		speed - Moving Speed(m/s)
	//		stopDistance - What distance between entity and destination is representing "Arrive".
	//					   (the value must >0)
	// Return Value:
	//     True if entity have arrived.else false.
	const bool MoveToPosition(IEntity *pEntity, Vec3 Destination, float speed, float stopDistance);
}

class IdPool
{
public:
	IdPool(){}
	IdPool(int _max)
		:	maxIdAmount(_max)
		,	curId(1)
		,	curIdAmount(0){}
	~IdPool()
	{
		Pool.clear();
		std::set<unsigned int>().swap(Pool);
		std::set<unsigned int>().swap(ReterivedIdPool);
	}

	const unsigned int AssignId();
	const unsigned int AssignSpecialId(unsigned int Id);
	const bool CheckIdUsable(unsigned int Id);
	const bool RecoveryId(unsigned int Id);
	ILINE void SetMaxIdAmount(unsigned int _max){maxIdAmount = _max;}
private:
	unsigned int maxIdAmount;
	unsigned int curIdAmount;
	int curId;
	std::set<unsigned int> Pool;
	std::set<unsigned int> ReterivedIdPool;
};


#endif