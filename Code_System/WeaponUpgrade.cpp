#include "StdAfx.h"
#include "WeaponUpgrade.h"
#include "WeaponSharedParams.h"
#include "FireMode.h"

WeaponUpgrade::WeaponUpgrade()
{
	Player_Profile = gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/WeaponUpgradeState.xml");
	UpgradeSetting = gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/WeaponUpgradeSetting.xml");
	curItem = NULL;
	Init();
}

void WeaponUpgrade::Init()
{
	if(Player_Profile)
	{
		const int max_child = Player_Profile->getChildCount();
		for( int i = 0;i < max_child;++i )
		{
			XmlNodeRef WeaponProfile=Player_Profile->getChild(i);
			XmlString WeaponName=WeaponProfile->getTag();
			IEntityClass *pClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(WeaponName);
			if(pClass)
			{
				WeaponUpgradeState new_state;
				WeaponProfile->getChild(0)->getAttr("value",new_state.level);
				WeaponProfile->getChild(1)->getAttr("value",new_state.cur_exp);
				WeaponProfile->getChild(2)->getAttr("value",new_state.next_exp);
				WeaponProfile->getChild(3)->getAttr("value",new_state.bonus.DamageBonus);
				WeaponProfile->getChild(4)->getAttr("value",new_state.bonus.FireRateBonus);
				WeaponProfile->getChild(5)->getAttr("value",new_state.bonus.ClipSizeBonus);

				XmlNodeRef Setting=UpgradeSetting->findChild(WeaponName);
				if(Setting)
				{
					XmlNodeRef Levels = Setting->getChild(new_state.level-1);
					if(Levels)
					{
						XmlNodeRef Params = Levels->getChild(0);
						Params->getAttr("damageBonus",new_state.bonus.DamageBonus);
						Params->getAttr("firerateBonus",new_state.bonus.FireRateBonus);
						Params->getAttr("clipsizeBonus",new_state.bonus.ClipSizeBonus);
						wusMap.insert(std::map<IEntityClass*,WeaponUpgradeState>::value_type(pClass,new_state));
					}
				}
			}
		}
	}
}

const bool WeaponUpgrade::Update()
{
	if(curItem)
	{
		WeaponUpgradeState &cur_state=wusMap[curItem];
		if(cur_state.cur_exp >= cur_state.next_exp)
		{
			cur_state.cur_exp=cur_state.cur_exp-cur_state.next_exp;
			++cur_state.level;
			//cur_state.next_exp*=2;		//Beware to change.May use an Xml to decide the needed exp.
			UpdateProfile(curItem,cur_state);
			if(cur_state.cur_exp >= cur_state.next_exp)
				Update();
			return true;
		}
		return false;
	}
	return false;
}

void WeaponUpgrade::UpdateProfile(IEntityClass *weaponclass , WeaponUpgradeState &cur_state)
{
	XmlNodeRef WeaponProfile=Player_Profile->findChild(weaponclass->GetName());
	XmlNodeRef Setting=UpgradeSetting->findChild("WeaponName");
	if(Setting && WeaponProfile)
	{
		XmlNodeRef Levels = Setting->getChild(cur_state.level-1)->getChild(0);
		if(Levels)
		{
			XmlNodeRef Params = Levels->getChild(0);
			int next_exp;
			float new_damage_bonus;
			float new_rate_bonus;
			float new_clip_bonus;

			Params->getAttr("nextExp",next_exp);
			Params->getAttr("damageBonus",new_damage_bonus);
			Params->getAttr("firerateBonus",new_rate_bonus);
			Params->getAttr("clipsizeBonus",new_clip_bonus);

			WeaponProfile->getChild(0)->setAttr("value",cur_state.level);
			WeaponProfile->getChild(1)->setAttr("value",cur_state.cur_exp);
			WeaponProfile->getChild(2)->setAttr("value",next_exp);
			WeaponProfile->getChild(3)->setAttr("value",new_damage_bonus);
			WeaponProfile->getChild(4)->setAttr("value",new_rate_bonus);
			WeaponProfile->getChild(5)->setAttr("value",new_clip_bonus);

			Player_Profile->saveToFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/WeaponUpgradeState.xml");
			Player_Profile = gEnv->pSystem->LoadXmlFromFile(PathUtil::GetGameFolder() + "/Scripts/GameRules/WeaponUpgradeState.xml");

			CActor *pActor = (CActor*)g_pGame->GetIGameFramework()->GetClientActor();
			CWeapon *pWeapon = static_cast<CWeapon*>(pActor->GetCurrentItem(false));
			if(pWeapon)
			{
				g_pGame->GetModifySystem()->GetModifierManager()->ResetAllModifier();
				int fire_mode_index=pWeapon->GetCurrentFireMode();
				CFireMode* pFiremode = static_cast<CFireMode*>(pWeapon->GetFireMode(fire_mode_index));
				SFireModeParams* tFireModeParams=pFiremode->GetFireModeParams_Non_Const();
				SFireParams &pBasicfireParams=const_cast<SFireParams&>(tFireModeParams->fireparams);

				float trate= pBasicfireParams.rate* cur_state.bonus.FireRateBonus;
				float tclip_size= pBasicfireParams.clip_size * cur_state.bonus.ClipSizeBonus;
				float tdamage= pBasicfireParams.damage * cur_state.bonus.DamageBonus;

				pBasicfireParams.rate = static_cast<short>(trate);
				pBasicfireParams.clip_size = static_cast<short>(tclip_size);
				pBasicfireParams.damage = static_cast<int>(tdamage);
			}
		}
	}
}

const unsigned int WeaponUpgrade::GetWeaponLevel(IEntityClass* weaponclass)
{
	if(weaponclass && wusMap.count(weaponclass))
	{
		return wusMap[weaponclass].level;
	}
	return 0;
}

const unsigned int WeaponUpgrade::GetCurExp(IEntityClass* weaponclass)
{
	if(weaponclass && wusMap.count(weaponclass))
	{
		return wusMap[weaponclass].cur_exp;
	}
	return 0;
}

const unsigned int WeaponUpgrade::GetNextExp(IEntityClass* weaponclass)
{
	if(weaponclass && wusMap.count(weaponclass))
	{
		return wusMap[weaponclass].next_exp;
	}
	return 0;
}

void WeaponUpgrade::SetCurItem(IEntityClass* weaponclass)
{
	if(weaponclass && wusMap.count(weaponclass))
	{
		curItem=weaponclass;
	}
}

void WeaponUpgrade::GainExp(int g_exp)
{
	if(curItem)
	{
		wusMap[curItem].cur_exp+=g_exp;
		Update();
	}
}

const WeaponBonus *WeaponUpgrade::GetWeaponBonusByName(const string & weaponname)
{
	IEntityClass *pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass(weaponname);
	if(pClass)
	{
		if(wusMap.count(pClass))
		{
			return &wusMap[pClass].bonus;
		}
		else
		{
			XmlNodeRef WeaponProfile=Player_Profile->findChild(pClass->GetName());
			if(WeaponProfile)
			{
				WeaponProfile->getChild(3)->getAttr("value",tempBonus.DamageBonus);
				WeaponProfile->getChild(4)->getAttr("value",tempBonus.FireRateBonus);
				WeaponProfile->getChild(5)->getAttr("value",tempBonus.ClipSizeBonus);
				return &tempBonus;
			}
		}
	}
	return 0;
}