#ifndef _PERK_H_
#define _PERK_H_

#include <map>

enum ePerkType
{
	ePT_DamageReduction,
	ePT_DamageBonus,
	ePT_StabilityCostReduction,
	ePT_CoolDownReduction,
	ePT_MoveSpeedUp,
	ePT_SpawnTime,
	ePT_DeleteTime,
	ePT_FastSpawn,
	ePT_FastDelete,
	ePT_MoreMetaData,
	ePT_Last,
};

struct Perk
{
	int Type;
	double EffectScale;
	std::string Desc;
	virtual void Active(){};
	virtual void DeActive(){};
	virtual void GenerateDesc(){};
	Perk()
	:	Type(-1)
	,	EffectScale(0){Desc.reserve(30);}

	Perk(int type)
		:	Type(type)
		,	EffectScale(0){Desc.reserve(30);}

	Perk(int type, float effect)
	:	Type(type)
	,	EffectScale(effect){Desc.reserve(30);}
};

#define MK_PERK(name)							\
struct name##Perk : public Perk					\
{												\
	name##Perk()								\
	:	Perk(ePT_##name){}						\
	name##Perk(float effect)					\
	:	Perk(ePT_##name, effect){}				\
	virtual void Active();						\
	virtual void DeActive();					\
	virtual void GenerateDesc();				\
};												

MK_PERK(DamageReduction);
MK_PERK(DamageBonus);
MK_PERK(StabilityCostReduction);
MK_PERK(CoolDownReduction);
MK_PERK(MoveSpeedUp);
MK_PERK(SpawnTime);
MK_PERK(DeleteTime);
MK_PERK(FastSpawn);
MK_PERK(FastDelete);
MK_PERK(MoreMetaData);


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
struct PerkEffect
{
	double DamageReductionScale;
	double DamageBonusScale;
	double StabilityCostReductionScale;
	double CoolDownReductionScale;
	double MoveSpeedUpScale;
	double SpawnTimeScale;
	double DeleteTimeScale;
	double FastSpawnScale;
	double FastDeleteScale;
	double MoreMataDataScale;
};

struct PossibilityRange
{
	float RangeFloor;
	float RangeCeiling;
	const bool Within(float cmp_value)
	{
		if(cmp_value >= RangeFloor && cmp_value < RangeCeiling)
			return true;
		return false;
	}

	float GetRange()
	{
		return RangeCeiling - RangeFloor;
	}

	PossibilityRange()
	:	RangeFloor(0.0f)
	,	RangeCeiling(0.0f){}

	PossibilityRange(const PossibilityRange &rhs)
	{
		RangeFloor = rhs.RangeFloor;
		RangeCeiling = rhs.RangeCeiling;
	}

	PossibilityRange &operator=(const PossibilityRange& rhs)
	{
		if(this == &rhs)
			return *this;
		RangeFloor = rhs.RangeFloor;
		RangeCeiling = rhs.RangeCeiling;
		return *this;
	}
};

class CPerkMenuListener : public IUIElementEventListener
{
public:
	virtual void OnUIEvent( IUIElement* pSender, const SUIEventDesc& event, const SUIArguments& args );
};

class PerkManager
{
public:
	static PerkManager &Instance(){static PerkManager instance;return instance;}
	void AddPerkToList(int PerkType, Perk *new_Perk);
	void AddActivePerk(Perk *new_perk);
	void ClearPerk();

	const PerkEffect &GetPerkEffect(){return CurPerkEffect;}
	void GenerateRandomPerks();
	void UpdatePossibility(int type, float new_poss);
	float GetPossibility(int type){return Possibilities[type].GetRange();}
	void SelectPerk(int type);
	void HandleMenu();

	inline void SetCurDamageReduction(double scale){CurPerkEffect.DamageReductionScale = scale;}
	inline void SetCurDamageBonus(double scale){CurPerkEffect.DamageBonusScale = scale;}
	inline void SetCurStabilityCostReduction(double scale){CurPerkEffect.StabilityCostReductionScale = scale;}
	inline void SetCurCoolDownReduction(double scale){CurPerkEffect.CoolDownReductionScale = scale;}
	inline void SetCurMoveSpeedUp(double scale){CurPerkEffect.MoveSpeedUpScale = scale;}
	inline void SetCurSpawnTimeScale(double scale){CurPerkEffect.SpawnTimeScale = scale;}
	inline void SetCurDeleteTimeScale(double scale){CurPerkEffect.DeleteTimeScale = scale;}
	inline void SetCurFastSpawnScale(double scale){CurPerkEffect.FastSpawnScale = scale;}
	inline void SetCurFastDeleteScale(double scale){CurPerkEffect.FastDeleteScale = scale;}
	inline void SetCurMoreMateDataScale(double scale){CurPerkEffect.MoreMataDataScale = scale;}

	inline float GetBonusMoveSpeed(){return static_cast<float>(1*(1+CurPerkEffect.MoveSpeedUpScale));}
private:
	PerkManager()
	:	pElement(NULL){InitPossibilities();}
	std::map<int, Perk*> ActivatedPerks;
	std::map<int, Perk*> RandomPerks;
	std::map<int,PossibilityRange> Possibilities;
	std::map<int,float> AppearCounts;
	std::vector<float> DiffCounts;
	PerkEffect CurPerkEffect;
	IUIElement *pElement;
	CPerkMenuListener UIListener;
	float MaxAppearance;
	float TotalDiff;

	void InitPossibilities();
	void UpdatePossibilities();
	void ActivePerk(int type);
	void OpenMenu();
	void CloseMenu();
	void UIAddPerk();
};

#endif