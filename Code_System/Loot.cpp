#include "StdAfx.h"
#include "Code_System/Loot.h"
#include "EntityUtility/EntityEffects.h"
#include "EntityUtility/EntityEffectsCloak.h"
#include "AbilityModule/BaseAbility.h"
#include "PlayerInventory.h"
#include "Code_System/MicroSystem/EnemyScan.h"

DropParams& DropParams::operator=(const DropParams & dp)
{
	if(this == &dp)
		return *this;
	money_num=dp.money_num;
	health_num=dp.health_num;
	stability_num=dp.stability_num;
	ammo_num=dp.ammo_num;
	AmmoClass=dp.AmmoClass;
	data_num=dp.data_num;
	pEffect=dp.pEffect;
	timer=dp.timer;
	maxTime=dp.maxTime;
	return *this;
}

//------------------------------------------------------------------------
LootSystem::LootSystem()
{
	Money_Drop_Probability=80;
	Health_Drop_Pobability=50;
	Stability_Drop_Probability=98;
	Ammo_Drop_Stability=40;
	Data_Drop_Probability=80;
	Acessoris_Drop_Probability=1;
	MaxProbability=100;
	AbilityEnhance_Drop_Probability = 100;
	pWorld=gEnv->pPhysicalWorld;
	hits=0;
	query_flag=ent_rigid | ent_sleeping_rigid | ent_living | ent_independent;
	pEntity=NULL;
	pEffect=NULL;
	Is_Scanning=false;
	Scaned_Once=false;
	After_Scanning=false;
	once=false;
	isBody=false;
	max_pending_time = 1.0;
	tick_timer = 0.0;
	pElement = NULL;
}

//------------------------------------------------------------------------
LootSystem::~LootSystem()	
{

}

//------------------------------------------------------------------------
void LootSystem::Update(float frametime)
{
	/*EntityId newEntityId=gEnv->pEntitySystem->ReserveUnknownEntityId();
	if(newEntityId == 62394)
		CryLogAlways("LaLaLa");*/
	IActor *pSelfActor=g_pGame->GetIGameFramework()->GetClientActor();
	if(pSelfActor)
	{
		//Init RayCast
		pRenderer=gEnv->pRenderer;
		IEntity *pSelf=pSelfActor->GetEntity();
		IMovementController *pMC=pSelfActor->GetMovementController();
		SMovementState info;
		pMC->GetMovementState(info);
		int rayFlags(rwi_stop_at_pierceable|rwi_colltype_any);
		IPhysicalEntity *pSelfPhysics=pSelf->GetPhysics();
		hits = pWorld->RayWorldIntersection(info.eyePosition,info.eyeDirection*10.f,query_flag,rayFlags,&hit,1,&pSelfPhysics,1);

		//If the entity is a body
		if(hit.pCollider && !After_Scanning)
		{
			pEntity=(IEntity*)hit.pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);
			if(pEntity)
			{
				pActor=g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());
				if(pActor && pActor->IsDead())
					isBody=true;
				else
					isBody=false;
			}
		}
		else
		{
			isBody=false;
			pEntity=NULL;
		}

		//Begin Scanning
		if(Is_Scanning  && !After_Scanning)
		{
			if(isBody && pEntity)
			{
				Id=pEntity->GetId();
				if(Drops.count(pEntity->GetId()))    
				{
					if(Drops[Id].timer<Drops[Id].maxTime)
					{
						Drops[Id].timer+=frametime;
						pRenderer->DrawLabel(pEntity->GetPos(),1.5f,"Scanning...%.0f%%",Drops[Id].percentage);    //Will use Flash later
						Drops[Id].percentage+=100/1*frametime;
					}
					else
						After_Scanning=true;
				}
			}
		}

		//Hide the body and spawn particles after scanning
		if(After_Scanning)
		{
			if(Id)
			{
				if(!pEffect)
				{
					//Spawn Particles
					SEntitySpawnParams eParams;
					eParams.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
					eParams.sName="HidingEffect";
					eParams.vScale=Vec3(1,1,1);
					eParams.vPosition=pEntity->GetPos();
					pEffect=gEnv->pEntitySystem->SpawnEntity(eParams);
					SmartScriptTable root=pEffect->GetScriptTable();
					SmartScriptTable props;
					root->GetValue("Properties",props);
					props->SetValue("ParticleEffect","Code_System.Loot.Hiding");
				}
				if(!once)
				{
					IEntity *pEntity=gEnv->pEntitySystem->GetEntity(Id);
					SmartScriptTable root=pEffect->GetScriptTable();
					pEntity->AttachChild(pEffect);
					Script::CallMethod(root,"OnReset");
					EnemyScan::theScan().MarkEntityForRemove(Id);
					GetLootItems(Drops[Id]);
					once=true;
				}
				/*if(Drops[Id].hidetimer<=3.0f)
				{
					IEntity *pEntity=gEnv->pEntitySystem->GetEntity(Id);
					if(pEntity)
					{
						Drops[Id].hidetimer+=frametime;
						IEntityRenderProxy *pRenderProxy=(IEntityRenderProxy*)pEntity->GetProxy(ENTITY_PROXY_RENDER);
						pRenderProxy->SetOpacity(Drops[Id].opacity);
						pRenderProxy->SetMaterialLayersMask(MTL_LAYER_CLOAK);
						Drops[Id].opacity-=100/1*frametime;
					}
				}*/
				if(Drops.count(Id))
				{
					EntityEffects::Cloak::CloakEntity(pEntity,true,true,1,false,0,true);
					Drops[Id].hidetimer+=frametime;
				}
				if(Drops[Id].hidetimer>3.0f)
				{
					Drops.erase(Id);
					pEffect->DetachAll();
					//pEntity->SetUpdatePolicy(ENTITY_UPDATE_NEVER);
					IEntity *tEntity=gEnv->pEntitySystem->GetEntity(Id);
					if(tEntity)
					{
						//gEnv->pEntitySystem->RemoveEntity(Id);
						SEntitySpawnParams params;
						tEntity->SetPos(Vec3(0,0,0));
						tEntity->EnablePhysics(false);
						//pEntity->Hide(true);
						pEntity=NULL;
						gEnv->pEntitySystem->RemoveEntity(pEffect->GetId());
						pEffect=NULL;
						After_Scanning=false;
						once=false;
						isBody=false;
					}
				}
			}
		}

		//If we stop scanning before the scan is over 
		if(Scaned_Once && !After_Scanning)
		{
			if(pEntity)
			{
				if(Drops.count(pEntity->GetId()) && Drops[pEntity->GetId()].timer != 0)
				{
					Drops[pEntity->GetId()].timer=0;
					Drops[pEntity->GetId()].percentage=0;
				}
				pEntity=NULL;
			}
			Scaned_Once=false;
		}
	}

	UpdatePendingQueue(frametime);
}

//------------------------------------------------------------------------
void LootSystem::ProcessLoot(CActor *pActor)
{
	player=g_pGame->GetIGameFramework()->GetClientActor();
	int t=cry_rand() % MaxProbability;
	SmartScriptTable root=pActor->GetEntity()->GetScriptTable();
	SmartScriptTable props;
	root->GetValue("Properties",props);
	int level;
	props->GetValue("Level",level);
	if(!Drops.count(pActor->GetEntity()->GetId()))
	{
		DropParams param;
		if(t<Money_Drop_Probability)
		{
			DropMoney(level,param);
			t=cry_rand() % 100;
		}
		if(t<Health_Drop_Pobability)
		{
			DropHealth(level,param);
			t=cry_rand() % 100;
		}
		if(t<Stability_Drop_Probability)
		{
			DropStability(level,param);
			t=cry_rand() % 100;
		}
		if(t<Ammo_Drop_Stability)
		{
			DropAmmo(level,pActor,param);
			t=cry_rand() % 100;
		}
		if(t<Data_Drop_Probability)
		{
			DropData(level,param);
			t=cry_rand() % 100;
		}
		Drops[pActor->GetEntity()->GetId()]=param;
	}
}

//------------------------------------------------------------------------
void LootSystem::DropMoney(int level,DropParams &param)
{
	int money(0);
	money=(int)(level*20+cry_rand()%10);
	param.money_num=money;
}

//------------------------------------------------------------------------
void LootSystem::DropHealth(int level,DropParams &param)
{
	param.health_num=static_cast<float>(level*30+50);
}

//------------------------------------------------------------------------
void LootSystem::DropStability(int level,DropParams &param)
{
	param.stability_num=(int)(cry_frand()*100*level);
}

//------------------------------------------------------------------------
void LootSystem::DropAmmo(int level,CActor *pActor,DropParams &param)
{
	IItem *pItem=pActor->GetCurrentItem(false);
	if(pItem)
	{
		param.AmmoClass=pActor->GetInventory()->GetAmmoType(0);
		if(!param.AmmoClass)
			return;
		param.ammo_num=(int)(cry_rand()%30*sqrt(level));
	}
}

//------------------------------------------------------------------------
void LootSystem::DropData(int level,DropParams &param)
{
	param.data_num=(int)(cry_rand()%2*level);
}

//------------------------------------------------------------------------
void LootSystem::GetLootItems(DropParams &param)
{
	if(param.money_num!=0)
	{
		g_pGame->GetMoneySystem()->MoneyGain(param.money_num);
	}

	if(param.stability_num!=0)
	{
		g_pGame->GetWorldStability()->StabilityGain(param.stability_num);
		std::string message;
		message.reserve(30);
		message = "Stability +";
		char *temp = new char[10];
		itoa(param.stability_num,temp,10);
		message += temp;
		delete temp;
		PendingMessage.push_back(message);
		if(PendingMessage.size() == 1)
			Pending_Iter = PendingMessage.begin();
	}

	if(param.health_num != 0)
	{
		float curhealth=g_pGame->GetIGameFramework()->GetClientActor()->GetHealth();
		g_pGame->GetIGameFramework()->GetClientActor()->SetHealth(curhealth+param.health_num);
		std::string message;
		message.reserve(30);
		message = "Health +";
		char *temp = new char[10];
		itoa((int)param.health_num,temp,10);
		message += temp;
		delete temp;
		PendingMessage.push_back(message);
		if(PendingMessage.size() == 1)
			Pending_Iter = PendingMessage.begin();
	}

	if(param.ammo_num != 0)
	{
		CActor *player=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
		IInventory *pInventory=player->GetInventory();
		int curAmmo=pInventory->GetAmmoCount(param.AmmoClass);
		pInventory->SetAmmoCount(param.AmmoClass,curAmmo+param.ammo_num);
	}

	int t=cry_rand() % MaxProbability;

	if(t < AbilityEnhance_Drop_Probability)
	{
		AbilityEnhancement *new_enhance = new AbilityEnhancement(0.1,0,0,0,0,"Cool_Down_Enhance",1,1000);
		PlayerInventory *pInventory = g_pGame->GetPlayerInventory();
		if(pInventory)
		{
			pInventory->AddNewInventory(new_enhance);
		}
	}
}

void LootSystem::UpdatePendingQueue(float frameTime)
{
	if(PendingMessage.empty())
		return;

	if(tick_timer < max_pending_time)
		tick_timer += frameTime;
	else
	{
		tick_timer = 0;
		if(!pElement)
			pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
		if(pElement)
		{
			SUIArguments args;
			args.AddArgument(Pending_Iter->c_str());
			args.AddArgument(float(1.5));
			pElement->CallFunction("AddMessage",args);
		}
		++Pending_Iter;
	}
	if(Pending_Iter == PendingMessage.end())
		PendingMessage.clear();
}