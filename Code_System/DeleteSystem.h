#ifndef _DELETESYSTEM_H_
#define _DELETESYSTEM_H_

#include "Code_System\EntityParamsUtils.h"

class DeleteSystem
{
private:
	struct DeleteParams
	{
		IEntity *DEntity;
		float deleteTime;	//Time needed to delete this entity
		float timer;
		float rate;
		int cost;
		IMaterial *deletemtl;
		float amount;	//Glow amount
		bool available;	//If this entity is being deleting
		bool AI;
		bool IsPlayerEntityDelete;		//if this entity is created by player.
		DeleteParams()
			:	DEntity(NULL)
			,	deleteTime(0)
			,	timer(0)
			,	rate(0)
			,	cost(0)
			,	deletemtl(NULL)
			,	amount(10)
			,	available(true)
			,	AI(false)
			,	IsPlayerEntityDelete(false){}
	};

	static const int maxSlot=30;
	DeleteParams DP[maxSlot];
	short curindex;
	short Maxnumber;
	bool OnDelete;
	IMaterial *DefaultDeleteMtl;
	std::vector<SEntitySpawnParams> DeleteRecoveryInfo;

	EntityClassPreLoad preLoad;

	IEntityClass *m_RobotClass;
	void UpdateDP(const int i,const float frameTime);
	void UpdateDPStep_1(const int i,const float frameTime);
	void UpdateDPStep_2(const int i,const float frameTime);
	void UpdateDPStep_3(const int i,const float frameTime);
	void ProcessBuff(IEntity *pEntity);
	// Description :
	//		Reset the DP member.
	//		Otherwise something bad will happen.
	//	Arguments : 
	//		i - index of DP member
	void ClearDP(const int i);

	// Description :
	//		Check if an entity is being deleting.
	//		If this entity is in the DP array,we know it is being deleting.
	//	Arguments : 
	//		pEntity - checked Entity.
	const bool IsDeleting(IEntity *pEntity);

	// Description :
	//		Get the delete information(cost and time).
	//		Cost and time are set in the entity's Lua file.
	void ProcessScript(const bool IsSystemDeleting);

	// Description :
	//		Retrieves free slot index in DP for new entity to be deleted.
	//		Each entity will take one free slot.
	//		After they have been deleted,their slot will be free again.
	// Returns:
	//     The free slot index.
	const int FindAvailable(const bool IsSystemDeleting = false);


	// Description :
	//		Checks if there are entities being deleting.
	const bool NeedDelete();


	inline  IMaterial* GetDefDelMtl(){return DefaultDeleteMtl;}

	std::set<EntityId> to_delete_list;
public:
	DeleteSystem();
	~DeleteSystem();
	void Update(float &frameTime);

	// Description :
	//		Get the information of the entity which will be deleted later
	//	Arguments : 
	//		DEntity - The entity which will be deleted later
	void ProcessDeleteEntity(IEntity *DEntity,const bool IsSystemDeleting = false);

	// Description :
	//		Get the information of the entity which will be deleted later
	//	Arguments : 
	//		DEntity - The entity which will be deleted later
	void ProcessDeleteEntityForFastDelete(IEntity *DEntity);

	void AddToDeleteRecovery(IEntity *pEntity, bool isSystemDeleting);
	std::vector<SEntitySpawnParams> &GetDeleteRecoveryInfo(){return DeleteRecoveryInfo;}

	void ClearDeleteList(bool force = false);
};



#endif // !_DELETESYSTEM_H_
