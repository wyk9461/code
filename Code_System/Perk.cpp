#include "StdAfx.h"
#include "Perk.h"
#include "GameActions.h"

//----------------------------------------------------------------------------
void DamageReductionPerk::Active()
{
	PerkManager::Instance().SetCurDamageReduction(EffectScale);
}

void DamageReductionPerk::DeActive()
{
	PerkManager::Instance().SetCurDamageReduction(0);
}

void DamageReductionPerk::GenerateDesc()
{
	char temp[30];
	sprintf(temp,"%.2f",EffectScale*100);
	std::string combine_temp(temp);
	Desc = "Armor increased by ";
	Desc += combine_temp;
	Desc += "%";
}

//----------------------------------------------------------------------------
void DamageBonusPerk::Active()
{
	PerkManager::Instance().SetCurDamageBonus(EffectScale);
}

void DamageBonusPerk::DeActive()
{
	PerkManager::Instance().SetCurDamageBonus(0);
}

void DamageBonusPerk::GenerateDesc()
{
	char temp[30];
	sprintf(temp,"%.2f",EffectScale*100);
	std::string combine_temp(temp);
	Desc = "Damage increase by ";
	Desc += combine_temp;
	Desc += "%";
}

//----------------------------------------------------------------------------
void StabilityCostReductionPerk::Active()
{
	PerkManager::Instance().SetCurStabilityCostReduction(EffectScale);
}

void StabilityCostReductionPerk::DeActive()
{
	PerkManager::Instance().SetCurStabilityCostReduction(0);
}

void StabilityCostReductionPerk::GenerateDesc()
{
	char temp[30];
	sprintf(temp,"%.2f",EffectScale*100);
	std::string combine_temp(temp);
	Desc = "STA cost decreased by ";
	Desc += combine_temp;
	Desc += "%";
}

//----------------------------------------------------------------------------
void CoolDownReductionPerk::Active()
{
	PerkManager::Instance().SetCurCoolDownReduction(EffectScale);
}

void CoolDownReductionPerk::DeActive()
{
	PerkManager::Instance().SetCurCoolDownReduction(0);
}

void CoolDownReductionPerk::GenerateDesc()
{
	char temp[30];
	sprintf(temp,"%.2f",EffectScale*100);
	std::string combine_temp(temp);
	Desc = "Ability CD decreased by ";
	Desc += combine_temp;
	Desc += "%";
}

//----------------------------------------------------------------------------
void MoveSpeedUpPerk::Active()
{
	PerkManager::Instance().SetCurMoveSpeedUp(EffectScale);
	const PerkEffect &rpEffect = PerkManager::Instance().GetPerkEffect();
	CActor* pActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(g_pGame->GetClientActorId());
	pActor->SetSpeedMultipler(SActorParams::eSMR_Internal, PerkManager::Instance().GetBonusMoveSpeed());
	pActor->SetSpeedMultipler(SActorParams::eSMR_Item, PerkManager::Instance().GetBonusMoveSpeed());
	pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules, PerkManager::Instance().GetBonusMoveSpeed());
}

void MoveSpeedUpPerk::DeActive()
{
	PerkManager::Instance().SetCurMoveSpeedUp(0);
}

void MoveSpeedUpPerk::GenerateDesc()
{
	char temp[30];
	sprintf(temp,"%.2f",EffectScale*100);
	std::string combine_temp(temp);
	Desc = "Move speed increased by ";
	Desc += combine_temp;
	Desc += "%";
}


//----------------------------------------------------------------------------
void SpawnTimePerk::Active()
{
	PerkManager::Instance().SetCurSpawnTimeScale(EffectScale);
}

void SpawnTimePerk::DeActive()
{
	PerkManager::Instance().SetCurSpawnTimeScale(0);
}

void SpawnTimePerk::GenerateDesc()
{
	char temp[30];
	sprintf(temp,"%.2f",EffectScale*100);
	std::string combine_temp(temp);
	Desc = "Spawn time decreased by ";
	Desc += combine_temp;
	Desc += "%";
}


//----------------------------------------------------------------------------
void DeleteTimePerk::Active()
{
	PerkManager::Instance().SetCurDeleteTimeScale(EffectScale);
}

void DeleteTimePerk::DeActive()
{
	PerkManager::Instance().SetCurDeleteTimeScale(0);
}

void DeleteTimePerk::GenerateDesc()
{
	char temp[30];
	sprintf(temp,"%.2f",EffectScale*100);
	std::string combine_temp(temp);
	Desc = "Delete time decreased by ";
	Desc += combine_temp;
	Desc += "%";
}


//----------------------------------------------------------------------------
void MoreMetaDataPerk::Active()
{
	PerkManager::Instance().SetCurMoreMateDataScale(EffectScale);
}

void MoreMetaDataPerk::DeActive()
{
	PerkManager::Instance().SetCurMoreMateDataScale(0);
}

void MoreMetaDataPerk::GenerateDesc()
{
	char temp[30];
	sprintf(temp,"%.2f",EffectScale*100);
	std::string combine_temp(temp);
	Desc = "MetaData collected increased by ";
	Desc += combine_temp;
	Desc += "%";
}

//----------------------------------------------------------------------------
void FastSpawnPerk::Active()
{
	PerkManager::Instance().SetCurFastSpawnScale(EffectScale);
}

void FastSpawnPerk::DeActive()
{
	PerkManager::Instance().SetCurFastSpawnScale(0);
}

void FastSpawnPerk::GenerateDesc()
{
	char temp[30];
	sprintf(temp,"%.2f",EffectScale*100);
	std::string combine_temp(temp);
	Desc = "Spawn speed increased by ";
	Desc += combine_temp;
	Desc += "%";
}


//----------------------------------------------------------------------------
void FastDeletePerk::Active()
{
	PerkManager::Instance().SetCurFastDeleteScale(EffectScale);
}

void FastDeletePerk::DeActive()
{
	PerkManager::Instance().SetCurFastDeleteScale(0);
}

void FastDeletePerk::GenerateDesc()
{
	char temp[30];
	sprintf(temp,"%.2f",EffectScale*100);
	std::string combine_temp(temp);
	Desc = "Delete speed increased by ";
	Desc += combine_temp;
	Desc += "%";
}


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void PerkManager::AddPerkToList(int PerkType, Perk *new_Perk)
{
	RandomPerks[PerkType] = new_Perk;
}

void PerkManager::AddActivePerk(Perk *new_perk)
{
	int type = new_perk->Type;
	if(ActivatedPerks.count(type))
	{
		ActivatedPerks[type]->EffectScale += new_perk->EffectScale;
		ActivePerk(type);
	}
	else
	{
		ActivatedPerks.insert(std::map<int,Perk*>::value_type(type,new_perk));
		ActivePerk(type);
	}
}

void PerkManager::ClearPerk()
{
	std::map<int,Perk*>::iterator iter = ActivatedPerks.begin();
	std::map<int,Perk*>::iterator end = ActivatedPerks.end();
	while(iter != end)
	{
		iter->second->DeActive();
		ActivatedPerks.erase(iter++);
	}
}

void PerkManager::ActivePerk(int type)
{
	ActivatedPerks[type]->Active();
}

void PerkManager::InitPossibilities()
{
	const unsigned int MaxCount = ePT_Last;
	const float defaultPossibility = 1.0 / ePT_Last;		//Pay attention to 1.0f
	for(int i=0; i<MaxCount; ++i)
	{
		PossibilityRange range;
		range.RangeFloor = i * defaultPossibility;
		range.RangeCeiling = range.RangeFloor + defaultPossibility;
		Possibilities.insert(std::map<int,PossibilityRange>::value_type(i,range));
		AppearCounts.insert(std::map<int,float>::value_type(i,0.f));
	}
	MaxAppearance = 0;
	DiffCounts.reserve(30);

	std::map<int,float>::iterator CounterIter = AppearCounts.begin();
	std::map<int,float>::iterator CounterEnd = AppearCounts.end();
	TotalDiff = 0;
	while(CounterIter != CounterEnd)
	{
		float diffCount(MaxAppearance - CounterIter->second + 1);
		DiffCounts.push_back(diffCount);
		TotalDiff += diffCount;
		++CounterIter;
	}

#define CODE_TEST
#ifdef CODE_TEST
	//UpdatePossibility(0,1);
	UpdatePossibility(1,0.4);
#endif // CODE_DEBUG

}

void PerkManager::UpdatePossibilities()
{
	std::map<int,float>::iterator CounterIter = AppearCounts.begin();
	std::map<int,float>::iterator CounterEnd = AppearCounts.end();
	DiffCounts.clear();
	TotalDiff = 0;
	while(CounterIter != CounterEnd)
	{
		float diffCount(MaxAppearance - CounterIter->second + 1);
		DiffCounts.push_back(diffCount);
		TotalDiff += diffCount;
		++CounterIter;
	}

	float last_possibility = 0.f;
	for(int i = 0; i < DiffCounts.size(); ++i)
	{
		Possibilities[i].RangeFloor = last_possibility;
		Possibilities[i].RangeCeiling = Possibilities[i].RangeFloor + (float)(DiffCounts[i]) / TotalDiff;
		last_possibility = Possibilities[i].RangeCeiling;
	}
}

void PerkManager::UpdatePossibility(int type, float new_poss)
{
	if(Possibilities.count(type))
	{
		if(new_poss >= 1)
			new_poss = 0.99;
		if(new_poss <= 0)
			new_poss = 0.01;
		if(Possibilities[type].GetRange() < new_poss)
		{
			float change_count = (new_poss * TotalDiff - DiffCounts[type]) / (1 - new_poss);
			AppearCounts[type] -= change_count;
			if(AppearCounts[type] == 0.0f)
				AppearCounts[type] = 0.1;
			UpdatePossibilities();
		}
		else if(Possibilities[type].GetRange() > new_poss)
		{
			float change_count = -(new_poss * TotalDiff - DiffCounts[type]) / (1 - new_poss);
			AppearCounts[type] += change_count;
			if(AppearCounts[type] == 0.0f)
				AppearCounts[type] = 0.1;
			if(AppearCounts[type] > MaxAppearance)
				MaxAppearance = AppearCounts[type];
			UpdatePossibilities();
		}
		else
			return;
	}
}

void PerkManager::GenerateRandomPerks()
{
	std::map<int,Perk*>::iterator iter = RandomPerks.begin();
	std::map<int,Perk*>::iterator end = RandomPerks.end();
	while(iter != end)
	{
		delete iter->second;
		++iter;
	}

	RandomPerks.clear();
	for(int i = 0;i < 4;++i)
	{
		int Perk_Seed = ePT_Last;
		while(RandomPerks.count(Perk_Seed) || Perk_Seed == ePT_Last)
		{
			float Possibility_Seed = cry_frand();
			std::map<int,PossibilityRange>::iterator possibility_iter = Possibilities.begin();
			std::map<int,PossibilityRange>::iterator possibility_end = Possibilities.end();
			while(possibility_iter != possibility_end)
			{
				if(possibility_iter->second.Within(Possibility_Seed))
				{
					Perk_Seed = possibility_iter->first;
					break;
				}
				++possibility_iter;
			}
		}

		//May use UpdatePossibility to replace
		AppearCounts[Perk_Seed] += 1;
		if(AppearCounts[Perk_Seed] > MaxAppearance)
			MaxAppearance = AppearCounts[Perk_Seed];

		switch(Perk_Seed)
		{
		case ePT_DamageReduction:
			{
				DamageReductionPerk *NewRandomPerk = new DamageReductionPerk;
				NewRandomPerk->EffectScale = 0.005 + cry_frand() * 0.02 * Possibilities[ePT_DamageReduction].GetRange();
				NewRandomPerk->GenerateDesc();
				RandomPerks[ePT_DamageReduction] = NewRandomPerk;
				break;
			}
		case ePT_DamageBonus:
			{
				DamageBonusPerk *NewRandomPerk = new DamageBonusPerk;
				NewRandomPerk->EffectScale = 0.005 + cry_frand() * 0.02 * Possibilities[ePT_DamageBonus].GetRange();
				NewRandomPerk->GenerateDesc();
				RandomPerks[ePT_DamageBonus] = NewRandomPerk;
				break;
			}
		case ePT_StabilityCostReduction:
			{
				StabilityCostReductionPerk *NewRandomPerk = new StabilityCostReductionPerk;
				NewRandomPerk->EffectScale = 0.005 + cry_frand() * 0.02 * Possibilities[ePT_StabilityCostReduction].GetRange();
				NewRandomPerk->GenerateDesc();
				RandomPerks[ePT_StabilityCostReduction] = NewRandomPerk;
				break;
			}
		case ePT_CoolDownReduction:
			{
				CoolDownReductionPerk *NewRandomPerk = new CoolDownReductionPerk;
				NewRandomPerk->EffectScale = 0.005 + cry_frand() * 0.02 * Possibilities[ePT_CoolDownReduction].GetRange();
				NewRandomPerk->GenerateDesc();
				RandomPerks[ePT_CoolDownReduction] = NewRandomPerk;
				break;
			}
		case ePT_MoveSpeedUp:
			{
				MoveSpeedUpPerk *NewRandomPerk = new MoveSpeedUpPerk;
				NewRandomPerk->EffectScale = 0.005 + cry_frand() * 0.02 * Possibilities[ePT_MoveSpeedUp].GetRange();
				NewRandomPerk->GenerateDesc();
				RandomPerks[ePT_MoveSpeedUp] = NewRandomPerk;
				break;
			}
		case ePT_SpawnTime:
			{
				SpawnTimePerk *NewRandomPerk = new SpawnTimePerk;
				NewRandomPerk->EffectScale = 0.005 + cry_frand() * 0.02 * Possibilities[ePT_SpawnTime].GetRange();
				NewRandomPerk->GenerateDesc();
				RandomPerks[ePT_SpawnTime] = NewRandomPerk;
				break;
			}
		case ePT_DeleteTime:
			{
				DeleteTimePerk *NewRandomPerk = new DeleteTimePerk;
				NewRandomPerk->EffectScale = 0.005 + cry_frand() * 0.02 * Possibilities[ePT_DeleteTime].GetRange();
				NewRandomPerk->GenerateDesc();
				RandomPerks[ePT_DeleteTime] = NewRandomPerk;
				break;
			}
		case ePT_FastSpawn:
			{
				FastSpawnPerk *NewRandomPerk = new FastSpawnPerk;
				NewRandomPerk->EffectScale = 0.005 + cry_frand() * 0.02 * Possibilities[ePT_FastSpawn].GetRange();
				NewRandomPerk->GenerateDesc();
				RandomPerks[ePT_FastSpawn] = NewRandomPerk;
				break;
			}
		case ePT_FastDelete:
			{
				FastDeletePerk *NewRandomPerk = new FastDeletePerk;
				NewRandomPerk->EffectScale = 0.005 + cry_frand() * 0.02 * Possibilities[ePT_FastDelete].GetRange();
				NewRandomPerk->GenerateDesc();
				RandomPerks[ePT_FastDelete] = NewRandomPerk;
				break;
			}
		case ePT_MoreMetaData:
			{
				MoreMetaDataPerk *NewRandomPerk = new MoreMetaDataPerk;
				NewRandomPerk->EffectScale = 0.005 + cry_frand() * 0.02 * Possibilities[ePT_MoreMetaData].GetRange();
				NewRandomPerk->GenerateDesc();
				RandomPerks[ePT_MoreMetaData] = NewRandomPerk;
				break;
			}
		}
	}
	UpdatePossibilities();
}

void PerkManager::SelectPerk(int type)
{
	CRY_ASSERT_MESSAGE(RandomPerks.count(type),"Not in RandomPerk Cluster");
	AddActivePerk(RandomPerks[type]);
	RandomPerks.erase(type);
	float curPossibility = GetPossibility(type);
	curPossibility /= 2;
	UpdatePossibility(type,curPossibility);
	GenerateRandomPerks();
	UIAddPerk();
}

void PerkManager::HandleMenu()
{
	IUIElement *pElement = gEnv->pFlashUI->GetUIElement("PrototypePerk");
	if(pElement)
	{
		if(pElement->IsVisible())
			CloseMenu();
		else
			OpenMenu();
	}
}

void PerkManager::OpenMenu()
{
	if(!pElement)
		pElement = gEnv->pFlashUI->GetUIElement("PrototypePerk");
	if(pElement)
	{
		pElement->SetVisible(true);
		g_pGame->Actions().FilterNoMouse()->Enable(true);
		pElement->AddEventListener(&UIListener,"PerkMenu_Listener");
		UIAddPerk();
	}
}

void PerkManager::CloseMenu()
{
	if(!pElement)
		pElement = gEnv->pFlashUI->GetUIElement("PrototypePerk");
	if(pElement)
	{
		g_pGame->Actions().FilterNoMouse()->Enable(false);
		pElement->SetVisible(false);
		pElement->RemoveEventListener(&UIListener);
	}
}

void CPerkMenuListener::OnUIEvent( IUIElement* pSender, const SUIEventDesc& event, const SUIArguments& args )
{
	if (strcmp(event.sDisplayName, "PerkType") == 0 )
	{
		int FunctionIndex;
		args.GetArg( 0, FunctionIndex);
		PerkManager::Instance().SelectPerk(FunctionIndex);
	}
}

void PerkManager::UIAddPerk()
{
	if(!pElement)
		pElement = gEnv->pFlashUI->GetUIElement("PrototypePerk");
	if(pElement)
	{
		std::map<int,Perk*>::iterator iter = RandomPerks.begin();
		std::map<int,Perk*>::iterator end = RandomPerks.end();
		int count = 1;
		while(iter != end)
		{
			SUIArguments args;
			args.AddArgument(count);
			args.AddArgument(iter->second->Type);
			args.AddArgument(iter->second->Desc.c_str());
			pElement->CallFunction("AddPerk",args);
			++iter;
			++count;
		}
	}
}