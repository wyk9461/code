#ifndef _DISABLESYSTEM_H_
#define _DISABLESYSTEM_H_

#include "Game.h"
#include <map>
#include "IFlashPlayer.h"

class DisableSystem
{
	private:
		enum DisableMode
		{
			EDM_DisableLegs = 0,
			EDM_DisableArms,
			EDM_DisableEyes,
			EDM_DisableMind,
			EDM_ChangeMind,
			EDM_WeakLegs,
			EDM_WeakArms,
			EDM_DisableEngine,
			EDM_DisableAWheel,
			EDM_DisableDriver,
			EDM_LAST
		};
		enum TargetType
		{
			ETT_AI = 0,
			ETT_Vehicle,
			ETT_LAST
		};
		//Ray Cast and Common
		ray_hit hit;
		int hits;
		int query_flag;
		IPhysicalWorld *pWorld;
		IRenderer *pRenderer;
		//index
		int curindex;
		int Maxnumber;
		int MaxMode;
		uint8 DisableSlot[6];
		//System State Check
		bool StartFunction;
		string store_weapon_name;
		//Disable Time
		float DisableLegsTime;
		float DisableArmsTime;
		float DisableEyesTime;
		float DisableMindTime;
		float ChangeMindTime;
		float WeakLegsTime;
		float WeakArmsTime;
		float DisableEngineTime;
		float DisableAWheelTime;
		float DisableDriverTime;
		//Disable Entities Structure
		struct Disables
		{
			IEntity* DisableEntity;
			EntityId DriverID;
			uint8 type;
			uint8 mode;
			string className;
			float DisableTimer;
			float TotalTime;
			float DisableRate;
			float Progress;
			bool available;
			bool finished;
			bool firstset;
			bool locked;
			bool disabling;
			//Disable Params
			std::vector<EntityId>DropItems;
			int defViewFOV;
			int defSightRange;
			int defAccuracy;
			float defSpeedMulti;
			char* defBehaviorTree;
			char* defFaction;
			//Common
			float cost;
			Disables()
			:	DisableEntity(NULL)
			,	DriverID(0)
			,	type(0)
			,	mode(EDM_LAST)
			,	className("")
			,	DisableTimer(0)
			,	TotalTime(0)
			,	DisableRate(0)
			,	Progress(0)
			,	defViewFOV(60)
			,	defSightRange(150)
			,	defAccuracy(1)
			,	defSpeedMulti(1)
			,	finished(false)
			,	firstset(true)
			,	locked(false)
			,	disabling(false)
			,	available(true)
			,	defBehaviorTree("")
			,	defFaction(""){}
		};
		Disables DIS[11];

		//Disable Function
		void DisableLegs(const int i);
		void DisableArms(const int i);
		void DisableEyes(const int i);
		void WeakLegs(const int i);
		void WeakArms(const int i);
		void DisableMind(const int i);
		void ChangeMind(const int i);
		void DisableEngine(const int i);
		void DisableAWheel(const int i);
		void DisableDriver(const int i);

		void AfterDisableArms(const int i);
		void AfterDisableEyes(const int i);
		void AfterWeakLegs(const int i);
		void AfterWeakArms(const int i);
		void AfterDisableMind(const int i);
		void AfterChangeMind(const int i);
		void AfterDisableEngine(const int i);
		void AfterDisableDriver(const int i);

		float SetActiveTime(const int i);

		// Description :
		//		If current disable mode can apply on current entity.
		//	Arguments : 
		//		i - the member index of DIS.
		bool CompareType(const int i);

		const bool IsDisabled(IEntity *pEntity);
		int FindAvailable();

		// Description :
		//		Choose the entity type(AI or Vehicle)
		//	Arguments : 
		//		i - the member index of DIS.
		void ChooseTargetType(const int i);

		void PostUpdate(float frametime);
		void DrawDisableMode();
		void DrawLabel();
		void ClearDIS(const int index);
		void SetTotalTime(const int i);

		void ApplyChange(const int index);

		bool NeedPostUpdate();
		bool NeedLockLabel();

	public:
		//Common Function
		DisableSystem();
		std::map<EntityId,int> LockedEntity;
		~DisableSystem(){}
		void Update(float frametime);

		// Description :
		//		Get the information of the entity which will be disabled later
		//	Arguments : 
		//		DEntity - The entity which will be disabled later
		const bool ProcessDisableEntity(IEntity *DEntity);

		// Description :
		//		Set the disable mode,time and so on.
		//	Arguments : 
		//		SlotIndex - the disable slot(SlotIndex == EDM_LAST mean this slot is empty,therefore nothing will happen.)
		void PerApply(const int SlotIndex = EDM_LAST);

		// Description :
		//		Set the disable mode,time and so on via Disable Menu(UI).
		//	Arguments : 
		//		mode - the disable mode.
		// See also:
		//		PerApply.
		void PreApply_viaMenu(const int mode);

		//inline
		//Get
		inline const bool IsFunctionStarted(){return StartFunction;}
		inline const int GetMode(){return DIS[curindex].mode;}
		inline const int GetMaxMode(){return MaxMode;}		//Remove Later
		inline const int GetCurIndex(){return curindex;}
		//Set
		inline void SetStartFunction(const bool enable){StartFunction=enable;}
		inline void SetMode(const int i){DIS[curindex].mode=i;}
		inline void AddModeIndex(){++DIS[curindex].mode;}
		inline void MinusModeIndex(){--DIS[curindex].mode;}
};
#endif