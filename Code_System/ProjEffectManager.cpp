#include "StdAfx.h"
#include "ProjEffectManager.h"

ProjEffectManager::~ProjEffectManager()
{
	delete m_DataLeak;
	delete m_DataRedundance;
	delete m_InterfaceTrans;
	delete m_DataExplode;
	delete m_DataFreeze;
	delete m_DataBlock;
}


const bool ProjEffectManager::HasAdvancedEffect()
{
	if(HasDataExplodeEffect() || HasDataFreezeEffect() || HasDataBlockEffect()
		|| HasRedundanceLeakEffect() || HasTransLeakLeakEffect() || HasTransRedundanceEffect())
		return true;
	return false;
}

//------------------------------------------------------------------------
void ProjEffectManager::Update(const float &frameTime)
{
	if(m_DataLeak)
	{
		m_DataLeak->Update(frameTime);
		m_DataRedundance->Update(frameTime);
		m_InterfaceTrans->Update(frameTime);
		m_DataFreeze->Update(frameTime);
		m_DataBlock->Update(frameTime);
	}
}

//--------DataLeak--------//
void ProjEffectManager::Add_DataLeak_Effect()
{
	if(curProjEffects.empty())
		curProjEffects.insert(Data_Leak);
	else if(HasAdvancedEffect())
	{
		curProjEffects.clear();
		curProjEffects.insert(Data_Leak);
	}

	//RedundanceLeak
	else if(HasDataRedundanceEffect())
		Add_RedundanceLeak_Effect();

	//TransLeak
	else if(HasInterfaceTransEffect())
		Add_TransLeak_Effect();

	//DataExplode
	else if(HasDataLeakEffect())
		Add_DataExplode_Effect();

}

void ProjEffectManager::AddDataLeakTarget(HitInfo &info , float fValue /* = 0 */)
{
	m_DataLeak->Add_To_List(info);
}

//--------DataRedundance--------//
void ProjEffectManager::Add_DataRedundance_Effect()
{
	if(curProjEffects.empty())
		curProjEffects.insert(Data_Redundance);
	else if(HasAdvancedEffect())
	{
		curProjEffects.clear();
		curProjEffects.insert(Data_Redundance);
	}

	//RedundanceLeak
	else if(HasDataLeakEffect())
		Add_RedundanceLeak_Effect();

	//TrasnRedundance
	else if(HasInterfaceTransEffect())
		Add_TransRedundance_Effect();

	//DataFreeze
	else if(HasDataRedundanceEffect())
		Add_DataFreeze_Effect();

}

void ProjEffectManager::Add_DataRedundanceTarget(HitInfo &info , float fValue /* = 0 */)
{
	m_DataRedundance->Add_To_List(info);
}

//--------InterfaceTrans--------//
void ProjEffectManager::Add_InterfaceTrans_Effect()
{
	if(curProjEffects.empty())
		curProjEffects.insert(Interface_Trans);

	else if(HasAdvancedEffect())
	{
		curProjEffects.clear();
		curProjEffects.insert(Interface_Trans);
	}
	//TransLeak
	else if(HasDataLeakEffect())
		Add_TransLeak_Effect();

	//TrasnRedundance
	else if(HasDataRedundanceEffect())
		Add_TransRedundance_Effect();

	//DataBlock
	else if(HasInterfaceTransEffect())
		Add_DataBlock_Effect();

}

void ProjEffectManager::Add_InterfaceTransTarget(HitInfo &info , int iValue /* = 0 */)
{
	m_InterfaceTrans->Add_To_List(info);
}

//--------DataExplode--------//
void ProjEffectManager::Add_DataExplode_Effect()
{
	curProjEffects.clear();
	curProjEffects.insert(Data_Explode);
}

void ProjEffectManager::ProcessDataExplode(HitInfo &info)
{
	m_DataExplode->ProcessInstantEvent(info);
}

//--------DataFreeze--------//
void ProjEffectManager::Add_DataFreeze_Effect()
{
	curProjEffects.clear();
	curProjEffects.insert(Data_Explode);
}

void ProjEffectManager::AddDataFreezeTarget(HitInfo &info)
{
	m_DataFreeze->Add_To_List(info);
}

//--------DataBlock--------//
void ProjEffectManager::Add_DataBlock_Effect()
{
	curProjEffects.clear();
	curProjEffects.insert(Data_Block);
}

void ProjEffectManager::AddDataBlockTarget(HitInfo &info)
{
	m_DataBlock->Add_To_List(info);
}

//--------RedundanceLeak--------//
void ProjEffectManager::Add_RedundanceLeak_Effect()
{
	curProjEffects.clear();
	curProjEffects.insert(Redundance_Leak);
}

//--------TransLeak--------//
void ProjEffectManager::Add_TransLeak_Effect()
{
	curProjEffects.clear();
	curProjEffects.insert(Trans_Leak);
}

//--------Trans_Redundance--------//
void ProjEffectManager::Add_TransRedundance_Effect()
{
	curProjEffects.clear();
	curProjEffects.insert(Trans_Redundance);
}


//--------BulletReflection--------//
void ProjEffectManager::Add_BulletReflection_Effect()
{
	if(curProjEffects.empty())
		curProjEffects.insert(Bullet_Reflection);

	else if(HasAdvancedEffect())
	{
		curProjEffects.clear();
		curProjEffects.insert(Bullet_Reflection);
	}
	/*//TransLeak
	else if(HasDataLeakEffect())
		Add_TransLeak_Effect();

	//TrasnRedundance
	else if(HasDataRedundanceEffect())
		Add_TransRedundance_Effect();

	//DataBlock
	else if(HasInterfaceTransEffect())
		Add_DataBlock_Effect();*/

}

void ProjEffectManager::ProcessBulletReflection(HitInfo &info)
{
	m_BulletReflection->ProcessInstantEvent(info);
}
//------------------------------------------------------------------------
