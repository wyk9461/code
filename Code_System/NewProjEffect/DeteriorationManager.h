#ifndef _DETERIORATIONMANAGER_H_
#define _DETERIORATIONMANAGER_H_

#include "StdAfx.h"

class DeteriorationManager
{
public:
	static DeteriorationManager &Instance(){static DeteriorationManager instance;return instance;}

	bool HitSameTarget(IEntity *pEntity){return pEntity == pTarget;}

	const float GetDeterValue(){return deter_value;}

	void ClearDeterValue(){deter_value = 0;}

	void IncDeterValue(){deter_value += 0.1;}

	void SetTarget(IEntity *pEntity){pTarget = pEntity;}

	~DeteriorationManager(){}
private:
	DeteriorationManager()
	:	pTarget(NULL)
	,	deter_value(0){}

	IEntity *pTarget;
	float deter_value;
};
#endif