#include "StdAfx.h"
#include "VirusManager.h"

//IParticleEffect* InfectionVirusManager::pInfectedEffect = NULL;
//IParticleEffect* ReplicationVirusManager::pInfectedEffect = NULL;

void VirusManager::AddNewCoreVirus(VirusParams &params)
{
	if(InfectionList.empty())
	{
		AABB aabb;
		params.pHost->GetWorldBounds(aabb);
		params.pEmitter = pInfectedEffect->Spawn(false,params.pHost->GetWorldTM());
		params.pEmitter->SetEntity(params.pHost,0);
		/*ParticleTarget target;
		target.bPriority = true;
		target.bTarget = true;
		target.fRadius = aabb.GetVolume();
		target.vTarget = aabb.GetCenter();
		target.vVelocity = Vec3(ZERO);

		params.pEmitter->SetTarget(target);*/

		SpawnParams EmitterSpawnParams;
		params.pEmitter->GetSpawnParams(EmitterSpawnParams);
		EmitterSpawnParams.eAttachForm = GeomForm_Volume;
		EmitterSpawnParams.eAttachType = GeomType_Physics;
		params.pEmitter->SetSpawnParams(EmitterSpawnParams);

		params.pEmitter->Activate(true);

		InfectionList.push_back(&params);
		InfactedEntities.insert(params.pHost->GetId());
	}
}

void VirusManager::AddNewChildVirus(VirusParams &params)
{
	AABB aabb;
	params.pHost->GetWorldBounds(aabb);
	params.pEmitter = pInfectedEffect->Spawn(false,params.pHost->GetWorldTM());
	params.pEmitter->SetEntity(params.pHost,0);

	SpawnParams EmitterSpawnParams;
	params.pEmitter->GetSpawnParams(EmitterSpawnParams);
	EmitterSpawnParams.eAttachForm = GeomForm_Volume;
	EmitterSpawnParams.eAttachType = GeomType_Physics;
	params.pEmitter->SetSpawnParams(EmitterSpawnParams);

	params.pEmitter->Activate(true);

	InfectionList.push_back(&params);
	InfactedEntities.insert(params.pHost->GetId());

	++child_count;
}

//-----------------------------------------------------------------------------------------------------------------------------------
void InfectionVirusManager::Update(float frameTime)
{
	std::list<VirusParams*>::iterator iter = InfectionList.begin();
	std::list<VirusParams*>::iterator end = InfectionList.end();
	while(iter != end)
	{
		InfectionVirusParams &virus = (InfectionVirusParams&)(**iter);
		CActor *pActor = (CActor *)g_pGame->GetGameRules()->GetActorByEntityId(virus.pHost->GetId());
		if(pActor)
		{
			if(pActor->IsDead())
			{
				gEnv->pParticleManager->DeleteEmitter(virus.pEmitter);
				SplitVirus(virus);
				iter = InfectionList.erase(iter);
				delete &virus;
				continue;
			}
			IEntity *pPlayer = gEnv->pEntitySystem->GetEntity(g_pGame->GetClientActorId());
			HitInfo hit;
			hit.shooterId = pPlayer->GetId();
			hit.targetId = virus.pHost->GetId();
			hit.dir = Vec3(0,0,1);
			hit.weaponId = 0;
			hit.damage = virus.damage;
			hit.type = CGameRules::EHitType::Fire;
			g_pGame->GetGameRules()->ClientHit(hit);
		}
		else
		{
			float scale_offset = BiRandom(0.03);
			float scale = 1 * (1 - scale_offset);
			virus.pHost->SetScale(Vec3(scale));
			virus.tick_timer += frameTime;
			if(virus.tick_timer > virus.InfectionTime)
			{
				gEnv->pParticleManager->DeleteEmitter(virus.pEmitter);
				SplitVirus(virus);
				virus.pHost->Hide(true);
				CommonUsage::CreateQueueExplosion(g_pGame->GetClientActorId(),virus.pHost->GetPos(),200,3,0, CGameRules::EHitType::Explosion, 
					"EntityProjectile.StickBomb_Explode");
				g_pGame->GetWorldStability()->StabilityGain(50);
				iter = InfectionList.erase(iter);
				delete &virus;
				continue;
			}
		}
		++iter;
	}
	if(InfectionList.empty())
	{
		child_count = 0;
		InfactedEntities.clear();
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------
void InfectionVirusManager::AddNewCoreVirus(VirusParams &params)
{
	VirusManager::AddNewCoreVirus(params);
}

//-----------------------------------------------------------------------------------------------------------------------------------
void InfectionVirusManager::AddNewChildVirus(VirusParams &params)
{
	VirusManager::AddNewChildVirus(params);
}

//-----------------------------------------------------------------------------------------------------------------------------------
void InfectionVirusManager::SplitVirus(VirusParams &params)
{
	if(child_count < max_child_count)
	{
		IPhysicalEntity **nearbyEntities;
		Vec3 pos = params.pHost->GetPos();
		int num=gEnv->pPhysicalWorld->GetEntitiesInBox(pos-Vec3(5), pos+Vec3(5), nearbyEntities, ent_all);
		EntityId nearestId = 0;
		AABB aabb;
		for(int i = 0; i < num; ++i)
		{
			IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
			if(pEntity && !InfactedEntities.count(pEntity->GetId()))
			{
				InfectionVirusParams *new_params = new InfectionVirusParams;
				new_params->damage = 1;
				new_params->tick_timer = 0;
				new_params->InfectionTime = 3;
				new_params->pHost = pEntity;
				AddNewChildVirus(*new_params);
			}
		}
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------
void ReplicationVirusManager::AddNewCoreVirus(VirusParams &params)
{
	VirusManager::AddNewCoreVirus(params);
}

//-----------------------------------------------------------------------------------------------------------------------------------
void ReplicationVirusManager::AddNewChildVirus(VirusParams &params)
{
	VirusManager::AddNewChildVirus(params);
}


void ReplicationVirusManager::Update(float frameTime)
{
	std::list<VirusParams*>::iterator iter = InfectionList.begin();
	std::list<VirusParams*>::iterator end = InfectionList.end();
	while(iter != end)
	{
		ReplicationVirusParams &virus = (ReplicationVirusParams&)(**iter);
		float scale_offset = BiRandom(0.1);
		float scale = 1 * (1 - scale_offset);
		virus.pHost->SetScale(Vec3(scale));
		virus.tick_timer += frameTime;
		if(virus.tick_timer > virus.InfectionTime)
		{
			//gEnv->pParticleManager->DeleteEmitter(virus.pEmitter);
			virus.tick_timer = 0;
			SplitVirus(virus.pHost,virus);
			/*SplitVirus(virus.pHost,virus);
			virus.pHost->Hide(true);
			iter = InfectionList.erase(iter);
			delete &virus;*/
			continue;
		}
		if(child_count >= max_child_count)
		{
			ClearVirus();
			return;
		}
		++iter;
	}
}

IEntity *ReplicationVirusManager::ReplicateEntity(IEntity *pEntity)
{
	AABB aabb;
	pEntity->GetWorldBounds(aabb);
	SEntitySpawnParams params;

	params.vPosition = pEntity->GetPos();
	params.vScale=Vec3(1,1,1);
	params.pClass = pEntity->GetClass();

	IScriptTable *pScriptTable = pEntity->GetScriptTable();
	SmartScriptTable pPropertiesTable;
	pScriptTable->GetValue("Properties", pPropertiesTable);
	params.pPropertiesTable = pPropertiesTable;

	IEntity *pReplicationEntity = gEnv->pEntitySystem->SpawnEntity(params);

	return pReplicationEntity;
}

void ReplicationVirusManager::SplitVirus(IEntity *pEntity, ReplicationVirusParams &params)
{
	if(child_count < max_child_count)
	{
		IEntity *pREntity = ReplicateEntity(pEntity);

		ReplicationVirusParams *new_params = new ReplicationVirusParams;
		new_params->tick_timer = 0;
		new_params->InfectionTime = 3;
		new_params->pHost = pREntity;
		new_params->mass = params.mass;

		AddNewChildVirus(*new_params);

		Vec3 dir = BiRandom(Vec3(1,1,0));
		dir.normalize();
		IPhysicalEntity *pPEnt=pREntity->GetPhysics();
		if(pPEnt)
		{
			pe_action_set_velocity ve;
			ve.v=dir * 15;
			pPEnt->Action(&ve);
		}

		/*IEntityPhysicalProxy *pPhysicsProxy = static_cast<IEntityPhysicalProxy*>(pREntity->GetProxy(ENTITY_PROXY_PHYSICS));
		if(pPhysicsProxy)
		{
			AABB aabb;
			pREntity->GetWorldBounds(aabb);
			pPhysicsProxy->AddImpulse(-1, aabb.GetCenter(), dir*new_params->mass * 10, false, 1);
		}*/
	}
}

void ReplicationVirusManager::ClearVirus()
{
	std::list<VirusParams*>::iterator iter = InfectionList.begin();
	std::list<VirusParams*>::iterator end = InfectionList.end();
	while(iter != end)
	{
		ReplicationVirusParams &virus = (ReplicationVirusParams&)(**iter);

		gEnv->pParticleManager->DeleteEmitter(virus.pEmitter);
		virus.pHost->SetScale(Vec3(1));
		iter = InfectionList.erase(iter);
		delete &virus;
	}
	child_count = 0;
}