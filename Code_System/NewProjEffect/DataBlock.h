#ifndef _DATABLOCK_H_
#define _DATABLOCK_H_

#include "StdAfx.h"

struct DataBlockParams
{
	float last_time;
	float tick_timer;
	float fake_timer;
	float damage;
	HitInfo BlockInfo;
	IPhysicalEntity **nearbyEntities;
	const bool operator<(const DataBlockParams &dbp){return tick_timer<dbp.tick_timer;}

	DataBlockParams &operator=(const DataBlockParams &dbp)
	{
		if(this==&dbp)
			return *this;
		last_time=dbp.last_time;
		tick_timer=dbp.tick_timer;
		fake_timer=dbp.fake_timer;
		damage=dbp.damage;
		BlockInfo=dbp.BlockInfo;
		nearbyEntities=dbp.nearbyEntities;
		return *this;
	}

	DataBlockParams()
		:	last_time(0)
		,	tick_timer(0)
		,	damage(0)
		,	fake_timer(0){}
};

class DataBlock
{
private:
	typedef std::map<EntityId , DataBlockParams> BlockMap;
	BlockMap DataBlockTargets;
public:
	DataBlock(){}
	~DataBlock(){DataBlockTargets.clear();}
	void Update(const float frameTime);
	void Add_To_List(HitInfo &info);
};
#endif