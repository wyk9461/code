#ifndef _STICKPROJECTILEMANAGER_H_
#define _STICKPROJECTILEMANAGER_H_

#include "StdAfx.h"

struct StickParams
{
	float tick_timer;
	float explode_time;
	float damage;
	IEntity *pTarget;
	IParticleEmitter *pEmitter;
	Vec3 offset;


};

class StickProjectileManager
{
public:
	~StickProjectileManager(){}
	static StickProjectileManager &Instance(){static StickProjectileManager instance; return instance;}
	void AddToDeque(StickParams params);
	void Update(float frameTime);
private:
	StickProjectileManager();
	std::list<StickParams> Sticks;
	static IParticleEffect *pStickEffect;
};

#endif