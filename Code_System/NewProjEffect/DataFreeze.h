#ifndef _DATAFREEZE_H_
#define _DATAFREEZE_H_

#include "StdAfx.h"

struct DataFreezeParams
{
	float last_time;
	float tick_timer;

	DataFreezeParams &operator=(const DataFreezeParams &dfp)
	{
		if(this==&dfp)
			return *this;
		last_time=dfp.last_time;
		tick_timer=dfp.tick_timer;
		return *this;
	}

	const bool operator<(const DataFreezeParams &drp){return last_time<drp.last_time;}

	DataFreezeParams()
		:	last_time(0)
		,	tick_timer(0){}
};


class DataFreeze
{
private:
	typedef std::map<EntityId , DataFreezeParams> FreezeMap;
	FreezeMap DataFreezeTargets;
public:
	DataFreeze(){}
	~DataFreeze(){DataFreezeTargets.clear();}
	void Update(const float frameTime);
	void Add_To_List(HitInfo &info);
};


#endif