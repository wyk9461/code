#ifndef _INTERFACETRANS_H_
#define _INTERFACETRANS_H_

struct InterfaceTransPrams
{
	float damage;
	int max_bounce;
	int bounce_count;
	int num;
	IEntity *prevEntity;
	IEntity *curEntity;
	IEntity *nextEntity;
	IEntity *Particle;
	IPhysicalEntity **nearbyEntities;
	HitInfo BounceInfo;
	bool has_target;
	Vec3 curPos;
	Vec3 Destination;
	float distance;
	float speed;
	std::map<float,EntityId> Distance;    //from curEntity to NextEntity
	std::set<EntityId> HitEntity;

	InterfaceTransPrams &operator=(const InterfaceTransPrams&bp)
	{
		if(this == &bp)
			return *this;
		damage=bp.damage;
		max_bounce=bp.max_bounce;
		bounce_count=bp.bounce_count;
		num=bp.num;

		prevEntity=bp.prevEntity;
		curEntity=bp.curEntity;
		nextEntity=bp.nextEntity;
		nearbyEntities=bp.nearbyEntities;

		BounceInfo=bp.BounceInfo;
		Distance=bp.Distance;
		HitEntity=bp.HitEntity;
		has_target=bp.has_target;

		Particle=bp.Particle;
		curPos=bp.curPos;
		Destination=bp.Destination;
		Destination=bp.Destination;
		distance=bp.distance;
		speed=bp.speed;
		return *this;
	}

	InterfaceTransPrams()
		:	damage(0)
		,	max_bounce(0)
		,	bounce_count(0)
		,	num(0)
		,	prevEntity(NULL)
		,	curEntity(NULL)
		,	nextEntity(NULL)
		,	Particle(NULL)
		,	nearbyEntities(0)
		,	has_target(false)
		,	curPos(0,0,0)
		,	Destination(0,0,0)
		,	distance(0)
		,	speed(0){}
};

class InterfaceTrans
{
private:
	typedef std::map<EntityId , InterfaceTransPrams> TransMap;
	TransMap InterfaceTransTargets;
	void ProcessCombination(HitInfo &info);
public:
	InterfaceTrans(){}
	~InterfaceTrans(){InterfaceTransTargets.clear();}
	void Update(const float frameTime);
	void Add_To_List(HitInfo &info , int iValue = 0);
};


#endif