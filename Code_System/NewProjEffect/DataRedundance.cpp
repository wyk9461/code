#include "StdAfx.h"
#include "DataRedundance.h"


void DataRedundance::Update(const float frameTime)
{
	if(!DataRedundanceTargets.empty())
	{
		RedundanceMap::iterator iter=DataRedundanceTargets.begin();
		RedundanceMap::iterator end=DataRedundanceTargets.end();
		while(iter != end)
		{
			DataRedundanceParams &params=iter->second;
			if(params.tick_timer <= params.last_time)
			{
				CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(iter->first));
				if(pActor && !pActor->IsDead())
				{
					pActor->SetSpeedMultipler(SActorParams::eSMR_Internal,params.freeze_level);
					pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,params.freeze_level);
					pActor->SetSpeedMultipler(SActorParams::eSMR_Item,params.freeze_level);
				}
				params.tick_timer+=frameTime;
			}
			if(params.tick_timer>params.last_time)
			{
				CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(iter->first));
				IEntity *pEntity=pActor->GetEntity();

				//May need to get defSpeed
				pActor->SetSpeedMultipler(SActorParams::eSMR_Internal,1);
				pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,1);
				pActor->SetSpeedMultipler(SActorParams::eSMR_Item,1);

				IParticleEmitter *pEmitter=pEntity->GetParticleEmitter(10);
				if(pEmitter)
					gEnv->pParticleManager->DeleteEmitter(pEmitter);

				DataRedundanceTargets.erase(iter++);
				end=DataRedundanceTargets.end();
			}
			else
				++iter;
		}
	}
}


void DataRedundance::Add_To_List(HitInfo &info , float fValue /*= 0 */)
{
	DataRedundanceParams tParams;
	tParams.freeze_level=0.3;
	if(fValue == 0)
		tParams.last_time=3;
	else
		tParams.last_time=fValue;
	tParams.tick_timer=0;

	DataRedundanceTargets[info.targetId]=tParams;

	IEntity *pEntity=gEnv->pEntitySystem->GetEntity(info.targetId);
	IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Code_System.DeleteEntity");
	pEntity->LoadParticleEmitter(10,pEffect);
}