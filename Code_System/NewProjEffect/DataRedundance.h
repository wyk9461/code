#ifndef _DATAREDUNDANCE_H_
#define _DATAREDUNDANCE_H_

#include "StdAfx.h"

struct DataRedundanceParams
{
	float last_time;
	float tick_timer;
	float freeze_level;

	DataRedundanceParams &operator=(const DataRedundanceParams &drp)
	{
		if(this==&drp)
			return *this;
		last_time=drp.last_time;
		tick_timer=drp.tick_timer;
		freeze_level=drp.freeze_level;
		return *this;
	}

	const bool operator<(const DataRedundanceParams &drp){return last_time<drp.last_time;}

	DataRedundanceParams()
		:	last_time(0)
		,	tick_timer(0)
		,	freeze_level(0){}
};


class DataRedundance
{
private:
	typedef std::map<EntityId , DataRedundanceParams> RedundanceMap;
	RedundanceMap DataRedundanceTargets;
public:
	DataRedundance(){}
	~DataRedundance(){DataRedundanceTargets.clear();}
	void Update(const float frameTime);
	void Add_To_List(HitInfo &info , float fValue = 0);
};
#endif