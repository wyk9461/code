#ifndef _BULLETREFLECTION_H_
#define _BULLETREFLECTION_H_

class BulletReflection
{
private:
	void ProcessTracer(HitInfo &hitInfo,CProjectile *pProjectile,Vec3 &dest);
public:
	BulletReflection(){}
	~BulletReflection(){}
	void ProcessInstantEvent(HitInfo &hitInfo);
};
#endif