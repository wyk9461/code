#include "StdAfx.h"
#include "InterfaceTrans.h"

void InterfaceTrans::Update(const float frameTime)
{
	if(!InterfaceTransTargets.empty())
	{
		TransMap::iterator iter=InterfaceTransTargets.begin();
		TransMap::iterator end=InterfaceTransTargets.end();
		while(iter!=end)
		{
			InterfaceTransPrams &params=iter->second;

			//Get Target
			if(!params.has_target)
			{
				//Init
				params.curEntity=gEnv->pEntitySystem->GetEntity(params.BounceInfo.targetId);
				params.HitEntity.insert(params.curEntity->GetId());
				if(params.prevEntity == NULL)
					params.prevEntity=params.curEntity;
				params.nextEntity=NULL;
				//Get nearby entities
				params.num=gEnv->pPhysicalWorld->GetEntitiesInBox(params.BounceInfo.pos-Vec3(8),params.BounceInfo.pos+Vec3(8),params.nearbyEntities,ent_living);
				//Check if this entity has been hit
				for(int i=0;i<params.num;i++)
				{
					IEntity* tEntity=gEnv->pEntitySystem->GetEntityFromPhysics(params.nearbyEntities[i]);
					if(!params.HitEntity.count(tEntity->GetId()))
					{
						float distance=(params.curEntity->GetPos()-tEntity->GetPos()).GetLength();
						params.Distance[distance]=tEntity->GetId();
					}
				}
				//Choose the nearest entity
				std::map<float,EntityId>::iterator mit=params.Distance.begin();
				while(mit !=params.Distance.end())
				{
					params.nextEntity=gEnv->pEntitySystem->GetEntity(mit->second);
					if(params.HitEntity.count(params.nextEntity->GetId()) || params.nextEntity == g_pGame->GetIGameFramework()->GetClientActor()->GetEntity())
						++mit;
					else
					{
						params.has_target=true;
						params.prevEntity=params.curEntity;
						params.curEntity=params.nextEntity;
						params.nextEntity=NULL;
						params.BounceInfo.shooterId=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetId();
						params.BounceInfo.targetId=params.curEntity->GetId();
						params.bounce_count++;
						AABB aabb;
						params.prevEntity->GetWorldBounds(aabb);
						params.curPos=aabb.GetCenter();
						params.curEntity->GetWorldBounds(aabb);
						params.Destination=aabb.GetCenter();
						params.distance=(params.Destination-params.curPos).GetLength();
						params.speed=(float)(params.distance/0.1);		//speed Change
						break;
					}
				}
				//if no entity available,then finish the process and clear everything
				if(mit == params.Distance.end() || params.bounce_count >= params.max_bounce)
				{
					gEnv->pEntitySystem->RemoveEntity(params.Particle->GetId());
					params.Particle=NULL;
					params.has_target=false;
					InterfaceTransTargets.erase(iter++);
					break;
				}
			}

			//Move Particle and cause some damage
			if(params.has_target)
			{
				AABB aabb;
				params.curEntity->GetWorldBounds(aabb);
				params.Destination=aabb.GetCenter();
				Vec3 Velocity=(params.Destination-params.curPos).normalize();
				params.distance=(params.Destination-params.curPos).GetLength();
				params.Particle->SetPos(params.curPos);
				params.curPos+=(Velocity*params.speed) /(1/frameTime);
				if( (params.curPos - params.Destination).GetLengthSquared() < 0.2)
				{
					params.nearbyEntities=0;
					params.has_target=false;
					params.BounceInfo.damage*=0.8;		//Damage reduction
					g_pGame->GetGameRules()->ClientHit(params.BounceInfo);
					//Process combined effects
					ProcessCombination(params.BounceInfo);
					IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("explosions.Code_Explode.Explode1");
					pEffect->Spawn(true,IParticleEffect::ParticleLoc(params.curPos));
					params.Distance.clear();
				}
			}
			++iter;
		}
	}
}

void InterfaceTrans::Add_To_List(HitInfo &info , int iValue /* = 0 */)
{
	if(!InterfaceTransTargets.count(info.targetId))
	{
		SEntitySpawnParams ParticleParams;
		ParticleParams.vPosition=gEnv->pEntitySystem->GetEntity(info.targetId)->GetPos();
		ParticleParams.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
		ParticleParams.sName="BounceEffect";
		ParticleParams.vScale=Vec3(1,1,1);

		InterfaceTransPrams tParams;
		tParams.damage=static_cast<float>(info.damage*0.9);
		if(iValue == 0)
			tParams.max_bounce=10;
		else
			tParams.max_bounce=iValue;
		tParams.bounce_count=0;
		tParams.BounceInfo=info;
		tParams.BounceInfo.projectileId=0;

		tParams.Particle=gEnv->pEntitySystem->SpawnEntity(ParticleParams);
		tParams.Particle->SetRotation(tParams.Particle->GetRotation().CreateRotationX(DEG2RAD(90)));
		SmartScriptTable root=tParams.Particle->GetScriptTable();
		SmartScriptTable props;
		root->GetValue("Properties",props);
		props->SetValue("ParticleEffect","Projectile_Effect.Bounce");
		Script::CallMethod(root,"OnReset");
		InterfaceTransTargets[info.targetId]=tParams;
	}
}

void InterfaceTrans::ProcessCombination(HitInfo &info)
{
	ProjEffectManager *Manager=g_pGame->GetProjEffectManager();
	if(Manager->HasTransLeakLeakEffect())
		Manager->AddDataLeakTarget(info,2);
	if(Manager->HasTransRedundanceEffect())
		Manager->Add_DataRedundanceTarget(info,2);
}