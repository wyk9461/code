#include "StdAfx.h"
#include "DataBlock.h"

void DataBlock::Update(const float frameTime)
{
	if(!DataBlockTargets.empty())
	{
		BlockMap::iterator iter=DataBlockTargets.begin();
		BlockMap::iterator end=DataBlockTargets.end();
		while(iter != end)
		{
			DataBlockParams & params=iter->second;
			if(params.tick_timer<=params.last_time)
			{
				if(params.tick_timer>=params.fake_timer)
				{
					IEntity *pEntity=gEnv->pEntitySystem->GetEntity(iter->first);
					int num=gEnv->pPhysicalWorld->GetEntitiesInBox(pEntity->GetPos()-Vec3(5),pEntity->GetPos()+Vec3(5),params.nearbyEntities,ent_living);
					for(int i=0;i<num;++i)
					{
						IEntity* tEntity=gEnv->pEntitySystem->GetEntityFromPhysics(params.nearbyEntities[i]);
						if(tEntity)
						{
							IActor *pActor=g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(tEntity->GetId());
							params.BlockInfo.targetId=tEntity->GetId();

							IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("explosions.Code_Explode.Explode1");
							pEffect->Spawn(true,IParticleEffect::ParticleLoc(tEntity->GetPos()));

							if(pActor && !pActor->IsDead())
							{
								float curHealth=pActor->GetHealth();
								if(curHealth - params.damage > 0)
								{
									curHealth-=params.damage;
									pActor->SetHealth(curHealth);
								}
								else
									g_pGame->GetGameRules()->ClientHit(params.BlockInfo);			
							}
						}
					}
					params.fake_timer+=0.5;
				}
				params.tick_timer+=frameTime;
			}
			if(params.tick_timer>params.last_time)
			{
				IEntity *pEntity=gEnv->pEntitySystem->GetEntity(iter->first);
				IParticleEmitter *pEmitter=pEntity->GetParticleEmitter(10);
				if(pEmitter)
					gEnv->pParticleManager->DeleteEmitter(pEmitter);
				DataBlockTargets.erase(iter++);
				end=DataBlockTargets.end();
			}
			else
				++iter;
		}
	}
}


void DataBlock::Add_To_List(HitInfo &info)
{
	DataBlockParams tParams;
	tParams.damage=static_cast<float>(info.damage*0.9);		//Need to Change later
	tParams.last_time=2;		//Need to Change later
	tParams.tick_timer=0;
	tParams.fake_timer=tParams.tick_timer+=0.5;

	tParams.BlockInfo=info;
	tParams.BlockInfo.damage=tParams.damage;
	tParams.BlockInfo.projectileId=0;

	DataBlockTargets[info.targetId]=tParams;

	IEntity *pEntity=gEnv->pEntitySystem->GetEntity(info.targetId);
	IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Code_System.DeleteEntity");
	pEntity->LoadParticleEmitter(10,pEffect);
}