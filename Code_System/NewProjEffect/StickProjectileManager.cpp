#include "StdAfx.h"
#include "StickProjectileManager.h"

IParticleEffect *StickProjectileManager::pStickEffect = NULL;

StickProjectileManager::StickProjectileManager()
{
	if(!pStickEffect)
	{
		pStickEffect = gEnv->pParticleManager->FindEffect("EntityProjectile.StickBomb");
	}
}

void StickProjectileManager::AddToDeque(StickParams params)
{
	Matrix34 newLocalTM = Matrix34(IDENTITY);
	if(params.pTarget)
	{
		newLocalTM.SetTranslation(params.pTarget->GetLocalTM().GetTranslation() + params.offset);
	}
	else
	{
		newLocalTM.SetTranslation(params.offset);
	}
	params.pEmitter = pStickEffect->Spawn(false,newLocalTM);
	SpawnParams spawnParams;
	params.pEmitter->GetSpawnParams(spawnParams);
	spawnParams.fTimeScale = params.explode_time / 3;
	params.pEmitter->SetSpawnParams(spawnParams);
	params.pEmitter->Activate(true);
	Sticks.push_back(params);
}

void StickProjectileManager::Update(float frameTime)
{
	std::list<StickParams>::iterator iter = Sticks.begin();
	std::list<StickParams>::iterator end = Sticks.end();

	while(iter != end)
	{
		StickParams &params = *iter;
		params.tick_timer += frameTime;
		Matrix34 newLocalTM = Matrix34(IDENTITY);
		if(params.pTarget)
		{
			newLocalTM.SetTranslation(params.pTarget->GetLocalTM().GetTranslation() + params.offset);
		}
		else
		{
			newLocalTM.SetTranslation(params.offset);
		}
		Vec3 StickPos = newLocalTM.GetTranslation();
		params.pEmitter->SetMatrix(newLocalTM);
		if(params.tick_timer > params.explode_time)
		{
			params.pEmitter->Activate(false);
			CommonUsage::CreateQueueExplosion(g_pGame->GetClientActorId(), StickPos, params.damage, 0.5, 0, CGameRules::EHitType::Explosion, "EntityProjectile.StickBomb_Explode");
			iter = Sticks.erase(iter);
		}
		else
			++iter;
	}
}