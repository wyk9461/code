#include "StdAfx.h"
#include "DataExplode.h"

void DataExplode::ProcessInstantEvent(HitInfo &hitInfo)
{
	float damage=hitInfo.damage;
	damage*=1.5;
	ExplosionInfo einfo(hitInfo.shooterId, hitInfo.weaponId, 0 , damage , hitInfo.pos, hitInfo.dir, 0.5, 0.5, 0.1f, 0.5f, hitInfo.angle, 300 , 0.0f, 0);
	einfo.SetEffect("explosions.Code_Explode.Explode1",1.0f,0.2f);
	einfo.type = g_pGame->GetGameRules()->GetHitTypeId( "Explosion" );
	g_pGame->GetGameRules()->QueueExplosion(einfo);
}