#include "StdAfx.h"
#include "BulletReflection.h"
#include "Projectile.h"
#include "WeaponSharedParams.h"
#include "FireMode.h"

void BulletReflection::ProcessInstantEvent(HitInfo &hitInfo)
{
	if(hitInfo.damage > 10)
	{
		CWeaponSystem *pSystem=g_pGame->GetWeaponSystem();
		CProjectile *pProjectile = pSystem->GetProjectile(hitInfo.projectileId);
		IEntityClass *pAmmoClass=pProjectile->GetEntity()->GetClass();
		if(pAmmoClass)
		{
			CProjectile *new_Projectile_1=pSystem->SpawnAmmo(pAmmoClass);
			CProjectile *new_Projectile_2=pSystem->SpawnAmmo(pAmmoClass);
			if(new_Projectile_1)
			{
				int new_damage=static_cast<int>(hitInfo.damage/2);
				CProjectile::SProjectileDesc projectileDesc(
					hitInfo.shooterId, 0, hitInfo.weaponId, new_damage, 0, 0, 0, hitInfo.type,
					0, false);

				EntityId actorId = g_pGame->GetClientActorId();
				Vec3 startPos = gEnv->pGame->GetIGameFramework()->GetClientActor()->GetCurrentItem()->GetIWeapon()->GetFiringPos(hitInfo.pos);

				Vec3 pos = hitInfo.pos;
				Vec3 r = pos - startPos;
				Vec3 new_pos=pos+r*0.1;
				r.normalize();
				float cos30=cos(DEG2RAD(15));
				float sin30=sin(DEG2RAD(15));
				float cos30_=cos(DEG2RAD(-15));
				float sin30_=sin(DEG2RAD(-15));
				IParticleEffect *bulletEffect=gEnv->pParticleManager->FindEffect("Projectile_Effect.Reflection_Indicator");
				Vec3 dir(r.x*cos30-r.y*sin30 , r.x*sin30+r.y*cos30,r.z);
				dir.normalize();
				Vec3 dir2(r.x*cos30_-r.y*sin30_ , r.x*sin30_+r.y*cos30_,r.z);
				dir2.normalize();
				new_Projectile_1->SetParams(projectileDesc);
				new_Projectile_1->Launch(new_pos,dir,Vec3(ZERO));
				bulletEffect->Spawn(true,IParticleEffect::ParticleLoc(pos,dir));
				//ProcessTracer(hitInfo,new_Projectile_1,pos+10000*dir);

				new_Projectile_2->SetParams(projectileDesc);
				new_Projectile_2->Launch(new_pos,dir2,Vec3(ZERO));
				bulletEffect->Spawn(true,IParticleEffect::ParticleLoc(pos,dir2));
				//ProcessTracer(hitInfo,new_Projectile_2,pos+10000*dir);
			}
		}
	}
}

void BulletReflection::ProcessTracer(HitInfo &hitInfo , CProjectile *pProjectile , Vec3 &dest)
{
	CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetClientActor());
	CWeapon *pWeapon=(CWeapon*)pActor->GetCurrentItem();

	int fire_mode_index=pWeapon->GetCurrentFireMode();
	CFireMode* pFiremode = static_cast<CFireMode*>(pWeapon->GetFireMode(fire_mode_index));
	const SFireModeParams* tFireModeParams=pFiremode->GetShared();
	const STracerParams * useTracerParams = &tFireModeParams->tracerparams;

	CTracerManager::STracerParams params;
	params.position = hitInfo.pos;
	params.destination = dest;

	if(pWeapon->GetStats().fp)
	{
		Vec3 dir = (dest-params.position);

		const float length = dir.NormalizeSafe();

		if(length < (2.0f))
			return;

		params.position += (dir * 2.0f);
		params.effect = useTracerParams->effectFP.c_str();
		params.geometry = useTracerParams->geometryFP.c_str();
		params.speed = useTracerParams->speedFP;
		params.scale = useTracerParams->thicknessFP;
	}
	else
	{
		params.effect = useTracerParams->effect.c_str();
		params.geometry = useTracerParams->geometry.c_str();
		params.speed = useTracerParams->speed;
		params.scale = useTracerParams->thickness;
	}

	params.slideFraction = useTracerParams->slideFraction;
	params.lifetime = useTracerParams->lifetime;
	params.delayBeforeDestroy = useTracerParams->delayBeforeDestroy;

	const int tracerIdx = g_pGame->GetWeaponSystem()->GetTracerManager().EmitTracer(params, pProjectile ? pProjectile->GetEntityId() : 0);

	if (pProjectile)
	{
		pProjectile->BindToTracer(tracerIdx);
	}
}