#include "StdAfx.h"
#include "DataFreeze.h"


void DataFreeze::Update(const float frameTime)
{
	if(!DataFreezeTargets.empty())
	{
		FreezeMap::iterator iter=DataFreezeTargets.begin();
		FreezeMap::iterator end=DataFreezeTargets.end();
		while(iter != end)
		{
			DataFreezeParams &params=iter->second;
			if(params.tick_timer <= params.last_time)
			{
				CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(iter->first));
				if(pActor && !pActor->IsDead())
				{
					pActor->SetSpeedMultipler(SActorParams::eSMR_Internal,0);
					pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,0);
					pActor->SetSpeedMultipler(SActorParams::eSMR_Item,0);
				}
				params.tick_timer+=frameTime;
			}
			if(params.tick_timer>params.last_time)
			{
				CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(iter->first));
				IEntity *pEntity=pActor->GetEntity();

				//May need to get defSpeed
				pActor->SetSpeedMultipler(SActorParams::eSMR_Internal,1);
				pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,1);
				pActor->SetSpeedMultipler(SActorParams::eSMR_Item,1);

				IParticleEmitter *pEmitter=pEntity->GetParticleEmitter(10);
				if(pEmitter)
					gEnv->pParticleManager->DeleteEmitter(pEmitter);

				DataFreezeTargets.erase(iter++);
				end=DataFreezeTargets.end();
			}
			else
				++iter;
		}
	}
}


void DataFreeze::Add_To_List(HitInfo &info)
{
	DataFreezeParams tParams;
	tParams.last_time=1;
	tParams.tick_timer=0;

	DataFreezeTargets[info.targetId]=tParams;

	IEntity *pEntity=gEnv->pEntitySystem->GetEntity(info.targetId);
	IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Code_System.DeleteEntity");
	pEntity->LoadParticleEmitter(10,pEffect);
}