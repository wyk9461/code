#ifndef _VIRUSMANAGER_H_
#define _VIRUSMANAGER_H_

#include "StdAfx.h"

struct VirusParams
{
public:
	float damage;
	float tick_timer;
	float InfectionTime;
	IParticleEmitter *pEmitter;
	IEntity *pHost;
};

struct InfectionVirusParams : public VirusParams
{
	float damage;
};

struct ReplicationVirusParams : public VirusParams
{	
	float mass;
};

class VirusManager
{
public:
	VirusManager(int _child_count, int _max_child_count)
	:	child_count(_child_count)
	,	max_child_count(_max_child_count)
	,	pInfectedEffect(NULL){}

	virtual void Update(float frameTime){};
	virtual void AddNewCoreVirus(VirusParams &params);
protected:
	IParticleEffect *pInfectedEffect;
	int child_count;
	int max_child_count;
	std::list<VirusParams*> InfectionList;
	std::set<EntityId> InfactedEntities;
	virtual void AddNewChildVirus(VirusParams &params);
	virtual void SplitVirus(VirusParams &params){};
};

//-----------------------------------------------------------------------------------------------------------------------------------
class InfectionVirusManager : public VirusManager
{
public:
	static InfectionVirusManager &Instance(){static InfectionVirusManager instance;return instance;}
	virtual void Update(float frameTime);
	virtual void AddNewCoreVirus(VirusParams &params);
	const IParticleEffect *GetInfectedEffect(){return pInfectedEffect;}

	~InfectionVirusManager(){}
private:
	InfectionVirusManager()
	:	VirusManager(0,30)
	{
		pInfectedEffect = gEnv->pParticleManager->FindEffect("EntityProjectile.Virus.InfectionVirus_Infected");
	}
	virtual void AddNewChildVirus(VirusParams &params);
	virtual void SplitVirus(VirusParams &params);
};

//-----------------------------------------------------------------------------------------------------------------------------------
class ReplicationVirusManager : public VirusManager
{
public:
	static ReplicationVirusManager &Instance(){static ReplicationVirusManager instance;return instance;}
	~ReplicationVirusManager(){}

	virtual void Update(float frameTime);
	virtual void AddNewCoreVirus(VirusParams &params);
	const IParticleEffect *GetInfectedEffect(){return pInfectedEffect;}
private:
	ReplicationVirusManager()
		:	VirusManager(0,15)
	{
		pInfectedEffect = gEnv->pParticleManager->FindEffect("EntityProjectile.Virus.InfectionVirus_Infected");
	}
	virtual void AddNewChildVirus(VirusParams &params);
	virtual void SplitVirus(VirusParams &params){};
	void SplitVirus(IEntity *pEntity, ReplicationVirusParams &params);
	void ClearVirus();
	IEntity *ReplicateEntity(IEntity *pEntity);
};



#endif