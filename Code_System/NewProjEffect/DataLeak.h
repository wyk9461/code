#ifndef _DATALEAK_H_
#define _DATALEAK_H_

#include "StdAfx.h"

struct DataLeakParams
{
	float last_time;
	float tick_timer;
	float fake_timer;
	float damage;
	HitInfo LeakInfo;
	const bool operator<(const DataLeakParams &dlp){return tick_timer<dlp.tick_timer;}

	DataLeakParams &operator=(const DataLeakParams &dlp)
	{
		if(this==&dlp)
			return *this;
		last_time=dlp.last_time;
		tick_timer=dlp.tick_timer;
		fake_timer=dlp.fake_timer;
		damage=dlp.damage;
		LeakInfo=dlp.LeakInfo;
		return *this;
	}


	DataLeakParams()
		:	last_time(0)
		,	tick_timer(0)
		,	damage(0)
		,	fake_timer(0){}
};


class DataLeak
{
private:
	typedef std::map<EntityId , DataLeakParams> LeakMap;
	LeakMap DataLeakTargets;
public:
	DataLeak(){}
	~DataLeak(){DataLeakTargets.clear();}
	void Update(const float frameTime);
	void Add_To_List(HitInfo &info , float fValue = 0);
};

#endif