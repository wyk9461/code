#include "StdAfx.h"
#include "DataLeak.h"

void DataLeak::Update(const float frameTime)
{
	if(!DataLeakTargets.empty())
	{
		LeakMap::iterator iter=DataLeakTargets.begin();
		LeakMap::iterator end=DataLeakTargets.end();
		while(iter != end)
		{
			DataLeakParams & params=iter->second;
			if(params.tick_timer<=params.last_time)
			{
				if(params.tick_timer>=params.fake_timer)
				{
					IActor *pActor=g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(iter->first);
					if(pActor && !pActor->IsDead())
					{
						float curHealth=pActor->GetHealth();
						if(curHealth - params.damage > 0)
						{
							curHealth-=params.damage;
							pActor->SetHealth(curHealth);
						}
						else
							g_pGame->GetGameRules()->ClientHit(params.LeakInfo);			
					}
					params.fake_timer+=0.5;
				}
				params.tick_timer+=frameTime;
			}
			if(params.tick_timer>params.last_time)
			{
				IEntity *pEntity=gEnv->pEntitySystem->GetEntity(iter->first);
				IParticleEmitter *pEmitter=pEntity->GetParticleEmitter(10);
				if(pEmitter)
					pEmitter->Activate(false);
				DataLeakTargets.erase(iter++);
				end=DataLeakTargets.end();
			}
			else
				++iter;
		}
	}
}

void DataLeak::Add_To_List(HitInfo &info , float fValue /* = 0 */)
{
	DataLeakParams tParams;
	tParams.damage=static_cast<float>(info.damage*0.3);		//Need to Change later
	if(fValue == 0)
		tParams.last_time=3;		//Need to Change later
	else
		tParams.last_time=fValue;
	tParams.tick_timer=0;
	tParams.fake_timer=tParams.tick_timer+=0.5;

	tParams.LeakInfo=info;
	tParams.LeakInfo.knocksDown=false;
	tParams.LeakInfo.knocksDownLeg=false;
	tParams.LeakInfo.pos=Vec3(0,0,0);
	tParams.LeakInfo.damage=tParams.damage;
	tParams.LeakInfo.projectileId=0;

	DataLeakTargets[info.targetId]=tParams;

	IEntity *pEntity=gEnv->pEntitySystem->GetEntity(info.targetId);
	IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Projectile_Effect.DataLeak");
	pEntity->LoadParticleEmitter(10,pEffect);
	IParticleEmitter *pEmitter=pEntity->GetParticleEmitter(10);
	if(pEmitter)
		pEmitter->Activate(true);
}