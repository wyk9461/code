#include "StdAfx.h"
#include "DisableSystem.h"
#include "CryPath.h"
#include "ISystem.h"
#include "Code_System/WorldStability.h"
#include "Code_System/ScriptModify_AI.h"
#include "ItemSystem.h"

DisableSystem::DisableSystem()
{
	pWorld=gEnv->pPhysicalWorld;
	hits=0;
	query_flag=ent_rigid | ent_sleeping_rigid | ent_living | ent_independent;
	StartFunction=false;
	pRenderer=gEnv->pRenderer;
	curindex=0;
	DIS[curindex].type=ETT_LAST;
	DIS[curindex].mode=EDM_DisableLegs;
	MaxMode=EDM_LAST;
	Maxnumber=5;
	DisableLegsTime=10.0f;
	DisableArmsTime=5.0f;
	DisableEyesTime=8.0f;
	DisableMindTime=10.0f;
	ChangeMindTime=10.0f;
	WeakLegsTime=7.0f;
	WeakArmsTime=6.0f;
	DisableEngineTime=10.0f;
	DisableAWheelTime=0.5f;
	DisableDriverTime=8.0f;
	for(int i=0;i<5;i++)
		DisableSlot[i]=EDM_LAST;
	DisableSlot[1]=EDM_DisableLegs;
}

void DisableSystem::Update(float frametime)
{
	if(StartFunction)
	{
		IActor *pSelfActor=g_pGame->GetIGameFramework()->GetClientActor();
		IEntity *pSelf=pSelfActor->GetEntity();
		IMovementController *pMC=pSelfActor->GetMovementController();
		SMovementState info;
		pMC->GetMovementState(info);
		int rayFlags(rwi_stop_at_pierceable|rwi_colltype_any);
		IPhysicalEntity *pSelfPhysics=pSelf->GetPhysics();
		hits = pWorld->RayWorldIntersection(info.eyePosition,info.eyeDirection*30.f,query_flag,rayFlags,&hit,1,&pSelfPhysics,1);
		const float color[ 4 ] = { 0.258824f, 0.258824f, 0.435294f,1.0f};
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Stability  : %.0f",g_pGame->GetWorldStability()->Getstability());
		DrawDisableMode();
		if(hit.pCollider)
		{
			DIS[curindex].DisableEntity=(IEntity*)hit.pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);
			if(DIS[curindex].DisableEntity)
				pRenderer->DrawLabel(hit.pt,1.5f,"%s",DIS[curindex].DisableEntity->GetClass()->GetName());
		}
	}
	/*if(NeedLockLabel())
		DrawLabel();*/
	if(NeedPostUpdate())
		PostUpdate(frametime);
}

void DisableSystem::PostUpdate(float frametime)
{
	for(int i=0;i<Maxnumber;i++)
	{
		if(!DIS[i].available && !DIS[i].finished)
		{
			DIS[i].DisableTimer+=frametime;
			DIS[i].Progress+=DIS[i].DisableRate*frametime;
			if(DIS[i].DisableTimer < DIS[i].TotalTime)
				pRenderer->DrawLabel(DIS[i].DisableEntity->GetPos(),1.5f,"Disabling...%.0f%%",DIS[i].Progress);
			if(DIS[i].DisableTimer >= DIS[i].TotalTime)
			{
				DIS[i].finished=true;
				DIS[i].DisableTimer=0;
				DIS[i].TotalTime=SetActiveTime(i);    //Need to Chnage;
				DIS[i].disabling=false;
				ClearDIS(i);
			}
		}
		if(DIS[i].finished)
		{
			DIS[i].DisableTimer+=frametime;
			if(DIS[i].DisableTimer < DIS[i].TotalTime)
				ApplyChange(i);
			else
			{
				DIS[i].available=true;
				DIS[i].finished=false;
				DIS[i].locked=false;
				if(DIS[i].mode==EDM_DisableArms)
					AfterDisableArms(i);
				if(DIS[i].mode==EDM_DisableEyes)
					AfterDisableEyes(i);
				if(DIS[i].mode==EDM_DisableMind)
					AfterDisableMind(i);
				if(DIS[i].mode==EDM_WeakLegs)
					AfterWeakLegs(i);
				if(DIS[i].mode==EDM_WeakArms)
					AfterWeakArms(i);
				if(DIS[i].mode==EDM_DisableMind)
					AfterDisableMind(i);
				if(DIS[i].mode==EDM_ChangeMind)
					AfterChangeMind(i);
				if(DIS[i].mode==EDM_DisableEngine)
					AfterDisableEngine(i);
				if(DIS[i].mode==EDM_DisableDriver)
					AfterDisableDriver(i);
				LockedEntity.erase(DIS[i].DisableEntity->GetId());
			}
		}
	}
}

void DisableSystem::ApplyChange(const int index)
{
	if(DIS[index].type==ETT_AI)
	{
		if(DIS[index].mode == EDM_DisableLegs)
			DisableLegs(index);
		if(DIS[index].mode == EDM_DisableArms)
			DisableArms(index);
		if(DIS[index].mode==EDM_DisableEyes)
			DisableEyes(index);
		if(DIS[index].mode==EDM_WeakLegs)
			WeakLegs(index);
		if(DIS[index].mode==EDM_WeakArms)
			WeakArms(index);
		if(DIS[index].mode==EDM_DisableMind)
			DisableMind(index);
		if(DIS[index].mode==EDM_ChangeMind)
			ChangeMind(index);
	}
	if(DIS[index].type==ETT_Vehicle)
	{
		if(DIS[index].mode==EDM_DisableEngine)
			DisableEngine(index);
		if(DIS[index].mode==EDM_DisableAWheel)
			DisableAWheel(index);
		if(DIS[index].mode==EDM_DisableDriver)
			DisableDriver(index);
	}
}


void DisableSystem::PerApply(const int SlotIndex)
{
	if(DisableSlot[SlotIndex] != EDM_LAST)
		DIS[curindex].mode=DisableSlot[SlotIndex];
	SetTotalTime(curindex);
	DIS[curindex].available=false;
	DIS[curindex].finished=false;
	DIS[curindex].firstset=true;
	DIS[curindex].disabling=true;
	DIS[curindex].DisableTimer=0;
	DIS[curindex].Progress=0;
	DIS[curindex].DisableRate=100/DIS[curindex].TotalTime;
	g_pGame->GetConnectMode()->SetLevel(eConnectMode::eCLevel::eCLevel_ChooseSystem);
}


void DisableSystem::PreApply_viaMenu(const int mode)
{
	DIS[curindex].mode=mode;
	SetTotalTime(curindex);
	DIS[curindex].available=false;
	DIS[curindex].finished=false;
	DIS[curindex].firstset=true;
	DIS[curindex].disabling=true;
	DIS[curindex].DisableTimer=0;
	DIS[curindex].Progress=0;
	DIS[curindex].DisableRate=100/DIS[curindex].TotalTime;
	g_pGame->GetConnectMode()->SetLevel(eConnectMode::eCLevel::eCLevel_ChooseSystem);
}



void DisableSystem::SetTotalTime(const int i)
{
	if(DIS[i].type==ETT_AI)
	{
		if(DIS[i].mode == EDM_DisableLegs)
			DIS[i].TotalTime=3.0f;
		else if(DIS[i].mode == EDM_DisableArms)
			DIS[i].TotalTime=1.0f;
		else if(DIS[i].mode==EDM_DisableEyes)
			DIS[i].TotalTime=1.5f;
		else if(DIS[i].mode==EDM_DisableMind)
			DIS[i].TotalTime=4.0f;
		else if(DIS[i].mode==EDM_ChangeMind)
			DIS[i].TotalTime=5.0f;
		else if(DIS[i].mode==EDM_WeakLegs)
			DIS[i].TotalTime=1.0f;
		else if(DIS[i].mode==EDM_WeakArms)
			DIS[i].TotalTime=1.3f;
		else
			DIS[i].TotalTime=0.1f;
	}
	if(DIS[i].type==ETT_Vehicle)
	{
		if(DIS[i].mode==EDM_DisableEngine)
			DIS[i].TotalTime=2.0f;
		if(DIS[i].mode==EDM_DisableAWheel)
			DIS[i].TotalTime=1.0f;
		if(DIS[i].mode==EDM_DisableDriver)
			DIS[i].TotalTime=2.0f;
	}
}


float DisableSystem::SetActiveTime(const int i)
{
	if(DIS[i].mode==EDM_DisableLegs)
		return DisableLegsTime;
	if(DIS[i].mode==EDM_DisableArms)
		return DisableArmsTime;
	if(DIS[i].mode==EDM_DisableEyes)
		return DisableEyesTime;
	if(DIS[i].mode==EDM_DisableMind)
		return DisableMindTime;
	if(DIS[i].mode==EDM_ChangeMind)
		return ChangeMindTime;
	if(DIS[i].mode==EDM_WeakLegs)
		return WeakLegsTime;
	if(DIS[i].mode==EDM_WeakArms)
		return WeakArmsTime;
	if(DIS[i].mode==EDM_DisableEngine)
		return DisableEngineTime;
	if(DIS[i].mode==EDM_DisableAWheel)
		return DisableAWheelTime;
	if(DIS[i].mode==EDM_DisableDriver)
		return DisableDriverTime;
	return 0.0f;
}



void DisableSystem::ChooseTargetType(const int i)
{
	DIS[i].className=DIS[i].DisableEntity->GetClass()->GetName();
	if(DIS[i].className == "Human")
		DIS[i].type=ETT_AI;
	if(DIS[i].className == "HMMWV" || DIS[i].className == "Abrams" || DIS[i].className == "MH60_Blackhawk" || DIS[i].className == "SpeedBoat")
		DIS[i].type=ETT_Vehicle;
}

int DisableSystem::FindAvailable()
{
	if(DIS[curindex].available && curindex!=-1 && !DIS[curindex].locked)
		return curindex;
	for(int i=0;i<Maxnumber;i++)
		if(DIS[i].available && !DIS[i].locked)
			return i;
	return -1;
}

void DisableSystem::DrawDisableMode()
{
	const float AIModeClr[ 4 ] = { 0.137255f, 0.137255f, 0.556863f,1.0f};
	const float VehicleModeClr[ 4 ] = { 0.0f, 0.30f, 0.75f,1.0f};
	if(DIS[curindex].mode==EDM_DisableLegs)
		pRenderer->Draw2dLabel(50,75,1.5f,AIModeClr,false,"DisableMode : DisableLegs");
	if(DIS[curindex].mode==EDM_DisableArms)
		pRenderer->Draw2dLabel(50,75,1.5f,AIModeClr,false,"DisableMode : DisableArms");
	if(DIS[curindex].mode==EDM_DisableEyes)
		pRenderer->Draw2dLabel(50,75,1.5f,AIModeClr,false,"DisableMode : DisableEyes");
	if(DIS[curindex].mode==EDM_DisableMind)
		pRenderer->Draw2dLabel(50,75,1.5f,AIModeClr,false,"DisableMode : DisableMind");
	if(DIS[curindex].mode==EDM_ChangeMind)
		pRenderer->Draw2dLabel(50,75,1.5f,AIModeClr,false,"DisableMode : ChangeMind");
	if(DIS[curindex].mode==EDM_WeakLegs)
		pRenderer->Draw2dLabel(50,75,1.5f,AIModeClr,false,"DisableMode : WeakLegs");
	if(DIS[curindex].mode==EDM_WeakArms)
		pRenderer->Draw2dLabel(50,75,1.5f,AIModeClr,false,"DisableMode : WeakArms");
	if(DIS[curindex].mode==EDM_DisableEngine)
		pRenderer->Draw2dLabel(50,75,1.5f,VehicleModeClr,false,"DisableMode : DisableEngine");
	if(DIS[curindex].mode==EDM_DisableAWheel)
		pRenderer->Draw2dLabel(50,75,1.5f,VehicleModeClr,false,"DisableMode : DisableAWheel");
	if(DIS[curindex].mode==EDM_DisableDriver)
		pRenderer->Draw2dLabel(50,75,1.5f,VehicleModeClr,false,"DisableMode : DisableDriver");
}

void DisableSystem::DrawLabel()
{
	for(int i=0;i<Maxnumber;i++)
		if(DIS[i].locked && !DIS[i].disabling)
		{
			if(DIS[i].mode==EDM_DisableLegs)
				pRenderer->DrawLabel(DIS[i].DisableEntity->GetPos(),1.5f,"DisableLegs");
			if(DIS[i].mode==EDM_DisableArms)
				pRenderer->DrawLabel(DIS[i].DisableEntity->GetPos(),1.5f,"DisableArms");
			if(DIS[i].mode==EDM_DisableEyes)
				pRenderer->DrawLabel(DIS[i].DisableEntity->GetPos(),1.5f,"DisableEyes");
			if(DIS[i].mode==EDM_DisableMind)
				pRenderer->DrawLabel(DIS[i].DisableEntity->GetPos(),1.5f,"DisableMind");
			if(DIS[i].mode==EDM_ChangeMind)
				pRenderer->DrawLabel(DIS[i].DisableEntity->GetPos(),1.5f,"ChangeMind");
			if(DIS[i].mode==EDM_WeakLegs)
				pRenderer->DrawLabel(DIS[i].DisableEntity->GetPos(),1.5f,"WeakLegs");
			if(DIS[i].mode==EDM_WeakArms)
				pRenderer->DrawLabel(DIS[i].DisableEntity->GetPos(),1.5f,"WeakArms");
			if(DIS[i].mode==EDM_DisableEngine)
				pRenderer->DrawLabel(DIS[i].DisableEntity->GetPos(),1.5f,"DisableEngine");
			if(DIS[i].mode==EDM_DisableAWheel)
				pRenderer->DrawLabel(DIS[i].DisableEntity->GetPos(),1.5f,"DisableAWheel");
			if(DIS[i].mode==EDM_DisableDriver)
				pRenderer->DrawLabel(DIS[i].DisableEntity->GetPos(),1.5f,"DisableDriver");
		}
}

bool DisableSystem::NeedPostUpdate()
{
	for(int i=0;i<Maxnumber;i++)
		if(!DIS[i].available)
			return true;
	return false;
}

bool DisableSystem::NeedLockLabel()
{
	for(int i=0;i<Maxnumber;i++)
		if(DIS[i].locked)
			return true;
	return false;
}

bool DisableSystem::CompareType(const int i)
{
	if(DIS[i].type==ETT_AI)
		if(DIS[i].mode==EDM_DisableEngine)
			return false;
	if(DIS[i].type==ETT_Vehicle)
		if(DIS[i].mode==EDM_DisableLegs || DIS[i].mode==EDM_DisableArms || DIS[i].mode==EDM_DisableEyes
			|| DIS[i].mode==EDM_DisableMind || DIS[i].mode==EDM_ChangeMind || DIS[i].mode==EDM_WeakLegs
			|| DIS[i].mode==EDM_WeakArms)
			return false;
	return true;
}

//Disable Legs
void DisableSystem::DisableLegs(const int i)
{
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(DIS[i].DisableEntity->GetId());
	if(!pActor->IsFallen())
		pActor->Fall();
}

//DisableArms
void DisableSystem::DisableArms(const int i)
{
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(DIS[i].DisableEntity->GetId());
	if(pActor->GetCurrentItemId())
	{
		DIS[i].DropItems.push_back(pActor->GetCurrentItemId());
		pActor->DropItem(pActor->GetCurrentItemId());
	}
}

//DisableEyes
void DisableSystem::DisableEyes(const int i)
{
	if(DIS[i].firstset)
	{
		CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(DIS[i].DisableEntity->GetId());
		float health=pActor->GetHealth();
		SmartScriptTable root=DIS[i].DisableEntity->GetScriptTable();
		SmartScriptTable props;
		SmartScriptTable perce;
		root->GetValue("Properties",props);
		props->GetValue("Perception",perce);
		perce->GetValue("FOVPrimary	",DIS[i].defViewFOV);
		perce->GetValue("sightrange",DIS[i].defSightRange);
		perce->SetValue("FOVPrimary	",0);
		perce->SetValue("sightrange",0);
		Script::CallMethod(root,"OnReset");
		pActor->SetHealth(health);
		DIS[i].firstset=false;
	}
}

//DisableMind
void DisableSystem::DisableMind(const int i)
{
	if(DIS[i].firstset)
	{
		CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(DIS[i].DisableEntity->GetId());
		SmartScriptTable root=DIS[i].DisableEntity->GetScriptTable();
		SmartScriptTable props;
		float health=pActor->GetHealth();
		string behavior("Civilian");
		root->GetValue("Properties",props);
		props->GetValue("esFaction",DIS[i].defBehaviorTree);
		props->SetValue("esFaction",behavior.c_str());
		Script::CallMethod(root,"OnReset");
		pActor->SetHealth(health);
		DIS[i].firstset=false;
	}
}

//ChangeMind
void DisableSystem::ChangeMind(const int i)
{
	if(DIS[i].firstset)
	{
		CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(DIS[i].DisableEntity->GetId());
		SmartScriptTable root=DIS[i].DisableEntity->GetScriptTable();
		SmartScriptTable props;
		float health=pActor->GetHealth();
		string faction("Players");
		root->GetValue("Properties",props);
		props->GetValue("esFaction",DIS[i].defFaction);
		props->SetValue("esFaction",faction.c_str());
		Script::CallMethod(root,"OnReset");
		pActor->SetHealth(health);
		DIS[i].firstset=false;
	}
}

//WeakLegs
void DisableSystem::WeakLegs(const int i)
{
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(DIS[i].DisableEntity->GetId());
	DIS[i].defSpeedMulti=pActor->GetSpeedMultiplier(SActorParams::eSMR_Internal);
	pActor->SetSpeedMultipler(SActorParams::eSMR_Internal,0.25f);
	pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,0.25f);
	pActor->SetSpeedMultipler(SActorParams::eSMR_Item,0.25f);
}

//WeakArms
void DisableSystem::WeakArms(const int i)
{
	if(DIS[i].firstset)
	{
		CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(DIS[i].DisableEntity->GetId());
		float health=pActor->GetHealth();
		SmartScriptTable root=DIS[i].DisableEntity->GetScriptTable();
		SmartScriptTable props;
		SmartScriptTable RoD;
		root->GetValue("Properties",props);
		props->GetValue("RateOfDeath",RoD);
		RoD->GetValue("accuracy",DIS[i].defAccuracy);
		RoD->SetValue("accuracy",0);
		Script::CallMethod(root,"OnReset");
		pActor->SetHealth(health);
		DIS[i].firstset=false;
	}
}

//DisableEngine
void DisableSystem::DisableEngine(const int i)
{
	IVehicle *pVehicle=g_pGame->GetIGameFramework()->GetIVehicleSystem()->GetVehicle(DIS[i].DisableEntity->GetId());
	IVehicleMovement *pMovement=pVehicle->GetMovement();
	pMovement->DisableEngine(true);
}

//DisableAWheel
void DisableSystem::DisableAWheel(const int i)
{
	if(DIS[i].firstset)
	{
		IVehicle *pVehicle=g_pGame->GetIGameFramework()->GetIVehicleSystem()->GetVehicle(DIS[i].DisableEntity->GetId());
		int maxwheel=pVehicle->GetWheelCount();
		int wheelindex=(int)(rand()%maxwheel);
		IVehiclePart *part=pVehicle->GetWheelPart(wheelindex);
		IVehiclePart::SVehiclePartEvent partEvent;
		partEvent.type = IVehiclePart::eVPE_Damaged;
		partEvent.fparam=1000;
		part->OnEvent(partEvent);
		DIS[i].firstset=false;
	}
}

//DisableDriver
//CE has Bug
void DisableSystem::DisableDriver(const int i)
{
	IVehicle *pVehicle=g_pGame->GetIGameFramework()->GetIVehicleSystem()->GetVehicle(DIS[i].DisableEntity->GetId());
	IVehicleSeat *pDriver=pVehicle->GetSeatById(pVehicle->GetSeatId("driver"));
	DIS[i].DriverID=pDriver->GetPassenger();
	if(DIS[i].DriverID)
		pDriver->Exit(true,true);
}

void DisableSystem::AfterDisableEyes(const int i)
{
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(DIS[i].DisableEntity->GetId());
	if(pActor->IsDead())
		return;
	float health=pActor->GetHealth();
	SmartScriptTable root=DIS[i].DisableEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable perce;
	root->GetValue("Properties",props);
	props->GetValue("Perception",perce);
	perce->SetValue("FOVPrimary	",DIS[i].defViewFOV);
	perce->SetValue("sightrange",DIS[i].defSightRange);
	Script::CallMethod(root,"OnReset");
	pActor->SetHealth(health);
	DIS[i].firstset=false;
}

void DisableSystem::AfterWeakLegs(const int i)
{
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(DIS[i].DisableEntity->GetId());
	if(pActor->IsDead())
		return;
	pActor->SetSpeedMultipler(SActorParams::eSMR_Internal,1.0f);
	pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules,1.0f);
	pActor->SetSpeedMultipler(SActorParams::eSMR_Item,1.0f);
}


void DisableSystem::AfterDisableArms(const int i)
{
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(DIS[i].DisableEntity->GetId());
	if(pActor->IsDead())
		return;
	for(std::vector<EntityId>::iterator it=DIS[i].DropItems.begin();it!=DIS[i].DropItems.end();++it)
		pActor->PickUpItem(*it,false,true);
}

void DisableSystem::AfterWeakArms(const int i)
{
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(DIS[i].DisableEntity->GetId());
	if(pActor->IsDead())
		return;
	float health=pActor->GetHealth();
	SmartScriptTable root=DIS[i].DisableEntity->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable RoD;
	root->GetValue("Properties",props);
	props->GetValue("RateOfDeath",RoD);
	RoD->SetValue("accuracy",DIS[i].defAccuracy);
	Script::CallMethod(root,"OnReset");
	pActor->SetHealth(health);
	DIS[i].firstset=false;
}

void DisableSystem::AfterDisableMind(const int i)
{
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(DIS[i].DisableEntity->GetId());
	if(pActor->IsDead())
		return;
	IAIObject *pAI=DIS[i].DisableEntity->GetAI();
	SmartScriptTable root=DIS[i].DisableEntity->GetScriptTable();
	SmartScriptTable props;
	root->GetValue("Properties",props);
	float health=pActor->GetHealth();
	props->SetValue("esFaction",DIS[i].defBehaviorTree);
	Script::CallMethod(root,"OnReset");
	pActor->SetHealth(health);
}

void DisableSystem::AfterChangeMind(const int i)
{
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(DIS[i].DisableEntity->GetId());
	if(pActor->IsDead())
		return;
	IAIObject *pAI=DIS[i].DisableEntity->GetAI();
	SmartScriptTable root=DIS[i].DisableEntity->GetScriptTable();
	SmartScriptTable props;
	root->GetValue("Properties",props);
	float health=pActor->GetHealth();
	props->SetValue("esFaction",DIS[i].defFaction);
	Script::CallMethod(root,"OnReset");
	pActor->SetHealth(health);
}

void DisableSystem::AfterDisableEngine(const int i)
{
	IVehicle *pVehicle=g_pGame->GetIGameFramework()->GetIVehicleSystem()->GetVehicle(DIS[i].DisableEntity->GetId());
	IVehicleMovement *pMovement=pVehicle->GetMovement();
	pMovement->DisableEngine(false);
}

void DisableSystem::AfterDisableDriver(const int i)
{
	CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(DIS[i].DriverID);
	if(!pActor->IsDead())
	{
		IVehicle *pVehicle=g_pGame->GetIGameFramework()->GetIVehicleSystem()->GetVehicle(DIS[i].DisableEntity->GetId());
		IVehicleSeat *pDriver=pVehicle->GetSeatById(pVehicle->GetSeatId("driver"));
		pDriver->Enter(DIS[i].DriverID);
	}
}


const bool  DisableSystem::ProcessDisableEntity(IEntity *DEntity)
{
	if(IsDisabled(DEntity))
		return false;
	curindex = FindAvailable();
	if(!DIS[curindex].available || curindex == -1)
		return false;
	DIS[curindex].DisableEntity=DEntity;
	ChooseTargetType(curindex);
	g_pGame->GetConnectMode()->SetLevel(eConnectMode::eCLevel::eCLevel_Disable_ChooseSlot);
	CryLogAlways("DisableMode_SlotSelection");
	return true;
}


const bool DisableSystem::IsDisabled(IEntity *pEntity)
{
	for (int i=0;i<10;++i)
	{
		if(DIS[i].DisableEntity && pEntity)
			if(DIS[i].DisableEntity->GetId() == pEntity->GetId())
				return true;
	}
	return false;
}

void DisableSystem::ClearDIS(const int index)
{
	DIS[index].DisableEntity=NULL;
	DIS[index].DriverID = 0;
	DIS[index].type = 0;
	DIS[index].mode = EDM_LAST;
	DIS[index].className = "";
	DIS[index].DisableTimer = 0;
	DIS[index].TotalTime = 0;
	DIS[index].DisableRate = 0;
	DIS[index].Progress = 0;
	DIS[index].defViewFOV = 60;
	DIS[index].defSightRange = 150;
	DIS[index].defAccuracy = 1;
	DIS[index].defSpeedMulti = 1;
	DIS[index].finished = false;
	DIS[index].firstset = true;
	DIS[index].locked = false;
	DIS[index].disabling = false;
	DIS[index].available= true;
	DIS[index].defBehaviorTree ="";
	DIS[index].defFaction = "";
}