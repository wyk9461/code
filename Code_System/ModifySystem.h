#ifndef _MODIFYSYSTEM_H_
#define _MODIFYSYSTEM_H_

#include "Code_System\EntityParamsUtils.h"
#include "ModifySlot\ModifierManager.h"
#include "IFlashUI.h"

class CModifyMenuListener : public IUIElementEventListener
{
public:
	virtual void OnUIEvent( IUIElement* pSender, const SUIEventDesc& event, const SUIArguments& args );
};


class ModifySystem
{
	private:
		// Description : 
		//		What type of entity is been modified.
		struct ModifyParams
		{
			IEntity *MEntity;
			CItem *pItem;
			CActor *pActor;
			int BasicEntityIndex;
			const int MaxBasicIndex;
			int DestroyableObjectIndex;
			const int MaxDestroyableObjectIndex;
			int VehiclesIndex;
			const int MaxVehiclesIndex;
			int LightIndex;
			const int MaxLightIndex;
			int WeaponIndex;
			int MaxWeaponIndex;
			int PlayerWeaponIndex;
			int MaxPlayerWeaponIndex;
			//--------DestroyableObject--------//
			SmartScriptTable props;
			SmartScriptTable physics_props;
			SmartScriptTable explosion_props;
			SmartScriptTable health_props;
			//--------Vehicles--------//
			bool disablephysics;
			bool defaultxml;
			//--------Light--------//
			SmartScriptTable pScript;
			SmartScriptTable Damage;
			SmartScriptTable PropertiesInstance;
			SmartScriptTable LightParams;
			SmartScriptTable Style;
			SmartScriptTable Color;
			//--------Weapon--------//

			//Common
			int cost;
			float WeaponTimer;
			float VehicleTimer;
			bool needupdate;
			bool WeaponInTime;
			bool VehicleInTime;
			IVehicle *pVehicle;
			float sharedName;

			EntityParamsUtils::BasicPhysParams BPP;
			EntityParamsUtils::ExplosiveParams EP;
			EntityParamsUtils::LandVehiceParams LVP;
			EntityParamsUtils::LightParams LP;
			EntityParamsUtils::BasicWeaponParams BWP;
			EntityParamsUtils::ShotgunParams SGP;


			ModifyParams()
				: MEntity(NULL)
				, disablephysics(false)
				, defaultxml(false)
				, needupdate(false)
				, cost(0)
				, WeaponInTime(false)
				, VehicleInTime(false)
				, BasicEntityIndex(0)
				, MaxBasicIndex(2)  //2 means 3 params
				, DestroyableObjectIndex(0)
				, MaxDestroyableObjectIndex(12)
				, VehiclesIndex(0)
				, sharedName(0.0f)
				, MaxVehiclesIndex(10)
				, LightIndex(0)
				, MaxLightIndex(21)
				, WeaponIndex(0)
				, MaxWeaponIndex(3)
				, PlayerWeaponIndex(0)
				, MaxPlayerWeaponIndex(2){}
			ModifyParams operator=(const ModifyParams &MP);
		};

		struct CustomizeParams
		{
			int EntityClassindex;
			bool Available;
			bool Selected;
			EntityParamsUtils::BasicPhysParams cBPP;
			EntityParamsUtils::ExplosiveParams cEP;
			EntityParamsUtils::LandVehiceParams cLVP;
			EntityParamsUtils::LightParams cLP;
			EntityParamsUtils::BasicWeaponParams cBWP;
			EntityParamsUtils::ShotgunParams cSGP;

			CustomizeParams()
			:	EntityClassindex(0)
			,	Available(false)
			,	Selected(false){}
		};

		ray_hit hit;
		int hits;
		IPhysicalWorld *pWorld;
		int query_flag;
		IEntity *LastEntity;
		IEntity *pTarget;
		IMaterial *LastEntityMtl;
		IMaterial *DefaultDeleteMtl;
		XmlString className;
		string drawstring;

		EntityClassPreLoad preLoad;



		//New Mode
		IEntity *pEntity;
		ModifyID ModifySlot[6];
		ModifierManager *Modifiers;
		//~New Mode



		//--------Function State--------//
		bool StartFunction;
		bool EntityLocked;
		bool HasApply;
		bool NeedParams;
		bool isplayer;
		bool OnLocking;
		bool HasLockedOnce;
		bool preStore;
		
		int Maxnumber;
		int curindex;
		int lastindex;
		int modifymode;
		int modifytype;
		short SlotIndex;

		string store_weapon_name;
		float Locking_timer;
		float Locking_rate;
		float Locking_progress;
		float Locking_time;

		float WeaponModifyLastTime;
		float VehicleModifyLastTime;
		float defWeaponModifyLastTime;
		float defVehicleModifyLastTime;
		//--------Time Scale--------//
		float timescale_timer;
		float max_timescale_timer;
		float timescale_rate;
		float fade_timescale_timer;
		ICVar * timeScaleCVar;

		//--------UI--------//
		IUIElement *pElement;
		CModifyMenuListener UIListener;

		//--------MultiModify--------//
		bool bMultiModify;
		std::list<EntityId> MultiModifyList;
		void GetBasicEntityParams();
		void GetDestroyableObjectParams();
		void GetVehicleParams();
		void GetLightParams();
		void GetNormalWeaponParams(string weaponName);

		void AfterModify(const int type);
		void ReloadItems();
		void ClearMP();

		void InsertModifierToUIMenu(IEntityClass *pEntityClass);

		void InsertModifier(int _begin, int _end);
		void InsertPhysicsClassification();

		void InsertDestroyableClassification();

		void InsertVehicleClassification();

		void InsertHumanClassification();

		void InsertTrapClassification();

		void InsertLeakHoleClassification();

		void InsertForceDeviceClassification();

		void InsertPowerBallClassification();

		void InsertForceFieldClassification();

		void FindSameEntityClassInRange(IEntity *pEntity);
	public:
		std::vector<EntityId> backpack;
		ModifyParams MP,defMP;
		ModifySystem();
		~ModifySystem(){};
		//--------Utils--------//
		void Update(float frametime);
		void StoreInventory();
		void RestoreInventory();
		//--------Modify Mode--------//

		// Description :
		//		Get the information of the entity which may be modified later (Level 0->1).
		//	Arguments : 
		//		MEntity - the entity which may be modified later.
		void ProcessModifyEntity(IEntity *MEntity);

		// Description :
		//		If the entity is an AI.
		//		For AI entity,we can only modify their weapon.
		//	Arguments : 
		//		className - the entity class name.
		bool AI_Type(string & className);

		// Description :
		//		Lock the entity to enter the Complex Modify Mode(Level 1->2).
		void LockEntity();

		// Description :
		//		Unlock the entity to return to the Modify Slot Mode(Level 2->1).
		void UnLockEntity();

		// Description :
		//		Apply modify.
		//	Arguments : 
		//		index - if we use slot,this represent the slot index.
		void ApplyChange(const long index = 0);

		// Description :
		//		Do some cleaning work.
		void PostApply();

		// Description :
		//		Calculate the stability cost(based on the modify value),if the entity is vehicle or weapon,it will also calculate the last time.
		//	Arguments : 
		//		type - the entity type.
		//		index - if we use slot,this represent the slot index.
		void CalculateCostandTime(const int type,const long index=0);

		// Description :
		//		May remove later
		void SetPlayerWeaponClass();

		// Description :
		//		Shutdown the modify system.Clear modify params
		void Shutdown();

		ModifierManager *GetModifierManager(){return Modifiers;}
		ModifierRecoveryManager *GetModifierRecoveryManager(){return Modifiers->GetModifierRecoveryManager();}

		void ShowMenu();

		void CloseMenu();
		//--------InLine--------//
		inline const bool IsFunctionStarted(){return StartFunction;}
		inline const bool IsEntityLocked(){return EntityLocked;}
		inline const bool HasApplied(){return HasApply;}
		inline const bool Isplayer(){return isplayer;}

		inline const int GetType(){return modifytype;}
		inline const short GetSlotIndex(){return SlotIndex;}
		inline const string GetclassName(){return className;}
		inline const float GetSharedName(){return MP.sharedName;}
		inline  IMaterial* GetDefDelMtl(){return DefaultDeleteMtl;}
		inline void SetStartFunction(bool enable){StartFunction = enable;}
		inline void SetEntityLock(bool enable){EntityLocked=enable;}
		inline void Setcurindex(int index){curindex=index;}
		inline void Setlasindex(int index){lastindex=index;}
		inline void SetIsPlayer(bool is){isplayer=is;}
		inline void SetNeedParams(bool enable){NeedParams=enable;}
		inline void ResetType(){modifytype=5;}
		inline void SetLockedOnce(const bool once){HasLockedOnce=once;}
		inline IEntity *GetModifiedEntity(){return MP.MEntity;}
		inline IUIElement *GetModifyMenu(){return pElement;}
		inline const bool IsMenuOpened(){if(pElement) return pElement->IsVisible(); return false;}
		inline bool GetSelectionMode(){return bMultiModify;}
		inline void SetSelectionMode(bool _mode);
};

#endif