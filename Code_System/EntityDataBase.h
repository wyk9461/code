#ifndef _ENTITYDATABASE_H_
#define _ENTITYDATABASE_H_

#include "EntityParamsUtils.h"


enum EntityType
{
	eET_BasicEntity = 1,
	eET_Destroyable,
	eET_Light,
	eET_Vehicle,
	eET_Weapon,
	eET_Functional,
};

struct UIEntityInfo
{
	XmlString Name;
	XmlString ClassName;
	int EntityType;
	int Mass;
	int Cost;
	int Autho;
	int ProjectileType;
	float Damage;
	float Radius;
	bool Favorite;

	UIEntityInfo()
	:	Name("")
	,	ClassName("")
	,	EntityType(eET_BasicEntity)
	,	Mass(-1)
	,	Cost(0)
	,	Autho(2)
	,	ProjectileType(0)
	,	Damage(0)
	,	Radius(0)
	,	Favorite(false){}
};

class CEntityDBListener : public IUIElementEventListener
{
public:
	virtual void OnUIEvent( IUIElement* pSender, const SUIEventDesc& event, const SUIArguments& args );
};

class EntityDataBase
{
private:
	EntityClassPreLoad preLoad;
	IUIElement *pElement;
	IUIElement *pCreateMenu;
	CEntityDBListener UIListener;
	EntityDataBase();
	int curListIndex;
	int curDBIndex;
	int curSlotDBIndex;

	Vec3 ProjectWorldPos;

	int SpawnSlot[6];

	bool ShouldInitEntityDB;

	void ScanEntityInfo(XmlNodeRef &root, int xml_index, UIEntityInfo &info);
	const int GetEntityType(XmlString &str) const;
public:
	static EntityDataBase& Instance(){static EntityDataBase db;return db;}
	void InitDataBase();
	void AddNewItem(int xml_index, XmlNodeRef &root);

	void OpenDataBase();
	void CloseDataBase();
	void OpenCreateMenu();
	void CloseCreateMenu();

	void SetFavorite();
	void SetStability(int sta);

	const bool IsDBOpened();
	const bool IsCreateMenuOpened();
	void SetCurListIndex(int _index){curListIndex = _index;}
	const int GetCurListIndex(){return curListIndex;}
	void SetCurDBIndex(int _index){curDBIndex = _index;}
	const int GetCurDBIndex(){return curDBIndex;}
	void SetCurSlotDBIndex(int _index){curSlotDBIndex = _index;}
	const int GetCurSlotDBIndex(){return curSlotDBIndex;}

	void SetProject(Vec3 pos){ProjectWorldPos = pos;}

	void SetSlotInfo(int slot_index, int db_index);
	int RetriveSlotInfo(int slot_index){return SpawnSlot[slot_index];}
};
#endif