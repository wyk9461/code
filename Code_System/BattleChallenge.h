#ifndef _BATTLECHALLENGE_H_
#define _BATTLECHALLENGE_H_

enum ChallangeType
{
	eCT_ChallengeCanceled = -2,
	eCT_NoChallenge = -1,
	eCT_Eliminate = 0,
	eCT_Survivor,
	eCT_Restrict,

};

enum EliminateType
{
	eET_WithWeapon,
	eET_WithAbility,
};

enum ReserictType
{
	eRT_NoFire,
	eRT_NoAbility,
	eRT_NoCreate,
	eRT_NoModify,
	eRT_NoDelete,

	eRT_Last,
};

struct Challenge_Params
{
	bool enable;
};

struct EliminateParams : public Challenge_Params
{
	unsigned int Type;
	unsigned int EmemyCount;
	std::set<EntityId> Targets;
	EntityId WeaponId;
	int AbilityType;
	int AbilitySlotIndex;
};

struct SurvivorParams : public Challenge_Params
{
	float CheckedHealth;
	float HealthLimit;
};

struct RestrictParams : public Challenge_Params
{
	int RestrictType;
	float tick_timer;
	float last_time;
};


class BattleChallenge
{
private:
	typedef unsigned int ChallengeId;
	BattleChallenge();
	~BattleChallenge(){}

	EliminateParams m_EChallange;
	SurvivorParams m_SChallange;
	RestrictParams m_RChallange;

	std::vector<EntityId> CurrentWeaponId;
	std::vector<int> CurrentAbilityType;
	std::vector<int> PossibleChallange;
	void ScanWeaponStatus(CActor *pPlayer);
	void ScanAbilityStatus();

	void InitChallengeParams();

	void GenerateEliminateChallenge(int enemyCount);
	void GenerateSurvivorChallenge(int enemyCount);
	void GenerateRestrictChallenge(int enemyCount);

	void UpdateEliminate(float frameTime);
	void UpdateSurvivor(float frameTime);
	void UpdateRestrict(float frameTime);

	void RewardEliminate();
	void RewardSurvivor();
	void RewardRestrict();

	void ClearEliminateChallenge();
	void ClearChallenge();

	float Reset_Timer;
	float Reset_Time;

	bool HasGeneratedChallenge;
	bool CanGenerateChallenge;
public:

	static BattleChallenge &Instance(){static BattleChallenge instance;return instance;}
	std::set<ChallangeType> Challenges;

	const bool IsChallangeTarget(const EntityId Id);
	//Call when there are some enemy near the player
	//Call in EnemyScan.cpp
	void GenerateChallenge(int enemyCount);
	void CheckHitForEliminteChallenge(const HitInfo &hit);

	void Update(float frameTime);
	void ResetChallengeStatus();

	const int GetRestrictType();
	void FailRestrictChallenge();
};

#endif