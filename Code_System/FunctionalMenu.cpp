#include "StdAfx.h"
#include "FunctionalMenu.h"
#include "GameActions.h"

void FunctionalMenu::OpenMenu()
{
	if(!pElement)
	{
		pElement = gEnv->pFlashUI->GetUIElement("Prototype");
		pElement->AddEventListener(&UIListener,"Functional_Listener");
	}
	if(pElement)
	{
		pElement->SetVisible(true);
		SUIArguments args;
		float pos_x = 0.5;
		float pos_y = 0.5;
		args.AddArgument(pos_x);
		args.AddArgument(pos_y);
		args.AddArgument(true);
		args.AddArgument(0);
		args.AddArgument(0);
		args.AddArgument(gEnv->pRenderer->GetWidth());
		args.AddArgument(gEnv->pRenderer->GetHeight());
		pElement->CallFunction("UpdateGroupPos",args);
		g_pGame->Actions().FilterNoMouse()->Enable(true);
		//g_pGame->GetIGameFramework()->PauseGame(true,false,1000);
		//g_pGame->Actions().FilterNoMove()->Enable(true);
	}
}

void FunctionalMenu::CloseMenu()
{
	if(!pElement)
	{
		pElement = gEnv->pFlashUI->GetUIElement("Prototype");
		pElement->AddEventListener(&UIListener,"Functional_Listener");
	}
	if(pElement)
	{
		pElement->SetVisible(false);
		g_pGame->Actions().FilterNoMouse()->Enable(false);
		//g_pGame->GetIGameFramework()->PauseGame(false,true,1000);
		//g_pGame->Actions().FilterNoMove()->Enable(false);
	}
}

const bool FunctionalMenu::IsMenuOpened()
{
	if(!pElement)
	{
		pElement = gEnv->pFlashUI->GetUIElement("Prototype");
		pElement->AddEventListener(&UIListener,"Functional_Listener");
	}
	if(pElement)
	{
		return pElement->IsVisible();
	}
	return false;
}

void CFunctionalMenuListener::OnUIEvent( IUIElement* pSender, const SUIEventDesc& event, const SUIArguments& args )
{
	SpawnSystem *pSpawnSystem = g_pGame->GetSpawnSystem();
	DeleteSystem *pDeleteSystem = g_pGame->GetDeleteSystem();
	ModifySystem *pModifySystem = g_pGame->GetModifySystem();
	if ( strcmp(event.sDisplayName, "FunctionIndex") == 0 )
	{
		int FunctionIndex;
		args.GetArg( 0, FunctionIndex);
		if(FunctionIndex == 1)
		{
			if(pDeleteSystem)
			{
				IEntity *pDeleteEntity = g_pGame->GetBasicRayCast()->GetEntity();
				pDeleteSystem->ProcessDeleteEntity(pDeleteEntity);
			}
		}
	}
}