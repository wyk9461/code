#include "StdAfx.h"
#include "WeaponModifier.h"
#include "WeaponSharedParams.h"
#include "FireMode.h"


SFireParams *MFireMode::GetBasicParams(IEntity *pEnt)
{
	CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEnt->GetId()));
	CWeapon *pWeapon=(CWeapon*)pActor->GetCurrentItem();

	if(pWeapon)
	{
		int fire_mode_index=pWeapon->GetCurrentFireMode();
		CFireMode* pFiremode = static_cast<CFireMode*>(pWeapon->GetFireMode(fire_mode_index));
		SFireModeParams* tFireModeParams=pFiremode->GetFireModeParams_Non_Const();
		SFireParams &pBasicfireParams=const_cast<SFireParams&>(tFireModeParams->fireparams);
		return &pBasicfireParams;
	}
	return 0;
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void FireRateModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SFireParams *pBasicfireParams=MFireMode::GetBasicParams(pEnt);
		if(pBasicfireParams)
		{
			prev_fvalue=pBasicfireParams->rate;

			if(!IsPercentage)
				fvalue=newValue;
			else
				fvalue=prev_fvalue * newValue;
		}
	}
}

void FireRateModifier::GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue)
{
	prev_fvalue = g_pGame->GetModifySystem()->GetModifierRecoveryManager()->GetCurWeaponLastTime(pEntity);
	fvalue = prev_fvalue - reduction;
}

void FireRateModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoWeaponList(pEnt,reduction);
		SFireParams *pBasicfireParams=MFireMode::GetBasicParams(pEnt);
		if(pBasicfireParams)
		{
			prevValue=static_cast<float>(pBasicfireParams->rate);
			if(!IsPercentage)
				pBasicfireParams->rate=static_cast<short>(newValue);
			else
				pBasicfireParams->rate=static_cast<short>(prevValue * newValue);

			CalculateCost();
		}
	}
}

void FireRateModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void AI_PlayerDamageModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SFireParams *pBasicfireParams=MFireMode::GetBasicParams(pEnt);
		if(pBasicfireParams)
		{
			prev_fvalue=static_cast<float>(pBasicfireParams->ai_vs_player_damage);

			if(!IsPercentage)
				fvalue=newValue;
			else
				fvalue=prev_fvalue * newValue;
		}
	}
}

void AI_PlayerDamageModifier::GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue)
{
	prev_fvalue = g_pGame->GetModifySystem()->GetModifierRecoveryManager()->GetCurWeaponLastTime(pEntity);
	fvalue = prev_fvalue - reduction;
}

void AI_PlayerDamageModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoWeaponList(pEnt,reduction);
		SFireParams *pBasicfireParams=MFireMode::GetBasicParams(pEnt);
		if(pBasicfireParams)
		{
			prevValue=static_cast<float>(pBasicfireParams->ai_vs_player_damage);
			if(!IsPercentage)
				pBasicfireParams->ai_vs_player_damage=static_cast<short>(newValue);
			else
				pBasicfireParams->ai_vs_player_damage=static_cast<short>(prevValue * newValue);

			CalculateCost();
		}
	}
}

void AI_PlayerDamageModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void Player_AIDamageModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SFireParams *pBasicfireParams=MFireMode::GetBasicParams(pEnt);
		if(pBasicfireParams)
		{
			prev_fvalue=static_cast<float>(pBasicfireParams->damage);

			if(!IsPercentage)
				fvalue=newValue;
			else
				fvalue=prev_fvalue * newValue;
		}
	}
}

void Player_AIDamageModifier::GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue)
{
	prev_fvalue = g_pGame->GetModifySystem()->GetModifierRecoveryManager()->GetCurWeaponLastTime(pEntity);
	fvalue = prev_fvalue - reduction;
}

void Player_AIDamageModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoWeaponList(pEnt,reduction);
		SFireParams *pBasicfireParams=MFireMode::GetBasicParams(pEnt);
		if(pBasicfireParams)
		{
			prevValue=static_cast<float>(pBasicfireParams->damage);
			if(!IsPercentage)
				pBasicfireParams->damage=static_cast<short>(newValue);
			else
				pBasicfireParams->damage=static_cast<short>(prevValue * newValue);

			CalculateCost();
		}
	}
}

void Player_AIDamageModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void ClipSizeModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SFireParams *pBasicfireParams=MFireMode::GetBasicParams(pEnt);
		if(pBasicfireParams)
		{
			prev_fvalue=pBasicfireParams->clip_size;

			if(!IsPercentage)
				fvalue=newValue;
			else
				fvalue=prev_fvalue * newValue;
		}
	}
}

void ClipSizeModifier::GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue)
{
	prev_fvalue = g_pGame->GetModifySystem()->GetModifierRecoveryManager()->GetCurWeaponLastTime(pEntity);
	fvalue = prev_fvalue - reduction;
}

void ClipSizeModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoWeaponList(pEnt,reduction);
		SFireParams *pBasicfireParams=MFireMode::GetBasicParams(pEnt);
		if(pBasicfireParams)
		{
			if(pBasicfireParams)
			{
				prevValue=static_cast<float>(pBasicfireParams->clip_size);
				if(!IsPercentage)
					pBasicfireParams->clip_size=static_cast<short>(newValue);
				else
					pBasicfireParams->clip_size=static_cast<short>(prevValue * newValue);
			}

			CalculateCost();
		}
	}
}

void ClipSizeModifier::CalculateCost()
{

}