#include "StdAfx.h"
#include "LightModifier.h"

void LightRadiusModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable light_table=MScriptBind::GetLightTable(root);

		light_table->GetValue("Radius",prevValue);

		if(!IsPercentage)
			light_table->SetValue("Radius", newValue);
		else
			light_table->SetValue("Radius",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CalculateCost();
	}
}

void LightRadiusModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void DiffuseModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable light_table=MScriptBind::GetLightTable(root);

		light_table->GetValue("fDiffuseMultiplier",prevValue);

		if(!IsPercentage)
			light_table->SetValue("fDiffuseMultiplier", newValue);
		else
			light_table->SetValue("fDiffuseMultiplier",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CalculateCost();
	}
}

void DiffuseModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void SpecularModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable light_table=MScriptBind::GetLightTable(root);

		light_table->GetValue("fSpecularMultiplier",prevValue);

		if(!IsPercentage)
			light_table->SetValue("fSpecularMultiplier", newValue);
		else
			light_table->SetValue("fSpecularMultiplier",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CalculateCost();
	}
}

void SpecularModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void HDRModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable light_table=MScriptBind::GetLightTable(root);

		light_table->GetValue("fHDRDynamic",prevValue);

		if(!IsPercentage)
			light_table->SetValue("fHDRDynamic", newValue);
		else
			light_table->SetValue("fHDRDynamic",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CalculateCost();
	}
}

void HDRModifier::CalculateCost()
{

}