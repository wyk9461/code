#include "StdAfx.h"
#include "ModifierRecovery.h"
#include "WeaponSharedParams.h"
#include "FireMode.h"
#include "IVehicleSystem.h"
#include "VehicleMovementArcadeWheeled.h"


ModifierRecoveryManager::ModifierRecoveryManager()
{
	gEnv->pGame->GetIGameFramework()->RegisterListener(this, "ModifyRecovery",FRAMEWORKLISTENERPRIORITY_GAME);
	MaxWeaponTime = 60;
	MaxVehicleTime = 60;
}


void ModifierRecoveryManager::OnPostUpdate(float fDelta)
{
	if(!WeaponList.empty())
	{
		WRecoveryMap::iterator it=WeaponList.begin();
		while(it != WeaponList.end())
		{
			WeaponRecovery &WR=it->second;
			WR.tick_timer+=fDelta;
			if(WR.tick_timer > WR.last_time)
			{
				Back_To_Default(WR);
				WeaponList.erase(it++);
			}
			else
				++it;
		}
	}
	if(!AdvModifierList.empty())
	{
		AdvList::iterator it=AdvModifierList.begin();
		AdvList::iterator end=AdvModifierList.end();
		while (it != end)
		{
			AdvancedModifierRecovery &advMR=*it;
			advMR.tick_timer+=fDelta;
			if(advMR.tick_timer > advMR.last_time)
			{
				Back_To_Default(advMR);
				AdvModifierList.erase(it++);
			}
			else
			{
				IEntity *pEnt=gEnv->pEntitySystem->GetEntity(advMR.Id);
				advMR.pModifier->ApplyChange(pEnt);
				++it;
			}
		}
	}
	if (!VehicleList.empty())
	{
		VRecoveryMap::iterator it=VehicleList.begin();
		while (it !=VehicleList.end())
		{
			VehicleRecovery &VR=it->second;
			VR.tick_timer+=fDelta;
			if(VR.tick_timer > VR.last_time)
			{
				Back_To_Default(VR);
				VehicleList.erase(it++);
			}
			else
				++it;
		}
	}
}

void ModifierRecoveryManager::AddtoWeaponList(IEntity* pEnt , const float reTime)
{
	const char* weaponClass=pEnt->GetClass()->GetName();
	if(!WeaponList.count(weaponClass))
	{
		CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEnt->GetId()));
		if(!pActor->IsDead())
		{
			CWeapon *pWeapon=(CWeapon*)pActor->GetCurrentItem();
			if(pWeapon)
			{
				int fire_mode_index=pWeapon->GetCurrentFireMode();
				CFireMode* pFiremode = static_cast<CFireMode*>(pWeapon->GetFireMode(fire_mode_index));

				WeaponRecovery WR;

				WR.pFiremode=pFiremode;
				WR.weaponClass=weaponClass;
				WR.tick_timer=0;
				WR.last_time=MaxWeaponTime-reTime;

				//WeaponList[weaponClass]=WR;
				WR.params=RetriveWeaponDefault(pEnt,pFiremode);
				WeaponList.insert(WRecoveryMap::value_type(weaponClass,WR));
			}
		}
	}
	else
		WeaponList[weaponClass].last_time-=reTime;
}

WeaponParams ModifierRecoveryManager::RetriveWeaponDefault(IEntity* pEnt , CFireMode *pFireMode)
{
	WeaponParams params;
	SFireModeParams *tFireModeParams=pFireMode->GetFireModeParams_Non_Const();

	const SFireParams pBasicfireParams=tFireModeParams->fireparams;

	params.rate=pBasicfireParams.rate;
	params.ai_vs_player_damage=pBasicfireParams.ai_vs_player_damage;
	params.player_vs_ai_damage=pBasicfireParams.damage;
	params.clip_size=pBasicfireParams.clip_size;

	return params;
}

void ModifierRecoveryManager::Back_To_Default(WeaponRecovery &wr)
{
	SFireModeParams *tFireModeParams=wr.pFiremode->GetFireModeParams_Non_Const();
	SFireParams &pBasicfireParams=const_cast<SFireParams&>(tFireModeParams->fireparams);
	pBasicfireParams.rate=wr.params.rate;
	pBasicfireParams.ai_vs_player_damage=wr.params.ai_vs_player_damage;
	pBasicfireParams.damage=wr.params.player_vs_ai_damage;
	pBasicfireParams.clip_size=wr.params.clip_size;
	wr.pFiremode=NULL;
}

void ModifierRecoveryManager::Back_To_Default(VehicleRecovery &vr)
{
	float &acceleration=CVehicleMovementArcadeWheeled::GetAcceleration(vr.vehicleClass);
	float &top_speed=CVehicleMovementArcadeWheeled::GetTopSpeed(vr.vehicleClass);
	float &reverseSpeed=CVehicleMovementArcadeWheeled::GetReverseSpeed(vr.vehicleClass);
	float &decceleration=CVehicleMovementArcadeWheeled::GetDecceleration(vr.vehicleClass);
	float &backFriction=CVehicleMovementArcadeWheeled::GetBackFriction(vr.vehicleClass);
	float &frontFriction=CVehicleMovementArcadeWheeled::GetFrontFriction(vr.vehicleClass);

	acceleration=vr.params.acceleration;
	top_speed=vr.params.top_speed;
	reverseSpeed=vr.params.reverseSpeed;
	decceleration=vr.params.decceleration;
	backFriction=vr.params.backFriction;
	frontFriction=vr.params.frontFriction;
}

void ModifierRecoveryManager::Back_To_Default(AdvancedModifierRecovery &advMR)
{
	advMR.pModifier->BacktoDefault(advMR.Id);
}



void ModifierRecoveryManager::AddtoVehicleList(const string& name , const float reTime)
{
	if(!VehicleList.count(name))
	{
		VehicleParams params;
		params.acceleration=CVehicleMovementArcadeWheeled::GetAcceleration(name);
		params.top_speed=CVehicleMovementArcadeWheeled::GetTopSpeed(name);
		params.reverseSpeed=CVehicleMovementArcadeWheeled::GetReverseSpeed(name);
		params.decceleration=CVehicleMovementArcadeWheeled::GetDecceleration(name);
		params.backFriction=CVehicleMovementArcadeWheeled::GetBackFriction(name);
		params.frontFriction=CVehicleMovementArcadeWheeled::GetFrontFriction(name);

		VehicleRecovery VR;
		VR.vehicleClass=name;
		VR.params=params;
		VR.last_time=MaxVehicleTime-reTime;
		VR.tick_timer=0;

		VehicleList.insert(VRecoveryMap::value_type(name,VR));
	}
	else
		VehicleList[name].last_time-=reTime;
}


void ModifierRecoveryManager::AddtoAdvancedModifierList(IEntity *pEnt , float last_time,CAdvancedModifier *pModifier)
{
	AdvancedModifierRecovery advMR;
	advMR.Id=pEnt->GetId();
	advMR.last_time=last_time;
	advMR.pModifier=pModifier;
	AdvModifierList.push_back(advMR);
}

void ModifierRecoveryManager::RecoveryAllWeapon()
{
	if(!WeaponList.empty())
	{
		WRecoveryMap::iterator it=WeaponList.begin();
		while(it != WeaponList.end())
		{
			WeaponRecovery &WR=it->second;
			Back_To_Default(WR);
			WeaponList.erase(it++);
		}
	}
}

void ModifierRecoveryManager::RecoveryAll()
{
	if(!WeaponList.empty())
	{
		WRecoveryMap::iterator it=WeaponList.begin();
		while(it != WeaponList.end())
		{
			WeaponRecovery &WR=it->second;
			Back_To_Default(WR);
			WeaponList.erase(it++);
		}
	}
	if(!AdvModifierList.empty())
	{
		AdvList::iterator it=AdvModifierList.begin();
		while (it != AdvModifierList.end())
		{
			AdvancedModifierRecovery &advMR=*it;
			Back_To_Default(advMR);
			it=AdvModifierList.erase(it);
		}
	}
	if (!VehicleList.empty())
	{
		VRecoveryMap::iterator it=VehicleList.begin();
		while (it !=VehicleList.end())
		{
			VehicleRecovery &VR=it->second;
			Back_To_Default(VR);
			VehicleList.erase(it++);
		}
	}
}

const float ModifierRecoveryManager::GetCurVehicleLastTime(IEntity *pEntity)
{
	if(pEntity)
	{
		IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
		IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEntity->GetId());
		CryFixedStringT<256>	sharedParamsName;
		sharedParamsName.Format("%s::%s::CVehicleMovementArcadeWheeled", pVehicle->GetEntity()->GetClass()->GetName(), pVehicle->GetModification());
		string name=sharedParamsName;

		if(VehicleList.count(name))
		{
			return VehicleList[name].last_time - VehicleList[name].tick_timer;
		}
		else
			return MaxVehicleTime;
	}
	return MaxVehicleTime;
}

const float ModifierRecoveryManager::GetCurWeaponLastTime(IEntity *pEntity)
{
	if(pEntity)
	{
		const string &WeaponClass=pEntity->GetClass()->GetName();
		if(WeaponList.count(WeaponClass))
		{
			return WeaponList[WeaponClass].last_time - WeaponList[WeaponClass].tick_timer;
		}
		else
			return MaxWeaponTime;
	}
	return MaxWeaponTime;
}