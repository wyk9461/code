#ifndef _ADVANCEDMODIFIER_H_
#define _ADVANCEDMODIFIER_H_

#include "IModifier.h"

class LegDisableModifier : public CAdvancedModifier
{
protected:
	void CalculateCost();
public:
	LegDisableModifier(float last_time,const int cost,const ModifyID id)
		:	CAdvancedModifier(last_time,cost,id,eMODT_Disable,"LegDisable"){}

	~LegDisableModifier(){}

	void ApplyChange(IEntity *pEnt);
	void Apply(IEntity* pEnt);
	void BacktoDefault(EntityId Id);
};

//------------------------------------------------------------------------
class ArmDisableModifier : public CAdvancedModifier
{
protected:
	void CalculateCost();
public:
	ArmDisableModifier(float last_time,const int cost,const ModifyID id)
		:	CAdvancedModifier(last_time,cost,id,eMODT_Disable,"ArmDisable"){}

	~ArmDisableModifier(){}

	void ApplyChange(IEntity *pEnt);
	void Apply(IEntity* pEnt);
	void BacktoDefault(EntityId Id);
private:
	static std::map<EntityId,EntityId> RecoveryMap;
};

//------------------------------------------------------------------------
class EyeDisableModifier : public CAdvancedModifier
{
protected:
	void CalculateCost();
public:
	EyeDisableModifier(float last_time,const int cost,const ModifyID id)
		:	CAdvancedModifier(last_time,cost,id,eMODT_Disable,"EyeDisable"){}

	~EyeDisableModifier(){}

	void ApplyChange(IEntity *pEnt);
	void Apply(IEntity* pEnt);
	void BacktoDefault(EntityId Id);
private:
	static std::map<EntityId,int> RecoveryMap;
};

//------------------------------------------------------------------------
class MindDisableModifier : public CAdvancedModifier
{
protected:
	void CalculateCost();
public:
	MindDisableModifier(float last_time,const int cost,const ModifyID id)
		:	CAdvancedModifier(last_time,cost,id,eMODT_Disable,"MindDisable"){}

	~MindDisableModifier(){}

	void ApplyChange(IEntity *pEnt);
	void Apply(IEntity* pEnt);
	void BacktoDefault(EntityId Id);
private:
	static std::map<EntityId,int> RecoveryMap;
};

//------------------------------------------------------------------------
class MindChangeModifier : public CAdvancedModifier
{
protected:
	void CalculateCost();
public:
	MindChangeModifier(float last_time,const int cost,const ModifyID id)
		:	CAdvancedModifier(last_time,cost,id,eMODT_Control,"MindChange"){}

	~MindChangeModifier(){}

	void ApplyChange(IEntity *pEnt);
	void Apply(IEntity* pEnt);
	void BacktoDefault(EntityId Id);
private:
	static std::map<EntityId,int> RecoveryMap;
};

//------------------------------------------------------------------------
class ArmWeakModifier : public CAdvancedModifier
{
protected:
	void CalculateCost();
public:
	ArmWeakModifier(float last_time,const int cost,const ModifyID id)
		:	CAdvancedModifier(last_time,cost,id,eMODT_Weak,"ArmWeak"){}

	~ArmWeakModifier(){}

	void ApplyChange(IEntity *pEnt);
	void Apply(IEntity* pEnt);
	void BacktoDefault(EntityId Id);
private:
	static std::map<EntityId,float> RecoveryMap;
};

//------------------------------------------------------------------------
class LegWeakModifier : public CAdvancedModifier
{
protected:
	void CalculateCost();
public:
	LegWeakModifier(float last_time,const int cost,const ModifyID id)
		:	CAdvancedModifier(last_time,cost,id,eMODT_Weak,"LegWeak"){}

	~LegWeakModifier(){}

	void ApplyChange(IEntity *pEnt);
	void Apply(IEntity* pEnt);
	void BacktoDefault(EntityId Id);
private:
	static std::map<EntityId,float> RecoveryMap;
};

#endif
