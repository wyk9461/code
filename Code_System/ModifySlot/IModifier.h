#ifndef _IMODIFYSLOT_H_
#define  _IMODIFYSLOT_H_

#include "StdAfx.h"

typedef unsigned int ModifyID;


namespace MScriptBind
{
	SmartScriptTable GetProps(SmartScriptTable root);
	SmartScriptTable GetPropsInstance(SmartScriptTable root);
	SmartScriptTable GetPhysTable(SmartScriptTable root);
	SmartScriptTable GetExploTable(SmartScriptTable root);
	SmartScriptTable GetHealthTable(SmartScriptTable root);
	SmartScriptTable GetLightTable(SmartScriptTable root);
	SmartScriptTable GetFunctionTable(SmartScriptTable root);
};

enum Modifiers_Type
{
	eMODT_ALL = 0,
	eMODT_Physics = 1,
	eMODT_Damage,
	eMODT_Health,
	eMODT_Light,
	eMODT_Kinetic,
	eMODT_Control,
	eMODT_Disable,
	eMODT_Weak,
};


class IModifier
{
protected:
	virtual void CalculateCost() = 0;
public:
	virtual void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue) = 0;
	virtual void Apply(IEntity* pEnt) = 0;
	virtual const ModifyID GetModifierId() = 0;
	virtual const int GetCost() const = 0;
	virtual const bool Percentage() const = 0;
	virtual const float GetfValue() const = 0;
	virtual const int GetiValue() const = 0;
	virtual const char* GetDesc() = 0;
	virtual const int GetType() = 0;
	virtual void GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue) = 0;

};

template<typename T>
class CModifier : public IModifier
{
protected:
	T prevValue;
	bool IsPercentage;
	T newValue;
	ModifyID Id;
	Modifiers_Type Type;
	unsigned int Cost;
	virtual void CalculateCost(){}
	string Desc;

public:
	CModifier(const T val,const int cost, ModifyID id, Modifiers_Type _type, char* _desc, const bool ispercentage)
	:	prevValue(0)
	,	Cost(cost)
	,	Id(id)
	,	Type(_type)
	,	newValue(val)
	,	IsPercentage(ispercentage)
	,	Desc(_desc){}

	virtual void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue){}
	virtual void Apply(IEntity* pEnt){}

	void SetDesc(char* _desc){Desc = _desc;}
	virtual const ModifyID GetModifierId(){return Id;}
	virtual const int GetCost() const {return Cost;}
	virtual const bool Percentage() const {return IsPercentage;}
	virtual const float GetfValue() const {return static_cast<float>(newValue);}
	virtual const int GetiValue() const {return static_cast<int>(newValue);}
	virtual const char* GetDesc(){return Desc.c_str();}
	virtual const int GetType(){return Type;}
	virtual void GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue){}
};


class CTransformModifier : public IModifier
{
protected:
	IEntityClass *prevClass;
	IEntityClass *TransClass;
	ModifyID Id;
	unsigned int Cost;
	virtual void CalculateCost(){}
	virtual void TransForm(IEntity *prevEntity , IEntity *transEntity) {}
public:
	CTransformModifier(const char *className , const int cost , const ModifyID id)
	:	Cost(cost)
	,	Id(id)
	{
		TransClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass(className);
	}
	~CTransformModifier();

	virtual void Apply(IEntity* pEnt){}
	const ModifyID GetModifierId(){return Id;}

};

class CAdvancedModifier : public IModifier
{
protected:
	ModifyID Id;
	float LastTime;
	unsigned int Cost;
	Modifiers_Type Type;
	char* Desc;
	virtual void CalculateCost(){}
public:
	CAdvancedModifier(float last_time,const int cost,const ModifyID id, Modifiers_Type _type, char* _desc)
		:	LastTime(last_time)
		,	Cost(cost)
		,	Id(id)
		,	Type(_type)
		,	Desc(_desc){}

	virtual void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue){prev_fvalue = LastTime;fvalue = LastTime;}
	virtual void Apply(IEntity* pEnt){}
	virtual void ApplyChange(IEntity *pEnt){}
	virtual void BacktoDefault(EntityId Id){};

	virtual const ModifyID GetModifierId(){return Id;}
	virtual const int GetCost() const {return Cost;}
	virtual const bool Percentage() const {return false;}
	virtual const float GetfValue() const {return -1;}
	virtual const int GetiValue() const {return -1;}
	virtual const char* GetDesc(){return Desc;}
	virtual const int GetType(){return Type;}
	virtual void GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue){prev_fvalue = 0;fvalue = LastTime;};
};

class CTimingModifier
{
protected:
	float reduction;
public:
	CTimingModifier(float nwe_reduction)
		:	reduction(nwe_reduction){}
	void SetTime_Reduce(float nwe_reduction){reduction = nwe_reduction;}
};

#endif