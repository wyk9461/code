#ifndef _WEAPONMODIFIER_H_
#define _WEAPONMODIFIER_H_

#include "StdAfx.h"
#include "IModifier.h"
#include "Weapon.h"

/*namespace WeaponClass
{
	const IEntityClass *RifleClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("Rifle");
	const IEntityClass *ShotgunClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("Shotgun");
	const IEntityClass *PistolClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("Pistol");
};*/

namespace MFireMode
{
	SFireParams *GetBasicParams(IEntity *pEnt);
};

class FireRateModifier : public CModifier<float> , public CTimingModifier
{
protected:
	void CalculateCost();
public:
	FireRateModifier(const float val , const int cost , float time_reduction , ModifyID id , const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Control,"FireRate",ispercentage)
		,	CTimingModifier(time_reduction){}

	~FireRateModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};


class AI_PlayerDamageModifier : public CModifier<float> , public CTimingModifier
{
protected:
	void CalculateCost();
public:
	AI_PlayerDamageModifier(const float val , const int cost , float time_reduction , ModifyID id , const bool ispercentage)
	:	CModifier(val,cost,id,eMODT_Damage,"AI->Player_Damage",ispercentage)
	,	CTimingModifier(time_reduction){}

	~AI_PlayerDamageModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};


class Player_AIDamageModifier : public CModifier<float> , public CTimingModifier
{
protected:
	void CalculateCost();
public:
	Player_AIDamageModifier(const float val , const int cost , float time_reduction , ModifyID id , const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Damage,"Player->AI_Damage",ispercentage)
		,	CTimingModifier(time_reduction){}

	~Player_AIDamageModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

class ClipSizeModifier : public CModifier<float> , public CTimingModifier
{
protected:
	void CalculateCost();
public:
	ClipSizeModifier(const float val , const int cost , float time_reduction , ModifyID id , const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Control,"ClipSize",ispercentage)
		,	CTimingModifier(time_reduction){}

	~ClipSizeModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};



#endif