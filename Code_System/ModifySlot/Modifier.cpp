#include "StdAfx.h"
#include "IModifier.h"


SmartScriptTable MScriptBind::GetProps(SmartScriptTable root)
{
	if(root)
	{
		SmartScriptTable props;
		root->GetValue("Properties",props);
		return props;
	}
	return 0;
}

SmartScriptTable MScriptBind::GetPropsInstance(SmartScriptTable root)
{
	if(root)
	{
		SmartScriptTable props_instance;
		root->GetValue("PropertiesInstance",props_instance);
		return props_instance;
	}
	return 0;
}


SmartScriptTable MScriptBind::GetPhysTable(SmartScriptTable root)
{
	if(root)
	{
		SmartScriptTable props=GetProps(root);
		if(!props)
			return 0;
		SmartScriptTable phys_table;
		props->GetValue("Physics",phys_table);

		return phys_table;
	}
	return 0;
}

SmartScriptTable MScriptBind::GetExploTable(SmartScriptTable root)
{
	if(root)
	{
		SmartScriptTable props=GetProps(root);
		if(!props)
			return 0;
		SmartScriptTable explo_table;
		props->GetValue("Explosion",explo_table);

		return explo_table;
	}
	return 0;
}


SmartScriptTable MScriptBind::GetHealthTable(SmartScriptTable root)
{
	if(root)
	{
		SmartScriptTable props=GetProps(root);
		if(!props)
			return 0;
		SmartScriptTable health_table;
		props->GetValue("Health",health_table);

		return health_table;
	}
	return 0;
}

SmartScriptTable MScriptBind::GetLightTable(SmartScriptTable root)
{
	if(root)
	{
		SmartScriptTable props_instance=GetPropsInstance(root);
		if(!props_instance)
			return 0;
		SmartScriptTable light_table;
		props_instance->GetValue("LightProperties_Base",light_table);

		return light_table;
	}
	return 0;
}

SmartScriptTable MScriptBind::GetFunctionTable(SmartScriptTable root)
{
	if(root)
	{
		SmartScriptTable props=GetProps(root);
		if(!props)
			return 0;
		SmartScriptTable func_table;
		props->GetValue("FunctionParams",func_table);

		return func_table;
	}
	return 0;
}
