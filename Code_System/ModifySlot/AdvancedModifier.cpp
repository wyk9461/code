#include "StdAfx.h"
#include "AdvancedModifier.h"
#include "Code_System/ScriptModify_AI.h"

std::map<EntityId,EntityId> ArmDisableModifier::RecoveryMap;
std::map<EntityId,int> EyeDisableModifier::RecoveryMap;
std::map<EntityId,int> MindDisableModifier::RecoveryMap;
std::map<EntityId,int> MindChangeModifier::RecoveryMap;
std::map<EntityId,float> ArmWeakModifier::RecoveryMap;
std::map<EntityId,float> LegWeakModifier::RecoveryMap;

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void LegDisableModifier::Apply(IEntity* pEnt)
{
	CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEnt->GetId()));
	if(pActor)
	{
		g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoAdvancedModifierList(pEnt,LastTime,this);
		CalculateCost();
	}
}

void LegDisableModifier::CalculateCost()
{

}

void LegDisableModifier::BacktoDefault(EntityId Id)
{

}

void LegDisableModifier::ApplyChange(IEntity *pEnt)
{
	if(pEnt)
	{
		CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEnt->GetId()));
		if(pActor && !pActor->IsFallen())
			pActor->Fall();
	}
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void ArmDisableModifier::Apply(IEntity* pEnt)
{
	CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEnt->GetId()));
	if(pActor && !pActor->IsDead())
	{
		IItem *pItem=pActor->GetCurrentItem(false);
		if(pItem)
		{
			pActor->DropItem(pItem->GetEntityId());
			if(!RecoveryMap.count(pEnt->GetId()))
			{
				RecoveryMap[pEnt->GetId()]=pItem->GetEntityId();
				g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoAdvancedModifierList(pEnt,LastTime,this);
				CalculateCost();
			}
		}
	}
}

void ArmDisableModifier::CalculateCost()
{

}

void ArmDisableModifier::BacktoDefault(EntityId Id)
{
	if(RecoveryMap.count(Id))
	{
		IItem *pItem=g_pGame->GetIGameFramework()->GetIItemSystem()->GetItem(RecoveryMap[Id]);
		if(pItem)
		{
			CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(Id));
			if(pActor && !pActor->IsDead())
			{
				pActor->PickUpItem(pItem->GetEntityId(),false,true);
				RecoveryMap.erase(Id);
			}
		}
	}
}

void ArmDisableModifier::ApplyChange(IEntity *pEnt)
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void EyeDisableModifier::Apply(IEntity* pEnt)
{
	CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEnt->GetId()));
	if(pActor && !pActor->IsDead())
	{
		int defSightRange=ScriptModify_AI::ChangeSightRange(pActor,0);
		if(!RecoveryMap.count(pEnt->GetId()))
		{
			RecoveryMap[pEnt->GetId()]=defSightRange;
			g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoAdvancedModifierList(pEnt,LastTime,this);
			CalculateCost();
		}
	}
}

void EyeDisableModifier::CalculateCost()
{

}

void EyeDisableModifier::BacktoDefault(EntityId Id)
{
	if(RecoveryMap.count(Id))
	{
		CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(Id));
		if(pActor && !pActor->IsDead())
		{
			ScriptModify_AI::ChangeSightRange(pActor,RecoveryMap[Id]);
			RecoveryMap.erase(Id);
		}
	}
}

void EyeDisableModifier::ApplyChange(IEntity *pEnt)
{

}


//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void MindDisableModifier::Apply(IEntity* pEnt)
{
	if(!pEnt)
		return;
	CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEnt->GetId()));
	if(pActor && !pActor->IsDead())
	{
		if(!RecoveryMap.count(pEnt->GetId()))
		{
			int defFaction=ScriptModify_AI::ChangeFaction(pActor,"Civilians");
			RecoveryMap[pEnt->GetId()]=defFaction;
			g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoAdvancedModifierList(pEnt,LastTime,this);
			CalculateCost();
		}
	}
}

void MindDisableModifier::CalculateCost()
{

}

void MindDisableModifier::BacktoDefault(EntityId Id)
{
	if(RecoveryMap.count(Id))
	{
		CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(Id));
		if(pActor && !pActor->IsDead())
		{
			const int faction=RecoveryMap[Id];
			ScriptModify_AI::ChangeFaction(pActor,faction);
			RecoveryMap.erase(Id);
		}
	}
}

void MindDisableModifier::ApplyChange(IEntity *pEnt)
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void MindChangeModifier::Apply(IEntity* pEnt)
{
	if(!pEnt)
		return;
	CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEnt->GetId()));
	if(pActor && !pActor->IsDead())
	{
		if(!RecoveryMap.count(pEnt->GetId()))
		{
			int defFaction=ScriptModify_AI::ChangeFaction(pActor,"Players");
			RecoveryMap[pEnt->GetId()]=defFaction;
			g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoAdvancedModifierList(pEnt,LastTime,this);
			CalculateCost();
		}
	}
}

void MindChangeModifier::CalculateCost()
{

}

void MindChangeModifier::BacktoDefault(EntityId Id)
{
	if(RecoveryMap.count(Id))
	{
		CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(Id));
		if(pActor && !pActor->IsDead())
		{
			const int faction=RecoveryMap[Id];
			ScriptModify_AI::ChangeFaction(pActor,faction);
			RecoveryMap.erase(Id);
		}
	}
}

void MindChangeModifier::ApplyChange(IEntity *pEnt)
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void ArmWeakModifier::Apply(IEntity* pEnt)
{
	CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEnt->GetId()));
	if(pActor && !pActor->IsDead())
	{
		if(!RecoveryMap.count(pEnt->GetId()))
		{
			float defAccuracy=ScriptModify_AI::ChangeAccuracy(pActor,0.2);
			RecoveryMap[pEnt->GetId()]=defAccuracy;
			g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoAdvancedModifierList(pEnt,LastTime,this);
			CalculateCost();
		}
	}
}

void ArmWeakModifier::CalculateCost()
{

}

void ArmWeakModifier::BacktoDefault(EntityId Id)
{
	if(RecoveryMap.count(Id))
	{
		CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(Id));
		if(pActor && !pActor->IsDead())
		{
			ScriptModify_AI::ChangeAccuracy(pActor,RecoveryMap[Id]);
			RecoveryMap.erase(Id);
		}

	}
}

void ArmWeakModifier::ApplyChange(IEntity *pEnt)
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void LegWeakModifier::Apply(IEntity* pEnt)
{
	CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEnt->GetId()));
	if(pActor && !pActor->IsDead())
	{
		if(!RecoveryMap.count(pEnt->GetId()))
		{
			float defSpeed=ScriptModify_AI::ChangeSpeed(pActor,0.25);
			RecoveryMap[pEnt->GetId()] = defSpeed;
			g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoAdvancedModifierList(pEnt,LastTime,this);
			CalculateCost();
		}
	}
}

void LegWeakModifier::CalculateCost()
{

}

void LegWeakModifier::BacktoDefault(EntityId Id)
{
	if(RecoveryMap.count(Id))
	{
		CActor *pActor=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(Id));
		if(pActor && !pActor->IsDead())
		{
			ScriptModify_AI::ChangeSpeed(pActor,RecoveryMap[Id]);
			RecoveryMap.erase(Id);
		}
	}
}

void LegWeakModifier::ApplyChange(IEntity *pEnt)
{

}