#ifndef _LIGHTMODIFIER_H_
#define _LIGHTMODIFIER_H_

#include "IModifier.h"

class LightRadiusModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	LightRadiusModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Physics,"LightRadius",ispercentage){}

	~LightRadiusModifier(){}

	void Apply(IEntity* pEnt);
};


class DiffuseModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	DiffuseModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Physics,"Diffuse",ispercentage){}

	~DiffuseModifier(){}

	void Apply(IEntity* pEnt);
};


class SpecularModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	SpecularModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Physics,"Specular",ispercentage){}

	~SpecularModifier(){}

	void Apply(IEntity* pEnt);
};


class HDRModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	HDRModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Physics,"HDR",ispercentage){}

	~HDRModifier(){}

	void Apply(IEntity* pEnt);
};


#endif