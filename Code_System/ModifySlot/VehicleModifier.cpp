#include "StdAfx.h"
#include "VehicleModifier.h"
#include "IVehicleSystem.h"
#include "VehicleMovementArcadeWheeled.h"

void AccelerationModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
		IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEnt->GetId());
		if(pVehicle)
		{
			CryFixedStringT<256>	sharedParamsName;
			sharedParamsName.Format("%s::%s::CVehicleMovementArcadeWheeled", pVehicle->GetEntity()->GetClass()->GetName(), pVehicle->GetModification());
			string name=sharedParamsName;
			float &acceleration=CVehicleMovementArcadeWheeled::GetAcceleration(name);

			prev_fvalue=acceleration;

			if(!IsPercentage)
				fvalue=newValue;
			else
				fvalue=prev_fvalue * newValue;
		}
	}
}

void AccelerationModifier::GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue)
{
	prev_fvalue = g_pGame->GetModifySystem()->GetModifierRecoveryManager()->GetCurVehicleLastTime(pEntity);
	fvalue = prev_fvalue - reduction;
}

void AccelerationModifier::Apply(IEntity* pEnt)
{
	IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
	IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEnt->GetId());
	if(pVehicle)
	{
		CryFixedStringT<256>	sharedParamsName;
		sharedParamsName.Format("%s::%s::CVehicleMovementArcadeWheeled", pVehicle->GetEntity()->GetClass()->GetName(), pVehicle->GetModification());
		string name=sharedParamsName;
		g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoVehicleList(name,reduction);
		float &acceleration=CVehicleMovementArcadeWheeled::GetAcceleration(name);
		prevValue=acceleration;
		if(!IsPercentage)
			acceleration=newValue;
		else
			acceleration=prevValue * newValue;
	}
}

void AccelerationModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void TopSpeedModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
		IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEnt->GetId());
		if(pVehicle)
		{
			CryFixedStringT<256>	sharedParamsName;
			sharedParamsName.Format("%s::%s::CVehicleMovementArcadeWheeled", pVehicle->GetEntity()->GetClass()->GetName(), pVehicle->GetModification());
			string name=sharedParamsName;
			float &acceleration=CVehicleMovementArcadeWheeled::GetTopSpeed(name);

			prev_fvalue=acceleration;

			if(!IsPercentage)
				fvalue=newValue;
			else
				fvalue=prev_fvalue * newValue;
		}
	}
}

void TopSpeedModifier::GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue)
{
	prev_fvalue = g_pGame->GetModifySystem()->GetModifierRecoveryManager()->GetCurVehicleLastTime(pEntity);
	fvalue = prev_fvalue - reduction;
}

void TopSpeedModifier::Apply(IEntity* pEnt)
{
	IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
	IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEnt->GetId());
	if(pVehicle)
	{
		CryFixedStringT<256>	sharedParamsName;
		sharedParamsName.Format("%s::%s::CVehicleMovementArcadeWheeled", pVehicle->GetEntity()->GetClass()->GetName(), pVehicle->GetModification());
		string name=sharedParamsName;
		g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoVehicleList(name,reduction);
		float &top_speed=CVehicleMovementArcadeWheeled::GetTopSpeed(name);
		prevValue=top_speed;
		if(!IsPercentage)
			top_speed=newValue;
		else
			top_speed=prevValue * newValue;
	}
}

void TopSpeedModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void ReverseSpeedModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
		IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEnt->GetId());
		if(pVehicle)
		{
			CryFixedStringT<256>	sharedParamsName;
			sharedParamsName.Format("%s::%s::CVehicleMovementArcadeWheeled", pVehicle->GetEntity()->GetClass()->GetName(), pVehicle->GetModification());
			string name=sharedParamsName;
			float &acceleration=CVehicleMovementArcadeWheeled::GetReverseSpeed(name);

			prev_fvalue=acceleration;

			if(!IsPercentage)
				fvalue=newValue;
			else
				fvalue=prev_fvalue * newValue;
		}
	}
}

void ReverseSpeedModifier::GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue)
{
	prev_fvalue = g_pGame->GetModifySystem()->GetModifierRecoveryManager()->GetCurVehicleLastTime(pEntity);
	fvalue = prev_fvalue - reduction;
}

void ReverseSpeedModifier::Apply(IEntity* pEnt)
{
	IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
	IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEnt->GetId());
	if(pVehicle)
	{
		CryFixedStringT<256>	sharedParamsName;
		sharedParamsName.Format("%s::%s::CVehicleMovementArcadeWheeled", pVehicle->GetEntity()->GetClass()->GetName(), pVehicle->GetModification());
		string name=sharedParamsName;
		g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoVehicleList(name,reduction);
		float &reverse_speed=CVehicleMovementArcadeWheeled::GetReverseSpeed(name);
		prevValue=reverse_speed;
		if(!IsPercentage)
			reverse_speed=newValue;
		else
			reverse_speed=prevValue * newValue;
	}
}

void ReverseSpeedModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------


void DeccelerationModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
		IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEnt->GetId());
		if(pVehicle)
		{
			CryFixedStringT<256>	sharedParamsName;
			sharedParamsName.Format("%s::%s::CVehicleMovementArcadeWheeled", pVehicle->GetEntity()->GetClass()->GetName(), pVehicle->GetModification());
			string name=sharedParamsName;
			float &acceleration=CVehicleMovementArcadeWheeled::GetDecceleration(name);

			prev_fvalue=acceleration;

			if(!IsPercentage)
				fvalue=newValue;
			else
				fvalue=prev_fvalue * newValue;
		}
	}
}

void DeccelerationModifier::GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue)
{
	prev_fvalue = g_pGame->GetModifySystem()->GetModifierRecoveryManager()->GetCurVehicleLastTime(pEntity);
	fvalue = prev_fvalue - reduction;
}

void DeccelerationModifier::Apply(IEntity* pEnt)
{
	IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
	IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEnt->GetId());
	if(pVehicle)
	{
		CryFixedStringT<256>	sharedParamsName;
		sharedParamsName.Format("%s::%s::CVehicleMovementArcadeWheeled", pVehicle->GetEntity()->GetClass()->GetName(), pVehicle->GetModification());
		string name=sharedParamsName;
		g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoVehicleList(name,reduction);
		float &decceleration=CVehicleMovementArcadeWheeled::GetDecceleration(name);
		prevValue=decceleration;
		if(!IsPercentage)
			decceleration=newValue;
		else
			decceleration=prevValue * newValue;
	}
}

void DeccelerationModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void BackFrictionModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
		IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEnt->GetId());
		if(pVehicle)
		{
			CryFixedStringT<256>	sharedParamsName;
			sharedParamsName.Format("%s::%s::CVehicleMovementArcadeWheeled", pVehicle->GetEntity()->GetClass()->GetName(), pVehicle->GetModification());
			string name=sharedParamsName;
			float &acceleration=CVehicleMovementArcadeWheeled::GetBackFriction(name);

			prev_fvalue=acceleration;

			if(!IsPercentage)
				fvalue=newValue;
			else
				fvalue=prev_fvalue * newValue;
		}
	}
}

void BackFrictionModifier::GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue)
{
	prev_fvalue = g_pGame->GetModifySystem()->GetModifierRecoveryManager()->GetCurVehicleLastTime(pEntity);
	fvalue = prev_fvalue - reduction;
}

void BackFrictionModifier::Apply(IEntity* pEnt)
{
	IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
	IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEnt->GetId());
	if(pVehicle)
	{
		CryFixedStringT<256>	sharedParamsName;
		sharedParamsName.Format("%s::%s::CVehicleMovementArcadeWheeled", pVehicle->GetEntity()->GetClass()->GetName(), pVehicle->GetModification());
		string name=sharedParamsName;
		g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoVehicleList(name,reduction);
		float &back_friction=CVehicleMovementArcadeWheeled::GetBackFriction(name);
		prevValue=back_friction;
		if(!IsPercentage)
			back_friction=newValue;
		else
			back_friction=prevValue * newValue;
	}
}

void BackFrictionModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void FrontFrictionModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
		IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEnt->GetId());
		if(pVehicle)
		{
			CryFixedStringT<256>	sharedParamsName;
			sharedParamsName.Format("%s::%s::CVehicleMovementArcadeWheeled", pVehicle->GetEntity()->GetClass()->GetName(), pVehicle->GetModification());
			string name=sharedParamsName;
			float &acceleration=CVehicleMovementArcadeWheeled::GetFrontFriction(name);

			prev_fvalue=acceleration;

			if(!IsPercentage)
				fvalue=newValue;
			else
				fvalue=prev_fvalue * newValue;
		}
	}
}

void FrontFrictionModifier::GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue)
{
	prev_fvalue = g_pGame->GetModifySystem()->GetModifierRecoveryManager()->GetCurVehicleLastTime(pEntity);
	fvalue = prev_fvalue - reduction;
}

void FrontFrictionModifier::Apply(IEntity* pEnt)
{
	IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
	IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEnt->GetId());
	if(pVehicle)
	{
		CryFixedStringT<256>	sharedParamsName;
		sharedParamsName.Format("%s::%s::CVehicleMovementArcadeWheeled", pVehicle->GetEntity()->GetClass()->GetName(), pVehicle->GetModification());
		string name=sharedParamsName;
		g_pGame->GetModifySystem()->GetModifierRecoveryManager()->AddtoVehicleList(name,reduction);
		float &front_friction=CVehicleMovementArcadeWheeled::GetFrontFriction(name);
		prevValue=front_friction;
		if(!IsPercentage)
			front_friction=newValue;
		else
			front_friction=prevValue * newValue;
	}
}

void FrontFrictionModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------


void SpeedModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
	IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEnt->GetId());
	if(pVehicle)
	{
		IPhysicalEntity *pPEnt=pEnt->GetPhysics();
		pe_status_dynamics params;
		pPEnt->GetStatus(&params);
		prev_fvalue = params.v.GetLengthFast();
		fvalue = newValue;
	}
}

void SpeedModifier::GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue)
{
	prev_fvalue = g_pGame->GetModifySystem()->GetModifierRecoveryManager()->GetCurVehicleLastTime(pEntity);
	fvalue = prev_fvalue - reduction;
}

void SpeedModifier::Apply(IEntity* pEnt)
{
	IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
	IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEnt->GetId());
	if(pVehicle)
	{
		IPhysicalEntity *pPEnt=pEnt->GetPhysics();
		pe_action_set_velocity ve;
		ve.v=pEnt->GetForwardDir()*newValue;
		pPEnt->Action(&ve);
	}
}

void SpeedModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void RotationModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
	IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEnt->GetId());
	if(pVehicle)
	{
		//Need Calculate?
		prev_fvalue = 0;
		fvalue = newValue;
	}
}

void RotationModifier::GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue)
{
	prev_fvalue = g_pGame->GetModifySystem()->GetModifierRecoveryManager()->GetCurVehicleLastTime(pEntity);
	fvalue = prev_fvalue - reduction;
}


void RotationModifier::Apply(IEntity* pEnt)
{
	IVehicleSystem *pVehicleSystem=g_pGame->GetIGameFramework()->GetIVehicleSystem();
	IVehicle *pVehicle = pVehicleSystem->GetVehicle(pEnt->GetId());
	if(pVehicle)
	{
		IPhysicalEntity *pPEnt=pEnt->GetPhysics();
		pe_action_set_velocity ve;
		ve.w.x=0;
		ve.w.y=0;
		ve.w.z=DEG2RAD(newValue);
		pPEnt->Action(&ve);
	}
}

void RotationModifier::CalculateCost()
{

}