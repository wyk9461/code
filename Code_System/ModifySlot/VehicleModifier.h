#ifndef _VEHICLEMODIFIER_H_
#define _VEHICLEMODIFIER_H_

#include "IModifier.h"

class AccelerationModifier : public CModifier<float> , public CTimingModifier
{
protected:
	void CalculateCost();
public:
	AccelerationModifier(const float val , const int cost , float time_reduction , ModifyID id , const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Kinetic,"Acceleration",ispercentage)
		,	CTimingModifier(time_reduction){}

	~AccelerationModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	virtual void GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class TopSpeedModifier : public CModifier<float> , public CTimingModifier
{
protected:
	void CalculateCost();
public:
	TopSpeedModifier(const float val , const int cost , float time_reduction , ModifyID id , const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Kinetic,"TopSpeed",ispercentage)
		,	CTimingModifier(time_reduction){}

	~TopSpeedModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	virtual void GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class ReverseSpeedModifier : public CModifier<float> , public CTimingModifier
{
protected:
	void CalculateCost();
public:
	ReverseSpeedModifier(const float val , const int cost , float time_reduction , ModifyID id , const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Kinetic,"ReverseSpeed",ispercentage)
		,	CTimingModifier(time_reduction){}

	~ReverseSpeedModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	virtual void GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class DeccelerationModifier : public CModifier<float> , public CTimingModifier
{
protected:
	void CalculateCost();
public:
	DeccelerationModifier(const float val , const int cost , float time_reduction , ModifyID id , const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Kinetic,"Decceleration",ispercentage)
		,	CTimingModifier(time_reduction){}

	~DeccelerationModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	virtual void GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class BackFrictionModifier : public CModifier<float> , public CTimingModifier
{
protected:
	void CalculateCost();
public:
	BackFrictionModifier(const float val , const int cost , float time_reduction , ModifyID id , const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Kinetic,"BackFriction",ispercentage)
		,	CTimingModifier(time_reduction){}

	~BackFrictionModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	virtual void GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class FrontFrictionModifier : public CModifier<float> , public CTimingModifier
{
protected:
	void CalculateCost();
public:
	FrontFrictionModifier(const float val , const int cost , float time_reduction , ModifyID id , const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Kinetic,"FrontFriction",ispercentage)
		,	CTimingModifier(time_reduction){}

	~FrontFrictionModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	virtual void GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class InertiaModifier : public CModifier<float> , public CTimingModifier
{
protected:
	void CalculateCost();
public:
	InertiaModifier(const float val , const int cost , float time_reduction , ModifyID id , const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Kinetic,"Inertia",ispercentage)
		,	CTimingModifier(time_reduction){}

	~InertiaModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	virtual void GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class SpeedModifier : public CModifier<float> , public CTimingModifier
{
protected:
	void CalculateCost();
public:
	SpeedModifier(const float val , const int cost , float time_reduction , ModifyID id , const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Control,"Speed",ispercentage)
		,	CTimingModifier(time_reduction){}

	~SpeedModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	virtual void GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class RotationModifier : public CModifier<float> , public CTimingModifier
{
protected:
	void CalculateCost();
public:
	RotationModifier(const float val , const int cost , float time_reduction , ModifyID id , const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Control,"Rotation",ispercentage)
		,	CTimingModifier(time_reduction){}

	~RotationModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	virtual void GetLastTime(IEntity *pEntity, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};


#endif