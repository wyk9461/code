#include "StdAfx.h"
#include "DestroyableModifier.h"


void ExplodeModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	SmartScriptTable root=pEnt->GetScriptTable();
	SmartScriptTable props=MScriptBind::GetProps(root);

	props->GetValue("bExplode",prevValue);

	prev_fvalue = (float)prevValue;
	fvalue = (float)newValue;
}

void ExplodeModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable props=MScriptBind::GetProps(root);

		props->SetValue("bExplode",newValue);

		Script::CallMethod(root,"OnReset"); 
		CalculateCost();
	}
}


void ExplodeModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	if(prevValue != newValue)
		pStabilitySystem->StabilityCost(Cost);
	else
		return;
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void RadiusModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	SmartScriptTable root=pEnt->GetScriptTable();
	SmartScriptTable explosion_table=MScriptBind::GetExploTable(root);

	explosion_table->GetValue("Radius",prevValue);

	prev_fvalue = prevValue;

	if(!IsPercentage)
		fvalue = newValue;
	else
		fvalue = prevValue * newValue;
}


void RadiusModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable explosion_table=MScriptBind::GetExploTable(root);

		if(!IsPercentage)
		{
			explosion_table->SetValue("Radius", newValue);
			explosion_table->SetValue("MinRadius", newValue/2);
			explosion_table->SetValue("PhysRadius", newValue);
			explosion_table->SetValue("MinPhysRadius", newValue/2);
		}
		else
		{
			explosion_table->SetValue("Radius",prevValue * newValue);
			explosion_table->SetValue("MinRadius",prevValue * newValue/2);
			explosion_table->SetValue("PhysRadius",prevValue * newValue);
			explosion_table->SetValue("MinPhysRadius",prevValue * newValue/2);
		}

		Script::CallMethod(root,"OnReset");
		CalculateCost();
	}
}


void RadiusModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void PhysRadiusModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	SmartScriptTable root=pEnt->GetScriptTable();
	SmartScriptTable explosion_table=MScriptBind::GetExploTable(root);

	explosion_table->GetValue("PhysRadius",prevValue);

	prev_fvalue = prevValue;

	if(!IsPercentage)
		fvalue = newValue;
	else
		fvalue = prevValue * newValue;
}

void PhysRadiusModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable explosion_table=MScriptBind::GetExploTable(root);

		if(!IsPercentage)
			explosion_table->SetValue("PhysRadius", newValue);
		else
			explosion_table->SetValue("PhysRadius",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CalculateCost();
	}
}

void PhysRadiusModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void DamageModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	SmartScriptTable root=pEnt->GetScriptTable();
	SmartScriptTable explosion_table=MScriptBind::GetExploTable(root);

	explosion_table->GetValue("Damage",prevValue);

	prev_fvalue = prevValue;

	if(!IsPercentage)
		fvalue = newValue;
	else
		fvalue = prevValue * newValue;
}

void DamageModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable explosion_table=MScriptBind::GetExploTable(root);

		if(!IsPercentage)
			explosion_table->SetValue("Damage", newValue);
		else
			explosion_table->SetValue("Damage",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CalculateCost();
	}
}

void DamageModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void PressureModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	SmartScriptTable root=pEnt->GetScriptTable();
	SmartScriptTable explosion_table=MScriptBind::GetExploTable(root);

	explosion_table->GetValue("Pressure",prevValue);

	prev_fvalue = prevValue;

	if(!IsPercentage)
		fvalue = newValue;
	else
		fvalue = prevValue * newValue;
}

void PressureModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable explosion_table=MScriptBind::GetExploTable(root);

		if(!IsPercentage)
			explosion_table->SetValue("Pressure", newValue);
		else
			explosion_table->SetValue("Pressure",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CalculateCost();
	}
}

void PressureModifier::CalculateCost()
{

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void InvulnerableModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	SmartScriptTable root=pEnt->GetScriptTable();
	SmartScriptTable health_table=MScriptBind::GetHealthTable(root);

	health_table->GetValue("bInvulnerable",prevValue);

	prev_fvalue = (float)prevValue;
	fvalue = (float)newValue;
}


void InvulnerableModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable health_table=MScriptBind::GetHealthTable(root);

		health_table->SetValue("bInvulnerable",newValue);

		Script::CallMethod(root,"OnReset");
		CalculateCost();
	}
}


void InvulnerableModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	if(prevValue != newValue)
		pStabilitySystem->StabilityCost(Cost);
	else
		return;
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

void HealthModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	SmartScriptTable root=pEnt->GetScriptTable();
	SmartScriptTable health_table=MScriptBind::GetHealthTable(root);

	health_table->GetValue("MaxHealth",prevValue);

	prev_fvalue = prevValue;

	if(!IsPercentage)
		fvalue = newValue;
	else
		fvalue = prevValue * newValue;
}

void HealthModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable health_table=MScriptBind::GetHealthTable(root);

		if(!IsPercentage)
			health_table->SetValue("MaxHealth", newValue);
		else
			health_table->SetValue("MaxHealth",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CalculateCost();
	}
}

void HealthModifier::CalculateCost()
{

}