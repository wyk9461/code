#include "StdAfx.h"
#include "BasicModifier.h"


void MassModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable phys_table = MScriptBind::GetPhysTable(root);

		phys_table->GetValue("Mass",prevValue);
		prev_fvalue = prevValue;
		if(!IsPercentage)
			fvalue = newValue;
		else
			fvalue = prevValue * newValue;
	}
}

void MassModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable phys_table = MScriptBind::GetPhysTable(root);

		if(!IsPercentage)
			phys_table->SetValue("Mass", newValue);
		else
			phys_table->SetValue("Mass",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CalculateCost();
	}
}

//------------------------------------------------------------------------
void MassModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	pStabilitySystem->StabilityCost(Cost);
}


//------------------------------------------------------------------------

void RigidModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable props;
		SmartScriptTable phys_table;
		root->GetValue("Properties",props);
		props->GetValue("Physics",phys_table);

		phys_table->GetValue("bRigidBody",prevValue);

		prev_fvalue = (float)prevValue;
		fvalue = (float)newValue;
	}
}

void RigidModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable props;
		SmartScriptTable phys_table;
		root->GetValue("Properties",props);
		props->GetValue("Physics",phys_table);

		phys_table->SetValue("bRigidBody",newValue);

		float mass;
		phys_table->GetValue("Mass",mass);
		if(mass < 0)
			phys_table->SetValue("Mass",100);

		Script::CallMethod(root,"OnReset");
		CalculateCost();
	}
}

//------------------------------------------------------------------------
void RigidModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	if(prevValue != newValue)
		pStabilitySystem->StabilityCost(Cost);
	else
		return;
}


