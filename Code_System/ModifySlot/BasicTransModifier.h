#ifndef _BASICTRANSMODIFIER_H_
#define _BASICTRANSMODIFIER_H_

#include "IModifier.h"


class BasicTransModifier : public CTransformModifier
{
protected:
	void CalculateCost();
	void TransForm(IEntity *prevEntity , IEntity *transEntity);
public:
	BasicTransModifier(const char *className , const int cost , const ModifyID id)
		:	CTransformModifier(className,cost,id){}	
	~BasicTransModifier();

	void Apply(IEntity* pEnt);
};
#endif