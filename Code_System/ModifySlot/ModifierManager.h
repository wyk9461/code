#ifndef _ModifierManager_H_
#define _ModifierManager_H_

#include "StdAfx.h"
#include "ModifierRecovery.h"
#include "BasicModifier.h"
#include "BasicTransModifier.h"
#include "DestroyableModifier.h"
#include "WeaponModifier.h"
#include "LightModifier.h"
#include "AdvancedModifier.h"
#include "VehicleModifier.h"
#include "FunctionalObjectModifier.h"



enum Modifiers_Id
{
	MASS_10_PERCENT = 1,
	MASS_50_PERCENT = 2,
	MASS_90_PERCENT = 3,
	MASS_200_PERCENT = 4,
	MASS_TURNTO_1 = 10,
	MASS_TURNTO_10 = 11,
	MASS_TURNTO_1000 = 12,

	RIGID_ON = 50,
	RIGID_OFF = 51,

	EXPLODE_ON = 100,
	EXPLODE_OFF = 101,
	RADIUS_10_PERCENT = 103,
	RADIUS_50_PERCENT = 104,
	RADIUS_200_PERCENT = 105,
	DAMAGE_10_PERCENT = 200,
	DAMAGE_50_PERCENT = 201,
	DAMAGE_200_PERCENT = 202,
	DAMAGE_TURNTO_0 = 203,
	DAMAGE_TURNTO_m200 = 204,

	PRESSURE_10_PERCENT = 250,

	INVULENERABLE_ON = 300,
	INVULENERABLE_OFF = 301,

	HEALTH_10_PERCENT = 303,

	LIGHTRADIUS_10_PERCENT = 500,
	DIFFUSE_10_PERCENT = 550,
	SPEC_10_PERCENT = 600,
	HDR_10_PERCENT = 650,

	FIRERATE_10_PERCENT = 1000,
	AIDAMAGE_10_PERCENT = 1050,
	PLAYERDAMAGE_10_PERCENT = 1100,
	CLIPSIZE_10_PERCENT = 1150,

	ACCELERATION_10_PERCENT = 5000,
	TOPSPEED_10_PERCENT = 5050,
	REVERSPEED_10_PERCENT = 5100,
	DECCELETATION_10_PERCENT = 5150,
	BACKFRICTION_10_PERCENT = 5200,
	BACKFRICTION_TURNTO_0 = 5201,
	FRONTFRICTION_10_PERCENT = 5250,
	FRONTFRICTION_TURNTO_0 = 5251,

	SPEED_TURNTO_30 = 5300,
	ROTATION_TURNTO_180 = 5350,

	TRAP_RADIUS_30 = 6000,
	TRAP_EFFECT_50 = 6050,
	TRAP_EFFECT_200 = 6100,
	TRAP_DATALEAK_ENABLE = 6150,
	TRAP_DATALEAK_DISABLE = 6151,
	TRAP_SLOW_ENABLE = 6152,
	TRAP_SLOW_DISABLE = 6153,
	TRAP_DEDICATIENABLE_ENABLE = 6154,
	TRAP_DEDICATIENABLE_DISABLE = 6155,
	TRAP_RECYCLE_ENABLE = 6156,
	TRAP_RECYCLE_DISABLE = 6157,
	TRAP_DAMAGECOV_ENABLE = 6158,
	TRAP_DAMAGECOV_DISABLE = 6159,

	LEAKHOLE_CD_30 = 6500,
	LEAKHOLE_DAMAGE_50 = 6550,
	LEAKHOLE_LENGTH_50 = 6600,

	FORCEDEVICE_HITLIMITS_200 = 7000,
	FORCEDEVICE_DAMAGE_150 = 7050,
	FORCEDEVICE_RADIUS_150 = 7100,
	FORCEDEVICE_FORCE_200 = 7150,

	POWERBALL_DAMAGE_200 = 7500,
	POWERBALL_RADIUS_50 = 7550,
	POWERBALL_CRITICAL_200 = 7600,

	FORCEFIELD_RADIUS_150 = 8000,
	FORCEFIELD_SCALE_50 = 8050,

	LEG_DISABLE_10S = 50000,
	ARM_DISABLE_10S = 50010,
	EYE_DISABLE_10S = 50020,
	MIND_DISABLE_10S = 50030,
	MIND_CHANGE_10S  = 50040,
	ARM_WEAK_10S = 50050,
	LEG_WEAK_10S = 50060,
};


class ModifierManager
{
private:
	typedef std::map<ModifyID,IModifier*> ModifyMap;

	ModifyMap Modifiers;
	ModifierRecoveryManager *MRM ;
public:
	ModifierManager();
	~ModifierManager();
	IModifier *GetModifier(ModifyID Id);
	void AddModifier(IModifier *Modifier);
	void RemoveModifier(ModifyID Id);
	void ResetAllModifier(){MRM->RecoveryAllWeapon();}
	ModifierRecoveryManager *GetModifierRecoveryManager(){return MRM;}

private:

	//--------BasicModifier--------//

	/*MassModifier*/
	MassModifier *m_MassModifier_10;
	MassModifier *m_MassModifier_50;
	MassModifier *m_MassModifier_90;
	MassModifier *m_MassModifier_200;
	MassModifier *m_MassModifier_v1;
	MassModifier *m_MassModifier_v10;
	MassModifier *m_MassModifier_v1000;

	/*RigidModifier*/
	RigidModifier *m_RigidModifier_Rigid;
	RigidModifier *m_RigidModifier_Static;

	//~~~~~~~~BasicModifier~~~~~~~~//



	//--------DestroyableModifier--------//

	/*ExplodeModifier*/
	ExplodeModifier *m_ExplodeModifier_On;
	ExplodeModifier *m_ExplodeModifier_Off;

	/*RadiusModifier*/
	RadiusModifier *m_RadiusModifier_10;
	RadiusModifier *m_RadiusModifier_50;
	RadiusModifier *m_RadiusModifier_200;

	/*PhysRadiusModifier*/

	/*DamageModifier*/
	DamageModifier *m_DamageModifier_10;
	DamageModifier *m_DamageModifier_50;
	DamageModifier *m_DamageModifier_200;
	DamageModifier *m_DamageModifier_v0;
	DamageModifier *m_DamageModifier_vm200;

	/*PressureModifier*/
	PressureModifier *m_PressureModifier_10;

	/*InvulnerableModifier*/
	InvulnerableModifier *m_InvulenerableModifier_On;
	InvulnerableModifier *m_InvulenerableModifier_Off;

	/*HealthModifier*/
	HealthModifier *m_HealthModifier_10;

	//~~~~~~~~DestroyableModifier~~~~~~~~//



	//--------LightModifier--------//

	/*Light_Radius_Modifier*/
	LightRadiusModifier *m_LightRadiusModifier_10;

	/*DiffuseModifier*/
	DiffuseModifier *m_DiffuseModifier_10;

	/*SpecularModifier*/
	SpecularModifier *m_SpecModifier_10;

	/*HDRModifier*/
	HDRModifier *m_HDRModifier_10;

	//~~~~~~~~LightModifier~~~~~~~~//



	//--------WeaponModifier--------//

	/*FireRateModifier*/
	FireRateModifier *m_FireRateModifier_10;

	/*AI_PlayerDamageModifier*/
	AI_PlayerDamageModifier *m_AIDamageModifier_10;

	/*Player_AIDamageModifier*/
	Player_AIDamageModifier *m_PlayerDamageModifier_10;

	/*ClipSizeModifier*/
	ClipSizeModifier *m_ClipSizeModifier_120;

	//~~~~~~~~WeaponModifier~~~~~~~~//



	//--------VechileModifier--------//

	/*AccelerationModifier*/
	AccelerationModifier *m_AccelerationModifier_50;

	/*TopSpeedModifier*/
	TopSpeedModifier *m_TopSpeedModifier_30;

	/*ReverseSpeedModifier*/
	ReverseSpeedModifier *m_ReverseSpeedModifier_20;

	/*DeccelerationModifier*/
	DeccelerationModifier *m_DeccelerationModifier_10;

	/*BackFrictionModifier*/
	BackFrictionModifier *m_BFrictionModifier_10;
	BackFrictionModifier *m_BFrictionModifier_v0;

	/*FrontFrictionModifier*/
	FrontFrictionModifier *m_FFrictionModifier_10;
	FrontFrictionModifier *m_FFrictionModifier_v0;

	/*SpeedModifier*/
	SpeedModifier *m_SpeedModifier_v30;

	/*RotationModifier*/
	RotationModifier *m_RotationModifier_v180;

	//~~~~~~~~VechileModifier~~~~~~~~//



	//--------AdvModifier--------//

	/*LegDisableModifier*/
	LegDisableModifier *m_LegDisableModifier_10s;

	/*ArmDisableModifier*/
	ArmDisableModifier *m_ArmDisableModifier_10s;

	/*EyeDisableModifier*/
	EyeDisableModifier *m_EyeDisableMoifier_10s;

	/*MindDisableModifier*/
	MindDisableModifier *m_MindDisableModifier_10s;

	/*MindChangeModifier*/
	MindChangeModifier *m_MindChangeModifier_30s;

	/*ArmWeakModifier*/
	ArmWeakModifier *m_ArmWeakModifier_10s;

	/*LegWeakModifier*/
	LegWeakModifier *m_LegWeakModifier_10s;

	//~~~~~~~~AdvModifier~~~~~~~~//


	//--------TrapModifier--------//
	TrapRadiusModifier *m_TrapRadiusModifier_30;
	TrapEffectModifier *m_TrapEffectModifier_50;
	TrapEffectModifier *m_TrapEffectModifier_200;
	TrapTypeModifier *m_TrapDataLeak_Enable;
	TrapTypeModifier *m_TrapDataLeak_Disable;
	TrapTypeModifier *m_TrapSlow_Enable;
	TrapTypeModifier *m_TrapSlow_Disable;
	TrapTypeModifier *m_TrapDedication_Enable;
	TrapTypeModifier *m_TrapDedication_Disable;
	TrapTypeModifier *m_TrapRecycle_Enable;
	TrapTypeModifier *m_TrapRecycle_Disable;
	TrapTypeModifier *m_TrapDamageCov_Enable;
	TrapTypeModifier *m_TrapDamageCov_Disable;
	//~~~~~~~~TrapModifier~~~~~~~~//

	//--------LeakHoleModifier--------//
	LeakHoleCdModifier *m_LeakHoleCD_30;
	LeakHoleDamageModifier *m_LeakHoleDamage_50;
	LeakHoleLengthModifier *m_LeakHoleLength_50;
	//~~~~~~~~LeakHoleModifier~~~~~~~~//

	//--------ForceDevice--------//
	ForceDeviceHitLimitsModifier *m_ForceDeviceHitLimits_200;
	ForceDeviceDamageModifier *m_ForceDeviceDamage_150;
	ForceDeviceRadiusModifier *m_ForceDeviceRadius_150;
	ForceDeviceForceModifier *m_ForceDeviceForce_200;
	//~~~~~~~~ForceDevice~~~~~~~~//

	//--------PowerBall--------//
	PowerBallDamageModifier *m_PowerBallDamage_200;
	PowerBallRadiusModifier *m_PowerBallRadius_50;
	PowerBallCriticalModifier *m_PowerBallCritical_200;
	//~~~~~~~~PowerBall~~~~~~~~//

	//--------ForceField--------//
	ForceFieldRadiusModifier *m_ForceFieldRadius_150;
	ForceFieldScaleModifier *m_ForceFieldScale_50;
	//~~~~~~~~ForceField~~~~~~~~//
};
#endif