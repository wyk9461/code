#ifndef _BASICMODIFIER_H_
#define _BASICMODIFIER_H_

#include "IModifier.h"

class MassModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	MassModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Physics,"Mass",ispercentage){}

	~MassModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

class RigidModifier : public CModifier<int>
{
protected:
	void CalculateCost();
public:
	RigidModifier(const int val,const int cost,ModifyID id)
		:	CModifier(val,cost,id,eMODT_Physics,"Rigid",false){}

	~RigidModifier(){}
	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};


#endif