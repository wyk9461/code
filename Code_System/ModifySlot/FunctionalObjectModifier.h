#ifndef _FUNCTIONALOBJECTMODIFIER_H_
#define _FUNCTIONALOBJECTMODIFIER_H_

#include "IModifier.h"


//------------------------------------------------------------------------
//Trap
//------------------------------------------------------------------------

enum TrapType
{
	eTT_DataLeak,
	eTT_Slow,
	eTT_Dedication,
	eTT_Recycle,
	eTT_DamageCov,
};

class TrapRadiusModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	TrapRadiusModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Control,"Radius",ispercentage){}

	~TrapRadiusModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class TrapEffectModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	TrapEffectModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Control,"Effect",ispercentage){}

	~TrapEffectModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class TrapTypeModifier : public CModifier<int>
{
private:
	int Type;
protected:
	void CalculateCost();
public:
	TrapTypeModifier(const int &val,const int cost,ModifyID id,int type)
		:	CModifier(val,cost,id,eMODT_Disable,"Type",false)
		,	Type(type)
	{
		if(Type == eTT_DataLeak)
			CModifier<int>::SetDesc("Type-DataLeak");
		else if(Type == eTT_Slow)
			CModifier<int>::SetDesc("Type-Slow");
		else if(Type == eTT_Dedication)
			CModifier<int>::SetDesc("Type-Dedication");
		else if(Type == eTT_Recycle)
			CModifier<int>::SetDesc("Type-Recycle");
		else if(Type == eTT_DamageCov)
			CModifier<int>::SetDesc("Type-DamageCov");
	}

	~TrapTypeModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};


//------------------------------------------------------------------------
//LeakHole
//------------------------------------------------------------------------
class LeakHoleCdModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	LeakHoleCdModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Control,"Cd",ispercentage){}

	~LeakHoleCdModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class LeakHoleDamageModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	LeakHoleDamageModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Damage,"Damage",ispercentage){}

	~LeakHoleDamageModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class LeakHoleLengthModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	LeakHoleLengthModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Damage,"Length",ispercentage){}

	~LeakHoleLengthModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
//ForceDevice
//------------------------------------------------------------------------
class ForceDeviceHitLimitsModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	ForceDeviceHitLimitsModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Control,"HitTimes",ispercentage){}

	~ForceDeviceHitLimitsModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class ForceDeviceDamageModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	ForceDeviceDamageModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Damage,"Damage",ispercentage){}

	~ForceDeviceDamageModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class ForceDeviceRadiusModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	ForceDeviceRadiusModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Damage,"Radius",ispercentage){}

	~ForceDeviceRadiusModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class ForceDeviceForceModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	ForceDeviceForceModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Damage,"Force",ispercentage){}

	~ForceDeviceForceModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};


//------------------------------------------------------------------------
//PowerBall
//------------------------------------------------------------------------
class PowerBallDamageModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	PowerBallDamageModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Damage,"Damage",ispercentage){}

	~PowerBallDamageModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class PowerBallRadiusModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	PowerBallRadiusModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Damage,"Radius",ispercentage){}

	~PowerBallRadiusModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class PowerBallCriticalModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	PowerBallCriticalModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Damage,"CriticalDamage",ispercentage){}

	~PowerBallCriticalModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
//ForceField
//------------------------------------------------------------------------
class ForceFieldRadiusModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	ForceFieldRadiusModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Control,"Radius",ispercentage){}

	~ForceFieldRadiusModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class ForceFieldScaleModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	ForceFieldScaleModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Control,"ForceScale",ispercentage){}

	~ForceFieldScaleModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};


#endif