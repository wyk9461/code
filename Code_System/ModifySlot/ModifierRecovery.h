#ifndef _MODIFIERRECOVERY_H_
#define _MODIFIERRECOVERY_H_

#include "IModifier.h"
#include "WeaponModifier.h"

enum WeaponModifier_type
{
	BasicModifier = 0x000001,
	ShotgunModifier = 0x000002,
};

struct WeaponParams
{
	//May replaced by WeaponUtils(in EntityPaamsUtils.h)
	short rate;
	short ai_vs_player_damage;
	short player_vs_ai_damage;
	short clip_size;
	WeaponParams &operator=(const WeaponParams &rhs)
	{
		if(this == &rhs)
			return *this;
		rate = rhs.rate;
		ai_vs_player_damage=rhs.ai_vs_player_damage;
		player_vs_ai_damage=rhs.player_vs_ai_damage;
		clip_size=rhs.clip_size;
		return *this;
	}
};

struct WeaponRecovery
{
	CFireMode* pFiremode;
	WeaponParams params;

	string weaponClass;
	float tick_timer;
	float last_time;
	WeaponRecovery()
	:	pFiremode(NULL)
	,	weaponClass("")
	,	tick_timer(0)
	,	last_time(60){}
	WeaponRecovery &operator=(const WeaponRecovery &rhs)
	{
		if(this == &rhs)
			return *this;
		pFiremode=rhs.pFiremode;
		weaponClass=rhs.weaponClass;
		tick_timer=rhs.tick_timer;
		last_time=rhs.last_time;
		return *this;
	}
};

struct VehicleParams
{
	//May replaced by VehicleUtils(in EntityPaamsUtils.h)
	float acceleration;
	float top_speed;
	float reverseSpeed;
	float decceleration;
	float backFriction;
	float frontFriction;
	VehicleParams &operator=(const VehicleParams &rhs)
	{
		if(this == &rhs)
			return *this;
		acceleration = rhs.acceleration;
		top_speed = rhs.top_speed;
		reverseSpeed = rhs.reverseSpeed;
		decceleration = rhs.decceleration;
		backFriction = rhs.backFriction;
		frontFriction = rhs.frontFriction;
		return *this;
	}
};

struct VehicleRecovery
{
	string vehicleClass;
	VehicleParams params;

	float tick_timer;
	float last_time;
	VehicleRecovery()
		:	tick_timer(0)
		,	last_time(60){}
	VehicleRecovery &operator=(const VehicleRecovery &rhs)
	{
		if(this == &rhs)
			return *this;
		vehicleClass=rhs.vehicleClass;
		params=rhs.params;
		tick_timer=rhs.tick_timer;
		last_time=rhs.last_time;
		return *this;
	}
};

struct AdvancedModifierRecovery
{
	EntityId Id;
	float tick_timer;
	float last_time;
	CAdvancedModifier *pModifier;
	AdvancedModifierRecovery()
	:	Id(0)
	,	tick_timer(0)
	,	last_time(0)
	,	pModifier(NULL){}

	AdvancedModifierRecovery &operator=(const AdvancedModifierRecovery &advm)
	{
		if(this == &advm)
			return *this;
		Id=advm.Id;
		tick_timer=advm.tick_timer;
		last_time=advm.last_time;
		pModifier=advm.pModifier;
		return *this;
	}
};

class ModifierRecoveryManager : public IGameFrameworkListener
{
private:
	typedef unsigned ModifyLevel;
	typedef std::map<string , WeaponRecovery> WRecoveryMap;
	typedef std::map<string , VehicleRecovery>	VRecoveryMap;
	typedef std::list<AdvancedModifierRecovery> AdvList;

	WRecoveryMap WeaponList;
	VRecoveryMap VehicleList;
	AdvList AdvModifierList;

	void Back_To_Default(WeaponRecovery &wr);
	void Back_To_Default(AdvancedModifierRecovery &advMR);
	void Back_To_Default(VehicleRecovery &vr);
	WeaponParams RetriveWeaponDefault(IEntity* pEnt,CFireMode *pFireMode);

	float MaxVehicleTime;
	float MaxWeaponTime;
public:
	ModifierRecoveryManager();
	~ModifierRecoveryManager(){gEnv->pGame->GetIGameFramework()->UnregisterListener(this);}
	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) {}
	virtual void OnPostUpdate(float fDelta);

	void AddtoWeaponList(IEntity* pEnt , const float reTime);
	void AddtoVehicleList(const string& name, const float reTime);
	void AddtoAdvancedModifierList(IEntity *pEnt , float last_time,CAdvancedModifier *pModifier);	
	void RecoveryAllWeapon();
	void RecoveryAll();

	void SetMaxVehicleTime(float _time){MaxVehicleTime = _time;}
	void SetMaxWeaponTime(float _time){MaxWeaponTime = _time;}

	const float GetMaxVehicleTime(){return MaxVehicleTime;}
	const float GetMaxWeaponTime(){return MaxWeaponTime;}

	const float GetCurVehicleLastTime(IEntity *pEntity);
	const float GetCurWeaponLastTime(IEntity *pEntity);
};

#endif