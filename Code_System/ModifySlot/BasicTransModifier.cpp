#include "StdAfx.h"
#include "BasicTransModifier.h"

void BasicTransModifier::Apply(IEntity* pEnt)
{
	prevClass = pEnt->GetClass();
	if(prevClass == TransClass)
	{
		CryLogAlways("Same Class");
		return;
	}
	SEntitySpawnParams params;
	params.nFlags=pEnt->GetFlags();
	params.vPosition=pEnt->GetPos();
	params.sName=pEnt->GetName();
	params.qRotation=pEnt->GetRotation();
	params.pClass=TransClass;

	gEnv->pEntitySystem->RemoveEntity(pEnt->GetId());
	IEntity *newEntity=gEnv->pEntitySystem->SpawnEntity(params);

	TransForm(pEnt,newEntity);
}


void BasicTransModifier::TransForm(IEntity *prevEntity , IEntity *transEntity)
{
	char *SpawnName;
	int DeleteCost;
	float SpawnTime;
	int StabilityCost;
	char* object_Model;

	SmartScriptTable prevRoot=prevEntity->GetScriptTable();
	SmartScriptTable prevProps=MScriptBind::GetProps(prevRoot);
	SmartScriptTable prevPhys_table=MScriptBind::GetPhysTable(prevRoot);

	prevProps->GetValue("SpawnName",SpawnName);
	prevProps->GetValue("DeleteCost",DeleteCost);
	prevProps->GetValue("SpawnTime",SpawnTime);
	prevProps->GetValue("StabilityCost",StabilityCost);
	prevProps->GetValue("object_Model",object_Model);

	SmartScriptTable root=transEntity->GetScriptTable();
	SmartScriptTable props=MScriptBind::GetProps(root);

	props->SetValue("SpawnName",SpawnName);
	props->SetValue("DeleteCost",DeleteCost);
	props->SetValue("SpawnTime",SpawnTime);
	props->SetValue("StabilityCost",StabilityCost);
	props->SetValue("object_Model",object_Model);

	props->SetValue("Physics",prevPhys_table);

	Script::CallMethod(root,"OnPropertyChange");
	Script::CallMethod(root,"OnReset");
}
void BasicTransModifier::CalculateCost()
{

}

