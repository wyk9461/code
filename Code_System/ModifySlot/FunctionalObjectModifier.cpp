#include "StdAfx.h"
#include "FunctionalObjectModifier.h"
#include "Code_System/CodeObject/Trap.h"
#include "Code_System/CodeObject/LeakHole.h"
#include "Code_System/CodeObject/ForceDevice.h"
#include "Code_System/CodeObject/PowerBall.h"
#include "Code_System/CodeObject/ForceField.h"

//------------------------------------------------------------------------
//Trap
//------------------------------------------------------------------------
void TrapRadiusModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		func_table->GetValue("Radius",prevValue);
		prev_fvalue = prevValue;
		if(!IsPercentage)
			fvalue = newValue;
		else
			fvalue = prevValue * newValue;
	}
}

void TrapRadiusModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		if(!IsPercentage)
			func_table->SetValue("Radius", newValue);
		else
			func_table->SetValue("Radius",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CTrap* pTrap = static_cast<CTrap*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEnt->GetId(), "Trap"));
		pTrap->ReadScript();
		CalculateCost();
	}
}

void TrapRadiusModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	pStabilitySystem->StabilityCost(Cost);
}


//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
void TrapEffectModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		func_table->GetValue("EffectScale",prevValue);
		prev_fvalue = prevValue;
		if(!IsPercentage)
			fvalue = newValue;
		else
			fvalue = prevValue * newValue;
	}
}

void TrapEffectModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		if(!IsPercentage)
			func_table->SetValue("EffectScale", newValue);
		else
			func_table->SetValue("EffectScale",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CTrap* pTrap = static_cast<CTrap*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEnt->GetId(), "Trap"));
		pTrap->ReadScript();
		CalculateCost();
	}
}

void TrapEffectModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	pStabilitySystem->StabilityCost(Cost);
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
void TrapTypeModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		if(Type == eTT_DataLeak)
			func_table->GetValue("isDataLeakage",prevValue);
		else if(Type == eTT_Slow)
			func_table->GetValue("isSlowDown",prevValue);
		else if(Type == eTT_Dedication)
			func_table->GetValue("isDedication",prevValue);
		else if(Type == eTT_Recycle)
			func_table->GetValue("isRecycle",prevValue);
		else if(Type == eTT_DamageCov)
			func_table->GetValue("isDamageCov",prevValue);

		prev_fvalue = (float)prevValue;
		fvalue = (float)newValue;
	}
}

void TrapTypeModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		if(Type == eTT_DataLeak)
			func_table->SetValue("isDataLeakage", newValue);
		else if(Type == eTT_Slow)
			func_table->SetValue("isSlowDown", newValue);
		else if(Type == eTT_Dedication)
			func_table->SetValue("isDedication", newValue);
		else if(Type == eTT_Recycle)
			func_table->SetValue("isRecycle", newValue);
		else if(Type == eTT_DamageCov)
			func_table->SetValue("isDamageCov", newValue);

		Script::CallMethod(root,"OnReset");
		CTrap* pTrap = static_cast<CTrap*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEnt->GetId(), "Trap"));
		pTrap->ReadScript();
		CalculateCost();
	}
}

void TrapTypeModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	pStabilitySystem->StabilityCost(Cost);
}

//------------------------------------------------------------------------
//LeakHole
//------------------------------------------------------------------------
void LeakHoleCdModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		func_table->GetValue("Cd",prevValue);
		prev_fvalue = prevValue;
		if(!IsPercentage)
			fvalue = newValue;
		else
			fvalue = prevValue * newValue;
	}
}

void LeakHoleCdModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		if(!IsPercentage)
			func_table->SetValue("Cd", newValue);
		else
			func_table->SetValue("Cd",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CLeakHole* pLeakHole = static_cast<CLeakHole*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEnt->GetId(), "LeakHole"));
		pLeakHole->ReadScript();
		CalculateCost();
	}
}

void LeakHoleCdModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	pStabilitySystem->StabilityCost(Cost);
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
void LeakHoleDamageModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		func_table->GetValue("Damage",prevValue);
		prev_fvalue = prevValue;
		if(!IsPercentage)
			fvalue = newValue;
		else
			fvalue = prevValue * newValue;
	}
}

void LeakHoleDamageModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		if(!IsPercentage)
			func_table->SetValue("Damage", newValue);
		else
			func_table->SetValue("Damage",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CLeakHole* pLeakHole = static_cast<CLeakHole*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEnt->GetId(), "LeakHole"));
		pLeakHole->ReadScript();
		CalculateCost();
	}
}

void LeakHoleDamageModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	pStabilitySystem->StabilityCost(Cost);
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
void LeakHoleLengthModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		func_table->GetValue("Radius",prevValue);		//Remember to Change all Radius to Length
		prev_fvalue = prevValue;
		if(!IsPercentage)
			fvalue = newValue;
		else
			fvalue = prevValue * newValue;
	}
}

void LeakHoleLengthModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		if(!IsPercentage)
			func_table->SetValue("Radius", newValue);
		else
			func_table->SetValue("Radius",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CLeakHole* pLeakHole = static_cast<CLeakHole*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEnt->GetId(), "LeakHole"));
		pLeakHole->ReadScript();
		CalculateCost();
	}
}

void LeakHoleLengthModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	pStabilitySystem->StabilityCost(Cost);
}


//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
void ForceDeviceHitLimitsModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		func_table->GetValue("HitTimesLimit",prevValue);		//Remember to Change all Radius to Length
		prev_fvalue = prevValue;
		if(!IsPercentage)
			fvalue = newValue;
		else
			fvalue = prevValue * newValue;
	}
}

void ForceDeviceHitLimitsModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		if(!IsPercentage)
			func_table->SetValue("HitTimesLimit", (int)newValue);
		else
			func_table->SetValue("HitTimesLimit",(int)(prevValue * newValue));

		Script::CallMethod(root,"OnReset");
		CForceDevice* pForceDevice = static_cast<CForceDevice*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEnt->GetId(), "ForceDevice"));
		pForceDevice->ReadScript();
		CalculateCost();
	}
}

void ForceDeviceHitLimitsModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	pStabilitySystem->StabilityCost(Cost);
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
void ForceDeviceDamageModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		func_table->GetValue("Damage",prevValue);		//Remember to Change all Radius to Length
		prev_fvalue = prevValue;
		if(!IsPercentage)
			fvalue = newValue;
		else
			fvalue = prevValue * newValue;
	}
}

void ForceDeviceDamageModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		if(!IsPercentage)
			func_table->SetValue("Damage", newValue);
		else
			func_table->SetValue("Damage",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CForceDevice* pForceDevice = static_cast<CForceDevice*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEnt->GetId(), "ForceDevice"));
		pForceDevice->ReadScript();
		CalculateCost();
	}
}

void ForceDeviceDamageModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	pStabilitySystem->StabilityCost(Cost);
}


//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
void ForceDeviceRadiusModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		func_table->GetValue("ExploRadius",prevValue);		//Remember to Change all Radius to Length
		prev_fvalue = prevValue;
		if(!IsPercentage)
			fvalue = newValue;
		else
			fvalue = prevValue * newValue;
	}
}

void ForceDeviceRadiusModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		if(!IsPercentage)
			func_table->SetValue("ExploRadius", newValue);
		else
			func_table->SetValue("ExploRadius",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CForceDevice* pForceDevice = static_cast<CForceDevice*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEnt->GetId(), "ForceDevice"));
		pForceDevice->ReadScript();
		CalculateCost();
	}
}

void ForceDeviceRadiusModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	pStabilitySystem->StabilityCost(Cost);
}


//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
void ForceDeviceForceModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		func_table->GetValue("Force",prevValue);		//Remember to Change all Radius to Length
		prev_fvalue = prevValue;
		if(!IsPercentage)
			fvalue = newValue;
		else
			fvalue = prevValue * newValue;
	}
}

void ForceDeviceForceModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		if(!IsPercentage)
			func_table->SetValue("Force", newValue);
		else
			func_table->SetValue("Force",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CForceDevice* pForceDevice = static_cast<CForceDevice*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEnt->GetId(), "ForceDevice"));
		pForceDevice->ReadScript();
		CalculateCost();
	}
}

void ForceDeviceForceModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	pStabilitySystem->StabilityCost(Cost);
}


//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
void PowerBallDamageModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		func_table->GetValue("Damage",prevValue);		//Remember to Change all Radius to Length
		prev_fvalue = prevValue;
		if(!IsPercentage)
			fvalue = newValue;
		else
			fvalue = prevValue * newValue;
	}
}

void PowerBallDamageModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		if(!IsPercentage)
			func_table->SetValue("Damage", newValue);
		else
			func_table->SetValue("Damage",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CPowerBall* pPowerBall = static_cast<CPowerBall*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEnt->GetId(), "PowerBall"));
		pPowerBall->ReadScript();
		CalculateCost();
	}
}

void PowerBallDamageModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	pStabilitySystem->StabilityCost(Cost);
}


//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
void PowerBallRadiusModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		func_table->GetValue("ExploRadius",prevValue);		//Remember to Change all Radius to Length
		prev_fvalue = prevValue;
		if(!IsPercentage)
			fvalue = newValue;
		else
			fvalue = prevValue * newValue;
	}
}

void PowerBallRadiusModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		if(!IsPercentage)
			func_table->SetValue("ExploRadius", newValue);
		else
			func_table->SetValue("ExploRadius",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CPowerBall* pPowerBall = static_cast<CPowerBall*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEnt->GetId(), "PowerBall"));
		pPowerBall->ReadScript();
		CalculateCost();
	}
}

void PowerBallRadiusModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	pStabilitySystem->StabilityCost(Cost);
}


//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
void PowerBallCriticalModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		func_table->GetValue("PowerUpperBound",prevValue);		//Remember to Change all Radius to Length
		prev_fvalue = prevValue;
		if(!IsPercentage)
			fvalue = newValue;
		else
			fvalue = prevValue * newValue;
	}
}

void PowerBallCriticalModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		if(!IsPercentage)
			func_table->SetValue("PowerUpperBound", newValue);
		else
			func_table->SetValue("PowerUpperBound",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CPowerBall* pPowerBall = static_cast<CPowerBall*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEnt->GetId(), "PowerBall"));
		pPowerBall->ReadScript();
		CalculateCost();
	}
}

void PowerBallCriticalModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	pStabilitySystem->StabilityCost(Cost);
}


//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
void ForceFieldRadiusModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		func_table->GetValue("EffectRadius",prevValue);		//Remember to Change all Radius to Length
		prev_fvalue = prevValue;
		if(!IsPercentage)
			fvalue = newValue;
		else
			fvalue = prevValue * newValue;
	}
}

void ForceFieldRadiusModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		if(!IsPercentage)
			func_table->SetValue("EffectRadius", newValue);
		else
			func_table->SetValue("EffectRadius",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CForceField* pForceField = static_cast<CForceField*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEnt->GetId(), "ForceField"));
		pForceField->ReadScript();
		CalculateCost();
	}
}

void ForceFieldRadiusModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	pStabilitySystem->StabilityCost(Cost);
}


//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
void ForceFieldScaleModifier::GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		func_table->GetValue("AccScale",prevValue);		//Remember to Change all Radius to Length
		prev_fvalue = prevValue;
		if(!IsPercentage)
			fvalue = newValue;
		else
			fvalue = prevValue * newValue;
	}
}

void ForceFieldScaleModifier::Apply(IEntity* pEnt)
{
	if(pEnt)
	{
		SmartScriptTable root=pEnt->GetScriptTable();
		SmartScriptTable func_table = MScriptBind::GetFunctionTable(root);

		if(!IsPercentage)
			func_table->SetValue("AccScale", newValue);
		else
			func_table->SetValue("AccScale",prevValue * newValue);

		Script::CallMethod(root,"OnReset");
		CForceField* pForceField = static_cast<CForceField*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEnt->GetId(), "ForceField"));
		pForceField->ReadScript();
		CalculateCost();
	}
}

void ForceFieldScaleModifier::CalculateCost()
{
	WorldStability *pStabilitySystem=g_pGame->GetWorldStability();
	pStabilitySystem->StabilityCost(Cost);
}
