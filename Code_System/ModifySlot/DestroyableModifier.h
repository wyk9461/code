#ifndef _DESTROYABLEMODIFIER_H_
#define _DESTROYABLEMODIFIER_H_

#include "IModifier.h"


class ExplodeModifier : public CModifier<int>
{
protected:
	void CalculateCost();
public:
	ExplodeModifier(const int val,const int cost,ModifyID id)
		:	CModifier(val,cost,id,eMODT_Damage,"Explode",false){}

	~ExplodeModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class RadiusModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	RadiusModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Damage,"Radius",ispercentage){}

	~RadiusModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class PhysRadiusModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	PhysRadiusModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Damage,"PhysRadius",ispercentage){}

	~PhysRadiusModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class DamageModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	DamageModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Damage,"Damage",ispercentage){}

	~DamageModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class PressureModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	PressureModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Damage,"Pressure",ispercentage){}

	~PressureModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};


//------------------------------------------------------------------------
class InvulnerableModifier : public CModifier<int>
{
protected:
	void CalculateCost();
public:
	InvulnerableModifier(const int val,const int cost,ModifyID id)
		:	CModifier(val,cost,id,eMODT_Health,"Invulnerable",false){}

	~InvulnerableModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};

//------------------------------------------------------------------------
class HealthModifier : public CModifier<float>
{
protected:
	void CalculateCost();
public:
	HealthModifier(const float val,const int cost,ModifyID id,const bool ispercentage)
		:	CModifier(val,cost,id,eMODT_Health,"Health",ispercentage){}

	~HealthModifier(){}

	void GetPrevValue(IEntity* pEnt, float &prev_fvalue, float &fvalue);
	void Apply(IEntity* pEnt);
};


#endif