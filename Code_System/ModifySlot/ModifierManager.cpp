#include "StdAfx.h"
#include "ModifierManager.h"
#include "ModifierRecovery.h"

ModifierManager::ModifierManager()
{
	MRM = new ModifierRecoveryManager();

	//--------BasicModifier--------//
	m_MassModifier_10 = new MassModifier(0.1 , 150 , MASS_10_PERCENT , true);
	AddModifier(m_MassModifier_10);

	m_MassModifier_50 = new MassModifier(0.5 , 100 , MASS_50_PERCENT , true);
	AddModifier(m_MassModifier_50);

	m_MassModifier_90 = new MassModifier(0.9 , 50 , MASS_90_PERCENT , true);
	AddModifier(m_MassModifier_90);

	m_MassModifier_200 = new MassModifier(2 , 50 , MASS_200_PERCENT , true);
	AddModifier(m_MassModifier_200);

	m_MassModifier_v1 = new MassModifier(1 , 80 , MASS_TURNTO_1 , false);
	AddModifier(m_MassModifier_v1);

	m_MassModifier_v10 = new MassModifier(10 , 60 , MASS_TURNTO_10 , false);
	AddModifier(m_MassModifier_v10);

	m_MassModifier_v1000 = new MassModifier(1000 , 50 , MASS_TURNTO_1000 , false);
	AddModifier(m_MassModifier_v1000);

	m_RigidModifier_Rigid = new RigidModifier(1,50,RIGID_ON);
	AddModifier(m_RigidModifier_Rigid);

	m_RigidModifier_Static = new RigidModifier(0,50,RIGID_OFF);
	AddModifier(m_RigidModifier_Static);
	//~~~~~~~~BasicModifier~~~~~~~~//


	//--------DestroyableModifier--------//
	m_ExplodeModifier_On = new ExplodeModifier(1,50,EXPLODE_ON);
	AddModifier(m_ExplodeModifier_On);

	m_ExplodeModifier_Off = new ExplodeModifier(0,50,EXPLODE_OFF);
	AddModifier(m_ExplodeModifier_Off);

	m_RadiusModifier_10 = new RadiusModifier(0.1 , 150 , RADIUS_10_PERCENT , true);
	AddModifier(m_RadiusModifier_10);

	m_RadiusModifier_50 = new RadiusModifier(0.5 , 150 , RADIUS_50_PERCENT , true);
	AddModifier(m_RadiusModifier_50);

	m_RadiusModifier_200 = new RadiusModifier(2 , 150 , RADIUS_200_PERCENT , true);
	AddModifier(m_RadiusModifier_200);

	m_DamageModifier_10 = new DamageModifier(0.1 , 200 , DAMAGE_10_PERCENT , true);
	AddModifier(m_DamageModifier_10);

	m_DamageModifier_50 = new DamageModifier(0.5 , 200 , DAMAGE_50_PERCENT , true);
	AddModifier(m_DamageModifier_50);

	m_DamageModifier_200 = new DamageModifier(2 , 200 , DAMAGE_200_PERCENT , true);
	AddModifier(m_DamageModifier_200);

	m_DamageModifier_v0 = new DamageModifier(0 , 200 , DAMAGE_TURNTO_0 , false);
	AddModifier(m_DamageModifier_v0);

	m_DamageModifier_vm200 = new DamageModifier(-200 , 200 , DAMAGE_TURNTO_m200 , false);
	AddModifier(m_DamageModifier_vm200);

	m_PressureModifier_10 = new PressureModifier(0.1 , 80 , PRESSURE_10_PERCENT , true);
	AddModifier(m_PressureModifier_10);

	m_InvulenerableModifier_On = new InvulnerableModifier(1,50,INVULENERABLE_ON);
	AddModifier(m_InvulenerableModifier_On);

	m_InvulenerableModifier_Off = new InvulnerableModifier(0,50,INVULENERABLE_OFF);
	AddModifier(m_InvulenerableModifier_Off);

	m_HealthModifier_10 = new HealthModifier(0.1 , 100 , HEALTH_10_PERCENT , true);
	AddModifier(m_HealthModifier_10);
	//~~~~~~~~DestroyableModifier~~~~~~~~//


	//--------LightModifier--------//
	m_LightRadiusModifier_10 = new LightRadiusModifier(0.1,50,LIGHTRADIUS_10_PERCENT,true);

	m_DiffuseModifier_10 = new DiffuseModifier(0.1,50,DIFFUSE_10_PERCENT,true);

	m_SpecModifier_10 = new SpecularModifier(0.1,50,SPEC_10_PERCENT,true);

	m_HDRModifier_10 = new HDRModifier(0.1,50,HDR_10_PERCENT,true);
	//~~~~~~~~LightModifier~~~~~~~~//


	//--------WeaponModifier--------//
	m_FireRateModifier_10 = new FireRateModifier(0.1 , 150 , 50 , FIRERATE_10_PERCENT , true);
	AddModifier(m_FireRateModifier_10);

	m_AIDamageModifier_10 = new AI_PlayerDamageModifier(0.1 , 150 , 50 , AIDAMAGE_10_PERCENT , true);
	AddModifier(m_AIDamageModifier_10);

	m_PlayerDamageModifier_10 = new Player_AIDamageModifier(0.1 , 150 , 50 , PLAYERDAMAGE_10_PERCENT , true);
	AddModifier(m_PlayerDamageModifier_10);

	m_ClipSizeModifier_120 = new ClipSizeModifier(0.1 , 150 , 50 , CLIPSIZE_10_PERCENT , true);
	AddModifier(m_ClipSizeModifier_120);

	//~~~~~~~~WeaponModifier~~~~~~~~//


	//--------VechileModifier--------//
	m_AccelerationModifier_50 = new AccelerationModifier(0.5,80,20,ACCELERATION_10_PERCENT,true);
	AddModifier(m_AccelerationModifier_50);

	m_TopSpeedModifier_30 = new TopSpeedModifier(0.3,70,30,TOPSPEED_10_PERCENT,true);
	AddModifier(m_TopSpeedModifier_30);

	m_ReverseSpeedModifier_20 = new ReverseSpeedModifier(0.2,50,30,REVERSPEED_10_PERCENT,true);
	AddModifier(m_ReverseSpeedModifier_20);

	m_DeccelerationModifier_10 = new DeccelerationModifier(0.1,50,30,DECCELETATION_10_PERCENT,true);
	AddModifier(m_DeccelerationModifier_10);

	m_BFrictionModifier_10 = new BackFrictionModifier(0.1,200,50,BACKFRICTION_10_PERCENT,true);
	AddModifier(m_BFrictionModifier_10);

	m_BFrictionModifier_v0 = new BackFrictionModifier(0,400,50,BACKFRICTION_TURNTO_0,false);
	AddModifier(m_BFrictionModifier_v0);

	m_FFrictionModifier_10 = new FrontFrictionModifier(0.1,200,50,FRONTFRICTION_10_PERCENT,true);
	AddModifier(m_FFrictionModifier_10);

	m_FFrictionModifier_v0 = new FrontFrictionModifier(0,400,50,FRONTFRICTION_TURNTO_0,false);
	AddModifier(m_BFrictionModifier_v0);

	m_SpeedModifier_v30 = new SpeedModifier(30,50,0,SPEED_TURNTO_30,false);
	AddModifier(m_SpeedModifier_v30);

	m_RotationModifier_v180 = new RotationModifier(180,50,0,ROTATION_TURNTO_180,false);
	AddModifier(m_RotationModifier_v180);

	//~~~~~~~~VechileModifier~~~~~~~~//


	//--------AdvModifier--------//
	m_LegDisableModifier_10s = new LegDisableModifier(10,50,LEG_DISABLE_10S);
	AddModifier(m_LegDisableModifier_10s);

	m_ArmDisableModifier_10s = new ArmDisableModifier(10,150,ARM_DISABLE_10S);
	AddModifier(m_ArmDisableModifier_10s);

	m_EyeDisableMoifier_10s = new EyeDisableModifier(10,100,EYE_DISABLE_10S);
	AddModifier(m_EyeDisableMoifier_10s);

	m_MindDisableModifier_10s = new MindDisableModifier(10,200,MIND_DISABLE_10S);
	AddModifier(m_MindDisableModifier_10s);

	m_MindChangeModifier_30s = new MindChangeModifier(30,250,MIND_CHANGE_10S);
	AddModifier(m_MindChangeModifier_30s);

	m_ArmWeakModifier_10s = new ArmWeakModifier(10,60,ARM_WEAK_10S);
	AddModifier(m_ArmWeakModifier_10s);

	m_LegWeakModifier_10s = new LegWeakModifier(10,30,LEG_WEAK_10S);
	AddModifier(m_LegWeakModifier_10s);

	//~~~~~~~~AdvModifier~~~~~~~~//

	//--------TrapModifier--------//
	m_TrapRadiusModifier_30 = new TrapRadiusModifier(0.3,200,TRAP_RADIUS_30,true);
	AddModifier(m_TrapRadiusModifier_30);

	m_TrapEffectModifier_50 = new TrapEffectModifier(0.5,300,TRAP_EFFECT_50,true);
	AddModifier(m_TrapEffectModifier_50);

	m_TrapEffectModifier_200 = new TrapEffectModifier(2,350,TRAP_EFFECT_200,true);
	AddModifier(m_TrapEffectModifier_200);

	m_TrapDataLeak_Enable = new TrapTypeModifier(1,200,TRAP_DATALEAK_ENABLE,eTT_DataLeak);
	AddModifier(m_TrapDataLeak_Enable);
	m_TrapDataLeak_Disable = new TrapTypeModifier(0,200,TRAP_DATALEAK_DISABLE,eTT_DataLeak);
	AddModifier(m_TrapDataLeak_Disable);

	m_TrapSlow_Enable = new TrapTypeModifier(1,200,TRAP_SLOW_ENABLE,eTT_Slow);
	AddModifier(m_TrapSlow_Enable);
	m_TrapSlow_Disable = new TrapTypeModifier(0,200,TRAP_SLOW_DISABLE,eTT_Slow);
	AddModifier(m_TrapSlow_Disable);

	m_TrapDedication_Enable = new TrapTypeModifier(1,200,TRAP_DEDICATIENABLE_ENABLE,eTT_Dedication);
	AddModifier(m_TrapDedication_Enable);
	m_TrapDedication_Disable = new TrapTypeModifier(0,200,TRAP_DEDICATIENABLE_DISABLE,eTT_Dedication);
	AddModifier(m_TrapDedication_Disable);

	m_TrapRecycle_Enable = new TrapTypeModifier(1,200,TRAP_RECYCLE_ENABLE,eTT_Recycle);
	AddModifier(m_TrapRecycle_Enable);
	m_TrapRecycle_Disable = new TrapTypeModifier(0,200,TRAP_RECYCLE_DISABLE,eTT_Recycle);
	AddModifier(m_TrapRecycle_Disable);

	m_TrapDamageCov_Enable = new TrapTypeModifier(1,200,TRAP_DAMAGECOV_ENABLE,eTT_DamageCov);
	AddModifier(m_TrapDamageCov_Enable);
	m_TrapDamageCov_Disable = new TrapTypeModifier(0,200,TRAP_DAMAGECOV_DISABLE,eTT_DamageCov);
	AddModifier(m_TrapDamageCov_Disable);
	//~~~~~~~~TrapModifier~~~~~~~~//



	//--------LeakHoleModifier--------//
	m_LeakHoleCD_30 = new LeakHoleCdModifier(0.3,400,LEAKHOLE_CD_30,true);
	AddModifier(m_LeakHoleCD_30);

	m_LeakHoleDamage_50 = new LeakHoleDamageModifier(0.5,200,LEAKHOLE_DAMAGE_50,true);
	AddModifier(m_LeakHoleDamage_50);

	m_LeakHoleLength_50 = new LeakHoleLengthModifier(0.5,100,LEAKHOLE_LENGTH_50,true);
	AddModifier(m_LeakHoleLength_50);

	//~~~~~~~~LeakHoleModifier~~~~~~~~//

	//--------ForceDevice--------//
	m_ForceDeviceHitLimits_200 = new ForceDeviceHitLimitsModifier(2,200,FORCEDEVICE_HITLIMITS_200,true);
	AddModifier(m_ForceDeviceHitLimits_200);

	m_ForceDeviceDamage_150 = new ForceDeviceDamageModifier(1.5,200,FORCEDEVICE_DAMAGE_150,true);
	AddModifier(m_ForceDeviceDamage_150);

	m_ForceDeviceRadius_150 = new ForceDeviceRadiusModifier(1.5,150,FORCEDEVICE_RADIUS_150,true);
	AddModifier(m_ForceDeviceRadius_150);

	m_ForceDeviceForce_200 = new ForceDeviceForceModifier(2,150,FORCEDEVICE_FORCE_200,true);
	AddModifier(m_ForceDeviceForce_200);
	//~~~~~~~~ForceDevice~~~~~~~~//

	//--------PowerBall--------//
	m_PowerBallDamage_200 = new PowerBallDamageModifier(2,200,POWERBALL_DAMAGE_200,true);
	AddModifier(m_PowerBallDamage_200);

	m_PowerBallRadius_50 = new PowerBallRadiusModifier(0.5,150,POWERBALL_RADIUS_50,true);
	AddModifier(m_PowerBallRadius_50);

	m_PowerBallCritical_200 = new PowerBallCriticalModifier(2,200,POWERBALL_CRITICAL_200,true);
	AddModifier(m_PowerBallCritical_200);

	//~~~~~~~~PowerBall~~~~~~~~//

	/*ForceFieldRadiusModifier *m_ForceFieldRadius_150;
	ForceFieldScaleModifier *m_ForceFieldScale_50;*/

	//--------ForceField--------//
	m_ForceFieldRadius_150 = new ForceFieldRadiusModifier(1.5,200,FORCEFIELD_RADIUS_150,true);
	AddModifier(m_ForceFieldRadius_150);

	m_ForceFieldScale_50 = new ForceFieldScaleModifier(0.5,200,FORCEFIELD_SCALE_50,true);
	AddModifier(m_ForceFieldScale_50);

	//~~~~~~~~ForceField~~~~~~~~//
}


ModifierManager::~ModifierManager()
{
	delete MRM;

	delete m_MassModifier_10;
	delete m_MassModifier_50;
	delete m_MassModifier_90;

	Modifiers.clear();
}


IModifier *ModifierManager::GetModifier(ModifyID Id)
{
	if(Modifiers.count(Id))
		return Modifiers[Id];
	return 0;
}


void ModifierManager::AddModifier(IModifier *Modifier)
{
	ModifyID Id=Modifier->GetModifierId();
	if(!Modifiers.count(Id))
		Modifiers[Id]=Modifier;
}


void ModifierManager::RemoveModifier(ModifyID Id)
{
	if(Modifiers.count(Id))
		Modifiers.erase(Id);
}