#include "Stdafx.h"
#include "SpawnedAIManager.h"
#include "Code_System/CodeObject/AISpawnPoint.h"

SpawnedAIManager::SpawnedAIManager()
{
};

SpawnedAIManager::~SpawnedAIManager()
{
	ManagedAI.clear();
}

AIBindParams* SpawnedAIManager::GetBindParams()
{
	if(!ManagedAI.empty())
		return &(*iter);
	else
		return 0;
}

bool SpawnedAIManager::AddToList(AIBindParams param)
{
	if(ManagedAI.empty())
	{
		ManagedAI.push_back(param);
		iter = ManagedAI.begin();
		return true;
	}

	ManagedAI.push_back(param);
	return true;
}

void SpawnedAIManager::AddSpawnerData(unsigned int _count , unsigned int _max ,const EntityId _Id)
{
	Spawner new_Spawner;
	new_Spawner.Enable = true;
	new_Spawner.SpawnCount = _count;
	new_Spawner.MaxSpawn = _max;
	if(Spawners.empty())
	{
		Spawners[_Id] = new_Spawner;
		Spawner_iter = Spawners.begin();
	}
	else
		Spawners[_Id] = new_Spawner;
}

void SpawnedAIManager::RemoveSpawner(EntityId _Id)
{
	if(Spawners.count(_Id))
	{
		Spawners.erase(_Id);
		Spawner_iter = Spawners.begin();
	}
}

const Spawner *SpawnedAIManager::GetSpawnerData(EntityId _Id)
{
	if(Spawners.count(_Id))
	{
		return &Spawners[_Id];
	}
	return 0;
}

bool SpawnedAIManager::removeCurData()
{
	if(ManagedAI.empty())
		return false;
	ManagedAI.remove(*iter);
	return true;
}

void SpawnedAIManager::Update()
{
/*	if(!ManagedAI.empty())
	{
		std::list<AIBindParams>::iterator end = ManagedAI.end();
		if(iter != end)
		{
			IActor *pActor = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(iter->AI_Id);
			if(pActor)
			{
				if(pActor->IsDead())
				{
					std::list<AIBindParams>::iterator before = iter;
					Spawner &the_spawner = Spawners[iter->SpawnerId];
					if(++the_spawner.SpawnCount >= the_spawner.MaxSpawn)
					{
						CAISpawnPoint* pAISpawnPoint = static_cast<CAISpawnPoint*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(iter->SpawnerId, "AISpawnPoint"));
						if(pAISpawnPoint)
							pAISpawnPoint->SendClearMessage();
					}
					if(--(Spawners[iter->SpawnerId].SpawnCount) < 0)
						Spawners[iter->SpawnerId].SpawnCount = 0;
					++iter;
					ManagedAI.remove(*before);
				}
				else
					++iter;
			}
		}
		if(iter == end)
			iter = ManagedAI.begin();
	}

	if(!Spawners.empty())
	{
		std::map<EntityId,Spawner>::iterator end = Spawners.end();
		if(Spawner_iter != end)
		{
			Spawner &_spawner = Spawner_iter->second;
			IEntity *pEntity = gEnv->pEntitySystem->GetEntity(Spawner_iter->first);
			Vec3 SpawnerPos = pEntity->GetPos();
			Vec3 PlayerPos = gEnv->pEntitySystem->GetEntity(g_pGame->GetClientActorId())->GetPos();
			float fDistance = (SpawnerPos - PlayerPos).GetLengthSquared();
			bool IsFarEnough = fDistance > 250;
			bool isCloseEnough = fDistance <= 900;
			if(_spawner.SpawnCount < _spawner.MaxSpawn && IsFarEnough && isCloseEnough)
				_spawner.Enable = true;
			else
				_spawner.Enable = false;
			++Spawner_iter;
		}
		if(Spawner_iter == end)
			Spawner_iter = Spawners.begin();
	}

	//Clear spawned AI data in editor
	if(g_pGame->GetIGameFramework()->IsEditing())
		g_pGame->GetSpawnedAIManager()->ClearList();*/
}

void SpawnedAIManager::ClearList()
{
	if(!ManagedAI.empty())
	{
		ManagedAI.clear();
		if(!Spawners.empty())
		{
			std::map<EntityId,Spawner>::iterator iter = Spawners.begin();
			std::map<EntityId,Spawner>::iterator end = Spawners.end();
			while(iter != end)
			{
				iter->second.SpawnCount = 0;
				++iter;
			}
		}
	}
}

const bool SpawnedAIManager::CheckSpawner(EntityId _Id)
{
	if(Spawners.count(_Id))
	{
		return Spawners[_Id].Enable;
	}
	return false;
}

void SpawnedAIManager::AddSpawnCount(EntityId _Id)
{
	if(Spawners.count(_Id))
	{
		++Spawners[_Id].SpawnCount;
	}
}

void SpawnedAIManager::ResetSpawnCount()
{
	std::map<EntityId,Spawner>::iterator iter = Spawners.begin();
	std::map<EntityId,Spawner>::iterator end = Spawners.end();
	while(iter != end)
	{
		Spawner &the_spawner = iter->second;
		if(the_spawner.SpawnCount > 0)
		{
			the_spawner.SpawnCount = 0;
			if(the_spawner.SpawnCount == the_spawner.MaxSpawn)
				the_spawner.Enable = true;
		}
		++iter;
	}
}