#ifndef _SPAWNSYSTEM_H_
#define _SPAWNSYSTEM_H_
#pragma once

#include "Code_System/WorldStability.h"
#include "Code_System/EntityParamsUtils.h"


typedef std::vector<Vec3> tempVec;
class SpawnSystem
{
private:
	enum Direction
	{
		eD_Nothing= 0,
		eD_Left,
		eD_Right,
		eD_Forward,
		eD_Backward,
		eD_Up,
		eD_Down,
	};
	int hits;
	IPhysicalWorld *pWorld;
	IRenderer *pRenderer;

private:
	EntityClassPreLoad preLoad;
	//--------CollisionCheck--------//
	IEntity *pEntity;
	AABB aabb;
	AABB localaabb;
	AABB spawnaabb;
	AABB spawn_local_aabb;
	//--------Edit Params--------//
	Vec3 last_dir;
	Vec3 dir;
	ray_hit hit;
	int query_flag;
	//--------Function State Check--------//
	bool StartFunction;    //If SpawnSystem has started
	bool edit;    //If is Edit Mode
	bool locked;
	bool zChange;
	bool needXMLUpdate;
	bool RayCastEnabled;
	bool AligntoEntity;
	int mode;


	//Slot Spawn
	int SlotIndex;	//The xml Index for QuickSpawn

	//--------XML--------//
	XmlNodeRef root;
	int index;
	XmlNodeRef EntityNode;
	XmlNodeRef Params;
	//--------Align State--------//
	Vec3 left_dir;
	Vec3 right_dir;
	Vec3 forward_dir;
	Vec3 back_dir;
	Vec3 hit_dir;

	short placedir;
	short last_placedir;

	Vec3 offset_left;
	Vec3 offset_right;
	Vec3 offset_forward;
	Vec3 offset_back;
	Vec3 offset_up;
	Vec3 offset_down;

	float manual_offset;

	EntityId SpawnId;
	EntityId SpawnId_min;
	//--------For SystemRobot--------//
	std::list<EntityId> SpawnedEntityIdSet;

	float rotationReset_timer;
	//--------SpawnEntity Params--------//
	struct Spawns
	{
		IEntity *SpawnEntity;
		IEntityClass *pEntityClass;
		unsigned int XMLIndex;
		EntityParamsUtils::Utils Utils;
		XmlString model;    // Entity Model
		XmlString name;
		XmlString ArchetypeName;
		IMaterial *defaultmat;   // Default material of this entity
		IMaterial *spawnmat;    // Spawn mat for this entity
		IMaterial *lmat;    // Lock Material
		float timer;    // Spawn Timer
		float rate;    // Count Rate
		float spawntime;    // Spawn Time
		float amount;  // Use to control glow amount
		float progress;    // Spawning progress of this entity
		Spawns()
			: SpawnEntity(NULL)
			, pEntityClass(NULL)
			, XMLIndex(0)
			, model("")
			, ArchetypeName("")
			, lmat(NULL)
			, defaultmat(NULL)
			, spawnmat(NULL)
			, timer(0)
			, rate(0)
			, spawntime(0)
			, amount(50)
			, progress(0){}

		Spawns(const Spawns &rhs)
		{
			SpawnEntity = rhs.SpawnEntity;
			pEntityClass = rhs.pEntityClass;
			XMLIndex = rhs.XMLIndex;
			model = rhs.model;
			lmat = rhs.lmat;
			defaultmat = rhs.defaultmat;
			spawnmat = rhs.spawnmat;
			timer = rhs.timer;
			rate = rhs.rate;
			spawntime = rhs.spawntime;
			amount = rhs.amount;
			progress = rhs.progress;
			Utils = rhs.Utils;
		}
		Spawns &operator=(const Spawns &rhs)
		{
			if(this == &rhs)
				return *this;
			SpawnEntity = rhs.SpawnEntity;
			pEntityClass = rhs.pEntityClass;
			XMLIndex = rhs.XMLIndex;
			model = rhs.model;
			lmat = rhs.lmat;
			defaultmat = rhs.defaultmat;
			spawnmat = rhs.spawnmat;
			timer = rhs.timer;
			rate = rhs.rate;
			spawntime = rhs.spawntime;
			amount = rhs.amount;
			progress = rhs.progress;
			Utils = rhs.Utils;
			return *this;
		}
	};
	IMaterial *defspawnmat;
	IMaterial *lockedmat;
	Quat LastQuat;

	typedef std::map<EntityId , Spawns> ProcessMap;
	typedef EntityParamsUtils::BasicPhysParams BasicPhysParams;
	typedef EntityParamsUtils::ExplosiveParams ExplosiveParams;
	typedef EntityParamsUtils::LightParams LightParams;
	typedef EntityParamsUtils::AIParams AIParams;
	typedef EntityParamsUtils::TrapParams TrapParams;
	typedef EntityParamsUtils::LeakHoleParams LeakHoleParams;
	typedef EntityParamsUtils::ForceDeviceParams ForceDeviceParams;
	typedef EntityParamsUtils::PowerBallParams PowerBallParams;
	typedef EntityParamsUtils::ForceFieldParams ForceFieldParams;

	void editMode(float frameTime);    // EditMode
	//--------Process XML File--------//
	void SpawnBasicEntityandItem(const ray_hit &hit,SEntitySpawnParams &params);
	void SpawnVehicle(const ray_hit &hit,SEntitySpawnParams& params);
	void SpawnAI(const ray_hit &hit,SEntitySpawnParams& params);
	void SpawnLight(const ray_hit &hit,SEntitySpawnParams& params);
	void SpawnDestroyableObject(const ray_hit &hit,SEntitySpawnParams& params);
	void SpawnLadder(const ray_hit &hit,SEntitySpawnParams& params);
	void SpawnTrap(const ray_hit &hit,SEntitySpawnParams& params);
	void SpawnLeakHole(const ray_hit &hit,SEntitySpawnParams& params);
	void SpawnForceDevice(const ray_hit &hit,SEntitySpawnParams& params);
	void SpawnPowerBall(const ray_hit &hit,SEntitySpawnParams& params);
	void SpawnForceField(const ray_hit &hit,SEntitySpawnParams& params);

	void SpawnTheEntity(const ray_hit &hit,SEntitySpawnParams& params, int spawnflag);

	void ProcessBasicEntity(BasicPhysParams &BPP , const int SIndex = -1);
	void ProcessItem(BasicPhysParams &BPP , const int SIndex = -1);
	void ProcessVehicle(const int SIndex = -1);
	void ProcessAI(AIParams &AP , const int SIndex = -1);
	void ProcessLight(BasicPhysParams &BPP , LightParams &LP , ExplosiveParams &EP , const int SIndex = -1);
	void ProcessDestroyableObject(BasicPhysParams & BPP , ExplosiveParams &EP , const int SIndex = -1);
	void ProcessLadder(float &height , const int SIndex  = -1 );
	void ProcessTrap(BasicPhysParams & BPP , ExplosiveParams &EP , TrapParams &TP, AIParams &AP, const int SIndex = -1);
	void ProcessLeakHole(BasicPhysParams & BPP , LeakHoleParams &LHP, const int SIndex = -1);
	void ProcessForceDevice(BasicPhysParams & BPP , ForceDeviceParams &FDP, const int SIndex = -1);
	void ProcessPowerBall(BasicPhysParams & BPP , PowerBallParams &PBP, const int SIndex = -1);
	void ProcessForceField(BasicPhysParams & BPP , ForceFieldParams &DP, const int SIndex = -1);






	void PostSpawnPhysics(Spawns &SP , const BasicPhysParams & BPP);
	void PostSpawnVehiclePhysics(Spawns & SP);
	void PostSpawnAIPhysics(Spawns &SP , const AIParams &AP);
	void PostSpawnLight(Spawns &SP , const BasicPhysParams &BPP , const LightParams &LP , const ExplosiveParams &EP);
	void PostSpawnDestroyableObject(Spawns &SP , const BasicPhysParams & BPP , const ExplosiveParams &EP);
	void PostSpawnLadder (Spawns &SP , float height);
	void PostSpawnTrap(Spawns &SP, const BasicPhysParams & BPP , const ExplosiveParams &EP , const TrapParams &TP, const AIParams &AP);
	void PostSpawnLeakHole(Spawns &SP , const LeakHoleParams &LHP);
	void PostSpawnForceDevice(Spawns &SP,const BasicPhysParams & BPP, const ForceDeviceParams &FDP);
	void PostSpawnPowerBall(Spawns &SP,const BasicPhysParams & BPP, const PowerBallParams &PBP);
	void PostSpawnForceField(Spawns &SP,const ForceFieldParams &DP);

	void ProcessSpawnMat();
	void GetMat();
	const short GetNearestDir(Vec3 &pos);

	void GetEntityDir(IEntity *pEntity);
	void CalculateOffset(IEntity *pEntity, AABB &localAABB);
	//--------Array Check--------//
	int curindex;
	int lastindex;
	int Maxnumber;
	ProcessMap SpawnProcessMap;
	Spawns CurrentSpawn;
	Spawns LastSpawn;
	ProcessMap::iterator iter;
	//--------Function--------//
	public:
		SpawnSystem();
		~SpawnSystem(){};
		void Update(float frametime);

		// Description :
		//		Spawn an entity need some time...
		void PostSpawned(float &frametime);

		// Description :
		//		Shutdown the SpawnSystem.Reset everything.
		void Shutdown();

		// Description :
		//		Spawn an entity based on the entity class.
		//	Arguments : 
		//		SIndex - Slot index in Normal mode.(SIndex == -1 means the slot is empty,therefore nothing will spawn).
		void Spawn(const int & SIndex = -1);

		// Description :
		//		Clear SP[i]'s data.
		//	Arguments : 
		//		i - index of SP member.
		void ClearSpawnData(Spawns &SP);

		// Description :
		//		Reinit some members of SpawnSystem
		void ReInit();

		void CreateSpawnParticle(int index);

		void AddtoSpawnEntitySet(EntityId Id){SpawnedEntityIdSet.push_back(Id);}
		std::list<EntityId> &GetSpawnEntitySet(){return SpawnedEntityIdSet;}

		void RemoveFromSpawnEntitySet(EntityId Id);
		//Inline
		inline void SetStartFunction(bool enable){StartFunction=enable;}
		inline void Setedit(bool enable){edit=enable;}
		inline void Setlocked(bool enable){locked=enable;}
		inline void SetZchange(bool enable){zChange=enable;}
		inline void SetFlag(){if(query_flag == ent_terrain) query_flag = ent_all;else query_flag=ent_terrain;}
		inline void SetLastIndex(){LastSpawn = CurrentSpawn;}
		inline void SetXMLindex(const int xindex){index=xindex;}
		inline void SetXMLUpdate(const bool enable){needXMLUpdate=enable;}
		inline void EnableRayCast(bool enable){RayCastEnabled = enable;}
		inline void EnableAlign(bool enable){AligntoEntity=enable;}
		inline void change_offset(float num){manual_offset+=num;}
		inline void set_offset(float num){manual_offset=num;}
		inline void SetLastQuat(const Quat rot){LastQuat=rot;}
		inline const int Getindex(){return index;}
		inline const int GetMode(){return mode;}
		inline const int GetSlotIndex(){return SlotIndex;}
		inline const bool IsZchange(){return zChange;}
		inline const bool IsFunctionStarted(){return StartFunction;}
		inline const bool Iseditmodeon(){return edit;}
		inline const bool IsXMLUpdate(){return needXMLUpdate;}
		inline const bool IsRayCastEnabled(){return RayCastEnabled;}
		inline const bool IsAlignEnabled(){return AligntoEntity;}
		inline const bool IsSlotAvailable(){return SlotIndex != -1;}
		inline const float Getoffset(){return manual_offset;}
		inline void SetXMLRoot(XmlNodeRef new_root){root = new_root;}
		inline const XmlNodeRef GetXMLRoot(){return root;}
		inline void FindEntityNode(int child_index){EntityNode = root->getChild(child_index);}
		inline const XmlNodeRef GetEntityNode(){return EntityNode;}
		inline void FindEntityParams(){Params = EntityNode->getChild(0);}
		inline const XmlNodeRef GetEntityParams(){return Params;}
		inline void ResetRotationtimer(){rotationReset_timer = 0;}

		void PreProcessModelAndName(int SIndex = -1);
		Spawns &GetCurSpawn(){return CurrentSpawn;}
		void AddtoSpawnProcessMap(const Spawns &SP);

		void SetQuickSpawnIndex(int index){SlotIndex = index;};
};
#endif