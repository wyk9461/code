#include "StdAfx.h"
#include "Code_System/WorldStability.h"
#include "Code_System/EntityDataBase.h"

WorldStability::WorldStability()
{
	Stability=10000;
	timer=0;
	rate=2;
	gaintime=1.5f;
	pElement = NULL;
	maxSTA = 20000;
}

void WorldStability::Update(float frametime)
{
	timer+=frametime;
	if(timer>= gaintime)
	{
		Stability+=rate;
		timer=0;
	}
	UpdateUIInfo();

	//EntityDataBase::Instance().SetStability(stability);
}

void WorldStability::UpdateUIInfo()
{
	if(!pElement)
	{
		pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	}
	if(pElement)
	{
		SUIArguments args;
		args.AddArgument(Stability);
		args.AddArgument(maxSTA);
		args.AddArgument(rate);
		pElement->CallFunction("SetSTA",args);
	}
}