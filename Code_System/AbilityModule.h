#ifndef _ABILITYMODULE_H_
#define _ABILITYMODULE_H_

#include "ScreenEffects.h"
#include "Code_System/BasicRayCast.h"
#include "IFlashUI.h"

#include "Code_System/AbilityModule/BaseAbility.h"
#include "Code_System/AbilityModule/TeleportModule.h"
#include "Code_System/AbilityModule/ShockWaveModule.h"
#include "Code_System/AbilityModule/BulletTimeModule.h"
#include "Code_System/AbilityModule/KineticShieldModule.h"
#include "Code_System/AbilityModule/ReflectionShieldModule.h"
#include "Code_System/AbilityModule/ExplodeShieldModule.h"
#include "Code_System/AbilityModule/DataBallModule.h"
#include "Code_System/AbilityModule/HealModule.h"
#include "Code_System/AbilityModule/BackupModule.h"
#include "Code_System/AbilityModule/RushModule.h"
#include "Code_System/AbilityModule/RushModule.h"
#include "Code_System/AbilityModule/NuclearRadModule.h"
#include "Code_System/AbilityModule/FloodModule.h"
#include "Code_System/AbilityModule/DataBiteModule.h"
#include "Code_System/AbilityModule/GrabModule.h"
#include "Code_System/AbilityModule/ExplosionModule.h"
#include "Code_System/AbilityModule/GravityPoolModule.h"
#include "Code_System/AbilityModule/Reposition.h"
#include "Code_System/AbilityModule/DataVoid.h"
#include "Code_System/AbilityModule/DataGuard.h"
#include "Code_System/AbilityModule/BaltModule.h"
#include "Code_System/AbilityModule/BulletStrom.h"
#include "Code_System/AbilityModule/BlinkStrike.h"
#include "Code_System/AbilityModule/DataArrow.h"


class AbilityModule
{
	private:
		std::vector<BaseAbility*> InstalledModule;
		std::vector<BaseAbility*> TotalModule;
		Process_Data pData;
		int max_index;
		bool StartFunction;
		bool ShowChangeText;
		bool ModuleSelected[3];
		float text_timer;
		std::vector<BaseAbility*>::iterator it;
		ray_hit hit;
		int hits;
		int query_flag;
		IPhysicalWorld *pWorld;
		EntityId store_weapon_id;
		BasicRayCast *pBRC;
		IUIElement *pElement;

		//Module
		TeleportModule *m_Teleport;
		ShockWaveModule *m_ShockWave;
		BulletTimeModule *m_BulletTime;
		KineticShieldModule *m_Shield;
		ReflectionShieldModule *m_Reflection;
		ExplodeShieldModule *m_ExploShield;
		DataBallModule *m_DataBall;
		HealModule *m_Heal;
		BackupModule *m_Backup;
		RushModule *m_Rush;
		NuclearRadModule *m_NuclearRad;
		FloodModule *m_Flood;
		DataBiteModule *m_DataBite;
		GrabModule *m_Grab;
		ExplosionModule *m_Explosion;
		GravityPoolModule *m_GravityPool;
		RepositionModule *m_Reposition;
		DataVoidModule *m_DataVoid;
		DataGuardModule *m_DataGuard;
		BaltModule *m_Balt;
		BulletStromModule *m_BulletStrom;
		BlinkStrikeModule *m_BlinkStrike;
		DataArrow *m_DataArrow;
		
		//~Module

		IRenderer *pRenderer;
		float color[ 4 ];

		void DisplayModule();
		void AfterFinished(float frameTime);
		const bool HasShield();

		void PerProcessAllModule(Process_Data &_rdata);
		void ProcessAllModule(Process_Data &_rdata);
		void PostProcessAllModule(Process_Data &_rdata);

	public:
		/*std::vector<int> InstlledModule;
		std::map<int,Timer_Cd*> ModuleTimer;
		std::map<int,string*> ModuleName;*/
		AbilityModule();
		~AbilityModule();
		void Update(float frameTime);

		//Shield Inline
		inline const bool ShieldHasSet(){return m_Shield->isSet();}
		inline void CallKineticShieldDamage(HitInfo &info){m_Shield->ShieldDamage(info);}
		//Reflection Inline
		inline const bool ReflectionHasSet(){return m_Reflection->isSet();}
		//ExploShield Inline
		inline const bool ExploShieldHasSet(){return m_ExploShield->isSet();}
		inline void CallExploShieldDamage(HitInfo &info){m_ExploShield->ExploShieldDamage(info);}
		//BackUp Inline
		inline const bool BackUpHasSet(){return m_Backup->isSet();}
		//DataBite Inline
		inline const bool IsBitePreProcessed(){return m_DataBite->isSet();}
		inline const bool IsBiteLaunched(){return m_DataBite->isSet();}
		inline void setBite(){m_DataBite->setHasHit();}
		inline void SetBiteTarget(CActor *target){m_DataBite->setHitTarget(target);}

		//Main Function Inline
		inline void SetStartFunction(bool enable){StartFunction = enable;}
		inline void SetTextChanged(bool enable){ShowChangeText=enable;}
		inline void SetModule(int i,bool enable){ModuleSelected[i]=enable;}
		inline void Setindex(const int i){it=InstalledModule.begin()+i;}
		inline void AddIndex(){++it;}
		inline void MinuesIndex(){--it;}
		inline void ZeroIndex(){it=InstalledModule.begin();}
		inline void ZeroTextTimer(){text_timer=0;}
		inline const bool IsModuleSelected(int i){return ModuleSelected[i];}
		inline std::vector<BaseAbility*>::iterator GetIndex(){return it;}

		BaseAbility *GetSlot(const int index)
		{
			if(InstalledModule[index])
				return InstalledModule[index];
			return 0;
		}
};

#endif
