#ifndef _WORLDSTABILITY_H_
#define _WORLDSTABILITY_H_
#pragma once

#include "IRenderer.h"
#include "IFlashUI.h"



class WorldStability
{
public:
	WorldStability();
	~WorldStability(){}
	void Update(float frametime);
	inline const int Getstability() const {return Stability;}
	inline void StabilityCost(const int cost){Stability -= cost;}
	inline void StabilityGain(const int gain){Stability += gain;}
	inline void SetMaxStability(int MSTA){maxSTA = MSTA;}

private:
	int Stability;
	int maxSTA;
	int rate;
	float timer;
	float gaintime;
	IUIElement *pElement;

	void UpdateUIInfo();
};


#endif