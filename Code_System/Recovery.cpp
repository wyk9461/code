#include "StdAfx.h"
#include "Code_System/Recovery.h"

void CRecovery::Update(float frameTime)
{
	if(!StabilityRecovery_Queue.empty())
	{
		StabiityRecovery_Timer += frameTime;
		if(StabiityRecovery_Timer > StabilityRecovery_Queue.front().delay)
		{
			g_pGame->GetWorldStability()->StabilityGain(StabilityRecovery_Queue.front().amount);
			StabilityRecovery_Queue.pop();
			StabiityRecovery_Timer = 0;
		}
	}

	if(!IsPlayerHealthChanged())
	{
		CActor *player=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
		HealthRecovery_Timer += frameTime;
		if(HealthRecovery_Timer > HealthRecovery_Delay && player->GetHealth() < player->GetMaxHealth())
		{
			if(player)
			{
				player->SetHealth(player->GetHealth() + HealthRecovery_Amount*frameTime);
				prevHealth=player->GetHealth();
			}
		}
	}
	else
		HealthRecovery_Timer = 0;
}

const bool CRecovery::IsPlayerHealthChanged()
{
	CActor *pPlayer=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
	if(pPlayer)
	{
		curHealth = pPlayer->GetHealth();
		if(prevHealth == curHealth)
			return false;
		else
		{
			prevHealth = curHealth;
			return true;
		}
	}
	return true;
}