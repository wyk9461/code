#include "StdAfx.h"
#include "Trap.h"
#include "IAIActor.h"
#include "Code_System/Perk.h"

IParticleEffect *CTrap::pLeakEffect = NULL;
IParticleEffect *CTrap::pDamageCovEffect = NULL;
HitInfo CTrap::covHit = HitInfo();
bool CTrap::HasProcessedHit = false;


CTrap::CTrap()
: totalDamage(0)
, movementDamage(2)
, Radius(5)
, slowDownScale(1)
, dedicationDamageScale(0.1)
, treatment(100)
, stabilityLost(1)
, stabilityGain(100)
, lostTime(2)
, timer(0)
, EnemyId(0)
, damageScale(0)
, isDataLeakage(false)
, isSlowDown(false)
, isDedication(false)
, isRecycle(false)
, isDamageCov(false)
{
	const IFactionMap& factionMap = gEnv->pAISystem->GetFactionMap();
	gruntFactionID = factionMap.GetFactionID("Grunts");
	playerFactionID = factionMap.GetFactionID("Players");
	civilianFactionID = factionMap.GetFactionID("Civilians");
}

CTrap::~CTrap(){}

namespace STrap
{
	void RegisterEvents(IGameObjectExtension &goExt, IGameObject &gameObject)
	{
		const int events[] =
		{
			eGFE_ScriptEvent,
			eEvent_TrapEvent,
		};
		gameObject.UnRegisterExtForEvents(&goExt, NULL, 0);
		gameObject.RegisterExtForEvents(&goExt, events, (sizeof(events) / sizeof(int)));
	}
}

bool CTrap::Init(IGameObject  *pGameObject)
{
	pEffect = NULL;
	angle = 0;
	pLeakEffect = NULL;
	SetGameObject(pGameObject);
	GetGameObject()->EnablePhysicsEvent(true, eEPE_AllLogged);
	return true;
}

void CTrap::PostInit(IGameObject *pGameObject)
{
	STrap::RegisterEvents(*this, *pGameObject);
	GetGameObject()->EnableUpdateSlot(this, 0);
	GetGameObject()->SetUpdateSlotEnableCondition(this, 0, eUEC_Always);

	ReadScript();
	IEntity *thisEntity = GetEntity();
	AABB aabb;
	thisEntity->GetLocalBounds(aabb);
	IEntityAreaProxyPtr proxy = crycomponent_cast<IEntityAreaProxyPtr>(GetEntity()->CreateProxy(ENTITY_PROXY_AREA));
	if (proxy)
	{
		proxy->SetID(GetEntityId());
		proxy->SetSphere(aabb.GetCenter(), Radius);
		proxy->AddEntity(GetEntityId());
	}
}

bool CTrap::ReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params)
{
	ResetGameObject();
	STrap::RegisterEvents(*this, *pGameObject);
	ReadScript();
	return true;
}

void CTrap::Release()
{
	Disable();
}

void CTrap::Disable()
{
	std::map<EntityId, HealthAndDie>::iterator it_begin = ActorsWithinArea.begin();
	std::map<EntityId, HealthAndDie>::iterator it_end = ActorsWithinArea.end();
	EntityId playerId = g_pGame->GetClientActorId();

	while(it_begin != it_end)
	{
		EntityId Id = it_begin->first;
		CActor* pActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(Id);
		if(pActor)
		{
			if (pActor && (Id != playerId))
			{
				IAIActor *pAIActor = CastToIAIActorSafe(pActor->GetEntity()->GetAI());
				if (pAIActor)
					pAIActor->SetSpeed(1);
			}
			else if (pActor && Id == playerId)
			{
				pActor->SetSpeedMultipler(SActorParams::eSMR_Internal, PerkManager::Instance().GetBonusMoveSpeed());
				pActor->SetSpeedMultipler(SActorParams::eSMR_Item, PerkManager::Instance().GetBonusMoveSpeed());
				pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules, PerkManager::Instance().GetBonusMoveSpeed());
			}
		}
		ActorsWithinArea.erase(it_begin++);
	}
}
void CTrap::Update(SEntityUpdateContext &ctx, int slot)
{
	if(!g_pGame->GetIGameFramework()->IsGamePaused() && !g_pGame->GetIGameFramework()->IsEditing())
	{
		/*if(!pEffect)
			SpawnIndicateParticle();
		else
		{
			angle += 15;
			Vec3 newPos = CommonUsage::MoveAroundPoint(pEffect,GetEntity()->GetPos(),Radius,angle,0);
			pEffect->SetPos(newPos);
		}*/
		if (!ActorsWithinArea.empty() && GetEntity()->IsActive())
		{
			if (isSlowDown)
				SlowDown();
			if (isDataLeakage)
				DataLeakage();
			if (isDamageCov)
				DamageCov();
			if (isDedication)
				Dedication();
			if (isRecycle)
				Recycle(ctx.fFrameTime);
			//==================================================
			//check whether the enemy is dead or not
			//==================================================
			for (std::map<EntityId, HealthAndDie>::iterator check_die_iterator = ActorsWithinArea.begin(); check_die_iterator != ActorsWithinArea.end(); ++check_die_iterator)
			{
				CActor* pPerson = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(check_die_iterator->first);
				if (pPerson && (pPerson->GetHealth() <= 0))
				{
					check_die_iterator->second.isDead = true;
				}
			}

		}
	}
	if(g_pGame->GetIGameFramework()->IsEditing())
	{
		if(pEffect)
		{
			pEffect = NULL;
		}
	}
}

void CTrap::HandleEvent(const SGameObjectEvent &gameObjectEvent)
{
	const uint32 eventId = gameObjectEvent.event;
	void* pParam = gameObjectEvent.param;
	if ((eventId == eGFE_ScriptEvent) && (pParam != NULL))
	{
		const char* eventName = static_cast<const char*>(pParam);
		if (strcmp(eventName, "OnDestroy") == 0)
		{
			Disable();
		}
	}
	else if ((eventId == eEvent_TrapEvent) && (pParam != NULL))
	{

	}

}

void CTrap::ProcessEvent(SEntityEvent &entityEvent)
{
	switch (entityEvent.event)
	{
	case ENTITY_EVENT_COLLISION:
	{
		break;
	}
	case ENTITY_EVENT_ENTERAREA:
	{
		EntityId Id = static_cast<EntityId>(entityEvent.nParam[0]);
		IActor* pActor = (IActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(Id);
		if (pActor)
		{
			//float h = pActor->GetHealth();
			HealthAndDie healthAndDie(pActor->GetHealth(), false);
			std::map<EntityId, HealthAndDie>::value_type p(Id, healthAndDie);

			ActorsWithinArea.insert(p);
		}
		break;
	}
	case ENTITY_EVENT_LEAVEAREA:
	{
		EntityId Id = static_cast<EntityId>(entityEvent.nParam[0]);
		EntityId playerId = g_pGame->GetClientActorId();
		CActor* pActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(Id);
		if (pActor && (Id != playerId))
		{
			IAIActor *pAIActor = CastToIAIActorSafe(pActor->GetEntity()->GetAI());
			if (pAIActor)
				pAIActor->SetSpeed(1);
		}
		else if (pActor && Id == playerId)
		{
			pActor->SetSpeedMultipler(SActorParams::eSMR_Internal, 1);
			pActor->SetSpeedMultipler(SActorParams::eSMR_Item, 1);
			pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules, 1);
		}
		std::map<EntityId, HealthAndDie>::iterator it_begin = ActorsWithinArea.begin();
		std::map<EntityId, HealthAndDie>::iterator it_end = ActorsWithinArea.end();
		while (it_begin != it_end)
		{
			if (it_begin->first == Id)
			{
				ActorsWithinArea.erase(it_begin++);
			}
			else
				++it_begin;
		}
		break;
								   
	}
	case ENTITY_EVENT_MOVEINSIDEAREA:
	{
		EntityId Id = static_cast<EntityId>(entityEvent.nParam[0]);
		if(!ActorsWithinArea.count(Id))
		{
			IActor* pActor = (IActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(Id);
			if (pActor)
			{
				//float h = pActor->GetHealth();
				HealthAndDie healthAndDie(pActor->GetHealth(), false);
				std::map<EntityId, HealthAndDie>::value_type p(Id, healthAndDie);

				ActorsWithinArea.insert(p);
			}
		}
	}
	default:
		break;
	}
}

void CTrap::GetMemoryUsage(ICrySizer *pSizer) const
{

}

void CTrap::FullSerialize(TSerialize ser)
{

}

bool CTrap::GetEntityPoolSignature(TSerialize  signature)
{
	return true;
}

void CTrap::ReadScript()
{
	SmartScriptTable root = GetEntity()->GetScriptTable();
	SmartScriptTable props, fp;

	root->GetValue("Properties", props);

	props->GetValue("esFaction", faction);
	props->GetValue("FunctionParams", fp);

	fp->GetValue("isDataLeakage", isDataLeakage);
	fp->GetValue("isSlowDown", isSlowDown);
	fp->GetValue("isDedication", isDedication);
	fp->GetValue("isRecycle", isRecycle);
	fp->GetValue("isDamageCov", isDamageCov);


	float effect_scale;
	fp->GetValue("EffectScale", effect_scale);
	fp->GetValue("Radius", Radius);

	slowDownScale = static_cast<float>(1/pow(1.15, effect_scale));
	if(slowDownScale < 0.4)
		slowDownScale = 0.4;

	damageScale = static_cast<float>(1 - 1 / pow(1.15 , effect_scale));

	movementDamage = static_cast<float>(3 - (21/(effect_scale+7)));

	lostTime = static_cast<float>(0.5 + (1 / pow(1.13,effect_scale - 7)));
	stabilityLost = static_cast<float>(5 * sqrt(effect_scale));
	if(effect_scale > 0)
		stabilityGain = static_cast<float>(100 - pow((1/1.07),effect_scale -67));
	else
		stabilityGain = 0;

	dedicationDamageScale = static_cast<float>(1.5 - 1 / pow(1.15 , effect_scale-2.7));

	if(effect_scale > 0)
		treatment = static_cast<float>(200 - pow((1/1.07),effect_scale -74.8));
	else
		treatment = 0;

	/*fp->GetValue("slowDownScale", slowDownScale);

	fp->GetValue("lostTime", lostTime);
	fp->GetValue("stabilityLost", stabilityLost);
	fp->GetValue("stabilityGain", stabilityGain);

	fp->GetValue("movementDamage", movementDamage);
	fp->GetValue("Radius", Radius);

	fp->GetValue("damageScale", dedicationDamageScale);
	fp->GetValue("treatment", treatment);
	
	fp->GetValue("damageScale", damageScale);*/
}

void CTrap::SlowDown()
{
	CActor *pActor;
	EntityId id = g_pGame->GetClientActorId();
	std::map<EntityId, HealthAndDie>::iterator it_begin = ActorsWithinArea.begin();
	std::map<EntityId, HealthAndDie>::iterator it_end = ActorsWithinArea.end();
	while (it_begin != it_end)
	{
		pActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(it_begin->first);
		if (pActor && id != (it_begin->first))
		{
			const IAIObject* pAIObject = pActor->GetEntity()->GetAI();
			const IFactionMap& factionMap = gEnv->pAISystem->GetFactionMap();
			if (pAIObject->GetFactionID() != factionMap.GetFactionID(faction))
			{
				IAIActor *pAIActor = CastToIAIActorSafe(pActor->GetEntity()->GetAI());
				if (pAIActor)
					pAIActor->SetSpeed(slowDownScale);
			}
		}
		else if (pActor)
		{
			const IAIObject* pAIObject = pActor->GetEntity()->GetAI();
			const IFactionMap& factionMap = gEnv->pAISystem->GetFactionMap();
			if (pAIObject->GetFactionID() != factionMap.GetFactionID(faction))
			{
				const IAIObject* pAIObject = pActor->GetEntity()->GetAI();
				pActor->SetSpeedMultipler(SActorParams::eSMR_Internal, slowDownScale);
				pActor->SetSpeedMultipler(SActorParams::eSMR_Item, slowDownScale);
				pActor->SetSpeedMultipler(SActorParams::eSMR_GameRules, slowDownScale);
			}
		}
		++it_begin;
	}
}

void CTrap::DataLeakage()
{
	CActor *pActor;
	std::map<EntityId, HealthAndDie>::iterator it_begin = ActorsWithinArea.begin();
	std::map<EntityId, HealthAndDie>::iterator it_end = ActorsWithinArea.end();
	while (it_begin != it_end)
	{
		pActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(it_begin->first);
		if (pActor)
		{
			//Code Note:SMovementState state
			const IAIObject* pAIObject = pActor->GetEntity()->GetAI();
			const IFactionMap& factionMap = gEnv->pAISystem->GetFactionMap();
			if (pAIObject->GetFactionID() != factionMap.GetFactionID(faction))
			{
				SMovementState state;
				pActor->GetMovementController()->GetMovementState(state);
				if (state.isMoving)
				{
					if(!pLeakEffect)
						pLeakEffect=gEnv->pParticleManager->FindEffect("Code_System.CodeObject.Trap_DataLeak");
					else
						pLeakEffect->Spawn(true,IParticleEffect::ParticleLoc(pActor->GetEntity()->GetPos()));
					if ((pActor->GetHealth()-movementDamage) <= 0)
					{
						CreateHit(GetEntityId(), pActor->GetEntityId(), movementDamage);
					}
					else
						pActor->SetHealth(pActor->GetHealth() - movementDamage);
				}
			}
		}
		++it_begin;
	}
}

void CTrap::DamageCov()
{
	const IFactionMap& factionMap = gEnv->pAISystem->GetFactionMap();
	uint8 entityFactionID = factionMap.GetFactionID(faction);
	std::map<EntityId, HealthAndDie>::iterator it_begin = ActorsWithinArea.begin();
	std::map<EntityId, HealthAndDie>::iterator it_end = ActorsWithinArea.end();
	float minHealth = 0;
	while (it_begin != it_end)
	{
		CActor *pActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(it_begin->first);
		if (pActor && pActor->GetEntityId() != covHit.targetId && !pActor->IsDead())
		{
			const IAIObject* pAIObject = pActor->GetEntity()->GetAI();
			uint8 actorFactionID = pAIObject ? pAIObject->GetFactionID() : IFactionMap::InvalidFactionID;
			
			if (pAIObject && (pAIObject->GetFactionID() != entityFactionID))
			{
				float health = pActor->GetHealth();
				if(health > minHealth)
				{
					minHealth = health;
					EnemyId = pActor->GetEntityId();
				}
			}
		}
		++it_begin;
	}

	totalDamage = covHit.damage;
	CActor *pCovFromActor = (CActor*)g_pGame->GetGameRules()->GetActorByEntityId(covHit.targetId);
	if(pCovFromActor && !pCovFromActor->IsDead())
	{
		pCovFromActor->SetHealth(pCovFromActor->GetHealth() + totalDamage * damageScale);
		if (totalDamage > 0 && EnemyId != -1 && HasProcessedHit)
		{
			if(!pDamageCovEffect)
				pDamageCovEffect=gEnv->pParticleManager->FindEffect("Code_System.CodeObject.Trap_DamageCov");
			if(pDamageCovEffect)
			{
				AABB CovFromAABB;
				AABB CovToAABB;
				IEntity *pCovFromEntity = gEnv->pEntitySystem->GetEntity(covHit.targetId);
				IEntity *pCovToEntity = gEnv->pEntitySystem->GetEntity(EnemyId);
				if(!pCovFromEntity || !pCovToEntity)
					return;
				pCovFromEntity->GetWorldBounds(CovFromAABB);
				pCovToEntity->GetWorldBounds(CovToAABB);
				gEnv->pRenderer->DrawLabel(CovFromAABB.GetCenter(),1.5,"CovFrom");
				gEnv->pRenderer->DrawLabel(CovToAABB.GetCenter(),1.5,"CovTo");
				Vec3 dir = (CovToAABB.GetCenter() - CovFromAABB.GetCenter()).normalize();

				IParticleEmitter *pEmitter = pDamageCovEffect->Spawn(false,IParticleEffect::ParticleLoc(CovFromAABB.GetCenter(),dir));
				//GetEntity()->LoadParticleEmitter(30,pDamageCovEffect);
				if(pEmitter)
				{
					float length = (CovToAABB.GetCenter() - CovFromAABB.GetCenter()).GetLengthFast();
					float speed = length / 5;
					SpawnParams Emitter_Params;
					pEmitter->GetSpawnParams(Emitter_Params);
					Emitter_Params.eAttachType = GeomType_None;
					Emitter_Params.fSpeedScale = speed;
					pEmitter->SetSpawnParams(Emitter_Params);
				}
			}
			CreateHit(GetEntityId(), EnemyId, totalDamage*damageScale);
			HasProcessedHit = false;
		}
	}

	totalDamage = 0;//reset total damage
	EnemyId = -1;//reset EnemyId
}

void CTrap::Dedication()
{
	CActor *pActor;
	const IFactionMap& factionMap = gEnv->pAISystem->GetFactionMap();
	uint8 entityFactionID = factionMap.GetFactionID(faction);
	std::map<EntityId, HealthAndDie>::iterator out_it_begin = ActorsWithinArea.begin();
	std::map<EntityId, HealthAndDie>::iterator out_it_end = ActorsWithinArea.end();
	while (out_it_begin != out_it_end)
	{
		pActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(out_it_begin->first);
		if (pActor && pActor->GetHealth() <= 0 && !out_it_begin->second.isDead)
		{
			out_it_begin->second.isDead = true;
			const IAIObject* out_pAIObject = pActor->GetEntity()->GetAI();
			uint8 actorFactionID = out_pAIObject ? out_pAIObject->GetFactionID() : IFactionMap::InvalidFactionID;

			if (entityFactionID == playerFactionID && actorFactionID == playerFactionID)
			{
				std::map<EntityId, HealthAndDie>::iterator in_it_begin = ActorsWithinArea.begin();
				std::map<EntityId, HealthAndDie>::iterator in_it_end = ActorsWithinArea.end();
				while (in_it_begin != in_it_end)
				{
					CActor* pPerson = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(in_it_begin->first);
					if (pPerson && (pPerson->GetHealth() > 0))
					{
						const IAIObject* in_pAIObject = pPerson->GetEntity()->GetAI();
						if (in_pAIObject->GetFactionID() == entityFactionID)
						{
							pPerson->SetHealth(pPerson->GetHealth() + treatment);
							//CryLogAlways("heal player %d",pPerson->GetEntityId());
						}
						else
						{
							CreateHit(out_it_begin->first, in_it_begin->first, pPerson->GetHealth()*dedicationDamageScale);
							//CryLogAlways("Hit player %d ,now he's health is %f", pPerson->GetEntityId(), pPerson->GetHealth());
						}
					}
					++in_it_begin;
				}
			}
			else if (entityFactionID == gruntFactionID && actorFactionID == gruntFactionID)
			{
				std::map<EntityId, HealthAndDie>::iterator in_it_begin = ActorsWithinArea.begin();
				std::map<EntityId, HealthAndDie>::iterator in_it_end = ActorsWithinArea.end();
				while (in_it_begin != in_it_end)
				{
					CActor* pPerson = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(in_it_begin->first);
					if (pPerson && (pPerson->GetHealth() > 0))
					{
						const IAIObject* in_pAIObject = pPerson->GetEntity()->GetAI();
						if (in_pAIObject->GetFactionID() == gruntFactionID)
						{
							pPerson->SetHealth(pPerson->GetHealth() + treatment);
							//CryLogAlways("heal player %d",pPerson->GetEntityId());
						}
						else
						{
							CreateHit(out_it_begin->first, in_it_begin->first, pPerson->GetHealth()*dedicationDamageScale);
							//CryLogAlways("Hit player %d ,now he's health is %f", pPerson->GetEntityId(), pPerson->GetHealth());
						}
					}
					++in_it_begin;
				}
			}
			else if (entityFactionID == civilianFactionID && actorFactionID == civilianFactionID)
			{
				for (std::map<EntityId, HealthAndDie>::iterator in_iterator = ActorsWithinArea.begin(); in_iterator != ActorsWithinArea.end(); ++in_iterator)
				{
					CActor* pPerson = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(in_iterator->first);
					if (pPerson && (pPerson->GetHealth() > 0))
					{
						CreateHit(out_it_begin->first, in_iterator->first, pPerson->GetHealth()*dedicationDamageScale);
					}
				}
			}
		}
		++out_it_begin;
	}
}

void CTrap::Recycle(float fFrameTime)
{
	CActor *pActor;
	const IFactionMap& factionMap = gEnv->pAISystem->GetFactionMap();
	uint8 entityFactionID = factionMap.GetFactionID(faction);
	std::map<EntityId, HealthAndDie>::iterator it_begin = ActorsWithinArea.begin();
	std::map<EntityId, HealthAndDie>::iterator it_end = ActorsWithinArea.end();
	while (it_begin != it_end)
	{
		pActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(it_begin->first);
		if (pActor && pActor->GetHealth() <= 0 && !it_begin->second.isDead)
		{
			it_begin->second.isDead = true;
			const IAIObject* pAIObject = pActor->GetEntity()->GetAI();
			uint8 actorFactionID = pAIObject ? pAIObject->GetFactionID() : IFactionMap::InvalidFactionID;
			

			if (entityFactionID == civilianFactionID)//if this entity's faction is civilian
			{
				pActor->GetEntity()->SetPos(ZERO);
				pActor->GetEntity()->Hide(true);
			}
			else if(entityFactionID == playerFactionID)//player's
			{

				if (actorFactionID != playerFactionID)
				{
					g_pGame->GetWorldStability()->StabilityGain(static_cast<const int>(stabilityGain));
				}

			}
		}
		if (entityFactionID == gruntFactionID)//if this entity's faction is civilian
		{
			timer += fFrameTime;
			if (timer >= lostTime)
			{
				g_pGame->GetWorldStability()->StabilityCost(static_cast<const int>(stabilityLost));
				timer = 0;
			}
		}
		++it_begin;
	}
}

void CTrap::CreateHit(EntityId shooterId, EntityId targetId, float damage)
{
	HitInfo hit;
	hit.shooterId = shooterId;
	hit.targetId = targetId;
	hit.dir = Vec3(0, 0, 1);
	hit.weaponId = 0;
	hit.damage = damage;
	hit.type = 27;
	if (hit.targetId != hit.shooterId)
	{
		g_pGame->GetGameRules()->ClientHit(hit);
	}
}


void CTrap::SpawnIndicateParticle()
{
	SEntitySpawnParams params;
	params.sName = "TrapIndicator";

	params.vPosition=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetPos();
	params.vScale=Vec3(1,1,1);
	params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("ParticleEffect");
	pEffect=gEnv->pEntitySystem->SpawnEntity(params);

	SmartScriptTable proot=pEffect->GetScriptTable();
	SmartScriptTable pprops;
	proot->GetValue("Properties",pprops);
	pprops->SetValue("ParticleEffect","Code_System.Module_Effect.DataArrow");
	Script::CallMethod(proot,"OnReset");
}

void CTrap::SetCovHit(const HitInfo &info)
{
	covHit = info;
	HasProcessedHit = true;
}