#ifndef _SMARTTURRET_HELPERS_H_
#define _SMARTTURRET_HELPERS_H_

#include <Game.h>
#include <IGameRulesSystem.h>
#include <Projectile.h>

namespace SmartTurretHelpers
{



	void EmitTracer(const Vec3& pos, const Vec3& destination, CProjectile* pProjectile, const float speed)
	{
		CTracerManager::STracerParams params;
		params.position = pos;
		params.destination = destination;

		params.effect = "Code_System.Module_Effect.DataBite";
		params.geometry = 0;
		params.speed = speed;
		params.scale = 1;

		params.slideFraction = 0;
		params.lifetime = 2;
		params.delayBeforeDestroy = 0;

		const int tracerIdx = g_pGame->GetWeaponSystem()->GetTracerManager().EmitTracer(params, pProjectile ? pProjectile->GetEntityId() : 0);

		if (pProjectile)
		{
			pProjectile->BindToTracer(tracerIdx);
		}
	}

	ILINE IItemSystem* GetItemSystem()
	{
		assert(g_pGame != NULL);

		IGameFramework* pGameFramework = g_pGame->GetIGameFramework();
		if (pGameFramework == NULL)
		{
			return NULL;
		}

		IItemSystem* pItemSystem = pGameFramework->GetIItemSystem();
		return pItemSystem;
	}

	ILINE IEntitySystem* GetEntitySystem()
	{
		assert(gEnv != NULL);
		IEntitySystem* pEntitySystem = gEnv->pEntitySystem;
		return pEntitySystem;
	}
}

#endif