#ifndef _SMART_TURRET_BEHAVIOR_EVENTS_
#define _SMART_TURRET_BEHAVIOR_EVENTS_
#include "State.h"

enum ESmartTurretStates
{
	eSmartTurretState_Behavior = STATE_FIRST,
};

enum ESmartTurretBehaviorEvent
{
	STATE_EVENT_SMART_TURRET_SAY_HELLO = STATE_EVENT_CUSTOM,
	STATE_EVENT_SMART_TURRET_FORCE_STATE,
};
#endif