#ifndef _SMARTTURRET_H_
#define _SMARTTURRET_H_

#include "State.h"
#include <IGameObject.h>
#include <Health.h>

enum ECSmartTurretEventListenerGameObjectEvent
{
	eSmartTurretListenerGameObjectEvent_Test = 20,
};

enum ESmartTurretBehaviorState
{
	eSmartTurretBehaviorState_Undeployed,
	eSmartTurretBehaviorState_Alive,
	eSmartTurretBehaviorState_Dead,
	eSmartTurretBehaviorState_Deployed,

	eSmartTurretBehaviorState_SayHello,
	eSmartTurretBehaviorState_SayGoodBye,
};

class CSmartTurret :
	public CGameObjectExtensionHelper<CSmartTurret, IGameObjectExtension>
{
public:
	CSmartTurret();
	virtual ~CSmartTurret();

	// IGameObjectExtension
	virtual bool Init(IGameObject * pGameObject);
	virtual void InitClient(int channelId) {};
	virtual void PostInit(IGameObject * pGameObject);
	virtual void PostInitClient(int channelId) {};
	virtual bool ReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params);
	virtual void PostReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params) {}
	virtual bool GetEntityPoolSignature(TSerialize signature);
	virtual void Release();
	virtual void FullSerialize(TSerialize ser);
	virtual bool NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) { return false; };
	virtual void PostSerialize() {};
	virtual void SerializeSpawnInfo(TSerialize ser) {}
	virtual ISerializableInfoPtr GetSpawnInfo() { return 0; }
	virtual void Update(SEntityUpdateContext& ctx, int slot);
	virtual void HandleEvent(const SGameObjectEvent& gameObjectEvent);
	virtual void ProcessEvent(SEntityEvent& entityEvent);
	virtual void SetChannelId(uint16 id) {};
	virtual void SetAuthority(bool auth) {};
	virtual void PostUpdate(float frameTime) { CRY_ASSERT(false); }
	virtual void PostRemoteSpawn() {};
	virtual void GetMemoryUsage(ICrySizer *pSizer) const;

	// ~IGameObjectExtension

	bool IsDead();
	float GetHealth();
	float GetHealthMax();
	void SetHealth(const float);
	void SetHealthMax(const float);
	void SetFactionId(const uint8 factionId);
	uint8 GetFactionId();

	bool IsVisibleInRange(const VisionID&);
	bool IsEntityCloaked(IEntity*);
	bool IsEntityInvisible(const IEntity* targetEntity);
	bool IsEntityInRange(const IEntity* pTargetEntity, const float range);

	bool IsEntityHostileAndThreading(IEntity* pTargetEntity);
	bool IsVehicleHostileAndThreading(IEntity* pTargetVehicleEntity);

	void StartFire();
	void Sweep();
	void UpdateTargetWroldPos();
protected:
	Vec3 CaculateTargetPredictWroldPos(IEntity* pTargetEntity,const float predictDuration);
	IEntity* GetTargetEntity();
	void AddVisibleTarget(const EntityId targetId);
	void RemoveVisibleTarget(const EntityId targetId);
	void UpdateVisibleTarget();
private:
	float timer;
	uint8 m_turretFactionId;
	Vec3 m_turretWorldPos;
	Vec3 m_targetWroldPos;

	//VisionID m_radarVisionId;

	float m_predictDuration;

	float m_radarRange;
	float m_radarFov;
	float m_RadarVerticalOffset;
	float m_eyeRange;
	float m_eyeFov;
	float m_cloakDetectionDistance;

	struct STrackedTarget
	{
		EntityId entityId;
		bool valid;
		int referenceCount;

		STrackedTarget(EntityId _entityId)
			: entityId(_entityId)
			, valid(false)
			, referenceCount(1)
		{
		}

		bool operator == (const EntityId otherEntityId) const
		{
			return (entityId == otherEntityId);
		}
	};

	typedef std::vector<STrackedTarget> TrackedTargetList;
	TrackedTargetList m_trackedTargets;

	IEntity* m_targetEntity;
	EntityId m_visibleTargetId;

	void ReadScript();
	DECLARE_STATE_MACHINE(CSmartTurret, Behavior);
};


#endif