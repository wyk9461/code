#include "StdAfx.h"
#include "Utility/CryHash.h"
#include "SmartTurretBehaviorEvents.h"
#include "SmartTurret.h"

class CSmartTurretBehavior
	:public CStateHierarchy<CSmartTurret>
{
	DECLARE_STATE_CLASS_BEGIN(CSmartTurret, CSmartTurretBehavior);
	DECLARE_STATE_CLASS_ADD(CSmartTurret, Alive);
	DECLARE_STATE_CLASS_ADD(CSmartTurret, Dead);
	DECLARE_STATE_CLASS_ADD(CSmartTurret, UnDeployed);
	DECLARE_STATE_CLASS_ADD(CSmartTurret, Deployed);

	DECLARE_STATE_CLASS_ADD(CSmartTurret, SayHello);
	DECLARE_STATE_CLASS_ADD(CSmartTurret, SayGoodBye);

	DECLARE_STATE_CLASS_END(CSmartTurret);
public:
protected:
private:
};

DEFINE_STATE_CLASS_BEGIN(CSmartTurret, CSmartTurretBehavior,eSmartTurretState_Behavior,Deployed);
	DEFINE_STATE_CLASS_ADD(CSmartTurret, CSmartTurretBehavior, Alive, Root);
		DEFINE_STATE_CLASS_ADD(CSmartTurret, CSmartTurretBehavior, SayHello, Alive);
		DEFINE_STATE_CLASS_ADD(CSmartTurret, CSmartTurretBehavior, SayGoodBye, Alive);
	DEFINE_STATE_CLASS_ADD(CSmartTurret, CSmartTurretBehavior, Dead, Alive);
DEFINE_STATE_CLASS_END(CSmartTurret, CSmartTurretBehavior);

const CSmartTurretBehavior::TStateIndex CSmartTurretBehavior::Root(CSmartTurret& turret, const SStateEvent& event)
{
	const ESmartTurretBehaviorEvent eventId = static_cast<ESmartTurretBehaviorEvent>(event.GetEventId());

	switch (eventId)
	{
		//case STATE_EVENT_INIT:
		//m_pParams = &turret.GetBehaviorParams();
		//break;

	case STATE_EVENT_SMART_TURRET_FORCE_STATE:
	{
												 /*								   const SStateEventForceState& stateEventForceState = static_cast<const SStateEventForceState&>(event);
												 const ETurretBehaviorState forcedStateId = stateEventForceState.GetForcedState();
												 if (forcedStateId == eTurretBehaviorState_Undeployed)
												 {
												 return State_Undeployed;
												 }
												 else if (forcedStateId == eTurretBehaviorState_Deployed)
												 {
												 return State_Deployed;
												 }
												 else if (forcedStateId == eTurretBehaviorState_PartiallyDeployed)
												 {
												 return State_PartiallyDeployed;
												 }*/
	}
		break;
	}

	return State_Done;
}

const CSmartTurretBehavior::TStateIndex CSmartTurretBehavior::Alive(CSmartTurret& turret, const SStateEvent& event)
{
	const ESmartTurretBehaviorEvent eventId = static_cast<ESmartTurretBehaviorEvent>(event.GetEventId());
	switch (eventId)
	{
	case STATE_EVENT_ENTER:
	{
							  CryLogAlways("Coming in state-alive");
	}
	}
	return STATE_CONTINUE;
}

const CSmartTurretBehavior::TStateIndex CSmartTurretBehavior::Dead(CSmartTurret& blah, const SStateEvent& event)
{
	const ESmartTurretBehaviorEvent eventId = static_cast<ESmartTurretBehaviorEvent>(event.GetEventId());
	switch (eventId)
	{
	case STATE_EVENT_ENTER:
	{
							  CryLogAlways("Coming in state-dead");
	}
	default:
		break;
	}
	return State_Done;
}
const CSmartTurretBehavior::TStateIndex CSmartTurretBehavior::SayGoodBye(CSmartTurret& blah, const SStateEvent& event)
{
	const ESmartTurretBehaviorEvent evneId = static_cast<ESmartTurretBehaviorEvent>(event.GetEventId());
	switch (evneId)
	{
	case STATE_EVENT_ENTER:
	{
							  CryLogAlways("Coming in state-sayGoodBye");
							  break;
	}

	}
	return State_Done;
}
const CSmartTurretBehavior::TStateIndex CSmartTurretBehavior::SayHello(CSmartTurret& blah, const SStateEvent& event)
{
	const ESmartTurretBehaviorEvent eventId = static_cast<ESmartTurretBehaviorEvent>(event.GetEventId());
	switch (eventId)
	{
	case STATE_EVENT_ENTER:
	{
							  CryLogAlways("Coming in state-sayHello");
							  break;
	}
	}
	CryLogAlways("Hello world");
	return State_SayGoodBye;
}