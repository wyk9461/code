#include "StdAfx.h"
#include "SmartTurret.h"
#include <IItemSystem.h>
#include <IAttachment.h>
#include <Projectile.h>
#include "SmartTurretHelpers.h"
#include "SmartTurretBehaviorEvents.h"
#include <IVisionMap.h>
#include <IAIActor.h>
#include <IPhysics.h>

using namespace SmartTurretHelpers;
DEFINE_STATE_MACHINE(CSmartTurret, Behavior);

CSmartTurret::CSmartTurret()
:timer(0)
{
}

CSmartTurret::~CSmartTurret()
{
	StateMachineReleaseBehavior();
}


bool CSmartTurret::Init(IGameObject * pGameObject)
{
	SetGameObject(pGameObject);
	GetGameObject()->EnablePhysicsEvent(true, eEPE_AllLogged);
	//StateMachineInitBehavior();
	return true;
}

void CSmartTurret::PostInit(IGameObject * pGameObject)
{
	ReadScript();
	GetGameObject()->EnableUpdateSlot(this, 0);
	GetGameObject()->SetUpdateSlotEnableCondition(this, 0, eUEC_Always);
}

bool CSmartTurret::ReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params)
{
	ReadScript();
	ResetGameObject();
	return true;
}

bool CSmartTurret::GetEntityPoolSignature(TSerialize signature)
{
	return true;
}

void CSmartTurret::Release()
{
	delete this;
}

void CSmartTurret::FullSerialize(TSerialize ser)
{

}

void CSmartTurret::HandleEvent(const SGameObjectEvent& gameObjectEvent)
{
	const uint32 eventId = gameObjectEvent.event;
	void* pParam = gameObjectEvent.param;
	if ((eventId == eGFE_ScriptEvent) && (pParam != NULL))
	{
		const char* eventName = static_cast<const char*>(pParam);
		if (strcmp(eventName, "OnDestroy") == 0)
		{

		}
	}
	else if (eventId == eSmartTurretListenerGameObjectEvent_Test)
	{

	}
}

void CSmartTurret::ProcessEvent(SEntityEvent& entityEvent)
{

}

void CSmartTurret::GetMemoryUsage(ICrySizer *pSizer) const
{

}

void CSmartTurret::Update(SEntityUpdateContext& ctx, int slot)
{
	timer += ctx.fFrameTime;
	if (timer >= 1)
	{
		m_turretWorldPos = GetEntity()->GetPos();
		m_targetEntity = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity();
		if (m_targetEntity && !IsEntityCloaked(m_targetEntity))
		{
			UpdateTargetWroldPos();
			StartFire();
		}
		timer = 0;
	}
}

void CSmartTurret::StartFire()
{
	IEntityClass* m_pBulletClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("RifleBullet");
	CProjectile* m_pProjectile = g_pGame->GetWeaponSystem()->SpawnAmmo(m_pBulletClass);
	Vec3 spawnPos(m_turretWorldPos);
	spawnPos.z += 1;
	Vec3 dir = m_targetWroldPos - spawnPos;
	m_pProjectile->SetParams(CProjectile::SProjectileDesc(GetEntityId(), 0, 0, 100, 0.f, 0.f, 0.f, 2, 0, false));
	m_pProjectile->Launch(spawnPos, dir, dir * 10);
	SmartTurretHelpers::EmitTracer(spawnPos, dir, m_pProjectile, 35);
}

bool CSmartTurret::IsEntityInvisible(const IEntity* targetEntity)
{
	const Vec3 turretWorldPos = GetEntity()->GetWorldPos();
	const Vec3 targetWorldPos = targetEntity->GetWorldPos();
	const Vec3 dir = targetWorldPos - turretWorldPos;
	ray_hit hit;
	const int rayFlags = rwi_pierceability_mask | rwi_colltype_any;
	IPhysicalEntity* pSelf = GetEntity()->GetPhysics();
	gEnv->pPhysicalWorld->RayWorldIntersection(turretWorldPos, dir, ent_all, rayFlags, &hit, 1, &pSelf, 1);
	if (hit.pCollider)
	{
		return true;
	}
	return false;
}

bool CSmartTurret::IsEntityCloaked(IEntity* pTargetEntity)
{
	const IEntity* const pTurretEntity = GetEntity();
	const Vec3 turretWorldPosition = pTurretEntity->GetWorldPos();

	const bool isCloaked = IsEntityInvisible(pTargetEntity);

	const Vec3 targetWorldPosition = pTargetEntity->GetWorldPos();
	const float cloakDetectionDistanceSquared = m_cloakDetectionDistance * m_cloakDetectionDistance;
	const float distanceSquared = Vec3(targetWorldPosition - turretWorldPosition).GetLengthSquared();
	const bool inCloakDetectionDistance = (distanceSquared < cloakDetectionDistanceSquared);

	const bool isInvisible = (isCloaked && !inCloakDetectionDistance);
	return isInvisible;
}

bool CSmartTurret::IsEntityInRange(const IEntity* pTargetEntity,const float range)
{
	const Vec3 turretWorldPos = GetEntity()->GetWorldPos();
	const Vec3 targetWroldPos = pTargetEntity->GetWorldPos();
	const float distanceSquared = turretWorldPos.GetSquaredDistance(targetWroldPos);
	const float rangeSquared = range * range;
	const bool isInRange = (distanceSquared <= rangeSquared);
	return isInRange;
}

Vec3 CSmartTurret::CaculateTargetPredictWroldPos(IEntity* pTargetEntity,const float predictDuration)
{
	IPhysicalEntity* pTargetPhyEnt = pTargetEntity->GetPhysics();
	assert(pTargetPhyEnt != NULL);
	pe_status_dynamics dyn;
	pTargetPhyEnt->GetStatus(&dyn);
	const float posOffset_x = predictDuration * dyn.v.x;
	const float posOffset_y = predictDuration * dyn.v.y;
	const float posOffset_z = predictDuration * dyn.v.z;
	const Vec3 posOffset(posOffset_x, posOffset_y, posOffset_z);
	const Vec3 tagetWorldPos = pTargetEntity->GetWorldPos();
	const Vec3 targetPredictWroldPos = tagetWorldPos + posOffset;
	return targetPredictWroldPos;
}

void CSmartTurret::UpdateTargetWroldPos()
{
	m_targetWroldPos = CaculateTargetPredictWroldPos(m_targetEntity, m_predictDuration);
}

IEntity* CSmartTurret::GetTargetEntity()
{
	const IEntitySystem* const pEntitySystem = GetEntitySystem();
	assert(pEntitySystem != NULL);
	IEntity* const pTargetEntity = pEntitySystem->GetEntity(m_visibleTargetId);
	return pTargetEntity;
}

void CSmartTurret::AddVisibleTarget(const EntityId targetId)
{
	TrackedTargetList::iterator it = std::find(m_trackedTargets.begin(), m_trackedTargets.end(), targetId);
	const bool entryfound = (it != m_trackedTargets.end());
	if (entryfound)
	{
		STrackedTarget& trackedTraget = *it;
		trackedTraget.referenceCount++;
	}
	else
	{
		m_trackedTargets.push_back(STrackedTarget(targetId));
	}
}

void CSmartTurret::RemoveVisibleTarget(const EntityId targetId)
{
	TrackedTargetList::iterator it = std::find(m_trackedTargets.begin(), m_trackedTargets.end(), targetId);
	const bool entryfound = (it != m_trackedTargets.end());
	if (!entryfound)
	{
		return;
	}
	STrackedTarget& trackedTarget = *it;
	trackedTarget.referenceCount--;
	if (trackedTarget.referenceCount == 0)
	{
		m_trackedTargets.erase(it);
	}
}

void CSmartTurret::UpdateVisibleTarget()
{

}

bool CSmartTurret::IsEntityHostileAndThreading(IEntity* pTargetEntity)
{
	return false;
}

bool CSmartTurret::IsVehicleHostileAndThreading(IEntity* pTargetVehicleEntity)
{
	if (pTargetVehicleEntity == NULL)
	{
		return false;
	}
	IEntity* const pTurretEntity = GetEntity();

	bool isAlive = true;
	bool isHostileAndThreatening = false;
	IAIObject* const pTurretAiObject = pTurretEntity->GetAI();
	IAIObject* pTargetAiObject = pTargetVehicleEntity->GetAI();

	{
		IVehicleSystem* const pVehicleSystem = g_pGame->GetIGameFramework()->GetIVehicleSystem();
		const EntityId targetVehicleEntityId = pTargetVehicleEntity->GetId();
		const IVehicle* const pVehicle = pVehicleSystem->GetVehicle(targetVehicleEntityId);
		if (pVehicle != NULL)
		{
			const IActor* const pDriverActor = pVehicle->GetDriver();
			if (pDriverActor != NULL)
			{
				IEntity* const pDriverEntity = pDriverActor->GetEntity();
				pTargetAiObject = pDriverEntity->GetAI();
			}
		}
	}

	if (pTurretAiObject != NULL && pTargetAiObject != NULL)
	{
		const uint8 turretFactionId = pTurretAiObject->GetFactionID();
		const uint8 targetFactionId = pTargetAiObject->GetFactionID();

		const IFactionMap& factionMap = gEnv->pAISystem->GetFactionMap();
		const IFactionMap::ReactionType reactionType = factionMap.GetReaction(turretFactionId, targetFactionId);
		isHostileAndThreatening = (reactionType == IFactionMap::Hostile) && pTargetAiObject->IsThreateningForHostileFactions();
	}

	return isHostileAndThreatening;
}

void CSmartTurret::SetFactionId(const uint8 factionId)
{
	IF_UNLIKELY(factionId == IFactionMap::InvalidFactionID)
	{
		return;
	}

	IEntity* const pEntity = GetEntity();
	IAIObject* const pAiObject = pEntity->GetAI();
	if (pAiObject != NULL)
	{
		pAiObject->SetFactionID(factionId);
	}
}

uint8 CSmartTurret::GetFactionId()
{
	return m_turretFactionId;
}

void CSmartTurret::ReadScript()
{
	SmartScriptTable root = GetEntity()->GetScriptTable();

	SmartScriptTable props, vision,rateOfDeath;

	root->GetValue("Properties", props);
	props->GetValue("Vision", vision);
	props->GetValue("RateOfDeath", rateOfDeath);

	vision->GetValue("fRadarRange", m_radarRange);
	vision->GetValue("fRadarFov", m_radarFov);
	vision->GetValue("fRadarVerticalOffset", m_RadarVerticalOffset);
	vision->GetValue("fEyeRange", m_eyeRange);
	vision->GetValue("fEyeFov", m_eyeFov);
	vision->GetValue("fCloakDetectionDistance", m_cloakDetectionDistance);

	rateOfDeath->GetValue("predictDuration", m_predictDuration);
}