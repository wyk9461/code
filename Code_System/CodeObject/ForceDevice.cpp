#include "StdAfx.h"
#include "ForceDevice.h"


IParticleEffect *CForceDevice::pEffect = NULL;

CForceDevice::CForceDevice()
: hitTimes(0)
, hitTimesLimit(0)
, isMother(false)
, count(0)
, timer(0)
, enable(false)
, explosionDamage(0)
, exploRadius(0)
, force(0)
, hitTimeInterval(1)
{
}

CForceDevice::~CForceDevice()
{
}


bool CForceDevice::Init(IGameObject * pGameObject)
{
	SetGameObject(pGameObject);
	GetGameObject()->EnablePhysicsEvent(true, eEPE_AllLogged);
	IAIObject *pAI = GetEntity()->GetAI();
	return true;
}

void CForceDevice::PostInit(IGameObject * pGameObject)
{
	ReadScript();
	GetGameObject()->EnableUpdateSlot(this, 0);
	GetGameObject()->SetUpdateSlotEnableCondition(this, 0, eUEC_Always);
}

bool CForceDevice::ReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params)
{
	ResetGameObject();
	return true;
}

bool CForceDevice::GetEntityPoolSignature(TSerialize signature)
{
	return true;
}

void CForceDevice::Release()
{
	delete this;
}

void CForceDevice::FullSerialize(TSerialize ser)
{

}


//======================================================================
//the number of this entity will remain CloneLimit 
//	unless the mother is destroyed
//timer:time before a new one spawn
//count:the number of entity
//=====================================================================
void CForceDevice::Update(SEntityUpdateContext& ctx, int slot)
{
	if(!g_pGame->GetIGameFramework()->IsGamePaused() && !g_pGame->GetIGameFramework()->IsEditing())
	{
		timer += ctx.fFrameTime;
		if (GetEntity()->IsActive())
		{
			if (hitTimes >= hitTimesLimit)
			{
				CommonUsage::CreateExplosion(GetEntityId(),GetEntity()->GetPos(), explosionDamage, exploRadius, 1000);
				count = 0;
				hitTimes = 0;
				g_pGame->GetBasicRayCast()->ClearLastEntity(GetEntityId());
				gEnv->pEntitySystem->RemoveEntity(GetEntityId(), false);
			}
		}
	}
}


void CForceDevice::HandleEvent(const SGameObjectEvent& gameObjectEvent)
{
	const uint32 eventId = gameObjectEvent.event;
	void* pParam = gameObjectEvent.param;
	if ((eventId == eGFE_ScriptEvent) && (pParam != NULL))
	{

	}
	else if (eventId == eForceDeviceListenerGameObjectEvent_Test)
	{

	}
}

void CForceDevice::ProcessEvent(SEntityEvent& entityEvent)
{
	switch (entityEvent.event)
	{
		case ENTITY_EVENT_COLLISION:
		{
			if(timer >= hitTimeInterval && hitTimes <= hitTimesLimit)
			{
				AABB bounds;
				IEntityPhysicalProxy *pProxy = (IEntityPhysicalProxy*)GetEntity()->GetProxy(ENTITY_PROXY_PHYSICS);
				if (pProxy)
				{
					EventPhysCollision *pCollision = (EventPhysCollision *)(entityEvent.nParam[0]);
					pProxy->GetWorldBounds(bounds);
					pProxy->AddImpulse(-1, pCollision->pt, (bounds.GetCenter() - pCollision->pt)*force, true, 1, 10);
					if(!pEffect)
						pEffect = gEnv->pParticleManager->FindEffect("Code_System.CodeObject.ForceDevice_Collision");
					else
						pEffect->Spawn(false,IParticleEffect::ParticleLoc(pCollision->pt,pCollision->n));
					++hitTimes;
				}
				timer = 0;
			}
		}				  
	}
}

void CForceDevice::GetMemoryUsage(ICrySizer *pSizer) const
{

}

/*void CForceDevice::EraseDestroyedOnes()
{
	CForceDevice *pKMF;
	std::vector<EntityId>::iterator iterator_begin = memberId.begin();
	std::vector<EntityId>::iterator iterator_end = memberId.end();
	while (iterator_begin != iterator_end)
	{
		pKMF = static_cast<CForceDevice*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(*iterator_begin, "ForceDevice"));
		if (pKMF == NULL)
		{
			memberId.erase(iterator_begin);
			--count;
		}
		else
			++iterator_begin;
	}
}

void CForceDevice::SpawnNewOnes(IEntity* pMotherEnt)
{
	SEntitySpawnParams params;
	//params.sName = "Spawn_Particle" + index;
	params.vPosition = pMotherEnt->GetPos() + BiRandom(Vec3(10, 10, 0));
	params.vScale = Vec3(1, 1, 1);
	params.qRotation = Quat::CreateRotationX(DEG2RAD(90));
	params.pClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("ForceDevice");
	IEntity* pEntity = gEnv->pEntitySystem->SpawnEntity(params);
	SmartScriptTable proot = pEntity->GetScriptTable();
	SmartScriptTable pprops;
	//proot->GetValue("Properties", pprops);
	//pprops->SetValue("ParticleEffect", "Code_System.WarpSpawn_2");
	Script::CallMethod(proot, "OnReset");
	memberId.push_back(pEntity->GetId());
	++count;
	CForceDevice *pKMF = static_cast<CForceDevice*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEntity->GetId(), "ForceDevice"));
	if (pKMF)
	{
		pKMF->isMother = false;
		pKMF->CloneLimit = CloneLimit;
	}
}*/

void CForceDevice::ReadScript()
{
	SmartScriptTable root = GetEntity()->GetScriptTable();
	SmartScriptTable props,fp;
	
	root->GetValue("Properties", props);
	props->GetValue("FunctionParams", fp);

	fp->GetValue("bEnable", enable);
	fp->GetValue("Damage", explosionDamage);
	fp->GetValue("HitTimesLimit", hitTimesLimit);
	fp->GetValue("ExploRadius", exploRadius);
	fp->GetValue("Force", force);
}