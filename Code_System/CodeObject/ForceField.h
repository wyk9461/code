#ifndef _FORCEFIELD_H_
#define _FORCEFIELD_H_


#include <IGameObject.h>


enum ECForceFieldEventListenerGameObjectEvent
{
	eForceFieldListenerGameObjectEvent_Test = 20,
};


class CForceField: public CGameObjectExtensionHelper<CForceField, IGameObjectExtension>
{
public:
	CForceField();
	virtual ~CForceField();

	// IGameObjectExtension
	virtual bool Init(IGameObject * pGameObject);
	virtual void InitClient(int channelId) {};
	virtual void PostInit(IGameObject * pGameObject);
	virtual void PostInitClient(int channelId) {};
	virtual bool ReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params);
	virtual void PostReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params) {}
	virtual bool GetEntityPoolSignature(TSerialize signature);
	virtual void Release();
	virtual void FullSerialize(TSerialize ser);
	virtual bool NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) { return false; };
	virtual void PostSerialize() {};
	virtual void SerializeSpawnInfo(TSerialize ser) {}
	virtual ISerializableInfoPtr GetSpawnInfo() { return 0; }
	virtual void Update(SEntityUpdateContext& ctx, int slot);
	virtual void HandleEvent(const SGameObjectEvent& gameObjectEvent);
	virtual void ProcessEvent(SEntityEvent& entityEvent);
	virtual void SetChannelId(uint16 id) {};
	virtual void SetAuthority(bool auth) {};
	virtual void PostUpdate(float frameTime) { CRY_ASSERT(false); }
	virtual void PostRemoteSpawn() {};
	virtual void GetMemoryUsage(ICrySizer *pSizer) const;

	// ~IGameObjectExtension
	void ReadScript();
	ILINE void EnableForceField(bool _enable){enable = _enable;}
	ILINE bool IsEnabled(){return enable;}
private:
	AABB aabb;
	float effectRadius;
	float timer;
	float accScale;
	static IParticleEffect *pEffect;
	struct VelAndMass 
	{
		Vec3 vel;
		float mass;
		float acc;
		VelAndMass()
		:	vel(Vec3(ZERO))
		,	mass(0)
		,	acc(0){}
		VelAndMass(const Vec3& v, const float m,const float a)
		{
			vel = v;
			mass = m;
			acc = a;
		}
	};

	float tick_timer;
	float cd;
	float setup_timer;

	bool enable;
	std::map<EntityId, VelAndMass> entityMap;

	void EnterArea();
	void LeaveArea();
};


#endif