#ifndef _SYSTEMROBOT_H_
#define _SYSTEMROBOT_H_

#include "IGameObject.h"

class CSystemRobot : public CGameObjectExtensionHelper<CSystemRobot, IGameObjectExtension>
{
public:
	CSystemRobot();
	virtual ~CSystemRobot();

	// IGameObjectExtension
	virtual bool Init( IGameObject * pGameObject );
	virtual void InitClient( int channelId ) {};
	virtual void PostInit( IGameObject * pGameObject );
	virtual void PostInitClient( int channelId ) {};
	virtual bool ReloadExtension( IGameObject * pGameObject, const SEntitySpawnParams &params );
	virtual void PostReloadExtension( IGameObject * pGameObject, const SEntitySpawnParams &params ) {}
	virtual bool GetEntityPoolSignature( TSerialize signature );
	virtual void Release();
	virtual void FullSerialize( TSerialize ser );
	virtual bool NetSerialize( TSerialize ser, EEntityAspects aspect, uint8 profile, int flags ) { return false; };
	virtual void PostSerialize() {};
	virtual void SerializeSpawnInfo( TSerialize ser ) {}
	virtual ISerializableInfoPtr GetSpawnInfo() {return 0;}
	virtual void Update( SEntityUpdateContext& ctx, int slot );
	virtual void HandleEvent( const SGameObjectEvent& gameObjectEvent );
	virtual void ProcessEvent( SEntityEvent& entityEvent );
	virtual void SetChannelId( uint16 id ) {};
	virtual void SetAuthority( bool auth ) {};
	virtual void PostUpdate( float frameTime ) { CRY_ASSERT(false); }
	virtual void PostRemoteSpawn() {};
	virtual void GetMemoryUsage( ICrySizer *pSizer ) const;

	// ~IGameObjectExtension

	bool MoveToDestination(Vec3 &oringe_pos , Vec3 &dest_pos , float speed , float stop_length ,float z_offset = 0);
private:
	bool fast_delete;
	bool delete_phase;

	float timer;
	std::vector<EntityId> DeleteTask;
	std::vector<EntityId>::iterator iter;

	std::vector<SEntitySpawnParams> RecoveryTask;
	std::vector<SEntitySpawnParams>::iterator r_iter;
};
#endif