#include "StdAfx.h"
#include "PressureTrigger.h"


#include "Trap.h"
#include "ForceDevice.h"
#include "ForceField.h"

#include "Code_System/MicroSystem/ProbeManager.h"


EntityClassPreLoad *CPressureTrigger::preload = NULL;

CPressureTrigger::CPressureTrigger()
:	isTriggered(false)
,	PressureTimer(0.f){}

CPressureTrigger::~CPressureTrigger()
{

}

bool CPressureTrigger::Init(IGameObject * pGameObject)
{
	isTriggered = false;
	FirstInit = true;
	preload = &EntityClassPreLoad::Instance();
	SetGameObject(pGameObject);

	GetGameObject()->EnablePhysicsEvent(true, eEPE_AllImmediate);
	return true;
}

void CPressureTrigger::PostInit(IGameObject *pGameObject)
{
	GetGameObject()->EnableUpdateSlot(this, 0);
	GetGameObject()->SetUpdateSlotEnableCondition(this, 0, eUEC_Always);

	ReadScript();
}

bool CPressureTrigger::ReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params)
{
	IEntityAreaProxyPtr proxy = crycomponent_cast<IEntityAreaProxyPtr>(GetEntity()->CreateProxy(ENTITY_PROXY_AREA));
	if(proxy)
	{
		proxy->ClearEntities();
	}
	ResetGameObject();
	ReadScript();
	return true;
}

bool CPressureTrigger::GetEntityPoolSignature(TSerialize signature)
{
	return true;
}

void CPressureTrigger::Release()
{
	//ProbeManager::Instance().UnlinkEntity(this->GetEntity());
	delete this;
}

void CPressureTrigger::FullSerialize(TSerialize ser)
{

}

void CPressureTrigger::Update(SEntityUpdateContext& ctx, int slot)
{
	if(!g_pGame->GetIGameFramework()->IsGamePaused() && !g_pGame->GetIGameFramework()->IsEditing())
	{
		ray_hit hit;
		IPhysicalEntity *pSelf = GetEntity()->GetPhysics();
		AABB aabb;
		GetEntity()->GetLocalBounds(aabb);
		int hits = gEnv->pPhysicalWorld->RayWorldIntersection(GetEntity()->GetPos() + Vec3(0,0,aabb.GetSize().z), Vec3(0,0,1)*0.3, ent_all, rwi_pierceability_mask | rwi_colltype_any, &hit, 1, &pSelf, 1);
		if(hits && !isTriggered)
		{
			isTriggered = true;
			ProcessTrigger();
		}
		else if(!hits && isTriggered)
		{
			isTriggered = false;
		}

		IEntityLink *pLink = GetEntity()->GetEntityLinks();
		while(pLink)
		{
			IParticleEmitter *pEmitter = GetEntity()->GetParticleEmitter(pLink->entityId);
			if(pEmitter)
			{
				IEntity *pTarget = gEnv->pEntitySystem->GetEntity(pLink->entityId);
				ParticleTarget target;
				target.bPriority = true;
				target.bTarget = true;
				target.fRadius = 0.1;
				target.vTarget = pTarget->GetPos();
				target.vVelocity = Vec3(ZERO);
				pEmitter->SetTarget(target);

			}
			pLink = pLink->next;
		}

		/*primitives::box  Primitivebox;
		PhysSkipList skipList;
		skipList.clear();
		hit_dist = gEnv->pPhysicalWorld->PrimitiveWorldIntersection(sphere.type, &sphere, Vec3Constants<float>::fVec3_Zero, ent_all, &pContact, 0, (rwi_ignore_noncolliding | rwi_stop_at_pierceable), 0, 0, 0, !skipList.empty() ? &skipList[0] : NULL, skipList.size());*/
		/*if(FirstInit)
		{
			IEntity *thisEntity = GetEntity();
			AABB aabb;
			thisEntity->GetLocalBounds(aabb);
			IEntityAreaProxyPtr proxy = crycomponent_cast<IEntityAreaProxyPtr>(GetEntity()->CreateProxy(ENTITY_PROXY_AREA));
			if (proxy)
			{
				proxy->SetID(GetEntityId());
				proxy->SetSphere(aabb.GetCenter(),(float)(aabb.GetRadius() * 0.8));
				proxy->AddEntity(GetEntityId());
			}
			FirstInit = false;
		}
		AABB aabb;
		GetEntity()->GetLocalBounds(aabb);
		IPhysicalEntity **nearbyEntities;
		int num=gEnv->pPhysicalWorld->GetEntitiesInBox(aabb.min, aabb.max, nearbyEntities, ent_all);
		if(isTriggered && num == 1)
		{
			CryLogAlways("Leave PressureTrigger Sphere");
			isTriggered = false;
			return;
		}
		else if(num > 1 && !isTriggered)
		{
			CryLogAlways("Enter PressureTrigger Sphere");
			isTriggered = true;
			ProcessTrigger();
			return;
		}*/
	}
}


void CPressureTrigger::HandleEvent(const SGameObjectEvent& gameObjectEvent)
{

}

void CPressureTrigger::ProcessEvent(SEntityEvent& entityEvent)
{
	switch (entityEvent.event)
	{
	case ENTITY_EVENT_COLLISION:
		{
			/*AABB aabb;
			GetEntity()->GetWorldBounds(aabb);
			EventPhysCollision *pCollision = (EventPhysCollision *)(entityEvent.nParam[0]);
			if(pCollision->pt.z > aabb.GetCenter().z && !isTriggered)
			{
				CryLogAlways("Enter PressureTrigger Sphere");
				isTriggered = true;
				ProcessTrigger();
			}*/
			//CryLogAlways("Collision");
			break;
		}
	case ENTITY_EVENT_ENTERAREA:
		{
			//CryLogAlways("Enter PressureTrigger Sphere");
			//isTriggered = true;
			//ProcessTrigger();
			break;
		}
	case ENTITY_EVENT_LEAVEAREA:
		{
			//CryLogAlways("Leave PressureTrigger Sphere");
			//isTriggered = false;
			break;
		}
	default:
		{
			/*if(isTriggered)
			{
				CryLogAlways("Leave PressureTrigger Sphere");
				isTriggered = false;
				PressureTimer = 0;
			}*/
		}
	}
}

void CPressureTrigger::GetMemoryUsage(ICrySizer *pSizer) const
{

}


void CPressureTrigger::ReadScript()
{

}

void CPressureTrigger::ProcessTrigger()
{
	IEntity *pThisEntity = GetEntity();
	IEntityLink *pLink = pThisEntity->GetEntityLinks();
	while(pLink)
	{
		IEntity *pLinkEntity = gEnv->pEntitySystem->GetEntity(pLink->entityId);
		if(pLinkEntity)
		{
			ProcessLinkedEntity(pLinkEntity);
		}
		pLink = pLink->next;
	}
}

void CPressureTrigger::ProcessLinkedEntity(IEntity *pEntity)
{
	IEntityClass *pClass = pEntity->GetClass();

	if(pClass == preload->pDestroyableObjectClass)
	{
		Script::CallMethod(pEntity->GetScriptTable(),"Explode");
	}
	else if(pClass == preload->pForceField)
	{
		CForceField* pObject = static_cast<CForceField*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pEntity->GetId(), "ForceField"));
		if(pObject->IsEnabled())
			pObject->EnableForceField(false);
		else
			pObject->EnableForceField(true);
	}
}