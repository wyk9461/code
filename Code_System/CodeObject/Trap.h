#ifndef _Trap_H_
#define  _Trap_H_


#include <IGameObject.h>


enum TrapEvent
{
	eEvent_TrapEvent,
};


class CTrap : public CGameObjectExtensionHelper<CTrap, IGameObjectExtension>
{
public:
	CTrap();
	virtual ~CTrap();

	// IGameObjectExtension
	virtual bool Init(IGameObject * pGameObject);		//初始化
	virtual void InitClient(int channelId) {};
	virtual void PostInit(IGameObject * pGameObject);		//放置后初始化
	virtual void PostInitClient(int channelId) {};
	virtual bool ReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params);		//重置时发生的事件
	virtual void PostReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params) {}
	virtual bool GetEntityPoolSignature(TSerialize signature);
	virtual void Release();		//析构
	virtual void FullSerialize(TSerialize ser);
	virtual bool NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) { return false; };
	virtual void PostSerialize() {};
	virtual void SerializeSpawnInfo(TSerialize ser) {}
	virtual ISerializableInfoPtr GetSpawnInfo() { return 0; }
	virtual void Update(SEntityUpdateContext& ctx, int slot);		//更新
	virtual void HandleEvent(const SGameObjectEvent& gameObjectEvent);		//处理事件
	virtual void ProcessEvent(SEntityEvent& entityEvent);		//处理事件
	virtual void SetChannelId(uint16 id) {};
	virtual void SetAuthority(bool auth) {};
	virtual void PostUpdate(float frameTime) { CRY_ASSERT(false); }
	virtual void PostRemoteSpawn() {};
	virtual void GetMemoryUsage(ICrySizer *pSizer) const;

	void ReadScript();
	void Disable();
	static void SetCovHit(const HitInfo &info);
	// ~IGameObjectExtension




private:

	float totalDamage;
	EntityId EnemyId;

	float lostTime;
	float timer;

	float Radius;
	float movementDamage;
	float slowDownScale;
	float dedicationDamageScale;
	float treatment;
	float stabilityLost;
	float stabilityGain;
	float damageScale;

	bool isDataLeakage;
	bool isSlowDown;
	bool isDedication;
	bool isRecycle;
	bool isDamageCov;

	uint8 gruntFactionID;
	uint8 playerFactionID;
	uint8 civilianFactionID;

	char* faction;

	struct HealthAndDie
	{
		float health;
		bool isDead;

		HealthAndDie(float h, bool b)
		{
			health = h;
			isDead = b;
		}
		HealthAndDie& operator=(const HealthAndDie& healthAndDie)
		{
			if (this == &healthAndDie)
			{
				return *this;
			}
			health = healthAndDie.health;
			isDead = healthAndDie.isDead;
			return *this;
		};
	};
	std::map<EntityId,HealthAndDie> ActorsWithinArea;

	void CreateHit(EntityId, EntityId, float);

	void SlowDown();
	void DataLeakage();
	void DamageCov();
	void Dedication();
	void Recycle(float);

	void SpawnIndicateParticle();

	float angle;
	IEntity *pEffect;
	static HitInfo covHit;
	static bool HasProcessedHit;
	static IParticleEffect *pLeakEffect;
	static IParticleEffect *pDamageCovEffect;
};


#endif