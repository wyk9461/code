#include "StdAfx.h"
#include "AISpawnPoint.h"
#include "IAIActor.h"
#include "Code_System/SpawnedAIManager.h"
#include "IAIObjectManager.h"
#include "IAIObject.h"


CAISpawnPoint::CAISpawnPoint()
:	HasParticle(false)
,	SpawnCount(0)
,	MaxSpawn(3)
,	TickTimer(0)
,	SpawnDelay(5)
,	pEffect(NULL)
,	AIArchetype("")
,	pStronghold(NULL)
,	eliminateCount(0)
,	isEnabled(false)
,	Level(1)
,	requiredLevel(1)
,	LevelCap(3)
,	tick_timer_for_dynamic_spawn(0.f)
,	ShouldFindDynamicSpawnPoint(true)
,	FailCount(0)
{

}

CAISpawnPoint::~CAISpawnPoint()
{

}

bool CAISpawnPoint::Init( IGameObject * pGameObject )
{
	SetGameObject(pGameObject);
	GetGameObject()->EnablePhysicsEvent(true, eEPE_AllLogged);
	pEffect=NULL;
	HasParticle=false;
	return true;
}

void CAISpawnPoint::PostInit( IGameObject * pGameObject )
{
	ReadScript();
	GetGameObject()->EnableUpdateSlot(this,0);
	GetGameObject()->SetUpdateSlotEnableCondition(this,0,eUEC_Always);

	g_pGame->GetSpawnedAIManager()->RemoveSpawner(GetEntityId());
	//AddToManager();
}

bool CAISpawnPoint::ReloadExtension( IGameObject * pGameObject, const SEntitySpawnParams &params )
{
	ResetGameObject();
	return true;
}

bool CAISpawnPoint::GetEntityPoolSignature( TSerialize signature )
{
	return true;
}

void CAISpawnPoint::Release()
{
	//g_pGame->GetSpawnedAIManager()->RemoveSpawner(GetEntityId());
	IParticleEmitter *pEmitter=GetEntity()->GetParticleEmitter(10);
	if(pEmitter)
		gEnv->pParticleManager->DeleteEmitter(pEmitter);
	/*if(pEffect)
		gEnv->pParticleManager->DeleteEffect(pEffect);*/
	delete this;
}

void CAISpawnPoint::FullSerialize( TSerialize ser )
{

}

void CAISpawnPoint::Update( SEntityUpdateContext& ctx, int slot )
{
	if(!pStronghold)
	{
		IEntityLink *pLink = GetEntity()->GetEntityLinks();
		if(pLink)
		{
			IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pLink->entityId);
			if(pEntity)
			{
				CStronghold* phold = static_cast<CStronghold*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pLink->entityId, "Stronghold"));
				if(phold)
				{
					pStronghold = phold;
					pStronghold->AddToSpawnPointSet(GetEntityId());
				}
			}
		}
	}


	if(!g_pGame->GetIGameFramework()->IsGamePaused() && !g_pGame->GetIGameFramework()->IsEditing())
	{
		/*if(tick_timer_for_dynamic_spawn > 2 && DynamicSpawnPosDeque.size() < 8)
		{
			Vec3 Pos = Instance.GetBestResult().vPos;
			if(Pos != Vec3(ZERO))
			{
				ShouldFindDynamicSpawnPoint = true;
				DynamicSpawnPosDeque.push_back(Pos);
			}
			else
				++FailCount;
			if(FailCount > 6)
			{
				FailCount = 0;
				ShouldFindDynamicSpawnPoint = true;
			}
			tick_timer_for_dynamic_spawn = 0;
		}
		tick_timer_for_dynamic_spawn += ctx.fFrameTime;*/

		UpdateActorStatus();

		if(TickTimer >= SpawnDelay && isEnabled)
		{
			if(SpawnCount < MaxSpawn)
			{
				ProcessSpawn(ctx.fFrameTime);
				/*if(isAI)
				{
					Vec3 &new_pos = pStronghold->GetRandomPos();
					if(new_pos != Vec3(ZERO))
						GetEntity()->SetPos(new_pos);
				}*/
			}
			TickTimer = 0;
		}
		TickTimer+=ctx.fFrameTime;
		/*if( !isEnabled )
			TickTimer = 0;*/
	}
	else
	{
		ClearSpawnStatus();
		isEnabled = false;
	}
}

void CAISpawnPoint::HandleEvent( const SGameObjectEvent& gameObjectEvent )
{

}

void CAISpawnPoint::ProcessEvent( SEntityEvent& entityEvent )
{

}

void CAISpawnPoint::GetMemoryUsage( ICrySizer *pSizer ) const
{

}


void CAISpawnPoint::ProcessSpawn(const float frameTime)
{
	Vec3 SpawnPos;
	if(DynamicSpawnPosDeque.empty())
		SpawnPos = GetEntity()->GetPos();
	else
	{
		int index = cry_rand() % DynamicSpawnPosDeque.size();
		SpawnPos = DynamicSpawnPosDeque[index];
	}
	/*SpawnPos.x += BiRandom(1.5);
	SpawnPos.y +=BiRandom(1.5);*/

	//SpawnPos = Result.vPos;

	IPhysicalEntity *pSelf = GetEntity()->GetPhysics();
	ray_hit hit;
	gEnv->pPhysicalWorld->RayWorldIntersection(SpawnPos, Vec3(0,0,-1)*30, ent_all, rwi_pierceability_mask | rwi_colltype_any, &hit, 1, &pSelf, 1);
	SpawnPos.z = hit.pt.z;
	char cat_str[3];
	_itoa(Level,cat_str,3);
	IEntityArchetype *pArchetype = gEnv->pEntitySystem->LoadEntityArchetype(AIArchetype + cat_str);
	if(pArchetype)
	{
		SEntitySpawnParams params;
		params.vPosition=SpawnPos;
		params.qRotation = GetEntity()->GetRotation();
		params.sName = pArchetype->GetName();
		params.pClass=pArchetype->GetClass();
		params.pPropertiesTable=pArchetype->GetProperties();
		params.pArchetype = pArchetype;
		IEntity *Spawned=gEnv->pEntitySystem->SpawnEntity(params);
		IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Code_System.WarpSpawn");
		if(pEffect)
			pEffect->Spawn(true,IParticleEffect::ParticleLoc(SpawnPos,Vec3(0,0,1)));

		const IAIObject* pAI = Spawned->GetAI();
		if(pAI)
		{
			CActor *pActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(Spawned->GetId());
			Actors.push_back(pActor);
			StrongholdManager::Instance().SetStrongholdTargets(pStronghold->GetEntityId(), Spawned, eAT_SpawnedAI);
		}
		else
		{
			pSpawnedEnity = Spawned;
			Spawned->AddEntityLink("SpawnedEntity",GetEntity()->GetId());
			IEntityLink *pLink = Spawned->GetEntityLinks();
			//GetEntity()->AttachChild(pSpawnedEnity);
			isAI = false;
			isEnabled = false;
		}


		++SpawnCount;


		/*AIBindParams AIparams;
		AIparams.AI_Id = SpawnAI->GetId();
		AIparams.SpawnerId = GetEntityId();
		g_pGame->GetSpawnedAIManager()->AddToList(AIparams);
		g_pGame->GetSpawnedAIManager()->AddSpawnCount(GetEntityId());*/
		/*
		if(pAIObject)
		{
			static_cast<IAIActor*>(pAIObject->CastToIAIActor())->SetSpeed(AISPEED_SPRINT);
			IAISignalExtraData * exd = gEnv->pAISystem->CreateSignalExtraData();
			exd->point = Vec3(55,55,15);
			static_cast<IAIActor*>(pAIObject->CastToIAIActor())->SetSignal(AISIGNAL_DEFAULT,"ACT_GOTO", SpawnAI,exd);
		}*/

		/*//works also with the pAISystem from the GlobalEnvrionment
		if(pAIObject)
		{
			IAISignalExtraData * exd = gEnv->pAISystem->CreateSignalExtraData();
			exd->point = Vec3(55,55,15);
			gEnv->pAISystem->SendSignal(SIGNALFILTER_SENDER,AISIGNAL_PROCESS_NEXT_UPDATE,"ACT_GOTO",pAIObject,exd);
		}*/

		/*IAIObject * pAIObject = SpawnAI->GetAI();
		if(pAIObject)
		{
			AIObjectParams op(AIOBJECT_TARGET);
			IAIObject * pAIObjectDummy = static_cast<IAIObjectManager*>(gEnv->pAISystem->GetAIObjectManager())->CreateAIObject(op);
			pAIObjectDummy->SetPos(Vec3(55,55,15));
			pAIObject->CastToIPipeUser()->SelectPipe(AIGOALPIPE_HIGHPRIORITY, "_first_", pAIObjectDummy);
		}*/
	}
}

void CAISpawnPoint::ReadScript()
{
	SmartScriptTable root=GetEntity()->GetScriptTable();
	SmartScriptTable props;
	char *Arche;
	root->GetValue("Properties",props);
	props->GetValue("SpawnDelay",SpawnDelay);
	props->GetValue("MaxSpawn",MaxSpawn);
	props->GetValue("sAIArcheType",Arche);
	props->GetValue("RequiredLevel",requiredLevel);
	props->GetValue("LevelCap",LevelCap);
	AIArchetype = Arche;
}

void CAISpawnPoint::AddToManager()
{
	SpawnedAIManager *Manager = g_pGame->GetSpawnedAIManager();
	if(Manager)
	{
		Manager->AddSpawnerData(SpawnCount,MaxSpawn,GetEntityId());
	}
}

void CAISpawnPoint::UpdateActorStatus()
{
	if(!Actors.empty())
	{
		if(ShouldFindDynamicSpawnPoint)
		{
			int index = cry_rand() % Actors.size();
			FindDynamicSpawnPoint(Actors[index]->GetEntity());
			ShouldFindDynamicSpawnPoint = false;
		}

		std::deque<CActor*>::iterator iter = Actors.begin();
		std::deque<CActor*>::iterator end = Actors.end();
		while(iter != end)
		{
			CActor *pActor = *iter;
			if(pActor->IsDead())
			{
				SendEliminateMessage(pActor);
				if(++eliminateCount >= MaxSpawn)
					SendClearMessage();
				Actors.erase(iter++);
			}
			else
			{
				++iter;
			}
		}
	}
}

void CAISpawnPoint::ClearSpawnStatus()
{
	SpawnCount = 0;
	eliminateCount = 0;
	Actors.clear();
}

void CAISpawnPoint::SendEliminateMessage(CActor *pActor)
{	DynamicSpawnPosDeque.erase(DynamicSpawnPosDeque.begin(), DynamicSpawnPosDeque.begin()+5);
	if(pStronghold)
	{
		pStronghold->ReceiveEnemyEliminateMessage(pActor->GetCapability());
	}
}

void CAISpawnPoint::SendClearMessage()
{
	DynamicSpawnPosDeque.erase(DynamicSpawnPosDeque.begin(), DynamicSpawnPosDeque.begin()+5);
	//Instance.Cancel();
	if(pStronghold)
	{
		pStronghold->ReceiveClearMessage();
	}
}

void CAISpawnPoint::FindDynamicSpawnPoint(IEntity *pAI)
{
	static const char *sQueryName = "FindSpawnPoint";
	ITacticalPointSystem *pTPS = gEnv->pAISystem->GetTacticalPointSystem();
	int iQueryId = pTPS->GetQueryID( sQueryName );

	QueryContext context;
	context.actorPos = GetEntity()->GetPos();
	context.pAIActor = CastToIAIActorSafe(pAI->GetAI());
	if(context.pAIActor)
	{
		Instance.SetContext(context);
		Instance.SetQueryID(iQueryId);
		Instance.Execute(eTPQT_GENERATOR);
	}
}