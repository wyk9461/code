#ifndef _ForceDevice_H_
#define _ForceDevice_H_


#include <IGameObject.h>


enum ECForceDeviceEventListenerGameObjectEvent
{
	eForceDeviceListenerGameObjectEvent_Test = 20,
};


class CForceDevice : public CGameObjectExtensionHelper<CForceDevice, IGameObjectExtension>
{
public:
	CForceDevice();
	virtual ~CForceDevice();

	// IGameObjectExtension
	virtual bool Init(IGameObject * pGameObject);
	virtual void InitClient(int channelId) {};
	virtual void PostInit(IGameObject * pGameObject);
	virtual void PostInitClient(int channelId) {};
	virtual bool ReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params);
	virtual void PostReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params) {}
	virtual bool GetEntityPoolSignature(TSerialize signature);
	virtual void Release();
	virtual void FullSerialize(TSerialize ser);
	virtual bool NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) { return false; };
	virtual void PostSerialize() {};
	virtual void SerializeSpawnInfo(TSerialize ser) {}
	virtual ISerializableInfoPtr GetSpawnInfo() { return 0; }
	virtual void Update(SEntityUpdateContext& ctx, int slot);
	virtual void HandleEvent(const SGameObjectEvent& gameObjectEvent);
	virtual void ProcessEvent(SEntityEvent& entityEvent);
	virtual void SetChannelId(uint16 id) {};
	virtual void SetAuthority(bool auth) {};
	virtual void PostUpdate(float frameTime) { CRY_ASSERT(false); }
	virtual void PostRemoteSpawn() {};
	virtual void GetMemoryUsage(ICrySizer *pSizer) const;

	// ~IGameObjectExtension
	void ReadScript();

private:
	AABB aabb;

	float timer;
	int hitTimes;
	int hitTimesLimit;
	float explosionDamage;
	float exploRadius;
	float force;
	float hitTimeInterval;
	int count;
	bool isMother;
	bool enable;

	static IParticleEffect *pEffect;

	std::vector<EntityId> memberId;
	//void EraseDestroyedOnes();
	//void SpawnNewOnes(IEntity*);
};


#endif