#include "StdAfx.h"
#include "RayMine.h"
#include "ParticleParams.h"
#include "Code_System/CommonUsage.h"

int CRayMine::rayFlags = rwi_pierceability_mask | rwi_colltype_any;

CRayMine::CRayMine() 
	:	effectRadius(0)
	,	pEntity(0)
	,	damage(300)
	{

	}

CRayMine::~CRayMine()
{

}


bool CRayMine::Init(IGameObject * pGameObject)
{
	SetGameObject(pGameObject);
	GetGameObject()->EnablePhysicsEvent(true, eEPE_AllLogged);
	IAIObject *pAI = GetEntity()->GetAI();
	return true;
}

void CRayMine::PostInit(IGameObject * pGameObject)
{
	ReadScript();
	GetGameObject()->EnableUpdateSlot(this, 0);
	GetGameObject()->SetUpdateSlotEnableCondition(this, 0, eUEC_Always);
	IParticleEffect* pParticleEffect = gEnv->pParticleManager->FindEffect("Code_System.CodeObject.LeakHole_Indicator");
	GetEntity()->LoadParticleEmitter(10,pParticleEffect);

	if(IParticleEmitter *pEmitter = GetEntity()->GetParticleEmitter(10))
	{
		pEmitter->SetLocation(IParticleEffect::ParticleLoc(GetEntity()->GetPos(),Vec3(0,0,1)));
		SpawnParams Emitter_Params;
		pEmitter->GetSpawnParams(Emitter_Params);
		Emitter_Params.eAttachType = GeomType_None;
		Emitter_Params.fSpeedScale = effectRadius / 5;
		pEmitter->SetSpawnParams(Emitter_Params);
	}
}

bool CRayMine::ReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params)
{
	ResetGameObject();
	return true;
}

bool CRayMine::GetEntityPoolSignature(TSerialize signature)
{
	return true;
}

void CRayMine::Release()
{
	IParticleEmitter *pEmitter = GetEntity()->GetParticleEmitter(10);
	if (pEmitter)
		gEnv->pParticleManager->DeleteEmitter(pEmitter);
	delete this;
}

void CRayMine::FullSerialize(TSerialize ser)
{

}

void CRayMine::Update(SEntityUpdateContext& ctx, int slot)
{
	if(!g_pGame->GetIGameFramework()->IsGamePaused() && !g_pGame->GetIGameFramework()->IsEditing())
	{
		//Code Note: Get Up Direction
		Matrix34 mat = GetEntity()->GetLocalTM();
		Quat rot = GetEntity()->GetRotation();

		//Vec3 UpDir = Vec3(-mat.m20,-mat.m21,mat.m22);

		Vec3 UpDir = rot.GetColumn2().GetNormalized();

		if(IParticleEmitter *pEmitter = GetEntity()->GetParticleEmitter(10))
		{
			pEmitter->SetLocation(IParticleEffect::ParticleLoc(GetEntity()->GetPos(),UpDir));
		}
		if(GetEntity()->IsActive())
		{
			Vec3 pos = GetEntity()->GetPos();
			IParticleEmitter *pEmitter = GetEntity()->GetParticleEmitter(10);
			pEmitter->Activate(true);
			IPhysicalEntity* pSelf = GetEntity()->GetPhysics();
			if (pSelf)
			{
				ray_hit hit;
				gEnv->pPhysicalWorld->RayWorldIntersection(pos, UpDir*effectRadius, ent_rigid | ent_living | ent_sleeping_rigid | ent_independent, rayFlags, &hit, 1, &pSelf, 1);
				if (hit.pCollider)
				{
					pEntity = (IEntity*)hit.pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);
					if (pEntity)
					{
						CommonUsage::CreateExplosion(g_pGame->GetClientActorId(), GetEntity()->GetPos(), damage, effectRadius, damage * 3);
						gEnv->pEntitySystem->RemoveEntity(GetEntityId(),false);
					}
					pEmitter->Activate(false);
					Enabled = false;
				}
			}
		}
	}
}


void CRayMine::HandleEvent(const SGameObjectEvent& gameObjectEvent)
{

}

void CRayMine::ProcessEvent(SEntityEvent& entityEvent)
{

}

void CRayMine::GetMemoryUsage(ICrySizer *pSizer) const
{

}


void CRayMine::ReadScript()
{
	SmartScriptTable root = GetEntity()->GetScriptTable();
	SmartScriptTable props,fp;
	root->GetValue("Properties", props);
	props->GetValue("FunctionParams", fp);
	fp->GetValue("Radius", effectRadius);
	fp->GetValue("Damage", damage);
	if(IParticleEmitter *pEmitter = GetEntity()->GetParticleEmitter(10))
	{
		pEmitter->SetLocation(IParticleEffect::ParticleLoc(GetEntity()->GetPos(),Vec3(0,0,1)));
		SpawnParams Emitter_Params;
		pEmitter->GetSpawnParams(Emitter_Params);
		Emitter_Params.eAttachType = GeomType_None;
		Emitter_Params.fSpeedScale = effectRadius / 5;
		pEmitter->SetSpawnParams(Emitter_Params);
	}
}