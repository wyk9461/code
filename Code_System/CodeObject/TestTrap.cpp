#include "StdAfx.h"
#include "TestTrap.h"

namespace STestTrap
{
	void RegisterEvents( IGameObjectExtension& goExt, IGameObject& gameObject )
	{
		const int events[] = 
		{	
			eGFE_ScriptEvent,
			eTestTrapListenerGameObjectEvent_Test,
		};

		gameObject.UnRegisterExtForEvents( &goExt, NULL, 0 );
		gameObject.RegisterExtForEvents( &goExt, events, (sizeof(events) / sizeof(int)) );
	}
}

CTestTrap::CTestTrap()
{

}

CTestTrap::~CTestTrap()
{

}


bool CTestTrap::Init( IGameObject * pGameObject )
{
	SetGameObject(pGameObject);
	GetGameObject()->EnablePhysicsEvent(true, eEPE_AllLogged);
	IAIObject *pAI=GetEntity()->GetAI();
	return true;
}

void CTestTrap::PostInit( IGameObject * pGameObject )
{
	/*GetEntity()->GetLocalBounds(aabb);
	IEntityAreaProxyPtr proxy=crycomponent_cast<IEntityAreaProxyPtr>(GetEntity()->CreateProxy(ENTITY_PROXY_AREA));
	if(proxy)
	{
		proxy->SetID(GetEntityId());
		proxy->SetSphere(aabb.GetCenter(),10);
		proxy->AddEntity(GetEntityId());
	}*/
	ReadScript();
	STestTrap::RegisterEvents( *this, *pGameObject );
	GetGameObject()->EnableUpdateSlot(this,0);
	GetGameObject()->SetUpdateSlotEnableCondition(this,0,eUEC_Always);
	AIObject = new FlyingAI;
	AIObject->Init(GetEntity());
}

bool CTestTrap::ReloadExtension( IGameObject * pGameObject, const SEntitySpawnParams &params )
{
	ResetGameObject();
	AIObject->Init(GetEntity());
	STestTrap::RegisterEvents( *this, *pGameObject );
	return true;
}

bool CTestTrap::GetEntityPoolSignature( TSerialize signature )
{
	return true;
}

void CTestTrap::Release()
{
	IParticleEmitter *pEmitter=GetEntity()->GetParticleEmitter(10);
	if(pEmitter)
		gEnv->pParticleManager->DeleteEmitter(pEmitter);
	delete this;
}

void CTestTrap::FullSerialize( TSerialize ser )
{

}

void CTestTrap::Update( SEntityUpdateContext& ctx, int slot )
{
	if(!gEnv->IsEditing())
	{
		IEntity *pEntity = GetEntity();
		IEntity *pPlayer = gEnv->pEntitySystem->GetEntity(g_pGame->GetClientActorId());
		CActor *pActor = (CActor*)g_pGame->GetIGameFramework()->GetClientActor();
		AIObject->Update(ctx.fFrameTime);
		/*Vec3 nextPos = pPlayer->GetPos();
		nextPos.z += 1.8;*/
		if(pActor->IsThirdPerson())
		{
			//DynamicNavMesh::Instance().ClearMesh();
			//AIObject->ForceFindPath(pEntity,nextPos);
			//AIObject->MoveAlongPath(pEntity, nextPos);
		}
		else
		{
			/*DynamicNavMesh::Instance().CreateNavMesh(pActor->GetEntity()->GetPos(),8,40,ctx.fFrameTime);
			DynamicNavMesh::Instance().DebugMesh();
			PathFinder::DebugInstance().DebugPath();*/
		}
		//AIObject->DebugPath();
		//AIObject->DebugDraw();
	}
}


void CTestTrap::HandleEvent( const SGameObjectEvent& gameObjectEvent )
{
	const uint32 eventId = gameObjectEvent.event;
	void* pParam = gameObjectEvent.param;
	if ((eventId == eGFE_ScriptEvent) && (pParam != NULL))
	{
		const char* eventName = static_cast<const char*>(pParam);
		if (strcmp(eventName, "OnDestroy") == 0)
		{
			
		}
	}
	else if(eventId == eTestTrapListenerGameObjectEvent_Test)
	{

	}
}

void CTestTrap::ProcessEvent( SEntityEvent& entityEvent )
{
	switch(entityEvent.event)
	{
		case ENTITY_EVENT_COLLISION:
		{
			AIObject->ProcessCollision();
		}
		case  ENTITY_EVENT_ENTERAREA:
		{
			//CryLogAlways("LaLaLa");
		}
		break;
	}
}

void CTestTrap::GetMemoryUsage( ICrySizer *pSizer ) const
{

}


void CTestTrap::ReadScript()
{

}