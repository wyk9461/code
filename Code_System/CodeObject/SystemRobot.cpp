#include "StdAfx.h"
#include "SystemRobot.h"
#include "Code_System/CodeExclusion.h"

CSystemRobot::CSystemRobot()
{

}

CSystemRobot::~CSystemRobot()
{

}

bool CSystemRobot::Init( IGameObject * pGameObject )
{
	SetGameObject(pGameObject);
	GetGameObject()->EnablePhysicsEvent(true, eEPE_AllLogged);
	IAIObject *pAI=GetEntity()->GetAI();
	timer = 0;
	fast_delete=false;
	delete_phase=true;
	SpawnSystem *pSpawnSystem=g_pGame->GetSpawnSystem();
	std::list<EntityId> &SpawnEntitySet = pSpawnSystem->GetSpawnEntitySet();
	if(!SpawnEntitySet.empty())
	{
		int max=SpawnEntitySet.size();
		if(max <= 30)
		{
			DeleteTask.assign(SpawnEntitySet.begin(),SpawnEntitySet.end());
			SpawnEntitySet.clear();
			//std::vector<EntityId>().swap(SpawnEntitySet);
		}
		else
		{
			std::list<EntityId>::iterator back=SpawnEntitySet.begin();
			std::advance(back,15);
			DeleteTask.assign(SpawnEntitySet.begin(),back);
			SpawnEntitySet.erase(SpawnEntitySet.begin(),back);
		}
		iter=DeleteTask.begin();
	}
	std::vector<SEntitySpawnParams> &DeleteRecoveryInfo = g_pGame->GetDeleteSystem()->GetDeleteRecoveryInfo();
	if(!DeleteRecoveryInfo.empty())
	{
		int max = DeleteRecoveryInfo.size();
		if(max < 30)
		{
			RecoveryTask.assign(DeleteRecoveryInfo.begin(),DeleteRecoveryInfo.end());
			DeleteRecoveryInfo.clear();
		}
		else
		{
			std::vector<SEntitySpawnParams>::iterator back=DeleteRecoveryInfo.begin()+15;
			RecoveryTask.assign(DeleteRecoveryInfo.begin(),back);
			DeleteRecoveryInfo.erase(DeleteRecoveryInfo.begin(),back);
		}
		r_iter = RecoveryTask.begin();
	}
	return true;
}

void CSystemRobot::PostInit( IGameObject * pGameObject )
{
	/*IEntityAreaProxyPtr proxy=crycomponent_cast<IEntityAreaProxyPtr>(GetEntity()->CreateProxy(ENTITY_PROXY_AREA));
	if(proxy)
	{
		proxy->SetID(GetEntityId());
		proxy->AddEntity(GetEntityId());
	}*/
	GetGameObject()->EnableUpdateSlot(this,0);
	GetGameObject()->SetUpdateSlotEnableCondition(this,0,eUEC_Always);
	IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Code_System.Scans");
	//pEffect->Spawn(false,IParticleEffect::ParticleLoc(GetEntity()->GetPos(),Vec3(0,0,1)));
	GetEntity()->LoadParticleEmitter(10,pEffect);
	IParticleEmitter *pEmitter=GetEntity()->GetParticleEmitter(10);
	IParticleEffect *pSpawnEffect=gEnv->pParticleManager->FindEffect("Code_System.WarpSpawn");
	pSpawnEffect->Spawn(true,IParticleEffect::ParticleLoc(GetEntity()->GetPos()));
	pEmitter->Activate(false);
}

bool CSystemRobot::ReloadExtension( IGameObject * pGameObject, const SEntitySpawnParams &params )
{
	ResetGameObject();
	timer = 0;
	fast_delete=false;
	delete_phase=true;
	//STestTrap::RegisterEvents( *this, *pGameObject );
	return true;
}

bool CSystemRobot::GetEntityPoolSignature( TSerialize signature )
{
	return true;
}

void CSystemRobot::Release()
{
	IParticleEmitter *pEmitter=GetEntity()->GetParticleEmitter(10);
	if(pEmitter)
		gEnv->pParticleManager->DeleteEmitter(pEmitter);
	if(!DeleteTask.empty())
	{
		std::list<EntityId> &SpawnEntitySet = g_pGame->GetSpawnSystem()->GetSpawnEntitySet();
		SpawnEntitySet.insert(SpawnEntitySet.end(),iter,DeleteTask.end());
		DeleteTask.clear();
	}
	std::vector<EntityId>().swap(DeleteTask);
	if(!RecoveryTask.empty())
	{
		std::vector<SEntitySpawnParams> &DeleteRecoveryInfo = g_pGame->GetDeleteSystem()->GetDeleteRecoveryInfo();
		DeleteRecoveryInfo.insert(DeleteRecoveryInfo.end(),r_iter,RecoveryTask.end());
		RecoveryTask.clear();
	}
	std::vector<SEntitySpawnParams>().swap(RecoveryTask);
	delete this;
	GetEntity();
}

void CSystemRobot::FullSerialize( TSerialize ser )
{

}

void CSystemRobot::Update( SEntityUpdateContext& ctx, int slot )
{
	if(!gEnv->IsEditing())
	{
		if(!DeleteTask.empty())
		{
			if(iter !=DeleteTask.end() && delete_phase)
			{
				GetEntity()->SetScale(Vec3(1,1,1));
				AABB aabb;
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(*iter);
				if(pEntity)
				{
					IParticleEmitter *pEmitter=GetEntity()->GetParticleEmitter(10);
					pEntity->GetWorldBounds(aabb);
					Vec3 orin_pos = GetEntity()->GetPos();
					Vec3 dest_pos=aabb.GetCenter();
					dest_pos.z += aabb.GetRadius();
					float speed = 3 / (1 / ctx.fFrameTime);
					Quat quat=Quat::CreateRotationVDir((aabb.GetCenter()-orin_pos).normalize());
					GetEntity()->SetRotation(quat);
					bool canDelete=false;
					if(!fast_delete)
						canDelete = MoveToDestination(orin_pos,dest_pos,speed,1.2);
					if(canDelete || fast_delete)
					{
						pEmitter->Activate(true);
						timer+=ctx.fFrameTime;
						if(timer > 2 || fast_delete)
						{
							pEmitter->Activate(false);
							g_pGame->GetDeleteSystem()->ProcessDeleteEntity(pEntity,true);
							timer=0;
							++iter;

							if(iter != DeleteTask.end())
							{
								orin_pos = aabb.GetCenter();
								IEntity *pNextEntity=gEnv->pEntitySystem->GetEntity(*iter);
								if(pNextEntity)
								{
									pNextEntity->GetWorldBounds(aabb);
									dest_pos=aabb.GetCenter();
									float length = (orin_pos-dest_pos).GetLengthFast();
									if(length < aabb.GetRadius()*1.5)
										fast_delete = true;
									else
										fast_delete = false;
								}
							}
						}
					}
				}
				else
					++iter;
			}
		}
		if(delete_phase && iter == DeleteTask.end())
		 {
		 	timer = 0;
		 	delete_phase=false;
		 }
		  
		 //Spawn Phase
		 if(!delete_phase && r_iter != RecoveryTask.end() && !RecoveryTask.empty())
		 {
		 	Vec3 orin_pos = GetEntity()->GetPos();
		 	float speed = 3 / (1 / ctx.fFrameTime);
		 	Vec3 dest_pos = r_iter->vPosition;
			dest_pos.z += 3;
			Quat quat=Quat::CreateRotationVDir((r_iter->vPosition-orin_pos).normalize());
			GetEntity()->SetRotation(quat);
		 	bool CanSpawn=MoveToDestination(orin_pos,dest_pos,speed,1.5);
		 	if(CanSpawn)
		 	{
		 		IParticleEmitter *pEmitter=GetEntity()->GetParticleEmitter(10);
		 		pEmitter->Activate(true);
		 		timer+=ctx.fFrameTime;
		 		if(timer > 2)
		 		{
		 			CodeExclusion::Instance().SystemSpawn(*r_iter);
		 			timer = 0;
					pEmitter->Activate(false);
		 			++r_iter;
		 		}
		 		/*Vec3 orin_pos_z_offset = GetEntity()->GetPos();
		 		IPhysicalEntity **nearbyEntities;
		 		int n=gEnv->pPhysicalWorld->GetEntitiesInBox(orin_pos-Vec3(2),orin_pos+Vec3(2),nearbyEntities,ent_rigid|ent_static);
		 		if(n >= 4)
		 		{
		 			IParticleEmitter *pEmitter=GetEntity()->GetParticleEmitter(10);
		 			pEmitter->Activate(false);
		 			++s_iter;
		 		}
		 		else if(n<4)
		 		{
		 			if(n == 1)
		 				size_offset += 0.5;
		 			n = gEnv->pPhysicalWorld->GetEntitiesInBox(orin_pos_z_offset-Vec3(3+size_offset),orin_pos_z_offset+Vec3(3+size_offset),nearbyEntities,ent_rigid|ent_static);
		 			int spawn_index = cry_rand32() % n;
		 			IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[spawn_index]);
		 			if(pEntity && strcmp(pEntity->GetClass()->GetName(),"BasicEntity") == 0)
		 			{
		 				IParticleEmitter *pEmitter=GetEntity()->GetParticleEmitter(10);
		 				pEmitter->Activate(true);
		 				AABB aabb;
		 				pEntity->GetLocalBounds(aabb);
		 				const char *aaaa = pEntity->GetName();
		 				CryLogAlways(aaaa);
		 				float volume = aabb.GetVolume();
		 				if(volume < 5)
		 				{
		 					SEntitySpawnParams params;
		 					Vec3 SpawnPos=orin_pos + BiRandom(Vec3(2,2,2));
		 					SpawnPos.z = gEnv->p3DEngine->GetTerrainElevation(orin_pos.x,orin_pos.y);
		 
		 					params.vPosition = SpawnPos;
		 					params.sName = pEntity->GetName();
		 					params.nFlags = pEntity->GetFlags();
		 					params.pClass = pEntity->GetClass();
		 					//params.pArchetype = pEntity->GetArchetype();
		 					params.pClass=pEntity->GetClass();
		 					SmartScriptTable pPropertiesTable;
		 					pEntity->GetScriptTable()->GetValue("Properties", pPropertiesTable);
		 					if(pPropertiesTable)
		 					{
		 						params.pPropertiesTable=pPropertiesTable;
		 						CodeExclusion::Instance().SystemSpawn(params);
		 						size_offset = 0;
		 					}
		 				}
		 				else
		 					size_offset+=0.2;
		 			}
		 			else
		 				size_offset+=0.2;
		 		}*/
		 	}
		 }
		 if(!delete_phase && r_iter == RecoveryTask.end())
		 {
			 timer += ctx.fFrameTime;
			 if(timer > 3)
			 {
				RecoveryTask.clear();
				timer = 0;
				GetEntity()->EnablePhysics(false);
				g_pGame->GetBasicRayCast()->SetScreenEntity(NULL);
				IParticleEffect *pSpawnEffect=gEnv->pParticleManager->FindEffect("Code_System.WarpSpawn");
				pSpawnEffect->Spawn(true,IParticleEffect::ParticleLoc(GetEntity()->GetPos()));
		 		g_pGame->GetDeleteSystem()->ProcessDeleteEntity(GetEntity(),true);		//SelfDestruction
			 }
		 }
	}
}


void CSystemRobot::HandleEvent( const SGameObjectEvent& gameObjectEvent )
{
	const uint32 eventId = gameObjectEvent.event;
	void* pParam = gameObjectEvent.param;
	if ((eventId == eGFE_ScriptEvent) && (pParam != NULL))
	{
		const char* eventName = static_cast<const char*>(pParam);
		if (strcmp(eventName, "OnDestroy") == 0)
		{

		}
	}
	/*else if(eventId == eTestTrapListenerGameObjectEvent_Test)
	{

	}*/
}

void CSystemRobot::ProcessEvent( SEntityEvent& entityEvent )
{
}

void CSystemRobot::GetMemoryUsage( ICrySizer *pSizer ) const
{

}

bool CSystemRobot::MoveToDestination(Vec3 &oringe_pos , Vec3 &dest_pos , float speed , float stop_length , float z_offset /* = 0 */)
{
	oringe_pos.z-=z_offset;
	float cur_length=(oringe_pos-dest_pos).GetLengthFast();
	if(cur_length > 20)
	{
		Vec3 newPos=dest_pos +BiRandom(Vec3(3,3,3));
		if(z_offset !=0)
			newPos.z=gEnv->p3DEngine->GetTerrainElevation(newPos.x,newPos.y)+z_offset;
		GetEntity()->SetPos(newPos);
		return false;
	}
	float test = cur_length / stop_length;
	if(cur_length < stop_length || (test  > 0.7 && test  < 1.3))
		return true;
	Vec3 new_dir=(dest_pos-oringe_pos).normalize();
	Vec3 newPos=oringe_pos + new_dir*speed;
	if(z_offset !=0)
		newPos.z=gEnv->p3DEngine->GetTerrainElevation(newPos.x,newPos.y)+z_offset;
	GetEntity()->SetPos(newPos);
	return false;
}