#ifndef _AISPAWNPOINT_H_
#define _AISPAWNPOINT_H_

#include "Stronghold.h"
#include "ITacticalPointSystem.h"

class CAISpawnPoint : public CGameObjectExtensionHelper<CAISpawnPoint, IGameObjectExtension>
{
public:
	CAISpawnPoint();
	virtual ~CAISpawnPoint();

	// IGameObjectExtension
	virtual bool Init( IGameObject * pGameObject );
	virtual void InitClient( int channelId ) {};
	virtual void PostInit( IGameObject * pGameObject );
	virtual void PostInitClient( int channelId ) {};
	virtual bool ReloadExtension( IGameObject * pGameObject, const SEntitySpawnParams &params );
	virtual void PostReloadExtension( IGameObject * pGameObject, const SEntitySpawnParams &params ) {}
	virtual bool GetEntityPoolSignature( TSerialize signature );
	virtual void Release();
	virtual void FullSerialize( TSerialize ser );
	virtual bool NetSerialize( TSerialize ser, EEntityAspects aspect, uint8 profile, int flags ) { return false; };
	virtual void PostSerialize() {};
	virtual void SerializeSpawnInfo( TSerialize ser ) {}
	virtual ISerializableInfoPtr GetSpawnInfo() {return 0;}
	virtual void Update( SEntityUpdateContext& ctx, int slot );
	virtual void HandleEvent( const SGameObjectEvent& gameObjectEvent );
	virtual void ProcessEvent( SEntityEvent& entityEvent );
	virtual void SetChannelId( uint16 id ) {};
	virtual void SetAuthority( bool auth ) {};
	virtual void PostUpdate( float frameTime ) { CRY_ASSERT(false); }
	virtual void PostRemoteSpawn() {};
	virtual void GetMemoryUsage( ICrySizer *pSizer ) const;

	// ~IGameObjectExtension

	void SetLevel(int _level){_level < LevelCap ? Level = _level : Level = LevelCap;}
	void SetEnable(const bool _enable){isEnabled = _enable;}
	void SendEliminateMessage(CActor *pActor);
	void SendClearMessage();
	void ClearSpawnStatus();
	const bool HasEliminateAll(){return eliminateCount >= MaxSpawn;}
	void ClearEntitySpawn(){SpawnCount = 0;pSpawnedEnity = NULL;}
	const bool isSpawnAI(){return isAI;}
private:
	bool HasParticle;
	bool isEnabled;
	bool isAI;
	unsigned int SpawnCount;
	unsigned int eliminateCount;
	int MaxSpawn;

	float TickTimer;
	float SpawnDelay;

	string AIArchetype;
	int Level;
	int requiredLevel;
	int LevelCap;

	CStronghold *pStronghold;

	IParticleEffect *pEffect;

	std::deque<CActor*> Actors;
	IEntity *pSpawnedEnity;

	float tick_timer_for_dynamic_spawn;
	int FailCount;

	CTacticalPointQueryInstance Instance;
	bool ShouldFindDynamicSpawnPoint;
	std::deque<Vec3> DynamicSpawnPosDeque;
	void ProcessSpawn(const float frameTime);
	void ReadScript();
	void AddToManager();
	void UpdateActorStatus();
	void FindDynamicSpawnPoint(IEntity *pAI);
};
#endif