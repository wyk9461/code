#include "StdAfx.h"
#include "LeakHole.h"
#include "ParticleParams.h"

int CLeakHole::rayFlags = rwi_pierceability_mask | rwi_colltype_any;

CLeakHole::CLeakHole() 
	:effectRadius(0)
	, pEntity(0)
	, drawDamage(0)
	, tick_timer(0)
	, Cd(30)
	{

	}

CLeakHole::~CLeakHole()
{

}


bool CLeakHole::Init(IGameObject * pGameObject)
{
	SetGameObject(pGameObject);
	GetGameObject()->EnablePhysicsEvent(true, eEPE_AllLogged);
	IAIObject *pAI = GetEntity()->GetAI();
	return true;
}

void CLeakHole::PostInit(IGameObject * pGameObject)
{
	ReadScript();
	GetGameObject()->EnableUpdateSlot(this, 0);
	GetGameObject()->SetUpdateSlotEnableCondition(this, 0, eUEC_Always);
	IParticleEffect* pParticleEffect = gEnv->pParticleManager->FindEffect("Code_System.CodeObject.LeakHole_Indicator");
	GetEntity()->LoadParticleEmitter(10,pParticleEffect);

	if(IParticleEmitter *pEmitter = GetEntity()->GetParticleEmitter(10))
	{
		pEmitter->SetLocation(IParticleEffect::ParticleLoc(GetEntity()->GetPos(),Vec3(0,0,1)));
		SpawnParams Emitter_Params;
		pEmitter->GetSpawnParams(Emitter_Params);
		Emitter_Params.eAttachType = GeomType_None;
		Emitter_Params.fSpeedScale = effectRadius / 5;
		pEmitter->SetSpawnParams(Emitter_Params);
	}
}

bool CLeakHole::ReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params)
{
	ResetGameObject();
	return true;
}

bool CLeakHole::GetEntityPoolSignature(TSerialize signature)
{
	return true;
}

void CLeakHole::Release()
{
	IParticleEmitter *pEmitter = GetEntity()->GetParticleEmitter(10);
	if (pEmitter)
		gEnv->pParticleManager->DeleteEmitter(pEmitter);
	delete this;
}

void CLeakHole::FullSerialize(TSerialize ser)
{

}

void CLeakHole::Update(SEntityUpdateContext& ctx, int slot)
{
	if(!g_pGame->GetIGameFramework()->IsGamePaused() && !g_pGame->GetIGameFramework()->IsEditing())
	{
		//Code Note: Get Up Direction
		Matrix34 mat = GetEntity()->GetLocalTM();
		Quat rot = GetEntity()->GetRotation();

		//Vec3 UpDir = Vec3(-mat.m20,-mat.m21,mat.m22);

		Vec3 UpDir = rot.GetColumn2().GetNormalized();

		if(IParticleEmitter *pEmitter = GetEntity()->GetParticleEmitter(10))
		{
			pEmitter->SetLocation(IParticleEffect::ParticleLoc(GetEntity()->GetPos(),UpDir));
		}
		if(GetEntity()->IsActive())
		{
			Vec3 pos = GetEntity()->GetPos();
			IParticleEmitter *pEmitter = GetEntity()->GetParticleEmitter(10);
			pEmitter->Activate(true);
			IPhysicalEntity* pSelf = GetEntity()->GetPhysics();
			if (pSelf)
			{
				gEnv->pPhysicalWorld->RayWorldIntersection(pos+UpDir * 0.2, UpDir*effectRadius, ent_all, rayFlags, &hit, 1, &pSelf, 1);
				if (hit.pCollider)
				{
					pEntity = (IEntity*)hit.pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);
					if (pEntity)
					{
						CActor* pActor = (CActor*)g_pGame->GetGameRules()->GetActorByEntityId(pEntity->GetId());
						if (pActor)
						{
							HitInfo hit;
							hit.shooterId = GetEntityId();
							hit.targetId = pActor->GetEntityId();
							hit.dir = Vec3(0, 0, 1);
							hit.weaponId = 0;
							hit.damage = drawDamage;
							hit.type = 27;
							if (hit.targetId != hit.shooterId)
							{
								g_pGame->GetGameRules()->ClientHit(hit);
								//pEntity->Hide(true);
							}
						}
						else
						{
							g_pGame->GetDeleteSystem()->ProcessDeleteEntity(pEntity);
							/*pEntity->Hide(true);
							g_pGame->GetBasicRayCast()->ClearLastEntity(pEntity->GetId());
							gEnv->pEntitySystem->RemoveEntity(pEntity->GetId(), true);*/
						}
					}
					pEmitter->Activate(false);
					Enable = false;
				}
			}
		}
		else
		{
			tick_timer += ctx.fFrameTime;
			if(tick_timer >= Cd)
			{
				tick_timer = 0;
				Enable = true;
			}
		}
	}
}


void CLeakHole::HandleEvent(const SGameObjectEvent& gameObjectEvent)
{

}

void CLeakHole::ProcessEvent(SEntityEvent& entityEvent)
{

}

void CLeakHole::GetMemoryUsage(ICrySizer *pSizer) const
{

}


void CLeakHole::ReadScript()
{
	SmartScriptTable root = GetEntity()->GetScriptTable();
	SmartScriptTable props,fp;
	root->GetValue("Properties", props);
	props->GetValue("FunctionParams", fp);
	fp->GetValue("Radius", effectRadius);
	fp->GetValue("Damage", drawDamage);
	fp->GetValue("Cd",Cd);
	if(IParticleEmitter *pEmitter = GetEntity()->GetParticleEmitter(10))
	{
		pEmitter->SetLocation(IParticleEffect::ParticleLoc(GetEntity()->GetPos(),Vec3(0,0,1)));
		SpawnParams Emitter_Params;
		pEmitter->GetSpawnParams(Emitter_Params);
		Emitter_Params.eAttachType = GeomType_None;
		Emitter_Params.fSpeedScale = effectRadius / 5;
		pEmitter->SetSpawnParams(Emitter_Params);
	}
}