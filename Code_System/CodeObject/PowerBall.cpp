#include "StdAfx.h"
#include "PowerBall.h"


CPowerBall::CPowerBall() 
:	 powerDamage(0)
,	totalDamage(0)
,	aimPower(0)
,	exploRadius(0)
{
}

CPowerBall::~CPowerBall()
{

}


bool CPowerBall::Init(IGameObject * pGameObject)
{
	totalDamage = 0;
	SetGameObject(pGameObject);
	GetGameObject()->EnablePhysicsEvent(true, eEPE_AllLogged);
	IAIObject *pAI = GetEntity()->GetAI();
	return true;
}

void CPowerBall::PostInit(IGameObject * pGameObject)
{
	ReadScript();
	GetGameObject()->EnableUpdateSlot(this, 0);
	GetGameObject()->SetUpdateSlotEnableCondition(this, 0, eUEC_Always);
}

bool CPowerBall::ReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params)
{
	ResetGameObject();
	return true;
}

bool CPowerBall::GetEntityPoolSignature(TSerialize signature)
{
	return true;
}

void CPowerBall::Release()
{
	IParticleEmitter *pEmitter = GetEntity()->GetParticleEmitter(10);
	if (pEmitter)
		gEnv->pParticleManager->DeleteEmitter(pEmitter);
	delete this;
}

void CPowerBall::FullSerialize(TSerialize ser)
{

}

void CPowerBall::Update(SEntityUpdateContext& ctx, int slot)
{
	if(!g_pGame->GetIGameFramework()->IsGamePaused() && !g_pGame->GetIGameFramework()->IsEditing())
	{
		if (totalDamage >= aimPower)
		{
			totalDamage = 0;
			entityMap.clear();
			CommonUsage::CreateExplosion(GetEntityId(), GetEntity()->GetPos(), powerDamage, exploRadius, 1000, CGameRules::EHitType::Frag, "Code_System.Module_Effect.DataBall_Explosion");

			g_pGame->GetBasicRayCast()->ClearLastEntity(GetEntityId());
			gEnv->pEntitySystem->RemoveEntity(GetEntityId(), false);
		}
		for (std::map<EntityId, bool>::iterator it = entityMap.begin(); it != entityMap.end();++it)
		{
			if (!it->second)
			{
				IEntity* pEntity = gEnv->pEntitySystem->GetEntity(it->first);
				if (pEntity)
				{
					HitInfo hit;
					hit.shooterId = GetEntityId();
					hit.targetId = pEntity->GetId();
					hit.dir = Vec3(0, 0, 1);
					hit.weaponId = 0;
					hit.damage = powerDamage * Random(0.6f, 1.5f);
					hit.type = 27;
					if (hit.targetId != hit.shooterId)
					{
						g_pGame->GetGameRules()->ClientHit(hit);
						totalDamage += powerDamage;
					}
					(*it).second = true;
				}
			}
		}
	}
}


void CPowerBall::HandleEvent(const SGameObjectEvent& gameObjectEvent)
{

}

void CPowerBall::ProcessEvent(SEntityEvent& entityEvent)
{
	switch (entityEvent.event)
	{
	case ENTITY_EVENT_COLLISION:
		{
			EventPhysCollision *pCollision = (EventPhysCollision *)(entityEvent.nParam[0]);
			if (pCollision)
			{
				IEntity *pTarget = pCollision->iForeignData[1] == PHYS_FOREIGN_ID_ENTITY ? (IEntity*)pCollision->pForeignData[1] : 0;
				if (pCollision->iForeignData[1] == PHYS_FOREIGN_ID_ENTITY)
				{
					if ((IEntity*)pCollision->pForeignData[1] == GetEntity())
						pTarget = (IEntity*)pCollision->pForeignData[0];
				}
				if (pTarget)
				{
					std::map<EntityId, bool>::value_type p(pTarget->GetId(), false);
					entityMap.insert(p);
				}
			}						   
		}
	}
}

void CPowerBall::GetMemoryUsage(ICrySizer *pSizer) const
{

}


void CPowerBall::ReadScript()
{
	SmartScriptTable root = GetEntity()->GetScriptTable();
	SmartScriptTable props,fp;
	root->GetValue("Properties", props);
	props->GetValue("FunctionParams", fp);
	fp->GetValue("Damage", powerDamage);
	fp->GetValue("PowerUpperBound", aimPower);
	fp->GetValue("ExploRadius", exploRadius);
}