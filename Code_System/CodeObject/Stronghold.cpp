#include "StdAfx.h"
#include "Stronghold.h"
#include "AISpawnPoint.h"

CStronghold::CStronghold() 
	: Level(1)
	, Radius(30)
	, LevelCap(3)
	, threatCount(0)
	, refresh_time(30)
	{

	}

CStronghold::~CStronghold()
{

}


bool CStronghold::Init(IGameObject * pGameObject)
{
	SetGameObject(pGameObject);
	return true;
}

void CStronghold::PostInit(IGameObject * pGameObject)
{
	ReadScript();
	GetGameObject()->EnableUpdateSlot(this, 0);
	GetGameObject()->SetUpdateSlotEnableCondition(this, 0, eUEC_Never);
	//mesh.ClearMesh();

	StrongholdManager::Instance().RegisterStronghold(GetEntityId(),Level,LevelCap,refresh_time);
}

bool CStronghold::ReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params)
{
	ResetGameObject();
	return true;
}

bool CStronghold::GetEntityPoolSignature(TSerialize signature)
{
	return true;
}

void CStronghold::Release()
{
	delete this;
}

void CStronghold::FullSerialize(TSerialize ser)
{

}

void CStronghold::Update(SEntityUpdateContext& ctx, int slot)
{
	/*if(!g_pGame->GetIGameFramework()->IsGamePaused() && !g_pGame->GetIGameFramework()->IsEditing())
	{
		mesh.CreateNavMesh(GetEntity()->GetPos(),2,15,ctx.fFrameTime);
		//mesh.DebugMesh();
		if(tick_timer > update_time)
		{
			CheckDistance();
			tick_timer = 0;
		}
		else
			tick_timer += ctx.fFrameTime;

		if(isClear)
		{
			tick_timer_for_refresh += ctx.fFrameTime;
			if(tick_timer_for_refresh > refresh_time)
			{
				isClear = false;
				tick_timer_for_refresh = 0;
				RebootAllSpawnPoints();
				if(hasEntered)
					SetAllSpawnsStatus(true);
			}
		}
	}
	else
	{
		hasEntered = false;
		isClear = false;
	}*/
}


void CStronghold::HandleEvent(const SGameObjectEvent& gameObjectEvent)
{

}

void CStronghold::ProcessEvent(SEntityEvent& entityEvent)
{

}

void CStronghold::GetMemoryUsage(ICrySizer *pSizer) const
{

}


void CStronghold::ReadScript()
{
	SmartScriptTable root = GetEntity()->GetScriptTable();
	SmartScriptTable props,fp;
	root->GetValue("Properties", props);
	props->GetValue("Radius", Radius);
	props->GetValue("Level", Level);
	props->GetValue("RefreshTime", refresh_time);
}

void CStronghold::AddToSpawnPointSet(EntityId id)
{
	SpawnPointSet.push_back(id);
}

void CStronghold::IncreaseThreatCount(int count)
{
	Level += count;
	CalculateLevel();
}

void CStronghold::CalculateLevel()
{
	/*For now I don't know how to balance the threat count*/
	IEntity *pStronghold = GetEntity();
	std::deque<EntityId>::iterator iter = SpawnPointSet.begin();
	std::deque<EntityId>::iterator end = SpawnPointSet.end();
	while(iter != end)
	{
		IEntity *pEntity = gEnv->pEntitySystem->GetEntity(*iter);
		if(pEntity)
		{
			CAISpawnPoint* pAISpawnPoint = static_cast<CAISpawnPoint*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(*iter, "AISpawnPoint"));
			if(pAISpawnPoint)
			{
				pAISpawnPoint->SetLevel(Level);
			}
		}
		++iter;
	}
}

void CStronghold::SetAllSpawnsStatus(const bool _enable)
{
	IEntity *pStronghold = GetEntity();
	std::deque<EntityId>::iterator iter = SpawnPointSet.begin();
	std::deque<EntityId>::iterator end = SpawnPointSet.end();
	while(iter != end)
	{
		IEntity *pEntity = gEnv->pEntitySystem->GetEntity(*iter);
		if(pEntity)
		{
			CAISpawnPoint* pAISpawnPoint = static_cast<CAISpawnPoint*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(*iter, "AISpawnPoint"));
			if(pAISpawnPoint)
			{
				pAISpawnPoint->SetEnable(_enable);
			}
		}
		++iter;		//Some Bugs occur when play for a while.
	}
}

void CStronghold::ReceiveClearMessage()
{
	std::deque<EntityId>::iterator iter = SpawnPointSet.begin();
	std::deque<EntityId>::iterator end = SpawnPointSet.end();
	while(iter != end)
	{
		IEntity *pEntity = gEnv->pEntitySystem->GetEntity(*iter);
		if(pEntity)
		{
			CAISpawnPoint* pAISpawnPoint = static_cast<CAISpawnPoint*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(*iter, "AISpawnPoint"));
			if(pAISpawnPoint)
			{
				if(!pAISpawnPoint->HasEliminateAll() && pAISpawnPoint->isSpawnAI())
					return;
			}
		}
		++iter;
	}
	SendClearMessageToManager();
	/*isClear = true;
	threatCount += count * 10;
	CalculateLevel();*/
}

void CStronghold::CheckDistance()
{
	/*IEntity *pStronghold = GetEntity();
	Vec3 SpawnerPos = pStronghold->GetPos();
	Vec3 PlayerPos = gEnv->pEntitySystem->GetEntity(g_pGame->GetClientActorId())->GetPos();
	float fDistance = (SpawnerPos - PlayerPos).GetLengthSquared();
	bool isCloseEnough = fDistance <= Radius * Radius;
	if(isCloseEnough)
	{
		if(!hasEntered)
		{
			hasEntered = true;
			SetAllSpawnsStatus(true);
		}
	}
	else
	{
		//GenerateRandomPosFromNevMesh();
		if(hasEntered)
		{
			hasEntered = false;
			//RandomSpawnPosDeque.clear();
			SetAllSpawnsStatus(false);
		}
	}*/
}

void CStronghold::RebootAllSpawnPoints()
{
	std::deque<EntityId>::iterator iter = SpawnPointSet.begin();
	std::deque<EntityId>::iterator end = SpawnPointSet.end();
	while(iter != end)
	{
		IEntity *pEntity = gEnv->pEntitySystem->GetEntity(*iter);
		if(pEntity)
		{
			CAISpawnPoint* pAISpawnPoint = static_cast<CAISpawnPoint*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(*iter, "AISpawnPoint"));
			if(pAISpawnPoint)
			{
				pAISpawnPoint->ClearSpawnStatus();
			}
		}
		++iter;
	}
}

Vec3 CStronghold::GetRandomPos()
{
	if(RandomSpawnPosDeque.empty())
		return Vec3(ZERO);

	IAreaManager* pAreaManager = gEnv->pEntitySystem->GetAreaManager();
	const int k_shapeArraySize = 16;
	int shapeArrayCount = k_shapeArraySize;
	EntityId AreaId(0);
	pAreaManager->GetLinkedAreas(GetEntityId(),&AreaId, shapeArrayCount);
	IEntity *pArea = gEnv->pEntitySystem->GetEntity(AreaId);
	if(!pArea)
		return Vec3(ZERO);
	IEntityAreaProxy *pAreaProxy = (IEntityAreaProxy*)pArea->GetProxy(ENTITY_PROXY_AREA);

	int index = cry_rand() % RandomSpawnPosDeque.size();
	Vec3 pos = RandomSpawnPosDeque[index];
	bool inside = pAreaProxy->CalcPointWithin(INVALID_ENTITYID, pos);
	while(!inside)
	{
		pos = mesh.GenerateRandomPos();
		inside = pAreaProxy->CalcPointWithin(INVALID_ENTITYID, pos);
	}

	if(inside)
		return pos;

	return Vec3(ZERO);
}

void CStronghold::GenerateRandomPosFromNevMesh()
{
	if(RandomSpawnPosDeque.size() > 10)
		return;
	Vec3 pos = mesh.GenerateRandomPosPreFrame();
	if(pos != Vec3(ZERO))
	{
		RandomSpawnPosDeque.push_back(pos);
	}
}

void CStronghold::SendClearMessageToManager()
{
	StrongholdManager::Instance().ReceiveStrongholdClearMessage(GetEntityId());
}

void CStronghold::ReceiveEnemyEliminateMessage(float capability)
{
	StrongholdManager::Instance().ReceiveStrongholdEliminateMessage(GetEntityId(), capability);
}