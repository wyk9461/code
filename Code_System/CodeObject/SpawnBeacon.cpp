#include "StdAfx.h"
#include "SpawnBeacon.h"

unsigned int CSpawnBeacon::AvailableBeaconNum=0;

namespace SBeacon
{
	void RegisterEvents( IGameObjectExtension& goExt, IGameObject& gameObject )
	{
		const int events[] = 
		{	
			eGFE_ScriptEvent,
			eSpawnBeaconEventListenerGameObjectEvent_Test,
		};

		gameObject.UnRegisterExtForEvents( &goExt, NULL, 0 );
		gameObject.RegisterExtForEvents( &goExt, events, (sizeof(events) / sizeof(int)) );
	}
}

CSpawnBeacon::CSpawnBeacon()
{
	TickTimer = 0;
	SpawnDelay = 5;
	SpawnCount = 0;
	ReturnTimer=0;
	MaxSpawn=5;
	HasSet=false;
	isDestroyed=false;
}

CSpawnBeacon::~CSpawnBeacon()
{

}


bool CSpawnBeacon::Init( IGameObject * pGameObject )
{
	SetGameObject(pGameObject);
	GetGameObject()->EnablePhysicsEvent(true, eEPE_AllLogged);
	pEffect=NULL;
	++AvailableBeaconNum;
	HasParticle=false;
	HasSet=false;
	isDestroyed=false;
	return true;
}

void CSpawnBeacon::PostInit( IGameObject * pGameObject )
{
	ReadScript();
	SBeacon::RegisterEvents( *this, *pGameObject );
	GetGameObject()->EnableUpdateSlot(this,0);
	GetGameObject()->SetUpdateSlotEnableCondition(this,0,eUEC_Always);
}

bool CSpawnBeacon::ReloadExtension( IGameObject * pGameObject, const SEntitySpawnParams &params )
{
	ResetGameObject();
	SBeacon::RegisterEvents( *this, *pGameObject );
	return true;
}

bool CSpawnBeacon::GetEntityPoolSignature( TSerialize signature )
{
	return true;
}

void CSpawnBeacon::Release()
{
	if(!isDestroyed)
		--AvailableBeaconNum;
	IParticleEmitter *pEmitter=GetEntity()->GetParticleEmitter(10);
	if(pEmitter)
		gEnv->pParticleManager->DeleteEmitter(pEmitter);
	/*if(pEffect)
		gEnv->pParticleManager->DeleteEffect(pEffect);*/
	delete this;
}

void CSpawnBeacon::FullSerialize( TSerialize ser )
{

}

void CSpawnBeacon::Update( SEntityUpdateContext& ctx, int slot )
{
	/*uint32 event=eSpawnBeaconEventListenerGameObjectEvent_Test; 
	const EntityId thisEntityId = GetEntityId();
	SGameObjectEvent goEvent( (uint32)event, eGOEF_ToExtensions, IGameObjectSystem::InvalidExtensionID, (void*)(&(thisEntityId)) );

	GetGameObject()->SendEvent( goEvent );*/

	if(!g_pGame->GetIGameFramework()->IsGamePaused() && !g_pGame->GetIGameFramework()->IsEditing())
	{
		IEntity *pEntity=GetEntity();
		Vec3 BeaconPos=pEntity->GetPos();
		float TerrainHeight=gEnv->p3DEngine->GetTerrainElevation(BeaconPos.x,BeaconPos.y);
		IEntityPhysicalProxy *pp=static_cast<IEntityPhysicalProxy *>(pEntity->GetProxy(ENTITY_PROXY_PHYSICS));
		AABB aabb;
		pEntity->GetWorldBounds(aabb);
		if(BeaconPos.z - TerrainHeight > 0.1)
		{
			if(!HasSet)
			{
				Vec3 NextPos=BeaconPos;
				NextPos.z-=50*ctx.fFrameTime;
				pEntity->SetPos(NextPos);
			}
		}
		else
		{
			if(!isDestroyed)
			{
				if(!HasParticle)
				{
					HasParticle=true;
					pEffect=gEnv->pParticleManager->FindEffect("Code_System.SpawnBeacon.BeaconBeam");
					//pEffect->Spawn(false,IParticleEffect::ParticleLoc(GetEntity()->GetPos(),Vec3(0,0,1)));
					pEntity->LoadParticleEmitter(10,pEffect);
					IParticleEmitter *pEmitter=GetEntity()->GetParticleEmitter(10);
					pEmitter->SetLocation(IParticleEffect::ParticleLoc(GetEntity()->GetPos(),Vec3(0,0,1)));
				}
				HasSet=true;
				/*Vec3 FinalPos=BeaconPos;
				//FinalPos.z=static_cast<f32>(TerrainHeight+2.3);
				pEntity->SetPos(FinalPos);*/
			}
		}

		if(HasSet && !isDestroyed)
			ProcessSpawn(ctx.fFrameTime);
		if(SpawnCount >=MaxSpawn)
			ReturnTimer+=ctx.fFrameTime;
		if(ReturnTimer > 120)
		{
			gEnv->pEntitySystem->RemoveEntity(GetEntityId());
			/*Vec3 NextPos=BeaconPos;
			NextPos.z+=75*ctx.fFrameTime;
			pEntity->SetPos(NextPos);*/
		}
	}
}


void CSpawnBeacon::HandleEvent( const SGameObjectEvent& gameObjectEvent )
{
	const uint32 eventId = gameObjectEvent.event;
	void* pParam = gameObjectEvent.param;
	if ((eventId == eGFE_ScriptEvent) && (pParam != NULL))
	{
		const char* eventName = static_cast<const char*>(pParam);
		if (strcmp(eventName, "OnDestroy") == 0)
		{
			--AvailableBeaconNum;
			isDestroyed=true;
			IParticleEmitter *pEmitter=GetEntity()->GetParticleEmitter(10);
			if(pEmitter)
				gEnv->pParticleManager->DeleteEmitter(pEmitter);
			/*if(pEffect)
			{
				gEnv->pParticleManager->DeleteEffect(pEffect);
				pEffect=NULL;
			}*/
		}
	}
	else if(eventId == eSpawnBeaconEventListenerGameObjectEvent_Test)
	{

	}
}

void CSpawnBeacon::ProcessEvent( SEntityEvent& entityEvent )
{
	switch(entityEvent.event)
	{
	case ENTITY_EVENT_COLLISION:
		{
			//CryLogAlways("Lalala");
		}
		break;
	}
}

void CSpawnBeacon::GetMemoryUsage( ICrySizer *pSizer ) const
{

}


void CSpawnBeacon::ProcessSpawn(const float frameTime)
{
	if(SpawnCount < MaxSpawn && TickTimer > SpawnDelay)
	{
		IEntityArchetype *pArchetype=gEnv->pEntitySystem->LoadEntityArchetype("SystemSpawnList.Notice_AI_1");
		Vec3 pos=GetEntity()->GetPos();
		pos.z+=0.1;
		pos.x+=BiRandom(1);
		pos.y+=BiRandom(1);
		SEntitySpawnParams params;
		params.vPosition=pos;
		params.pClass=pArchetype->GetClass();
		params.sName=pArchetype->GetName();
		params.pPropertiesTable=pArchetype->GetProperties();
		params.pPropertiesTable=pArchetype->GetProperties();
		IEntity *SpawnAI=gEnv->pEntitySystem->SpawnEntity(params);
		IParticleEffect *pEffect=gEnv->pParticleManager->FindEffect("Code_System.SpawnAI");
		if(pEffect)
			pEffect->Spawn(true,IParticleEffect::ParticleLoc(pos,Vec3(0,0,1)));
		++SpawnCount;
		TickTimer = 0;
	}
	if(TickTimer <= SpawnDelay)
		TickTimer+=frameTime;
}

void CSpawnBeacon::ReadScript()
{
	SmartScriptTable root=GetEntity()->GetScriptTable();
	SmartScriptTable props;
	root->GetValue("Properties",props);
	props->GetValue("StabilityGain",StabilityReward);
	props->GetValue("SpawnDelay",SpawnDelay);
	props->GetValue("MaxSpawn",MaxSpawn);
}