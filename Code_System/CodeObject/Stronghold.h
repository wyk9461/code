#ifndef _SHRONGHOLD_H_
#define _SHRONGHOLD_H_

#include <IGameObject.h>
#include "Code_System/CodeAI/DynamicNavMesh.h"
#include "Code_System/MicroSystem/StrongholdManager.h"

class CStronghold : public CGameObjectExtensionHelper<CStronghold, IGameObjectExtension>
{
public:
	CStronghold();
	virtual ~CStronghold();

	// IGameObjectExtension
	virtual bool Init(IGameObject * pGameObject);
	virtual void InitClient(int channelId) {};
	virtual void PostInit(IGameObject * pGameObject);
	virtual void PostInitClient(int channelId) {};
	virtual bool ReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params);
	virtual void PostReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params) {}
	virtual bool GetEntityPoolSignature(TSerialize signature);
	virtual void Release();
	virtual void FullSerialize(TSerialize ser);
	virtual bool NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) { return false; };
	virtual void PostSerialize() {};
	virtual void SerializeSpawnInfo(TSerialize ser) {}
	virtual ISerializableInfoPtr GetSpawnInfo() { return 0; }
	virtual void Update(SEntityUpdateContext& ctx, int slot);
	virtual void HandleEvent(const SGameObjectEvent& gameObjectEvent);
	virtual void ProcessEvent(SEntityEvent& entityEvent);
	virtual void SetChannelId(uint16 id) {};
	virtual void SetAuthority(bool auth) {};
	virtual void PostUpdate(float frameTime) { CRY_ASSERT(false); }
	virtual void PostRemoteSpawn() {};
	virtual void GetMemoryUsage(ICrySizer *pSizer) const;

	// ~IGameObjectExtension
	void ReadScript();
	void AddToSpawnPointSet(EntityId id);
	void IncreaseThreatCount(int count);
	void CalculateLevel();
	//const bool HasEntered(){return hasEntered;}

	void ReceiveClearMessage();
	void ReceiveEnemyEliminateMessage(float capability);
	Vec3 GetRandomPos();
	void GenerateRandomPosFromNevMesh();

	void RebootAllSpawnPoints();
private:
	int Level;
	float Radius;
	int LevelCap;
	int threatCount;
	float refresh_time;

	std::deque<EntityId> SpawnPointSet;
	std::deque<Vec3> RandomSpawnPosDeque;
	void SetAllSpawnsStatus(const bool _enable);
	void CheckDistance();
	void SendClearMessageToManager();

	char* NevMesh_Name;


	DynamicNavMesh mesh;
};


#endif