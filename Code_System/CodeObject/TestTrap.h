#ifndef _TESTTRAP_H_
#define  _TESTTRAP_H_


#include <IGameObject.h>
#include "Code_System/CodeAI/FlyingAI.h"
#include "Code_System/CodeAI/PathFinding.h"

enum ECTestTrapEventListenerGameObjectEvent
{
	eTestTrapListenerGameObjectEvent_Test = 20,
};


class CTestTrap : public CGameObjectExtensionHelper<CTestTrap, IGameObjectExtension>
{
public:
	CTestTrap();
	virtual ~CTestTrap();

	// IGameObjectExtension
	virtual bool Init( IGameObject * pGameObject );
	virtual void InitClient( int channelId ) {};
	virtual void PostInit( IGameObject * pGameObject );
	virtual void PostInitClient( int channelId ) {};
	virtual bool ReloadExtension( IGameObject * pGameObject, const SEntitySpawnParams &params );
	virtual void PostReloadExtension( IGameObject * pGameObject, const SEntitySpawnParams &params ) {}
	virtual bool GetEntityPoolSignature( TSerialize signature );
	virtual void Release();
	virtual void FullSerialize( TSerialize ser );
	virtual bool NetSerialize( TSerialize ser, EEntityAspects aspect, uint8 profile, int flags ) { return false; };
	virtual void PostSerialize() {};
	virtual void SerializeSpawnInfo( TSerialize ser ) {}
	virtual ISerializableInfoPtr GetSpawnInfo() {return 0;}
	virtual void Update( SEntityUpdateContext& ctx, int slot );
	virtual void HandleEvent( const SGameObjectEvent& gameObjectEvent );
	virtual void ProcessEvent( SEntityEvent& entityEvent );
	virtual void SetChannelId( uint16 id ) {};
	virtual void SetAuthority( bool auth ) {};
	virtual void PostUpdate( float frameTime ) { CRY_ASSERT(false); }
	virtual void PostRemoteSpawn() {};
	virtual void GetMemoryUsage( ICrySizer *pSizer ) const;

	// ~IGameObjectExtension

private:
	AABB aabb;
	void ReadScript();
	FlyingAI *AIObject;
};


#endif