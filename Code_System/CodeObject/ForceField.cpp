#include "StdAfx.h"
#include "ForceField.h"
#include "Code_System/MicroSystem/FallenActorManager.h"

IParticleEffect *CForceField::pEffect = NULL;

CForceField::CForceField() :effectRadius(0), timer(0), accScale(0)
{
	enable = true;
	tick_timer = 0;
	setup_timer = 0;
	cd = 0.5;
}

CForceField::~CForceField()
{

}


bool CForceField::Init(IGameObject * pGameObject)
{
	SetGameObject(pGameObject);
	GetGameObject()->EnablePhysicsEvent(true, eEPE_AllLogged);
	return true;
}

void CForceField::PostInit(IGameObject * pGameObject)
{
	ReadScript();
	GetGameObject()->EnableUpdateSlot(this, 0);
	GetGameObject()->SetUpdateSlotEnableCondition(this, 0, eUEC_Always);
}

bool CForceField::ReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params)
{
	ResetGameObject();
	return true;
}

bool CForceField::GetEntityPoolSignature(TSerialize signature)
{
	return true;
}

void CForceField::Release()
{
	IParticleEmitter *pEmitter = GetEntity()->GetParticleEmitter(10);
	if (pEmitter)
		gEnv->pParticleManager->DeleteEmitter(pEmitter);
	delete this;
}

void CForceField::FullSerialize(TSerialize ser)
{

}

void CForceField::Update(SEntityUpdateContext& ctx, int slot)
{
	if(!g_pGame->GetIGameFramework()->IsGamePaused() && !g_pGame->GetIGameFramework()->IsEditing())
	{
		if(setup_timer < 1)
			setup_timer += ctx.fFrameTime;
		if(GetEntity()->IsActive() && setup_timer >= 1)
		{
			EnterArea();
			for (std::map<EntityId, VelAndMass>::iterator it = entityMap.begin(); it != entityMap.end(); ++it)
			{

				tick_timer += ctx.fFrameTime;
				//AABB bounds;
				IEntity* ForcedEntity = gEnv->pEntitySystem->GetEntity(it->first);
				if(ForcedEntity)
				{
					IEntityPhysicalProxy *proxy = (IEntityPhysicalProxy*)ForcedEntity->GetProxy(ENTITY_PROXY_PHYSICS);
					if (proxy)
					{
						CActor *pActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(it->first);
						if(pActor && pActor != g_pGame->GetIGameFramework()->GetClientActor())
						{
							/*if(!pActor->IsFallen())
								FallenActorManager::Instance().AddToManager(pActor);*/
						}
						if(!pActor)
						{
							AABB aabb;
							ForcedEntity->GetWorldBounds(aabb);
							Vec3 vel(it->second.vel);
							float mass = it->second.mass;
							float acc = it->second.acc;
							Vec3 force_dir = (aabb.GetCenter() - GetEntity()->GetPos()).normalize();
							float length = (aabb.GetCenter() - GetEntity()->GetPos()).GetLengthFast();
							//Vec3 force = -vel.normalize()*mass*acc*accScale;
							Vec3 force = force_dir*mass*accScale * 1/length;
							proxy->AddImpulse(-1, aabb.GetCenter(), force, true, 1);
							if(!pEffect)
								pEffect = gEnv->pParticleManager->FindEffect("Code_System.CodeObject.ForceFieldPush");
							else
							{
								if(tick_timer >= cd)
								{
									pEffect->Spawn(false,IParticleEffect::ParticleLoc(GetEntity()->GetPos(),Vec3(0,0,1)));
									tick_timer = 0;
								}
							}
						}
					}
				}
			}
			LeaveArea();
		}
	}
}


void CForceField::HandleEvent(const SGameObjectEvent& gameObjectEvent)
{

}

void CForceField::ProcessEvent(SEntityEvent& entityEvent)
{

}

void CForceField::GetMemoryUsage(ICrySizer *pSizer) const
{

}

void CForceField::EnterArea()
{
	Vec3 pos = GetEntity()->GetPos();
	IPhysicalEntity **nearbyEntities;
	int n = gEnv->pPhysicalWorld->GetEntitiesInBox( pos - Vec3(10), pos + Vec3(10), nearbyEntities, ent_all);
	for (int i = 0; i < n; i++)
	{
		IEntity* pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(nearbyEntities[i]);
		if (pEntity && (pEntity->GetId()!=GetEntityId()))
		{
			IPhysicalEntity* pPhyEnt = pEntity->GetPhysics();
			float dis = (pos - pEntity->GetPos()).GetLengthSquared();
			if (pPhyEnt && (dis <= effectRadius*effectRadius))
			{
				pe_status_dynamics dyn;
				pPhyEnt->GetStatus(&dyn);
				float a = dyn.v.GetLengthSquared()/(2*effectRadius);
				VelAndMass new_nifo = VelAndMass(dyn.v,dyn.mass,a);
				entityMap[pEntity->GetId()] = new_nifo;
			}
		}
	}
}

void CForceField::LeaveArea()
{
	Vec3 pos = GetEntity()->GetPos();
	for (std::map<EntityId, VelAndMass>::iterator it = entityMap.begin(); it != entityMap.end();)
	{
		IEntity* pEntity = gEnv->pEntitySystem->GetEntity((*it).first);
		if (pEntity)
		{
			float dis = (pos - pEntity->GetPos()).GetLengthSquared();
			if (dis > effectRadius*effectRadius)
			{
				entityMap.erase(it++);
			}
			else
				++it;
		}
		else
			entityMap.erase(it++);
	}
}

void CForceField::ReadScript()
{
	SmartScriptTable root = GetEntity()->GetScriptTable();
	SmartScriptTable props;
	SmartScriptTable funtion_tale;

	root->GetValue("Properties", props);
	props->GetValue("FunctionParams", funtion_tale);

	funtion_tale->GetValue("EffectRadius",effectRadius);
	funtion_tale->GetValue("AccScale", accScale);
}