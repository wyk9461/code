#include "StdAfx.h"
#include "CodeExclusion.h"
#include "Code_System/CodeObject/SpawnBeacon.h"
#include "Perk.h"

CodeExclusion::CodeExclusion()
	:	CurExclusionLevel(eExL_Notice)
	,	ExclusionAmount(0)
	,	ProcessEntity(NULL)
	,	BasicEntityClass(NULL)
	,	Delete_Delay(5)
	,	BeaconSpawn_Delay(5)
	,	EntitySpawn_Delay(5)
	,	ExclusionTimer(0)
	,	ExclusionFadeTimer(0)
	,	ExclusionFadeRate(2)
	,	SpawnId(2000000000)
	,	CanDelete(false)
	,	CanDeleteCrossHair(false)
	,	CanSpawnEntity(false)
	,	CanSpawnBeacon(false)
	,	CanSpawnSystemRobot(false)
	,	CanCreateDynamicExplosion(false)
	,	SpawnId_min(2000000000)
	,	maxExclusionAmount(1000)
	,	pElement(NULL)
{
	InitTimers();
	PerkManager::Instance();
	DynamicExplosionPool.SetMaxIdAmount(30);
}

//------------------------------------------------------------------------
void CodeExclusion::Update(const float frameTime)
{
	SetExclusionLevel();
	//FindSpawnPoint();
	//FindSpawnPosByPlayerSpeed();
	UpdateTimers(frameTime);
	UpdateUIInfo();
	UpdateDynamicExplosion(frameTime);
	SystemPostSpawn(frameTime);

	if(CanSpawnSystemRobot)
	{
		SpawnSystemRobot();
	}


	if(CurExclusionLevel == eExL_Safe)
	{
		return;
	}
	else
	{
		DeleteExEntity();
	}

	if(CurExclusionLevel == eExL_Notice)
	{

		if(CanDelete)
		{
			ExclusionTimers.DeleteTimer = 0;
			GetNearbyEntityForDelete();
			AddToDeleteList();
			CanDelete = false;
		}

		/*if(CanSpawnBeacon)
		{
			DoubleSpawnPointNumber();
			if(FindSpawnPoint())
			{
				SpawnBeacons();
			}
		}*/
	}

	else if(CurExclusionLevel >= eExL_Warning)
	{
		if(CanCreateDynamicExplosion)
		{
			ExclusionTimers.DynamicExplosionTimer = 0;
			CreateDynamicExplosion();
			CanCreateDynamicExplosion = false;
		}
		if(CanDelete)
		{
			ExclusionTimers.DeleteTimer = 0;
			GetNearbyEntityForDelete();
			AddToDeleteList();
			CanDelete = false;
		}
	}
}

//------------------------------------------------------------------------
void CodeExclusion::InitTimers()
{
	DefaultSpawnMat=gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("spawn");
	IEntityArchetype *pEA=gEnv->pEntitySystem->LoadEntityArchetype("SystemSpawnList.Notice_AI_1");
	IEntityArchetype *pp=gEnv->pEntitySystem->CreateEntityArchetype(pEA->GetClass(),"SystemSpawnList.lAlAlA");
	CanDelete=false;

	ExclusionTimers.DeleteTimer=0;
	ExclusionTimers.DeleteCrossHairTimer = 0;
	ExclusionTimers.BeaconSpawnTimer=0;
	ExclusionTimers.EntitySpawnTimer=0;
	ExclusionTimers.SystemRobotTimer=0;
	ExclusionTimers.DynamicExplosionTimer = 0;
}

//------------------------------------------------------------------------
void CodeExclusion::UpdateTimers(const float frameTime)
{
	if(ExclusionFadeTimer > 1)
	{
		if(ExclusionAmount - ExclusionFadeRate < 0)
			ExclusionAmount = 0;
		else
			ExclusionAmount -= ExclusionFadeRate;
		ExclusionFadeTimer = 0;
	}
	ExclusionFadeTimer += frameTime;
	if(!CanSpawnSystemRobot)
	{
		std::list<EntityId> &SpawnEntitySet = g_pGame->GetSpawnSystem()->GetSpawnEntitySet();
		std::vector<SEntitySpawnParams> &DeleteRecoveryInfo = g_pGame->GetDeleteSystem()->GetDeleteRecoveryInfo();
		if(SpawnEntitySet.empty() && DeleteRecoveryInfo.empty())
			ExclusionTimers.SystemRobotTimer = 0;

		if(ExclusionTimers.SystemRobotTimer > GetSystemRobotRefreshTime())
		{
			CanSpawnSystemRobot=true;
			ExclusionTimers.SystemRobotTimer=0;
		}
		ExclusionTimers.SystemRobotTimer+=frameTime;
	}

	if(CurExclusionLevel > eExL_Safe)
		UpdateNoticeTimer(frameTime);
}

//------------------------------------------------------------------------
void CodeExclusion::UpdateNoticeTimer(const float frameTime)
{
	if(ExclusionTimers.DeleteTimer > Delete_Delay)
	{
		CanDelete=true;
		ExclusionTimers.DeleteTimer=0;
	}

	if(ExclusionTimers.DeleteCrossHairTimer > DeleteCrossHair_Delay)
	{
		CanDeleteCrossHair=true;
		ExclusionTimers.DeleteCrossHairTimer=0;
	}

	if(ExclusionTimers.BeaconSpawnTimer > BeaconSpawn_Delay)
	{
		CanSpawnBeacon=true;
		ExclusionTimers.BeaconSpawnTimer=0;
	}

	if(ExclusionTimers.EntitySpawnTimer > EntitySpawn_Delay)
	{
		CanSpawnEntity=true;
		ExclusionTimers.EntitySpawnTimer=0;
	}

	if(ExclusionTimers.DynamicExplosionTimer > DynamicExplosion_Delay)
	{
		CanCreateDynamicExplosion=true;
		ExclusionTimers.DynamicExplosionTimer=0;
	}

	ExclusionTimers.DeleteTimer += frameTime;
	ExclusionTimers.DeleteCrossHairTimer += frameTime;
	ExclusionTimers.BeaconSpawnTimer += frameTime;
	ExclusionTimers.EntitySpawnTimer += frameTime;
	ExclusionTimers.DynamicExplosionTimer += frameTime;
}

//------------------------------------------------------------------------
void CodeExclusion::SetExclusionLevel()
{
	if(ExclusionAmount < 200)
	{
		CurExclusionLevel=eExL_Safe;
		Delete_Delay = -1;
		return;
	}
	if(ExclusionAmount >=200 && ExclusionAmount < 400)
	{
		CurExclusionLevel=eExL_Notice;
		Delete_Delay = eDNotice_Delete;
		DeleteCrossHair_Delay = eDNotice_DeleteCrossHair;
		BeaconSpawn_Delay = eDNotice_Beacon;
		EntitySpawn_Delay = eDNotice_Entity;

		EntitySpawnTime = eSNotice_Entity;
		ExplosionSpawnTime=eSNotice_Explosion;
		return;
	}
	if(ExclusionAmount >=400 && ExclusionAmount < 600)
	{
		CurExclusionLevel=eExL_Warning;
		Delete_Delay = eDWarning_Delete;
		DeleteCrossHair_Delay = eDWarning_DeleteCrossHair;
		//BeaconSpawn_Delay = eDWarning_Entity;
		EntitySpawn_Delay = eDWarning_Entity;
		DynamicExplosion_Delay = eDWarning_Explosion;

		EntitySpawnTime = 3;
		ExplosionSpawnTime=eSNotice_Explosion;
		return;
	}
	if(ExclusionAmount >=600 && ExclusionAmount < 800)
	{
		CurExclusionLevel=eExL_Exclusion;
		Delete_Delay = eDNotice_Delete;
		DeleteCrossHair_Delay = eDExclusion_DeleteCrossHair;
		//BeaconSpawn_Delay = eDNotice_Beacon;
		EntitySpawn_Delay = eDNotice_Entity;
		DynamicExplosion_Delay = eDExclusion_Explosion;

		EntitySpawnTime = 2;
		ExplosionSpawnTime=eSNotice_Explosion;
		return;
	}
	if(ExclusionAmount >=800)
	{
		CurExclusionLevel=eExL_Eliminate;
		Delete_Delay = eDNotice_Delete;
		DeleteCrossHair_Delay = eDEliminate_DeleteCrossHair;
		//BeaconSpawn_Delay = eDNotice_Beacon;
		EntitySpawn_Delay = eDNotice_Entity;
		DynamicExplosion_Delay = eDEliminate_Explosion;

		EntitySpawnTime = 1;
		ExplosionSpawnTime=eSNotice_Explosion;
		return;
	}
}

//------------------------------------------------------------------------
void CodeExclusion::GetNearbyEntityForDelete()
{
	if(!BasicEntityClass)
		BasicEntityClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("BasicEntity");
	if(g_pGame->GetIGameFramework()->GetClientActor())
	{
		IEntity *pPlayerEntity = g_pGame->GetIGameFramework()->GetClientActor()->GetEntity();
		IPhysicalEntity *pPhysicsEntity = pPlayerEntity->GetPhysics();
		AABB aabb;
		pPlayerEntity->GetWorldBounds(aabb);

		PlayerPos=aabb.GetCenter();

		Vec3 fwd_dir = pPlayerEntity->GetForwardDir();
		Vec3 back_dir = -fwd_dir;
		Vec3 right_dir = fwd_dir.Cross(Vec3(0,0,1));
		Vec3 left_dir = -right_dir;

		ray_hit hit;
		int ray_flag = rwi_stop_at_pierceable|rwi_colltype_any;

		gEnv->pPhysicalWorld->RayWorldIntersection(PlayerPos,fwd_dir*1.5,ent_static | ent_rigid | ent_sleeping_rigid,ray_flag,&hit,1,&pPhysicsEntity);
		if(hit.pCollider)
		{
			IEntity *pEntity=(IEntity*)hit.pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);
			if(pEntity)
			{
				AddToDeleteList(pEntity->GetId());
			}
		}

		gEnv->pPhysicalWorld->RayWorldIntersection(PlayerPos,back_dir*1.5,ent_rigid | ent_static | ent_sleeping_rigid,ray_flag,&hit,1,&pPhysicsEntity);
		if(hit.pCollider)
		{
			IEntity *pEntity=(IEntity*)hit.pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);
			if(pEntity)
			{
				AddToDeleteList(pEntity->GetId());
			}
		}

		gEnv->pPhysicalWorld->RayWorldIntersection(PlayerPos,right_dir*1.5,ent_rigid | ent_static | ent_sleeping_rigid,ray_flag,&hit,1,&pPhysicsEntity);
		if(hit.pCollider)
		{
			IEntity *pEntity=(IEntity*)hit.pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);
			if(pEntity)
			{
				AddToDeleteList(pEntity->GetId());
			}
		}

		gEnv->pPhysicalWorld->RayWorldIntersection(PlayerPos,left_dir*1.5,ent_rigid | ent_static | ent_sleeping_rigid,ray_flag,&hit,1,&pPhysicsEntity);
		if(hit.pCollider)
		{
			IEntity *pEntity=(IEntity*)hit.pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);
			if(pEntity)
			{
				AddToDeleteList(pEntity->GetId());
			}
		}
		/*PlayerDir=g_pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetForwardDir();
		DetectPos=PlayerPos+2.5*PlayerDir;
		const int num=gEnv->pPhysicalWorld->GetEntitiesInBox(DetectPos-Vec3(2.5),DetectPos+Vec3(2.5),NearbyEntity,ent_rigid | ent_static | ent_sleeping_rigid);
		for(int i=0;i<num;++i)
		{
			IEntity* tEntity=gEnv->pEntitySystem->GetEntityFromPhysics(NearbyEntity[i]);
			if(tEntity && tEntity->GetClass() == BasicEntityClass)
			{
				AABB aabb;
				tEntity->GetLocalBounds(aabb);
				bool IsBigEnough(aabb.GetVolume() > 1.5);
				float dis=( PlayerPos-tEntity->GetPos() ).GetLengthSquared();
				DisEntities[dis] = tEntity->GetId();
			}
		}*/
	}
}

//------------------------------------------------------------------------
void CodeExclusion::GetDeleteEntity()
{
	if(!DisEntities.empty())
	{
		distance_iter = DisEntities.begin();
		ProcessEntity=gEnv->pEntitySystem->GetEntity(distance_iter->second);
		DisEntities.clear();
	}
}

//------------------------------------------------------------------------
void CodeExclusion::AddToDeleteList()
{
	GetDeleteEntity();
	g_pGame->GetDeleteSystem()->ProcessDeleteEntity(ProcessEntity,true);
	ProcessEntity=NULL;
}

//------------------------------------------------------------------------
void CodeExclusion::AddToDeleteList(EntityId Id)
{
	IEntity *pEntity=gEnv->pEntitySystem->GetEntity(Id);
	g_pGame->GetDeleteSystem()->ProcessDeleteEntity(pEntity,true);
}

//------------------------------------------------------------------------
void CodeExclusion::SystemSpawnObstacle()
{
	CActor *pPlayer=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetClientActor());
	IEntity *pPlayerEntity=gEnv->pEntitySystem->GetEntity(g_pGame->GetClientActorId());
	
	IEntityArchetype *pArchetype=gEnv->pEntitySystem->LoadEntityArchetype("SystemSpawnList.Notice_SpawnEntity_1");
	SEntitySpawnParams params;
	params.vPosition=EntitySpawnPos;
	params.pClass=pArchetype->GetClass();
	params.sName=pArchetype->GetName();
	params.pPropertiesTable=pArchetype->GetProperties();
	params.qRotation=pPlayerEntity->GetRotation();
	IEntity *pEntity=gEnv->pEntitySystem->SpawnEntity(params);

	SystemSpawnEntityParams sparams;
	sparams.SpawnMat=gEnv->p3DEngine->GetMaterialManager()->CloneMaterial(DefaultSpawnMat,0);
	sparams.DefaultEntityMat=pEntity->GetMaterial();
	sparams.timer=0;
	sparams.spawntime=EntitySpawnTime;
	sparams.glow_amount=5;

	pEntity->EnablePhysics(false);
	pEntity->SetMaterial(sparams.SpawnMat);

	SystemEntities[pEntity->GetId()] = sparams;

	CanSpawnEntity=false;
}

//------------------------------------------------------------------------
bool CodeExclusion::FindSpawnPoint()
{
	if(CSpawnBeacon::GetTotalNum() > 5)
	{
		CanSpawnBeacon=false;
		return false;
	}
	Vec3 rand_dir(0,0,0);
	rand_dir.x=BiRandom(1.0);
	rand_dir.y=BiRandom(1.0);
	rand_dir.NormalizeFast();

	IEntity *pPlayerEntity=gEnv->pEntitySystem->GetEntity(g_pGame->GetClientActorId());
	if(pPlayerEntity)
	{
		Vec3 PlayerPos=pPlayerEntity->GetPos();

		float minDistance=20;
		float MaxDistance=25;

		Vec3 RayPosition(0,0,100);
		RayPosition=PlayerPos+rand_dir*minDistance*(1+cry_frand()*MaxDistance / minDistance);
		RayPosition.z=100;
		float TerrainHeight=gEnv->p3DEngine->GetTerrainElevation(RayPosition.x,RayPosition.y);
		Vec3 RayDir(0,0,-1);

		ray_hit hit;
		int rayFlags(rwi_stop_at_pierceable|rwi_colltype_any);
		int hits=gEnv->pPhysicalWorld->RayWorldIntersection(RayPosition,RayDir*1000,ent_all,rayFlags,&hit,1);
		bool SameHeightwithPlayer(hit.pt.z - PlayerPos.z < 0.05);

		if(hit.pt.z -  TerrainHeight < 0.1 || SameHeightwithPlayer)
		{
			BeaconSpawnPos=hit.pt;
			BeaconSpawnPos.z+=150;
			return true;
		}
	}
	return false;
}

//------------------------------------------------------------------------
void CodeExclusion::SpawnBeacons()
{
	IEntityArchetype *pArchetype=gEnv->pEntitySystem->LoadEntityArchetype("SystemSpawnList.Notice_SpawnBeacon_1");
	SEntitySpawnParams params;
	params.id = SpawnId;
	while(gEnv->pEntitySystem->IsIDUsed(SpawnId))
		++SpawnId;
	params.vPosition=BeaconSpawnPos;
	params.pClass=pArchetype->GetClass();
	params.sName=pArchetype->GetName();
	params.pPropertiesTable=pArchetype->GetProperties();
	params.nFlags = ENTITY_FLAG_CLIENT_ONLY | ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CALC_PHYSICS;
	IEntity *pBeacon=gEnv->pEntitySystem->SpawnEntity(params);
	SEntityPhysicalizeParams pparams;
	pparams.mass = 5000;
	pparams.density = -1;
	pparams.nSlot= -1;
	pparams.type = 2;
	pBeacon->Physicalize(pparams);
	pBeacon->EnablePhysics(true);
	pBeacon->SetUpdatePolicy(ENTITY_UPDATE_ALWAYS);
	CanSpawnBeacon=false;
	++SpawnId;
}

//------------------------------------------------------------------------
bool CodeExclusion::FindSpawnPosByPlayerSpeed()
{
	CActor *pPlayer=static_cast<CActor*>(g_pGame->GetIGameFramework()->GetClientActor());
	IEntity *pPlayerEntity=gEnv->pEntitySystem->GetEntity(g_pGame->GetClientActorId());
	if(pPlayerEntity)
	{
		Vec3 PlayerPos=pPlayerEntity->GetPos();
		const SActorPhysics& actorPhysics = pPlayer->GetActorPhysics();

		Vec3 vVelocity=actorPhysics.velocity;
		float fVelocity=vVelocity.GetLengthFast();
		//CryLogAlways("%.2f",fVelocity);
		if(fVelocity < 8)
		{
			CanSpawnEntity=false;
			ExclusionTimers.EntitySpawnTimer=0;
			return false;
		}

		//EntitySpawnPos=PlayerPos+vVelocity*EntitySpawnTime*cry_frand()*0.8;
		EntitySpawnPos=PlayerPos+fVelocity*pPlayerEntity->GetForwardDir()*EntitySpawnTime*1.1;
		EntitySpawnPos.z=gEnv->p3DEngine->GetTerrainElevation(EntitySpawnPos.x,EntitySpawnPos.y);
		return true;
	}
	return false;
}

//------------------------------------------------------------------------
void CodeExclusion::SystemPostSpawn(const float frameTime)
{
	if(SystemEntities.empty())
		return;
	entities_iter=SystemEntities.begin();
	while (entities_iter != SystemEntities.end())
	{
		SystemSpawnEntityParams &params=entities_iter->second;
		params.timer+=frameTime;
		params.glow_amount+=30*frameTime;
		params.SpawnMat->SetGetMaterialParamFloat("glow",params.glow_amount,false);
		if(params.timer >= params.spawntime)
		{
			IEntity *pEntity=gEnv->pEntitySystem->GetEntity(entities_iter->first);
			if(pEntity)
			{
				pEntity->SetMaterial(params.DefaultEntityMat);
				pEntity->EnablePhysics(true);
				DeleteQueue.push_back(pEntity->GetId());
				SystemEntities.erase(entities_iter++);
			}
		}
		else
			++entities_iter;
	}
}

//------------------------------------------------------------------------
void CodeExclusion::DeleteExEntity()
{
	if(DeleteQueue.size() >= 10)
	{
		std::deque<EntityId>::iterator it=DeleteQueue.begin();
		AddToDeleteList(*it);
		DeleteQueue.erase(it);
	}
}

//------------------------------------------------------------------------
void CodeExclusion::DoubleSpawnPointNumber()
{
	SpawnedAIManager *pManager = g_pGame->GetSpawnedAIManager();
	if(pManager)
	{
		//pManager->ResetSpawnCount();
	}
}

//------------------------------------------------------------------------
//Should add a particle effect later(Spawn Effect)
void CodeExclusion::SpawnSystemRobot()
{
	bool FromSpawnSet = !g_pGame->GetSpawnSystem()->GetSpawnEntitySet().empty();
	bool FromRecoverySet = !g_pGame->GetDeleteSystem()->GetDeleteRecoveryInfo().empty();
	if(!FromSpawnSet && !FromRecoverySet)
	{
		CanSpawnSystemRobot=false;
		return;
	}
	IEntity *pEntity(0);
	if(FromSpawnSet)
	{
		std::list<EntityId>::iterator iter=g_pGame->GetSpawnSystem()->GetSpawnEntitySet().begin();
		EntityId FirstEntityId =*iter;
		pEntity=gEnv->pEntitySystem->GetEntity(FirstEntityId);
		if(!pEntity)
			return;
	}
	Vec3 RandomPos;
	if(FromSpawnSet)
		RandomPos = pEntity->GetPos() + BiRandom(Vec3(3,3,3));
	else
		RandomPos = g_pGame->GetDeleteSystem()->GetDeleteRecoveryInfo().begin()->vPosition + BiRandom(Vec3(3,3,3));

	RandomPos.z = gEnv->p3DEngine->GetTerrainElevation(RandomPos.x,RandomPos.y)+1;
	SEntitySpawnParams params;
	params.vPosition=RandomPos;
	params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("SystemRobot");
	params.sName="SystemRobot";
	params.nFlags = ENTITY_FLAG_CLIENT_ONLY | ENTITY_FLAG_CASTSHADOW | ENTITY_FLAG_CALC_PHYSICS;
	IEntity *pRobot=gEnv->pEntitySystem->SpawnEntity(params);
	SEntityPhysicalizeParams pparams;
	pparams.mass = -1;
	pparams.density = -1;
	pparams.nSlot= -1;
	pparams.type = 2;
	pRobot->Physicalize(pparams);
	pRobot->EnablePhysics(true);
	pRobot->SetUpdatePolicy(ENTITY_UPDATE_ALWAYS);
	CanSpawnSystemRobot=false;
}

void CodeExclusion::SystemSpawn(SEntitySpawnParams &params, float _spawntime /*= 5.0f*/)
{
	IEntity *pPrevEntity = gEnv->pEntitySystem->GetEntity(params.prevId);
	if(!pPrevEntity)
		return;
	IScriptTable *pScriptTable = pPrevEntity->GetScriptTable();
	SmartScriptTable pPropertiesTable;
	pScriptTable->GetValue("Properties", pPropertiesTable);
	params.pPropertiesTable = pPropertiesTable;
	IEntity *pEntity = gEnv->pEntitySystem->SpawnEntity(params);
	if(params.prevId)
	{
		g_pGame->GetBasicRayCast()->ClearLastEntity(params.prevId);
		gEnv->pEntitySystem->RemoveEntity(params.prevId);
	}
	params.id = SpawnId;
	while(gEnv->pEntitySystem->IsIDUsed(SpawnId))
		++SpawnId;
	if(pEntity)
	{
		SystemSpawnEntityParams sparams;
		sparams.SpawnMat=gEnv->p3DEngine->GetMaterialManager()->CloneMaterial(DefaultSpawnMat,0);
		sparams.DefaultEntityMat=pEntity->GetMaterial();
		sparams.timer=0;
		sparams.spawntime=_spawntime;
		sparams.glow_amount=5;

		pEntity->EnablePhysics(false);
		pEntity->SetMaterial(sparams.SpawnMat);

		SystemEntities[pEntity->GetId()] = sparams;
	}
	++SpawnId;
}


void CodeExclusion::UpdateUIInfo()
{
	if(!pElement)
		pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	else
	{
		SUIArguments args;
		args.AddArgument(ExclusionAmount);
		args.AddArgument(maxExclusionAmount);
		pElement->CallFunction("SetExclusion",args);
		args.Clear();
	}
}

//------------------------------------------------------------------------
void CodeExclusion::DeleteCrossHairEntity()
{
	BasicRayCast *pBRC = g_pGame->GetBasicRayCast();
	IEntity *pEntity = pBRC->GetEntity();
	if(pEntity)
	{
		AddToDeleteList(pEntity->GetId());
	}
}

//------------------------------------------------------------------------
void CodeExclusion::SetupTrap()
{

}


//------------------------------------------------------------------------
const Vec3 CodeExclusion::RetriveSpawnPosByActorSpeed(CActor *pActor)
{
	IEntity *pActorEntity=pActor->GetEntity();
	if(pActorEntity)
	{
		Vec3 ActorPos=pActorEntity->GetPos();
		const SActorPhysics& actorPhysics = pActor->GetActorPhysics();

		Vec3 vVelocity=actorPhysics.velocity;
		float fVelocity=vVelocity.GetLengthFast();

		EntitySpawnPos=ActorPos+fVelocity*pActorEntity->GetForwardDir()*1.1;
		EntitySpawnPos.z=gEnv->p3DEngine->GetTerrainElevation(EntitySpawnPos.x,EntitySpawnPos.y);
		return EntitySpawnPos;
	}
	return Vec3(0,0,0);
}

//------------------------------------------------------------------------
const Vec3 CodeExclusion::RetriveSpawnPosByActorSpeed(CActor *pActor, float moving_time)
{
	IEntity *pActorEntity=pActor->GetEntity();
	if(pActorEntity)
	{
		Vec3 ActorPos=pActorEntity->GetPos();
		const SActorPhysics& actorPhysics = pActor->GetActorPhysics();

		pe_status_dynamics status;
		pActor->GetEntity()->GetPhysics()->GetStatus(&status);

		Vec3 vVelocity=status.v;
		float fVelocity=vVelocity.GetLengthFast();

		EntitySpawnPos=ActorPos+vVelocity*moving_time;
		EntitySpawnPos.z=gEnv->p3DEngine->GetTerrainElevation(EntitySpawnPos.x,EntitySpawnPos.y);
		return EntitySpawnPos;
	}
	return Vec3(0,0,0);
}

//------------------------------------------------------------------------
void CodeExclusion::CreateDynamicExplosion()
{
	CActor *pPlayer = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetClientActor());
	if(pPlayer)
	{
		float RandomTriggerTime = static_cast<float>(1 + cry_frand() * 1.8);
		Vec3 SpawnPos = RetriveSpawnPosByActorSpeed(pPlayer, RandomTriggerTime);
		if(SpawnPos != Vec3(0,0,0))
		{
			int Id = DynamicExplosionPool.AssignId();

			IParticleEffect *pEffect = gEnv->pParticleManager->FindEffect("Code_Exclusion.Dynamic_Explosion");
			float z_offset = gEnv->p3DEngine->GetTerrainElevation(SpawnPos.x, SpawnPos.y);
			SpawnPos.z = static_cast<float>(z_offset + 1.65);
			IParticleEmitter *pEmitter = pEffect->Spawn(true,IParticleEffect::ParticleLoc(SpawnPos,Vec3(0,0,1)));
			SpawnParams Emitter_Params;
			pEmitter->GetSpawnParams(Emitter_Params);
			Emitter_Params.fTimeScale = 1 / RandomTriggerTime;
			pEmitter->SetSpawnParams(Emitter_Params);
			pEmitter->Activate(true);

			DynamicExplosionParams params;
			params.damage = 300.f + 100.f * CurExclusionLevel;
			params.trigger_time = RandomTriggerTime;
			params.pEmitter = pEmitter;

			DynamicExplosions[Id] = params;
		}
	}
}

//------------------------------------------------------------------------
void CodeExclusion::UpdateDynamicExplosion(float frameTime)
{
	DynamicExplosionMap::iterator iter = DynamicExplosions.begin();
	DynamicExplosionMap::iterator end = DynamicExplosions.end();
	while(iter != end)
	{
		DynamicExplosionParams &the_explosion = iter->second;
		the_explosion.tick_timer += frameTime;
		if(the_explosion.tick_timer >= the_explosion.trigger_time)
		{
			CommonUsage::CreateExplosion(0, the_explosion.pEmitter->GetPos(true), the_explosion.damage, (float)(4 + 0.4 * CurExclusionLevel), 1000);
			gEnv->pParticleManager->DeleteEmitter(the_explosion.pEmitter);
			DynamicExplosionPool.RecoveryId(iter->first);
			DynamicExplosions.erase(iter++);
		}
		else
			++iter;
	}
}

const float CodeExclusion::GetSystemRobotRefreshTime()
{
	int CreateTaskNum = g_pGame->GetSpawnSystem()->GetSpawnEntitySet().size();
	int DeleteTaskNum = g_pGame->GetDeleteSystem()->GetDeleteRecoveryInfo().size();
	int TotalNum = CreateTaskNum + DeleteTaskNum;
	float defaultTime = 300 + BiRandom(10);
	defaultTime -= float(60 * log(TotalNum));
	if(defaultTime < 15)
		defaultTime = 15;
	return defaultTime;
}

const bool CodeExclusion::CheckAndDeleteAbsouredEntity(EntityId Id)
{
	if(CanDeleteCrossHair)
	{
		ExclusionTimers.DeleteCrossHairTimer = 0;
		CanDeleteCrossHair = false;
		AddToDeleteList(Id);
		return true;
	}
	return false;
}