#include "StdAfx.h"
#include "AbilityModule.h"

#include "BattleChallenge.h"
//------------------------------------------------------------------------
AbilityModule::AbilityModule()
{
	//Init
	max_index=3;
	StartFunction=false;
	pWorld=gEnv->pPhysicalWorld;
	ModuleSelected[0]=false;
	ModuleSelected[1]=false;
	ModuleSelected[2]=false;
	query_flag=ent_all;
	pBRC = g_pGame->GetBasicRayCast();
	pRenderer=gEnv->pRenderer;
	color[0] =0.258824f;
	color[1] =0.258824f;
	color[2] =0.435294f;
	color[3] =1.0f;
	text_timer=0;
	ShowChangeText=false;
	pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");

	m_Teleport = new TeleportModule();
	TeleportModule *m_teleport_2 = new TeleportModule();
	m_ShockWave = new ShockWaveModule();
	m_BulletTime = new BulletTimeModule();
	m_Shield = new KineticShieldModule();
	m_Reflection = new ReflectionShieldModule();
	m_ExploShield = new ExplodeShieldModule();
	m_DataBall = new DataBallModule();
	m_Heal = new HealModule();
	m_Backup = new BackupModule();
	m_Rush = new RushModule();
	m_NuclearRad = new NuclearRadModule();
	m_Flood = new FloodModule();
	m_DataBite = new DataBiteModule();
	m_Grab = new GrabModule();
	m_Explosion = new ExplosionModule();
	m_GravityPool = new GravityPoolModule();
	m_Reposition = new RepositionModule();
	m_DataVoid = new DataVoidModule();
	m_DataGuard = new DataGuardModule();
	m_Balt = new BaltModule();
	m_BulletStrom = new BulletStromModule();
	m_BlinkStrike = new BlinkStrikeModule();
	m_DataArrow = new DataArrow();

	g_pGame->GetModuleCraftSystem()->Combine(m_Explosion,m_teleport_2);
	g_pGame->GetModuleCraftSystem()->Combine(m_Teleport,m_Heal);
	InstalledModule.push_back(m_Teleport);
	InstalledModule.push_back(m_GravityPool);
	InstalledModule.push_back(m_DataArrow);
	it=InstalledModule.begin();
}

//------------------------------------------------------------------------
AbilityModule::~AbilityModule()
{

}

//------------------------------------------------------------------------
void AbilityModule::Update(float frameTime)
{
	//For UI testing
	static bool once = false;
	if(!once)
	{
		if(!pElement)
			pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
		if(pElement)
		{
			SUIArguments args;
			args.AddArgument(1);
			args.AddArgument(1);
			pElement->CallFunction("SetSkill",args);
			args.Clear();
			args.AddArgument(2);
			args.AddArgument(2);
			pElement->CallFunction("SetSkill",args);
			args.Clear();
			args.AddArgument(3);
			args.AddArgument(3);
			pElement->CallFunction("SetSkill",args);
			args.Clear();
			once = true;
		}
	}
	pData.frameTime = frameTime;
	pData.hit = pBRC->GetRay_hit();
	IEntity *pActorEntity = gEnv->pEntitySystem->GetEntity(g_pGame->GetClientActorId());
	if(pActorEntity)
	{
		pData.Finish_Pos = pActorEntity->GetPos();
		if(StartFunction)
		{
			BattleChallenge &instance = BattleChallenge::Instance();
			if(instance.GetRestrictType() == eRT_NoAbility)
				instance.FailRestrictChallenge();
			pBRC=g_pGame->GetBasicRayCast();
			PerProcessAllModule(pData);
			ProcessAllModule(pData);
		}
		PostProcessAllModule(pData);
		//ScreenEffect && finished
		AfterFinished(frameTime);
	}

	std::vector<BaseAbility*>::iterator iter=InstalledModule.begin();
	std::vector<BaseAbility*>::iterator end=InstalledModule.end();
	while(iter != end)
	{
		(*iter)->RegularUpdate(pData);
		++iter;
	}
}

//------------------------------------------------------------------------
void AbilityModule::DisplayModule()
{
	/*if(*it==eMT_Teleport)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : Teleport");
	if(*it==eMT_ShockWave)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : ShockWave");
	if(*it==eMT_BulletTime)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : BulletTime");
	if(*it==eMT_Shield)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : Shield");
	if(*it==eMT_Reflection)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : Reflection");
	if(*it==eMT_ExploShield)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : ExploShield");
	if(*it==eMT_DataBall)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : DataBall");
	if(*it==eMT_Heal)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : Heal");
	if(*it==eMT_Backup)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : Backup");
	if(*it==eMT_Rush)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : Rush");
	if(*it==eMT_NuclearRad)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : NuclearRad");
	if(*it==eMT_Flood)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : Flood");
	if(*it==eMT_DataBite)
		pRenderer->Draw2dLabel(50,50,1.5f,color,false,"Module : DataBite");*/
}

//------------------------------------------------------------------------
void AbilityModule::AfterFinished(float frameTime)
{
	SUIArguments args;
	std::vector<BaseAbility*>::iterator iter=InstalledModule.begin();
	std::vector<BaseAbility*>::iterator end=InstalledModule.end();
	int index(1);
	while(iter != end)
	{
		if((*iter)->isFinished())
			(*iter)->Cool_Down(frameTime);
		args.AddArgument(index);
		args.AddArgument((*iter)->GetTimer().Cdtimer);
		args.AddArgument((*iter)->GetTimer().Cd);
		pElement->CallFunction("UpdateSkill",args);
		args.Clear();
		++iter;
		++index;
	}
}

//------------------------------------------------------------------------
const bool AbilityModule::HasShield()
{
	/*for(int i=0;i<3;i++)
	{
		if(InstlledModule[i] == eMT_Shield || InstlledModule[i] == eMT_Reflection || InstlledModule[i] == eMT_ExploShield)
			return true;
	}*/
	return false;
}

void AbilityModule::PerProcessAllModule(Process_Data &_rdata)
{
	(*it)->PreProcess(_rdata);
	/*if(*it==eMT_Teleport && !m_Teleport->isFinished() && !m_Teleport->isPreProcessed())
		m_Teleport->PreProcess();

	if(*it==eMT_ShockWave && !m_ShockWave->isFinished() && !m_ShockWave->isPreProcessed())
		m_ShockWave->PreProcess();

	if(*it==eMT_BulletTime && !m_BulletTime->isFinished())
		m_BulletTime->PreProcess();

	if(*it==eMT_Shield && !m_Shield->isFinished())
		m_Shield->PreProcess();

	if(*it==eMT_Reflection && !m_Reflection->isFinished())
		m_Reflection->PreProcess();

	if(*it==eMT_ExploShield && !m_ExploShield->isFinished())
		m_ExploShield->PreProcess();
			
	if(*it==eMT_DataBall && !m_DataBall->isPreProcessed() && !m_DataBall->isFinished())
		m_DataBall->PreProcess();

	if(*it==eMT_Heal && !m_Heal->isPreProcessed() && !m_Heal->isFinished())
		m_Heal->PreProcess();

	if(*it==eMT_Backup && !m_Backup->isPreProcessed() && !m_Backup->isFinished())
		m_Backup->PreProcess();

	if(*it==eMT_Rush && !m_Rush->isPreProcessed() && !m_Rush->isFinished())
		m_Rush->PreProcess();

	if(*it==eMT_NuclearRad && !m_NuclearRad->isPreProcessed() && !m_NuclearRad->isFinished())
		m_NuclearRad->PreProcess();

	if(*it==eMT_Flood && !m_Flood->isPreProcessed() && !m_Flood->isFinished())
		m_Flood->PreProcess();

	if(*it==eMT_DataBite && !m_DataBite->isPreProcessed() && !m_DataBite->isFinished())
		m_DataBite->PreProcess();

	if((*it==eMT_Grab && !m_Grab->isPreProcessed() && !m_Grab->isFinished()) || m_Grab->IsGrab())
		m_Grab->PreProcess(pBRC->GetRay_hit());*/
}

void AbilityModule::ProcessAllModule(Process_Data &_rdata)
{
	(*it)->Process(_rdata);
	/*if(*it==eMT_Teleport && m_Teleport->isPreProcessed())
		m_Teleport->Process(pBRC->GetRay_hit());

	if(*it==eMT_ShockWave && m_ShockWave->isPreProcessed())
		m_ShockWave->Process();

	if(*it==eMT_BulletTime && m_BulletTime->isPreProcessed())
		m_BulletTime->Process();

	if(*it==eMT_Shield && m_Shield->isPreProcessed())
		m_Shield->Process();

	if(*it==eMT_Reflection && m_Reflection->isPreProcessed())
		m_Reflection->Process();

	if(*it==eMT_ExploShield && m_ExploShield->isPreProcessed())
		m_ExploShield->Process();

	if(*it==eMT_DataBall && m_DataBall->isPreProcessed())
		m_DataBall->Process(pBRC->GetRay_hit());

	if(*it==eMT_Heal && m_Heal->isPreProcessed())
		m_Heal->Process();

	if(*it==eMT_Backup && m_Backup->isPreProcessed())
		m_Backup->Process();

	if(*it==eMT_Rush && m_Rush->isPreProcessed())
		m_Rush->Process(pBRC->GetRay_hit(),frameTime);

	if(*it==eMT_NuclearRad && m_NuclearRad->isPreProcessed())
		m_NuclearRad->Process(pBRC->GetRay_hit());

	if(*it==eMT_Flood && m_Flood->isPreProcessed())
		m_Flood->Process(pBRC->GetRay_hit());

	if(*it==eMT_DataBite && m_DataBite->isPreProcessed())
		m_DataBite->Process();

	if(*it==eMT_Grab && m_Grab->isPreProcessed() && !m_Grab->IsGrab() && !m_Grab->IsDetected())
		m_Grab->Process(pBRC->GetRay_hit());*/
}

void AbilityModule::PostProcessAllModule(Process_Data &_rdata)
{
	std::vector<BaseAbility*>::iterator iter=InstalledModule.begin();
	std::vector<BaseAbility*>::iterator end=InstalledModule.end();
	while(iter != end)
	{
		if(!StartFunction)
		{
			(*iter)->PostProcess(_rdata);
		}
			++iter;
	}

	// For DataBite
	// No better way by now
	/*if(m_DataBite->isSet())
		m_DataBite->PostProcess(frameTime);*/
}
