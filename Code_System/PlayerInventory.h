#ifndef _PLAYERINVENTORY_H_
#define _PLAYERINVENTORY_H_

#include "CommonUsage.h"

enum InventoryType
{
	eIT_BluePrint,
	eIT_Enhancement,
	eIT_ParticleEnhance,
};

typedef unsigned int InventoryId;

class BaseInventory
{
public:
	BaseInventory(){};
	BaseInventory(char *_name, int _amount, InventoryType _type);
	BaseInventory(const BaseInventory &cps);
	BaseInventory &operator=(const BaseInventory &rhs);
	const bool operator==(const BaseInventory &rhs){return Id == rhs.Id;}
	virtual ~BaseInventory(){}
	const char * GetInventoryName() const{return InventoryName;}
	char *GetInventoryName(){return InventoryName;}
	const int GetInventoryAmount(){return InventoryAmount;}
	const int GetMaxAmount(){return maxAmount;}
	const InventoryType GetInventoryType(){return Type;}
	const InventoryId GetId(){return Id;}
	void SetId(InventoryId _Id){Id = _Id;}
	void ChangeAmount(int _amount){InventoryAmount += _amount;}
	void SetAmount(int _amount){InventoryAmount = _amount;}
private:
	InventoryId Id;
	char *InventoryName;
	int InventoryAmount;
	InventoryType Type;
	unsigned int maxAmount;
};


class PlayerInventory
{
public:
	PlayerInventory();
	~PlayerInventory();

	const bool AddNewInventory(BaseInventory *new_inventory);
	const bool RemoveInventory(BaseInventory *dump_inventory);
	const bool SpliteInventory(InventoryId _Id, int num);
	const bool CombineInventory(InventoryId _parent_id, InventoryId _child_id);
private:
	const bool ChangeInventory(BaseInventory *new_inventory);
	std::map<InventoryId,BaseInventory*> Inventories;
	IdPool InventoryIdPool;
};


#endif