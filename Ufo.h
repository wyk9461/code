//Bullet 
//Shoot enemy around five times when lunched

#ifndef _UFO_H_
#define _UFO_H_

#include "StdAfx.h"
#include "Projectile.h"


class CUfo : public CProjectile
{
private:
	typedef CProjectile BaseClass;

public:
	CUfo();
	~CUfo();
	virtual void HandleEvent(const SGameObjectEvent &);
	virtual void Update(SEntityUpdateContext &ctx, int updateSlot);

private:
	float existTime;
	float fireTime;
	std::map<IEntity*, int> tarMap;
	bool bMap;
};
#endif 