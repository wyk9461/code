#include "StdAfx.h"
#include "LockGun.h"

#include "FireMode.h"


CLockGun::CLockGun()
	:	distance(20)
	,	time(0)
	,	lockEntityId(NULL)
	,	pLockedEntity(NULL)
{
	lockPos = Vec3(ZERO);
}

CLockGun::~CLockGun()
{

}

void CLockGun::Update(SEntityUpdateContext& ctx, int slot)
{
	BaseClass::Update(ctx,slot);

	MyLock(ctx.fFrameTime);

	if (pLockedEntity)
	{
		/////////////////////////////debug/////////////////////////////////
		//EntityId pLockedEntityId = pLockedEntity->GetId();
		//CryLogAlways("Lock:%d",pLockedEntityId);
		///////////////////////////////////////////////////////////////////

		m_fm->GetLockEntity(pLockedEntity);
	}
}

void CLockGun::MyLock(float frametime)
{
	IActor *pSelfActor=g_pGame->GetIGameFramework()->GetClientActor();
	if(pSelfActor)
	{
		CActor *pActor=(CActor*)g_pGame->GetIGameFramework()->GetClientActor();
		IEntity *pSelf=pSelfActor->GetEntity();
		IMovementController *pMC=pSelfActor->GetMovementController();
		IPhysicalEntity *pSelfPhysics=pSelf->GetPhysics();
		SMovementState info;
		pMC->GetMovementState(info);
		EntityId aimEntityId = NULL;
		
		ray_hit hit;
		IPhysicalWorld *pWorld = gEnv->pPhysicalWorld;
		int query_flag = ent_rigid | ent_sleeping_rigid | ent_living | ent_independent | ent_static;
		int rayFlags = rwi_stop_at_pierceable|rwi_colltype_any;

		int hits = pWorld->RayWorldIntersection(info.eyePosition,info.eyeDirection*distance,query_flag,rayFlags,&hit,1,&pSelfPhysics,1);

		if(hit.pCollider)
		{
			IEntity *pEntity=(IEntity*)hit.pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);
			if(pEntity)
			{
				aimEntityId = pEntity->GetId();
				if (time == 0)
				{
					lockEntityId = aimEntityId;
					time += frametime;
				}
				else
				{
					if (aimEntityId == lockEntityId)
					{
						time += frametime;
						if (time > 5)		//瞄准5秒后锁定目标
						{
							pLockedEntity = pEntity;
							lockPos = pLockedEntity->GetPos();
							time = 0;
						}
					}
					else
					{
						time = 0;
					}
				}
			}
		}

		Vec3 mark(ZERO);
		if (!gEnv->pRenderer->GetCamera().Project(lockPos,mark))	//当实体移出镜头时取消锁定
		{
			pLockedEntity = NULL;
			m_fm->GetLockEntity(pLockedEntity);
		}
	}
}