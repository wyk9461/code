#include "StdAfx.h"
#include "Unnamed1.h"
#include "Weapon.h"
#include "Code_System/WorldStability.h"

CUnnamed1::CUnnamed1()
	: CDTime(0)
	, bCD(false)
	, m_pAmmoType(NULL)
{
}

CUnnamed1::~CUnnamed1()
{
}


void CUnnamed1::OnShoot(EntityId shooterId, EntityId ammoId, IEntityClass* pAmmoType, const Vec3 &pos, const Vec3 &dir, const Vec3 &vel)
{
	BaseClass::OnShoot(shooterId, ammoId, pAmmoType, pos, dir, vel);
	
	g_pGame->GetWorldStability()->StabilityCost(100);
	
	SetAmmoCount(pAmmoType, 2);

}

void CUnnamed1::StartFire()
{
	if (bCD)
	{
		BaseClass::StartFire();
		bCD = false;
		BaseClass::StopFire();
	}
}

void CUnnamed1::Update(SEntityUpdateContext& ctx, int slot)
{
	BaseClass::Update(ctx,slot);
	if (bCD == false)
	{
		CDTime += ctx.fFrameTime;

		if (CDTime > 6)
		{
			bCD = true;
			CDTime = 0;
		}
	}
}