#include "StdAfx.h"
#include "EntityMode.h"
#include "Projectile.h"

CRY_IMPLEMENT_GTI(CEntityMode, CSingle);

CEntityMode::CEntityMode()
{

}


bool CEntityMode::Shoot(bool resetAnimation, bool autoreload, bool isRemote)
{
	IEntityClass* pAmmoClass = m_fireParams->fireparams.ammo_type_class;

	const EntityId ownerEntityId = m_pWeapon->GetOwnerId();
	CActor *pActor = m_pWeapon->GetOwnerActor();

	const bool playerIsShooter = pActor ? pActor->IsPlayer() : false;
	const bool clientIsShooter = pActor ? pActor->IsClient() : false;

	const int clipSize = GetClipSize();
	int ammoCount = (clipSize != 0) ? m_pWeapon->GetAmmoCount(pAmmoClass) : m_pWeapon->GetInventoryAmmoCount(pAmmoClass);

	m_firePending = false;
	if (!CanFire(true))
	{
		if ((ammoCount <= 0) && !m_reloading && !m_reloadPending)
		{
			m_pWeapon->PlayAction(GetFragmentIds().empty_clip);
			m_pWeapon->OnFireWhenOutOfAmmo();
		}
		return false;
	}

	bool bHit = false;
	ray_hit rayhit;	 
	rayhit.pCollider = 0;

	Vec3 hit = GetProbableHit(WEAPON_HIT_RANGE, &bHit, &rayhit);
	Vec3 pos = GetFiringPos(hit);
	Vec3 dir = GetFiringDir(hit, pos);				//Spread was already applied if required in GetProbableHit( )
	Vec3 vel = GetFiringVelocity(dir); 

	PlayShootAction(ammoCount);

	CheckNearMisses(hit, pos, dir, (hit-pos).len(), 1.0f);

	int ammoCost = GetShootAmmoCost(playerIsShooter);
	ammoCost = min(ammoCount, ammoCost);

	EntityId ammoEntityId = 0;
	int ammoPredicitonHandle = 0;
	CProjectile *pAmmo = m_pWeapon->SpawnAmmo(m_fireParams->fireparams.spawn_ammo_class, false);
	
	IEntity *pEntity = m_pWeapon->GetBasicEntity(); 
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	//Code System(Weapon)
	//Launch the object
	if (pAmmo&&pEntity)
	{
		AABB aabb;
		m_pWeapon->GetEntity()->GetWorldBounds(aabb);
		Vec3 weaponCenterPos = aabb.GetCenter();
		float weaponRadius = aabb.GetRadius();

		pEntity->GetWorldBounds(aabb);
		float entityRadios = aabb.GetRadius();
		dir.normalize();

		Vec3 entityPos = dir*(entityRadios + weaponRadius);
		pEntity->Hide(false);
		
		LaunchEntity(pEntity,dir);
		
		
		pAmmo->SetAmmoCost(ammoCost);
	}

	if (m_pWeapon->IsServer())
	{
		const char *ammoName = pAmmoClass != NULL ? pAmmoClass->GetName() : NULL;
		g_pGame->GetIGameFramework()->GetIGameplayRecorder()->Event(m_pWeapon->GetOwner(), GameplayEvent(eGE_WeaponShot, ammoName, 1, (void *)(EXPAND_PTR)m_pWeapon->GetEntityId()));
	}

	m_muzzleEffect.Shoot(this, hit, m_barrelId);
	RecoilImpulse(pos, dir);

	m_fired = true;
	SetNextShotTime(m_next_shot + m_next_shot_dt);

	if (++m_barrelId == m_fireParams->fireparams.barrel_count)
	{
		m_barrelId = 0;
	}

	ammoCount -= ammoCost;
	if (ammoCount <= m_fireParams->fireparams.minimum_ammo_count)
		ammoCount = 0;

	if (clipSize != -1)
	{
		if (clipSize!=0)
			m_pWeapon->SetAmmoCount(pAmmoClass, ammoCount);
		else
			m_pWeapon->SetInventoryAmmoCount(pAmmoClass, ammoCount);
	}

	const EntityId shooterEntityId = ownerEntityId ? ownerEntityId : m_pWeapon->GetHostId();
	OnShoot(shooterEntityId, ammoEntityId, pAmmoClass, pos, dir, vel);

	const SThermalVisionParams& thermalParams = m_fireParams->thermalVisionParams;
	m_pWeapon->AddShootHeatPulse(pActor, thermalParams.weapon_shootHeatPulse, thermalParams.weapon_shootHeatPulseTime,
		thermalParams.owner_shootHeatPulse, thermalParams.owner_shootHeatPulseTime);
	if (OutOfAmmo())
	{
		OnOutOfAmmo(pActor, autoreload);
	}

	m_pWeapon->RequestShoot(pAmmoClass, pos, dir, vel, hit, m_speed_scale, ammoPredicitonHandle, false);

#if defined(ANTI_CHEAT)
	if(pAmmo)
	{
		const uint32 shotId = m_pWeapon->GetLastShotId();
		pAmmo->SetShotId(shotId);
	}
#endif
	return true;
}


void CEntityMode::GetMemoryUsage(ICrySizer * s) const
{ 
	s->AddObject(this, sizeof(*this));	
	BaseClass::GetInternalMemoryUsage(s);		// collect memory of parent class
}

void CEntityMode::LaunchEntity(IEntity* pEntity, Vec3 dir)
{
	AABB aabb;
	
	Vec3 GunPos = GetWeapon()->GetSlotHelperPos(eIGS_FirstPerson,"silencer_attach",true);
	pEntity->EnablePhysics(true);

	pEntity->GetWorldBounds(aabb);

	float entityRadios = aabb.GetRadius();
	float volume = aabb.GetVolume();
	//float diff = pow((float)(0.5 / volume),0.33);
	dir.normalize();

	Vec3 entityPos = GunPos + dir * (entityRadios);
	pEntity->Hide(false);
	pEntity->SetScale(Vec3(1));
	pEntity->SetPos(entityPos);
	/*IEntityRenderProxy *pRenderProxy=(IEntityRenderProxy*)pEntity->GetProxy(ENTITY_PROXY_RENDER);
	pRenderProxy->SetOpacity(1);*/
	float mass = m_pWeapon->GetBasicEntityMass();
	float times = mass;
	dir.normalized();
	IEntityPhysicalProxy* pp=(IEntityPhysicalProxy*)pEntity->GetProxy(ENTITY_PROXY_PHYSICS);
	pp->AddImpulse(-1,entityPos,dir*times*20,true,1,10);
}
