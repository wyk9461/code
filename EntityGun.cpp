#include "StdAfx.h"
#include "Weapon.h"
#include "EntityGun.h"
#include "Code_System/WorldStability.h"
#include "Code_System/EntityProjectile.h"
#include "WeaponSharedParams.h"
#include "FireMode.h"
#include "Code_System/MicroSystem/ProbeManager.h"
#include "Code_System/CodeExclusion.h"

IParticleEffect *CEntityGun::pObsorbParticle = NULL;
IParticleEffect *CEntityGun::pHideParticle = NULL;

CEntityGun::CEntityGun()
	: curType(0)
{
	pObsorbParticle = NULL;
	pEmitter = NULL;
	pIndicateEntity = NULL;
}

CEntityGun::~CEntityGun()
{

}

void CEntityGun::Update(SEntityUpdateContext& ctx, int num)
{
	BaseClass::Update(ctx,num);
	UpdateIndicatrEntity();
	if(!UpdateList.empty())
	{
		std::list<EntityUpdateParams>::iterator iter = UpdateList.begin();
		std::list<EntityUpdateParams>::iterator end = UpdateList.end();
		while(iter != end)
		{
			EntityUpdateParams &params = *iter;
			if(params.tick_timer < params.erase_time)
			{
				AABB aabb;
				params.pEntity->SetScale(Vec3(params.scale));
				params.pEntity->GetWorldBounds(aabb);
				Vec3 diff = params.Center - aabb.GetCenter();
				params.pEntity->SetPos(params.pEntity->GetPos() + diff);
				params.scale -= ctx.fFrameTime;
				params.tick_timer += ctx.fFrameTime;
				IEntityRenderProxy *pRenderProxy=(IEntityRenderProxy*)params.pEntity->GetProxy(ENTITY_PROXY_RENDER);
				pRenderProxy->SetOpacity(1 - params.tick_timer);
				++iter;
			}
			else
			{
				params.pEntity->SetScale(Vec3(1.0f));
				params.pEntity->Hide(true);
				IEntityRenderProxy *pRenderProxy=(IEntityRenderProxy*)params.pEntity->GetProxy(ENTITY_PROXY_RENDER);
				pRenderProxy->SetOpacity(1);
				UpdateList.erase(iter++);
			}
		}
	}
}

//-------------------------------------------------------------------------------------
bool CEntityGun::CanReload() const
{
	bool checkFlag = (m_AmmoMap.size() - 1) > 0;
	if (checkFlag)
	{
		return true;
	}
	return false;
}

//-------------------------------------------------------------------------------------
bool CEntityGun::OnActionTransformEntity(EntityId actorId, const ActionId& actionId, int activationMode, float value)
{
	if (activationMode == eAAM_OnPress)
	{
		if(!ProbeManager::Instance().IsProbeActived() && g_pGame->GetConnectMode()->GetMode() == eConnectMode::eCMode::Normal)
		{
			AbsorbEntity();
		}
		return true;
	}

	return false;
}

//-------------------------------------------------------------------------------------
void CEntityGun::OnShoot(EntityId shooterId, EntityId ammoId, IEntityClass* pAmmoType, const Vec3 &pos, const Vec3 &dir, const Vec3 &vel)
{
	BaseClass::OnShoot(shooterId, ammoId, pAmmoType, pos, dir, vel);
	--m_AmmoMap[curType].ammo_count;
}

//-------------------------------------------------------------------------------------
IEntity* CEntityGun::GetBasicEntity()
{
	if (m_AmmoMap.empty())
	{
		return NULL;
	}
	std::map<int,BaseEntityParames>::iterator it;
	it = m_AmmoMap.begin();
	IEntity* pEntity = it->second.pEntity;
	return pEntity;
}

//-------------------------------------------------------------------------------------
float CEntityGun::GetBasicEntityMass()
{
	if (m_AmmoMap.empty())
	{
		return 0;
	}
	std::map<int,BaseEntityParames>::iterator it;
	it = m_AmmoMap.begin();
	float entityMass = it->second.mass;
	return entityMass;
}

//-------------------------------------------------------------------------------------
int CEntityGun::GetProjectileType()
{
	return curType;
}

//-------------------------------------------------------------------------------------
void CEntityGun::DeleteBasicEntityClip(int type)
{
	m_AmmoMap.erase(type);
}

//-------------------------------------------------------------------------------------
void CEntityGun::SetCurType(int type)
{
	curType = type;
}

//-------------------------------------------------------------------------------------
int CEntityGun::GetEntityAmmoCount(int type)
{
	if (m_AmmoMap.empty())
	{
		return 0;
	}
	int ammo_count = m_AmmoMap[type].ammo_count;
	return ammo_count;
}

//-------------------------------------------------------------------------------------
float CEntityGun::GetDamage(int type)
{
	if(!m_AmmoMap.count(type))
	{
		return 0;
	}
	return m_AmmoMap[type].damage;
}

//-------------------------------------------------------------------------------------
int CEntityGun::GetStabilityCost(IEntity* pEntity)
{
	if (m_AmmoMap.empty())
	{
		return 0;
	}
	float num = GetEffect(pEntity);
	return static_cast<int>(num);
}

//-------------------------------------------------------------------------------------
//Note:Check data here for balance
float CEntityGun::GetEffect(IEntity* pEntity)
{
	pe_status_dynamics params;
	pEntity->GetPhysics()->GetStatus(&params);
	float &mass = params.mass;
	
	AABB aabb;
	pEntity->GetWorldBounds(aabb);
	float volume = aabb.GetVolume();
	
	float pram = 10;//调整数值平衡，实际值待定

	return mass/volume*pram;
}

//-------------------------------------------------------------------------------------
void CEntityGun::SetAmmoSlot(IEntity* pEntity,int type, int ammo_count)
{
	if (!m_AmmoMap.count(type))
	{
		m_fm->PushEntityBulletClipDeque(type);
	}

	pe_status_dynamics params;
	pEntity->GetPhysics()->GetStatus(&params);

	SEntityPhysicalizeParams pParmes;
	//如果实体没有质量添加默认值
	if (params.mass <= 0)
		pParmes.mass = 100;
	else
		pParmes.mass = params.mass;

	pParmes.nSlot = -1;
	pParmes.type = 2;
	pEntity->Physicalize(pParmes);
	pEntity->SetUpdatePolicy(ENTITY_UPDATE_ALWAYS);

	BaseEntityParames entityParmes = BaseEntityParames(pEntity, pParmes.mass, type);

	entityParmes.damage = CalculateDamage(pEntity);
	entityParmes.ammo_count = ammo_count;

	if(!m_AmmoMap.count(type))
		m_AmmoMap[type] = entityParmes;
	else
		m_AmmoMap[type].ammo_count = ammo_count;

	m_fm->SetCurrType(type);
	curType = type;
}

//-------------------------------------------------------------------------------------
//Note:Need some animation or some massage shows to player when clips are full
void CEntityGun::AbsorbEntity()
{
	IEntity *pEntity = g_pGame->GetBasicRayCast()->GetEntity();
	if(pEntity && !CodeExclusion::Instance().CheckAndDeleteAbsouredEntity(pEntity->GetId()))
	{
		IEntityClass *pHMMWV = gEnv->pEntitySystem->GetClassRegistry()->FindClass("HMMWV");
		IEntityClass *pHuman = gEnv->pEntitySystem->GetClassRegistry()->FindClass("Human");

		if (pEntity && pEntity->GetClass() != pHMMWV && pEntity->GetClass() != pHuman &&
			pEntity->GetId() != GetOwnerId() && GetEntityType(pEntity) > -1 && !IsBusy())
		{
			IEntityClass* pAmmoType = m_fm->GetAmmoType();

			//attention!!!!!!!!!!!!!!!!
			//need a upper limit for type
			int type = -1;
			type =	GetEntityType(pEntity);
			if (type == -1 )
			{
				type = curType;
			}

			EntityProjectileParams &rParams = EntityProjectileManager::Instance().GetProjectileParams(type);
			SFireModeParams* tFireModeParams=m_fm->GetFireModeParams_Non_Const();
			SFireParams &rBasicfireParams=const_cast<SFireParams&>(tFireModeParams->fireparams);
			SSpreadParams &spreadParams = const_cast<SSpreadParams&>(tFireModeParams->spreadparamsCopy);
			SRecoilParams &recoilParams = const_cast<SRecoilParams&>(tFireModeParams->recoilparamsCopy);

			int old_clipSize = m_fm->GetClipSize();

			rBasicfireParams.clip_size = rParams.clipsize;

			//SRapidParams &rRapid=const_cast<SRapidParams&>(tFireModeParams->rapidparams);
			int clipSize = m_fm->GetClipSize();

			int ammoCount = 0;
			if(m_AmmoMap.count(type))
			{
				ammoCount = m_AmmoMap[type].ammo_count;
			}

			if (ammoCount < clipSize)
			{
				//The Params change with Current type
				rBasicfireParams.rate = rParams.firerate;
				spreadParams.min = rParams.min_spread;
				spreadParams.max = rParams.max_spread;
				spreadParams.attack = rParams.attack_spread;
				recoilParams.max_recoil = rParams.max_recoil;

				//g_pGame->GetDeleteSystem()->ProcessDeleteEntityForFastDelete(pEntity);
				SpawnParticle(pEntity);
				int new_count = ammoCount + rParams.ammo_count;
				if(new_count > clipSize)
					new_count = clipSize;

				SetAmmoSlot(pEntity,type,new_count);

				pEntity->EnablePhysics(false);
				AABB aabb;
				pEntity->GetWorldBounds(aabb);
				EntityUpdateParams uparams;
				uparams.pEntity = pEntity;
				uparams.Center = aabb.GetCenter();
				UpdateList.push_back(uparams);

				//pEntity->Hide(true);
				int cost = GetStabilityCost(pEntity);
				//消耗稳定度
				g_pGame->GetWorldStability()->StabilityCost(cost / 100);
				//弹夹内子弹数增加
				SetAmmoCount(pAmmoType, new_count);

				g_pGame->GetDeleteSystem()->AddToDeleteRecovery(pEntity,false);
				SpawnIndicateEntity(pEntity);

				GenerateUIMessage(type, (int)m_AmmoMap[type].damage);
			}
			else
			{
				rBasicfireParams.clip_size = old_clipSize;
				//弹夹已满
			}
		}
	}
}

//-------------------------------------------------------------------------------------
//Note:Check data here for balance
float CEntityGun::CalculateDamage(IEntity *pEntity)
{
	pe_status_dynamics params;
	pEntity->GetPhysics()->GetStatus(&params);
	float &mass = params.mass;

	AABB aabb;
	pEntity->GetWorldBounds(aabb);
	float volume = aabb.GetVolume();

	float pram = 10;//调整数值平衡，实际值待定

	return mass/volume*pram;
}

//-------------------------------------------------------------------------------------
int CEntityGun::GetEntityType(IEntity *pEntity)
{
	if(!pEntity)
		return 0;
	SmartScriptTable root = pEntity->GetScriptTable();
	SmartScriptTable props;

	int type = -1;
	root->GetValue("Properties", props);
	props->GetValue("ProjectileType", type);

	return type;
}

//UI Interface 
//-------------------------------------------------------------------------------------
void CEntityGun::SpawnParticle(IEntity *pEntity)
{
	if(!pEntity)
		return;

	if(!pHideParticle)
		pHideParticle=gEnv->pParticleManager->FindEffect("EntityGun.HideEffect");
	if(!pObsorbParticle)
		pObsorbParticle=gEnv->pParticleManager->FindEffect("EntityGun.Absorb");

	AABB aabb;
	pEntity->GetWorldBounds(aabb);

	Vec3 vDestination = GetSlotHelperPos(eIGS_FirstPerson,"silencer_attach",true);


	float length = (vDestination - aabb.GetCenter()).GetLengthFast();
	float speed = length / 5;
	Vec3 dir = (vDestination - aabb.GetCenter()).normalize();

	pHideParticle->Spawn(false,IParticleEffect::ParticleLoc(aabb.GetCenter(),dir));
	pEmitter = pObsorbParticle->Spawn(false,IParticleEffect::ParticleLoc(aabb.GetCenter(),dir));
	SpawnParams Emitter_Params;
	pEmitter->GetSpawnParams(Emitter_Params);
	Emitter_Params.eAttachType = GeomType_None;
	Emitter_Params.fSpeedScale = speed;
	pEmitter->SetSpawnParams(Emitter_Params);

	pEmitter->Activate(true);
}

//-------------------------------------------------------------------------------------
void CEntityGun::SpawnIndicateEntity(IEntity *pEntity)
{
	if(!pIndicateEntity)
	{
		AABB aabb;
		pEntity->GetWorldBounds(aabb);
		float volume = aabb.GetVolume();
		float diff = (float)pow((float)(0.0005 / volume),0.33);

		SEntitySpawnParams params;
		params.sName = "EGINDICA";

		params.vPosition=GetSlotHelperPos(eIGS_FirstPerson,"silencer_attach",true);
		params.vScale=Vec3(diff);
		params.qRotation=Quat::CreateRotationX(DEG2RAD(90));
		params.pClass=gEnv->pEntitySystem->GetClassRegistry()->FindClass("BasicEntity");
		pIndicateEntity=gEnv->pEntitySystem->SpawnEntity(params);

		char* Model_Name;
		SmartScriptTable OriginRoot = pEntity->GetScriptTable();
		SmartScriptTable OriginProps;
		OriginRoot->GetValue("Properties",OriginProps);
		if(OriginProps->HaveValue("object_Model"))
		{
			OriginProps->GetValue("object_Model",Model_Name);

			IMaterial *pMaterial = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("spawn");
			IMaterial *cloneMat = gEnv->p3DEngine->GetMaterialManager()->CloneMaterial(pMaterial,0);
			pIndicateEntity->SetMaterial(cloneMat);

			pIndicateEntity->LoadGeometry(0,Model_Name);
			pIndicateEntity->EnablePhysics(false);
		}
	}
	else
	{
		AABB aabb;
		pEntity->GetWorldBounds(aabb);
		float volume = aabb.GetVolume();
		float diff = (float)pow((float)(0.0005 / volume),0.33);

		char* Model_Name;
		SmartScriptTable OriginRoot = pEntity->GetScriptTable();
		SmartScriptTable OriginProps;
		OriginRoot->GetValue("Properties",OriginProps);
		if(OriginProps->HaveValue("object_Model"))
		{
			OriginProps->GetValue("object_Model",Model_Name);

			pIndicateEntity->LoadGeometry(0,Model_Name);
			pIndicateEntity->SetPos(GetSlotHelperPos(eIGS_FirstPerson,"silencer_attach",true));
			pIndicateEntity->SetScale(Vec3(diff));
			pIndicateEntity->EnablePhysics(false);
		}
	}
}

//-------------------------------------------------------------------------------------
void CEntityGun::UpdateIndicatrEntity()
{
	if(pIndicateEntity)
	{
		//空指针
		IEntity *pEntity = gEnv->pEntitySystem->GetEntity(GetOwnerId());
		if (pEntity)
		{
			Vec3 fwddir = pEntity->GetForwardDir();
			Matrix34 mat = GetEntity()->GetLocalTM();
			Vec3 UpDir = Vec3(-mat.m20,-mat.m21,mat.m22);
			Vec3 left_dir = -fwddir.Cross(Vec3(0,0,1));

			Vec3 Pos = GetSlotHelperPos(eIGS_FirstPerson,"silencer_attach",true);
			AABB aabb;
			pIndicateEntity->GetWorldBounds(aabb);
			Vec3 Center = aabb.GetCenter();
			Vec3 diff = pIndicateEntity->GetPos() - Center;
			pIndicateEntity->SetPos(Pos + diff + left_dir*0.2);
		}
	}
}

//-------------------------------------------------------------------------------------
void CEntityGun::GenerateUIMessage(int type, int damage)
{
	string temp_str;
	temp_str.reserve(30);
	char *temp_1 = new char[3];
	char *temp_2 = new char[10];
	/*char temp_1[3];
	char temp_2[5];*/
	itoa(type,temp_1,10);
	itoa(damage,temp_2,10);
	temp_str = "Ammo Type_";
	temp_str += temp_1;
	temp_str += " Dmg: ";
	temp_str += temp_2;
	IUIElement *pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	if(pElement)
	{
		SUIArguments args;
		args.AddArgument(temp_str.c_str());
		args.AddArgument(5);
		pElement->CallFunction("AddMessage",args);
	}

	delete [] temp_1;
	delete [] temp_2;
}