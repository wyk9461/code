#ifndef __FISSIONGRENADE_H__
#define __FISSIONGRENADE_H__

#include "Grenade.h"

class CFissionGrenade : public CGrenade
{
private:
	typedef CGrenade BaseClass;

public:

	CFissionGrenade();
	~CFissionGrenade();

	virtual void Explode(const CProjectile::SExplodeDesc& explodeDesc);
	virtual void HandleEvent(const SGameObjectEvent &event);
	virtual void SetParams(const SProjectileDesc& projectileDesc){CProjectile::SetParams(projectileDesc);}
	virtual void SetParams(const SProjectileDesc& projectileDesc, int fissionTimes);

private:

	int m_fissionTimes;
};

#endif