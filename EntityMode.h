//For EntityGun
//launch Entity

#ifndef __ENTITYMODE_H__
#define __ENTITYMODE_H__

#if _MSC_VER > 1000
# pragma once
#endif


#include "Single.h"

class CEntityMode : public CSingle
{

private:
	typedef CSingle BaseClass;

public:
	CRY_DECLARE_GTI(CEntityMode);

	CEntityMode();
	virtual ~CEntityMode(){};
	virtual bool Shoot(bool resetAnimation, bool autoreload, bool isRemote=false);
	virtual void GetMemoryUsage(ICrySizer * s) const;

private:
	void LaunchEntity(IEntity* pEntity, Vec3 dir);

};

#endif 