#ifndef _SMASHGUN_H_
#define _SMASHGUN_H_

#include "Weapon.h"

class CSmashGun : public CWeapon
{
private:

	typedef CWeapon BaseClass;

public:

	CSmashGun();
	~CSmashGun();


	virtual void StartFire();
	virtual void Update(SEntityUpdateContext& ctx, int slot);
	virtual void OnShoot(EntityId shooterId, EntityId ammoId, IEntityClass* pAmmoType, const Vec3 &pos, const Vec3 &dir, const Vec3 &vel);
	virtual bool IsMounted() const;

private:

	IEntityClass* pAmmoType;
	int fireLimit;
	float startTime;
	float incTime;
	bool bInc;			//ammo increase control
	bool bStartFire;	//auto fire control
};

#endif