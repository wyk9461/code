//Use for AOE Weapon

#ifndef _LOCKGUN_H_
#define _LOCKGUN_H_

#include "Weapon.h"
#include "Laser.h"

class CLockGun : public CWeapon
{
private:

	typedef CWeapon BaseClass;

public:
	CLockGun();
	virtual ~CLockGun();

	virtual void Update(SEntityUpdateContext& ctx, int slot);
	
private:
	void MyLock(float fFrameTime);
	void MyLockAnimation();

private:
	float distance;
	float time;
	EntityId lockEntityId;
	IEntity *pLockedEntity;
	Vec3 lockPos;
};


#endif // _JAW_H_
