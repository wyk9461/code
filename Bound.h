#ifndef _BOUND_H_
#define _BOUND_H_

#include "StdAfx.h"
#include "Projectile.h"

class CBound : public CProjectile
{
public:
	CBound();
	~CBound();
	virtual void HandleEvent(const SGameObjectEvent &);
	virtual void SetParams(const SProjectileDesc& projectileDesc){CProjectile::SetParams(projectileDesc);}
	virtual void SetParams(const SProjectileDesc& projectileDesc, int count,float damage_,const Vec3 &start_pos);
private:
	int hitCount;
	float damage;
	Vec3 startPos;
	enum {maxCount = 5};		//how much times reflection
};
#endif 