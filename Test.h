#ifndef _TEST_H_
#define _TEST_H_

#include "StdAfx.h"
#include "Projectile.h"

class CTest : public CProjectile
{
public:
	CTest();
	~CTest();
	virtual void HandleEvent(const SGameObjectEvent &);
	virtual void ProcessEvent(SEntityEvent &event);
};
#endif 