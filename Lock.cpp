//Use for AOE Weapon

#include "StdAfx.h"
#include "Lock.h"
#include "Projectile.h"

CRY_IMPLEMENT_GTI(CLock, CSingle);

CLock::CLock()
	: myLockEntity(NULL)
{

}


bool CLock::Shoot(bool resetAnimation, bool autoreload, bool isRemote)
{
	IEntityClass* pAmmoClass = m_fireParams->fireparams.ammo_type_class;

	const EntityId ownerEntityId = m_pWeapon->GetOwnerId();
	CActor *pActor = m_pWeapon->GetOwnerActor();
	
	const bool playerIsShooter = pActor ? pActor->IsPlayer() : false;
	const bool clientIsShooter = pActor ? pActor->IsClient() : false;

	const int clipSize = GetClipSize();
	int ammoCount = (clipSize != 0) ? m_pWeapon->GetAmmoCount(pAmmoClass) : m_pWeapon->GetInventoryAmmoCount(pAmmoClass);

	m_firePending = false;
	if (!CanFire(true))
	{
		if ((ammoCount <= 0) && !m_reloading && !m_reloadPending)
		{
			m_pWeapon->PlayAction(GetFragmentIds().empty_clip);
			m_pWeapon->OnFireWhenOutOfAmmo();
		}
		return false;
	}

	bool bHit = false;
	ray_hit rayhit;	 
	rayhit.pCollider = 0;

	Vec3 hit(0,0,0);
	Vec3 pos(0,0,0);
	Vec3 dir(0,0,0);
	Vec3 vel(0,0,0);
	if (!myLockEntity)
	{
		hit = GetProbableHit(WEAPON_HIT_RANGE, &bHit, &rayhit);
		pos = GetFiringPos(hit);
		dir = GetFiringDir(hit, pos);		//Spread was already applied if required in GetProbableHit( )
		vel = GetFiringVelocity(dir);  
	}
	else
	{
		hit = myLockEntity->GetPos();
		pos = GetFiringPos(hit);
		dir = GetFiringDir(hit, pos);
		vel = GetFiringVelocity(dir);  
	}
	PlayShootAction(ammoCount);
	
	CheckNearMisses(hit, pos, dir, (hit-pos).len(), 1.0f);

	int ammoCost = GetShootAmmoCost(playerIsShooter);
	ammoCost = min(ammoCount, ammoCost);

	EntityId ammoEntityId = 0;
	int ammoPredicitonHandle = 0;
	CProjectile *pAmmo = m_pWeapon->SpawnAmmo(m_fireParams->fireparams.spawn_ammo_class, false);
	if (pAmmo)
	{
		ammoEntityId = pAmmo->GetEntityId();
		ammoPredicitonHandle = pAmmo->GetGameObject()->GetPredictionHandle();

		CRY_ASSERT_MESSAGE(m_fireParams->fireparams.hitTypeId, string().Format("Invalid hit type '%s' in fire params for '%s'", m_fireParams->fireparams.hit_type.c_str(), m_pWeapon->GetEntity()->GetName()));
		CRY_ASSERT_MESSAGE(m_fireParams->fireparams.hitTypeId == g_pGame->GetGameRules()->GetHitTypeId(m_fireParams->fireparams.hit_type.c_str()), "Sanity Check Failed: Stored hit type id does not match the type string, possibly CacheResources wasn't called on this weapon type");

		CProjectile::SProjectileDesc projectileDesc(
			ownerEntityId, m_pWeapon->GetHostId(), m_pWeapon->GetEntityId(), GetDamage(), m_fireParams->fireparams.damage_drop_min_distance,
			m_fireParams->fireparams.damage_drop_per_meter, m_fireParams->fireparams.damage_drop_min_damage, m_fireParams->fireparams.hitTypeId,
			m_fireParams->fireparams.bullet_pierceability_modifier, m_pWeapon->IsZoomed());
		projectileDesc.pointBlankAmount = m_fireParams->fireparams.point_blank_amount;
		projectileDesc.pointBlankDistance = m_fireParams->fireparams.point_blank_distance;
		projectileDesc.pointBlankFalloffDistance = m_fireParams->fireparams.point_blank_falloff_distance;
		if (m_fireParams->fireparams.ignore_damage_falloff)
			projectileDesc.damageFallOffAmount = 0.0f;
		pAmmo->SetParams(projectileDesc);
		// this must be done after owner is set
		pAmmo->InitWithAI();
    
		pAmmo->SetDestination(m_pWeapon->GetDestination());

		const float speedScale = (float)__fsel(-GetShared()->fireparams.speed_override, 1.0f, GetShared()->fireparams.speed_override * __fres(GetAmmoParams()->speed));

		OnSpawnProjectile(pAmmo);

		pAmmo->Launch(pos, dir, vel, m_speed_scale * speedScale);
		pAmmo->CreateBulletTrail(hit);
		pAmmo->SetKnocksTargetInfo( m_fireParams );
   
		pAmmo->SetRemote(isRemote || (!gEnv->bServer && m_pWeapon->IsProxyWeapon()));
		pAmmo->SetFiredViaProxy(m_pWeapon->IsProxyWeapon());

		const STracerParams * tracerParams = &m_fireParams->tracerparams;

		int frequency = tracerParams->frequency;

		bool emit = false;
		if (frequency > 0)
		{
			if(m_pWeapon->GetStats().fp)
				emit = (!tracerParams->geometryFP.empty() || !tracerParams->effectFP.empty()) && ((ammoCount == clipSize) || (ammoCount%frequency == 0));
			else
				emit = (!tracerParams->geometry.empty() || !tracerParams->effect.empty()) && ((ammoCount == clipSize) || (ammoCount%frequency==0));
		}

		if (emit)
		{
			EmitTracer(pos, hit, tracerParams, pAmmo);
		}

		m_projectileId = pAmmo->GetEntityId();

		if (clientIsShooter && pAmmo->IsPredicted() && gEnv->IsClient() && gEnv->bServer)
		{
			pAmmo->GetGameObject()->BindToNetwork();
		}

		pAmmo->SetAmmoCost(ammoCost);
	}

	if (m_pWeapon->IsServer())
	{
		const char *ammoName = pAmmoClass != NULL ? pAmmoClass->GetName() : NULL;
		g_pGame->GetIGameFramework()->GetIGameplayRecorder()->Event(m_pWeapon->GetOwner(), GameplayEvent(eGE_WeaponShot, ammoName, 1, (void *)(EXPAND_PTR)m_pWeapon->GetEntityId()));
	}

	m_muzzleEffect.Shoot(this, hit, m_barrelId);
	RecoilImpulse(pos, dir);

	m_fired = true;
	SetNextShotTime(m_next_shot + m_next_shot_dt);

	if (++m_barrelId == m_fireParams->fireparams.barrel_count)
	{
		m_barrelId = 0;
	}
	
	ammoCount -= ammoCost;
	if (ammoCount <= m_fireParams->fireparams.minimum_ammo_count)
		ammoCount = 0;

	if (clipSize != -1)
	{
		if (clipSize!=0)
			m_pWeapon->SetAmmoCount(pAmmoClass, ammoCount);
		else
			m_pWeapon->SetInventoryAmmoCount(pAmmoClass, ammoCount);
	}

	const EntityId shooterEntityId = ownerEntityId ? ownerEntityId : m_pWeapon->GetHostId();
	OnShoot(shooterEntityId, ammoEntityId, pAmmoClass, pos, dir, vel);

	const SThermalVisionParams& thermalParams = m_fireParams->thermalVisionParams;
	m_pWeapon->AddShootHeatPulse(pActor, thermalParams.weapon_shootHeatPulse, thermalParams.weapon_shootHeatPulseTime,
	thermalParams.owner_shootHeatPulse, thermalParams.owner_shootHeatPulseTime);
	if (OutOfAmmo())
	{
		OnOutOfAmmo(pActor, autoreload);
	}

	m_pWeapon->RequestShoot(pAmmoClass, pos, dir, vel, hit, m_speed_scale, ammoPredicitonHandle, false);

#if defined(ANTI_CHEAT)
	if(pAmmo)
	{
		const uint32 shotId = m_pWeapon->GetLastShotId();
		pAmmo->SetShotId(shotId);
	}
#endif
	return true;
}

void CLock::GetLockEntity(IEntity *lockEntity)
{
	myLockEntity = lockEntity;
}


void CLock::GetMemoryUsage(ICrySizer * s) const
{ 
	s->AddObject(this, sizeof(*this));	
	BaseClass::GetInternalMemoryUsage(s);		// collect memory of parent class
}
