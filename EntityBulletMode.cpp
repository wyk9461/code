#include "StdAfx.h"
#include "EntityBulletMode.h"
#include "Projectile.h"
#include "Code_System/EntityProjectile.h"

CRY_IMPLEMENT_GTI(CEntityBulletMode, CRapid);

CEntityBulletMode::CEntityBulletMode()
	:	curr_type(0)
{

}

bool CEntityBulletMode::Shoot(bool resetAnimation, bool autoreload, bool isRemote/* =false */)
{
	IEntityClass* pAmmoClass = m_fireParams->fireparams.ammo_type_class;

	const EntityId ownerEntityId = m_pWeapon->GetOwnerId();
	CActor *pActor = m_pWeapon->GetOwnerActor();
	
	const bool playerIsShooter = pActor ? pActor->IsPlayer() : false;
	const bool clientIsShooter = pActor ? pActor->IsClient() : false;

	const int clipSize = GetClipSize();
	int ammoCount = (clipSize != 0) ? m_pWeapon->GetAmmoCount(pAmmoClass) : m_pWeapon->GetInventoryAmmoCount(pAmmoClass);

	m_firePending = false;
	if (!CanFire(true))
	{
		if ((ammoCount <= 0) && !m_reloading && !m_reloadPending)
		{
			m_pWeapon->PlayAction(GetFragmentIds().empty_clip);
			m_pWeapon->OnFireWhenOutOfAmmo();
		}
		return false;
	}

	bool bHit = false;
	ray_hit rayhit;	 
	rayhit.pCollider = 0;

	Vec3 hit = GetProbableHit(WEAPON_HIT_RANGE, &bHit, &rayhit);
	Vec3 pos = GetFiringPos(hit);
	Vec3 dir = GetFiringDir(hit, pos);		//Spread was already applied if required in GetProbableHit( )
	Vec3 vel = GetFiringVelocity(dir);  

	PlayShootAction(ammoCount);
	
	CheckNearMisses(hit, pos, dir, (hit-pos).len(), 1.0f);

	int ammoCost = GetShootAmmoCost(playerIsShooter);
	ammoCost = min(ammoCount, ammoCost);

	EntityId ammoEntityId = 0;
	int ammoPredicitonHandle = 0;

	IEntity *pEntity = m_pWeapon->GetBasicEntity();
	CEntityProjectile *pAmmo = (CEntityProjectile*)g_pGame->GetWeaponSystem()->SpawnAmmo(m_fireParams->fireparams.spawn_ammo_class, false);
	if (pAmmo)
	{
		EntityProjectileParams &params = EntityProjectileManager::Instance().GetProjectileParams(curr_type);
		CTracerManager::STracerParams tracer_params = params.m_TracerParams;
		tracer_params.geometry = params.geom.c_str();
		tracer_params.effect = params.effect.c_str();

		//Check unlock first
		if(EntityProjectileManager::Instance().IsProjectileFunctionUnlocked(params.FunctionIndex))
			pAmmo->SetFunctionIndex(params.FunctionIndex);
		else
			pAmmo->SetFunctionIndex(0);

		pAmmo->SetType(curr_type);
		pAmmo->SetDamage(m_pWeapon->GetDamage(curr_type));


		EntityId OwnerId = g_pGame->GetClientActorId();
		CActor *pPlayer = (CActor*)g_pGame->GetGameRules()->GetActorByEntityId(OwnerId);
		EntityId WeaponId = pPlayer->GetCurrentItemId();

		CProjectile::SProjectileDesc projectileDesc(
			ownerEntityId, m_pWeapon->GetHostId(), m_pWeapon->GetEntityId(), GetDamage(), m_fireParams->fireparams.damage_drop_min_distance,
			m_fireParams->fireparams.damage_drop_per_meter, m_fireParams->fireparams.damage_drop_min_damage, m_fireParams->fireparams.hitTypeId,
			m_fireParams->fireparams.bullet_pierceability_modifier, m_pWeapon->IsZoomed());
		projectileDesc.pointBlankAmount = m_fireParams->fireparams.point_blank_amount;
		projectileDesc.pointBlankDistance = m_fireParams->fireparams.point_blank_distance;
		projectileDesc.pointBlankFalloffDistance = m_fireParams->fireparams.point_blank_falloff_distance;
		if (m_fireParams->fireparams.ignore_damage_falloff)
			projectileDesc.damageFallOffAmount = 0.0f;

		ammoEntityId = pAmmo->GetEntityId();
		ammoPredicitonHandle = pAmmo->GetGameObject()->GetPredictionHandle();

		CRY_ASSERT_MESSAGE(m_fireParams->fireparams.hitTypeId, string().Format("Invalid hit type '%s' in fire params for '%s'", m_fireParams->fireparams.hit_type.c_str(), m_pWeapon->GetEntity()->GetName()));
		CRY_ASSERT_MESSAGE(m_fireParams->fireparams.hitTypeId == g_pGame->GetGameRules()->GetHitTypeId(m_fireParams->fireparams.hit_type.c_str()), "Sanity Check Failed: Stored hit type id does not match the type string, possibly CacheResources wasn't called on this weapon type");


		pAmmo->SetParams(projectileDesc);
		// this must be done after owner is set
		pAmmo->InitWithAI();

		pAmmo->SetDestination(m_pWeapon->GetDestination());

		const float speedScale = (float)__fsel(-GetShared()->fireparams.speed_override, 1.0f, GetShared()->fireparams.speed_override * __fres(GetAmmoParams()->speed));

		OnSpawnProjectile(pAmmo);

		SAmmoParams &pAmmoParams = const_cast<SAmmoParams&>(pAmmo->GetAmmoParams());
		pAmmoParams.speed = params.m_TracerParams.speed;
		pAmmoParams.lifetime = params.m_TracerParams.lifetime;

		pAmmo->Launch(pos, dir, dir, speedScale);
		pAmmo->CreateBulletTrail(hit);
		pAmmo->SetKnocksTargetInfo( m_fireParams );

		pAmmo->SetRemote(isRemote || (!gEnv->bServer && m_pWeapon->IsProxyWeapon()));
		pAmmo->SetFiredViaProxy(m_pWeapon->IsProxyWeapon());

		const STracerParams * tracerParams = &m_fireParams->tracerparams;

		int frequency = tracerParams->frequency;

		bool emit = false;
		if (frequency > 0)
		{
			if(m_pWeapon->GetStats().fp)
				emit = (!tracerParams->geometryFP.empty() || !tracerParams->effectFP.empty()) && ((ammoCount == clipSize) || (ammoCount%frequency == 0));
			else
				emit = (!tracerParams->geometry.empty() || !tracerParams->effect.empty()) && ((ammoCount == clipSize) || (ammoCount%frequency==0));
		}

		if (emit)
		{
			//g_pGame->GetWeaponSystem()->GetTracerManager().EmitTracer(tracer_params,pAmmo->GetEntityId());
			EmitTracer(pos, hit, tracer_params, pAmmo);
		}

		m_projectileId = pAmmo->GetEntityId();

		if (clientIsShooter && pAmmo->IsPredicted() && gEnv->IsClient() && gEnv->bServer)
		{
			pAmmo->GetGameObject()->BindToNetwork();
		}

		pAmmo->SetAmmoCost(ammoCost);
	}
		
	if (m_pWeapon->IsServer())
	{
		const char *ammoName = pAmmoClass != NULL ? pAmmoClass->GetName() : NULL;
		g_pGame->GetIGameFramework()->GetIGameplayRecorder()->Event(m_pWeapon->GetOwner(), GameplayEvent(eGE_WeaponShot, ammoName, 1, (void *)(EXPAND_PTR)m_pWeapon->GetEntityId()));
	}

	m_muzzleEffect.Shoot(this, hit, m_barrelId);
	RecoilImpulse(pos, dir);

	m_fired = true;
	SetNextShotTime(m_next_shot + m_next_shot_dt);

	if (++m_barrelId == m_fireParams->fireparams.barrel_count)
	{
		m_barrelId = 0;
	}
	
	ammoCount -= ammoCost;
	if (ammoCount <= m_fireParams->fireparams.minimum_ammo_count)
		ammoCount = 0;

	if (clipSize != -1)
	{
		if (clipSize!=0)
			m_pWeapon->SetAmmoCount(pAmmoClass, ammoCount);
		else
			m_pWeapon->SetInventoryAmmoCount(pAmmoClass, ammoCount);
	}

	const EntityId shooterEntityId = ownerEntityId ? ownerEntityId : m_pWeapon->GetHostId();
	OnShoot(shooterEntityId, ammoEntityId, pAmmoClass, pos, dir, vel);

	const SThermalVisionParams& thermalParams = m_fireParams->thermalVisionParams;
	m_pWeapon->AddShootHeatPulse(pActor, thermalParams.weapon_shootHeatPulse, thermalParams.weapon_shootHeatPulseTime,
	thermalParams.owner_shootHeatPulse, thermalParams.owner_shootHeatPulseTime);
	if (OutOfAmmo())
	{
		OnOutOfAmmo(pActor, autoreload);
	}

	//CryLog("RequestShoot - pos(%f,%f,%f), dir(%f,%f,%f), hit(%f,%f,%f)", pos.x, pos.y, pos.z, dir.x, dir.y, dir.z, hit.x, hit.y, hit.z);
	m_pWeapon->RequestShoot(pAmmoClass, pos, dir, vel, hit, m_speed_scale, ammoPredicitonHandle, false);

#if defined(ANTI_CHEAT)
		if(pAmmo)
		{
			const uint32 shotId = m_pWeapon->GetLastShotId();
			pAmmo->SetShotId(shotId);
		}
#endif

#ifdef DEBUG_AIMING_AND_SHOOTING

	static ICVar* pAimDebug = gEnv->pConsole->GetCVar("g_aimdebug");
	if (pAimDebug->GetIVal()) 
	{
		IPersistantDebug* pDebug = g_pGame->GetIGameFramework()->GetIPersistantDebug();
		pDebug->Begin("CSingle::Shoot", false);
		pDebug->AddSphere(hit, 0.05f, ColorF(0,0,1,1), 10.f);
		pDebug->AddDirection(pos, 0.25f, dir, ColorF(0,0,1,1), 1.f);
	}

	//---------------------------------------------------------------------------
	// TODO remove when aiming/fire direction is working
	// debugging aiming dir
	if(++DGB_curLimit>DGB_ShotCounter)	DGB_curLimit = DGB_ShotCounter;
	if(++DGB_curIdx>=DGB_ShotCounter)	DGB_curIdx = 0;
	DGB_shots[DGB_curIdx].dst=pos+dir*200.f;
	DGB_shots[DGB_curIdx].src=pos;
	//---------------------------------------------------------------------------
#endif
	return true;
}

void CEntityBulletMode::GetMemoryUsage(ICrySizer * s) const
{ 
	s->AddObject(this, sizeof(*this));	
	BaseClass::GetInternalMemoryUsage(s);		// collect memory of parent class
}

void CEntityBulletMode::Reload(int zoomed)
{
	StartReload(zoomed);
}

bool CEntityBulletMode::FillAmmo(bool fromInventory)
{
	//Does't finish
	//Setclips
	IEntityClass* ammo = m_fireParams->fireparams.ammo_type_class;
	if (clips_Deq.empty())
	{
		m_pWeapon->SetAmmoCount(ammo, 0);
		return false;
	}

	std::deque<int>::iterator it;
	it = clips_Deq.begin();
	//get second type
	++it;


	if (it == clips_Deq.end())
	{
		clips_Deq.clear();
		m_pWeapon->DeleteBasicEntityClip(curr_type);
		m_pWeapon->SetAmmoCount(ammo, 0);
		return false;
	}
	
	//delete current type in EntityGun's map
	m_pWeapon->DeleteBasicEntityClip(curr_type);

	//update current type
	curr_type = *it;
	clips_Deq.pop_front();

	m_pWeapon->SetCurType(curr_type);

	EntityProjectileParams &rParams = EntityProjectileManager::Instance().GetProjectileParams(curr_type);
	SFireModeParams* tFireModeParams=GetFireModeParams_Non_Const();
	SFireParams &rBasicfireParams=const_cast<SFireParams&>(tFireModeParams->fireparams);
	rBasicfireParams.clip_size = rParams.clipsize;
	rBasicfireParams.rate = rParams.firerate;

	bool ammoFilledUp = true;

	const int clipSize = GetClipSize();
	const int ammo_count = m_pWeapon->GetEntityAmmoCount(curr_type);
	if (m_pWeapon->IsServer() && !m_reloadCancelled && !clips_Deq.empty())
	{
		CActor * pOwnerActor = m_pWeapon->GetOwnerActor();

		m_pWeapon->SetAmmoCount(ammo, ammo_count);

		const char* ammoName = ammo ? ammo->GetName() : "";

		g_pGame->GetIGameFramework()->GetIGameplayRecorder()->Event(m_pWeapon->GetOwner(), GameplayEvent(eGE_WeaponReload, ammoName, 0, (void *)(EXPAND_PTR)(m_pWeapon->GetEntityId())));
	}

	if (ammo_count < clipSize)
	{
		ammoFilledUp = false;
	}

	return ammoFilledUp;
}

// void CEntityBulletMode::StartReload(int zoomed)
// {
// 	//CCCPOINT(Single_StartReload);
// 
// 	if (zoomed != 0)
// 		m_pWeapon->ExitZoom(true);
// 	//m_pWeapon->SetBusy(true);
// 
// 	FillAmmo(true);
// }

void CEntityBulletMode::ClipSwitch()
{
	std::deque<int> temp_Deq;
	std::deque<int>::iterator it;
	//locate the current type in clip_Deq and switch it to first
	for (it = clips_Deq.begin(); it != clips_Deq.end(); ++it)
	{
		if (*it == curr_type)
			break;
	}

	while (!clips_Deq.empty())
	{
		if (it == clips_Deq.end())
		{
			it = clips_Deq.begin();
		}
		temp_Deq.push_back(*it);
		it = clips_Deq.erase(it);
	}
	for (it = temp_Deq.begin(); it != temp_Deq.end(); ++it)
	{
		clips_Deq.push_back(*it);
	}
	temp_Deq.clear();

	UpdateUIInfo();
}

void CEntityBulletMode::PushEntityBulletClipDeque(int type)
{
	clips_Deq.push_front(type);
	curr_type = type;
}

void CEntityBulletMode::PopEntityBulletClipDeque()
{
	IEntityClass* ammo = m_fireParams->fireparams.ammo_type_class;
	if (clips_Deq.empty())
	{
		return;
	}

	std::deque<int>::iterator it;
	it = clips_Deq.begin();
	//get second type
	++it;


	if (it == clips_Deq.end())
	{
		clips_Deq.clear();
		m_pWeapon->SetAmmoCount(ammo, 0);
		return;
	}


	//update current type
	curr_type = *it;
	clips_Deq.pop_front();

	m_pWeapon->SetCurType(curr_type);
}

// int CEntityBulletMode::GetCurrType()
// {
// 	return curr_type;
// }

void CEntityBulletMode::SelectNextClip(bool next)
{
	if (clips_Deq.empty())
	{
		return;
	}
	std::deque<int>::iterator it;
	
	if (next)
	{
		it = clips_Deq.begin();
		if(++it == clips_Deq.end())
		{
			it = clips_Deq.begin();		//Fix the iteration bug -- if it == end then crash 
		}
		curr_type = *it;
		m_pWeapon->SetCurType(curr_type);
		ClipSwitch();
	}
	else
	{
		it = clips_Deq.end();
		--it;
		curr_type = *it;
		m_pWeapon->SetCurType(curr_type);
		ClipSwitch();
	}
	EntityProjectileParams &rParams = EntityProjectileManager::Instance().GetProjectileParams(curr_type);
	SFireModeParams* tFireModeParams=GetFireModeParams_Non_Const();
	SFireParams &rBasicfireParams=const_cast<SFireParams&>(tFireModeParams->fireparams);
	rBasicfireParams.clip_size = rParams.clipsize;
	rBasicfireParams.rate = rParams.firerate;

	IEntityClass* ammo = m_fireParams->fireparams.ammo_type_class;
	const int clipSize = GetClipSize();
	const int ammo_count = m_pWeapon->GetEntityAmmoCount(curr_type);
	if (m_pWeapon->IsServer() && !m_reloadCancelled && !clips_Deq.empty())
	{
		CActor * pOwnerActor = m_pWeapon->GetOwnerActor();

		m_pWeapon->SetAmmoCount(ammo, ammo_count);

		const char* ammoName = ammo ? ammo->GetName() : "";

		g_pGame->GetIGameFramework()->GetIGameplayRecorder()->Event(m_pWeapon->GetOwner(), GameplayEvent(eGE_WeaponReload, ammoName, 0, (void *)(EXPAND_PTR)(m_pWeapon->GetEntityId())));
	}
}

void CEntityBulletMode::SetCurrType(int type)
{
	curr_type = type;
	ClipSwitch();
}

void CEntityBulletMode::EmitTracer(const Vec3& pos,const Vec3& destination, CTracerManager::STracerParams &params, CProjectile* pProjectile)
{
	Vec3 dir = (destination - pos).normalize();
	params.position = pos + dir * 2;
	params.destination = destination;

	const int tracerIdx = g_pGame->GetWeaponSystem()->GetTracerManager().EmitTracer(params, pProjectile ? pProjectile->GetEntityId() : 0);

	if (pProjectile)
	{
		pProjectile->BindToTracer(tracerIdx);
	}
}

void CEntityBulletMode::UpdateUIInfo()
{
	IUIElement *pElement = gEnv->pFlashUI->GetUIElement("HUD_Final_2");
	if(pElement && !clips_Deq.empty())
	{
		int prev_type = *clips_Deq.rbegin();
		int prev_ammo = m_pWeapon->GetEntityAmmoCount(prev_type);

		int cur_type = *(clips_Deq.begin());
		int cur_ammo = m_pWeapon->GetEntityAmmoCount(cur_type);
		int next_type;
		int next_ammo;

		if(clips_Deq.size() > 1)
		{
			next_type = *(++clips_Deq.begin());
			next_ammo = m_pWeapon->GetEntityAmmoCount(next_type);
		}
		else if(clips_Deq.size() == 1)
		{
			prev_type = cur_type;
			prev_ammo = 0;
			next_type = cur_type;
			next_ammo = 0;
		}


		SUIArguments args;
		args.AddArgument(prev_type);
		args.AddArgument(prev_ammo);
		pElement->CallFunction("SetPrevAmmo", args);
		args.Clear();
		args.AddArgument(cur_type);
		args.AddArgument(cur_ammo);
		pElement->CallFunction("SetCurAmmo", args);
		args.Clear();
		args.AddArgument(next_type);
		args.AddArgument(next_ammo);
		pElement->CallFunction("SetNextAmmo", args);
		args.Clear();
	}
	if(pElement && clips_Deq.empty())
	{
		SUIArguments args;
		args.AddArgument(0);
		args.AddArgument(0);
		pElement->CallFunction("SetPrevAmmo", args);
		args.Clear();
		args.AddArgument(0);
		args.AddArgument(0);
		pElement->CallFunction("SetCurAmmo", args);
		args.Clear();
		args.AddArgument(0);
		args.AddArgument(0);
		pElement->CallFunction("SetNextAmmo", args);
		args.Clear();
	}
}