#include "StdAfx.h"
#include "SmashGun.h"
#include "Weapon.h"
#include "Burst.h"

CSmashGun::CSmashGun()
	: startTime(0)
	, incTime(0)
	, bInc(true)
	, fireLimit(80)
	, bStartFire(false)
{
}

CSmashGun::~CSmashGun()
{
}

void CSmashGun::Update(SEntityUpdateContext& ctx, int slot)
{
	BaseClass::Update(ctx,slot);

	if (bInc == false)
	{
		startTime += ctx.fFrameTime;
		if(startTime > 10)
		{
			bInc = true;
			startTime = 0;
		}
	}
	if (bInc == true)
	{
		int currentAmmoCount = GetFireMode(GetCurrentFireMode())->GetAmmoCount();
		int clipSize = GetFireMode(GetCurrentFireMode())->GetClipSize();
			
		if (currentAmmoCount < clipSize)
		{
			pAmmoType = GetFireMode(GetCurrentFireMode())->GetAmmoType();
			incTime += ctx.fFrameTime;
			
			if (incTime > 0.25)
			{
				SetAmmoCount(pAmmoType, ++currentAmmoCount);
				incTime = 0;
			}
		}
	}

	//auto fire mode
	int currentAmmoCount = GetFireMode(GetCurrentFireMode())->GetAmmoCount();
	if (currentAmmoCount == 1)
	{
		bStartFire = false;
	}

	if (bStartFire)
	{
		BaseClass::StartFire();
	}
	if (!bStartFire)
	{
		BaseClass::StopFire();
	}
}

void CSmashGun::OnShoot(EntityId shooterId, EntityId ammoId, IEntityClass* pAmmoType, const Vec3 &pos, const Vec3 &dir, const Vec3 &vel)
{
	int currentAmmoCount = GetFireMode(GetCurrentFireMode())->GetAmmoCount();

	BaseClass::OnShoot(shooterId, ammoId, pAmmoType, pos, dir, vel);

	bInc = false;
}


void CSmashGun::StartFire()
{
	int currentAmmoCount = GetFireMode(GetCurrentFireMode())->GetAmmoCount();
	if (fireLimit <= currentAmmoCount)
	{
		//auto fire control
		bStartFire = true;
		BaseClass::StartFire();
	}
}


bool CSmashGun::IsMounted() const
{
	if (bStartFire)
	{
		return true;
	}
	return false;
}